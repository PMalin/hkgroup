
HerbInterface <- {
    herbId = -1,
    bucket = null,
    eggTimer = null,

    prepareWindow = function()
    {
        local result = {};

        result.window <- GUI.Window(anx(Resolution.x/2 - 120), any(Resolution.y/2 - 252), anx(240), any(380), "HK_BOX.TGA", null, false);
        result.careButton <- GUI.Button(anx(20), any(40), anx(200), any(60), "HK_BUTTON.TGA","Piel�gnuj", result.window);
        result.destroyButton <- GUI.Button(anx(20), any(120), anx(200), any(60), "HK_BUTTON.TGA","Zniszcz", result.window);
        result.takeButton <- GUI.Button(anx(20), any(200), anx(200), any(60), "HK_BUTTON.TGA","Zbierz", result.window);
        result.closeButton <- GUI.Button(anx(20), any(280), anx(200), any(60), "HK_BUTTON.TGA","Wyjd�", result.window);

        result.careButton.bind(EventType.Click, function(element) {
            if(getPlayerStamina(heroId) < 1.5) {
                addPlayerNotification(heroId, "Jeste� zbyt zm�czony.")
                return;
            }

            HerbInterface.eggTimer = EggTimer(5000);
            playAni(heroId, "T_PODLEWANIE");
            HerbInterface.bucket.window.setVisible(false);
            HerbInterface.eggTimer.attribute = HerbInterface.herbId;
            HerbInterface.eggTimer.onEnd = function(eleobjectment) {
                HerbPacket(getHerb(eleobjectment.attribute)).updateStamina();
                HerbInterface.hide();
            }
        });

        result.destroyButton.bind(EventType.Click, function(element) {
            if(getPlayerStamina(heroId) < 3.0) {
                addPlayerNotification(heroId, "Jeste� zbyt zm�czony.")
                return;
            }

            HerbInterface.eggTimer = EggTimer(5000);
            playAni(heroId, "S_DIGGER_S1");
            HerbInterface.bucket.window.setVisible(false);
            HerbInterface.eggTimer.attribute = HerbInterface.herbId;
            HerbInterface.eggTimer.onEnd = function(eleobjectment) {
                HerbPacket(getHerb(eleobjectment.attribute)).deleteHerb();
                HerbInterface.hide();
            }
        });

        result.takeButton.bind(EventType.Click, function(element) {
            if(getPlayerStamina(heroId) < 1.0) {
                addPlayerNotification(heroId, "Jeste� zbyt zm�czony.")
                return;
            }

            HerbInterface.eggTimer = EggTimer(5000);
            playAni(heroId, "T_WYRYWANIE");
            HerbInterface.bucket.window.setVisible(false);
            HerbInterface.eggTimer.attribute = HerbInterface.herbId;
            HerbInterface.eggTimer.onEnd = function(eleobjectment) {
                HerbPacket(getHerb(eleobjectment.attribute)).takeHerb();
                HerbInterface.hide();
            }
        });

        result.closeButton.bind(EventType.Click, function(element) {
            HerbInterface.hide();
        });

        return result;
    }

    show = function(_herbId)
    {
        if(bucket == null)
            bucket = prepareWindow();

        herbId = _herbId;
        BaseGUI.show();
        ActiveGui = PlayerGUI.Herb;
        bucket.window.setVisible(true);

        playAni(heroId, "S_DIGGER_S0");
    }

    hide = function()
    {
        bucket.window.setVisible(false);

        herbId = -1;
        BaseGUI.hide();
        ActiveGui = null;

        playAni(heroId,"S_RUN");
        resetEggTimer();
        eggTimer = null;
    }

    onForceCloseGUI = function()
    {
        if(ActiveGui == PlayerGUI.Herb)
            hide();
    }
}

addEventHandler("onForceCloseGUI", HerbInterface.onForceCloseGUI.bindenv(HerbInterface));
Bind.addKey(KEY_ESCAPE, HerbInterface.hide.bindenv(HerbInterface), PlayerGUI.Herb)
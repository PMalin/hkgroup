
HerbInterfaceCreate <- {
    groundLevel = -1,
    selectedScheme = -1,

    temporaryVob = null,
    bucket = null,
    eggTimer = null,

    prepareWindow = function()
    {
        local result = {};

        result.window <- GUI.Window(anx(Resolution.x - 480), any(Resolution.y/2 - 270), anx(440), any(540), "HK_BOX.TGA", null, false);
        result.labels <- [];
        result.buttons <- [];
        result.texts <- [];
        result.draws <- [];

        foreach(name, object in Config["HerbSystem"])
        {
            result.labels.append({
                name = name,
                instance = object[object.len() - 1].instance,
                requiredItem = object[0].instance,
                difficulty = object[0].difficulty,
                time = object[0].time,
                ground = "ground" in object[0] ? object[0].ground : 0,
            });
        }

        result.windowDescription <- GUI.Window(anx(Resolution.x/2 - 400), any(Resolution.y - 270), anx(800), any(240), "HK_BOX.TGA", null, false);
        result.description <- GUI.Draw(anx(40), any(30), "", result.windowDescription);
        result.addButton <- GUI.Button(anx(40), any(160), anx(150), any(60), "HK_BUTTON.TGA","Dodaj", result.windowDescription);

        result.addButton.bind(EventType.Click, function(element) {
            if(getPlayerStamina(heroId) < 1.5) {
                addPlayerNotification(heroId, "Jeste� zbyt zm�czony.")
                return;
            }

            HerbInterfaceCreate.eggTimer = EggTimer(5000);
            playAni(heroId, "S_DIGGER_S1");
            HerbInterfaceCreate.eggTimer.attribute = element.attribute;
            HerbInterfaceCreate.eggTimer.onEnd = function(eleobjectment) {
                local pos = HerbInterfaceCreate.temporaryVob.getPosition();
                local packet = Packet();
                packet.writeUInt8(PacketReceivers);
                packet.writeUInt8(Packets.Herb);
                packet.writeUInt8(HerbPackets.Create);
                packet.writeString(HerbInterfaceCreate.selectedScheme);
                packet.writeFloat(pos.x);
                packet.writeFloat(pos.y);
                packet.writeFloat(pos.z);
                packet.send(RELIABLE);
                HerbInterfaceCreate.hide();
            }
        });

        result.scrollBar <- GUI.ScrollBar(anx(440), any(50), anx(30), any(410), "HK_SCROLL.TGA", "HK_INDICATOR.TGA", "HK_U.TGA", "HK_O.TGA", Orientation.Vertical, result.window);

        result.bindForScrollbar <- Bind.onChange(result.scrollBar, function(value) {
            HerbInterfaceCreate.refresh(value);
        }, PlayerGUI.HerbCreate);

        return result;
    }

    show = function(_groundLevel)
    {
        if(bucket == null)
            bucket = prepareWindow();

        groundLevel = _groundLevel;
        BaseGUI.show();
        ActiveGui = PlayerGUI.HerbCreate;
        playAni(heroId, "S_DIGGER_S1");

        local pos = getPlayerPosition(heroId), angle = getPlayerAngle(heroId);
        temporaryVob = Vob("KOPA.3DS");
        temporaryVob.cdDynamic = false;
        temporaryVob.cdStatic = false;
        temporaryVob.setPosition(pos.x + (sin(angle * 3.14 / 180.0) * 120), pos.y - 92.5, pos.z + (cos(angle * 3.14 / 180.0) * 120));
        temporaryVob.setRotation(0, angle, 0);
        temporaryVob.addToWorld();

        bucket.window.setVisible(true);

        refresh();
    }

    hide = function()
    {
        bucket.window.setVisible(false);
        bucket.windowDescription.setVisible(false);

        groundLevel = -1;
        BaseGUI.hide();
        ActiveGui = null;
        temporaryVob = null;

        foreach(butt in bucket.buttons) {
            butt.setVisible(false);
            butt.destroy();
        }

        bucket.buttons.clear();
        bucket.texts.clear();
        bucket.draws.clear();

        resetEggTimer();
        HerbInterfaceCreate.eggTimer = null;

        playAni(heroId,"S_RUN");
    }

    refresh = function(value = 0)
    {
        bucket.texts.clear();

        foreach(butt in bucket.buttons) {
            butt.setVisible(false);
            butt.destroy();
        }

        bucket.buttons.clear();

        local visibleBars = 0;
        local posX = 0, posY = 0;
        foreach(_index, object in bucket.labels)
        {
            if(_index < value)
                continue;

            if(visibleBars > 12)
                break;

            bucket.buttons.push(GUI.Button(anx(Resolution.x - 460 + (100 * posX)), any(Resolution.y/2 - 250 + (80 * posY)), anx(100), any(80), "HK_BUTTON.TGA", ""));
            bucket.buttons[bucket.buttons.len() - 1].setVisible(true);
            bucket.buttons[bucket.buttons.len() - 1].attribute = _index;
            local text = ItemRender(anx(Resolution.x - 460 + (100 * posX)), any(Resolution.y/2 - 250 + (80 * posY)), anx(100), any(80), object.instance);
            text.visible = true;
            bucket.texts.push(text);

            posX = posX + 1;
            visibleBars = visibleBars + 1;

            if(posX > 3)
            {
                posX = 0;
                posY = posY + 1;
            }
        }
    }

    showDescription = function(_index)
    {
        local object = bucket.labels[_index];
        selectedScheme = object.name;
        bucket.windowDescription.setVisible(true);

        local groundColor = "55FF55";
        if(object.ground > groundLevel)
            groundColor = "FF5555";

        local skillColor = "55FF55";
        if(object.difficulty > Hero.skills[PlayerSkill.Herbalist])
            skillColor = "FF5555";

        local text = "Nazwa schematu: "+object.name+"\nRo�lina: "+getItemName(object.instance)+" Wymagane nasiono: "+getItemName(object.requiredItem);
        text += "\nWymagana umiej�tno�� [#"+skillColor+"]"+object.difficulty+"[#FFFFFF] Czas: "+object.time+" sekund.\nGleba: [#"+groundColor+"]"+Config["GroundLevel"][object.ground];

        bucket.description.setText(text);
    }

    onClick = function(element)
    {
        if(ActiveGui != PlayerGUI.HerbCreate)
            return;

        foreach(butt in bucket.buttons) {
            if(butt == element) {
                showDescription(butt.attribute);
                break;
            }
        }
    }

    onForceCloseGUI = function()
    {
        if(ActiveGui == PlayerGUI.HerbCreate)
            hide();
    }
}

addEventHandler("onForceCloseGUI", HerbInterfaceCreate.onForceCloseGUI.bindenv(HerbInterfaceCreate));
addEventHandler("GUI.onClick", HerbInterfaceCreate.onClick.bindenv(HerbInterfaceCreate));
Bind.addKey(KEY_ESCAPE, HerbInterfaceCreate.hide.bindenv(HerbInterfaceCreate), PlayerGUI.HerbCreate)
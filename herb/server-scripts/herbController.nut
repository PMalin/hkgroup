
class HerbController
{
    static herbs = {};

    static function onInit() {
        local herbsInDatabase = Query().select().from(Herb.HerbTable).all();

        foreach(herbObject in herbsInDatabase)
        {
            local newHerb = Herb(herbObject.id.tointeger())

            newHerb.scheme = herbObject.scheme;
            newHerb.status = herbObject.status.tointeger();
            newHerb.stamina = herbObject.stamina.tointeger();
            newHerb.growth = herbObject.growth.tointeger();
            newHerb.rotation = herbObject.rotation.tointeger();
            newHerb.position = {x = herbObject.x.tofloat(), y = herbObject.y.tofloat(), z = herbObject.z.tofloat()}

            herbs[newHerb.id] <- newHerb;
        }

        print("We have " + herbs.len() + " growing plants.");
    }

    static function onSecond() {
        local idsOfHerbs = [];

        foreach(herbId, herb in herbs)
        {
            if(herb.onSecond())
                idsOfHerbs.push({id = herbId, growth = herb.growth, stamina = herb.stamina, status = herb.status});
        }

        if(idsOfHerbs.len() == 0)
            return;

        local packet = Packet();
        packet.writeUInt8(PacketReceivers);
        packet.writeUInt8(Packets.Herb);
        packet.writeUInt8(HerbPackets.Update);
        packet.writeInt16(idsOfHerbs.len());
        foreach(obj in idsOfHerbs)
        {
            packet.writeInt32(obj.id);
            packet.writeInt16(obj.growth);
            packet.writeInt16(obj.stamina);
            packet.writeInt16(obj.status);
        }
        packet.sendToAll(RELIABLE);
    }

    static function onPlayerLoad(playerId) {
        foreach(herb in herbs)
            HerbPacket(herb).create(playerId);
    }

    static function getNearHerb(pos) {
        foreach(herbId, herb in herbs)
            if(getPositionDifference(pos, herb.position) < 150)
                return herbId;

        return null;
    }

    static function onPacket(playerId, packet) {
        local typ = packet.readUInt8();

        switch(typ)
        {
            case HerbPackets.Create:
                if(getPlayerStamina(playerId) < 1.0) {
                    addPlayerNotification(playerId, "Jeste� zbyt zm�czony.");
                    return;
                }

                setPlayerStamina(playerId, getPlayerStamina(playerId) - 1.0);

                local newHerb = Herb(-1);
                newHerb.scheme = packet.readString();

                local firstObject = newHerb.getScheme()[0];
                local difficulty = "difficulty" in firstObject ? firstObject.difficulty : 1;
                if(difficulty > getPlayerSkill(playerId, PlayerSkill.Herbalist)) {
                    addPlayerNotification(playerId, "Zbyt niski poziom umiej�tno�ci.");
                    return;
                }

                local instance = firstObject.instance;
                local amount = "amount" in firstObject ? firstObject.amount : 1;

                if(hasPlayerItem(playerId, instance) < amount) {
                    addPlayerNotification(playerId, "Nie masz nasion.");
                    return;
                }

                removeItem(playerId, instance, amount);

                local object = Config["ChatMethod"][ChatMethods.Action];
                distanceChat(playerId, ChatType.IC, object.distance, object.r, object.g, object.b, "[PLAYER:"+playerId+"] Posadzi� ro�lin�");

                newHerb.status = 0;
                newHerb.stamina = 100;
                newHerb.growth = 0;
                newHerb.rotation = getPlayerAngle(playerId);
                newHerb.position = {x = packet.readFloat(), y = packet.readFloat(), z = packet.readFloat()}

                setPlayerStamina(playerId, getPlayerStamina(playerId) - 1.5);

                Query().insertInto(Herb.HerbTable, ["scheme", "status", "stamina", "growth", "rotation", "x", "y", "z", "userId"
                ], [
                    "'"+newHerb.scheme+"'", newHerb.status, newHerb.stamina, newHerb.growth, newHerb.rotation, newHerb.position.x, newHerb.position.y, newHerb.position.z, getPlayer(playerId).rId,
                ]).execute();

                newHerb.id = DB.queryGet("SELECT id FROM "+Herb.HerbTable+" ORDER BY id DESC LIMIT 1")["id"];
                herbs[newHerb.id] <- newHerb;

                HerbPacket(newHerb).create();
                addPlayerLog(playerId, "Posadzono "+newHerb.scheme+" w "+newHerb.position.x+","+newHerb.position.y+","+newHerb.position.z);

                local pObject = getPlayer(playerId);
                foreach(_item in pObject.inventory.getItems()) {
                    if(_item.instance == "ITMW_HK_2H_OTH_07") {
                        if(_item.resistance > 0) {
                            _item.resistance = _item.resistance;
                            _item.sendUpdate(playerId);
                            break;
                        }
                    }
                }
            break;
            case HerbPackets.Growth:
                local id = packet.readInt32();
                if(!(id in herbs))
                    return;

                if(getPlayerStamina(playerId) < 1.0)
                    return;

                setPlayerStamina(playerId, getPlayerStamina(playerId) - 1.0);
                local herbObject = getHerb(id);

                local instance = herbObject.getScheme()[herbObject.status].instance;
                local amount = "amount" in herbObject.getScheme()[herbObject.status] ? herbObject.getScheme()[herbObject.status].amount : 1;

                giveItem(playerId, instance, amount);

                local object = Config["ChatMethod"][ChatMethods.Action];
                distanceChat(playerId, ChatType.IC, object.distance, object.r, object.g, object.b, "[PLAYER:"+playerId+"] Zebra� uprawy");
                DB.queryGet("DELETE FROM "+Herb.HerbTable+" WHERE id = "+id);
                HerbPacket(herbObject).deleteHerb();
                addPlayerLog(playerId, "Zebrano "+herbObject.scheme+" w "+herbObject.position.x+","+herbObject.position.y+","+herbObject.position.z);
                herbs.rawdelete(id);

                local pObject = getPlayer(playerId);
                foreach(_item in pObject.inventory.getItems()) {
                    if(_item.instance == "ITMW_HK_2H_OTH_07") {
                        if(_item.resistance > 0) {
                            _item.resistance = _item.resistance - 1;
                            _item.sendUpdate(playerId);
                            break;
                        }
                    }
                }
            break;
            case HerbPackets.Stamina:
                local id = packet.readInt32();
                if(!(id in herbs))
                    return;

                if(getPlayerStamina(playerId) < 1.5)
                    return;

                setPlayerStamina(playerId, getPlayerStamina(playerId) - 1.5);
                local herbObject = getHerb(id);

                herbObject.stamina = herbObject.stamina + 25;
                if(herbObject.stamina > 100)
                    herbObject.stamina = 100;

                local packet = Packet();
                packet.writeUInt8(PacketReceivers);
                packet.writeUInt8(Packets.Herb);
                packet.writeUInt8(HerbPackets.Update);
                packet.writeInt16(1);
                packet.writeInt32(herbObject.id);
                packet.writeInt16(herbObject.growth);
                packet.writeInt16(herbObject.stamina);
                packet.writeInt16(herbObject.status);
                packet.sendToAll(RELIABLE);
            break;
            case HerbPackets.Delete:
                local id = packet.readInt32();
                if(!(id in herbs))
                    return;

                if(getPlayerStamina(playerId) < 3.0)
                    return;

                setPlayerStamina(playerId, getPlayerStamina(playerId) - 3.0);
                local herbObject = getHerb(id);
                local object = Config["ChatMethod"][ChatMethods.Action];
                distanceChat(playerId, ChatType.IC, object.distance, object.r, object.g, object.b, "[PLAYER:"+playerId+"] Zniszczy� uprawy");

                DB.queryGet("DELETE FROM "+Herb.HerbTable+" WHERE id = "+id);
                addPlayerLog(playerId, "Zniszczyl "+herbObject.scheme+" w "+herbObject.position.x+","+herbObject.position.y+","+herbObject.position.z);
                HerbPacket(getHerb(id)).deleteHerb();
                herbs.rawdelete(id);
            break;
        }
    }
}

getHerb <- @(id) HerbController.herbs[id];
getHerbs <- @() HerbController.herbs;

setTimer(HerbController.onSecond.bindenv(HerbController), 1000, 0);
addEventHandler("onInit", HerbController.onInit.bindenv(HerbController))
addEventHandler("onPlayerJoin", HerbController.onPlayerLoad.bindenv(HerbController))
RegisterPacketReceiver(Packets.Herb, HerbController.onPacket.bindenv(HerbController));

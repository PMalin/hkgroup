
class CraftPacket
{
    packet = null;

    constructor()
    {
        this.packet = ExtendedPacket(Packets.Craft);
    }

    function use(playerId, message)
    {
        packet.writeUInt8(CraftPackets.Use)
        packet.writeString(message);
        packet.send(playerId);
    }

    function sendCraft(playerId, craft) {
        packet.writeUInt8(CraftPackets.Send)
        packet.writeInt32(craft.id);
        packet.writeString(craft.name);
        packet.writeInt16(craft.items.len())
        foreach(instance, amount in craft.items) {
            packet.writeString(instance);
            packet.writeInt16(amount);
        }
        packet.writeInt16(craft.rewards.len())
        foreach(instance, amount in craft.rewards) {
            packet.writeString(instance);
            packet.writeInt16(amount);
        }
        packet.writeString(craft.createdAt);
        packet.writeString(craft.createdBy);
        packet.writeString(craft.animation);
        packet.writeInt16(craft.caller);
        packet.writeFloat(craft.staminaCost);
        packet.writeInt16(craft.skillRequired.len())
        for(local i = 0; i < craft.skillRequired.len(); i ++) {
            packet.writeInt16(craft.skillRequired[i]);
            packet.writeInt16(craft.skillValue[i]);
        }
        packet.send(playerId);
    }
}

CraftListInterface <- {
    bucket = null,
    list = [],
    active = false,

    prepareWindow = function() {
        local result = {};

        result.background <- Texture(anx(0), any(0), anx(440), any(Resolution.y), "DLG_CONVERSATION.TGA");
        result.window <- GUI.Window(anx(50), any(Resolution.y/2 - 450), anx(340), any(740), "HK_BOX.TGA");
        result.scrollbarList <- GUI.ScrollBar(anx(350), any(50), anx(30), any(580), "HK_SCROLL.TGA", "HK_BUTTON.TGA", "HK_U.TGA", "HK_O.TGA", Orientation.Vertical, result.window);
        result.search <- GUI.Input(anx(25), any(25), anx(290), any(45), "HK_INPUT.TGA", "FONT_OLD_10_WHITE_HI.TGA", Input.Text, Align.Center, "Szukaj..", 2, result.window);
        result.listContent <- [];

        result.createButton <- GUI.Button(anx(50), any(750), anx(240), any(50), "HK_BUTTON.TGA", "Utw�rz", result.window);
        result.leaveButton <- GUI.Button(anx(50), any(825), anx(240), any(50), "HK_BUTTON.TGA", "Zamknij", result.window);

        result.createButton.attribute = -1;
        result.createButton.bind(EventType.Click, function(element) {
            CraftListInterface.create(element.attribute);
        });

        result.leaveButton.bind(EventType.Click, function(element) {
            CraftListInterface.hide();
        });

        result.bindForScrollbar <- Bind.onChange(result.scrollbarList, function(value) {
            CraftListInterface.refresh();
        }, PlayerGUI.CraftList);

        result.previewInfo <- {
            arrow = null,
            render = null,
            name = null,
            info = null,
            items = null,
            descriptionItem = null,
            errorTexture = null,
            errorDraw = null,
        }

        return result;
    }

    onInputInsertLetter = function(inpt, letter) {
        if(ActiveGui != PlayerGUI.CraftList)
            return;

        if(inpt != bucket.search)
            return;

        local text = bucket.search.getText().toupper();

        if(text.len() > 0)
        {
            list.sort(function (a,b)
            {
                a = getCraft(a);
                b = getCraft(b);

                if(a.getName().toupper().find(text) == null && b.getName().toupper().find(text) != null)
                    return 1;

                if(b.getName().toupper().find(text) == null && a.getName().toupper().find(text) != null)
                    return -1;

                if (a.getSkillValueTotal() > b.getSkillValueTotal()) return 1;
                if (a.getSkillValueTotal() < b.getSkillValueTotal()) return -1;
                return 0;
            });
        }else{
            list.sort(function (a,b)
            {
                a = getCraft(a);
                b = getCraft(b);
                if (a.getSkillValueTotal() > b.getSkillValueTotal()) return 1;
                if (a.getSkillValueTotal() < b.getSkillValueTotal()) return -1;
                return 0;
            });
        }

        refresh();
    }

    setError = function(text)
    {
        bucket.previewInfo.errorDraw = Draw(anx(0), any(0), text);
        if(getSetting("OtherCustomFont")) {
            bucket.previewInfo.errorDraw.font = "FONT_OLD_20_WHITE_HI.TGA";
            bucket.previewInfo.errorDraw.setScale(0.45, 0.45);
        }

        local width = bucket.previewInfo.errorDraw.widthPx;

        bucket.previewInfo.errorTexture = Texture(anx(Resolution.x/2 - (width/2 + 40)), any(100), anx(width + 80), any(50), "HK_BUTTON.TGA");
        bucket.previewInfo.errorDraw.setPosition(anx(Resolution.x/2 - width/2), any(120));
        bucket.previewInfo.errorTexture.visible = true;
        bucket.previewInfo.errorDraw.visible = true;
    }

    create = function(craftId)
    {
        local craft = getCraft(craftId);
        if(craft == null) {
            setError("Wybierz craft.");
            return;
        }

        if(Hero.stamina < craft.staminaCost) {
            setError("Nie jeste� na tyle wypocz�ty.");
            return;
        }

        for(local i = 0; i < craft.skillRequired.len(); i++) {
            if(Hero.skills[craft.skillRequired[i]] < craft.skillValue[i]) {
                setError("Nie wystarczaj�cy poziom umiej�tno�ci.");
                return;
            }
        }

        foreach(instance, amount in craft.items) {
            if(hasItem(instance) < amount) {
                setError("Brak "+getItemName(instance) +" w ilo�ci "+amount);
                return;
            }
        }

        if(CraftListInterface.active == true)
            return;

        local eggTimer = EggTimer(5000);
        eggTimer.attribute = craftId;
        if(craft.caller == CraftCaller.Tanning || craft.caller == CraftCaller.TanningFraction)
            eggTimer.externalAttribute = true;

        CraftListInterface.active = true;

        eggTimer.onEnd = function(eleobjectment) {
            if(eleobjectment.externalAttribute)
                useClosestMob(heroId, "BENCH", -1);

            CraftPacket().use(eleobjectment.attribute);
            CraftListInterface.active = false;
        }
    }

    preview = function(craftId)
    {
        bucket.createButton.attribute = craftId;
        Camera.setBesidePlayer(heroId, 160);

        local craft = getCraft(craftId);
        if(craft == null)
            return;

        bucket.previewInfo.name = Draw(anx(Resolution.x/2 + 70), any(Resolution.y/2 + 220), craft.getLabel());
        if(getSetting("OtherCustomFont")) {
            bucket.previewInfo.name.font = "FONT_OLD_20_WHITE_HI.TGA";
            bucket.previewInfo.name.setScale(0.45, 0.45);
        }
        bucket.previewInfo.name.visible = true;

        bucket.previewInfo.items = Draw(anx(Resolution.x/2 - 200), any(Resolution.y/2 + 220), craft.getItemsLabel());
        if(getSetting("OtherCustomFont")) {
            bucket.previewInfo.items.font = "FONT_OLD_20_WHITE_HI.TGA";
            bucket.previewInfo.items.setScale(0.45, 0.45);
        }
        bucket.previewInfo.items.visible = true;

        bucket.previewInfo.render = ItemRender(anx(Resolution.x/2 - 200), any(Resolution.y/2 - 150), anx(400), any(250), craft.getVisual());
        if(craft.isVob())
            bucket.previewInfo.render.visual = craft.getVisual();

        bucket.previewInfo.render.lightingswell = true;
        bucket.previewInfo.render.visible = true;

        bucket.previewInfo.arrow = Texture(anx(Resolution.x/2 - 50), any(Resolution.y/2 + 220), anx(100), any(40), "HK_R.TGA");
        bucket.previewInfo.arrow.visible = true;

        bucket.previewInfo.items.setPosition(anx(Resolution.x/2 - 70 - bucket.previewInfo.items.widthPx), any(Resolution.y/2 + 220));

        bucket.previewInfo.info = Draw(anx(Resolution.x/2 - 140), any(Resolution.y/2 - 280), craft.getInfo());
        if(getSetting("OtherCustomFont")) {
            bucket.previewInfo.info.font = "FONT_OLD_20_WHITE_HI.TGA";
            bucket.previewInfo.info.setScale(0.55, 0.55);
        }
        bucket.previewInfo.info.visible = true;

        bucket.previewInfo.descriptionItem = Draw(anx(Resolution.x - 200), any(Resolution.y/2 - 220), craft.getDescription());
        if(getSetting("OtherCustomFont")) {
            bucket.previewInfo.descriptionItem.font = "FONT_OLD_20_WHITE_HI.TGA";
            bucket.previewInfo.descriptionItem.setScale(0.45, 0.45);
        }
        bucket.previewInfo.descriptionItem.setPosition(anx(Resolution.x - 20) - bucket.previewInfo.descriptionItem.width, any(Resolution.y/2 - 220))
        bucket.previewInfo.descriptionItem.visible = true;
    }

    onCreateCraft = function(craftId)
    {
        if(ActiveGui == PlayerGUI.CraftList) {
            list.push(craftId);
            list.sort(function (a,b)
            {
                a = getCraft(a);
                b = getCraft(b);
                if (a.getSkillValueTotal() > b.getSkillValueTotal()) return 1;
                if (a.getSkillValueTotal() < b.getSkillValueTotal()) return -1;
                return 0;
            });
            bucket.scrollbarList.setMaximum(list.len() + 1);
        }
    }

    onInteractLostMob = function()
    {
        if(ActiveGui == PlayerGUI.CraftList)
            hide();
    }

    onInteractMob = function(interactionId)
    {
        local tabCraftInteractions = Config["InteractionsCraft"].find(interactionId);
        if(tabCraftInteractions == null)
            return;

        list.clear();
        CraftController.reset();
        CraftListInterface.show();
    }

    refresh = function() {
        if(ActiveGui != PlayerGUI.CraftList)
            return;

        foreach(item in bucket.listContent)
            item.destroy();

        bucket.listContent.clear();
        local value = bucket.scrollbarList.getValue();

        foreach(v, k in CraftListInterface.list) {
            if(v < value)
                continue;

            if(v > (value + 9))
                break;

            local button = CraftListInterface.Item(bucket.listContent.len(), k);
            button.setVisible(true);
            bucket.listContent.push(button);
        }
    }

    show = function() {
        if(bucket == null)
            bucket = prepareWindow();

        bucket.createButton.attribute = -1;
        bucket.background.visible = true;
        BaseGUI.show();
        bucket.window.setVisible(true);
        ActiveGui = PlayerGUI.CraftList;
        BlackScreen(1000);
        setTimer(function() {
            CraftListInterface.refresh();
        }, 800, 1);
    }

    hide = function() {
        BaseGUI.hide();
        bucket.window.setVisible(false);
        bucket.background.visible = false;
        ActiveGui = null;
        active = false;

        foreach(item in bucket.listContent)
            item.destroy();

        bucket.listContent.clear();

        bucket.previewInfo = {
            arrow = null,
            render = null,
            name = null,
            info = null,
            items = null,
            descriptionItem = null,
            errorTexture = null,
            errorDraw = null,
        }

        if(getEggTimer() != null)
            resetEggTimer();

        useClosestMob(heroId, "BENCH", -1);
    }
}

class CraftListInterface.Item {
    craftId = -1
    canBeUsed = false

    button = -1

    constructor(_y, _craftId = -1) {
        craftId = _craftId

        local craftObj = getCraft(craftId);
        canBeUsed = craftObj.check();

        button = GUI.Button(anx(75), any((Resolution.y/2 - 350) + _y * 60), anx(290), any(45), "HK_BUTTON.TGA", craftObj.getName());
        if(canBeUsed)
            button.draw.setColor(34, 200, 16);
        else
            button.draw.setColor(200, 34, 16);

        button.attribute = craftId;
        button.bind(EventType.Click, function(element) {
            CraftListInterface.preview(element.attribute);
        });
    }

    function setVisible(value) {
        button.setVisible(value);
    }

    function setPosition(x,y) {
        button.setPosition(x,y);
    }

    function destroy() {
        button.destroy();
    }
}

addEventHandler("onInteractLostMob", CraftListInterface.onInteractLostMob.bindenv(CraftListInterface));
addEventHandler("onInteractMob", CraftListInterface.onInteractMob.bindenv(CraftListInterface));
addEventHandler("onForceCloseGUI", CraftListInterface.onInteractLostMob.bindenv(CraftListInterface));
addEventHandler("onCreateCraft", CraftListInterface.onCreateCraft.bindenv(CraftListInterface));
addEventHandler("GUI.onInputInsertLetter", CraftListInterface.onInputInsertLetter.bindenv(CraftListInterface));
addEventHandler("GUI.onInputRemoveLetter", CraftListInterface.onInputInsertLetter.bindenv(CraftListInterface));

/**
 * Class to used for Active Record operations
 *
 * @type {Class}
 */
class ORM.ActiveRecord
{	
	static tableName = UNKNOWN;
	static fields = UNKNOWN;
	static validations = UNKNOWN;

    /**
     * Array that keeps names of modified fields and fields instances
     * (changed since last save/load)
     *
     * @type {Array}, {Table}
     */
	__storage_modified = null;
	__storage = null;
	
    /**  
	 * Vaariable store is record we use is new
     *
     * @type {Boolean}
     */	
	isNewRecord = false;
	
   /**
     * Create fields in constructor
	*/
    constructor() {
        if (this.tableName == UNKNOWN || this.fields == UNKNOWN) 
            throw "Wrong active record configuration";
        
        this.__storage = {};
        this.__storage_modified = [];

		this.isNewRecord = true;

        // fill in default values
        foreach (field in this.fields) {
			this.__storage[field.name] <- field.getDefaultValue();
        }
    }	

	/**
	* Method remove record in sql 
	* 
	* @type {Bool}
	*/	
	function remove()
	{
		if(isNewRecord)
			throw "Can't delete new object.";

		if(getPrimaryFieldFromField() == null)
			throw "Can't delete without primary field.";

		local primaryFieldName = getPrimaryFieldFromField().name;
		return Query().deleteFrom(tableName).where(primaryFieldName + " = "+ this.__storage[primaryFieldName]).execute();
	}
	
	/**
	* Method saving record in sql 
	* 
	* @type {Bool}
	*/
    function save(_runValidation = true) {
		if(_runValidation)
			if(validate() == false)
				return;

		if(isNewRecord)
			return saveNewRecord();

		if(this.__storage_modified.len() == 0){
			return true;
		}

		if(getPrimaryFieldFromField() == null)
			throw "Can't save without primary field.";

		return saveRecord();	
    }	
	
	/**
	* Method saving record in sql 
	* 
	* @type {Bool}
	*/
	function saveNewRecord() {
		local values = [];
		local indexes = [];

		this.onBeforeSave();

		foreach(_field in this.fields)
		{
			local findInStorage = this.__storage_modified.find(_field.name);
			if(findInStorage == null){
				if(!_field.isFieldPrimary() && _field.defaultValue != ""){
					values.append(rewriteSingleParam(this.__storage[_field.name]));
					indexes.append(_field.name);
				}
			}else{
				values.append(rewriteSingleParam(this.__storage[_field.name]));
				indexes.append(_field.name);
			}
		}

		if(values.len() == 0)
			return false;

		local res = Query().insertInto(tableName, indexes, values).execute();

		this.onAfterSave();
		return res;
	}

	/**
	* Method saving record in sql 
	* 
	* @type {Bool}
	*/
	function saveRecord() {
		local values = [];
		local indexes = [];

		this.onBeforeUpdate();

		foreach(name in this.__storage_modified)
		{
			values.append(rewriteSingleParam(this.__storage[name]));
			indexes.append(name);
		}

		local primaryFieldName = getPrimaryFieldFromField().name;
		local res = Query().update(tableName, indexes, values).where(primaryFieldName + " = "+ this.__storage[primaryFieldName]).execute();

		if(res == true)
			this.__storage_modified.clear();

		this.onAfterUpdate();

		return res;
	}

	/**
	* Functions check every save'd rules for validation
	* 
	* @type {Boolean}
	*/
	function validate() {
		if(this.validations == UNKNOWN)
			return true;

		// Variable with last rule
		local resultVal = true;	

        foreach (_rule in this.validations) {
			local field = _rule[0];
			if(!(this.__storage_modified.find(field)))
				continue;
			
			local valueField = this.__storage[field];

			_rule.remove(0);
			// foreach on every rule in array
			foreach(_ruleChild in _rule){
				local ruleName = _ruleChild[0];
				_ruleChild.remove(0);
				if(ORM.Rules[ruleName](valueField, _ruleChild) == false) {
					resultVal = false;
					onErrorValidation(ruleName);
				}
			}
        }

		return resultVal;
	}
	
	/**
	* Method to get instances of field witch is primary
	* 
	* @type {Object Field}
	*/
	function getPrimaryFieldFromField()
	{
		foreach(_field in this.fields)
		{
			if(_field.isFieldPrimary())
				return _field;
		}
		return null;
	}
	
	/**
	* Method to get remove instances from query
	* 
	* @type {Array}
	*/	
	static function removeAll(conditions = []) {
		if(conditions == [])
			Query().deleteFrom(tableName).execute();

		Query().deleteFrom(tableName).where(rewriteConditions(conditions)).execute();
	}

	/**
	* Method to get instances of self by query find
	* 
	* @type {Array}
	*/
	static function find(conditions = []) {
		local allRecords = Query().select().from(tableName).where(rewriteConditions(conditions)).all();
		
		if(!allRecords) return null;
		
		// create array contain all records
		local returnTab = [];
		
		// foreach on results
		foreach(oneRecord in allRecords) {
			local newInstance = this();

			newInstance.isNewRecord = false;

			foreach(record, value in oneRecord)
			{
				if (record in newInstance.__storage) 
					newInstance.set(record, value);
			}
			newInstance.__storage_modified.clear();
			returnTab.append(newInstance);
		}
		return returnTab;
	}

	/**
	* Method to get instance of self
	* 
	* @type {Object}
	*/
	static function findOneBy(conditions) {
		local oneRecord = Query().select().from(tableName).where(rewriteConditions(conditions)).one();
		
		if(!oneRecord) return null;
		
		local newInstance = this();
		newInstance.isNewRecord = false;		
		// foreach on result		
		foreach(record, value in oneRecord)
		{
			if (record in newInstance.__storage) 
				newInstance.set(record, value);
		}
		return newInstance;
	}

	/**
	* Methamethods construction over every class
	*/
    function _set(name, value) {
        if (!name in this.__storage) 
            throw null;
        
        if (this.__storage[name] != value) {
            this.__storage[name] = value;
            this.__storage_modified.push(name);
        }
    }

    function _get(name) {
        return this.get(name);
    }

	/**
	* Helpers for metamethods
	**/
    function set(name, value) {
        this[name] = value;
    }

    function get(name) {
        if (name in this.__storage) 
            return this.__storage[name];
        
        throw null;
    }

	/**
	* Events for meta datas
	*
    * @return {func}
	**/

	function onBeforeSave() {}
	function onAfterSave() {}
	function onBeforeUpdate() {}
	function onAfterUpdate() {}
	function onErrorValidation(ruleName) {}

	/**
	* Helpers to write conditions in good form
	**/
	function rewriteConditions(conditions) {
		local req = [];

		foreach(con in conditions)
		{
			if(typeof con[1] == "string")
				req.append(con[0]+" = '"+con[1]+"'");
			else 
				req.append(con[0]+" = "+con[1]);
		}
		return req;
	}

	function rewriteSingleParam(param)
	{
		switch( typeof param )
		{
			case "string":
				return "'"+param+"'"
			break;
		}
		return param;
	}
}



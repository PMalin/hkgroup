/**
 * Class to used for Active Record field operations
 *
 * @type {Class}
 */

class ORM.Field {
    /**
    * Contains size of field when we create row
    *
    * @type {int}
    */
    static size = UNKNOWN;

    /**
    * Contains name of field in table
    *
    * @type {String}
    */
    name = UNKNOWN;

    /**
    * Contains type of field
    *
    * @type {int}
    */    
    static type = UNKNOWN;

    /**
    * Contains the default value of field
    *
    * @type {string}
    */  
    defaultValue = "";

    /**
    * Contains is field primary
    *
    * @type {boolean}
    */     
    static fieldPrimary = false;

    constructor(data){
        if("name" in data)
            name = data.name;
        if("defaultValue" in data)
            defaultValue = data.defaultValue;
        
        if(name == UNKNOWN)
            throw "Bad field configuration. ORM > Field";
    }

    function getName() {
        return name;
    }

    function getDefaultValue() {
        return defaultValue;
    }

    function isFieldPrimary() {
        return fieldPrimary;
    }

    function getType() {
        return type;
    }
}
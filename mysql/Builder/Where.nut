class WhereQuery {
    static function combine(command) {
        if(typeof command == "array")
            return combineArray(command);

        local query = " WHERE "+command;
        return query;
    }

    static function combineArray(command)
    {
        local query = " WHERE ";
        foreach(arr in command)
            query += arr+" AND ";

        query = query.slice(0, -4);
        return query; 
    }
}
class UpdateQuery {
    static function combine(tab, tabOne, tabTwo) {
        local query = "UPDATE "+tab +" SET ";
        foreach(id, val in tabOne)
            query += val+" = "+tabTwo[id]+",";      

        query = query.slice(0, -1);
        return query;
    }
}
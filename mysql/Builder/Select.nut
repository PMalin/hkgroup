class SelectQuery {
    static function combine(command) {
        if(typeof command == "array")
            return combineArray(command);

        local query = "SELECT "+command;
        return query;
    }

    static function combineArray(command)
    {
        local query = "SELECT ";
        foreach(arr in command)
            query += arr+",";

        query = query.slice(0, -1);
        return query; 
    }
}
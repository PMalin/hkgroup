class CreateTableQuery {
    static function combine(tableName, fields) {
        local query = "CREATE TABLE `"+tableName +"` (";
        foreach(val in fields)
            query += val[0]+",";      

        query = query.slice(0, -1);
        query += ")"
        return query;
    }
}
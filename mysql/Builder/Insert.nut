class InsertQuery {
    static function combine(tab, tabOne, tabTwo) {
        local query = "INSERT INTO "+tab +" (";
        foreach(val in tabOne)
            query += val+",";

        query = query.slice(0, -1);

        if(type(tabTwo[0]) == "array")
        {
            query += ") VALUES ";
            foreach(_tab in tabTwo)
            {
                query += "(";
                foreach(val in _tab)
                    query += val+",";

                query = query.slice(0, -1);
                query += "),";
            }
            query = query.slice(0, -1);
            query += ";";
        }else{
            query += ") VALUES (";
            foreach(val in tabTwo)
                query += val+",";

            query = query.slice(0, -1);
            query += ");";
        }
        return query;
    }
}

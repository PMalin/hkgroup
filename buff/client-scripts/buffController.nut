
class BuffController
{
    static buffs = {}

    static function getExtraWeight() {
        local extraMaxWeight = 0;
        foreach(buff in buffs)
            if(buff.getType() == BuffType.WeightLimit)
                extraMaxWeight += buff.getScheme().value;

        return extraMaxWeight;
    }

    static function onPacket(packet) {
        local typ = packet.readUInt8();

        switch(typ)
        {
            case BuffPackets.Add:
                local id = packet.readInt32();
                local scheme = packet.readString();
                local counter = packet.readInt16();
                local interval = packet.readInt16();

                buffs[id] <- Buff(id, scheme, counter, interval);
                buffs[id].enable();
                BuffInterface.onAdd(buffs[id]);
            break;

            case BuffPackets.Remove:
                local id = packet.readInt32();
                buffs[id].disable();
                BuffInterface.onRemove(buffs[id]);
                buffs.rawdelete(id);
            break;
        }
    }
}

RegisterPacketReceiver(Packets.Buff, BuffController.onPacket.bindenv(BuffController));

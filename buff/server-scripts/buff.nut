
class Buff
{
    static BuffTable = "buff";

    id = -1
    playerId = -1

    scheme = ""
    counter = -1
    interval = -1

    constructor(id, playerId, scheme, counter, interval) {
        this.id = id
        this.playerId = playerId
        this.scheme = scheme
        this.counter = counter
        this.interval = interval
    }

    function packetAdd() {
        BuffPacket(this).add();
    }

    function packetRemove() {
        BuffPacket(this).remove();
    }

    function onSecond() {
        local _sxm = getScheme();

        counter = counter - 1;

        if(!("interval" in _sxm))
            return;

        interval = interval + 1;

        if(interval >= _sxm.interval) {
            interval = 0;

            switch(_sxm.type) {
                case BuffType.HealthRegeneration:
                    local val = getPlayerHealth(playerId) + _sxm.value;
                    if(val >= getPlayerMaxHealth(playerId))
                        val = getPlayerMaxHealth(playerId);

                    setPlayerHealth(playerId, val.tointeger());
                break;
                case BuffType.HealthReduce:
                    local val = getPlayerHealth(playerId) - _sxm.value;
                    if(val <= 2)
                        val = 2;

                    setPlayerHealth(playerId, val.tointeger());
                break;
                case BuffType.ManaReduce:
                    local val = getPlayerMana(playerId) - _sxm.value;
                    if(val <= 2)
                        val = 2;

                    setPlayerMana(playerId, val.tointeger());
                break;
                case BuffType.ManaRegeneration:
                    local val = getPlayerMana(playerId) + _sxm.value;
                    if(val >= getPlayerMaxMana(playerId))
                        val = getPlayerMaxMana(playerId);

                    setPlayerMana(playerId, val.tointeger());
                break;
                case BuffType.EnduranceRegeneration:
                    local val = getPlayerEndurance(playerId) + _sxm.value;
                    if(val <= 1)
                        val = 1;

                    if(val > 100)
                        val = 100;

                    setPlayerEndurance(playerId, val.tointeger());
                break;
                case BuffType.StaminaRegeneration:
                    local hunger = getPlayerHunger(playerId);
                    if(hunger > 50){
                        local val = getPlayerStamina(playerId) + _sxm.value;
                        if(val <= 1)
                            val = 1;

                        if(val > 100)
                            val = 100;

                        val = val.tofloat();
                        setPlayerStamina(playerId, val);
                    }

                break;
            }
        }
    }

    function enable() {
        local _sxm = getScheme();

        switch(_sxm.type) {
            case BuffType.Custom:
                _sxm.enableFunc(this);
            break;
            case BuffType.Attribute:
                setPlayerAttributeValue(playerId, _sxm.attributeId, getPlayerAttributeValue(playerId, _sxm.attributeId) + _sxm.value, true);

                if(_sxm.attributeId == PlayerAttributes.Hp)
                    setPlayerHealth(playerId, getPlayerHealth(playerId) + _sxm.value);
                else if(_sxm.attributeId == PlayerAttributes.Mana)
                    setPlayerMana(playerId, getPlayerMana(playerId) + _sxm.value);
            break;
            case BuffType.Skill:
                setPlayerSkill(playerId, _sxm.skillId, getPlayerSkill(playerId, _sxm.skillId) + _sxm.value);
            break;
            case BuffType.DamageBonus:
                setPlayerDamageBonus(playerId, _sxm.bonusType, getPlayerDamageBonus(playerId, _sxm.bonusType) + _sxm.value);
            break;
            case BuffType.DamageReduce:
                setPlayerDamageBonus(playerId, _sxm.bonusType, getPlayerDamageBonus(playerId, _sxm.bonusType) - _sxm.value);
            break;
            case BuffType.ProtectionBonus:
                setPlayerProtectionBonus(playerId, _sxm.bonusType, getPlayerProtectionBonus(playerId, _sxm.bonusType) + _sxm.value);
            break;
            case BuffType.ProtectionReduce:
                setPlayerProtectionBonus(playerId, _sxm.bonusType, getPlayerProtectionBonus(playerId, _sxm.bonusType) - _sxm.value);
            break;
            case BuffType.Mds:
                applyPlayerOverlay(playerId, Mds.id(_sxm.mds));
            break;
        }
    }

    function disable() {
        local _sxm = getScheme();

        switch(_sxm.type) {
            case BuffType.Custom:
                _sxm.disableFunc(this);
            break;
            case BuffType.Attribute:
                setPlayerAttributeValue(playerId, _sxm.attributeId, getPlayerAttributeValue(playerId, _sxm.attributeId) - _sxm.value, true);

                if(_sxm.attributeId == PlayerAttributes.Hp) {
                    local val = getPlayerHealth(playerId) - _sxm.value;
                    if(val <= 2)
                        val = 2;

                    setPlayerHealth(playerId, val);
                }else if(_sxm.attributeId == PlayerAttributes.Mana) {
                    local val = getPlayerMana(playerId) - _sxm.value;
                    if(val <= 2)
                        val = 2;

                    setPlayerMana(playerId, val);
                }
            break;
            case BuffType.Skill:
                setPlayerSkill(playerId, _sxm.skillId, getPlayerSkill(playerId, _sxm.skillId) - _sxm.value);
            break;
            case BuffType.DamageBonus:
                setPlayerDamageBonus(playerId, _sxm.bonusType, getPlayerDamageBonus(playerId, _sxm.bonusType) - _sxm.value);
            break;
            case BuffType.DamageReduce:
                setPlayerDamageBonus(playerId, _sxm.bonusType, getPlayerDamageBonus(playerId, _sxm.bonusType) - _sxm.value);
            break;
            case BuffType.ProtectionBonus:
                setPlayerProtectionBonus(playerId, _sxm.bonusType, getPlayerProtectionBonus(playerId, _sxm.bonusType) + _sxm.value);
            break;
            case BuffType.ProtectionReduce:
                setPlayerProtectionBonus(playerId, _sxm.bonusType, getPlayerProtectionBonus(playerId, _sxm.bonusType) - _sxm.value);
            break;
            case BuffType.Mds:
                removePlayerOverlay(playerId, Mds.id(_sxm.mds));
            break;
        }
    }

    function getType() {
        return getScheme().type;
    }

    function getIcon() {
        return getScheme().icon;
    }

    function getScheme() {
        return Config["Buffs"][scheme];
    }
}
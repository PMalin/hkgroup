
class AccountPacket
{
    id = -1;
    packet = null;

    constructor(playerId)
    {
        id = playerId;
        packet = Packet();
    }

    function logoutAction() {
        reset();

        packet.writeUInt8(AccountPackets.Logout);
        packet.send(id, RELIABLE);
    }

    function reset() {
        packet.reset();
        packet.writeUInt8(PacketAccount);
        packet.writeUInt8(Packets.Account);
    }

    function loggIn() {
        reset();

        packet.writeUInt8(AccountPackets.LoggIn)
        packet.send(id, RELIABLE);
    }

    function accept(realId, showReroll) {
        reset();

        packet.writeUInt8(AccountPackets.Accept)
        packet.writeInt16(realId);
        packet.writeBool(showReroll);
        packet.send(id, RELIABLE);
    }

    function register(message) {
        reset();

        packet.writeUInt8(AccountPackets.Register)
        packet.writeString(message);
        packet.send(id, RELIABLE);
    }

    function sendSkins(str) {
        reset();

        packet.writeUInt8(AccountPackets.Skins)
        packet.writeString(str);
        packet.send(id, RELIABLE);
    }

    function code() {
        reset();

        packet.writeUInt8(AccountPackets.CodeConfirmation);
        packet.send(id, RELIABLE);
    }

    function wrongReroll() {
        reset();

        packet.writeUInt8(AccountPackets.PasswordReroll);
        packet.send(id, RELIABLE);
    }

    function needAcceptation() {
        reset();

        packet.writeUInt8(AccountPackets.AcceptPolicy);
        packet.send(id, RELIABLE);
    }

    function whiteList() {
        reset();

        packet.writeUInt8(AccountPackets.WhiteList);
        packet.send(id, RELIABLE);
    }

    function settingWrite(tab) {
        reset();

        packet.writeUInt8(AccountPackets.Setting);
        packet.writeInt16(tab.len())
        foreach(name, value in tab) {
            packet.writeString(name);
            packet.writeString(value);
        }
        packet.send(id, RELIABLE);
    }

    function settingKeyWrite(tab) {
        reset();

        packet.writeUInt8(AccountPackets.SettingKey);
        packet.writeInt16(tab.len())
        foreach(name, value in tab) {
            packet.writeString(name);
            packet.writeInt16(value);
        }
        packet.send(id, RELIABLE);
    }
}
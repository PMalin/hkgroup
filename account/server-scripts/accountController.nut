class AccountController
{
    static accounts = [];

    static function onInit() {
        for(local i = 0; i < getMaxSlots(); i ++)
            accounts.push(Account(i));

        DB.queryGet("UPDATE "+Account.dbTable+" SET online = 0;");
    }

    static function onJoin(playerId) {
        MuteList.checkMuteList(playerId);

        setPlayerName(playerId, "Nieznajomy "+playerId);
    }

    static function onPlayerDisconnect(playerId, reason) {
        local dateObject = date();
        accounts[playerId].updatedAt = dateObject.year+"-"+dateObject.month+"-"+dateObject.day+" "+dateObject.hour+":"+dateObject.min;
        accounts[playerId].clear();
    }

    static function load(playerId, username, password) {
        local accountObject = accounts[playerId];

        if(accountObject.rId != -1)
            return;

        accountObject.username = username;
        accountObject.password = password;
        accountObject.load();

        if(accountObject.isConfirmed)
            accountObject.packetManager.accept(accountObject.rId, accountObject.isNeedPasswordReroll);
        else
            accountObject.packetManager.code();

        local settingsToSend = {};
        local myfile = io.file("database/settings/"+accountObject.rId, "r");
        if (myfile.isOpen)
        {
            local result = null;
            do
            {
                result = myfile.read(io_type.LINE);
                if(result != null) {
                    result = split(result, ":");
                    settingsToSend[result[0]] <- result[1];
                }
            }while(result != null)

            myfile.close();
        }

        foreach(settingName, settingValue in Config["Settings"])
        {
            if(!(settingName in settingsToSend))
                settingsToSend[settingName] <- Config["Settings"][settingName].defined;
        }

        accountObject.packetManager.settingWrite(settingsToSend);

        local settingsKeyToSend = {};
        myfile = io.file("database/keys/"+accountObject.rId, "r");
        if (myfile.isOpen)
        {
            local result = null;
            do
            {
                result = myfile.read(io_type.LINE);
                if(result != null) {
                    result = split(result, ":");
                    if(result.len() > 1)
                        settingsKeyToSend[result[0]] <- result[1].tointeger();
                }
            }while(result != null)

            myfile.close();
        }

        foreach(settingName, settingValue in Config["DynamicBindings"])
        {
            if(!(settingName in settingsKeyToSend))
                settingsKeyToSend[settingName] <- Config["DynamicBindings"][settingName].defined;
        }

        accountObject.packetManager.settingKeyWrite(settingsKeyToSend);

        callEvent("onAccountLoad", playerId);
    }

    static function register(playerId, username, password, email) {
        local accountObject = accounts[playerId];

        if(accountObject.rId != -1)
            return;

        username = mysql_escape_string(username);
        if(username == null)
            username = "";

        local checkUserExist = Query().select().from(Account.dbTable).where(["username = '"+username+"'"]).one();
        if(checkUserExist != null) {
            accountObject.packetManager.register("U�ytkownik istnieje w bazie danych.");
            return;
        }

        email = mysql_escape_string(email);
        if(email == null)
            email = "";

        local checkEmailExist = Query().select().from(Account.dbTable).where(["email = '"+email+"'"]).one();
        if(checkEmailExist != null) {
            accountObject.packetManager.register("E-Mail istnieje w bazie danych.");
            return;
        }

        accountObject.username = username;
        accountObject.password = password;
        accountObject.email = email;
        accountObject.confirmationCode = String.makeid(6);
        accountObject.isConfirmed = false;
        accountObject.isNeedPasswordReroll = false;
        accountObject.isNeedAcceptPolicy = true;
        accountObject.register();
        accountObject.sendConfirmationCodeRequestForMail();

        load(playerId, username, password);
    }

    static function passwordRequest(playerId, email) {
        local accountObject = accounts[playerId];

        if(accountObject.rId != -1)
            return;

        if(email.len() < 5)
            return;

        email = mysql_escape_string(email);
        if(email == null)
            return;

        local code = String.makeid(6);
        DB.query("UPDATE "+Account.dbTable+" SET resetCode='"+code+"' WHERE email='"+email+"';");
        system("wget -q \"https://historiakolonii.pl/mail.php?address="+email+"&subject="+code+"&type=2\" -O /dev/null");
        system("rm api");
    }

    static function passwordReroll(playerId, password) {
        local accountObject = accounts[playerId];

        if(accountObject.rId == -1)
            return;

        if(accountObject.isNeedPasswordReroll == false)
            return;

        password = mysql_escape_string(password);
        password = Account.prepareHash(password);

        if(password == accountObject.password)
            accountObject.packetManager.wrongReroll();
        else {
            accountObject.password = password;
            accountObject.isNeedPasswordReroll = false;
            kick(playerId, "Has�o zosta�o zmienione. Zaloguj si� ponownie.");
        }
    }

    static function loggIn(playerId, username, password) {
        if(username.len() > 2) {
            foreach(acc in accounts) {
                if(acc.username == username) {
                    kick(playerId, "Osoba o tym loginie jest ju� na serwerze!");
                    return;
                }
            }
        }

        local player = Account.findByUsername(username);
        if(player) {
            if(player.password != password)
                accounts[playerId].packetManager.loggIn();
            else
                AccountController.load(playerId, username, password);
        }else
            accounts[playerId].packetManager.loggIn();
    }

    static function confirmCode(playerId, code) {
        local accountObject = accounts[playerId];
        if(accountObject.rId == -1)
            return;

        if(code != accountObject.confirmationCode) {
            accountObject.packetManager.code();
            return;
        }

        accountObject.isConfirmed = true;
        accountObject.packetManager.accept(accountObject.rId, accountObject.isNeedPasswordReroll);
    }

    static function passwordConfirm(playerId, code, password) {
        local accountObject = accounts[playerId];
        if(accountObject.rId != -1)
            return;

        if(code.len() < 4 || code.len() > 8)
            return;

        password = mysql_escape_string(password);
        code = mysql_escape_string(code);

        DB.query("UPDATE "+Account.dbTable+" SET password='"+password+"' WHERE resetCode='"+code+"' LIMIT 1;");
    }

    static function whiteList(playerId, code) {
        local codeObject = Account.findWhiteList(code);
        if(codeObject == null)  {
            kick(playerId, "Z�y kod lub nie aktywny.")
            return;
        }

        if(codeObject.used == 0) {
            DB.query("UPDATE "+Account.dbWhiteListTable+" SET used=1, mac='"+getPlayerSerial(playerId)+"' WHERE code='"+code+"';");
        }else if(codeObject.used == 1 && getPlayerSerial(playerId) != codeObject.mac)  {
            kick(playerId, "Kod zosta� ju� u�yty.")
            return;
        }

        accounts[playerId].packetManager.whiteList();
    }

    static function logout(playerId) {
        if(getPlayer(playerId).loggIn == false)
            return;

        BuffController.resetBuffsForPlayer(playerId);

        getPlayer(playerId).save();
        getPlayer(playerId).reset();
        getPlayer(playerId).clear();

        getAccount(playerId).clear();
        AccountPacket(playerId).logoutAction();
    }

    static function onPacket(playerId, packet)
    {
        local identfier = packet.readUInt8();
        if(identfier != PacketAccount)
            return;

        local typeId = packet.readUInt8();
        if(typeId != Packets.Account)
            return;

        local typ = packet.readUInt8();

        switch(typ)
        {
            case AccountPackets.LoggIn:
                local username = packet.readString();
                local password = packet.readString();

                password = Account.prepareHash(password);

                AccountController.loggIn(playerId, username, password);
            break;
            case AccountPackets.Register:
                local username = packet.readString();
                local email = packet.readString();
                local password = packet.readString();

                password = Account.prepareHash(password);

                AccountController.register(playerId, username, password, email);
            break;
            case AccountPackets.CodeConfirmation:
                local code = packet.readString();

                AccountController.confirmCode(playerId, code);
            break;
            case AccountPackets.PasswordConfirm:
                local code = packet.readString();
                local password = packet.readString();

                password = Account.prepareHash(password);

                AccountController.passwordConfirm(playerId, code, password);
            break;
            case AccountPackets.PasswordRequest:
                local email = packet.readString();

                AccountController.passwordRequest(playerId, email);
            break;
            case AccountPackets.PasswordReroll:
                local password = packet.readString();
                AccountController.passwordReroll(playerId, password);
            break;
            case AccountPackets.WhiteList:
                local code = packet.readString();

                AccountController.whiteList(playerId, code);
            break;
            case AccountPackets.Logout:
                AccountController.logout(playerId);
            break;
            case AccountPackets.Kick:
                kick(playerId, packet.readString())
            break;
            case AccountPackets.Setting:
                local settingFile = file("database/settings/"+getAccount(playerId).rId, "w");
                local settingLen = packet.readInt16();

                for(local i = 0; i < settingLen; i++) {
                    local settingName = packet.readString();
                    local settingValue = packet.readString();

                    settingFile.write(settingName + ":"+settingValue+"\n");
                }

                settingFile.close();
            break;
            case AccountPackets.SettingKey:
                local settingFile = file("database/keys/"+getAccount(playerId).rId, "w");
                local settingLen = packet.readInt16();

                for(local i = 0; i < settingLen; i++) {
                    local settingName = packet.readString();
                    local settingValue = packet.readString();

                    settingFile.write(settingName + ":"+settingValue+"\n");
                }

                settingFile.close();
            break;
        }
    }
}

getAccount <- @(id) AccountController.accounts[id];
getAccounts <- @() AccountController.accounts;

addEventHandler("onInit", AccountController.onInit.bindenv(AccountController));
addEventHandler("onPacket", AccountController.onPacket.bindenv(AccountController));
addEventHandler("onPlayerJoin", AccountController.onJoin.bindenv(AccountController));
addEventHandler("onPlayerDisconnect", AccountController.onPlayerDisconnect.bindenv(AccountController));

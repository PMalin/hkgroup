
local window = GUI.Window(anx(80), any(Resolution.y/2 - 250), anx(424), any(562), "HK_BOX.TGA", null, false);
local logo = GUI.Button(anx(-20), any(-176), anx(424), any(212), "HK_LOGO.TGA", "", window);

local loggInButton = GUI.Button(anx(42), any(290), anx(340), any(50), "HK_BUTTON.TGA", "Zaloguj", window);
local exitGameButton = GUI.Button(anx(42), any(360), anx(340), any(50), "HK_BUTTON.TGA", "Wyjd�", window);
local registerButton = GUI.Button(anx(42), any(450), anx(170), any(50), "HK_BUTTON.TGA", "Rejestracja", window);
local resetPasswordButton = GUI.Button(anx(212), any(450), anx(170), any(50), "HK_BUTTON.TGA", "Reset has�a", window);

local _draw = GUI.Draw(anx(42), any(70), "Logowanie si� na serwerze jest r�wnoznaczne z\nakceptacj� polityki prywatno�ci: https://historiakolonii.pl/regulamin\nKontakt w sprawi� polityki prywatno�ci i danych w sekcji Kontakt.", window);
_draw.setPosition(anx(42), any(Resolution.y - 100));

local path = CameraPath()

path.createPoint({position = [1393.42, 854.815, 5132.67], rotation = [19.6167, 242.59, 0], lerpSpeed = 100})
path.createPoint({position = [-924.468, -623.636, 3745.59], rotation = [6.37207, 244.36, 0], lerpSpeed = 100})
path.createPoint({position = [-3142.54, -500.077, 2644.24], rotation = [-7.60498, 242.749, 0], lerpSpeed = 300})
path.createPoint({position = [-4172.86, -585.348, 2094.98], rotation = [10.6323, 215.344, 0], lerpSpeed = 300})
path.createPoint({position = [-4420.78, -596.184, 1691.16], rotation = [-1.0376, 287.378, 0], lerpSpeed = 300})
path.createPoint({position = [-5711.06, -722.778, 1955.3], rotation = [10.9985, 231.238, 0], lerpSpeed = 300})
path.createPoint({position = [-6617.2, -779.968, -361.991], rotation = [-3.25928, 181.812, 0], lerpSpeed = 300})
path.createPoint({position = [-6881.9, -657.49, -1781.56], rotation = [-13.4399, 226.55, 0], lerpSpeed = 300})
path.createPoint({position = [-7201.82, -576.546, -2683.53], rotation = [1.37939, 178.735, 0], lerpSpeed = 300})
path.createPoint({position = [-6941.97, 449.276, -7468.32], rotation = [11.2915, 39.8315, 0], lerpSpeed = 300})
path.createPoint({position = [-3811.24, 402.529, -4045.38], rotation = [-0.280762, -11.6699, 0], lerpSpeed = 300})
path.createPoint({position = [-3911.62, 385.416, -3038.25], rotation = [6.75049, -4.01611, 0], lerpSpeed = 300})
path.createPoint({position = [-3644.79, -68.4743, 54.9016], rotation = [-5.10254, 79.7852, 0], lerpSpeed = 300})
path.createPoint({position = [-2955.21, -250.049, 1441.35], rotation = [11.8408, -18.0786, 0], lerpSpeed = 300})
path.createPoint({position = [-5396.12, -562.526, 3403.69], rotation = [8.69141, -72.3389, 0], lerpSpeed = 300})
path.createPoint({position = [-6611.13, -614.761, 3852.48], rotation = [18.689, -28.125, 0], lerpSpeed = 200})
path.createPoint({position = [-7312.22, -990.858, 5348.81], rotation = [9.61914, -29.0039, 0], lerpSpeed = 200})
path.createPoint({position = [-8851, -507.976, 8529.45], rotation = [-15.5762, -30.4199, 0], lerpSpeed = 200})
path.createPoint({position = [-8608.2, -481.657, 10000.3], rotation = [-6.97021, 27.9175, 0], lerpSpeed = 200})
path.createPoint({position = [-8015.89, -485.941, 11400.9], rotation = [22.8516, 23.0225, 0], lerpSpeed = 200})

loggInButton.bind(EventType.Click, function(element) {
    sendLoggIn();
});

exitGameButton.bind(EventType.Click, function(element) {
    exitGame();
});

registerButton.bind(EventType.Click, function(element) {
    hideOutTemporaryLoggInMenu();
    showRegisterMenu();
});

resetPasswordButton.bind(EventType.Click, function(element) {
    hideOutTemporaryLoggInMenu();
    showPasswordRequestMenu();
});

local remember = GUI.CheckBox(anx(320), any(220), anx(35), any(40), "HK_CHECKBOX.TGA", "HK_CHECKBOX_CHECKED.TGA", window);
local password = GUI.Input(anx(42), any(140), anx(340), any(45), "HK_INPUT.TGA", "FONT_OLD_10_WHITE_HI.TGA", Input.Password, Align.Center, "Has�o", 2, window);
local username = GUI.Input(anx(42), any(70), anx(340), any(45), "HK_INPUT.TGA", "FONT_OLD_10_WHITE_HI.TGA", Input.Text, Align.Center, "Nazwa u�ytkownika", 2, window);
local draws = [
    GUI.Draw(anx(50), any(230), "Zapami�taj has�o:", window),
    GUI.Draw(anx(50), any(640), "", window),
];

foreach(draw in draws)
{
    draw.setScale(0.5,0.5);
    draw.setFont("FONT_OLD_20_WHITE_HI.TGA")
}

function sendLoggIn()
{
    if(Loading.active)
        false;

    local uiResult = BaseGUI.normalize({password=password,username=username,remember=remember});

    if(uiResult.password.len() > 3 && uiResult.username.len() > 3) {
        Loading.show();
        Account.Packet.loggIn(uiResult);
        if(uiResult.remember) {
            if("LocalStorage" in getroottable()) {
                LocalStorage.setItem("password", uiResult.password);
                LocalStorage.setItem("username", uiResult.username);
            }
        }
    }else
        showLoggInError("Has�o i nazwa min. 4 znaki.")
}

function showLoggInError(value)
{
    local positionDraw = draws[1].getPosition();
    draws[1].setText(value);
    draws[1].setPosition(anx(80), positionDraw.y);
}

function showOutTemporaryLoggInMenu() {
    window.setVisible(true);
    ActiveGui = PlayerGUI.LoggIn;
}

function showLoggInMenu()
{
    if(path == null) {
        path = CameraPath()

        path.createPoint({position = [1393.42, 854.815, 5132.67], rotation = [19.6167, 242.59, 0], lerpSpeed = 100})
        path.createPoint({position = [-924.468, -623.636, 3745.59], rotation = [6.37207, 244.36, 0], lerpSpeed = 100})
        path.createPoint({position = [-3142.54, -500.077, 2644.24], rotation = [-7.60498, 242.749, 0], lerpSpeed = 300})
        path.createPoint({position = [-4172.86, -585.348, 2094.98], rotation = [10.6323, 215.344, 0], lerpSpeed = 300})
        path.createPoint({position = [-4420.78, -596.184, 1691.16], rotation = [-1.0376, 287.378, 0], lerpSpeed = 300})
        path.createPoint({position = [-5711.06, -722.778, 1955.3], rotation = [10.9985, 231.238, 0], lerpSpeed = 300})
        path.createPoint({position = [-6617.2, -779.968, -361.991], rotation = [-3.25928, 181.812, 0], lerpSpeed = 300})
        path.createPoint({position = [-6881.9, -657.49, -1781.56], rotation = [-13.4399, 226.55, 0], lerpSpeed = 300})
        path.createPoint({position = [-7201.82, -576.546, -2683.53], rotation = [1.37939, 178.735, 0], lerpSpeed = 300})
        path.createPoint({position = [-6941.97, 449.276, -7468.32], rotation = [11.2915, 39.8315, 0], lerpSpeed = 300})
        path.createPoint({position = [-3811.24, 402.529, -4045.38], rotation = [-0.280762, -11.6699, 0], lerpSpeed = 300})
        path.createPoint({position = [-3911.62, 385.416, -3038.25], rotation = [6.75049, -4.01611, 0], lerpSpeed = 300})
        path.createPoint({position = [-3644.79, -68.4743, 54.9016], rotation = [-5.10254, 79.7852, 0], lerpSpeed = 300})
        path.createPoint({position = [-2955.21, -250.049, 1441.35], rotation = [11.8408, -18.0786, 0], lerpSpeed = 300})
        path.createPoint({position = [-5396.12, -562.526, 3403.69], rotation = [8.69141, -72.3389, 0], lerpSpeed = 300})
        path.createPoint({position = [-6611.13, -614.761, 3852.48], rotation = [18.689, -28.125, 0], lerpSpeed = 200})
        path.createPoint({position = [-7312.22, -990.858, 5348.81], rotation = [9.61914, -29.0039, 0], lerpSpeed = 200})
        path.createPoint({position = [-8851, -507.976, 8529.45], rotation = [-15.5762, -30.4199, 0], lerpSpeed = 200})
        path.createPoint({position = [-8608.2, -481.657, 10000.3], rotation = [-6.97021, 27.9175, 0], lerpSpeed = 200})
        path.createPoint({position = [-8015.89, -485.941, 11400.9], rotation = [22.8516, 23.0225, 0], lerpSpeed = 200})
    }

    path.start();
    clearMultiplayerMessages();
    enable_DamageAnims(false);
    enable_NicknameId(true);

    if("LocalStorage" in getroottable()) {
        local pass = LocalStorage.getItem("password")
        if(pass != null) {
            password.setText(pass);
            remember.setChecked(true);
        }

        local user = LocalStorage.getItem("username")
        if(user != null)
            username.setText(user);
    }

    BaseGUI.show();
    ActiveGui = PlayerGUI.LoggIn;

    window.setVisible(true);
}

function hideOutTemporaryLoggInMenu() {
    window.setVisible(false);
}

local function onStopCamera(obj)
{
    if(path == null)
        return;

    if(ActiveGui != PlayerGUI.LoggIn)
        return;

    path.start();
}

local function hideLoggIn()
{
    BaseGUI.hide();
    ActiveGui = null;
    path.stop();

    window.setVisible(false);
}

function hideCameraPath() {
    path = null;
    deleteActiveCameraPath();
}

addEventHandler("CameraPath.onStop", onStopCamera)
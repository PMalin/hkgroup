
local window = GUI.Window(anx(80), any(Resolution.y/2 - 250), anx(424), any(562), "HK_BOX.TGA", null, false);
local logo = GUI.Button(anx(-20), any(-176), anx(424), any(212), "HK_LOGO.TGA", "", window);

local resetPasswordButton = GUI.Button(anx(42), any(210), anx(340), any(50), "HK_BUTTON.TGA", "Potwierd�", window);

GUI.Draw(anx(42), any(70), "Na ten adres e-mail trafi\nkod do zmiany has�a.", window);
local email = GUI.Input(anx(42), any(140), anx(340), any(45), "HK_INPUT.TGA", "FONT_OLD_10_WHITE_HI.TGA", Input.Text, Align.Center, "E-mail", 2, window);

local draws = [
    GUI.Draw(anx(50), any(640), "", window),
];

foreach(draw in draws)
{
    draw.setScale(0.5,0.5);
    draw.setFont("FONT_OLD_20_WHITE_HI.TGA")
}

resetPasswordButton.bind(EventType.Click, function(element) {
    sendResetPasswordRequest();
});

function sendResetPasswordRequest() {
    if(Loading.active)
        false;

    local uiResult = BaseGUI.normalize({email = email});

    if(uiResult.email.len() < 4) {
        showPasswordRequestError("Zbyt kr�tki e-mail.");
        return;
    }

    Loading.show();
    Account.Packet.requestPassword(uiResult.email);
    hideOutResetPasswordMenu();
    showPasswordMenu();
}

function showPasswordRequestError() {
    draws[0].setText("B��dny kod.");
}

function showPasswordRequestMenu() {
    ActiveGui = PlayerGUI.PasswordRequest;
    window.setVisible(true);
}

function hideResetPasswordMenu() {
    BaseGUI.hide();
    ActiveGui = null;
    window.setVisible(false);
}

function hideOutResetPasswordMenu() {
    window.setVisible(false);
}
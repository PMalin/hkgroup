
local window = GUI.Window(anx(80), any(Resolution.y/2 - 250), anx(524), any(562), "HK_BOX.TGA", null, false);
local logo = GUI.Button(anx(-20), any(-176), anx(524), any(262), "HK_LOGO.TGA", "", window);

local rerollPasswordButton = GUI.Button(anx(42), any(450), anx(440), any(50), "HK_BUTTON.TGA", "Potwierd�", window);

GUI.Draw(anx(42), any(70), "Poprosimy o wybranie\n nowego has�a.\nJest to r�wnoznaczne z akceptacj�\npolityki prywatno�ci.\nhttps://historiakolonii.pl/regulamin", window);
local password = GUI.Input(anx(42), any(340), anx(440), any(45), "HK_INPUT.TGA", "FONT_OLD_10_WHITE_HI.TGA", Input.Password, Align.Center, "Has�o", 2, window);

local draws = [
    GUI.Draw(anx(50), any(640), "", window),
];

foreach(draw in draws)
{
    draw.setScale(0.5,0.5);
    draw.setFont("FONT_OLD_20_WHITE_HI.TGA")
}

rerollPasswordButton.bind(EventType.Click, function(element) {
    sendRerollPasswordRequest();
});

function sendRerollPasswordRequest() {
    if(Loading.active)
        false;

    local uiResult = BaseGUI.normalize({password = password});

    if(uiResult.password.len() < 4) {
        showPasswordRerollError("Zbyt kr�tke has�o.");
        return;
    }

    Loading.show();
    Account.Packet.rerollPassword(uiResult.password);
}

function showPasswordRerollError(val = "Has�o takie samo jak poprzednie.") {
    draws[0].setText(val);
}

function showPasswordRerollMenu() {
    ActiveGui = PlayerGUI.PasswordReroll;
    window.setVisible(true);
}

function hideRerollPasswordMenu() {
    BaseGUI.hide();
    ActiveGui = null;
    window.setVisible(false);
}
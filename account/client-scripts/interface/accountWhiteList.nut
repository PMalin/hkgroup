
local texture = Texture(0,0,8200,8200,"BLACK.TGA");
local window = GUI.Window(anx(80), any(Resolution.y/2 - 250), anx(424), any(362), "MENU_INGAME.TGA", null, false);
local whiteListButton = GUI.Button(anx(42), any(250), anx(340), any(50), "MENU_INGAME.TGA", "Potwierd�", window);

GUI.Draw(anx(42), any(70), "Wpisz kod, kt�ry dosta�e�\nod ekipy serwera.", window);
local code = GUI.Input(anx(42), any(140), anx(340), any(45), "MENU_INGAME.TGA", "FONT_OLD_10_WHITE_HI.TGA", Input.Text, Align.Center, "Kod", 2, window);

whiteListButton.bind(EventType.Click, function(element) {
    sendWhiteListAccept();
});

function sendWhiteListAccept() {
    local uiResult = BaseGUI.normalize({code = code});

    if(uiResult.code.len() < 4) {
        showPasswordError("Kod zbyt kr�tki.");
        return;
    }

    if(uiResult.code.len() > 32) {
        showPasswordError("Kod zbyt d�ugi.");
        return;
    }

    Account.Packet.whiteList(uiResult.code);
    hideWhiteListMenu();
    showLoggInMenu();
}

function showWhiteListMenu() {
    clearMultiplayerMessages();
    enable_DamageAnims(false);
    enable_NicknameId(true);
    showLoggInMenu();
}

function hideWhiteListMenu() {
    texture.visible = false;
    BaseGUI.hide();
    ActiveGui = null;
    window.setVisible(false);
}

addEventHandler("onInit", showWhiteListMenu);

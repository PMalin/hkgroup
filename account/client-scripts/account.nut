
Account <- {
    id = -1,

    loggIn = false,
    isNew = false,

    username = "",
    password = "",

    extraSkins = {},
    keyBinds = {},
}

foreach(_itemName, _item in Config["DynamicBindings"]) {
    Account.keyBinds[_itemName] <- _item.defined;
}

function setAccountKeyBind(_itemName, _value) {
    foreach(_id, _v in Account.keyBinds) {
        if(_v == _value)
            return;
    }

    Account.keyBinds[_itemName] = _value;
}

function getAccountKeyBind(_itemName) {
    return Account.keyBinds[_itemName];
}

function isKeyBindAccountTaken(_value) {
    foreach(_id, _v in Account.keyBinds) {
        if(_v == _value)
            return true;
    }

    return false;
}

class GamePacket
{
    packet = null;
    object = null;

    constructor(gameObject)
    {
        packet = ExtendedPacket(Packets.Game);
        object = gameObject;
    }

    function create(targetId = -1) {
        packet.writeUInt8(GamePackets.Create)
        packet.writeInt32(object.id);

        if(targetId == -1)
            packet.sendToAll(RELIABLE);
        else
            packet.send(targetId, RELIABLE);
    }

    function deleteGame() {
        packet.writeUInt8(GamePackets.Delete)
        packet.writeInt32(object.id);
        packet.sendToAll(RELIABLE);
    }
}

class DrawController
{
    static draws = {};

    static function onInit() {
        local drawsInDatabase = Query().select().from(WorldDraw.DrawTable).all();

        foreach(item in drawsInDatabase)
        {
            local textC = split(item["text"],"&");

            local text = [];
            foreach(item in textC)
                text.append(item);

            local drawObject = WorldDraw(item["id"].tointeger(), item["distance"].tointeger(), item["x"].tofloat(),item["y"].tofloat(),item["z"].tofloat(), item["r"].tointeger(),item["g"].tointeger(),item["b"].tointeger(), item["world"], text);

            draws[drawObject.id] <- drawObject;
        }

        print("We have "+DrawController.draws.len()+" registered draws.")
    }

    static function onPlayerJoin(playerId) {
        foreach(draw in draws)
            DrawPacket(draw).create(playerId);
    }

    static function destroyDraw(drawId)
    {
        if(drawId in draws) {
            for(local i = 0; i < getMaxSlots(); i ++)
            {
                if(isPlayerConnected(i))
                    DrawPacket(draws[drawId]).remove(i);
            }

            Query().deleteFrom(WorldDraw.DrawTable).where(["id = "+drawId]).execute();
            draws.rawdelete(drawId);
        }
    }

    static function onPacket(playerId, packet) {
        local typ = packet.readUInt8();

        switch(typ)
        {
            case DrawPackets.Create:
                local drawId = packet.readInt16();
                if(drawId in draws)
                    return false;

                local distance = packet.readInt16();

                local r = packet.readInt16();
                local g = packet.readInt16();
                local b = packet.readInt16();

                local x = packet.readFloat();
                local y = packet.readFloat();
                local z = packet.readFloat();

                local world = packet.readString();

                local text = [];

                local textLen = packet.readInt16();
                for(local i = 0; i < textLen; i ++)
                    text.push(packet.readString());

                local drawObject = WorldDraw(drawId, distance, x,y,z, r,g,b, world, text);
                draws[drawId] <- drawObject;

                Query().insertInto(WorldDraw.DrawTable, ["id", "distance", "x", "y", "z", "r", "g", "b", "world","text"], [
                    drawId, distance, x, y, z, r, g, b, "'"+getPlayerWorld(playerId)+"'", "'"+String.parseArray(text,"&")+"'"
                ]).execute();

                for(local i = 0; i < getMaxSlots(); i ++)
                {
                    if(isPlayerConnected(i))
                        DrawPacket(draws[drawId]).create(i);
                }
            break;
            case DrawPackets.Delete:
                local drawId = packet.readInt16();
                if(!(drawId in draws))
                    return false;

                destroyDraw(drawId)
            break;
        }
    }
}

addEventHandler("onInit", DrawController.onInit.bindenv(DrawController))
addEventHandler("onPlayerJoin", DrawController.onPlayerJoin.bindenv(DrawController))
RegisterPacketReceiver(Packets.Draw, DrawController.onPacket.bindenv(DrawController));

class WorldDraw
{
    static DrawTable = "draws";

    id = -1

    distance = -1

    position = null
    color = null
    text = null

    world = ""

    constructor(id, distance, x,y,z, r,g,b, world, text) {
        this.id = id;
        this.distance = distance;

        this.position = {x = x, y = y, z = z};
        this.color = {r = r, g = g, b = b};

        this.world = world;
        this.text = text;
    }
}


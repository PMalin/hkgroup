local tick = 0;

class DrawController
{
    static draws = {};

    static function destroyDraw(drawId)
    {
        draws[drawId].remove();
        draws.rawdelete(drawId);

        if(ActiveGui == PlayerGUI.ServerBuilderDraws)
            DrawsBuilder.refreshList();
    }

    static function onWorldChange(world)
    {
        foreach(draw in draws) {
            if(draw.world == world)
                draw.show();
            else
                draw.hide();
        }
    }

    static function onRender()
    {
        if(tick > getTickCount())
            return;

        tick = getTickCount() + 500;

        foreach(draw in draws)
            draw.element.visible = ActiveGui == null;

    }

    static function getNextId()
    {
        local id = 0;
        foreach(_id, draw in draws)
            if(_id >= id)
                id = _id + 1

        return id;
    }

    static function onPacket(packet) {
        local typ = packet.readUInt8();

        switch(typ)
        {
            case DrawPackets.Create:
                local drawId = packet.readInt16();
                if(drawId in draws)
                    return false;

                local distance = packet.readInt16();

                local r = packet.readInt16();
                local g = packet.readInt16();
                local b = packet.readInt16();

                local x = packet.readFloat();
                local y = packet.readFloat();
                local z = packet.readFloat();

                local world = packet.readString();

                local text = [];

                local textLen = packet.readInt16();
                for(local i = 0; i < textLen; i ++)
                    text.push(packet.readString());

                local drawObject = WorldDraw(drawId, distance, x,y,z, r,g,b, world, text);
                draws[drawId] <- drawObject;

                if(world == getWorld())
                    drawObject.show();

                if(ActiveGui == PlayerGUI.ServerBuilderDraws)
                    DrawsBuilder.refreshList();
            break;
            case DrawPackets.Delete:
                local drawId = packet.readInt16();
                if(!(drawId in draws))
                    return false;

                destroyDraw(drawId)
            break;
        }
    }
}

RegisterPacketReceiver(Packets.Draw, DrawController.onPacket.bindenv(DrawController));
addEventHandler("onWorldChange", DrawController.onWorldChange.bindenv(DrawController))
addEventHandler("onRender", DrawController.onRender.bindenv(DrawController))

class WorldDraw
{
    id = -1

    position = null
    color = null
    text = null
    distance = -1

    world = ""

    element = null

    constructor(id, distance, x,y,z, r,g,b, world, text) {
        this.id = id;

        this.position = {x = x, y = y, z = z};
        this.color = {r = r, g = g, b = b};

        this.world = world;
        this.text = text;
        this.distance = distance;

        this.element = WorldInterface.Draw3d(x,y,z);
        this.element.distance = distance;
        this.element.visible = false;

        foreach(_text in text)
            this.element.addLine(_text);

        this.element.setAlpha(200);
        this.element.setColor(r,g,b);
        this.element.setFont("FONT_OLD_20_WHITE_HI.TGA");
        this.element.setScale(0.5,0.5);
    }

    function remove() {
        element.remove();
    }

    function hide() {
        element.visible = false;
    }

    function show() {
        element.visible = true;
    }
}


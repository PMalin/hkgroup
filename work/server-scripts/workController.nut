
class WorkController
{
    static works = {};

    static function onInit() {
        local worksInDatabase = Query().select().from(Work.WorkTable).all();

        foreach(work in worksInDatabase)
        {
            local newWork = Work(work["id"].tointeger())
            newWork.name = work["name"];
            newWork.caller = work["caller"].tointeger();
            newWork.animation = work["animation"];
            newWork.staminaCost = work["staminaCost"].tofloat();
            newWork.staminaCostMultiplier = work["staminaCostMultiplier"].tointeger();
            newWork.seconds = work["seconds"].tointeger();
            newWork.secondsMultiplier = work["secondsMultiplier"].tointeger();
            newWork.skillRequired = work["skillRequired"].tointeger();
            newWork.skillValue = work["skillValue"].tointeger();
            newWork.createdBy = work["createdBy"];
            newWork.createdAt = work["createdAt"];

            local workRewards = Query().select().from(Work.WorkRewardsTable).where(["workId = "+newWork.id+""]).all();

            foreach(workReward in workRewards)
                newWork.rewards[workReward["instance"]] <- workReward["amount"].tointeger();

            works[newWork.id] <- newWork;
        }

        print("We have " + works.len() + " registered works.");
    }

    static function onPacket(playerId, packet) {

    }
}

addEventHandler("onInit", WorkController.onInit.bindenv(WorkController))
RegisterPacketReceiver(Packets.Work, WorkController.onPacket.bindenv(WorkController));

getWork <- @(id) WorkController.works[id];
getWorks <- @() WorkController.works;

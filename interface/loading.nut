
Loading <- {
    bg = Texture(anx(0), any(0), anx(Resolution.x), any(Resolution.y), "DLG_CONVERSATION.TGA"),
    draw = Draw(anx(0), any(0), ""),
    active = true
    renderTime = 0

    show = function(text = "Oczekiwanie na odpowied�..") {
        draw.text = text;
        draw.setColor(255,0,0);
        draw.setPosition(4129 - draw.width/2, 4129);

        bg.visible = true;
        draw.visible = true;
        draw.alpha = 0;
        bg.alpha = 0;

        renderTime = getTickCount() + 250;

        active = true
    }

    hide = function() {
        bg.visible = false;
        draw.visible = false;

        active = false
    }

    onRender = function() {
        if(active == false)
            return;

        if(renderTime != 0 && renderTime < getTickCount()) {
            draw.alpha = 255;
            bg.alpha = 255;
            renderTime = 0;
        }
    }
}

addEventHandler ("onRender", Loading.onRender.bindenv(Loading));

Notification <- {
    positionXOnScreen = anx(Resolution.x - 260)
    bucket = [null, null, null, null, null]

    add = function(text) {
        bucket[4] = bucket[3]
        bucket[3] = bucket[2]
        bucket[2] = bucket[1]
        bucket[1] = bucket[0]
        bucket[0] = Notification.Item(text);

        updatePositions()
    }

    onRender = function() {
        local tick = getTickCount();
        foreach(index, item in bucket) {
            if(item != null) {
                if(item.leftStare < tick)
                    bucket[index] = null;
                else
                    item.onRender();
            }
        }
    }

    remove = function(index) {
        bucket[index] = null;
    }

    clear = function() {
        bucket = [null, null, null, null, null];
    }

    updatePositions = function() {
        local height = 0;

        foreach(index, item in bucket)
            if(item != null)
                height += item.getHeight();

        local startY = any(Resolution.y/2) - height/2;
        foreach(index, item in bucket) {
            if(item != null) {
                local y = startY;
                startY = startY + item.getHeight() + any(20);
                height = height - (item.getHeight() + any(20));
                item.updatePosition(y)
            }
        }
    }
}

class Notification.Item
{
    draws = null
    background = null
    leftStare = 0;

    constructor(text) {
        draws = []

        local lines = abs(text.len()/21);
        for(local i = 0; i <= lines; i ++) {
            local slicedText = text;

            if(i < lines) {
                slicedText = text.slice(0, 21);
                text = text.slice(21, text.len())
            }

            draws.push(Draw(0,0,slicedText))
        }

        background = Texture(anx(Resolution.x + 20), 0, anx(240), any(10 + draws.len() * 20), "HK_INPUT.TGA");
        background.alpha = 155;
        background.visible = true;

        foreach(draw in draws) {
            draw.font = "FONT_OLD_20_WHITE_HI.TGA";
            draw.setScale(0.4,0.4);
            draw.alpha = 150;
            draw.visible = true;
        }

        leftStare = getTickCount() + (1000 * 10);
    }

    function onRender() {
        foreach(draw in draws)
            if(draw.alpha < 255)
                draw.alpha += 1;

        if(background.alpha < 225)
            background.alpha += 1;

        local pos = background.getPosition();

        if(pos.x >= Notification.positionXOnScreen)
            pos.x = pos.x - 50;

        background.setPosition(pos.x, pos.y);

        foreach(index, draw in draws)
            draw.setPosition(pos.x + anx(15), pos.y + any(10) + any(20 * index))
    }

    function getHeight() {
        return background.getSize().height;
    }

    function updatePosition(y) {
        local pos = background.getPosition();
        background.setPosition(pos.x,y);
        foreach(index, draw in draws)
            draw.setPosition(pos.x + anx(15), y + any(10) + any(20 * index))
    }
}

addEventHandler("onRender", Notification.onRender.bindenv(Notification));
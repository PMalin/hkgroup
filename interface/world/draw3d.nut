
class WorldInterface.Draw3dLine
{
    text = null

    scale = null
    font = null
    color = null
    rotation = null
    alpha = null

    element = null

    constructor(text)
    {
        this.text = text;

        this.scale = {x = 1.0, y = 1.0}
        this.font = "FONT_OLD_10_WHITE_HI.TGA"
        this.color = {r = 255, g = 255, b = 255}
        this.rotation = 0;
        this.alpha = 255;

        this.element = null;
    }

    function createRender() {
        element = Draw(0,0,text);
        updateRender();
    }

    function destroyRender() {
        element = null;
    }

    function updatePosition(cameraX, cameraY, width, height, shrinkX, shrinkY) {
        element.setScale(scale.x * shrinkX, scale.y * shrinkY);

        local mWidth = (getWidth())
        local mHeight = (getHeight())

        cameraX = (cameraX - width/2) + (width/2 - mWidth/2);

        element.setPositionPx(cameraX, cameraY + height);
        element.visible = true;

        return {width = width, height = ((mHeight * 1.7) + height)}
    }

    function hide() {
        if(element != null)
            element.visible = false;
    }

    function updateRender() {
        if(element == null)
            return;

        element.text = text;
        element.rotation = rotation
        element.font = font
        element.alpha = alpha;
        element.setScale(scale.x, scale.y)
        element.setColor(color.r, color.g, color.b)
    }

    function getWidth() {
        if(element != null)
            return element.widthPx;

        return 0;
    }

    function getHeight() {
        if(element != null)
            return element.heightPx;

        return 0;
    }
}

class WorldInterface.Draw3d
{
    lines = null
    position = null

    visible = false
    distance = false

    eyeMode = false
    rendered = false;

    shrinkMode = false
    shrinkScale = null

    constructor(x,y,z)
    {
        position = {x = x, y = y, z = z}
        lines = [];

        visible = false
        distance = 3000

        eyeMode = true
        rendered = false

        shrinkMode = true
        shrinkScale = {x = 1.0, y = 1.0};

        WorldInterface.list.append(this);
    }

    function addLine(text) {
        lines.push(WorldInterface.Draw3dLine(text));
    }

    function removeLine(index) {
        lines.remove(index);
    }

    function removeLines() {
        lines.clear();
    }

    function getLine(index) {
        return lines[index];
    }

    function getLines() {
        return lines;
    }

    function setFont(font) {
        foreach(line in lines) {
            line.font = font;
            line.updateRender();
        }
    }

    function setLineFont(index, font) {
        lines[index].font = font;
        lines[index].updateRender();
    }

    function setAlpha(value) {
        foreach(line in lines) {
            line.alpha = value;
            line.updateRender();
        }
    }

    function setLineAlpha(index, value) {
        lines[index].alpha = value;
        lines[index].updateRender();
    }

    function setRotation(rot) {
        foreach(line in lines) {
            line.rotation = rot;
            line.updateRender();
        }
    }

    function setLineRotation(index, rot) {
        lines[index].rotation = rot;
        lines[index].updateRender();
    }

    function setScale(x,y) {
        foreach(line in lines) {
            line.scale = {x=x,y=y};
            line.updateRender();
        }
    }

    function setLineScale(index,x,y) {
        lines[index].scale = {x=x,y=y};
        lines[index].updateRender();
    }

    function setColor(r,g,b) {
        foreach(line in lines) {
            line.color = {r=r,g=g,b=b};
            line.updateRender();
        }
    }

    function setLineColor(index, r,g,b) {
        lines[index].color = {r=r,g=g,b=b};
        lines[index].updateRender();
    }

    function setText(text) {
        foreach(line in lines) {
            line.text = text;
            line.updateRender();
        }
    }

    function setLineText(index,text) {
        lines[index].text = text;
        lines[index].updateRender();
    }

    function update(pos) {
        if(visible == false){
            foreach(item in lines)
                item.hide();

            return;
        }

        local _distance = getDistance3d(pos.x, pos.y, pos.z, position.x, position.y, position.z);
        if(_distance <= distance)
        {
            local show = true;
            local camera = _Camera.project(position.x, position.y, position.z);
            if(camera == null)
                show = false;

            local height = 0;
            local width = 0;

            if(eyeMode && show)
            {
                local vector = getVectorAngle(position.x, position.z, pos.x, pos.z) + 500;
                vector = abs(vector - (getPlayerAngle(heroId) + 500));
                if(!(vector > 140 && vector < 220))
                    show = false;
            }

            if(shrinkScale == false)
                shrinkScale = {x = 1.0, y = 1.0}
            else
                shrinkScale = {x = 1.0 - (_distance/distance), y = 1.0 - (_distance/distance)}

            foreach(item in lines)
            {
                local mWidth = (item.getWidth() * shrinkScale.x);
                if(width < mWidth)
                    width = mWidth;
            }

            foreach(item in lines)
            {
                if(show) {
                    local size = item.updatePosition(camera.x, camera.y, width, height, shrinkScale.x, shrinkScale.y);
                    height = size.height;
                }else
                    item.hide();
            }
        }else{
            foreach(item in lines)
                item.hide();
        }
    }

    function remove() {
        local index = WorldInterface.render.find(this);
        if(index != null)
            WorldInterface.render.remove(index);

        local index = WorldInterface.list.find(this);
        if(index != null)
            WorldInterface.list.remove(index);
    }

    function render() {
        rendered = true;

        foreach(item in lines)
            item.createRender();
    }

    function unrender() {
        rendered = false;

        foreach(item in lines)
            item.destroyRender();
    }

    function setWorldPosition(x,y,z) {
        position.x = x;
        position.y = y;
        position.z = z;
    }
}

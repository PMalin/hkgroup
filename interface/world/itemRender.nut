
class WorldInterface.ItemRender extends WorldInterface.Texture
{
    instance = null;
    visual = null;

    rotX = -1;
    rotY = -1;
    rotZ = -1;

    constructor(x, y, z, width, height, instance)
    {
        base.constructor(x, y, z, width, height, instance);

        this.instance = instance;
        this.visual = "";

        this.rotX = 0;
        this.rotY = 0;
        this.rotZ = 0;
    }

    function render() {
        rendered = true;

        element = ItemRender(0,0,0,0,instance);
        if(visual != "")
            element.visual = visual;

        element.rotX = rotX;
        element.rotY = rotY;
        element.rotZ = rotZ;
    }

    function unrender() {
        rendered = false;

        element = null;
    }
}
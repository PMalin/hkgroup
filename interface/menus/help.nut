

local window = GUI.Window(anx(Resolution.x/2 - 400), any(Resolution.y/2 - 400), anx(800), any(740), "HK_BOX.TGA", null, false);

local mainHelp = GUI.Button(anx(10), any(10), anx(250), any(60), "HK_BUTTON.TGA", "Og�lne", window);
local keysHelp = GUI.Button(anx(270), any(10), anx(250), any(60), "HK_BUTTON.TGA", "Klawisze", window);
local mechanicHelp = GUI.Button(anx(530), any(10), anx(250), any(60), "HK_BUTTON.TGA", "Mechanika", window);

mainHelp.bind(EventType.Click, function(element) {
    changeHelpMenu(0);
});

keysHelp.bind(EventType.Click, function(element) {
    changeHelpMenu(1);
});

mechanicHelp.bind(EventType.Click, function(element) {
    changeHelpMenu(2);
});

local drawHelp = GUI.Draw(anx(30), any(90), "", window);
local exitButton = GUI.Button(anx(775), any(-25), anx(50), any(50), "HK_CHECKBOX.TGA", "X", window);
local bindExit = Bind.addClick(exitButton, function() {
    hideHelpMenu();
}, PlayerGUI.HelpMenu);

function changeHelpMenu(id) {
    local text = "[#FF0000]Og�lne\n[#FFFFFF]Na serwerze istnieje wiele system�w\n";
    text += "kt�re pomog� ci stworzy� wymarzon� posta�.\n";
    text += "Do twojej dyspozycji jest wiele profesji.\n";
    text += "Wiele pobocznych aktywno�ci i wiele wyzwa�.\n";
    text += "Na serwerze nie jeste� przywi�zany do jakiejkolwiek\n";
    text += "frakcji, je�eli zbierzesz odpowiednie\n";
    text += "zasoby i grup� przyjaci� sam mo�esz\n";
    text += "stworzy� organizacj�, kt�ra zagrozi SO!\n";

    switch(id) {
        case 1:
            text = "[#FF0000]Klawisze\n[#FFFFFF]Dost�pne klawisze i ich spos�b dzia�ania:\n";
            text += "[#FF0000]ESCAPE[#FFFFFF] - menu gry\n";
            text += "[#FF0000]B[#FFFFFF] - statystyki\n";
            text += "[#FF0000]N[#FFFFFF] - notatki\n";
            text += "[#FF0000]I/TAB[#FFFFFF] - ekwipunek\n";
            text += "[#FF0000]X[#FFFFFF] - skradanie\n";
            text += "[#FF0000]LSHIFT + myszka[#FFFFFF] - 30x handel/skrzynia\n";
            text += "[#FF0000]Y[#FFFFFF] - w�am\n";
            text += "[#FF0000]O[#FFFFFF] - kalendarz\n";
            text += "[#FF0000]F5[#FFFFFF] - lista graczy\n";
            text += "[#FF0000]F6[#FFFFFF] - sie�\n";
            text += "[#FF0000]F7[#FFFFFF] - brak UI\n";
            text += "[#FF0000]UP[#FFFFFF] - Nast�pny blok chatu\n";
            text += "[#FF0000]NUMPAD 1/2/3[#FFFFFF] - <x> blok chatu\n";
            text += "[#FF0000]LWIN[#FFFFFF] - sprint\n";
            text += "[#FF0000]G/J/K/L[#FFFFFF] - interakcja z obiektem\n";
        break;
        case 2:
            text = "[#FF0000]Mechanika\n[#FFFFFF]Kr�tki spis najwa�niejszych mechanik:\n";
            text += "[#FF0000]Craft[#FFFFFF] - dost�pny pod wieloma r�nymi obiektami.\n";
            text += "[#FF0000]Wytrzyma�o��[#FFFFFF] - dost�pna pod dwoma wariantami.\n";
            text += "Jeden wariant obs�uguje prace i crafting. \nOdnawia si� on raz do 100% w nowym dniu.\n";
            text += "gry. Plus podczas nast�pnej godziny gry kolejne 100%. \nI sprint, kt�ry ondawia si� ca�y czas.\n";
            text += "[#FF0000]Prace[#FFFFFF] - prace na serwerze to g��wne 4 ga��zie, kt�re.\n";
            text += "s� najbardziej rozpowszechnione. Czyli pracowanie w \nkopalni i uzyskiwanie\n";
            text += "rud r�nych metali. Praca jako drwal i �cinanie drzew. \nPraca jako rybak.\n";
            text += "I praca jako rolnik, dzi�ki systemowi sadzenia ro�lin\nmamy mo�liwo��";
            text += "sadzenia w wybranym przez nas miejscu dowolnej ro�liny. \nMusimy pami�ta�";
            text += "�e niekt�re ro�liny mo�emy posadzi� \ntylko na dobrym gruncie. (jako�� gruntu)\n";
            text += "[#FF0000]Chat/komunikacja[#FFFFFF] - na serwerze mamy kilka \ntyp�w chat�w.";
            text += "Chat IC, OOC i globalny. To jest dost�pne do wgl�du \ndla ka�dego gracza.";
            text += "Na chacie IC mo�emy bez u�ycia komend u�ywa�\nME,DO dzi�ki # i <>.";
            text += "Ponad to mo�emy napisa� \nwi�cej ni� jedn� linijk� tekstu.\n";
            text += "Wi�cej informacji znajdziesz na discordzie \nprojektu, zapraszamy.\n";
        break;
    }

    drawHelp.setText(text);
}

function showHelpMenu() {
    BaseGUI.show();
    ActiveGui = PlayerGUI.HelpMenu;
    window.setVisible(true);
    changeHelpMenu(0);
}

function hideHelpMenu() {
    BaseGUI.hide();
    ActiveGui = null;
    window.setVisible(false);
}

Bind.addKey("HELP", showHelpMenu)
Bind.addKey("HELP", hideHelpMenu, PlayerGUI.HelpMenu)
Bind.addKey(KEY_ESCAPE, hideHelpMenu, PlayerGUI.HelpMenu)
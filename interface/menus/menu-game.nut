
local result = {};

result.window <- GUI.Window(anx(Resolution.x/2 - 175), any(Resolution.y/2 - 340), anx(350), any(600), "HK_BOX.TGA", null, false);
GUI.Window(anx(Resolution.x/2 - 175), any(Resolution.y/2 - 450), anx(350), any(180), "HK_LOGO.TGA", null, false);
result.backButton <- GUI.Button(anx(50), any(50), anx(250), any(50), "HK_BUTTON.TGA", "Wr��", result.window);
result.helpButton <- GUI.Button(anx(50), any(115), anx(250), any(50), "HK_BUTTON.TGA", "Pomoc", result.window);
result.visualButton <- GUI.Button(anx(50), any(180), anx(250), any(50), "HK_BUTTON.TGA", "Wygl�d", result.window);
result.characterButton <- GUI.Button(anx(50), any(245), anx(250), any(50), "HK_BUTTON.TGA", "Posta�", result.window);
result.animationsButton <- GUI.Button(anx(50), any(310), anx(250), any(50), "HK_BUTTON.TGA", "Animacje", result.window);
result.settingsButton <- GUI.Button(anx(50), any(375), anx(250), any(50), "HK_BUTTON.TGA", "Ustawienia", result.window);
result.logoutButton <- GUI.Button(anx(50), any(440), anx(250), any(50), "HK_BUTTON.TGA", "Wyloguj", result.window);
result.exitButton <- GUI.Button(anx(50), any(505), anx(250), any(50), "HK_BUTTON.TGA", "Wyjd�", result.window);

result.bindBack <- Bind.addClick(result.backButton, function() {
    hideMenuGame();
}, PlayerGUI.MenuGame);

result.bindHelp <- Bind.addClick(result.helpButton, function() {
    hideMenuGame();
    showHelpMenu();
}, PlayerGUI.MenuGame);

result.bindCharacter <- Bind.addClick(result.characterButton, function() {
    hideMenuGame();
    PlayerStatistics.show()
}, PlayerGUI.MenuGame)

result.bindSettings <- Bind.addClick(result.settingsButton, function() {
    hideMenuGame();
    showMenuSettings();
}, PlayerGUI.MenuGame)

result.animationsSettings <- Bind.addClick(result.animationsButton, function() {
    hideMenuGame();
    AnimationInterface.ShowList();
}, PlayerGUI.MenuGame)

result.bindVisual <- Bind.addClick(result.visualButton, function() {
    hideMenuGame();
    VisualChange.start();
}, PlayerGUI.MenuGame);

result.bindLogout <- Bind.addClick(result.logoutButton, function() {
    hideMenuGame();
    setPlayerWeaponMode(heroId,0);
    Loading.show();
    Account.Packet.logoutAction();
}, PlayerGUI.MenuGame);

result.bindExit <- Bind.addClick(result.exitButton, function() {
    exitGame();
}, PlayerGUI.MenuGame);

function showMenuGame()
{
    BaseGUI.show();
    ActiveGui = PlayerGUI.MenuGame;
    result.window.setVisible(true);
}

function hideMenuGame()
{
    BaseGUI.hide();
    ActiveGui = null;
    result.window.setVisible(false);
}

Bind.addKey(KEY_ESCAPE, showMenuGame);
addEventHandler("onForceCloseGUI", function() {
    if(ActiveGui == PlayerGUI.MenuGame)
        hideMenuGame();
});
Bind.addKey(KEY_ESCAPE, hideMenuGame, PlayerGUI.MenuGame)
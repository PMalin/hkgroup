local lock = false;

enum VisualOption
{
    Body,
    Skin,
    Head,
    Face,
    Size,
    Fatness,
    Walking
}


VisualChange <- {

    bucket = null,

    prepareWindow = function () {
        local result = {};

        result.window <- GUI.Window(anx(40), any(Resolution.y/2 - 300), anx(420), any(350), "HK_BOX.TGA", null, false);

        result.bodyButton <- GUI.Button(anx(10), any(50), anx(200), any(60), "HK_BUTTON.TGA", "P�e�", result.window);
        result.skinButton <- GUI.Button(anx(210), any(50), anx(200), any(60), "HK_BUTTON.TGA", "Cia�o", result.window);
        result.headButton <- GUI.Button(anx(10), any(130), anx(200), any(60), "HK_BUTTON.TGA", "G�owa", result.window);
        result.faceButton <- GUI.Button(anx(210), any(130), anx(200), any(60), "HK_BUTTON.TGA", "Twarz", result.window);
        result.walkingButton <- GUI.Button(anx(10), any(210), anx(200), any(60), "HK_BUTTON.TGA", "Chodzenie", result.window);
        result.sizeButton <- GUI.Button(anx(10), any(0), anx(0), any(0), "HK_BUTTON.TGA", "", result.window);
        result.fatnessButton <- GUI.Button(anx(210), any(210), anx(200), any(60), "HK_BUTTON.TGA", "Grubo��", result.window);

        result.scrollBar <- GUI.ScrollBar(anx(Resolution.x/2), any(50), anx(30), any(30), "HK_SCROLL_HORIZONTAL.TGA", "HK_INDICATOR.TGA", "HK_L.TGA", "HK_R.TGA", Orientation.Horizontal, result.window);
        result.scrollBar.setMaximum(360);
        result.scrollBar.setSize(anx(500), any(30));
        result.scrollBar.setPosition(anx(Resolution.x/2 - 250), any(Resolution.y - 200));

        result.bindForScrollbar <- Bind.onChange(result.scrollBar, function(value) {
            setPlayerAngle(heroId, value);
        }, PlayerGUI.Visual);

        result.bodyButton.bind(EventType.Click, function(element) {
            VisualChange.chooseOperation(VisualOption.Body);
        });

        result.skinButton.bind(EventType.Click, function(element) {
            VisualChange.chooseOperation(VisualOption.Skin);
        });

        result.headButton.bind(EventType.Click, function(element) {
            VisualChange.chooseOperation(VisualOption.Head);
        });

        result.faceButton.bind(EventType.Click, function(element) {
            VisualChange.chooseOperation(VisualOption.Face);
        });

        result.sizeButton.bind(EventType.Click, function(element) {
            VisualChange.chooseOperation(VisualOption.Size);
        });

        result.fatnessButton.bind(EventType.Click, function(element) {
            VisualChange.chooseOperation(VisualOption.Fatness);
        });

        result.walkingButton.bind(EventType.Click, function(element) {
            VisualChange.chooseOperation(VisualOption.Walking);
        });

        result.windowOperation <- GUI.Window(anx(Resolution.x - 420), any(Resolution.y/2 - 300), anx(400), any(560), "HK_BOX.TGA", null, false);
        result.operations <- [];

        return result;
    }

    chooseOperation = function (typeId)
    {
        foreach(element in bucket.operations)
        {
            if(element instanceof GUI.Input || element instanceof GUI.Button || element instanceof GUI.Texture || element instanceof GUI.CheckBox)
                element.setVisible(false);

            element.destroy();
        }

        lock = false;
        stopAni(heroId);

        bucket.operations.clear();
        bucket.windowOperation.destroyAll();
        bucket.windowOperation = GUI.Window(anx(Resolution.x - 420), any(Resolution.y/2 - 300), anx(400), any(560), "HK_BOX.TGA", null, false);

        switch(typeId)
        {
            case VisualOption.Body:
                local maleButton = GUI.Button(anx(50), any(200), anx(300), any(80), "HK_BUTTON.TGA", "Mczyzna", bucket.windowOperation);
                local femaleButton = GUI.Button(anx(50), any(300), anx(300), any(80), "HK_BUTTON.TGA", "Kobieta", bucket.windowOperation);

                maleButton.bind(EventType.Click, function(element) {
                    VisualChange.changeVisual(VisualOption.Body, "Hum_Body_Naked0");
                });
                femaleButton.bind(EventType.Click, function(element) {
                    VisualChange.changeVisual(VisualOption.Body, "Hum_Body_Babe0");
                });

                bucket.operations.append(maleButton);
                bucket.operations.append(femaleButton);
            break;
            case VisualOption.Skin:
                for(local left = 0; left < 3; left ++)
                    for(local top = 0; top < 3; top ++)
                        bucket.operations.append(GUI.Button(anx(120) * left + anx(20), any(120) * top + any(60), anx(120), any(120), "HUM_BODY_NAKED_V"+((left * 2) + top + left)+"_C0.TGA", ((left * 2) + top + left).tostring(), bucket.windowOperation));

                foreach(butt in bucket.operations) {
                    butt.bind(EventType.Click, function(element) {
                        VisualChange.changeVisual(VisualOption.Skin, element.getText().tointeger());
                    });
                }

                bucket.operations.append(GUI.ScrollBar(anx(Resolution.x/2), any(50), anx(30), any(30), "HK_SCROLL_HORIZONTAL.TGA", "HK_INDICATOR.TGA", "HK_L.TGA", "HK_R.TGA", Orientation.Horizontal, bucket.windowOperation));
                bucket.operations[bucket.operations.len()-1].setMaximum(Config["PlayerMaxSkin"]);
                bucket.operations[bucket.operations.len()-1].setSize(anx(340), any(30));
                bucket.operations[bucket.operations.len()-1].setPosition(anx(Resolution.x - 400), any(Resolution.y/2 + 200));

                Bind.onChange(bucket.operations[bucket.operations.len()-1], function(value) {
                    for(local left = 0; left < 3; left ++) {
                        for(local top = 0; top < 3; top ++) {
                            VisualChange.bucket.operations[((left * 2) + top + left)].setText((value + (left * 2) + top + left).tostring());
                            VisualChange.bucket.operations[((left * 2) + top + left)].setFile("HUM_BODY_NAKED_V"+(value + (left * 2) + top + left)+"_C0.TGA");
                        }
                    }

                }, PlayerGUI.Visual);
            break;
            case VisualOption.Head:
                local fatBaldHeadButton = GUI.Button(anx(20), any(30), anx(170), any(50), "HK_BUTTON.TGA", "Tusty �ysy", bucket.windowOperation);
                local fighterHeadButton = GUI.Button(anx(210), any(30), anx(170), any(50), "HK_BUTTON.TGA", "Wojowniczy", bucket.windowOperation);
                local ponyHeadButton = GUI.Button(anx(20), any(80), anx(170), any(50), "HK_BUTTON.TGA", "Kucyk", bucket.windowOperation);
                local baldHeadButton = GUI.Button(anx(210), any(80), anx(170), any(50), "HK_BUTTON.TGA", "�ysy", bucket.windowOperation);
                local thiefHeadButton = GUI.Button(anx(20), any(130), anx(170), any(50), "HK_BUTTON.TGA", "Z�odziej", bucket.windowOperation);
                local psionicHeadButton = GUI.Button(anx(210), any(130), anx(170), any(50), "HK_BUTTON.TGA", "Z�o�liwy", bucket.windowOperation);
                local tiredHeadButton = GUI.Button(anx(20), any(180), anx(170), any(50), "HK_BUTTON.TGA", "Zm�czony", bucket.windowOperation);
                local tallHeadButton = GUI.Button(anx(210), any(180), anx(170), any(50), "HK_BUTTON.TGA", "Wysoki", bucket.windowOperation);
                local fatHeadButton = GUI.Button(anx(20), any(230), anx(170), any(50), "HK_BUTTON.TGA", "Gruby", bucket.windowOperation);
                local angryHeadButton = GUI.Button(anx(210), any(230), anx(170), any(50), "HK_BUTTON.TGA", "Z�y", bucket.windowOperation);
                local bigBeardHeadButton = GUI.Button(anx(20), any(280), anx(170), any(50), "HK_BUTTON.TGA", "Broda dua", bucket.windowOperation);
                local smallBeardHeadButton = GUI.Button(anx(210), any(280), anx(170), any(50), "HK_BUTTON.TGA", "Broda ma�a", bucket.windowOperation);
                local middleBeardHeadButton = GUI.Button(anx(20), any(330), anx(170), any(50), "HK_BUTTON.TGA", "Broda �rednia", bucket.windowOperation);
                local musstacheHeadButton = GUI.Button(anx(210), any(330), anx(170), any(50), "HK_BUTTON.TGA", "Broda + w�s", bucket.windowOperation);
                local alternativeHeadButton = GUI.Button(anx(20), any(380), anx(170), any(50), "HK_BUTTON.TGA", "Broda alter.", bucket.windowOperation);
                local pontyailHeadButton = GUI.Button(anx(210), any(380), anx(170), any(50), "HK_BUTTON.TGA", "Broda + kucyk", bucket.windowOperation);
                local sideburnsHeadButton = GUI.Button(anx(20), any(430), anx(170), any(50), "HK_BUTTON.TGA", "Boki wygolone", bucket.windowOperation);
                local longhairHeadButton = GUI.Button(anx(210), any(430), anx(170), any(50), "HK_BUTTON.TGA", "Dugie wosy", bucket.windowOperation);
                local babe12HeadButton = GUI.Button(anx(20), any(480), anx(170), any(50), "HK_BUTTON.TGA", "Kobieta warkocz", bucket.windowOperation);
                local beard4HeadButton = GUI.Button(anx(210), any(480), anx(170), any(50), "HK_BUTTON.TGA", "Broda altern.", bucket.windowOperation);
                local nord2HeadButton = GUI.Button(anx(20), any(530), anx(170), any(50), "HK_BUTTON.TGA", "Nord altern.", bucket.windowOperation);
                local mageHeadButton = GUI.Button(anx(210), any(530), anx(170), any(50), "HK_BUTTON.TGA", "Mag", bucket.windowOperation);
                local nerdHeadButton = GUI.Button(anx(20), any(580), anx(170), any(50), "HK_BUTTON.TGA", "Nerd", bucket.windowOperation);
                local segmentsHeadButton = GUI.Button(anx(210), any(580), anx(170), any(50), "HK_BUTTON.TGA", "Segmenty", bucket.windowOperation);
                local dredHeadButton = GUI.Button(anx(20), any(630), anx(170), any(50), "HK_BUTTON.TGA", "Dredy", bucket.windowOperation);
                local page2HeadButton = GUI.Button(anx(210), any(630), anx(170), any(50), "HK_BUTTON.TGA", "Pa�", bucket.windowOperation);
                local pageHeadButton = GUI.Button(anx(20), any(680), anx(170), any(50), "HK_BUTTON.TGA", "Pa� altern.", bucket.windowOperation);
                local nordHeadButton = GUI.Button(anx(210), any(680), anx(170), any(50), "HK_BUTTON.TGA", "Nord", bucket.windowOperation);
                local monkHeadButton = GUI.Button(anx(20), any(730), anx(170), any(50), "HK_BUTTON.TGA", "Mnich", bucket.windowOperation);
                local longbabeHeadButton = GUI.Button(anx(210), any(730), anx(170), any(50), "HK_BUTTON.TGA", "Kobieta d�ugie", bucket.windowOperation);

                fatBaldHeadButton.bind(EventType.Click, function(element) {VisualChange.changeVisual(VisualOption.Head, "Hum_Head_FatBald");});
                fighterHeadButton.bind(EventType.Click, function(element) {VisualChange.changeVisual(VisualOption.Head, "Hum_Head_Fighter");});
                ponyHeadButton.bind(EventType.Click, function(element) {VisualChange.changeVisual(VisualOption.Head, "Hum_Head_Pony");});
                baldHeadButton.bind(EventType.Click, function(element) {VisualChange.changeVisual(VisualOption.Head, "Hum_Head_Bald");});
                thiefHeadButton.bind(EventType.Click, function(element) {VisualChange.changeVisual(VisualOption.Head, "Hum_Head_Thief");});
                psionicHeadButton.bind(EventType.Click, function(element) {VisualChange.changeVisual(VisualOption.Head, "Hum_Head_Psionic");});
                tiredHeadButton.bind(EventType.Click, function(element) {VisualChange.changeVisual(VisualOption.Head, "ASC_HUM_HEAD_TIRED");});
                tallHeadButton.bind(EventType.Click, function(element) {VisualChange.changeVisual(VisualOption.Head, "ASC_HUM_HEAD_TALL");});
                fatHeadButton.bind(EventType.Click, function(element) {VisualChange.changeVisual(VisualOption.Head, "ASC_HUM_HEAD_FAT");});
                angryHeadButton.bind(EventType.Click, function(element) {VisualChange.changeVisual(VisualOption.Head, "ASC_HUM_HEAD_ANGRY");});
                bigBeardHeadButton.bind(EventType.Click, function(element) {VisualChange.changeVisual(VisualOption.Head, "HUM_HEAD_BrodaDuza");});
                smallBeardHeadButton.bind(EventType.Click, function(element) {VisualChange.changeVisual(VisualOption.Head, "HUM_HEAD_BrodaMala");});
                middleBeardHeadButton.bind(EventType.Click, function(element) {VisualChange.changeVisual(VisualOption.Head, "HUM_HEAD_BrodaSrednia");});
                musstacheHeadButton.bind(EventType.Click, function(element) {VisualChange.changeVisual(VisualOption.Head, "HUM_HEAD_BrodaWas");});
                alternativeHeadButton.bind(EventType.Click, function(element) {VisualChange.changeVisual(VisualOption.Head, "HUM_HEAD_BEARD2");});
                pontyailHeadButton.bind(EventType.Click, function(element) {VisualChange.changeVisual(VisualOption.Head, "HUM_HEAD_PONYBEARD");});
                sideburnsHeadButton.bind(EventType.Click, function(element) {VisualChange.changeVisual(VisualOption.Head, "HUM_HEAD_SIDEBURNS");});
                longhairHeadButton.bind(EventType.Click, function(element) {VisualChange.changeVisual(VisualOption.Head, "HUM_HEAD_LONGHAIR");});
                babe12HeadButton.bind(EventType.Click, function(element) {VisualChange.changeVisual(VisualOption.Head, "HUM_HEAD_BABE12");});
                beard4HeadButton.bind(EventType.Click, function(element) {VisualChange.changeVisual(VisualOption.Head, "HUM_HEAD_BEARD4");});
                nord2HeadButton.bind(EventType.Click, function(element) {VisualChange.changeVisual(VisualOption.Head, "ASC_HUM_HEAD_NORD2");});
                mageHeadButton.bind(EventType.Click, function(element) {VisualChange.changeVisual(VisualOption.Head, "ASC_HUM_HEAD_MAGE");});
                nerdHeadButton.bind(EventType.Click, function(element) {VisualChange.changeVisual(VisualOption.Head, "ASC_HUM_HEAD_NERD");});
                segmentsHeadButton.bind(EventType.Click, function(element) {VisualChange.changeVisual(VisualOption.Head, "ASC_HUM_HEAD_SEGMENTS");});
                dredHeadButton.bind(EventType.Click, function(element) {VisualChange.changeVisual(VisualOption.Head, "ASC_HUM_HEAD_DRED");});
                page2HeadButton.bind(EventType.Click, function(element) {VisualChange.changeVisual(VisualOption.Head, "ASC_HUM_HEAD_PAGE2");});
                pageHeadButton.bind(EventType.Click, function(element) {VisualChange.changeVisual(VisualOption.Head, "ASC_HUM_HEAD_PAGE");});
                nordHeadButton.bind(EventType.Click, function(element) {VisualChange.changeVisual(VisualOption.Head, "ASC_HUM_HEAD_NORD");});
                monkHeadButton.bind(EventType.Click, function(element) {VisualChange.changeVisual(VisualOption.Head, "ASC_HUM_HEAD_MONK");});
                longbabeHeadButton.bind(EventType.Click, function(element) {VisualChange.changeVisual(VisualOption.Head, "HUM_HEAD_LONGBABE");});

                bucket.operations.append(fatBaldHeadButton);
                bucket.operations.append(fighterHeadButton);
                bucket.operations.append(ponyHeadButton);
                bucket.operations.append(baldHeadButton);
                bucket.operations.append(thiefHeadButton);
                bucket.operations.append(psionicHeadButton);
                bucket.operations.append(tiredHeadButton);
                bucket.operations.append(tallHeadButton);
                bucket.operations.append(fatHeadButton);
                bucket.operations.append(angryHeadButton);
                bucket.operations.append(bigBeardHeadButton);
                bucket.operations.append(smallBeardHeadButton);
                bucket.operations.append(middleBeardHeadButton);
                bucket.operations.append(musstacheHeadButton);
                bucket.operations.append(alternativeHeadButton);
                bucket.operations.append(pontyailHeadButton);
                bucket.operations.append(sideburnsHeadButton);
                bucket.operations.append(longhairHeadButton);
                bucket.operations.append(babe12HeadButton);
                bucket.operations.append(beard4HeadButton);
                bucket.operations.append(nord2HeadButton);
                bucket.operations.append(mageHeadButton);
                bucket.operations.append(nerdHeadButton);
                bucket.operations.append(segmentsHeadButton);
                bucket.operations.append(dredHeadButton);
                bucket.operations.append(page2HeadButton);
                bucket.operations.append(pageHeadButton);
                bucket.operations.append(nordHeadButton);
                bucket.operations.append(monkHeadButton);
                bucket.operations.append(longbabeHeadButton);
            break;
            case VisualOption.Face:
                for(local left = 0; left < 3; left ++)
                    for(local top = 0; top < 3; top ++)
                        bucket.operations.append(GUI.Button(anx(120) * left + anx(20), any(120) * top + any(60), anx(120), any(120), "Hum_Head_V"+((left * 2) + top + left)+"_C0.TGA", ((left * 2) + top + left).tostring(), bucket.windowOperation));

                foreach(butt in bucket.operations) {
                    butt.bind(EventType.Click, function(element) {
                        VisualChange.changeVisual(VisualOption.Face, element.getText().tointeger());
                    });
                }

                bucket.operations.append(GUI.ScrollBar(anx(Resolution.x/2), any(50), anx(30), any(30), "HK_SCROLL_HORIZONTAL.TGA", "HK_INDICATOR.TGA", "HK_L.TGA", "HK_R.TGA", Orientation.Horizontal, bucket.windowOperation));
                bucket.operations[bucket.operations.len()-1].setMaximum(Config["PlayerMaxFace"]);
                bucket.operations[bucket.operations.len()-1].setSize(anx(340), any(30));
                bucket.operations[bucket.operations.len()-1].setPosition(anx(Resolution.x - 400), any(Resolution.y/2 + 200));

                Bind.onChange(bucket.operations[bucket.operations.len()-1], function(value) {
                    for(local left = 0; left < 3; left ++) {
                        for(local top = 0; top < 3; top ++) {
                            VisualChange.bucket.operations[((left * 2) + top + left)].setText((value + (left * 2) + top + left).tostring());
                            VisualChange.bucket.operations[((left * 2) + top + left)].setFile("Hum_Head_V"+(value + (left * 2) + top + left)+"_C0.TGA");
                        }
                    }

                }, PlayerGUI.Visual);
            break;

            case VisualOption.Size:
                local midgetSizeButton = GUI.Button(anx(20), any(150), anx(170), any(80), "HK_BUTTON.TGA", "Krasnal", bucket.windowOperation);
                local dwarfSizeButton = GUI.Button(anx(210), any(150), anx(170), any(80), "HK_BUTTON.TGA", "Ma�y", bucket.windowOperation);
                local smallSizeButton = GUI.Button(anx(20), any(250), anx(170), any(80), "HK_BUTTON.TGA", "Niski", bucket.windowOperation);
                local normalSizeButton = GUI.Button(anx(210), any(250), anx(170), any(80), "HK_BUTTON.TGA", "Normalny", bucket.windowOperation);
                local tallSizeButton = GUI.Button(anx(20), any(350), anx(170), any(80), "HK_BUTTON.TGA", "Wysoki", bucket.windowOperation);
                local giantSizeButton = GUI.Button(anx(210), any(350), anx(170), any(80), "HK_BUTTON.TGA", "Gigant", bucket.windowOperation);

                midgetSizeButton.bind(EventType.Click, function(element) {VisualChange.changeVisual(VisualOption.Size, PlayerSpeech.Midget);});
                dwarfSizeButton.bind(EventType.Click, function(element) {VisualChange.changeVisual(VisualOption.Size, PlayerSpeech.Dwarf);});
                smallSizeButton.bind(EventType.Click, function(element) {VisualChange.changeVisual(VisualOption.Size, PlayerSpeech.Small);});
                normalSizeButton.bind(EventType.Click, function(element) {VisualChange.changeVisual(VisualOption.Size, PlayerSpeech.Normal);});
                tallSizeButton.bind(EventType.Click, function(element) {VisualChange.changeVisual(VisualOption.Size, PlayerSpeech.Tall);});
                giantSizeButton.bind(EventType.Click, function(element) {VisualChange.changeVisual(VisualOption.Size, PlayerSpeech.Giant);});

                bucket.operations.append(midgetSizeButton);bucket.operations.append(dwarfSizeButton);bucket.operations.append(smallSizeButton);
                bucket.operations.append(normalSizeButton);bucket.operations.append(tallSizeButton);bucket.operations.append(giantSizeButton);
            break;

            case VisualOption.Fatness:
                local thinFatnessButton = GUI.Button(anx(20), any(150), anx(170), any(80), "HK_BUTTON.TGA", "Chudzielec", bucket.windowOperation);
                local skinnyFatnessButton = GUI.Button(anx(210), any(150), anx(170), any(80), "HK_BUTTON.TGA", "Chudy", bucket.windowOperation);
                local normalFatnessButton = GUI.Button(anx(20), any(250), anx(170), any(80), "HK_BUTTON.TGA", "Normalny", bucket.windowOperation);
                local muscularFatnessButton = GUI.Button(anx(210), any(250), anx(170), any(80), "HK_BUTTON.TGA", "Uminiony", bucket.windowOperation);
                local thickFatnessButton = GUI.Button(anx(20), any(350), anx(170), any(80), "HK_BUTTON.TGA", "Pulchny", bucket.windowOperation);
                local fatFatnessButton = GUI.Button(anx(210), any(350), anx(170), any(80), "HK_BUTTON.TGA", "T�usty", bucket.windowOperation);

                thinFatnessButton.bind(EventType.Click, function(element) {VisualChange.changeVisual(VisualOption.Fatness, PlayerFatness.Thin);});
                skinnyFatnessButton.bind(EventType.Click, function(element) {VisualChange.changeVisual(VisualOption.Fatness, PlayerFatness.Skinny);});
                normalFatnessButton.bind(EventType.Click, function(element) {VisualChange.changeVisual(VisualOption.Fatness, PlayerFatness.Normal);});
                muscularFatnessButton.bind(EventType.Click, function(element) {VisualChange.changeVisual(VisualOption.Fatness, PlayerFatness.Muscular);});
                thickFatnessButton.bind(EventType.Click, function(element) {VisualChange.changeVisual(VisualOption.Fatness, PlayerFatness.Thick);});
                fatFatnessButton.bind(EventType.Click, function(element) {VisualChange.changeVisual(VisualOption.Fatness, PlayerFatness.Fat);});

                bucket.operations.append(thinFatnessButton);bucket.operations.append(skinnyFatnessButton);bucket.operations.append(normalFatnessButton);
                bucket.operations.append(muscularFatnessButton);bucket.operations.append(thickFatnessButton);bucket.operations.append(fatFatnessButton);
            break;

            case VisualOption.Walking:
                lock = getPlayerPosition(heroId);
                playAni(heroId, "S_WALKL");

                local normalWalkingButton = GUI.Button(anx(20), any(80), anx(170), any(50), "HK_BUTTON.TGA", "Normalny", bucket.windowOperation);
                local tiredWalkingButton = GUI.Button(anx(210), any(80), anx(170), any(50), "HK_BUTTON.TGA", "Zm�czony", bucket.windowOperation);
                local womanWalkingButton = GUI.Button(anx(20), any(130), anx(170), any(50), "HK_BUTTON.TGA", "Kobieta", bucket.windowOperation);
                local armyWalkingButton = GUI.Button(anx(210), any(130), anx(170), any(50), "HK_BUTTON.TGA", "Wojsko", bucket.windowOperation);
                local lazyWalkingButton = GUI.Button(anx(20), any(180), anx(170), any(50), "HK_BUTTON.TGA", "Wyluzowany", bucket.windowOperation);
                local arrogantWalkingButton = GUI.Button(anx(210), any(180), anx(170), any(50), "HK_BUTTON.TGA", "Arogancki", bucket.windowOperation);
                local mageWalkingButton = GUI.Button(anx(20), any(230), anx(170), any(50), "HK_BUTTON.TGA", "Mag", bucket.windowOperation);
                local pocketsWalkingButton = GUI.Button(anx(210), any(230), anx(170), any(50), "HK_BUTTON.TGA", "Z�odziej", bucket.windowOperation);
                local tired2WalkingButton = GUI.Button(anx(20), any(280), anx(170), any(50), "HK_BUTTON.TGA", "Zm�czony v2", bucket.windowOperation);
                local arrogance2WalkingButton = GUI.Button(anx(210), any(280), anx(170), any(50), "HK_BUTTON.TGA", "Arogancki v2", bucket.windowOperation);
                local mage2WalkingButton = GUI.Button(anx(20), any(330), anx(170), any(50), "HK_BUTTON.TGA", "Mag v2", bucket.windowOperation);
                local relaxed2WalkingButton = GUI.Button(anx(210), any(330), anx(170), any(50), "HK_BUTTON.TGA", "Wyluzowany v2", bucket.windowOperation);

                normalWalkingButton.bind(EventType.Click, function(element) {VisualChange.changeVisual(VisualOption.Walking, PlayerWalk.Normal);});
                tiredWalkingButton.bind(EventType.Click, function(element) {VisualChange.changeVisual(VisualOption.Walking, PlayerWalk.Tired);});
                womanWalkingButton.bind(EventType.Click, function(element) {VisualChange.changeVisual(VisualOption.Walking, PlayerWalk.Woman);});
                armyWalkingButton.bind(EventType.Click, function(element) {VisualChange.changeVisual(VisualOption.Walking, PlayerWalk.Army);});
                lazyWalkingButton.bind(EventType.Click, function(element) {VisualChange.changeVisual(VisualOption.Walking, PlayerWalk.Lazy);});
                arrogantWalkingButton.bind(EventType.Click, function(element) {VisualChange.changeVisual(VisualOption.Walking, PlayerWalk.Arrogant);});
                mageWalkingButton.bind(EventType.Click, function(element) {VisualChange.changeVisual(VisualOption.Walking, PlayerWalk.Mage);});
                pocketsWalkingButton.bind(EventType.Click, function(element) {VisualChange.changeVisual(VisualOption.Walking, PlayerWalk.Pockets);});
                tired2WalkingButton.bind(EventType.Click, function(element) {VisualChange.changeVisual(VisualOption.Walking, PlayerWalk.OldTired);});
                arrogance2WalkingButton.bind(EventType.Click, function(element) {VisualChange.changeVisual(VisualOption.Walking, PlayerWalk.OldArrogant);});
                mage2WalkingButton.bind(EventType.Click, function(element) {VisualChange.changeVisual(VisualOption.Walking, PlayerWalk.OldMage);});
                relaxed2WalkingButton.bind(EventType.Click, function(element) {VisualChange.changeVisual(VisualOption.Walking, PlayerWalk.OldLazy);});

                bucket.operations.append(normalWalkingButton);bucket.operations.append(tiredWalkingButton);bucket.operations.append(womanWalkingButton);
                bucket.operations.append(armyWalkingButton);bucket.operations.append(lazyWalkingButton);bucket.operations.append(arrogantWalkingButton);
                bucket.operations.append(mageWalkingButton);bucket.operations.append(pocketsWalkingButton);bucket.operations.append(tired2WalkingButton);
                bucket.operations.append(arrogance2WalkingButton);bucket.operations.append(mage2WalkingButton);bucket.operations.append(relaxed2WalkingButton);
            break;
        }

        bucket.windowOperation.setVisible(true);
    }

    changeVisual = function(type, value)
    {
        local getVisual = getPlayerVisual(heroId);
        switch(type)
        {
            case VisualOption.Body:
                setPlayerVisual(heroId, value, getVisual.bodyTxt, getVisual.headModel, getVisual.headTxt);
            break;
            case VisualOption.Skin:
                setPlayerVisual(heroId, getVisual.bodyModel, value, getVisual.headModel, getVisual.headTxt);
            break;
            case VisualOption.Head:
                setPlayerVisual(heroId, getVisual.bodyModel, getVisual.bodyTxt, value, getVisual.headTxt);
            break;
            case VisualOption.Face:
                setPlayerVisual(heroId, getVisual.bodyModel, getVisual.bodyTxt, getVisual.headModel, value);
            break;
            case VisualOption.Size:
                setPlayerHeight(heroId, value);
            break;
            case VisualOption.Fatness:
                setPlayerFatness(heroId, value);
            break;
            case VisualOption.Walking:
                setPlayerWalkingStyle(heroId, value);
            break;
        }
        getVisual = getPlayerVisual(heroId);
        PlayerPacket().sendVisual(getVisual.bodyModel, getVisual.bodyTxt, getVisual.headModel, getVisual.headTxt);
    }

    destroyWindow = function ()
    {
        foreach(element in bucket)
        {
            if(element instanceof GUI.Input || element instanceof GUI.Button || element instanceof GUI.Texture || element instanceof GUI.CheckBox || element instanceof GUI.ScrollBar)
                element.setVisible(false);
        }

        Bind.remove(bucket.bindForScrollbar);

        bucket.window.destroyAll();
        bucket.windowOperation.destroyAll();
    },

    start = function () {
        BaseGUI.show();
        ActiveGui = PlayerGUI.Visual;

        bucket = prepareWindow();
        bucket.window.setVisible(true);
        bucket.scrollBar.setVisible(true);
        bucket.scrollBar.setValue(getPlayerAngle(heroId).tointeger());
    },

    hide = function () {
        BaseGUI.hide();
        ActiveGui = null;

        lock = false;
        stopAni(heroId);

        destroyWindow();
        bucket = null;
    }
}


addEventHandler("onRender", function() {
    if(lock != false)
    {
        if(getPlayerAni(heroId) != "S_WALKL")
            playAni(heroId, "S_WALKL");

        setPlayerPosition(heroId, lock.x, lock.y, lock.z);
    }
})

addEventHandler("onForceCloseGUI", function() {
    if(ActiveGui == PlayerGUI.Visual)
        VisualChange.hide();
});
Bind.addKey(KEY_ESCAPE, VisualChange.hide.bindenv(VisualChange), PlayerGUI.Visual)

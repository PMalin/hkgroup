

function showFreeMenu(params = null) {
    BaseGUI.show();
    ActiveGui = PlayerGUI.Free;
    setFreeze(false);
    Camera.setFreeze(false);
}

function hideFreeMenu() {
    BaseGUI.hide();
    ActiveGui = null;
}

Bind.addKey(KEY_F7, showFreeMenu)
Bind.addKey(KEY_F7, hideFreeMenu, PlayerGUI.Free)
Bind.addKey(KEY_ESCAPE, hideFreeMenu, PlayerGUI.Free)

addCommand("hud", showFreeMenu);


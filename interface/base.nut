BaseGUI <- {};
ActiveGui <- null;

_setFreeze <- setFreeze;
isGUIInputActive <- false;

Resolution <- getResolution();

enableEvent_RenderFocus(true);

function BaseGUI::normalize(bucket)
{
    local result = {};

    foreach(key, value in bucket)
    {
        if(value instanceof GUI.Input)
            result[key] <- value.getText();
        if(value instanceof GUI.CheckBox)
            result[key] <- value.getChecked();
    }
    return result;
}

function BaseGUI::show(freezeCharacter = true, freezeCamera = true) {
    Chat.hide();
    Camera.setFreeze(freezeCamera);

    setCursorTxt("HK_CURSOR.TGA");
    setCursorSize(anx(32),any(32));
    setCursorPosition(4000, 4000);

    setHudMode(HUD_ALL, HUD_MODE_HIDDEN);

    for(local i = 0; i<200; i++)
      disableKey(i, true);

    setFreeze(freezeCharacter)
    setCursorVisible(true);
    callEvent("onOpenGUI");
}

function BaseGUI::hide() {
    disableKey(KEY_1, false);
    disableKey(KEY_2, false);
    disableKey(KEY_3, false);
    disableKey(KEY_4, false);
    disableKey(KEY_5, false);
    disableKey(KEY_6, false);
    disableKey(KEY_7, false);
    disableKey(KEY_8, false);
    disableKey(KEY_9, false);
    disableKey(KEY_0, false);
    Chat.show();
    Camera.setFreeze(false);
    setFreezeWithCoolDown(false);
    setCursorVisible(false);
    callEvent("onCloseGUI");
}

local timerToFreeze = null;

function setFreeze(value) {
    _setFreeze(value);

    if(timerToFreeze != null) {
        killTimer(timerToFreeze);
        timerToFreeze = null;
    }
}

function setFreezeWithCoolDown(value) {
    timerToFreeze = setTimer(function() {
        _setFreeze(value);
        timerToFreeze = null;
    }, 600, 1);
}

addEventHandler("onDamage", function(dmg, type) {
    if(ActiveGui != null)
        callEvent("onForceCloseGUI")
})

addEventHandler("onPlayerHit", function(killerId, playerId, dmg) {
    if(ActiveGui != null && (killerId == heroId || playerId == heroId))
        callEvent("onForceCloseGUI")
})

addEventHandler("GUI.onInputActive", function(obj) {
    isGUIInputActive = true
})

addEventHandler("GUI.onInputDeactive", function(obj) {
    isGUIInputActive = false
})

local focusPlayer = -1;

addEventHandler("onFocus", function (new, old) {
    focusPlayer = new;
})

function getPlayerFocus(pid = heroId) {
    if(pid == heroId)
        return focusPlayer;

    return -1;
}

local draw = Draw(0,0,"");

function getTestDraw(){
    return draw;
}

addEventHandler("GUI.onMouseIn", function(element) {
    if(element instanceof GUI.Button || element instanceof GUI.CheckBox || element instanceof GUI.Input || element instanceof MyGUI.DropDown || element instanceof MyGUI.SelectList)
        setCursorTxt("HK_CURSOR_HOVER.TGA");

    if(element instanceof GUI.Button && element.getFile() == "HK_BUTTON.TGA")
        element.setFile("HK_BUTTON_HOVER.TGA");
})

addEventHandler("GUI.onMouseOut", function(element) {
    if(element instanceof GUI.Button || element instanceof GUI.CheckBox || element instanceof GUI.Input || element instanceof MyGUI.DropDown || element instanceof MyGUI.SelectList)
        setCursorTxt("HK_CURSOR.TGA");

    if(element instanceof GUI.Button && element.getFile() == "HK_BUTTON_HOVER.TGA")
        element.setFile("HK_BUTTON.TGA");
})

function changeSpecialCharToNumber(letter) {
    local tab = {
        "!": "1",
        "@": "2",
        "#": "3",
        "$": "4",
        "%": "5",
        "^": "6",
        "&": "7",
        "*": "8",
        "(": "9",
        ")": "0",
    }

    if(letter in tab)
        return tab[letter];

    return letter;
}
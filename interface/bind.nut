
Bind <- {};
Bind.arrayBinds <- [];
Bind.nextId <- 0;

function Bind::addClick(_button, _func, _playerActiveGUI = null, _parameters = [])
{
    Bind.nextId = Bind.nextId + 1;

    Bind.arrayBinds.append({
        id = Bind.nextId
        buttonId = _button,
        func = _func,
        playerActiveGUI = _playerActiveGUI,
        parameters = _parameters
    })

    return Bind.nextId;
}

function Bind::addKey(_keyId, _func, _playerActiveGUI = null, _parameters = [])
{
    Bind.nextId = Bind.nextId + 1;

    Bind.arrayBinds.append({
        id = Bind.nextId
        keyId = _keyId,
        func = _func,
        playerActiveGUI = _playerActiveGUI,
        parameters = _parameters
    })

    return Bind.nextId;
}

function Bind::onChange(_element, _func, _playerActiveGUI = null, _parameters = [])
{
    Bind.nextId = Bind.nextId + 1;

    Bind.arrayBinds.append({
        id = Bind.nextId
        element = _element,
        func = _func,
        playerActiveGUI = _playerActiveGUI,
        parameters = _parameters
    })

    return Bind.nextId;
}

function Bind::remove(indexInArray)
{
    foreach(bindId, bind in Bind.arrayBinds)
    {
        if(indexInArray == bind.id)
        {
            Bind.arrayBinds.remove(bindId);
            return;
        }
    }
}

local function keyHandler(key)
{
    if (chatInputIsOpen())
		return

	if (isConsoleOpen())
		return

    if (isGUIInputActive)
        return;

    foreach(bind in Bind.arrayBinds)
    {
        if(!("keyId" in bind))
            continue;

        local checkKey = bind.keyId;

        if(typeof bind.keyId == "string")
            checkKey = Account.keyBinds[bind.keyId];

        if(checkKey == key)
        {
            if(bind.playerActiveGUI == null && ActiveGui == null)
            {
                bind.func();
                return;
            }

            if(bind.playerActiveGUI == ActiveGui)
            {
                bind.func();
                return;
            }
        }
    }
}

addEventHandler("onKey", keyHandler)

local function onClickObject(obj)
{
    foreach(bind in Bind.arrayBinds)
    {
        if(!("buttonId" in bind))
            continue;

        if(bind.buttonId == obj)
        {
            if(bind.playerActiveGUI == null && ActiveGui == null)
            {
                if(bind.parameters.len() > 0)
                    bind.func.acall(bind.parameters);
                else
                    bind.func();

                return;
            }

            if(bind.playerActiveGUI == ActiveGui)
            {
                if(bind.parameters.len() > 0)
                    bind.func.acall(bind.parameters);
                else
                    bind.func();
                return;
            }
        }
    }
}

addEventHandler("GUI.onClick", onClickObject);

local function onChangeObject(obj)
{
    foreach(bind in Bind.arrayBinds)
    {
        if(!("element" in bind))
            continue;

        if(bind.element == obj)
        {
            if(bind.playerActiveGUI == null && ActiveGui == null)
            {
                bind.func(obj.getValue());
                return;
            }

            if(bind.playerActiveGUI == ActiveGui)
            {
                bind.func(obj.getValue());
                return;
            }
        }
    }
}

addEventHandler("GUI.onChange", onChangeObject);
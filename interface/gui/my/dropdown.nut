local DropDowns = [];

class MyGUI.DropDown
{
    background = null
    selector = null
    scroll = null

	list = null
    items = null

    value = null
    onChange = null
    multiple = false
    placeholder = ""
    label = ""

    opened = false
    visible = false

    constructor(x,y,width,height,label,content,placeholder, valueStarted = null,multiple = false)
    {
        this.list = content;
        this.items = [];

        for(local i = 0; i <= 4; i ++)
            items.append(GUI.Button(x+anx(15), y + height + 30 + any(50 * i), width - anx(60), any(45), "HK_BUTTON.TGA", ""));

        this.scroll = GUI.ScrollBar(x + width - anx(40), y + height + 30, anx(30), any(260), "HK_SCROLL.TGA", "HK_INDICATOR.TGA", "HK_U.TGA", "HK_O.TGA", Orientation.Vertical)
        this.selector = GUI.Button(x,y,width,height,"HK_BOX.TGA","");
        this.background = GUI.Texture(x,y+height + 10,width, any(300), "BLACK.TGA");
        this.label = Draw(x + 20, y - 120, label);

        this.value = [];
        this.multiple = multiple;
        this.placeholder = placeholder;
        this.onChange = null;

        if(valueStarted != null)
        {
            if(typeof(valueStarted) == "array") {
                foreach(val in valueStarted)
                    this.value.append(val);
            }else
                this.value.append(valueStarted);
        }

        this.opened = false;
        this.visible = false;

        DropDowns.append(this);
    }

    function setVisible(_value) {
        visible = _value;

        visible ? show() : hide();
    }

    function show() {
        opened = false;
        selector.setVisible(true);
        selector.setText(getLabel());
        background.setVisible(false);
        label.visible = true;

        foreach(item in items)
            item.setVisible(false);
    }

    function hide() {
        opened = false;
        background.setVisible(false);
        selector.setVisible(false);
        scroll.setVisible(false);
        label.visible = false;

        foreach(item in items)
            item.setVisible(false);
    }

    function setScale(x,y) {
        label.setScale(x,y);
    }

    function toggle() {
        opened ? close() : open();
    }

    function open() {
        opened = true;
        background.setVisible(true);
        background.setAlpha(0);
        label.setColor(150,100,250);

        if(list.len() > 5) {
			scroll.setVisible(true);
			scroll.setMaximum(list.len() - 5);
		}else
			scroll.setVisible(false);

		scroll.top();
        updateItems();
    }

    function close() {
        opened = false;
        background.setVisible(false);
        scroll.setVisible(false);
        label.setColor(255, 255, 255);

		foreach(item in items)
            item.setVisible(false);

        selector.setText(getLabel());
    }

    function setContent(newContent) {
        list = newContent;
        value.clear();
    }

    function getLabel() {
        local str = "";

        foreach(item in list)
        {
            if(value.find(item.id) != null)
                str += item.name+",";
        }

        if(str.len() > 1)
            str = str.slice(0, -1);
        else
            str = placeholder;

        return str;
    }

    function getPosition() {
        return background.getPosition();
    }

	function updateItems(alpha = 0) {

		foreach(item in items)
			item.setVisible(false);

		if(scroll.visible)
		{
			local valueScroll = scroll.getValue();
			local i = 0;
			foreach(ind, option in list) {
				if(i >= valueScroll && i < (valueScroll + 5)) {
					items[(i - valueScroll)].setText(option.name);
					items[(i - valueScroll)].setVisible(true);
					items[(i - valueScroll)].attribute = ind;
					items[(i - valueScroll)].setAlpha(alpha);

                    if(value.find(option.id) != null)
                        items[(i - valueScroll)].setColor(150,100,250);
                    else
                        items[(i - valueScroll)].setColor(255, 255, 255);
				}
				i++;
			}
		}
		else
		{
			local i = 0;
			foreach(ind, option in list) {
				items[i].setText(option.name);
				items[i].setVisible(true);
				items[i].attribute = ind;
				items[i].setAlpha(alpha);

                if(value.find(option.id) != null)
                    items[i].setColor(150,100,250);
                else
                    items[i].setColor(255, 255, 255);

				i++;
			}
		}
	}

	function click(element) {
        if(element == selector)
            toggle();
        else
        {
            foreach(item in items)
            {
                if(item == element)
                {
                    local idToAdd = list[element.attribute].id;
                    if(multiple == false)
                    {
                        foreach(_item in items)
                            _item.setColor(255, 255, 255);

                        local alredyIdSelected = value.len() > 0 ? value[0] : -1;
                        value.clear();

                        if(idToAdd != alredyIdSelected)
                        {
                            value.append(idToAdd);
                            element.setColor(150,100,250);
                        }
                    }else{
                        local indexOption = value.find(idToAdd);
                        if(indexOption == null)
                        {
                            value.append(idToAdd);
                            element.setColor(150,100,250);
                        }else{
                            value.remove(indexOption);
                            element.setColor(255, 255, 255);
                        }
                    }


                    if(onChange != null)
                        onChange(value);

                    if(multiple == false)
                        close();
                    else
                        selector.setText(getLabel());
                }
            }
        }
	}

    function getValue()
    {
        if(multiple)
            return value;
        else {
            if(value.len() == 0)
                return null
            else
                return value[0]
        }
    }

    function render() {
        if(scroll.getAlpha() != 255)
        {
            local value = scroll.getAlpha() + 30;
            if(value > 255)
                value = 255;

            scroll.setAlpha(value);
        }
        if(background.getAlpha() != 255)
        {
            local value = background.getAlpha() + 30;
            if(value > 255)
                value = 255;

            background.setAlpha(value);
        }
        foreach(item in items) {
            if(item.getAlpha() != 255) {
                local value = item.getAlpha() + 30;
                if(value > 255)
                    value = 255;

                item.setAlpha(value);
            }
        }
    }

	function destroy() {
		background.destroy();
		scroll.destroy();
		selector.destroy();

		foreach(item in items)
			item.destroy();

		foreach(index, qdropdown in DropDowns)
			if(qdropdown == this)
                DropDowns.remove(index);
	}

    static function FromTable(tab)
    {
        local ret = [];
        foreach(index, value in tab)
            ret.append({ id = index, name = value })

        return ret;
    }
}

addEventHandler("GUI.onChange", function(element) {
	foreach(index, qdropdown in DropDowns)
		if(qdropdown.visible && qdropdown.opened)
			if(qdropdown.scroll == element)
                qdropdown.updateItems();
});

addEventHandler("GUI.onClick", function(element) {
	foreach(index, qdropdown in DropDowns)
		if(qdropdown.visible)
            qdropdown.click(element);
});

local fps = getTickCount();

addEventHandler("onRender", function() {
	if(fps < getTickCount()) {
		foreach(index, qdropdown in DropDowns)
			if(qdropdown.visible)
                qdropdown.render();

		fps = getTickCount() + 50;
	}
});
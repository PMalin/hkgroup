
local EquipmentInterfaces = [];

class Equipment
{
    bucket = null;
    background = null;
    titleDraw = null;

    movable = false;
    activeShadow = false;

    rows = 0;
    columns = 0;

    top = 0;
    left = 0;

    widthOfItem = 0;
    heightOfItem = 0;

    scroll = null;

    onRightClickObject = null;
    onLeftClickObject = null;
    onMoveOutObject = null;

    constructor(title)
    {
        bucket = [];

        movable = false;
        activeShadow = false;

        rows = 4;
        columns = 5;

        left = 0;
        top = 0;

        widthOfItem = 90;
        heightOfItem = 80;

        background = GUI.Window(0, 0, anx(512), any(512), "HK_BOX.TGA", null, false);
        scroll = GUI.ScrollBar(anx(470), any(50), anx(30), any(440), "HK_SCROLL.TGA", "HK_BUTTON.TGA", "HK_U.TGA", "HK_O.TGA", Orientation.Vertical, background);

        titleDraw = GUI.Draw(0, 0, title, background);
        titleDraw.setColor(255,255,255);
        titleDraw.setFont("FONT_OLD_20_WHITE_HI.TGA");
        titleDraw.setScale(0.8, 0.8);

        EquipmentInterfaces.append(this);

        onRightClickObject = function(obj){}
        onLeftClickObject = function(obj){}
        onMoveOutObject = function(obj){}
    }

    function onScroll()
    {
        showItemsWithIndex();
    }

    function setBucket(equipment)
    {
        foreach(index, item in bucket)
            item.destroy();

        bucket.clear();

        foreach(item in equipment) {
            if(item.amount == 0)
                continue;

            bucket.append(EquipmentItem(item.instance, item.amount, widthOfItem, heightOfItem));
        }
    }

    function hasItem(instance)
    {
        foreach(item in bucket) {
            if(item.instance == instance)
                return item.amount;
        }

        return 0;
    }

    function getBucket()
    {
        return bucket;
    }

    function setVisible(value)
    {
        if(value)
            show();
        else
            hide();
    }

    function mouseDown(obj, button)
    {
        if(!movable)
            return;

        local bucketItem = getBucketObject(obj);
        if(bucketItem == null)
            return;

        if(button == MOUSE_LMB)
        {
            if(activeShadow == false)
                activeShadow = EquipmentShadow(bucketItem);
        }else
            onRightClickObject(bucketItem);
    }

    function mouseUp(obj, button)
    {
        local bucketItem = getBucketObject(obj);
        if(bucketItem == null)
            return;

        switch(button)
        {
            case MOUSE_RMB:
                onRightClickObject(bucketItem);
            break;
        }
    }

    function mouseClick(obj)
    {
        local bucketItem = getBucketObject(obj);
        if(bucketItem == null)
            return;

        switch(obj.m_lastButtonPressed)
        {
            case MOUSE_LMB:
                onLeftClickObject(bucketItem);
            break;
            case MOUSE_RMB:
                onRightClickObject(bucketItem);
            break;
        }
    }

    function onRender()
    {
        if(activeShadow == false)
            return;

        if(isMouseBtnPressed(MOUSE_LMB))
            activeShadow.updatePosition();
        else
            mouseUpTest();
    }

    function show()
    {
        background.setPositionPx(left,top);
        titleDraw.setPositionPx(left + 256 - (titleDraw.getSizePx().width * 0.8)/2, top + 20);
        background.setVisible(true);
        showItemsWithIndex();
    }

    function hide()
    {
        background.setVisible(false);

        foreach(item in bucket)
            item.setVisible(false);

        activeShadow = false;
    }

    function showItemsWithIndex()
    {
        foreach(item in bucket)
            item.setVisible(false);

        local value = scroll.getValue();
        value = value * columns;

        local currentColumn = 0;
        local currentRow = 0;

        foreach(index, item in bucket)
        {
            if(index < value)
                continue;

            if(currentColumn >= columns) {
                currentRow = currentRow + 1;
                currentColumn = 0;
            }

            if(currentRow > rows)
                break;

            local indexLeft = 5 + currentColumn * widthOfItem + 10;
            local indexTop = 40 + currentRow * heightOfItem + 20;
            item.setPosition(left + indexLeft, top + indexTop);

            item.setVisible(true);

            currentColumn = currentColumn + 1;
        }
    }

    function getBucketObject(obj)
    {
        foreach(item in bucket)
            if(item.background == obj)
                return item;

        return null;
    }
}


class EquipmentItem
{
    render = null;

    background = null;
    texture = null;
    draw = null;

    instance = "";
    amount = -1;

    constructor(instance, amount, width, height)
    {
        this.amount = amount;
        this.instance = instance;

        background = GUI.Button(0,0,anx(width), any(height), "HK_BOX.TGA", "");
        render = ItemRender(0, 0,anx(width), any(height), instance);

        if(amount == -1)
            render.visual = instance;

        texture = Texture(0,0,anx(36),any(36),"HK_INPUT.TGA");
        draw = Draw(0,0,amount);
    }

    function setVisible(value)
    {
        background.setVisible(value);
        render.visible = value;

        if(amount != -1) {
            texture.visible = value;
            draw.visible = value;
        }

        render.lightingswell = true;
    }

    function setPosition(x,y)
    {
        x = anx(x);
        y = any(y);

        local size = background.getSizePx();
        background.setPosition(x,y);
        render.setPosition(x,y);
        texture.setPosition(x + anx(size.width - 38), y + any(size.height - 38));
        draw.setPosition(x + anx(size.width - 20) - draw.width/2, y + any(size.height - 30))
    }

    function setAmount(value)
    {
        amount = value;
        draw.text = value;
    }

    function destroy()
    {
        setVisible(false);

        background.destroy();
    }

    function getPosition()
    {
        return background.getPosition();
    }
}

class EquipmentShadow
{
    render = null;

    background = null;
    texture = null;
    draw = null;

    clonedObject = null;
    clonedPositionOb = null;

    constructor(objectToClone)
    {
        clonedObject = objectToClone;

        local width = clonedObject.background.getSize().width;
        local height = clonedObject.background.getSize().height;

        background = Texture(0,0,width, height, "HK_BOX.TGA");
        render = ItemRender(0, 0,width, height, objectToClone.instance);

        texture = Texture(0,0,anx(36),any(36),"HK_BOX.TGA");
        draw = Draw(0,0,objectToClone.amount);

        background.setColor(189, 160, 140);
        background.setAlpha(150);
        texture.setAlpha(150);
        draw.setAlpha(150);

        local clonedPosition = objectToClone.getPosition();
        local cursorPosition = getCursorPosition();

        clonedPositionOb = {
            x = cursorPosition.x - clonedPosition.x,
            y = cursorPosition.y - clonedPosition.y,
        }

        updatePosition();

        background.visible = true;
        render.visible = true;
        texture.visible = true;
        draw.visible = true;
    }

    function updatePosition()
    {
        local nowPositionCursor = getCursorPosition();

        local x = (nowPositionCursor.x - clonedPositionOb.x),
            y = (nowPositionCursor.y - clonedPositionOb.y);

        local size = background.getSizePx();
        background.setPosition(x,y);
        render.setPosition(x,y);
        texture.setPosition(x + anx(size.width - 38), y + any(size.height - 38));
        draw.setPosition(x + anx(size.width - 20) - draw.width/2, y + any(size.height - 30))
    }
}

local function onChangeObject(obj)
{
    foreach(equipmentObj in EquipmentInterfaces)
        if(obj == equipmentObj.scroll)
            equipmentObj.onScroll();
}

local function onMouseDownObject(obj, button)
{
    foreach(equipmentObj in EquipmentInterfaces)
        equipmentObj.mouseDown(obj, button);
}

local function onMouseUpObject(obj, button)
{
    foreach(equipmentObj in EquipmentInterfaces)
        equipmentObj.mouseUp(obj, button);
}

local function onMouseClick(obj)
{
    foreach(equipmentObj in EquipmentInterfaces)
        equipmentObj.mouseClick(obj);
}

local function onRender()
{
    foreach(equipmentObj in EquipmentInterfaces)
        equipmentObj.onRender();
}

addEventHandler("GUI.onChange", onChangeObject);
addEventHandler("GUI.onMouseDown", onMouseDownObject);
addEventHandler("GUI.onMouseUp", onMouseUpObject);
addEventHandler("GUI.onClick", onMouseClick);
addEventHandler("onRender", onRender);
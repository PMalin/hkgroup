local TickStatus =
{
    "Static": 1,
    "FloatUp": 2,
    "FloatDown": 3,
    "Locked": 4
};

VobUIThief <- {
    _vob = null,
    bucket = null,

    lastHitCooldown = 0

    prepareWindow = function()
    {
        local result = {};

        result.ticks <- {};

        result.window <- GUI.Window(anx(Resolution.x/2 - 100), any(Resolution.y/2 - 128), anx(668), any(334), "HK_LOCKPICK_BG.TGA", null, false);
        GUI.Texture(anx(5), any(0), anx(180), any(334), "HK_LOCKPICK_BORDER.TGA", result.window);
        GUI.Texture(anx(510), any(0), anx(180), any(334), "HK_LOCKPICK_BORDER.TGA", result.window);
        GUI.Texture(anx(30), any(120), anx(90), any(196), "HK_LOCKPICK_BORDER.TGA", result.window);
        result.lockpick <- Texture(anx(Resolution.x/2 - 700), any(Resolution.y/2 + 50), anx(756), any(96), "HK_LOCKPICK.TGA");

        for(local i = 0; i < _vob.complexity; i++)
            result.ticks[i] <- VobUIThief.Tick(i);

        return result;
    }

    destroyWindow = function() {
        foreach(element in bucket)
            if(element instanceof GUI.Input || element instanceof GUI.Button || element instanceof GUI.Texture || element instanceof GUI.CheckBox)
                element.setVisible(false);

        bucket.window.destroyAll();
    }

    success = function() {
        VobController.cachedOpeners[VobUIThief._vob.id] <- getTickCount() + (1000 * 120);
    }

    show = function() {
        if(bucket == null)
            bucket = prepareWindow();

        bucket.window.setVisible(true);
        bucket.lockpick.visible = true;

        foreach(tick in bucket.ticks)
            tick.top();

        setCursorPosition(anx(Resolution.x/2 - 350), any(Resolution.y/2 + 80));

        BaseGUI.show();
        ActiveGui = PlayerGUI.ThiefVob;
        playAni(heroId, "S_SCHRANK_S0");
        setCursorSize(anx(0),any(0));
    }

    hide = function() {
        destroyWindow();
        bucket = null;

        BaseGUI.hide();
        ActiveGui = null;
        playAni(heroId,"S_RUN");
        setCursorSize(anx(32),any(32));
    }

    breakStick = function() {
        VobPacket(_vob).wrongVobComplexity();

        if(hasItem("ITKE_LOCKPICK") == 1)
            hide();
    }

    onMouseMove = function(x,y) {
        if(ActiveGui != PlayerGUI.ThiefVob)
            return;

        local pos = bucket.lockpick.getPosition();
        pos.x = pos.x + y * 3;
        pos.y = pos.y + x * 3;

        local minX = anx(Resolution.x/2 - 800);
        local maxX = anx(Resolution.x/2 - 280);

        if(pos.x > maxX)
            pos.x = maxX;

        if(pos.x < minX)
            pos.x = minX;

        local minY = any(Resolution.y/2 + 30);
        local maxY = any(Resolution.y/2 + 90);

        if(pos.y > maxY)
            pos.y = maxY;

        if(pos.y < minY) {
            local hubX = pos.x + anx(752);
            local hubY = pos.y;

            foreach(tick in bucket.ticks) {
                if(tick.tryToTick(hubX, hubY))
                    tick.changeStatus(TickStatus.FloatUp);
            }

            pos.y = maxY;
            playAni(heroId, "t_CHESTSMALL_S0_PickRight");
        }

        bucket.lockpick.setPosition(pos.x, pos.y);
    }

    onRender = function() {
        if(ActiveGui != PlayerGUI.ThiefVob)
            return;

        foreach(tick in bucket.ticks)
            tick.onRender();
    }

    onMouseClick = function(btn) {
        if(ActiveGui != PlayerGUI.ThiefVob)
            return;

        if(lastHitCooldown > getTickCount()) {
            breakStick();
            return;
        }

        lastHitCooldown = getTickCount() + 300;

        if(btn == MOUSE_LMB) {
            local everyFit = true;
            foreach(tick in bucket.ticks) {
                tick.onMouseClick();

                if(tick.status != TickStatus.Locked)
                    everyFit = false;
            }

            if(everyFit) {
                VobController.cachedOpeners[_vob.id] <- getTickCount() + (1000 * 120);
                hide();
            }
        }
    }
}

addEventHandler ("onMouseMove", VobUIThief.onMouseMove.bindenv(VobUIThief))
addEventHandler ("onRender", VobUIThief.onRender.bindenv(VobUIThief))
addEventHandler ("onMouseClick", VobUIThief.onMouseClick.bindenv(VobUIThief))
Bind.addKey(KEY_ESCAPE, VobUIThief.hide.bindenv(VobUIThief), PlayerGUI.ThiefVob)


class VobUIThief.Tick
{
    bg = null;
    tick = null;

    status = -1;
    lastRender = 0;
    info = null;

    constructor(_index) {
        bg = Texture(anx(Resolution.x/2 + 30 + _index * 70), any(Resolution.y/2 - 130),anx(70),any(180),"HK_LOCKPICK_SEGMENT.TGA");
        tick = Texture(anx(Resolution.x/2 + 40 + _index * 70), any(Resolution.y/2 - 90),anx(50),any(140),"HK_LOCKPICK_TICK.TGA");

        bg.visible = true;
        tick.visible = true;

        status = TickStatus.Static;
        lastRender = getTickCount();

        info = {};
    }

    function tryToTick(x,y) {
        if(status != TickStatus.Static)
            return;

        local position = tick.getPosition();
        local size = tick.getSize();
        local isIn = (x >= position.x && x <= position.x + size.width && y >= position.y && y <= position.y + size.height);
        return isIn;
    }

    function changeStatus(newStatus) {
        status = newStatus;

        switch(status) {
            case TickStatus.FloatUp:case TickStatus.FloatDown:
                local speed = getPlayerDexterity(heroId) + Hero.skills[PlayerSkill.Thief] * 25 + irand(50);
                speed = irand(50) + (300 - speed);
                speed = (speed.tofloat()/100.0);

                info = {
                    speed = speed,
                    progress = 0.0,
                    spot = false
                }
            break;
            case TickStatus.Locked:
                info = {
                    timeout = getTickCount() + 1000 * (5 + abs(getPlayerDexterity(heroId)/7))
                }
            break;
        }
    }

    function onMouseClick() {
        switch(status) {
            case TickStatus.FloatUp:case TickStatus.FloatDown:
                if(info.spot == false)
                    return;

                tick.setSize(anx(50),any(40));
                changeStatus(TickStatus.Locked);
            break;
        }
    }

    function onRender() {
        local diff = getTickCount() - lastRender;
        switch(status) {
            case TickStatus.FloatUp:
                info.progress = info.progress + info.speed * diff;
                local xSpot = abs(info.progress/10);
                tick.setSize(anx(50),any(140 - xSpot));

                if(xSpot > 85 && xSpot < 100)
                    info.spot = true;
                else if(xSpot >= 100)
                    changeStatus(TickStatus.FloatDown);
            break;
            case TickStatus.FloatDown:
                info.progress = info.progress + info.speed * diff;
                local xSpot = abs(info.progress/10);
                tick.setSize(anx(50),any(40 + xSpot));

                if(xSpot > 15 && xSpot < 100)
                    info.spot = false;
                else if(xSpot >= 100)
                    changeStatus(TickStatus.Static);
                else
                    info.spot = true;
            break;
            case TickStatus.Locked:
                if(getTickCount() > info.timeout)
                    changeStatus(TickStatus.FloatDown);
            break;
        }

        lastRender = getTickCount();
    }

    function top() {
        tick.top();
        bg.top();
    }
}

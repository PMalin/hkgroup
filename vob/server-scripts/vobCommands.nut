addCommandAdmin("vob_key_configure", function (playerId, params) {
    local args = sscanf("dd", params);
    if (!args)
    {
        sendMessageToPlayer(playerId, 246, 10, 19, ChatType.ADMIN+"Wpisz /vob_key_configure <id voba> <od 1 - 6>");
        return;
    }

    local vobId = args[0], vobs = VobController.vobs;
    if(!(vobId in vobs)) {
        sendMessageToPlayer(playerId, 246, 10, 19, ChatType.ADMIN+"Vob o takim id nie istnieje "+config);
        return;
    }

    local vob = vobs[vobId];
    local config = args[1];

    vob.setComplexity(config);
    VobPacket(vob).setComplexity();

    sendMessageToPlayer(playerId, 9, 245, 9, ChatType.ADMIN+"Ustawiono konfiguracj� "+config);
}, "Komenda pozwalaj�ca ustawi� konfiguracj� otwierania dla voba.", PlayerRole.Support);

addCommandAdmin("vob_guid", function (playerId, params) {
    local args = sscanf("ds", params);
    if (!args)
    {
        sendMessageToPlayer(playerId, 246, 10, 19, ChatType.ADMIN+"Wpisz /vob_guid <id voba> <new guid>");
        return;
    }

    local vobId = args[0], vobs = VobController.vobs;
    if(!(vobId in vobs)) {
        sendMessageToPlayer(playerId, 246, 10, 19, ChatType.ADMIN+"Vob o takim id nie istnieje "+config);
        return;
    }

    local vob = vobs[vobId];
    local config = args[1];

    vob.setGuid(config);

    sendMessageToPlayer(playerId, 9, 245, 9, ChatType.ADMIN+"Ustawiono guid "+config);
}, "Komenda pozwalaj�ca ustawi� konfiguracj� otwierania dla voba.", PlayerRole.Support);

addCommandAdmin("vob_position", function (playerId, params) {
    local args = sscanf("dfff", params);
    if (!args)
    {
        sendMessageToPlayer(playerId, 246, 10, 19, ChatType.ADMIN+"Wpisz /vob_position <id voba> <x> <y> <z>");
        return;
    }

    local vobId = args[0], vobs = VobController.vobs;
    if(!(vobId in vobs)) {
        sendMessageToPlayer(playerId, 246, 10, 19, ChatType.ADMIN+"Vob o takim id nie istnieje "+config);
        return;
    }

    local vob = vobs[vobId];
    vob.setPosition(args[1], args[2], args[3]);
    VobPacket(vob).rebuild();

    sendMessageToPlayer(playerId, 9, 245, 9, ChatType.ADMIN+"Ustawiono pozycj� dla voba.");
}, "Komenda pozwalaj�ca ustawi� pozycj� dla voba.", PlayerRole.Support);


addCommandAdmin("vob_delete", function (playerId, params) {
    local args = sscanf("d", params);
    if (!args)
    {
        sendMessageToPlayer(playerId, 246, 10, 19, ChatType.ADMIN+"Wpisz /vob_delete <id voba>");
        return;
    }

    local vobId = args[0], vobs = VobController.vobs;
    if(!(vobId in vobs)) {
        sendMessageToPlayer(playerId, 246, 10, 19, ChatType.ADMIN+"Vob o takim id nie istnieje "+config);
        return;
    }

    local vob = vobs[vobId];

    Query().deleteFrom(GameVob.VobTable).where(["id = "+vobId]).execute();
    VobPacket(vobs[vobId]).remove();
    vobs.rawdelete(vobId);
    callEvent("onVobDestroy", vobId);

    sendMessageToPlayer(playerId, 9, 245, 9, ChatType.ADMIN+"Usuni�to "+vobId);
}, "Komenda pozwalaj�ca usun�� voba.", PlayerRole.Support);

addCommandAdmin("vob_info", function (playerId, params) {
    local args = sscanf("d", params);
    if (!args)
    {
        sendMessageToPlayer(playerId, 246, 10, 19, ChatType.ADMIN+"Wpisz /vob_info <id voba>");
        return;
    }

    local vobId = args[0], vobs = VobController.vobs;
    if(!(vobId in vobs)) {
        sendMessageToPlayer(playerId, 246, 10, 19, ChatType.ADMIN+"Vob o takim id nie istnieje "+config);
        return;
    }

    local vob = vobs[vobId];

    sendMessageToPlayer(playerId, 9, 245, 9, ChatType.ADMIN+"Info odno�nie voba: "+vobId);
    sendMessageToPlayer(playerId, 9, 245, 9, ChatType.ADMIN+"Typ voba: "+Config["VobType"][vob.type] + " operator voba " + vob.operationPlayer);
    sendMessageToPlayer(playerId, 9, 245, 9, ChatType.ADMIN+"�ycie voba: "+Config["VobHealth"][vob.hpMode] + " -- "+vob.hp+"/"+vob.hpMax);
    sendMessageToPlayer(playerId, 9, 245, 9, ChatType.ADMIN+"Maks waga: "+vob.weight +" Klucz: "+vob.guid+ " Skomplikowanie klucza: " +vob.complexity);
    sendMessageToPlayer(playerId, 9, 245, 9, ChatType.ADMIN+"Dynamiczny: "+( vob.dynamic ? "Tak" : "Nie" ) +" Wygl�d: "+vob.visual+ " Nazwa: " +vob.name);
    if(vob.inventory != null) {
        sendMessageToPlayer(playerId, 9, 245, 9, ChatType.ADMIN+"Itemy:");

        foreach(_item in vob.inventory.getItems())
            sendMessageToPlayer(playerId, 9, 245, 9, ChatType.ADMIN+" Item: "+_item.name+" "+_item.amount);
    }
}, "Komenda pozwalaj�ca sprawdzi� voba.", PlayerRole.Support);


class GameVob
{
    static VobTable = "vob";

    synchronized = false

    id = -1
    type = -1

    hpMode = -1
    hp = -1
    hpMax = -1

    weight = -1
    operationPlayer = -1
    guid = -1
    opened = 0

    movable = -1
    movablePlayer = -1

    alpha = -1
    dynamic = false
    visual = ""
    name = ""

    inventory = null
    position = null
    rotation = null

    targetPosition = null
    targetRotation = null

    trigger = -1
    triggerType = -1
    triggerTime = -1
    triggerAnimation = -1

    lastUsedVob = -1
    complexity = 0

    createdBy = "";

    constructor(_id)
    {
        synchronized = false

        id = _id
        type = VobType.Static;

        hpMode = VobHealth.Static;
        hp = -1
        hpMax = -1
        opened = 0

        movable = VobMovable.Static;
        movablePlayer = -1

        alpha = 1.0
        dynamic = false;
        visual = "";
        name = "";
        lastUsedVob = -1;

        weight = 0.0
        operationPlayer = -1
        guid = ""

        inventory = null

        trigger = 0
        triggerType = 0
        triggerAnimation = 0
        triggerTime = 0

        complexity = 3

        position = {x = 0, y = 0, z = 0}
        rotation = {x = 0, y = 0, z = 0}

        targetPosition = {x = 0, y = 0, z = 0}
        targetRotation = {x = 0, y = 0, z = 0}

        createdBy = "";
    }

    // Set attribute
    function setPosition(x,y,z)
    {
        synchronized = false;
        position = {x = x, y = y, z = z}
        save();
    }

    function setRotation(x,y,z)
    {
        synchronized = false;
        rotation = {x = x, y = y, z = z}
        save();
    }

    function setDynamic(playerId)
    {
        synchronized = false;
        save();
    }

    // Movable functions
    function setMovable(playerId) {
        if(movablePlayer == -1) {
            if(operationPlayer != -1)
                return;

            movablePlayer = playerId;
            VobController.movablePlayers[playerId] <- id;

            VobPacket(this).setMovable();
            applyPlayerOverlay(playerId, Mds.id("HUMANS_CARRYBARREL.MDS"));
        } else {
            if(movablePlayer == playerId) {
                VobController.movablePlayers.rawdelete(playerId);
                movablePlayer = -1;
                VobPacket(this).setMovable();

                removePlayerOverlay(playerId, Mds.id("HUMANS_CARRYBARREL.MDS"));
            }
        }
    }

    // Container operations
    function setOperation(playerId) {
        if(operationPlayer == -1) {
            if(movablePlayer != -1) {
                VobPacket(this).operation(false, playerId);
                return;
            }

            operationPlayer = playerId;
            VobPacket(this).operation(true, playerId);
            // send bucket with items

            if(inventory != null)
                inventory.open(playerId);
        } else {
            if(operationPlayer == playerId) {
                operationPlayer = -1;
                // save bucket with items
                if(inventory != null) {
                    inventory.save();
                    inventory.close(playerId);
                }

                VobPacket(this).operation(false, playerId);
            } else {
                VobPacket(this).operation(false, playerId);
            }
        }
    }

    // on player exit callback
    function onPlayerExit(playerId) {
        if(movablePlayer == playerId) {
            local pos = getPlayerPosition(playerId);
            local angle = getPlayerAngle(playerId);

            pos.y = pos.y - 65.50;
            pos.x = pos.x + (sin(angle * 3.14 / 180.0) * 50);
            pos.z = pos.z + (cos(angle * 3.14 / 180.0) * 50);

            movablePlayer = -1;
            VobPacket(this).setMovable();

            rebuild(pos.x,pos.y,pos.z,rotation.x,angle,rotation.z);
        }

        if(operationPlayer == playerId) {
            operationPlayer = -1;
            if(inventory != null) {
                inventory.save();
                inventory.close(playerId);
            }
            save();
        }
    }

    // rebuild vob
    function rebuild(x,y,z,rotx,roty,rotz) {
        position.x = x;
        position.y = y;
        position.z = z;

        rotation.x = rotx;
        rotation.y = roty;
        rotation.z = rotz;

        save();

        VobPacket(this).rebuild();
    }

    // hit vob
    function hit(playerId) {
        synchronized = false;
        hp = hp - 1;

        if(movablePlayer != -1) {
            local pos = getPlayerPosition(movablePlayer);
            local angle = getPlayerAngle(movablePlayer);

            pos.y = pos.y - 65.50;
            pos.x = pos.x + (sin(angle * 3.14 / 180.0) * 50);
            pos.z = pos.z + (cos(angle * 3.14 / 180.0) * 50);

            movablePlayer = -1;
            VobPacket(this).setMovable();

            rebuild(pos.x,pos.y,pos.z,rotation.x,angle,rotation.z);
        }

        if(operationPlayer != -1) {
            local playerIdOperation = operationPlayer;
            operationPlayer = -1;
            VobPacket(this).operation(false, playerIdOperation);
            save();
        }

        if(hp <= 0)
            VobController.onPlayerDestroyedVob(playerId, id);
        else
            VobPacket(this).setHealth();
    }

    // door interaction
    function doorInteraction(playerId) {
        if(lastUsedVob > getTickCount())
            return;

        lastUsedVob = getTickCount() + 2000;

        if(opened == 0)
            opened = 100;
        else
            opened = 0;

        Query().update(GameVob.VobTable, ["opened"], [opened]).where(["id = "+id]).execute();

        VobPacket(this).doorInteraction();
    }

    // set key configuration
    function setComplexity(newConfiguration) {
        complexity = newConfiguration;
        Query().update(GameVob.VobTable, ["complexity"], [newConfiguration]).where(["id = "+id]).execute();
    }

    function setGuid(newguid) {
        guid = newguid;
        Query().update(GameVob.VobTable, ["guid"], ["'"+ guid +"'"]).where(["id = "+id]).execute();
        VobPacket(this).newGuid();
    }

    // save vob
    function save() {
        Query().update(GameVob.VobTable, ["positionX", "positionY", "positionZ","rotationX", "rotationY", "rotationZ"], [
            position.x, position.y, position.z, rotation.x, rotation.y, rotation.z
        ]).where(["id = "+id]).execute();
    }
}

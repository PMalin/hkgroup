class VobPacket
{
    object = -1;
    packet = null;

    constructor(vobObject)
    {
        object = vobObject;
        packet = ExtendedPacket(Packets.Vob);
    }

    function sendCreateToAll() {
        packet.writeUInt8(VobPackets.Create)
        packet.writeInt16(object.id);
        packet.writeInt16(object.type);
        packet.writeBool(object.dynamic);
        packet.writeString(object.name);
        packet.writeString(object.visual);
        packet.writeInt16(object.movable);
        packet.writeInt16(object.movablePlayer);
        packet.writeInt16(object.trigger);
        packet.writeInt16(object.triggerType);
        packet.writeInt16(object.triggerAnimation);
        packet.writeInt16(object.triggerTime);
        packet.writeInt16(object.hpMode);
        packet.writeInt32(object.hp);
        packet.writeInt32(object.hpMax);
        packet.writeInt16(object.opened);
        packet.writeFloat(object.alpha);
        packet.writeFloat(object.position.x);
        packet.writeFloat(object.position.y);
        packet.writeFloat(object.position.z);
        packet.writeFloat(object.rotation.x);
        packet.writeFloat(object.rotation.y);
        packet.writeFloat(object.rotation.z);
        packet.writeFloat(object.targetPosition.x);
        packet.writeFloat(object.targetPosition.y);
        packet.writeFloat(object.targetPosition.z);
        packet.writeFloat(object.targetRotation.x);
        packet.writeFloat(object.targetRotation.y);
        packet.writeFloat(object.targetRotation.z);
        packet.writeFloat(object.weight);
        packet.writeString(object.guid);
        packet.writeUInt8(object.complexity);
        packet.writeInt16(object.operationPlayer);
        packet.writeString(object.createdBy);
        packet.sendToAll(RELIABLE);
    }

    function sendCreate(playerId) {
        packet.writeUInt8(VobPackets.Create)
        packet.writeInt16(object.id);
        packet.writeInt16(object.type);
        packet.writeBool(object.dynamic);
        packet.writeString(object.name);
        packet.writeString(object.visual);
        packet.writeInt16(object.movable);
        packet.writeInt16(object.movablePlayer);
        packet.writeInt16(object.trigger);
        packet.writeInt16(object.triggerType);
        packet.writeInt16(object.triggerAnimation);
        packet.writeInt16(object.triggerTime);
        packet.writeInt16(object.hpMode);
        packet.writeInt32(object.hp);
        packet.writeInt32(object.hpMax);
        packet.writeInt16(object.opened);
        packet.writeFloat(object.alpha);
        packet.writeFloat(object.position.x);
        packet.writeFloat(object.position.y);
        packet.writeFloat(object.position.z);
        packet.writeFloat(object.rotation.x);
        packet.writeFloat(object.rotation.y);
        packet.writeFloat(object.rotation.z);
        packet.writeFloat(object.targetPosition.x);
        packet.writeFloat(object.targetPosition.y);
        packet.writeFloat(object.targetPosition.z);
        packet.writeFloat(object.targetRotation.x);
        packet.writeFloat(object.targetRotation.y);
        packet.writeFloat(object.targetRotation.z);
        packet.writeFloat(object.weight);
        packet.writeString(object.guid);
        packet.writeUInt8(object.complexity);
        packet.writeInt16(object.operationPlayer);
        packet.writeString(object.createdBy);
        packet.send(playerId);
    }

    function operation(mode, playerId) {
        packet.writeUInt8(VobPackets.Operation)
        packet.writeInt16(object.id);
        packet.writeBool(mode);

        if(mode && object.inventory != null)
            packet.writeInt32(object.inventory.id);
        else
            packet.writeInt32(-1);

        packet.send(playerId);
    }

    function rebuild() {
        packet.writeUInt8(VobPackets.Rebuild)
        packet.writeInt16(object.id);
        packet.writeFloat(object.position.x);
        packet.writeFloat(object.position.y);
        packet.writeFloat(object.position.z);
        packet.writeFloat(object.rotation.x);
        packet.writeFloat(object.rotation.y);
        packet.writeFloat(object.rotation.z);
        packet.sendToAll(RELIABLE);
    }

    function doorInteraction() {
        packet.writeUInt8(VobPackets.DoorInteraction)
        packet.writeInt16(object.id);
        packet.writeInt16(object.opened);
        packet.sendToAll(RELIABLE);
    }

    function setHealth() {
        packet.writeUInt8(VobPackets.Health)
        packet.writeInt16(object.id);
        packet.writeInt32(object.hp);
        packet.sendToAll(RELIABLE);
    }

    function setMovable() {
        packet.writeUInt8(VobPackets.Movable)
        packet.writeInt16(object.id);
        packet.writeInt16(object.movablePlayer);
        packet.sendToAll(RELIABLE);
    }

    function remove() {
        packet.writeUInt8(VobPackets.Remove)
        packet.writeInt16(object.id);
        packet.sendToAll(RELIABLE);
    }

    function setComplexity() {
        packet.writeUInt8(VobPackets.Complexity)
        packet.writeInt16(object.id);
        packet.writeUInt8(object.complexity);
        packet.sendToAll(RELIABLE);
    }

    function newGuid() {
        packet.writeUInt8(VobPackets.NewGuid)
        packet.writeInt16(object.id);
        packet.writeString(object.guid);
        packet.sendToAll(RELIABLE);
    }
}


enum HousePackets
{
    Create,
    Remove,
    Owner,
    AddDoor,
    RemoveDoor,
}

enum HouseDoorState
{
    Opened,
    Closed,
}

class HouseController
{
    static houses = {};

    static function onWorldChange(world)
    {
        foreach(house in houses)
            house.refreshForWorld();
    }

    static function onHouseDoorInteract(houseId, doorId)
    {
        if (chatInputIsOpen())
		    return

        if (isConsoleOpen())
            return

        if(ActiveGui != null)
            return;

        if(getPlayerWeaponMode(heroId) != 0)
            return;


    }

    static function onPacket(packet)
    {
        local typ = packet.readUInt8();

        switch(typ)
        {
            case HousePackets.Create:
                local house = House(packet.readInt16())
                house.owner = packet.readInt16();
                house.world = packet.readString();
                house.name = packet.readString();
                house.minY = packet.readFloat();
                house.maxY = packet.readFloat();

                local positionToAdd = packet.readInt16();
                for(local i = 0; i < positionToAdd; i ++)
                    house.positions.append({x = packet.readFloat(), z = packet.readFloat()});

                local doorsToAdd = packet.readInt16();
                for(local i = 0; i < doorsToAdd; i ++)
                {
                    local door = HouseDoor(packet.readInt16(), house.id);
                    door.visual = packet.readString();
                    door.position = {x = packet.readFloat(), y = packet.readFloat(), z = packet.readFloat()}
                    door.rotation = {x = packet.readFloat(), y = packet.readFloat(), z = packet.readFloat()}
                    door.createObject();
                    house.doors.append(door);
                }

                houses[house.id] <- house;
                callEvent("onHouseAdd", houseId);
            break;
            case HousePackets.Remove:
                local houseId = packet.readInt16();
                if(!(houseId in houses))
                    return;

                houses.rawdelete(houseId);
                callEvent("onHouseRemove", houseId);
            break;
            case HousePackets.Owner:
                local houseId = packet.readInt16();
                local ownerId = packet.readInt16();
                if(!(houseId in houses))
                    return;
                    
                houses[houseId].owner = ownerId;
            break;
            case HousePackets.AddDoor:
                local houseId = packet.readInt16();
                local doorId = packet.readInt16();
                if(!(houseId in houses))
                    return;

                foreach(door in houses[houseId].doors)
                    if(door.id == doorId)
                        return;

                local door = HouseDoor(doorId, houseId);
                door.visual = packet.readString();
                door.position = {x = packet.readFloat(), y = packet.readFloat(), z = packet.readFloat()}
                door.rotation = {x = packet.readFloat(), y = packet.readFloat(), z = packet.readFloat()}
                door.createObject();
                houses[houseId].doors.append(door);
            break;
            case HousePackets.RemoveDoor:
                local houseId = packet.readInt16();
                local doorId = packet.readInt16();
                if(!(houseId in houses))
                    return;

                foreach(index, door in houses[houseId].doors) {
                    if(door.id == doorId) {
                        houses[houseId].doors.remove(index);
                        return;
                    }
                }
            break;
        }
    }
}

addEventHandler("onHouseDoorInteract", HouseController.onHouseDoorInteract.bindenv(HouseController));
addEventHandler("onWorldChange", HouseController.onWorldChange.bindenv(HouseController));
RegisterPacketReceiver(Packets.House, HouseController.onPacket.bindenv(HouseController));


getHouse <- @(id) HouseController.houses[id];
getHouses <- @() HouseController.houses;


class HousePacket
{
    object = -1;
    packet = null;

    constructor(houseObject)
    {
        object = houseObject;
        packet = ExtendedPacket(Packets.House);
    }
}
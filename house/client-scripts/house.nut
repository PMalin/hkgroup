
class House
{
    id = -1

    owner = -1
    minY = -1
    maxY = -1
    world = ""
    name = ""

    doors = null
    positions = null

    constructor(id)
    {
        this.id = id

        this.owner = -1
        this.minY = -1
        this.maxY = -1
        this.world = ""
        this.name = ""

        this.doors = []
        this.positions = []
    }

    function refreshForWorld()
    {
        foreach(door in doors)
            door.createObject();
    }
}

class HouseDoor
{
    id = -1

    houseId = -1
    owner = -1
    visual = ""

    position = null
    rotation = null

    element = null

    working = false
    workingTable = null

    constructor(id, houseId)
    {
        this.id = id
        this.houseId = houseId
        this.owner = -1
        this.visual = ""

        this.position = {x = 0, y = 0, z = 0}
        this.rotation = {x = 0, y = 0, z = 0}

        this.element = null
        
        this.working = false
        this.workingTable = false;
    }

    function createObject() {
        element = MobInter(visual);
        element.cdDynamic = true;
        element.cdStatic = true;
        element.setPosition(position.x, position.y, position.z);
        element.setRotation(rotation.x, rotation.y, rotation.z);

        if(getWorld() == getHouse(houseId).world)
            element.addToWorld();
    }
}
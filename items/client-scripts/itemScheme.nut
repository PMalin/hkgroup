class item.Scheme
{
    instance = ""
    flag = -1

    value = -1
    material = -1
    weight = -1
    resistance = 0

    description = ""
    name = ""

    dmg = null;
    dmgType = null;

    bonus = null;
    protection = null;
    requirement = null;
    restore = null;

    resistanceDebuf = 0;
    repairmentTool = 0;

    stacked = false;
    effect = null;

    buffApply = null
    buffRemove = null

    summon = null

    constructor(instance, properties)
    {
        instance = instance.toupper();
        this.instance = instance;
        this.flag = properties.flag;

        this.name = properties.name;
        this.description = "description" in properties ? properties.description : "";

        this.material = properties.material;
        this.value = properties.value;
        this.weight = 0.01;
        this.resistance = -1;
        this.resistanceDebuf = 0;
        this.repairmentTool = 0;

        this.dmg = null;
        this.dmgType = null;

        this.bonus = null;
        this.protection = [];
        this.requirement = null;
        this.restore = null;

        this.stacked = false;
        this.effect = null;

        this.summon = null;

        if("bonus" in properties)
            this.bonus = properties.bonus;

        if("weight" in properties)
            this.weight = properties.weight/1.0;

        if("dmg" in properties)
            this.dmg = properties.dmg;

        if("dmgType" in properties)
            this.dmgType = properties.dmgType;

        if("protection" in properties)
            this.protection = properties.protection;

        if("requirement" in properties)
            this.requirement = properties.requirement;

        if("restore" in properties)
            this.restore = properties.restore;

        if("stacked" in properties)
            this.stacked = properties.stacked;

        if("resistance" in properties)
            this.resistance = properties.resistance;

        if("resistanceDebuf" in properties)
            this.resistanceDebuf = properties.resistanceDebuf;

        if("repairmentTool" in properties)
            this.repairmentTool = properties.repairmentTool;

        if("effect" in properties)
            this.effect = properties.effect;

        if("buffApply" in properties)
            this.buffApply = properties.buffApply;

        if("buffRemove" in properties)
            this.buffRemove = properties.buffRemove;

        if("summon" in properties)
            this.summon = properties.summon;

        item.schemes[instance] <- this;
    }

    /**
     * Return dmg type of weapon
     */
    function getDmgType() {
        return dmgType;
    }

    /**
     * Return dmg
     */
    function getDamage() {
        return dmg;
    }

    /**
     * Returns description with name
     */
    function getFullStringDescriptionWithName()
    {
        return name + "\n\n" + getFullStringDescription();
    }

    /**
     * Returns description
     */
    function getFullStringDescription()
    {
        local string = description+"\n";
        switch(flag)
        {
            case ITEMCAT_ARMOR:
            case ITEMCAT_MASK:
            case ITEMCAT_HELMET:
                foreach(damageId, label in Config["DamageLabel"])
                {
                    if(protection.len() > damageId)
                        string += "Ochrona "+label+": "+protection[damageId]+"\n";
                }
            break;
            case ITEMCAT_ONEH: case ITEMCAT_TWOH: case ITEMCAT_BOW: case ITEMCAT_CBOW: case ITEMCAT_RUNE: case ITEMCAT_SCROLL:
                string += "Typ obra�e�: "+Config["DamageLabel"][dmgType]+"\n";
                string += "Obra�enia "+dmg+"\n";
            break;
            case ITEMCAT_RING: case ITEMCAT_BELT:
                foreach(damageId, label in Config["DamageLabel"])
                {
                    if(protection.len() > damageId)
                        string += "Ochrona "+label+": "+protection[damageId]+"\n";
                }
            break;
        }

        if(bonus) {
            string += "\n\nBonusy\n";
            foreach(_bonus in bonus)
                string += "Bonus do "+Config["PlayerAttributes"][_bonus.type]+": "+_bonus.value+"\n";
        }

        if(requirement) {
            string += "\n\nWymagania\n";
            foreach(_requirement in requirement)
                string += Config["PlayerAttributes"][_requirement.type]+": "+_requirement.value+"\n";
        }

        if(restore) {
            string += "\n\nPrzywraca\n";
            foreach(_restore in restore)
                string += Config["PlayerAttributes"][_restore.type]+": "+_restore.value+"\n";
        }

        if(resistance != -1)
            string += "\n\nWytrzyma�o��: "+resistance+"\n";

        return string;
    }

    static function isBowWeapon(weaponId) {
        local instance = Items.name(weaponId);

        if(instance in item.schemes)
            return item.schemes[instance].flag == ITEMCAT_BOW;

        return false;
    }

    static function isOneHWeapon(weaponId) {
        local instance = Items.name(weaponId);

        if(instance in item.schemes)
            return item.schemes[instance].flag == ITEMCAT_ONEH;

        return false;
    }
}

function getItemScheme(instance) {
    instance = instance.toupper();

    if(instance in item.schemes)
        return item.schemes[instance];

    return null
}
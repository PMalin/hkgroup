local windowSize = null;

class item.Bucket.UI {
    static _scene = [];

    bucket = null
    visible = false

    title = "";

    size = null
    connection = null
    blocked = false

    dimensionX = -1
    dimensionY = -1

    graphic = null

    constructor(_bucket){
        bucket = _bucket.weakref();

        visible = false
        connection = null
        blocked = false

        dimensionX = -1
        dimensionY = -1

        graphic = {}
        title = "";

        _scene.push(this.weakref());
    }

    function setTitle(_title) {
        title = _title;
    }

    function getTitle() {
        return title;
    }

    function setConnection(_connection) {
        connection = _connection.weakref();
    }

    function getConnection() {
        return connection;
    }

    function setBlock(_value) {
        blocked = _value

        if(_value == true) {
            graphic.background.setFile("WHITE.TGA");
            graphic.background.setColor(3, 25, 35);
        }else{
            graphic.background.setFile("BLACK.TGA");
            graphic.background.setColor(255,255,255);
        }

        foreach(butt in graphic.buttons) {
            butt.setVisible(false);
            butt.destroy();
        }

        graphic.elements.clear();
        graphic.buttons.clear();
        graphic.window.visible = false;
    }

    function getBlock() {
        return blocked;
    }

    function setVisible(value, side = ItemGUISide.Left, start = ItemGUIPosition.Top, dimension = ItemGUISize.Full) {
        value ? show(side, start, dimension) : hide();
    }

    function isVisible() {
        return visible;
    }

    function show(side, start, dimension) {
        local width = ((windowSize.factorX * (windowSize.oneIconX + windowSize.paddingX)) + (windowSize.paddingX * 3) + windowSize.scrollX);
        local height = ((windowSize.factorY * (windowSize.oneIconY + windowSize.paddingY)) + (windowSize.paddingY * 2));

        local positionX = side == ItemGUISide.Left ? anx(Resolution.x/2) - (width + windowSize.paddingX * 2) : anx(Resolution.x/2) + windowSize.paddingX * 2;
        local positionY = any(Resolution.y/2) - abs(height/2);

        if(dimension == ItemGUISize.Half)
            height = abs(height/2) - abs(windowSize.paddingY * 4);

        if(start == ItemGUIPosition.Bottom)
            positionY = positionY + height + abs(windowSize.paddingY * 4);

        graphic.background <- GUI.Texture(positionX, positionY, width, height, "BLACK.TGA");
        graphic.background.alpha = 200;
        graphic.slider <- GUI.ScrollBar(
            side == ItemGUISide.Left ? positionX + windowSize.paddingX : (positionX + width) - (windowSize.paddingX + windowSize.scrollX),
            positionY + windowSize.paddingY * 2,
            windowSize.scrollX,
            height - windowSize.paddingY * 4,
            "HK_SCROLL.TGA", "HK_BUTTON.TGA", "HK_U.TGA", "HK_O.TGA", Orientation.Vertical
        );

        graphic.window <- Texture(0,0,anx(200), any(200), "HK_BOX.TGA");
        graphic.buttons <- [];
        graphic.elements <- [];

        if(title != "") {
            graphic.title <- GUI.Button(positionX, positionY - any(50), width, any(50), "BLACK.TGA", title);
            graphic.title.setVisible(true);
        }

        graphic.background.setVisible(true);
        graphic.slider.setVisible(true);

        dimensionX = windowSize.factorX;

        if(dimension == ItemGUISize.Full)
            dimensionY = windowSize.factorY;
        else
            dimensionY = abs(windowSize.factorY/2);

        local slots = [];
        for(local y = 0; y < dimensionY; y++)
            for(local x = 0; x < dimensionX; x++)
                slots.push(item.Bucket.UI.Slot(x,y));

        graphic.slots <- slots;

        graphic.slider.setVisible(true);
        graphic.background.setVisible(true);

        visible = true
        refresh();
    }

    function hide() {
        graphic.slider.setVisible(false);
        graphic.background.setVisible(false);

        if("title" in graphic) {
            graphic.title.setVisible(false);
            graphic.title.destroy();
        }

        foreach(butt in graphic.buttons) {
            butt.setVisible(false);
            butt.destroy();
        }

        graphic.buttons.clear();
        graphic.elements.clear();
        graphic.slider.destroy();
        graphic.background.destroy();

        graphic = null
        visible = false
        graphic = {};
    }

    function refresh() {
        if(visible == false)
            return;

        local position = graphic.background.getPosition();
        local sizeWindow = graphic.background.getSize();

        if(bucket.maxWeight != 0.0) {
            graphic.weightDraw <- Draw(0, 0, bucket.weight +"/"+bucket.maxWeight);
            graphic.weightDraw.setPosition(position.x + sizeWindow.width - graphic.weightDraw.width - 100, position.y + 25);
            graphic.weightDraw.visible = true;
        }

        if(position.x < anx(Resolution.x/2))
            position.x = position.x + windowSize.paddingX + windowSize.scrollX;

        local value = graphic.slider.getValue(), items = bucket.getItems(), index = 0;
        for(local x = 0; x < dimensionY; x++) {
            for(local y = 0; y < dimensionX; y++) {
                local _item = index + (value * dimensionY);
                _item = items.len() > _item ? items[_item] : null;

                graphic.slots[index].update(_item, {x = position.x, y = position.y});
                index = index + 1;
            }
        }
    }

    function mouseClick(buttonId) {
        if(graphic == null)
            return;

        if(graphic.len() == 0)
            return;

        if(blocked)
            return;

        if(graphic.window.visible == false) {
            foreach(slot in graphic.slots) {
                if(slot.isMouseAt()) {
                    local item = slot.getItem();
                    if(item == null)
                        return;

                    foreach(butt in graphic.buttons) {
                        butt.setVisible(false);
                        butt.destroy();
                    }

                    graphic.buttons.clear();
                    graphic.elements.clear();

                    local cursorPositionPx = getCursorPositionPx();
                    switch(buttonId) {
                        case MOUSE_LMB:
                            graphic.window.setPositionPx(cursorPositionPx.x, cursorPositionPx.y);
                            graphic.window.setSizePx(200, 195);
                            graphic.window.file = "BLACK.TGA";
                            graphic.window.alpha = 200;
                            graphic.buttons <- [
                                GUI.Button(anx(cursorPositionPx.x) + anx(15), any(cursorPositionPx.y) + any(15), anx(170), any(45), "HK_BUTTON.TGA", "Informacja"),
                                GUI.Button(anx(cursorPositionPx.x) + anx(15), any(cursorPositionPx.y) + any(75), anx(170), any(45), "HK_BUTTON.TGA", "Przerzu�"),
                                GUI.Button(anx(cursorPositionPx.x) + anx(15), any(cursorPositionPx.y) + any(135), anx(170), any(45), "HK_BUTTON.TGA", "Zamknij"),
                            ];
                            graphic.buttons[0].attribute = {object = this, itemId = item.id};
                            graphic.buttons[1].attribute = {object = this, itemId = item.id};
                            graphic.buttons[2].attribute = {object = this, itemId = item.id};

                            graphic.buttons[2].bind(EventType.Click, function(element) {
                                local obj = element.attribute.object;
                                foreach(butt in obj.graphic.buttons) {
                                    butt.setVisible(false);
                                    butt.destroy();
                                }
                                obj.graphic.elements.clear();
                                obj.graphic.buttons.clear();
                                obj.graphic.window.visible = false;
                            });

                            graphic.buttons[1].bind(EventType.Click, function(element) {
                                local obj = element.attribute.object;
                                obj.swapItemDefine(element.attribute.itemId);
                            });

                            graphic.buttons[0].bind(EventType.Click, function(element) {
                                local obj = element.attribute.object;
                                obj.informationAboutItem(element.attribute.itemId);
                            });

                            graphic.window.visible = true;
                            foreach(butt in graphic.buttons)
                                butt.setVisible(true);
                        break;
                        case MOUSE_RMB:
                            if(connection == null)
                                return;

                            graphic.window.setPositionPx(cursorPositionPx.x, cursorPositionPx.y);
                            graphic.window.setSizePx(200, 195);
                            graphic.window.file = "BLACK.TGA";
                            graphic.window.alpha = 200;

                            swapItemDefine(item.id);
                        break;
                    }
                }
            }
        }
    }

    function informationAboutItem(itemId) {
        foreach(butt in graphic.buttons) {
            butt.setVisible(false);
            butt.destroy();
        }

        graphic.buttons.clear();
        graphic.elements.clear();

        local item = getItem(itemId);
        local windowPositionPx = graphic.window.getPositionPx();
        graphic.elements <- [Draw(anx(windowPositionPx.x) + anx(15), any(windowPositionPx.y) + any(75), item.name+"\n\n"+item.getFullStringDescription())]

        local size = graphic.elements[0].widthPx;
        graphic.window.setSize(anx(30 + graphic.elements[0].widthPx), any(85 + graphic.elements[0].heightPx));
        size = graphic.window.getSizePx();

        graphic.buttons = [
            GUI.Button(anx(windowPositionPx.x) + anx(15), any(windowPositionPx.y) + any(15), anx(size.width - 30), any(45), "BLACK.TGA", "Zamknij"),
        ];
        graphic.buttons[0].attribute = {object = this, itemId = item.id};

        graphic.window.visible = true;
        graphic.elements[0].visible = true;
        foreach(butt in graphic.buttons)
            butt.setVisible(true);

        graphic.buttons[0].bind(EventType.Click, function(element) {
            local obj = element.attribute.object;
            foreach(butt in obj.graphic.buttons) {
                butt.setVisible(false);
                butt.destroy();
            }
            obj.graphic.buttons.clear();
            obj.graphic.elements.clear();
            obj.graphic.window.visible = false;
        });
    }

    function swapItemDefine(itemId) {
        foreach(butt in graphic.buttons) {
            butt.setVisible(false);
            butt.destroy();
        }

        graphic.elements.clear();
        graphic.buttons.clear();

        if(connection == null) {
            graphic.window.visible = false;
            return;
        }

        graphic.window.setSize(anx(200), any(200));

        local _item = getItem(itemId);
        local size = graphic.window.getSizePx();
        local windowPositionPx = graphic.window.getPositionPx();

        graphic.buttons = [
            GUI.Input(anx(windowPositionPx.x) + anx(15), any(windowPositionPx.y) + any(15), anx(size.width - 30), any(45), "HK_BUTTON.TGA", "FONT_OLD_10_WHITE_HI.TGA", Input.Numbers, Align.Center, "0", 2),
            GUI.Button(anx(windowPositionPx.x) + anx(15), any(windowPositionPx.y) + any(75), anx(size.width - 30), any(45), "HK_BUTTON.TGA", "Przenie�"),
            GUI.Button(anx(windowPositionPx.x) + anx(15), any(windowPositionPx.y) + any(145), anx(size.width - 30), any(45), "HK_BUTTON.TGA", "Zamknij"),
        ];

        graphic.buttons[0].setText(_item.amount.tostring());
        graphic.buttons[1].attribute = {object = this, itemId = itemId};
        graphic.buttons[2].attribute = {object = this, itemId = itemId};

        graphic.window.visible = true;
        foreach(butt in graphic.buttons)
            butt.setVisible(true);

        graphic.buttons[1].bind(EventType.Click, function(element) {
            local obj = element.attribute.object;
            obj.swapItemExecute(element.attribute.itemId, obj.graphic.buttons[0].getText())
        });

        graphic.buttons[2].bind(EventType.Click, function(element) {
            local obj = element.attribute.object;
            foreach(butt in obj.graphic.buttons) {
                butt.setVisible(false);
                butt.destroy();
            }
            obj.graphic.buttons.clear();
            obj.graphic.elements.clear();
            obj.graphic.window.visible = false;
        });
    }

    function swapItemExecute(itemId, amount) {
        foreach(butt in graphic.buttons) {
            butt.setVisible(false);
            butt.destroy();
        }

        graphic.buttons.clear();
        graphic.elements.clear();
        graphic.window.visible = false;

        try {
            amount = amount.tointeger();
        }catch(error) {};

        if(typeof(amount) != "integer")
            return;

        local item = getItem(itemId);
        if(bucket.hasItem(item) == false)
            return;

        bucket.rechangeItem(item, amount, connection);
    }

    function change(obj) {
        if(!("slider" in graphic))
            return;

        if(obj != graphic.slider)
            return;

        refresh();
    }

    function render() {

    }
}

class item.Bucket.UI.Slot {
    position = null

    background = null
    resistanceback = null

    render = null
    amount = null

    visible = false
    itemRef = null

    constructor(x,y) {
        position = {x = x, y = y}

        background = Texture(0, 0, windowSize.oneIconX, windowSize.oneIconY, "HK_BOX.TGA");
        background.alpha = 180;

        resistanceback = Texture(0, 0, windowSize.oneIconX - anx(6), windowSize.oneIconY - any(6), "WHITE.TGA");
        resistanceback.setColor(190,180,170);
        resistanceback.alpha = 0;

        render = null
        amount = null

        visible = false
        itemRef = null
    }

    function update(_item, _position) {
        if(_item != null)
        {
            itemRef = _item.weakref();
            _position.x = _position.x + windowSize.paddingX;
            _position.y = _position.y + windowSize.paddingY;

            _position = {x = _position.x + ((windowSize.paddingX + windowSize.oneIconX) * position.x), y = _position.y + ((windowSize.paddingY + windowSize.oneIconY) * position.y)}

            render = ItemRender(_position.x - anx(7), _position.y - any(7), windowSize.oneIconX + anx(14), windowSize.oneIconY + any(14), _item.instance);
            background.setPosition(_position.x, _position.y);
            resistanceback.setPosition(_position.x + anx(5), _position.y + any(5));

            amount = Draw(_position.x, _position.y, _item.amount);
            if(getSetting("OtherCustomFont")) {
                amount.font = "FONT_OLD_20_WHITE_HI.TGA";
                amount.setScale(0.45, 0.45);
            }
            render.visible = true;
            background.visible = true;
            resistanceback.visible = true;
            amount.visible = true;

            amount.setPosition(_position.x + windowSize.oneIconX - anx(5) - amount.width, _position.y + windowSize.oneIconY - any(5) - amount.height);
        }else {
            itemRef = null
            render = null
            amount = null
            background.visible = false;
            resistanceback.visible = false;
        }
    }

    function getItem() {
        return itemRef;
    }

    function isMouseAt() {
        local texPosition = background.getPositionPx();
        local texSize = background.getSizePx();
        local cursorPosition = getCursorPositionPx()

		if (cursorPosition.x >= texPosition.x && cursorPosition.x <= texPosition.x + texSize.width
		&& cursorPosition.y >= texPosition.y && cursorPosition.y <= texPosition.y + texSize.height)
			return true

        return false;
    }
}

addEventHandler ("onMouseClick", function (buttonId) {
    local objectLength = item.Bucket.UI._scene.len();

    for(local i = (objectLength - 1); i >= 0; i--)
        item.Bucket.UI._scene[i].mouseClick(buttonId);
});

addEventHandler ("onRender", function () {
    local objectLength = item.Bucket.UI._scene.len();

    for(local i = (objectLength - 1); i >= 0; i--) {
        if(item.Bucket.UI._scene[i] == null)
            item.Bucket.UI._scene.remove(i);
        else
            item.Bucket.UI._scene[i].render();
    }
});

addEventHandler("GUI.onChange", function(self) {
    local objectLength = item.Bucket.UI._scene.len();

    for(local i = (objectLength - 1); i >= 0; i--) {
        item.Bucket.UI._scene[i].change(self);
    }
})

if(Resolution.x < 700) {
    windowSize = {
        oneIconX = anx(60),
        oneIconY = any(60),
        paddingX = anx(5),
        paddingY = any(5),
        scrollX = anx(25),
        factorX = 4,
        factorY = 4,
    }
}else if(Resolution.x < 1024) {
    windowSize = {
        oneIconX = anx(70),
        oneIconY = any(70),
        paddingX = anx(5),
        paddingY = any(5),
        scrollX = anx(25),
        factorX = 4,
        factorY = 6,
    }
}else if(Resolution.x < 1300) {
    windowSize = {
        oneIconX = anx(75),
        oneIconY = any(75),
        paddingX = anx(8),
        paddingY = any(8),
        scrollX = anx(30),
        factorX = 6,
        factorY = 8,
    }
}else {
    windowSize = {
        oneIconX = anx(80),
        oneIconY = any(80),
        paddingX = anx(10),
        paddingY = any(10),
        scrollX = anx(30),
        factorX = 6,
        factorY = 8,
    }
}
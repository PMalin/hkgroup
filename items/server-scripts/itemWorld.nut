
ItemsWorldRegister <- {
    worlds = {}
    add = function (world, _itemGroundId, _itemId) {
        if(!(world in worlds))
            worlds[world] <- [];

        worlds[world].push({
            itemGroundId = _itemGroundId,
            itemId = _itemId
        });
    }

    destroyColleration = function (_itemGroundId) {
        foreach(_world in worlds) {
            foreach(_index, _item in _world) {
                if(_item.itemGroundId == _itemGroundId) {
                    item.Controller.removeItem(_item.itemId);
                    return;
                }
            }
        }
    }

    onPlayerDropItem = function(playerId, itemGround) {
        removeItem(playerId, itemGround.instance, itemGround.amount);
        cancelEvent();
        return;

//      callEvent("onPlayerLeftOnGround", itemGround);
//      addPlayerLog(playerId, "Wyrzuci� "+itemGround.instance+" | " + itemGround.amount);
    }

    onPlayerTakeItem = function(playerId, itemGround) {
        local result = NoteController.onPlayerTakeItem(playerId, itemGround);
        if(result == true) {
            _removeItem(playerId, Items.id(itemGround.instance), itemGround.amount);
            return;
        }

        if(itemGround.amount != 0) {
            GroundHerbController.onPlayerTakeItem(itemGround);
            local searchForObject = false, world = getPlayerWorld(playerId);

            if(!(world in worlds))
                worlds[world] <- [];

            foreach(_index, _item in worlds[world]) {
                if(_item.itemGroundId == itemGround.id) {
                    local _player = getPlayer(playerId);
                    if(_player.loggIn && _player.inventory != null)
                        _player.inventory.addItem(_item.itemId);

                    searchForObject = true;
                    worlds[world].remove(_index);
                    break;
                }
            }

            if(searchForObject == false) {
                _removeItem(playerId, Items.id(itemGround.instance), itemGround.amount);
                giveItem(playerId, itemGround.instance, itemGround.amount);
            }

            callEvent("onPlayerTakeFromGround", itemGround);
            addPlayerLog(playerId, "Podni�s� "+itemGround.instance+" | " + itemGround.amount);
        }
    }
}

addEventHandler("onPlayerDropItem", ItemsWorldRegister.onPlayerDropItem.bindenv(ItemsWorldRegister));
addEventHandler("onPlayerTakeItem", ItemsWorldRegister.onPlayerTakeItem.bindenv(ItemsWorldRegister));
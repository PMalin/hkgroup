
class item.Packet
{
    packet = null
    bucket = null

    constructor(_bucket)
    {
        packet = Packet();
        packet.writeUInt8(item.packetId);

        bucket = _bucket
    }

    function send(playerId) {
        packet.writeUInt8(ItemPackets.SendBucket);
        packet.writeUInt16(bucket.id);
        packet.writeFloat(bucket.maxWeight);
        packet.writeUInt16(bucket.ids.len());
        foreach(itemId in bucket.ids) {
            local itemObj = getItem(itemId);
            packet.writeUInt32(itemObj.id);
            packet.writeString(itemObj.instance);
            packet.writeString(itemObj.name);
            packet.writeString(itemObj.description);
            packet.writeString(itemObj.guid);
            packet.writeUInt16(itemObj.amount);
            packet.writeUInt16(itemObj.resistance);
            packet.writeBool(itemObj.stacked);
            packet.writeBool(itemObj.equipped);
        }
        packet.send(playerId, RELIABLE);
    }

    function addItem(itemId) {
        packet.writeUInt8(ItemPackets.AddItemBucket);
        packet.writeUInt16(bucket.id);
        packet.writeUInt32(itemId);
        local itemObj = getItem(itemId);
        packet.writeString(itemObj.instance);
        packet.writeString(itemObj.name);
        packet.writeString(itemObj.description);
        packet.writeString(itemObj.guid);
        packet.writeUInt16(itemObj.amount);
        packet.writeUInt16(itemObj.resistance);
        packet.writeBool(itemObj.stacked);
        packet.writeBool(itemObj.equipped);
        foreach(observerId in bucket.observers)
            packet.send(observerId, RELIABLE);
    }

    function removeItem(itemId) {
        packet.writeUInt8(ItemPackets.RemoveItemBucket);
        packet.writeUInt16(bucket.id);
        packet.writeUInt32(itemId);
        foreach(observerId in bucket.observers)
            packet.send(observerId, RELIABLE);
    }

    function resend() {
        packet.writeUInt8(ItemPackets.ExchangeItemBucket);
        packet.writeUInt16(bucket.id);
        packet.writeFloat(bucket.maxWeight);
        packet.writeUInt16(bucket.ids.len());
        foreach(itemId in bucket.ids) {
            local itemObj = getItem(itemId);
            packet.writeUInt32(itemObj.id);
            packet.writeString(itemObj.instance);
            packet.writeString(itemObj.name);
            packet.writeString(itemObj.description);
            packet.writeString(itemObj.guid);
            packet.writeUInt16(itemObj.amount);
            packet.writeUInt16(itemObj.resistance);
            packet.writeBool(itemObj.stacked);
            packet.writeBool(itemObj.equipped);
        }
        foreach(observerId in bucket.observers)
            packet.send(observerId, RELIABLE);
    }
}

DMGCalculation <- {};

DMGCalculation.calculateInventoryProtection <- function(obj)
{
    return obj.getProtection();
}

DMGCalculation.calculateDamage <- function (obj, preyObj)
{
    local weaponMode = obj.getWeaponMode();
    local damage = 0, itemUsedToGiveDmg = -1, dmgType = DAMAGE_BLUNT;

    local protections = DMGCalculation.calculateInventoryProtection(preyObj);

    switch (weaponMode)
	{
		case WEAPONMODE_FIST:
            itemUsedToGiveDmg = null;
		break;

		case WEAPONMODE_1HS:
		case WEAPONMODE_2HS:
			itemUsedToGiveDmg = obj.getMeleeWeapon();
		break;

		case WEAPONMODE_BOW:
		case WEAPONMODE_CBOW:
			itemUsedToGiveDmg = obj.getRangedWeapon();
		break;

		case WEAPONMODE_MAG:
			itemUsedToGiveDmg = obj.getMagicWeapon();
		break;
	}

    if(itemUsedToGiveDmg == null) {
        if(obj.str > 10) {
            local dmg = abs(obj.str/2);
            local protectionForDamage = protections[DAMAGE_BLUNT];
            protectionForDamage = 100 - protectionForDamage;

            dmg = (dmg * protectionForDamage)/100;
            dmg = abs(dmg);
            if(dmg < 5)
                dmg = 5;

            return dmg;
        }

        return 5;
    }

    if(itemUsedToGiveDmg.getRequirement()) {
        foreach(_requirement in itemUsedToGiveDmg.getRequirement()) {
            if(_requirement.type == PlayerAttributes.Mana)
                continue;

            damage += obj.getAttribute(_requirement.type)/2;
        }
    }

    dmgType = itemUsedToGiveDmg.getDmgType();
    local protectionForDamage = protections[dmgType];
    if(protectionForDamage > 100)
        protectionForDamage = 100;

    protectionForDamage = 100 - protectionForDamage;
    damage += itemUsedToGiveDmg.getDamage();
    damage = (damage * protectionForDamage)/100;

    switch (weaponMode)
	{
		case WEAPONMODE_1HS:
            local random = irand(100)
            if(random < obj.weapon[0])
                damage = damage * 1.75;
        break;
		case WEAPONMODE_2HS:
            local random = irand(100)
            if(random < obj.weapon[1])
                damage = damage * 1.75;
		break;
		case WEAPONMODE_BOW:
            local random = irand(100)
            if(random < obj.weapon[2])
                damage = damage * 1.75;
        break;
		case WEAPONMODE_CBOW:
            local random = irand(100)
            if(random < obj.weapon[3])
                damage = damage * 1.75;
        break;
	}

    preyObj.resistanceCallback();


    if(itemUsedToGiveDmg.resistance > 0 && itemUsedToGiveDmg.resistance != -1) {
        itemUsedToGiveDmg.resistance = itemUsedToGiveDmg.resistance - 1;
        itemUsedToGiveDmg.sendResistance(obj.id);
    }

    return abs(damage);
}

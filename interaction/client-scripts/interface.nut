
local interactionLabel = GUI.Button(anx(Resolution.x/2 - 150), any(Resolution.y - 200), anx(300), any(60), "HK_INPUT.TGA", "Interakcja - LCTRL");

function showInteractionLabel() {
    interactionLabel.setVisible(true);
}

function hideInteractionLabel() {
    interactionLabel.setVisible(false);
}

addEventHandler("onOpenGUI", hideInteractionLabel);

addEventHandler("onKey", function(key) {
    if(ActiveGui != null)
        return;

    if (chatInputIsOpen())
		return

	if (isConsoleOpen())
		return

    if (isGUIInputActive)
        return;

    if(getPlayerWeaponMode(heroId) != 0)
        return;

    if(interactionLabel.m_visible && key == KEY_LCONTROL)
        FocusInteraction.onAcceptFocus();
})
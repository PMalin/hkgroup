
NoteInterface <- {
    previewId = -1,
    bucket = null,

    prepareWindow = function()
    {
        local result = {};

        result.window <- GUI.Window(anx(Resolution.x/2 - 350), any(Resolution.y/2 - 350), anx(700), any(700), "HK_BOX.TGA", null, false);
        result.scrollBar <- GUI.ScrollBar(anx(650), any(50), anx(30), any(600), "HK_SCROLL.TGA", "HK_INDICATOR.TGA", "HK_U.TGA", "HK_O.TGA", Orientation.Vertical, result.window);
        result.addButton <- GUI.Button(anx(0), any(720), anx(250), any(60), "HK_BUTTON.TGA", "Dodaj", result.window);
        result.saveButton <- GUI.Button(anx(450), any(720), anx(250), any(60), "HK_BUTTON.TGA", "Zapisz", result.window);
        result.dropButton <- GUI.Button(anx(0), any(720), anx(250), any(60), "HK_BUTTON.TGA", "Wypu��", result.window);
        result.destroyButton <- GUI.Button(anx(450), any(720), anx(250), any(60), "HK_BUTTON.TGA", "Zniszcz", result.window);
        result.input <- MyGUI.MultiLineInput(anx(Resolution.x/2 - 320), any(Resolution.y/2 - 270), anx(640), 16);
        result.nameInput <- GUI.Input(anx(20), any(20), anx(660), any(45), "HK_INPUT.TGA", "FONT_OLD_10_WHITE_HI.TGA", Input.Text, Align.Center, "Nazwa", 2, result.window);

        result.buttons <- [];
        result.draws <- [];

        result.bindDestroy <- Bind.addClick(result.destroyButton, function() {
            NoteInterface.destroy();
        }, PlayerGUI.Notes);

        result.bindDrop <- Bind.addClick(result.dropButton, function() {
            NoteInterface.drop();
        }, PlayerGUI.Notes);

        result.bindForScrollbar <- Bind.onChange(result.scrollBar, function(value) {
            NoteInterface.change(value);
        }, PlayerGUI.Notes);

        result.bindAdd <- Bind.addClick(result.addButton, function() {
            NoteInterface.add();
        }, PlayerGUI.Notes);

        result.bindSave <- Bind.addClick(result.saveButton, function() {
            NoteInterface.save();
        }, PlayerGUI.Notes);

        return result;
    }

    preview = function(noteId) {
        foreach(butt in bucket.buttons)
            butt.destroy();

        bucket.draws.clear();
        bucket.buttons.clear();

        previewId = noteId;

        bucket.dropButton.setVisible(true);
        bucket.destroyButton.setVisible(true);

        bucket.scrollBar.setVisible(false);
        bucket.addButton.setVisible(false);
        bucket.saveButton.setVisible(false);
        bucket.input.setVisible(false);
        bucket.nameInput.setVisible(false);

        bucket.dropButton.attribute = noteId;
        bucket.destroyButton.attribute = noteId;

        local object = {name = "", text = []};
        foreach(note in NoteController.notes) {
            if(note.id == noteId) {
                object = {name = note.name, text = String.parseArray(note.text, "\n")}
                break;
            }
        }

        bucket.draws.push(Draw(anx(Resolution.x/2 - 330), any(Resolution.y/2 - 320), object.name));
        bucket.draws.push(Draw(anx(Resolution.x/2 - 330), any(Resolution.y/2 - 280), object.text));

        foreach(d in bucket.draws) {
            if(getSetting("OtherCustomFont")) {
                d.font = "FONT_OLD_20_WHITE_HI.TGA";
                d.setScale(0.5, 0.5);
            }
            d.setColor(255, 248, 196);
            d.visible = true;
        }

        if(getSetting("OtherCustomFont")) {
            bucket.draws[0].setScale(0.6,0.6);
        }

        bucket.draws[0].setColor(255, 144, 66);
    }

    drop = function() {
        NotePacket().drop(previewId);
        hide();
    }

    destroy = function() {
        NotePacket().destroy(previewId);
        hide();
    }

    show = function() {
        if(bucket == null)
            bucket = prepareWindow();

        BaseGUI.show();
        ActiveGui = PlayerGUI.Notes;
        bucket.window.setVisible(true);
        NoteController.notes.clear();
        NotePacket().get();

        bucket.scrollBar.setVisible(true);
        bucket.addButton.setVisible(true);
        bucket.dropButton.setVisible(false);
        bucket.destroyButton.setVisible(false);
        bucket.saveButton.setVisible(false);
        bucket.input.setVisible(false);
        bucket.nameInput.setVisible(false);

        if(hasItem("ITUS_HK_TOOL_39") == 0)
            bucket.addButton.setVisible(false);

        playAni(heroId, "T_MAP_STAND_2_S0");
    }

    hide = function() {
        bucket.window.setVisible(false);
        foreach(butt in bucket.buttons)
            butt.destroy();

        bucket.draws.clear();
        bucket.buttons.clear();
        bucket.input.setVisible(false);
        BaseGUI.hide();
        ActiveGui = null;

        playAni(heroId,"S_RUN");
    }

    onForceCloseGUI = function() {
        if(ActiveGui == PlayerGUI.Notes)
            hide();
    }

    add = function() {
        foreach(butt in bucket.buttons)
            butt.destroy();

        bucket.buttons.clear();
        bucket.scrollBar.setVisible(false);
        bucket.addButton.setVisible(false);
        bucket.saveButton.setVisible(true);
        bucket.input.setVisible(true);
        bucket.nameInput.setVisible(true);
        bucket.input.active = true;
    }

    save = function() {
        NotePacket().save(bucket.nameInput.getText(), bucket.input.getValue());
        hide();
    }

    change = function(value = 0) {
        if(type(value) == "string" || type(value) == "object")
            return;

        foreach(butt in bucket.buttons)
            butt.destroy();

        bucket.buttons.clear();

        local skippedItems = -1, alredyVisible = 0;
        local notes = NoteController.notes;
        foreach(note in notes) {
            skippedItems = skippedItems + 1;

            if(skippedItems < value)
                continue;

            if(skippedItems > (value + 6))
                continue;

            bucket.buttons.push(NoteInterface.Button(note, alredyVisible));
            bucket.buttons[bucket.buttons.len() - 1].setVisible(true);
            alredyVisible = alredyVisible + 1;
        }
    }
}

Bind.addKey("NOTES", NoteInterface.show.bindenv(NoteInterface))
Bind.addKey(KEY_ESCAPE, NoteInterface.hide.bindenv(NoteInterface), PlayerGUI.Notes)

addEventHandler("onForceCloseGUI", NoteInterface.onForceCloseGUI.bindenv(NoteInterface));

class NoteInterface.Button {
    button = null
    render = null
    nameDraw = null

    constructor(_note, _visibleIndex) {
        local position = {x = 0, y = 0};
        switch(_visibleIndex) {
            case 0: position = {x = anx(Resolution.x/2 - 350) + anx(25), y = any(Resolution.y/2 - 350) + any(25)}; break;
            case 1: position = {x = anx(Resolution.x/2 - 350) + anx(325), y = any(Resolution.y/2 - 350) + any(25)}; break;
            case 2: position = {x = anx(Resolution.x/2 - 350) + anx(25), y = any(Resolution.y/2 - 350) + any(245)}; break;
            case 3: position = {x = anx(Resolution.x/2 - 350) + anx(325), y = any(Resolution.y/2 - 350) + any(245)}; break;
            case 4: position = {x = anx(Resolution.x/2 - 350) + anx(25), y = any(Resolution.y/2 - 350) + any(465)}; break;
            case 5: position = {x = anx(Resolution.x/2 - 350) + anx(325), y = any(Resolution.y/2 - 350) + any(465)}; break;
        }

        button = GUI.Button(position.x, position.y, anx(280), any(220), "HK_BUTTON.TGA", "");
        button.attribute = _note.id;
        render = ItemRender(position.x, position.y + any(20), anx(280), any(160), _note.renderInstance);

        button.bind(EventType.Click, function(element) {
            NoteInterface.preview(element.attribute);
        });

        local nameDrawText = _note.name;
        if(nameDrawText.len() > 25)
            nameDrawText = nameDrawText.slice(0, 25);

        nameDraw = GUI.Button(position.x, position.y + any(180), anx(280), any(40), "HK_BUTTON.TGA", nameDrawText);
    }

    function destroy() {
        button.destroy();
        nameDraw.destroy();
        render = null;
    }

    function setVisible(value) {
        button.setVisible(value);
        render.visible = value;
        nameDraw.setVisible(value);
        render.top();
    }
}
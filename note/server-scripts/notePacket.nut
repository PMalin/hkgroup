
class NotePacket {
    packet = null;

    constructor() {
        packet = ExtendedPacket(Packets.Note);
    }

    function get(playerId, id, name, text) {
        packet.writeUInt8(NotePackets.Get)
        packet.writeInt32(id);
        packet.writeString(name);
        packet.writeInt16(text.len());
        for(local i = 0; i < text.len(); i ++)
            packet.writeString(text[i]);
        packet.send(playerId, RELIABLE);
    }
}
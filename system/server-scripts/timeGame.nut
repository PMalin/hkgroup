
_setTime <- setTime;
_getTime <- getTime;

System.TimeGame <- {

    hour = 0,
    min = 0,
    day = 0,
    timerSystem = null,
    cache = {},

    onInit = function() {
        local value = System.readRecord(SystemTypes.Time, "1:8:0");
        value = split(value, ":");

        day = value[0].tointeger();
        hour = value[1].tointeger();
        min = value[2].tointeger();

        _setTime(hour, min);
        generateCache();

        timerSystem = setTimer(function() {
            System.TimeGame.plus();
        }, Config["MinuteSeconds"] * 1000, 0);
    }

    updateValue = function() {
        System.updateRecord(SystemTypes.Time, day+":"+hour+":"+min);
    }

    plus = function() {
        min = min + 1;

        if(min == 15 || min == 30 || min == 45 || min == 0) {
            _setTime(hour, min);
            updateValue();
        }

        if(min >= 60) {
            hour = hour + 1;
            min = 0;
        }

        if(hour >= 24) {
            hour = 0;
            min = 0;
            day = day + 1;
            generateCache();
        }
    }

    generateCache = function() {
        local __year = floor(day/Config["Calendar"].yearDays);
        local __dayOfTheYear = day - (Config["Calendar"].yearDays * __year);

        local __day = 0, __month = 0, __dayOfMonth = 0, __f = false, __season = "";
        foreach(season in Config["Calendar"]["Seasons"]) {
            if(__f)
                break;

            foreach(daysInMonth in Config["Calendar"][season].months) {
                __day = __day + daysInMonth;
                __month = __month + 1;
                if(__day > __dayOfTheYear) {
                    __dayOfMonth = __day - __dayOfTheYear;
                    __season = season;
                    __f = true;
                    break;
                }
            }
        }

        cache = {
            season = __season,
            year = __year,
            dayOfTheYear = __dayOfTheYear,
            month = __month,
            dayOfMonth = __dayOfMonth,
            dayOfWeek = day % 7,
        }
    }

    sendStartPacket = function(pid) {
        local packet = Packet();
        packet.writeUInt8(PacketSystem);
        packet.writeUInt8(SystemPackets.Time);
        packet.writeInt16(day);
        packet.writeInt16(hour);
        packet.writeInt16(min);
        packet.send(pid, RELIABLE);
    }
}

addEventHandler("onInit", System.TimeGame.onInit.bindenv(System.TimeGame));
addEventHandler("onPlayerJoin", System.TimeGame.sendStartPacket.bindenv(System.TimeGame));

function setTime(hour, min) {
    System.TimeGame.hour = hour;
    System.TimeGame.min = min;

    _setTime(hour, min);

    local packet = Packet();
    packet.writeUInt8(PacketSystem);
    packet.writeUInt8(SystemPackets.Time);
    packet.writeInt16(System.TimeGame.day);
    packet.writeInt16(System.TimeGame.hour);
    packet.writeInt16(System.TimeGame.min);
    packet.sendToAll(RELIABLE);

    System.TimeGame.updateValue();
}

function getTime() {
    return {hour = System.TimeGame.hour, min = System.TimeGame.min, day = System.TimeGame.day};
}

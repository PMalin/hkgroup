
System.Weather <- {
    cache = {
        weather = SystemWeatherType.SunnyDay,
        cloudsColor = {r = 255, g = 255, b = 255},
        sunSize = 1.0,
        sunColor = {r = 255, g = 255, b = 255},
        moonSize = 1.0,
        moonColor = {r = 255, g = 255, b = 255},
    }

    updateWeather = function() {
        local packet = Packet();
        packet.writeUInt8(PacketSystem);
        packet.writeUInt8(SystemPackets.Weather);
        packet.writeInt16(cache.weather);
        packet.writeInt16(cache.cloudsColor.r);
        packet.writeInt16(cache.cloudsColor.g);
        packet.writeInt16(cache.cloudsColor.b);
        packet.writeFloat(cache.sunSize);
        packet.writeInt16(cache.sunColor.r);
        packet.writeInt16(cache.sunColor.g);
        packet.writeInt16(cache.sunColor.b);
        packet.writeFloat(cache.moonSize);
        packet.writeInt16(cache.moonColor.r);
        packet.writeInt16(cache.moonColor.g);
        packet.writeInt16(cache.moonColor.b);
        packet.sendToAll(RELIABLE);
    }

    sendStartPacket = function(pid) {
        local packet = Packet();
        packet.writeUInt8(PacketSystem);
        packet.writeUInt8(SystemPackets.Weather);
        packet.writeInt16(cache.weather);
        packet.writeInt16(cache.cloudsColor.r);
        packet.writeInt16(cache.cloudsColor.g);
        packet.writeInt16(cache.cloudsColor.b);
        packet.writeFloat(cache.sunSize);
        packet.writeInt16(cache.sunColor.r);
        packet.writeInt16(cache.sunColor.g);
        packet.writeInt16(cache.sunColor.b);
        packet.writeFloat(cache.moonSize);
        packet.writeInt16(cache.moonColor.r);
        packet.writeInt16(cache.moonColor.g);
        packet.writeInt16(cache.moonColor.b);
        packet.send(pid, RELIABLE);
    }
}

addEventHandler("onPlayerJoin", System.Weather.sendStartPacket.bindenv(System.Weather));
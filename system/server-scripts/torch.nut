
System.Torch <- {
    players = array(getMaxSlots(), false)
    timers = array(getMaxSlots(), 0)

    change = function(playerId) {
        players[playerId] = !players[playerId];
        if (players[playerId] == true)
            timers[playerId] = 100 + irand(200);
        else
            timers[playerId] = 0;

        local packet = Packet();
        packet.writeUInt8(PacketSystem);
        packet.writeUInt8(SystemPackets.Torch);
        packet.writeInt16(playerId);
        packet.writeBool(players[playerId]);
        packet.sendToAll(RELIABLE)
    }

    onPlayerJoin = function(playerId) {
        foreach(_playerId, player in players) {
            if(player) {
                local packet = Packet();
                packet.writeUInt8(PacketSystem);
                packet.writeUInt8(SystemPackets.Torch);
                packet.writeInt16(_playerId);
                packet.writeBool(true);
                packet.send(playerId, RELIABLE)
            }
        }
    }

    onPlayerDisconnect = function(playerId, reason) {
        players[playerId] = false;

        local packet = Packet();
        packet.writeUInt8(PacketSystem);
        packet.writeUInt8(SystemPackets.Torch);
        packet.writeInt16(playerId);
        packet.writeBool(false);
        packet.sendToAll(RELIABLE)
    }

    onPlayerChangeWeaponMode = function(playerId, oldWeaponMode, newWeaponMode) {
        if(players[playerId] == false)
            return;

        players[playerId] = false;

        local packet = Packet();
        packet.writeUInt8(PacketSystem);
        packet.writeUInt8(SystemPackets.Torch);
        packet.writeInt16(playerId);
        packet.writeBool(false);
        packet.sendToAll(RELIABLE)
    }

    onPlayerHit = function(playerId, killerId, dmg, dmgType) {
        if(players[playerId] == false)
            return;

        players[playerId] = false;

        local packet = Packet();
        packet.writeUInt8(PacketSystem);
        packet.writeUInt8(SystemPackets.Torch);
        packet.writeInt16(playerId);
        packet.writeBool(false);
        packet.sendToAll(RELIABLE)
    }

    onSecond = function(){
        foreach(index, player in players){
            if (player == true){
                timers[index] = timers[index]-1;
                if (timers[index] == 0){
                    addPlayerNotification(index, "Pochodnia si� wypali�a");
                    change(index);
                }
            }
        }
    }
}

addEventHandler("onPlayerJoin", System.Torch.onPlayerJoin.bindenv(System.Torch))
addEventHandler("onPlayerDisconnect", System.Torch.onPlayerDisconnect.bindenv(System.Torch))
addEventHandler("onPlayerHit", System.Torch.onPlayerHit.bindenv(System.Torch))
addEventHandler("onPlayerChangeWeaponMode", System.Torch.onPlayerChangeWeaponMode.bindenv(System.Torch))
addEventHandler("onSecond", System.Torch.onSecond.bindenv(System.Torch))

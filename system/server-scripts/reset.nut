
System.Reset <- {
    resets = [],

    onInit = function() {
        local resetsInDatabase = Query().select("value").from(System.tableName).where(["type = "+SystemTypes.Reset]).all();
        foreach(resetInDatabase in resetsInDatabase) {
           resets.push(resetInDatabase["value"].tointeger())
        }
    }

    function add(playerId) {
        Query().insertInto(System.tableName, ["type", "value"], [SystemTypes.Reset, "'"+playerId+"'"]).execute();
        resets.push(playerId);
    }

    function exist(playerId) {
        if(resets.find(playerId) != null)
            return true;

        return false
    }
}

addEventHandler("onInit", System.Reset.onInit.bindenv(System.Reset));

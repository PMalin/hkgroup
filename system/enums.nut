
const PacketSystem = 102;

enum SystemPackets
{
    Time,
    AddPortal,
    RemovePortal,
    AddEffect,
    RemoveEffect,
    AddMount,
    RemoveMount,
    Torch,
    Bound,
    Weather,
    AddMusic,
    RemoveMusic,
    HideMusic,
    ShowMusic,
}

enum SystemTypes
{
    Time,
    Portal,
    Effect,
    Reset,
}

enum SystemEffectTarget
{
    Player,
    Npc,
    Vob,
}

enum SystemMountType
{

}

enum SystemWeatherType
{
    SunnyDay,
    Snow,
    Rain,
}

System_Effects <- {};

System_Effects["1"] <- "VOB_MAGICBURN";
System_Effects["2"] <- "SPELLFX_ITEMGLIMMER";
System_Effects["3"] <- "DEMENTOR_FX";
System_Effects["4"] <- "FOV_MORPH1";
System_Effects["5"] <- "FOV_MORPH2";
System_Effects["6"] <- "SLOW_MOTION";
System_Effects["7"] <- "FOCUS_HIGHLIGHT";
System_Effects["8"] <- "SLOW_TIME";
System_Effects["9"] <- "SPELLFX_FIRESWORD_ATTACK";
System_Effects["10"] <- "SPELLFX_FIRESWORD_HIT";
System_Effects["11"] <- "SPELLFX_MAGESTAFF1";
System_Effects["12"] <- "SPELLFX_MAGESTAFF2";
System_Effects["13"] <- "SPELLFX_MAGESTAFF3";
System_Effects["14"] <- "SPELLFX_MAGESTAFF4";
System_Effects["15"] <- "SPELLFX_MAGESTAFF5";
System_Effects["16"] <- "SPELLFX_BOW";
System_Effects["17"] <- "SPELLFX_FIREBOW";
System_Effects["18"] <- "SPELLFX_ARROW";
System_Effects["19"] <- "SPELLFX_FIREARROW";
System_Effects["20"] <- "SPELLFX_CROSSBOW";
System_Effects["21"] <- "SPELLFX_BOLT";
System_Effects["22"] <- "spellFX_Whirlwind";
System_Effects["23"] <- "spellFX_Whirlwind_COLLIDE";
System_Effects["24"] <- "spellFX_Whirlwind_Invest";
System_Effects["25"] <- "spellFX_Whirlwind_Sound";
System_Effects["26"] <- "spellFX_Whirlwind_TARGET";
System_Effects["27"] <- "spellFX_Whirlwind_SENDPERCEPTION";
System_Effects["28"] <- "spellFX_INCOVATION_GREEN";
System_Effects["29"] <- "spellFX_INCOVATION_RED";
System_Effects["30"] <- "spellFX_INCOVATION_BLUE";
System_Effects["31"] <- "spellFX_INCOVATION_VIOLET";
System_Effects["32"] <- "spellFX_INCOVATION_WHITE";
System_Effects["33"] <- "spellFX_LIGHTSTAR_VIOLET";
System_Effects["34"] <- "spellFX_LIGHTSTAR_WHITE";
System_Effects["35"] <- "spellFX_LIGHTSTAR_BLUE";
System_Effects["36"] <- "spellFX_LIGHTSTAR_RED";
System_Effects["37"] <- "spellFX_LIGHTSTAR_GREEN";
System_Effects["38"] <- "spellFX_LIGHTSTAR_ORANGE";
System_Effects["39"] <- "spellFX_Innoseye";
System_Effects["40"] <- "spellFX_HealShrine";
System_Effects["41"] <- "spellFX_RingRitual1";
System_Effects["42"] <- "spellFX_RingRitual2";
System_Effects["43"] <- "spellFX_LIGHTSTAR_RingRitual";
System_Effects["44"] <- "spellFX_ItemAusbuddeln";
System_Effects["45"] <- "spellFX_BeliarsWeapon_Upgrate";
System_Effects["46"] <- "spellFX_Maya_Ghost";
System_Effects["47"] <- "spellFX_Fear_WINGS";
System_Effects["48"] <- "spellFX_BELIARSRAGE";
System_Effects["49"] <- "spellFX_BELIARSRAGE_target_Cloud";
System_Effects["50"] <- "spellFX_BELIARSRAGE_COLLIDE";
System_Effects["51"] <- "SLOW_TIME_CHILD";
System_Effects["52"] <- "SLOW_TIME_CHILD2";
System_Effects["53"] <- "sPELLFX_MANAPOTION";
System_Effects["54"] <- "SPELLFX_HEALTHPOTION";
System_Effects["55"] <- "SPELLFX_YELLOWPOTION";
System_Effects["56"] <- "SPELLFX_WEAKGLIMMER";
System_Effects["57"] <- "SPELLFX_WEAKGLIMMER_RED";
System_Effects["58"] <- "SPELLFX_WEAKGLIMMER_BLUE";
System_Effects["59"] <- "SPELLFX_WEAKGLIMMER_YELLOW";
System_Effects["60"] <- "SPELLFX_ITEMSTARS";
System_Effects["61"] <- "SPELLFX_ITEMSTARS_RED";
System_Effects["62"] <- "SPELLFX_ITEMSTARS_BLUE";
System_Effects["63"] <- "SPELLFX_ITEMSTARS_YELLOW";
System_Effects["64"] <- "SPELLFX_GLOW";
System_Effects["65"] <- "SPELLFX_UNDEAD_DRAGON";
System_Effects["66"] <- "SPELLFX_DRAGONEYE_LEFT";
System_Effects["67"] <- "SPELLFX_DRAGONEYE_RIGHT";
System_Effects["68"] <- "spellFX_InvisibleProjectile";
System_Effects["69"] <- "SPELLFX_WATERFIST_CAST";
System_Effects["70"] <- "FX_EARTHQUAKE";

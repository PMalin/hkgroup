
System.Bound <- {
    players = [],

    add = function(playerId) {
        players.push(playerId);
    }

    remove = function(playerId) {
        local rFind = players.find(playerId);
        if(rFind == null)
            return;

        players.remove(rFind);
    }

    isIn = function(playerId) {
        local rFind = players.find(playerId);
        if(rFind == null)
            return false;

        return true;
    }

    onSecond = function() {
        if(isIn(heroId) == false)
            return;

        if(getPlayerWeaponMode(heroId) != 0) {
            setPlayerWeaponMode(heroId, 0);
            addPlayerNotification(heroId, "Jeste� uwi�ziony, nie mo�esz rusza� si�.")
        }
    }
}

addEventHandler("onSecond", System.Bound.onSecond.bindenv(System.Bound))
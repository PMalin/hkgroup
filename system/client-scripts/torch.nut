
System.Torch <- {
    players = array(getMaxSlots(), false)
    bucket = {}

    change = function(playerId, state) {
        players[playerId] = state;

        if(playerId != heroId)
            return;

        if(state) {
            if(isPlayerStreamed(playerId))
                equipTorch(playerId, 1);
            else
                bucket[playerId] <- true;
        }else {
            equipTorch(playerId, 0);
        }
    }

    onRender = function() {
        foreach(playerId, value in bucket) {
            if(players[playerId] == false) {
                bucket.rawdelete(playerId);
                return;
            }

            if(isPlayerStreamed(playerId)) {
                equipTorch(playerId, 1);
                bucket.rawdelete(playerId);
                return;
            }
        }
    }
}

addEventHandler ("onSecond", System.Torch.onRender.bindenv(System.Torch));

function equipTorch(_p, _state) {
    if(_state == 0)
        equipItem(_p, Items.id("ITMI_TORCH"));
    else
        unequipItem(_p, Items.id("ITMI_TORCH"));
}

class ChatScroll 
{
    currentValue = 0
    maxValue = 0;

    scrollTexture = null
    buttonTexture = null

    visible = false;

    constructor(linesLenght)
    {
        this.scrollTexture = Texture(anx(Resolution.x - 40), any(60), anx(20), linesLenght * Chat.lineHeight, "HK_SCROLL.TGA");
        this.buttonTexture = Texture(anx(Resolution.x - 40), any(60), anx(20), any(20), "HK_INDICATOR.TGA");

        this.currentValue = 0;
        this.maxValue = 0;

        this.visible = false;
    }

    function setChatLines(lines)
    {
        this.scrollTexture.setSize(anx(20), lines * Chat.lineHeight)

        refresh();
    }

    function setMaxValue(maximumValue)
    {
        maxValue = maximumValue;
        refresh();
    }

    function getMaxValue()
    {
        return maxValue;
    }

    function move(value)
    {
        currentValue = currentValue + value;

        if(currentValue < 0)
            currentValue = 0;

        if(currentValue >= maxValue)
            currentValue = maxValue;

        refresh();
    }

    function setValue(value)
    {
        currentValue = value;
        refresh();
    }

    function getValue()
    {
        return currentValue;
    }

    function toggle()
    {
        visible ? hide() : show();
    }

    function show(maxValueStart = null)
    {
        visible = true;

        scrollTexture.visible = true;
        buttonTexture.visible = true;

        if(maxValueStart)
            maxValue = maxValueStart;

        refresh();
    }

    function hide()
    {
        visible = false;

        scrollTexture.visible = false;
        buttonTexture.visible = false;
    }

    function slideDown()
    {
        if(currentValue > 0)
            currentValue = 0;
        
        refresh();
    }

    function slideUp()
    {
        if(currentValue < maxValue)
            currentValue = currentValue + 1;

        refresh();
    }

    function reset()
    {
        this.buttonTexture.setPosition(anx(Resolution.x - 40), any(60));
    }

    function refresh()
    {
        if(maxValue <= 0)
            reset();
        else {
            local onePart = abs((scrollTexture.getSize().height - any(20))/maxValue);

            if(currentValue != 0)
                this.buttonTexture.setPosition(anx(Resolution.x - 40), any(60) + onePart * (maxValue - currentValue));
            else
                this.buttonTexture.setPosition(anx(Resolution.x - 40), any(60) + onePart * (maxValue - currentValue));
        }
    }
}


class ChatContainer
{
    id = -1
    bucket = null

    bar = null

    constructor(id)
    {
        this.id = id
        this.bucket = []

        this.bar = GUI.Button(anx(15 + (90*id)), any(15), anx(80), any(40), "HK_TAB_"+Config["ChatLabel"][id]+".TGA", "")
        this.bar.attribute = id;

        this.bar.bind(EventType.Click, function(element) {
            Chat.switchContainer(element.attribute);
        });
    }

    function hideBar() {
        bar.setVisible(false);
    }

    function showBar() {
        bar.setVisible(true);
        bar.setFile("HK_TAB_"+Config["ChatLabel"][id]+".TGA");
        bar.setColor(155, 155, 155);
    }

    function addMessage(text, r, g, b) {
        text = chatTextFormat(text, r, g, b, id);

        foreach(line in text) {
            line.new <- true;
            bucket.insert(0, line)
        }

        if(bucket.len() > 100)
            bucket.remove(100);

        if(id == Chat.activeContainer)
            refresh();
        else
            bar.setFile("HK_TAB_"+Config["ChatLabel"][id]+"_HOVER.TGA");
    }

    function close()
    {
        bar.setFile("HK_TAB_"+Config["ChatLabel"][id]+".TGA");
        bar.setColor(155, 155, 155);

        foreach(line in Chat.lines)
            line.clear();
    }

    function open()
    {
        bar.setFile("HK_TAB_"+Config["ChatLabel"][id]+".TGA");
        bar.setColor(255, 255, 255);

        refresh();
    }

    function refresh()
    {
        local visible = 0;
        local indexScroll = Chat.scroll.getValue();

        for(local i = (Chat.lineLength - 1); i >= 0; i --)
        {
            if(bucket.len() == visible)
                break;

            local object = bucket[visible + indexScroll];
            Chat.lines[i].apply(i, object, object.new);
            object.new = false;
            visible = visible + 1;
        }

        if(bucket.len() <= Chat.lineLength)
            Chat.scroll.hide();
        else
            Chat.scroll.show(bucket.len() - Chat.lineLength);
    }
}

class ChatInput
{
    widthScreen = -1
    activeLine = -1
    counter = -1

    lines = null
    pointer = null

    visible = false

    constructor(width)
    {
        widthScreen = (8129 * width)/100;
        visible = false

        lines = []
        activeLine = 0
        counter = 0

        for(local i = 0; i < 3; i ++)
            lines.append(Draw(anx(70), any(0), ""));

        pointer = Texture(anx(0), any(0), anx(16), any(22), "HK_R.TGA")
    }

    function setWidthOfScreen(width) {
        widthScreen = (8129 * width)/100;
    }

    function toggle()
    {
        visible ? hide() : show();
    }

    function show()
    {
        visible = true

        local offsetY = any(90) + Chat.lineLength * Chat.lineHeight;

        foreach(ind, line in lines) {
            line.setPosition(anx(30), offsetY + any(ind * 30))
            line.text = "";
            line.visible = true;
        }

        pointer.visible = true;
        activeLine = 0;

        updatePointerPosition();
    }

    function hide()
    {
        foreach(line in lines)
            line.visible = false;

        pointer.visible = false;

        visible = false
    }

    function clear() {
        foreach(ind, line in lines) {
            line.text = "";
        }
    }

    function render()
    {
        counter = counter + 1;

        if(counter < 12)
            pointer.alpha = counter * 20;
        else
            pointer.alpha = 255;

        if(counter >= 30)
            counter = 0;
    }

    function addLetter(letter) {
        if(lines[activeLine].width > widthScreen) {
            if(activeLine < 2)
                activeLine = activeLine + 1;
            else
                return;
        }

        updatePointerPosition();

        if(!(isKeyPressed(KEY_LSHIFT) || isKeyPressed(KEY_RSHIFT)))
            letter = changeSpecialCharToNumber(letter);

        lines[activeLine].text += letter;
    }

    function removeLastLetter() {
        if(lines[activeLine].text.len() > 0)
            lines[activeLine].text = lines[activeLine].text.slice(0, lines[activeLine].text.len() - 1);
        else {
            if(activeLine == 0)
                return;

            activeLine = activeLine - 1;
            updatePointerPosition();
        }
    }

    function keyHandler(key)
    {
        local letter = getKeyLetter(key);
        if(letter) {
            if(lines[activeLine].width > widthScreen) {
                if(activeLine < 2)
                    activeLine = activeLine + 1;
                else
                    return;
            }

            updatePointerPosition();

            if(!(isKeyPressed(KEY_LSHIFT) || isKeyPressed(KEY_RSHIFT)))
                letter = changeSpecialCharToNumber(letter);

            lines[activeLine].text += letter;
        }

        if(key == KEY_BACK)
        {
            if(lines[activeLine].text.len() > 0)
                lines[activeLine].text = lines[activeLine].text.slice(0, lines[activeLine].text.len() - 1);
            else {
                if(activeLine == 0)
                    return;

                activeLine = activeLine - 1;
                updatePointerPosition();
            }
        }
    }

    function updatePointerPosition()
    {
        local pos = lines[activeLine].getPosition();
        pointer.setPosition(pos.x - anx(26), pos.y);
    }

    function getText()
    {
        local str = ""
        foreach(line in lines)
            str += line.text;

        return str;
    }
}
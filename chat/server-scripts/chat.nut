
class ChatController
{
    static players = [];

    static function onInit() {
        for(local i = 0; i <= getMaxSlots(); i ++)
            players.push(ChatPlayer(i));
    }

    static function addMute(playerId, muteOption, expirationTime)
    {
        local obj = players[playerId];
        switch(muteOption)
        {
            case PlayerMuteOption.IC:
                obj.blockChat = true;
            break;
            case PlayerMuteOption.OOC:
                obj.blockChatOOC = true;
            break;
            case PlayerMuteOption.ALL:
                obj.blockChat = true;
                obj.blockChatOOC = true;
            break;
        }
        obj.expiration = expirationTime;
    }

    static function onPlayerCommand(playerId, cmd, message)
    {
        local obj = players[playerId];
        obj.checkExpiration();

        switch(cmd)
        {
            case "ja": case "me":
                if(obj.blockChat == true)
                    return;

                local object = Config["ChatMethod"][ChatMethods.Action];
                distanceChat(playerId, ChatType.IC, object.distance, object.r, object.g, object.b, "#[PLAYER:"+playerId+"] "+message+"");
                addChatLog(playerId,message,"me");
            break;
            case "sz": case "wh": case "whisper": case "szept":
                if(obj.blockChat == true)
                    return;

                local object = Config["ChatMethod"][ChatMethods.Whisper];
                distanceChat(playerId, ChatType.IC, object.distance, object.r, object.g, object.b, "[PLAYER:"+playerId+"] szepcze: "+message);
                addChatLog(playerId,message,"whisper");
            break;
            case "k": case "sh": case "shout": case "krzyk":
                if(obj.blockChat == true)
                    return;

                local object = Config["ChatMethod"][ChatMethods.Shout];
                distanceChat(playerId, ChatType.IC, object.distance, object.r, object.g, object.b, "[PLAYER:"+playerId+"] krzyczy: "+message, false);
                addChatLog(playerId,message,"shout");
            break;
            case "env": case "do":
                if(obj.blockChat == true)
                    return;

                local object = Config["ChatMethod"][ChatMethods.Enviroment];
                distanceChat(playerId, ChatType.IC, object.distance, object.r, object.g, object.b, "[PLAYER:"+playerId+"] <"+message+">");
                addChatLog(playerId,message,"do");
            break;
            case "b": case "ooc":
                if(obj.blockChatOOC == true)
                    return;

                local object = Config["ChatMethod"][ChatMethods.OutOfCharacter];
                distanceChat(playerId, ChatType.OOC, object.distance, object.r, object.g, object.b, "[PLAYER:"+playerId+"]: "+message);
                addChatLog(playerId,message,"ooc");
            break;
        }
    }

    static function onPlayerMessage(playerId, message) {
        local containerId = String.firstChar(message).tointeger();
        message = message.slice(1);

        local obj = players[playerId];
        obj.checkExpiration();

        switch(containerId)
        {
            case ChatType.ALL:
                local player = getAdmin(playerId);
                if(player.canUseGlobal() == false)
                    return;

                local object = Config["ChatMethod"][ChatMethods.Global];
                sendMessageToAll(object.r, object.g, object.b, format("%d%s: %s", containerId, "[PLAYER:"+playerId+"]", message));
            break;
            case ChatType.OOC:
                if(obj.blockChatOOC == true)
                    return;

                local object = Config["ChatMethod"][ChatMethods.OutOfCharacter];
                distanceChat(playerId, containerId, object.distance, object.r, object.g, object.b, "[PLAYER:"+playerId+"]: "+message);
                addChatLog(playerId,message,"ooc");
            break;
            default:
                if(obj.blockChat == true)
                    return;

                local object = Config["ChatMethod"][ChatMethods.Normal];
                distanceChat(playerId, containerId, object.distance, object.r, object.g, object.b, "[PLAYER:"+playerId+"] m�wi: "+message);
                addChatLog(playerId,message,"ic");
            break;
        }
    }
}

addEventHandler("onInit", ChatController.onInit.bindenv(ChatController));
addEventHandler("onPlayerMessage", ChatController.onPlayerMessage.bindenv(ChatController));
addEventHandler("onPlayerCommand", ChatController.onPlayerCommand.bindenv(ChatController));


function distanceChat(playerId, containerId, distance, r, g, b, message, yCheck = true) {
    local pos = getPlayerPosition(playerId);
    foreach(player in getPlayers()) {
        if(player.distanceToPosition(pos) <= distance) {
            local minusValue = abs((80 * player.distanceToPosition(pos))/distance);

            local colors = { r = r, g = g, b = b }

            colors.r = colors.r - minusValue;
            if(colors.r < 0) colors.r = 0;

            colors.g = colors.g - minusValue;
            if(colors.g < 0) colors.g = 0;

            colors.b = colors.b - minusValue;
            if(colors.g < 0) colors.g = 0;

            if(yCheck) {
                if(player.distanceInYAxis(pos) <= abs(distance * 0.70))
                    sendMessageToPlayer(player.id, colors.r, colors.g, colors.b, format("%d%s",containerId,message))
            }else
                sendMessageToPlayer(player.id, colors.r, colors.g, colors.b, format("%d%s",containerId,message))
        }
    }
}

function sendMessage(playerId, containerId, r, g, b, message) {
    sendMessageToPlayer(playerId, r, g, b, format("%d%s",containerId,message))
}


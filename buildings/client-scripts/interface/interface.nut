
BuildingInterface <- {
    buildingId = -1,
    bucket = null,
    eggTimer = null,

    prepareWindow = function()
    {
        local result = {};

        result.window <- GUI.Window(anx(Resolution.x/2 - 120), any(Resolution.y/2 - 252), anx(240), any(300), "HK_BOX.TGA", null, false);
        result.buildButton <- GUI.Button(anx(20), any(40), anx(200), any(60), "HK_BUTTON.TGA","Buduj", result.window);
        result.hitButton <- GUI.Button(anx(20), any(120), anx(200), any(60), "HK_BUTTON.TGA","Niszcz", result.window);
        result.closeButton <- GUI.Button(anx(20), any(200), anx(200), any(60), "HK_BUTTON.TGA","Wyjd�", result.window);

        result.buildButton.bind(EventType.Click, function(element) {
            if(getPlayerStamina(heroId) < 1.0) {
                addPlayerNotification(heroId, "Jeste� zbyt zm�czony.")
                return;
            }

            BuildingInterface.eggTimer = EggTimer(5000);
            playAni(heroId, "S_REPAIR_S1");
            BuildingInterface.bucket.window.setVisible(false);
            BuildingInterface.eggTimer.attribute = BuildingInterface.buildingId;
            BuildingInterface.eggTimer.onEnd = function(eleobjectment) {
                BuildingPacket(getBuilding(eleobjectment.attribute)).build();
                BuildingInterface.hide();
            }
        });

        result.hitButton.bind(EventType.Click, function(element) {
            if(getPlayerStamina(heroId) < 1.0) {
                addPlayerNotification(heroId, "Jeste� zbyt zm�czony.")
                return;
            }

            BuildingInterface.eggTimer = EggTimer(5000);
            playAni(heroId, "T_BORINGKICK");
            BuildingInterface.bucket.window.setVisible(false);
            BuildingInterface.eggTimer.attribute = BuildingInterface.buildingId;
            BuildingInterface.eggTimer.onEnd = function(eleobjectment) {
                BuildingPacket(getBuilding(eleobjectment.attribute)).hit();
                BuildingInterface.hide();
            }
        });

        result.closeButton.bind(EventType.Click, function(element) {
            BuildingInterface.hide();
        });

        return result;
    }

    show = function(_buildingId)
    {
        if(bucket == null)
            bucket = prepareWindow();

        buildingId = _buildingId;
        BaseGUI.show();
        ActiveGui = PlayerGUI.Building;
        bucket.window.setVisible(true);
    }

    hide = function()
    {
        bucket.window.setVisible(false);

        buildingId = -1;
        BaseGUI.hide();
        ActiveGui = null;

        playAni(heroId,"S_RUN");
        resetEggTimer();
        eggTimer = null;
    }

    onForceCloseGUI = function()
    {
        if(ActiveGui == PlayerGUI.Building)
            hide();
    }
}

addEventHandler("onForceCloseGUI", BuildingInterface.onForceCloseGUI.bindenv(BuildingInterface));
Bind.addKey(KEY_ESCAPE, BuildingInterface.hide.bindenv(BuildingInterface), PlayerGUI.Building)
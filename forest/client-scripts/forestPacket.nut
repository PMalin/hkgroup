
class ForestPacket
{
    packet = null;
    object = null;

    constructor(forestObject)
    {
        packet = ExtendedPacket(Packets.Forest);
        object = forestObject;
    }

    function deleteForest() {
        packet.writeUInt8(ForestPackets.Delete)
        packet.writeInt32(object.id);
        packet.send(RELIABLE);
    }

    function takeForest() {
        packet.writeUInt8(ForestPackets.Growth)
        packet.writeInt32(object.id);
        packet.send(RELIABLE);
    }
}
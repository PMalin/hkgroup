
ForestInterface <- {
    forestId = -1,
    bucket = null,
    eggTimer = null,

    prepareWindow = function()
    {
        local result = {};

        result.window <- GUI.Window(anx(Resolution.x/2 - 120), any(Resolution.y/2 - 252), anx(240), any(220), "HK_BOX.TGA", null, false);
        result.lumberButton <- GUI.Button(anx(20), any(40), anx(200), any(60), "HK_BUTTON.TGA","Zetnij", result.window);
        result.closeButton <- GUI.Button(anx(20), any(120), anx(200), any(60), "HK_BUTTON.TGA","Wyjd�", result.window);

        result.lumberButton.bind(EventType.Click, function(element) {
            if(getPlayerStamina(heroId) < 1.0) {
                addPlayerNotification(heroId, "Jeste� zbyt zm�czony.")
                return;
            }

            ForestInterface.eggTimer = EggTimer(5000);
            playAni(heroId, "S_LUMBERJACK_S1");
            ForestInterface.bucket.window.setVisible(false);
            ForestInterface.eggTimer.attribute = ForestInterface.forestId;
            ForestInterface.eggTimer.onEnd = function(eleobjectment) {
                ForestPacket(getForest(eleobjectment.attribute)).takeForest();
                ForestInterface.hide();
            }
        });

        result.closeButton.bind(EventType.Click, function(element) {
            ForestInterface.hide();
        });

        return result;
    }

    show = function(_forestId)
    {
        if(bucket == null)
            bucket = prepareWindow();

        forestId = _forestId;
        BaseGUI.show();
        ActiveGui = PlayerGUI.Forest;
        bucket.window.setVisible(true);

        playAni(heroId, "S_LUMBERJACK_S1");
    }

    hide = function()
    {
        bucket.window.setVisible(false);

        forestId = -1;
        BaseGUI.hide();
        ActiveGui = null;

        playAni(heroId,"S_RUN");
        resetEggTimer();
        eggTimer = null;
    }

    onForceCloseGUI = function()
    {
        if(ActiveGui == PlayerGUI.Forest)
            hide();
    }
}

addEventHandler("onForceCloseGUI", ForestInterface.onForceCloseGUI.bindenv(ForestInterface));
Bind.addKey(KEY_ESCAPE, ForestInterface.hide.bindenv(ForestInterface), PlayerGUI.Forest)

class ForestController
{
    static forests = {};

    static function interactWithForest() {
        local pos = getPlayerPosition(heroId);
        local forestId = getNearForest(pos);
        if(forestId == null)
            return;

        if(hasItem("ITMW_HK_1H_AXE_04") == 0) {
            addPlayerNotification(heroId, "Nie masz siekiery.");
            return;
        }

        if(!(getPlayerMeleeWeapon(heroId) == Items.id("ITMW_HK_1H_AXE_04") && getPlayerWeaponMode(heroId) == WEAPONMODE_1HS)) {
            addPlayerNotification(heroId, "Wyci�gnij siekier�.");
            return;
        }

        local forest = forests[forestId];
        if(forest.growth < forest.getScheme().time) {
            addPlayerNotification(heroId, "Drzewo nie uros�o.");
            return;
        }

        ForestInterface.show(forestId);
    }

    static function getNearForest(pos)
    {
        foreach(forestId, forest in forests)
            if(getPositionDifference(pos, forest.position) < 300)
                return forestId;

        return null;
    }

    static function onPacket(packet)
    {
        local typ = packet.readUInt8();

        switch(typ)
        {
            case ForestPackets.Create:
                local id = packet.readInt32();
                local scheme = packet.readString();
                local positionX = packet.readFloat();
                local positionY = packet.readFloat();
                local positionZ = packet.readFloat();
                local rotationX = packet.readFloat();
                local rotationY = packet.readFloat();
                local rotationZ = packet.readFloat();
                local growth = packet.readInt16();

                local obj = Forest(id);
                obj.scheme = scheme;
                obj.position = {x = positionX, y = positionY, z = positionZ};
                obj.rotation = {x = rotationX, y = rotationY, z = rotationZ};
                obj.growth = growth;
                obj.addToWorld();

                forests[id] <- obj;
                callEvent("onForestAdd", id);
            break;
            case ForestPackets.Delete:
                local id = packet.readInt32();

                if(!(id in forests))
                    return;

                getForest(id).remove();
                forests.rawdelete(id);
                callEvent("onForestRemove", id);
            break;
            case ForestPackets.Update:
                local length = packet.readInt16();
                for(local i = 0; i < length; i ++)
                    forests[packet.readInt32()].update(packet.readInt16())
            break;
        }
    }
}

getForest <- @(id) ForestController.forests[id];
getForests <- @() ForestController.forests;

RegisterPacketReceiver(Packets.Forest, ForestController.onPacket.bindenv(ForestController));

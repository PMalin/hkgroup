
class ForestPacket
{
    packet = null;
    object = null;

    constructor(forestObject)
    {
        packet = ExtendedPacket(Packets.Forest);
        object = forestObject;
    }

    function create(targetId = -1) {
        packet.writeUInt8(ForestPackets.Create)
        packet.writeInt32(object.id);
        packet.writeString(object.scheme);
        packet.writeFloat(object.position.x);
        packet.writeFloat(object.position.y);
        packet.writeFloat(object.position.z);
        packet.writeFloat(object.rotation.x);
        packet.writeFloat(object.rotation.y);
        packet.writeFloat(object.rotation.z);
        packet.writeInt16(object.growth);

        if(targetId == -1)
            packet.sendToAll(RELIABLE);
        else
            packet.send(targetId, RELIABLE);
    }

    function deleteForest() {
        packet.writeUInt8(ForestPackets.Delete)
        packet.writeInt32(object.id);
        packet.sendToAll(RELIABLE);
    }
}
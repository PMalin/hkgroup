
class Forest
{
    static ForestTable = "forest";

    id = -1
    position = null
    rotation = -1

    scheme = -1
    growth = -1

    constructor(_id)
    {
        id = _id
        scheme = 0

        position = {
            x = 0,
            y = 0,
            z = 0
        }

        rotation = {
            x = 0,
            y = 0,
            z = 0
        }
        growth = 0
    }

    function onSecond() {
        if(growth >= getScheme().time)
            return false;

        growth = growth + 1;
        return true;
    }

    function update() {
        DB.queryGet("UPDATE "+Forest.ForestTable+" SET growth = "+growth+" WHERE id = "+id);
    }

    function getScheme() {
        return Config["ForestSystem"][scheme];
    }
}
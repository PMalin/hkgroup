
addCommand("draw", function(playerId, params) {
    local args = sscanf("s", params);

    if (!args) {
        sendMessageToPlayer(playerId, 255, 0, 0, ChatType.IC+"Wpisz /draw wiadomosc");
        return;
    }

    local text = args[0];

    if(text.len() > 100) {
        sendMessageToPlayer(playerId, 255, 0, 0, ChatType.IC+"Przekorczy�e� maksymaln� liczb� znak�w, limit to 100!");
        return;
    }

    sendMessageToPlayer(playerId, 0, 255, 0, ChatType.IC+"Poprosi�e� o dodanie drawu o tre�ci:"+text);

    local actionId = ActionDrawController.createActionDraw(playerId, text);

    foreach(adminId, admin in getAdmins()) {
        if(admin.role >= PlayerRole.Mod) {
            sendMessageToPlayer(adminId, 246, 10, 19, ChatType.ADMIN+"Gracz "+getAccount(playerId).username+"("+playerId+") prosi o publikacj� drawu ("+actionId+") o tre�ci: "+args[0]);
        }
    }
})

addCommandAdmin("drawstatus", function(playerId, params) {
    local args = sscanf("ds", params);
    if (!args)
    {
        sendMessageToPlayer(playerId, 255, 0, 0, ChatType.ADMIN+"Wpisz /drawstatus id [accept/delete]");
        return;
    }

    switch(args[1])
    {
        case "accept":
            if(ActionDrawController.activateActionDraw(args[0]))
                sendMessageToPlayer(playerId, 9, 245, 9, ChatType.ADMIN+"Draw zosta� aktywowany.");
            else
                sendMessageToPlayer(playerId, 255, 0, 0, ChatType.ADMIN+"Nie mo�na aktywowa� drawu o podanym id, draw o takim id nie istnieje.");
        break;
        case "delete":
            if(ActionDrawController.deleteActionDraw(args[0]))
                sendMessageToPlayer(playerId, 9, 245, 9, ChatType.ADMIN+"Draw zosta� usuni�ty.");
            else
                sendMessageToPlayer(playerId, 255, 0, 0, ChatType.ADMIN+"Nie mo�na usun�� drawu o podanym id, draw o takim id nie istnieje.");
        break;
        default:
            sendMessageToPlayer(playerId, 255, 0, 0, ChatType.ADMIN+"Wpisz /drawstatus id [accept/delete]");
        break;
    }
}, "Akceptacja lub odrzucenie zapytania o utworzenie action drawu przez gracza.", PlayerRole.Mod)

addCommandAdmin("playerdraws", function(playerId, params) {
    local args = sscanf("d", params);
    if (!args)
    {
        sendMessageToPlayer(playerId, 255, 0, 0, ChatType.ADMIN+"Wpisz /playerdraws id");
        return;
    }

    local id = args[0]

    if (!isPlayerConnected(id))
    {
        sendMessageToPlayer(playerId, 255, 0, 0, ChatType.ADMIN+"Nie istnieje gracz o tym Id.");
        return;
    }

    sendMessageToPlayer(playerId, 255, 85, 9, ChatType.ADMIN+"Lista draw�w gracza "+getAccount(id).username+":");

    local actionDraws = getActionDraws();
    local playerBaseId = getPlayer(playerId).id;

    foreach(actionDraw in actionDraws) {
        if(actionDraw.userId == playerBaseId)
            sendMessageToPlayer(playerId, 160, 160, 160, ChatType.ADMIN+"DrawID:"+actionDraw.id+"("+(actionDraw.active?"aktywny":"nieaktywny")+") o tre�ci:"+actionDraw.text);
    }
}, "Lista draw�w, kt�re utworzy� lub chce utworzy� gracz o danym ID!", PlayerRole.Mod);
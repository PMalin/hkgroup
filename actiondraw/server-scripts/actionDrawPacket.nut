
class ActionDrawPacket
{
    packet = null;
    object = null;

    constructor(actionDrawObject)
    {
        packet = ExtendedPacket(Packets.ActionDraw);
        object = actionDrawObject;
    }

    function create() {
        packet.writeUInt8(ActionDrawPackets.Create)
        packet.writeInt32(object.id);
        packet.writeString(object.text);
        packet.writeString(object.user);
        packet.writeInt8(object.active);
        packet.writeFloat(object.position.x);
        packet.writeFloat(object.position.y);
        packet.writeFloat(object.position.z);

        packet.sendToAll(RELIABLE);
    }

    function deleteActionDraw() {
        packet.writeUInt8(ActionDrawPackets.Delete)
        packet.writeInt32(object.id);
        packet.sendToAll(RELIABLE);
    }

    function activateActionDraw() {
        packet.writeUInt8(ActionDrawPackets.Activate)
        packet.writeInt32(object.id);
        packet.sendToAll(RELIABLE);
    }   
}
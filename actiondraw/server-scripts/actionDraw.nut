
class ActionDraw
{
    static ActionDrawTable = "actiondraw";

    id = -1

    text = null;

    user = null;
    userId = -1;

    active = 0;
    expireAt = 0;

    world = null;
    position = null;

    constructor(_id)
    {
        this.id = _id

        this.text = null;
    
        this.user = null;
        this.userId = -1;

        this.active = 0;
        this.expireAt = 0;

        this.world = "";
        this.position = {x = 0,y = 0,z = 0}
    }
}
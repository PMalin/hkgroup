
class GroundHerb
{
    static GroundHerbTable = "groundherb";

    id = -1
    position = null
    range = null
    scheme = null
    slots = null

    constructor(id) {
        this.id = id;
        this.position = {x = 0, y = 0, z = 0}
        this.range = 0
        this.scheme = null
        this.slots = null
    }

    function onSecond() {
        foreach(index, slot in slots) {
            if(typeof(slot) == "integer") {
                slots[index] = slots[index] - 1;
                if(slot <= 0) {
                    local randomHerbId = rand() % getScheme().len();
                    local randDirectionX = rand() % 10;
                    local randDirectionZ = rand() % 10;

                    local x = position.x;
                    if(randDirectionX <= 5)
                        x = position.x + rand() % range;
                    else
                        x = position.x - rand() % range;

                    local z = position.z;
                    if(randDirectionZ <= 5)
                        z = position.z + rand() % range;
                    else
                        z = position.z - rand() % range;

                    local item = GroundHerbItem(id, randomHerbId, x, position.y + 30, z);
                    slots[index] = item;
                }
            }
        }
    }

    function takeSlot(index) {
        local getSlot = slots[index].getScheme();
        slots[index] = getSlot.cooldown;
    }

    function getScheme() {
        return Config["GroundHerbSystem"][scheme];
    }

    function place() {
        local possibleSlots = abs((range + rand() % 300)/200);
        slots = [];

        for(local i = 0; i <= possibleSlots; i ++) {
            local randomHerbId = rand() % getScheme().len();
            local randDirectionX = rand() % 10;
            local randDirectionZ = rand() % 10;

            local x = position.x;
            if(randDirectionX <= 5)
                x = position.x + rand() % range;
            else
                x = position.x - rand() % range;

            local z = position.z;
            if(randDirectionZ <= 5)
                z = position.z + rand() % range;
            else
                z = position.z - rand() % range;

            local item = GroundHerbItem(id, randomHerbId, x, position.y + 30, z);
            slots.push(item);
        }
    }
}

class GroundHerbItem
{
    groundHerbId = -1
    groundSchemeId = -1
    item = null

    constructor(_groundHerbId, _idFromScheme, x, y, z){
        groundHerbId = _groundHerbId;
        groundSchemeId = _idFromScheme;

        local scheme = getScheme();
        item = ItemsGround.spawn(Items.id(scheme.instance), 1, x, y, z, getServerWorld());
    }

    function getScheme() {
        return getGroundHerb(groundHerbId).getScheme()[groundSchemeId]
    }
}



class GroundHerbController
{
    static groundHerbs = {};

    static function onInit() {
        local groundHerbsInDatabase = Query().select().from(GroundHerb.GroundHerbTable).all();

        foreach(groundHerb in groundHerbsInDatabase)
        {
            if(!(groundHerb.scheme in Config["GroundHerbSystem"]))
                continue;

            local newGroundHerb = GroundHerb(groundHerb.id.tointeger())

            newGroundHerb.scheme = groundHerb.scheme;
            newGroundHerb.range = groundHerb.rangeDistance.tointeger();
            newGroundHerb.position = {x = groundHerb.x.tofloat(), y = groundHerb.y.tofloat(), z = groundHerb.z.tofloat()}

            groundHerbs[newGroundHerb.id] <- newGroundHerb;

            newGroundHerb.place();
        }

        print("We have " + groundHerbs.len() + " places with herbs.");
    }

    static function add(schemeName, x, y, z, range) {
        Query().insertInto(GroundHerb.GroundHerbTable, ["scheme", "rangeDistance", "x", "y", "z"], ["'"+schemeName+"'", range, x, y, z]).execute();

        local object = Query().select().from(GroundHerb.GroundHerbTable).orderBy("id DESC").one();

        local newGroundHerb = GroundHerb(object.id.tointeger())

        newGroundHerb.scheme = object.scheme;
        newGroundHerb.range = object.rangeDistance.tointeger();
        newGroundHerb.position = {x = object.x.tofloat(), y = object.y.tofloat(), z = object.z.tofloat()}

        groundHerbs[newGroundHerb.id] <- newGroundHerb;

        newGroundHerb.place();
    }

    static function onPlayerTakeItem(item) {
        foreach(ground in groundHerbs) {
            foreach(index, slot in ground.slots) {
                if(slot == null)
                    continue;

                if(typeof(slot) == "integer")
                    continue;

                if(slot instanceof GroundHerbItem) {
                    if(slot.item == item)
                        ground.takeSlot(index);
                }
            }
        }
    }

    static function onSecond() {
        foreach(groundHerbId, groundHerb in groundHerbs)
            groundHerb.onSecond();
    }
}


getGroundHerb <- @(id) GroundHerbController.groundHerbs[id];
getGroundHerbs <- @() GroundHerbController.groundHerbs;

addEventHandler("onInit", GroundHerbController.onInit.bindenv(GroundHerbController));
addEventHandler("onSecond", GroundHerbController.onSecond.bindenv(GroundHerbController));
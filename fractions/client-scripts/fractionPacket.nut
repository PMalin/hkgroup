
class FractionPacket
{
    packet = null;

    constructor() {
        packet = ExtendedPacket(Packets.Fraction);
    }

    function setFractionPlayer(playerId, fraction, rankId) {
        packet.writeUInt8(FractionPackets.SetForPlayer)
        packet.writeInt16(playerId);
        packet.writeInt16(fraction);
        packet.writeInt16(rankId);
        packet.send();
    }
}


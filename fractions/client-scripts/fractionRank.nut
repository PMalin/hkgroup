
class FractionRank {
    id = -1
    name = ""

    crafts = null
    factions = null

    canTrade = false
    canInvite = false
    canManage = false

    constructor(_id, _name) {
        id = _id
        name = _name

        crafts = []
        factions = []

        canTrade = false
        canInvite = false
        canManage = false
    }

    function full(value) {
        canTrade = value
        canInvite = value
        canManage = value
    }

    function addFaction(value) {
        factions.push(value.id)
    }

    function addCrafts(value) {
        foreach(name in value)
            crafts.push(name);
    }
}


class FractionController
{
    static fractions = {};

    static function createFraction(fractionId, name) {
        local obj = Fraction();
        obj.name = name;
        obj.id = fractionId;

        fractions[fractionId] <- obj;
        return obj;
    }

    static function onPacket(packet) {

    }
}

getFraction <- @(id) FractionController.fractions[id];
getFractions <- @() FractionController.fractions;

addEventHandler("onPacket",FractionController.onPacket.bindenv(FractionController));

function canPlayerInvite() {
    if(Hero.fractionId == -1)
        return false;

    local fraction = getFraction(Hero.fractionId);
    if(fraction.ranks[Hero.fractionRoleId].canInvite)
        return true;

    if(fraction.ranks[Hero.fractionRoleId].canManage)
        return true;

    return false;
}

function canPlayerManageFraction() {
    if(Hero.fractionId == -1)
        return false;

    local fraction = getFraction(Hero.fractionId);
    if(fraction.ranks[Hero.fractionRoleId].canManage)
        return true;

    return false;
}
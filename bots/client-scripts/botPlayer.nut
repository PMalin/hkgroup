
BotPlayer <- {
    streamed = {},

    add = function(botId)
    {
        if(botId in streamed)
            streamed[botId] = true;
        else
            streamed[botId] <- true;
    }

    remove = function (botId) {
        if(botId in streamed)
            streamed.rawdelete(botId);
    }
}

addEventHandler("onPlayerHit", function(pid, kid, dmg)
{
    local maxSlots = getMaxSlots();
    if(pid in System.Mount.getNpcs())
    {
        eventValue(0);
        return;
    }

    if(pid >= maxSlots || kid >= maxSlots)
    {
        eventValue(0);

        checkMoveForwardChecker(pid, kid);

        if(pid == heroId)
            BotPacket.attackPlayer(getBotElements()[kid], pid);
        else if(kid == heroId)
            BotPacket.attackBot(getBotElements()[pid], kid);
    }
});
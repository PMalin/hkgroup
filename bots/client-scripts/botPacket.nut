

class BotPacket extends Packet
{
    static function sendSynchronization(bots) {
        local packet = Packet();
        packet.writeUInt8(PacketBots);
        packet.writeUInt8(BotPackets.Synchronization);
        packet.writeInt16(bots.len());
        foreach(bot in bots)
        {
            packet.writeInt16(bot.id);
            packet.writeFloat(bot.x);
            packet.writeFloat(bot.y);
            packet.writeFloat(bot.z);
        }
        packet.send(RELIABLE_ORDERED);
    }

    static function attackBot(botId, playerId) {
        local packet = Packet();
        packet.writeUInt8(PacketBots);
        packet.writeUInt8(BotPackets.AttackBot);
        packet.writeInt16(botId);
        packet.writeInt16(playerId);
        packet.send(RELIABLE_ORDERED);
    }

    static function attackPlayer(botId, playerId) {
        local packet = Packet();
        packet.writeUInt8(PacketBots);
        packet.writeUInt8(BotPackets.AttackPlayer);
        packet.writeInt16(botId);
        packet.writeInt16(playerId);
        packet.send(RELIABLE_ORDERED);
    }

    static function jumpForward(botId) {
        local packet = Packet();
        packet.writeUInt8(PacketBots);
        packet.writeUInt8(BotPackets.JumpForward);
        packet.writeInt16(botId);
        packet.send(RELIABLE_ORDERED);
    }

    static function moveForward(botId, playerId) {
        local packet = Packet();
        packet.writeUInt8(PacketBots);
        packet.writeUInt8(BotPackets.MoveForward);
        packet.writeInt16(botId);
        packet.writeInt16(playerId);
        packet.send(RELIABLE_ORDERED);
    }

    static function interactBot(botId) {
        local packet = Packet();
        packet.writeUInt8(PacketBots);
        packet.writeUInt8(BotPackets.Interaction);
        packet.writeInt16(botId);
        packet.send(RELIABLE);
    }

    static function callDialog(botId, dialogId) {
        local packet = Packet();
        packet.writeUInt8(PacketBots);
        packet.writeUInt8(BotPackets.CallDialog);
        packet.writeInt16(botId);
        packet.writeInt16(dialogId);
        packet.send(RELIABLE);
    }

    static function onInteractWithDeadBody(botId) {
        local packet = Packet();
        packet.writeUInt8(PacketBots);
        packet.writeUInt8(BotPackets.InteractDeadBody);
        packet.writeInt16(botId);
        packet.send(RELIABLE);
    }

    static function sendResetDirection(botId) {
        local packet = Packet();
        packet.writeUInt8(PacketBots);
        packet.writeUInt8(BotPackets.ResetDirection);
        packet.writeInt16(botId);
        packet.send(RELIABLE);
    }

    static function endAnimation(animation) {

    }
}
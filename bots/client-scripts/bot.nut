local bots = {};
local elements = {};

class Bot
{
    id = -1

    name = ""
    instance = ""
    group = ""

    element = null

    str = 10
    dex = 10

    hp = 40
    hpMax = 40
    mana = 10
    manaMax = 40

    weapon = null
    animation = ""

    position = null
    direction = null
    angle = 0
    scale = null
    fatness = 0.0

    lvl = 0
    magicLvl = 0
    weaponMode = 0

    melee = -1
    armor = -1
    ranged = -1
    helmet = -1
    shield = -1
    magic = -1

    isTakken = false
    visual = null

    constructor(id, name)
    {
        this.id = id

        this.group = ""
        this.name = name
        this.instance = "PC_HERO"

        this.str = 10
        this.dex = 10

        this.hp = 40
        this.hpMax = 40
        this.mana = 10
        this.manaMax = 40

        this.weapon = [0, 0, 0, 0]
        this.animation = ""

        this.position = {x = 0, y = 0, z = 0}
        this.direction = {x = 0, y = 0, z = 0, distance = 0, mode = 0}
        this.scale = {x = 1.0, y = 1.0, z = 1.0}
        this.angle = 0
        this.fatness = 1.0

        this.lvl = 0
        this.magicLvl = 0
        this.weaponMode = 0

        this.melee = -1
        this.armor = -1
        this.ranged = -1
        this.helmet = -1
        this.shield = -1
        this.magic = -1

        this.isTakken = false
        this.element = createNpc(name)

        this.visual = {bodyModel = "", bodyTxt = 1, headModel = "", headTxt = 1}

        bots[id] <- this;
        elements[this.element] <- id;
    }

    function spawn() {
        if(isTakken)
            return;

        spawnNpc(element, instance);

        setPlayerStrength(element, 200);
        setPlayerDexterity(element, 200);
        setPlayerScale(element, scale.x, scale.y, scale.z);
        setPlayerAngle(element, angle);
        setPlayerPosition(element, position.x, position.y, position.z);
        setPlayerFatness(element, fatness);
        setPlayerHealth(element, hp);
        setPlayerMaxHealth(element, hpMax);
        setPlayerSkillWeapon(element, 0, weapon[0])
        setPlayerSkillWeapon(element, 1, weapon[1])
        setPlayerSkillWeapon(element, 2, weapon[2])
        setPlayerSkillWeapon(element, 3, weapon[3])

        setPlayerMagicLevel(element, 6);
        setPlayerMaxMana(element, mana);
        setPlayerMana(element, manaMax);

        setPlayerCollision(element, true);

        if(instance == "PC_HERO")
        {
            setPlayerVisual(element, visual.bodyModel, visual.bodyTxt, visual.headModel, visual.headTxt);

            if(armor != -1)
                equipArmor(element, armor);
            if(melee != -1)
                equipMeleeWeapon(element, melee);
            if(ranged != -1)
                equipRangedWeapon(element, ranged);
            if(helmet != -1)
                equipHelmet(element, helmet);
            if(shield != -1)
                equipShield(element, shield);

            startFaceAni(element,"S_NEUTRAL");
            setPlayerWeaponMode(element, weaponMode);
        }

        playAni(element, animation);
        callEvent("onNpcSpawn", this);
    }

    function setAngle(value) {
        setPlayerAngle(element, value)
    }

    function setHealth(value) {
        hp = value;
        setPlayerHealth(element, value);
    }

    function setMana(value) {
        mana = value;
        setPlayerMana(element, value);
    }

    function setTakken(value) {
        isTakken = value;

        if(value == true)
            unspawnNpc(element);
        else
            spawn();
    }

    function setPosition(x,y,z)
    {
        position.x = x
        position.y = y
        position.z = z

        local distance = 45.0;
        if(animation == "S_FISTRUNL" || animation == "S_1HRUNL" || animation == "S_2HRUNL" || animation == "S_RUNL")
            distance = 115.0;

        if(positionDifference(getPlayerPosition(element)) > distance)
            setPlayerPosition(element, x,y,z)
    }

   function setDirection(x,y,z, distance, mode) {
        direction = {x = x, y = y, z = z, distance = distance, mode = mode}

        if(!(direction.x == 0 && direction.z == 0)) {
            switch(mode) {
                case BotDirectionMode.Walk:
                    local anim = "S_WALKL";
                    switch(weaponMode)
                    {
                        case WEAPONMODE_FIST: anim = "S_FISTWALKL"; break;
                        case WEAPONMODE_1HS: anim = "S_1HWALKL"; break;
                        case WEAPONMODE_2HS: anim = "S_2HWALKL"; break;
                        case WEAPONMODE_BOW: anim = "S_BOWWALKL"; break;
                        case WEAPONMODE_CBOW: anim = "S_CBOWWALKL"; break;
                        case WEAPONMODE_MAG: anim = "S_1HWALKL"; break;
                    }

                    playAnimation(anim);
                break;
                case BotDirectionMode.Run:
                    local anim = "S_RUNL";

                    switch(weaponMode)
                    {
                        case WEAPONMODE_FIST: anim = "S_FISTRUNL"; break;
                        case WEAPONMODE_1HS: anim = "S_1HRUNL"; break;
                        case WEAPONMODE_2HS: anim = "S_2HRUNL"; break;
                        case WEAPONMODE_BOW: anim = "S_BOWRUNL"; break;
                        case WEAPONMODE_CBOW: anim = "S_CBOWRUNL"; break;
                        case WEAPONMODE_MAG: anim = "S_1HRUNL"; break;
                    }

                    playAnimation(anim);
                break;
            }

            BotAnimations.directionControl(id, element, x, y, z, distance, mode);
        }
    }

    function resetDirection() {
        direction = {x = 0, y = 0, z = 0, distance = 0, mode = 0}
        BotAnimations.directionControlRemove(id);
    }

    function playAnimation(value) {
        if(value == "S_DEAD")
            value = "T_DEAD";

        if(value == "T_CBOWRELOAD" || value == "T_BOWRELOAD" || value == "T_1HATTACKL" || value == "T_1HATTACKR" ||
        value == "S_1HATTACK" || value == "T_2HATTACKL" || value == "T_2HATTACKR" || value == "S_2HATTACK" || value == "S_FISTATTACK")
            stopAni(element);

        animation = value
        playAni(element, animation)

        BotAnimations.change(id, value);
    }

    function isInteractionPossible() {
        if(getPlayerWeaponMode(element) != 0)
            return false;

        if(getPlayerHealth(element) == 0 && getPlayerInstance(element) != "PC_HERO")
            return false;

        if(getPlayerHealth(element) != 0 && getPlayerInstance(element) == "PC_HERO")
            return true;

        return false;
    }

    function setWeaponMode(value) {
        weaponMode = value

        if(getPlayerWeaponMode(element) == value)
            return;

        if(getPlayerWeaponMode(element) != 0)
        {
            setPlayerWeaponMode(element, 0);
            setTimer(function(thiz) {
                setPlayerWeaponMode(thiz.element, thiz.weaponMode)
            }, 500, 1, this);
        }else{
            setPlayerWeaponMode(element, weaponMode)
        }
    }

    function positionDifference(positionToCompare) {
        return getDistance3d(position.x, position.y, position.z, positionToCompare.x, positionToCompare.y, positionToCompare.z);
    }

    function destroyBot() {
        elements.rawdelete(element)
        destroyNpc(element)
        BotPlayer.remove(id);

        bots.rawdelete(id)
    }

    function attackPlayerId(playerId, typeAttack) {
        local posTarget = getPlayerPosition(playerId);
        local posBot = getPlayerPosition(element);
        local currAngle = getVectorAngle(posBot.x,posBot.z,posTarget.x,posTarget.z);

        setPlayerAngle(element, currAngle);
        switch(weaponMode)
        {
            case WEAPONMODE_MAG:
                attackPlayerMagic(element, playerId, magic)
            break;
            case WEAPONMODE_BOW: case WEAPONMODE_CBOW:
                attackPlayerRanged(element, playerId);
            break;
            default:
                if(instance != "PC_HERO")
                    typeAttack = 0;

                attackPlayer(element, playerId, typeAttack);
            break;
        }
    }
}

getBots <- @() bots;
getBotElements <- @() elements;

function getBot(id) {
    if(id in bots)
        return bots[id];

    return null;
}

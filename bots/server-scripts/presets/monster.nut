
class BotMonster extends BotBase
{
    routine = null;

    constructor(name, group = "")
    {
        base.constructor(name);

        this.group = group;
        this.name = name;

        addComponent(BotAnimComponent());
        addComponent(BotStreamComponent());
        addComponent(BotSpawnComponent());
        addComponent(BotFactionComponent());
        addComponent(BotWanderingComponent());
        addComponent(BotCombatComponent());

        //add for faster reach
        routine = addComponent(BotRoutineComponent());

        this.statistics = BEAST_COMBAT;

        //factions
        this.addFaction(BEAST_FACTION);
    }

    function addRoutine(routineObject) {
        return routine.add(routineObject)
    }

    function addInstantDrop(instance) {
        addComponent(BotInstantDropComponent(instance));
    }

    function addDrop(instance) {
        addComponent(BotDropComponent(instance));
    }
}
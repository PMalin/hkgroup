
class BotImportant extends BotBase
{
    routine = null;
    dialog = null;

    constructor(name, group = "")
    {
        base.constructor(name);

        this.name = name;
        this.instance = "PC_HERO";
        this.group = group;

        addComponent(BotAnimComponent());
        addComponent(BotStreamComponent());
        addComponent(BotSpawnComponent());
        addComponent(BotFactionComponent());
        addComponent(BotImportantComponent());

        //add for faster reach
        routine = addComponent(BotRoutineComponent());

        //add dialog for faster reach
        dialog = addComponent(BotDialogComponent());

        this.statistics = HUMAN_COMBAT;

        //factions
        this.addFaction(HUMAN_FACTION);
    }

    function addRoutine(routineObject) {
        return routine.add(routineObject)
    }

    function addHomePosition(x,y,z,angle,animation) {
        setPosition(x,y,z);
        setAngle(angle);
        playAnimation(animation);
        addComponent(BotBackHomeComponent(x,y,z,angle,animation));
    }

    function addDialog(type, label, text) {
        local dialogObj = BotDialog(type, label, text);
        dialog.add(dialogObj);

        return dialogObj;
    }
}
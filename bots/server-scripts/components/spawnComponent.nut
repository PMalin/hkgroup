class BotSpawnComponent extends BotComponent
{
    needToSpawn = false;
    neededRespawnTime = -1;
    ranges = null

    constructor () {
        needToSpawn = false;
        neededRespawnTime = -1;
        ranges = [];

        for(local i = 0; i < getMaxSlots(); i ++)
            ranges.push({close = false, medium = false, large = false});

        events = {
            "playerEnterRange" : "playerEnterRange",
            "playerExitRange" : "playerExitRange",
            "onUpdateAfterCombat" : "onUpdateAfterCombat",
            "onDeathUpdate" : "onDeathUpdate",
            "onLostTarget": "onLostTarget",
            "onDeath": "onDeath",
            "onRespawn": "onRespawn",
        }
    }

    function onRespawn() {
        neededRespawnTime = -1;
        needToSpawn = false;
    }

    function playerEnterRange(playerId) {
        parent.packetManager.spawn(playerId);
    }

    function playerExitRange(playerId) {
        parent.packetManager.unspawn(playerId);

        ranges[playerId] = {close = false, medium = false, large = false};
    }

    function checkContact(playerId) {
        local possitionDifference = parent.positionDifference(getPlayerPosition(playerId));

        if(ranges[playerId].large == true) {
            if(possitionDifference >= 900)
                ranges[playerId].large = false;
        }else {
            if(possitionDifference < 900) {
                ranges[playerId].large = true;
                parent.callComponent("onPlayerEnterCloseRange", playerId);
            }
        }

        if(ranges[playerId].medium == true) {
            if(possitionDifference >= 600)
                ranges[playerId].medium = false;
        }else {
            if(possitionDifference < 600) {
                ranges[playerId].medium = true;
                parent.callComponent("onPlayerEnterMediumCloseRange", playerId);
            }
        }

        if(ranges[playerId].close == true) {
            if(possitionDifference >= 300)
                ranges[playerId].close = false;
        }else {
            if(possitionDifference < 300) {
                ranges[playerId].close = true;
                parent.callComponent("onPlayerEnterVeryCloseRange", playerId);
            }
        }
    }

    function checkFirstContact(playerId) {
        if(parent.positionDifference(getPlayerPosition(playerId)) < 900) {
            firstContact[playerId] = true;
            parent.callComponent("onPlayerEnterCloseRange", playerId);
        }
    }

    function checkUnFirstContact(playerId) {
        if(parent.positionDifference(getPlayerPosition(playerId)) >= 900) {
            firstContact[playerId] = false;
        }
    }

    function onLostTarget() {
        if(parent.weaponMode != 0)
            parent.setWeaponMode(0);

        parent.playAnimation("STOP");
    }

    function onDeath(killerId) {
        if(parent.weaponMode != 0)
            parent.setWeaponMode(0);

        neededRespawnTime = getTickCount() + (parent.respawnTime * 1000);
        needToSpawn = true;
        parent.playAnimation("S_DEAD");
    }

    function onUpdateAfterCombat() {
        if(needToSpawn)
        {
            if(getTickCount() > neededRespawnTime)
                respawnBot();

            parent.setTickCount(getTickCount() + 1000);
        }else {
            parent.callComponent("afterUpdate");

            for(local i = 0; i <= getMaxSlots(); i ++)
            {
                if(isPlayerConnected(i) == false)
                    continue;

                checkContact(i);
            }
        }
    }

    function onDeathUpdate() {
        if(needToSpawn) {
            if(getTickCount() > neededRespawnTime)
                parent.respawn();

            parent.setTickCount(getTickCount() + 1000);
        }
    }
}
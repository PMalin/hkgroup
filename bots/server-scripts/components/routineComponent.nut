
local timers = [];
class BotRoutineComponent extends BotComponent
{
    activeRoutineBehaviour = null
    routines = null

    constructor()
    {
        routines = []
        activeRoutineBehaviour = null

        events = {
            "afterUpdate": "afterUpdate",
        }

        timers.push(this);
    }

    function afterUpdate() {
        local currentTime = getTime();
        currentTime = (currentTime.hour * 60) + currentTime.min;

        if(activeRoutineBehaviour == null)
        {
            foreach(routine in routines)
            {
                local timeStamp = routine.indexTime;
                if(timeStamp <= currentTime)
                {
                    if(checkRoutine(routine) == false)
                        activeRoutineBehaviour = routine;
                    else
                        activeRoutineBehaviour = null;
                }
            }

            if(activeRoutineBehaviour != null)
            {
                activeRoutineBehaviour = BotRoutineBehaviour(this, activeRoutineBehaviour, parent);
                parent.setTickCount(getTickCount() + 500);
                return;
            }

            parent.callComponent("noRoutineFinded");
            parent.setTickCount(getTickCount() + 2000);
            return;
        }

        activeRoutineBehaviour.continueBehaviour();
        parent.setTickCount(getTickCount() + 500);
    }

    function onOfflineUpdate() {
        if(parent.playersInStream.len() > 0)
            return;

        local currentTime = getTime();
        currentTime = (currentTime.hour * 60) + currentTime.min;

        if(activeRoutineBehaviour == null)
        {
            foreach(routine in routines)
            {
                local timeStamp = routine.indexTime;
                if(timeStamp <= currentTime)
                {
                    if(checkRoutine(routine) == false)
                        activeRoutineBehaviour = routine;
                    else
                        activeRoutineBehaviour = null;
                }
            }

            if(activeRoutineBehaviour != null)
                activeRoutineBehaviour = BotRoutineBehaviour(this, activeRoutineBehaviour, parent);
        }else
            activeRoutineBehaviour.continueOfflineBehaviour();
    }

    function onBehaviourStopped() {
        if("onComplete" in activeRoutineBehaviour.routineObject)
            activeRoutineBehaviour.routineObject.onComplete.acall([parent]);

        activeRoutineBehaviour = null;
    }

    function checkRoutine(routineObject) {
        if(parent.simplePositionDifference(routineObject.position) > 100)
            return false;

        if(parent.animation != routineObject.animation)
            return false;

        return true;
    }

    function add(routineObject) {
        routineObject.indexTime <- getTimeIndex(routineObject)
        this.routines.append(routineObject);

        if(this.routines.len() > 1)
        {
            this.routines.sort(function (a,b)
            {
                if (a.indexTime > b.indexTime) return 1;
                if (a.indexTime < b.indexTime) return -1;
                return 0;
            });
        }

        return this.routines[this.routines.len() - 1];
    }

    function getTimeIndex(routineObject) {
        return (routineObject.hour * 60) + routineObject.min;
    }
}

setTimer(function() {
    foreach(timer in timers)
        timer.onOfflineUpdate();
}, 1000, 0);

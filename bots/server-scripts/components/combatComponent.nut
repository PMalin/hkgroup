
class BotCombatComponent extends BotComponent
{
    enemies = null
    cache = null
    activeEnemy = -1
    lastAttack = -1
    lastPlayerAttacked = -1

    counter = 0;
    moveBehaviour = null
    status = null
    startPosition = null

    constructor()
    {
        enemies = {}
        cache = {}
        startPosition = null

        activeEnemy = -1
        lastAttack = -1
        lastPlayerAttacked = -1
        counter = 0

        moveBehaviour = BotMoveBehaviour(this);

        events = {
            "onUpdate": "onUpdate",
            "onAddEnemy": "onAddEnemy",
            "onDirectAttack": "onDirectAttack",
            "exitScene": "exitScene",
            "onDeath": "onDeath",
            "onPlayerBeforeInteract": "onPlayerBeforeInteract",
        }

        status = BotCombatStatus.Wait;
    }

    function resetState() {
        status = BotCombatStatus.Wait;
        enemies.clear();
        activeEnemy = -1
    }

    function exitScene() {
        if(enemies.len() > 0)
            resetState();
    }

    function onAddEnemy(enemyId, type) {
        if(enemyId in enemies)
            enemies[enemyId] = type;
        else
            enemies[enemyId] <- type;
    }

    function onDirectAttack(enemyId, type) {
        if(activeEnemy == -1)
            parent.setTickCount(getTickCount() + 200);

        if(!(enemyId in enemies))
            onAddEnemy(enemyId, type);

        if(lastPlayerAttacked != activeEnemy && enemyId != activeEnemy) {
            lastPlayerAttacked = enemyId;
            activeEnemy = enemyId;
        }

        lastPlayerAttacked = enemyId;
    }

    function onDeath(killerId) {
        resetState();
    }

    function onPlayerBeforeInteract(playerId) {
        if(activeEnemy == -1)
            parent.callComponent("onPlayerInteract", playerId);
    }

    function onUpdate() {
        if(parent.hp == 0) {
            parent.callComponent("onDeathUpdate");
            return;
        }

        if(activeEnemy == -1)
        {
            checkBotEnemies();

            if(activeEnemy != -1)
            {
                parent.playAnimation("STOP");
                return onUpdate();
            }
        } else {
            checkBotEnemyActiveTargetable();

            if(activeEnemy == -1) {
                parent.callComponent("onLostTarget");
                parent.resetDirection();
            }
            else
            {
                if(startPosition == null)
                    startPosition = parent.position;

                parent.setTickCount(getTickCount() + actionRecognizer());
                return;
            }
        }

        parent.callComponent("onUpdateAfterCombat");
    }

    function actionRecognizer() {
        local distanceToTarget = getDistanceToActiveTarget();
        switch(status)
        {
            case BotCombatStatus.Wait:
                if(parent.statistics.warnEnable) {
                    status = BotCombatStatus.Warn;
                    counter = abs(parent.statistics.warnTime/500);
                    moveBehaviour.openWeapon();
                }else
                    status = BotCombatStatus.Run;
            break;
            case BotCombatStatus.Warn:
                if(distanceToTarget > parent.statistics.warnMaxDistance)
                {
                    activeEnemy = -1;
                    return 200;
                }else if(distanceToTarget < parent.statistics.warnMinDistance) {
                    status = BotCombatStatus.Run;
                    return 200;
                }else{
                    setAngleToActiveTarget();
                    moveBehaviour.warn();
                    counter = counter - 1;
                    if(counter <= 0)
                        status = BotCombatStatus.Run;

                    return 500;
                }
            break;
            case BotCombatStatus.Run:
                if(parent.weaponMode == 0)
                    moveBehaviour.openWeapon();

                if(parent.weaponMode == WEAPONMODE_BOW || parent.weaponMode == WEAPONMODE_CBOW || parent.weaponMode == WEAPONMODE_MAG) {
                    status = BotCombatStatus.Attack;
                    return 200;
                }else{
                    if(distanceToTarget < parent.statistics.attackMaxDist)
                    {
                        status = BotCombatStatus.Attack;
                        return 150;
                    }
                }

                local pos = getActiveTargetPosition();
                parent.setDirection(pos.x, pos.y, pos.z, parent.statistics.attackMaxDist, BotDirectionMode.Run);
                return 300;
            break;
            case BotCombatStatus.Attack:
                setAngleToActiveTarget();

                if(parent.weaponMode == 0)
                    moveBehaviour.openWeapon();

                // Distance attack
                if(parent.weaponMode == WEAPONMODE_BOW || parent.weaponMode == WEAPONMODE_CBOW || parent.weaponMode == WEAPONMODE_MAG) {
                    if(distanceToTarget < 600 && moveBehaviour.checkForMeleeWeapon()) {
                        return 500;
                    } else {
                        moveBehaviour.attackPlayer(activeEnemy, enemies[activeEnemy]);

                        local delayMinus = 0;
                        if(parent.weaponMode == WEAPONMODE_BOW)
                            delayMinus = parent.weapon[2];
                        else if(parent.weaponMode == WEAPONMODE_CBOW)
                            delayMinus = parent.weapon[3];
                        else if(parent.weaponMode == WEAPONMODE_MAG)
                            delayMinus = parent.magicLvl * 15;

                        return parent.statistics.attackDelay - (delayMinus * 2);
                    }
                }
                if(distanceToTarget > parent.statistics.attackMaxDist)
                {
                    status = BotCombatStatus.Run;
                    return 150;
                }else if(distanceToTarget < parent.statistics.attackMinDist && parent.statistics.jumpBackActive)
                {
                    moveBehaviour.jumpBack();
                    return parent.statistics.jumpBackTime;
                }

                local chance = irand(100);
                local offset = 0;

                if(parent.statistics.jumpBackActive && offset < chance && (offset + parent.statistics.jumpBackChance) > chance)
                {
                    offset += parent.statistics.jumpBackChance;
                    moveBehaviour.jumpBack();
                    return parent.statistics.jumpBackTime;
                }

                if(parent.statistics.blockActive && offset < chance && (offset + parent.statistics.blockChance) > chance)
                {
                    offset += parent.statistics.blockChance;
                    moveBehaviour.block();
                    return parent.statistics.blockTime;
                }

                lastAttack = lastAttack == ATTACK_SWORD_LEFT ? ATTACK_SWORD_RIGHT : ATTACK_SWORD_LEFT;

                if(chance > 80)
                    lastAttack = -1

                parent.playAnimation("STOP");

                moveBehaviour.attackPlayer(activeEnemy, enemies[activeEnemy], lastAttack);

                local delayMinus = 0;
                if(parent.weaponMode == WEAPONMODE_1HS)
                    delayMinus = parent.weapon[0];
                else if(parent.weaponMode == WEAPONMODE_2HS)
                    delayMinus = parent.weapon[1];

                return parent.statistics.attackDelay - (delayMinus * 2);
            break;
        }

        return 1000;
    }

    function setAngleToActiveTarget() {
        local objectType = enemies[activeEnemy];
        local position = null
        switch(objectType)
        {
            case BotTargetType.Human:
                position = getPlayerPosition(activeEnemy)
            break;
            case BotTargetType.Npc:
                local _bot = getBot(activeEnemy);
                if(_bot == null) {
                    activeEnemy = -1;
                    return;
                }

                position = getBot(activeEnemy).position
            break;
        }
        parent.turnIntoPosition(position)
    }

    function getActiveTargetPosition() {
        local objectType = enemies[activeEnemy];
        local position = null
        switch(objectType)
        {
            case BotTargetType.Human:
                position = getPlayerPosition(activeEnemy)
            break;
            case BotTargetType.Npc:
                position = getBot(activeEnemy).position
            break;
        }
        return position;
    }

    function getDistanceToActiveTarget() {
        local objectType = enemies[activeEnemy];
        switch(objectType)
        {
            case BotTargetType.Human:
                return parent.positionDifference(getPlayerPosition(activeEnemy))
            break;
            case BotTargetType.Npc:
                if(getBot(activeEnemy)){
                    return parent.positionDifference(getBot(activeEnemy).position)
                }
            break;
        }
    }

    function checkBotEnemyActiveTargetable() {
        if(startPosition != null) {
            local distanceToTarget = parent.positionDifference(startPosition);
            if(distanceToTarget > 4000) {
                enemies.clear();
                activeEnemy = -1;
                parent.setPosition(startPosition.x, startPosition.y, startPosition.z);
                startPosition = null;
                return;
            }
        }

        local objectType = enemies[activeEnemy];
        switch(objectType)
        {
            case BotTargetType.Human:
                if(activeEnemy > getMaxSlots()) {
                    enemies[activeEnemy] = BotTargetType.Npc;
                }else {
                    if(getPlayer(activeEnemy).isTargetable() == false || parent.positionDifference(getPlayerPosition(activeEnemy)) > parent.statistics.interactDistance)
                    {
                        activeEnemy = -1;
                        enemies.rawdelete(activeEnemy);
                    }
                }
            break;
            case BotTargetType.Npc:
                if(getBot(activeEnemy) && (getBot(activeEnemy).isTargetable() == false || parent.positionDifference(getBot(activeEnemy).position) > parent.statistics.interactDistance))
                {
                    activeEnemy = -1;
                    enemies.rawdelete(activeEnemy);
                }
            break;
        }
    }

    function checkBotEnemies() {
        if(startPosition != null) {
            local distanceToTarget = parent.positionDifference(startPosition);
            if(distanceToTarget > 4000) {
                enemies.clear();
                activeEnemy = -1;
                parent.setPosition(startPosition.x, startPosition.y, startPosition.z);
                startPosition = null;
                return;
            }

            if(activeEnemy == -1) {
                if(distanceToTarget < 300)
                    startPosition = null;
            }
        }

        foreach(objectId, objectType in enemies)
        {
            switch(objectType)
            {
                case BotTargetType.Human:
                    if(objectId > getMaxSlots()) {
                        enemies.rawdelete(objectId);
                        continue;
                    }

                    if(getPlayer(objectId).isTargetable() == false || parent.positionDifference(getPlayerPosition(objectId)) > parent.statistics.interactDistance)
                        enemies.rawdelete(objectId);
                break;
                case BotTargetType.Npc:
                    local _bot = getBot(objectId);
                    if(_bot == null) {
                        enemies.rawdelete(objectId);
                        continue;
                    }

                    if(_bot.isTargetable() == false || parent.positionDifference(getBot(objectId).position) > parent.statistics.interactDistance)
                        enemies.rawdelete(objectId);
                break;
            }
        }

        local eyeViewPosition = null;
        if(parent.statistics.useEyeView == true)
            eyeViewPosition = parent.getEyeArea(parent.statistics.searchAttackDistance);

        foreach(playerId in parent.playersInStream)
        {
            if(playerId in enemies)
                continue;

            if(getPlayer(playerId).isTargetable() == false)
                continue;

            if(isBotEnemy(playerId, BotTargetType.Human) == false)
                continue;

            local pos = getPlayerPosition(playerId)
            local distance = parent.positionDifference(pos)
            if(distance < parent.statistics.searchAttackDistance)
            {
                if(parent.statistics.useEyeView == true) {
                    if(getDistance2d(pos.x, pos.z, eyeViewPosition.x, eyeViewPosition.z) < parent.statistics.searchAttackDistance && abs(pos.y - eyeViewPosition.y) < 300 ) {
                        enemies[playerId] <- BotTargetType.Human;
                    }else{
                        if(parent.statistics.useSmell == true && distance < 700)
                            enemies[playerId] <- BotTargetType.Human;
                    }
                }
                else
                {
                    enemies[playerId] <- BotTargetType.Human;
                }
            }
        }

        local botsNearest = GridRegistered[parent.world].nearestBots(parent.cell,2);
        foreach(botId, nbmBool in botsNearest)
        {
            if(botId == parent.id)
                continue;

            if(botId in enemies)
                continue;

            if(isBotEnemy(botId, BotTargetType.Npc) == false)
                continue;

            if(getBot(botId).isTargetable() == false)
                continue;

            local pos = getBot(botId).position;
            local distance = parent.positionDifference(pos)
            if(distance < parent.statistics.searchAttackDistance)
            {
                if(parent.statistics.useEyeView == true) {
                    if(getDistance2d(pos.x, pos.z, eyeViewPosition.x, eyeViewPosition.z) < parent.statistics.searchAttackDistance && abs(pos.y - eyeViewPosition.y) < 300 ) {
                        enemies[botId] <- BotTargetType.Npc;
                    }else{
                        if(parent.statistics.useSmell == true && distance < 700)
                            enemies[botId] <- BotTargetType.Npc;
                    }
                }
                else
                {
                    enemies[botId] <- BotTargetType.Npc;
                }
            }
        }

        if(enemies.len() > 0)
        {
            activeEnemy = -1;
            local distanceLeast = 9999;

            foreach(objectId, objectType in enemies)
            {
                switch(objectType)
                {
                    case BotTargetType.Human:
                        local dist = parent.positionDifference(getPlayerPosition(objectId));
                        if(dist < distanceLeast && getPlayerInstance(objectId) == "PC_HERO")
                        {
                            distanceLeast = dist;
                            activeEnemy = objectId;
                        }
                    break;
                    case BotTargetType.Npc:
                        local dist = parent.positionDifference(getBot(objectId).position);
                        if(dist < distanceLeast)
                        {
                            distanceLeast = dist;
                            activeEnemy = objectId;
                        }
                    break;
                }
            }
        }
    }

    function isBotEnemy(id, type) {
        if(!(type in cache))
            cache[type] <- {};

        if(id in cache[type])
            return cache[type][id];

        local botFactions = [];
        foreach(component in parent.components)
        {
            if(component instanceof BotFactionComponent)
            {
                botFactions = component.factions;
                break;
            }
        }

        local oponnentFactions = [];
        switch(type)
        {
            case BotTargetType.Human:
                oponnentFactions = getPlayer(id).factions;
            break;
            case BotTargetType.Npc:
                local bot = getBot(id);
                if(bot == null)
                    return;

                foreach(component in bot.components)
                {
                    if(component instanceof BotFactionComponent)
                    {
                        oponnentFactions = component.factions;
                        break;
                    }
                }
            break;
        }

        local returnValue = false;
        local factionsRegistered = getFactions();
        foreach(faction in botFactions)
        {
            local factionObject = factionsRegistered[faction];
            if(factionObject.searchForEnemies(oponnentFactions))
            {
                returnValue = true;
            }
        }

        cache[type][id] <- returnValue;
        return cache[type][id];
    }
}
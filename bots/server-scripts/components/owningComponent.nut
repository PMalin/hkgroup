local objs = [];

class BotOwningComponent extends BotComponent
{
    ownerId = -1;
    follow = false;

    constructor(_ownerId)
    {
        follow = false;
        ownerId = _ownerId;
        events = {
            "afterUpdate": "afterUpdate",
        }

        objs.push(this);
    }

    function afterUpdate() {
        local ownerPosition = getPlayerPosition(ownerId);
        local distance = parent.positionDifference(ownerPosition);

        if(distance > 1500)
            parent.setPosition(ownerPosition.x + rand() % 200, ownerPosition.y, ownerPosition.z + rand() % 200);
        else {
            if(follow && distance < 300) {
                follow = false;
                parent.playAnimation("STOP");
                parent.setTickCount(getTickCount() + 1000);
                return;
            }

            if(distance > 500) {
                parent.turnIntoPosition(ownerPosition);
                parent.playAnimation("S_FISTRUNL");
                follow = true;
            }else if(distance > 300) {
                parent.turnIntoPosition(ownerPosition);
                parent.playAnimation("S_FISTWALKL");
                follow = true;
            }
        }

        parent.setTickCount(getTickCount() + 500);
    }

    function triggerAttack(triggerId) {
        parent.callComponent("onDirectAttack", triggerId, BotTargetType.Human);
    }

    function triggerBotAttack(triggerId) {
        parent.callComponent("onDirectAttack", triggerId, BotTargetType.Npc);
    }

    function deleteObj() {
        parent.destroyBot();
        objs.remove(objs.find(this));
    }
}

function deleteObjetOnAllObjs(playerId) {
    foreach(obj in objs) {
        if(obj.ownerId == playerId) {
            obj.deleteObj();
            deleteObjetOnAllObjs(playerId);
            return;
        }
    }
    return false;
}

addEventHandler ("onPlayerHit", function (playerId, killerId, dmg, type) {
    
    foreach(obj in objs) {
        if(obj.ownerId == killerId){
            obj.triggerAttack(playerId);
        }
    }
});

addEventHandler ("onPlayerDisconnect", function (playerId, reason) {
    deleteObjetOnAllObjs(playerId);
});

addEventHandler ("onPlayerDead", function (playerId, killerId) {
    deleteObjetOnAllObjs(playerId);
});

local tableOfBots = {}

class BotDropComponent extends BotComponent
{
    dropItem = null;
    secondsLeft = 0;

    constructor(instance)
    {
        dropItem = instance;
        secondsLeft = 0;

        events = {
            "onDeath": "onDeath",
        }
    }

    function onSecond() {
        secondsLeft = secondsLeft - 1;
        if(secondsLeft <= 0) {
            local pos = parent.position;
            parent.isTakken = true;
            parent.packetManager.sendTakken();

            local _item = createItem(dropItem, 1);
            _item.save();

            local itemGround = ItemsGround.spawn(Items.id(dropItem), 1, pos.x, pos.y + 120, pos.z, parent.world);
            tableOfBots.rawdelete(parent.id);
            GarbageCollector.add(GarbageCollectorType.ItemGround, itemGround.id, 600);
            ItemsWorldRegister.add(parent.world, itemGround.id, _item.id);
        }
    }

    function onDeath(killerId) {
        secondsLeft = 0;

        if(parent.isTemporary)
            onSecond();
        else {
            tableOfBots[parent.id] <- this;
            secondsLeft = 5;
        }
    }

    function onSecure(killerId) {
        giveItem(killerId, dropItem, 1);
    }
}

addEventHandler ("onSecond", function () {
    foreach(bot in tableOfBots)
        bot.onSecond();
});
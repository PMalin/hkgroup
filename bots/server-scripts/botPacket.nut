

class BotPacket extends Packet
{
    bot = -1
    packet = null

    constructor(bot)
    {
        base.constructor();

        this.bot = bot;

        writeUInt8(PacketBots);
    }

    function reset() {
        base.reset();

        writeUInt8(PacketBots);
    }

    function destroyBot() {
        reset();

        writeUInt8(BotPackets.Destroy);
        writeInt16(bot.id);

        foreach (playerId in bot.playersInStream)
            send(playerId, RELIABLE);
    }

    function spawn(playerId) {
        reset();

        writeUInt8(BotPackets.Spawn);
        writeInt16(bot.id);
        writeBool(bot.isTakken);
        writeString(bot.name);
        writeString(bot.group);
        writeString(bot.instance);
        writeString(bot.animation);
        writeFloat(bot.position.x);
        writeFloat(bot.position.y);
        writeFloat(bot.position.z);
        writeFloat(bot.scale.x);
        writeFloat(bot.scale.y);
        writeFloat(bot.scale.z);
        writeFloat(bot.angle);
        writeFloat(bot.fatness);
        writeInt32(bot.hp);
        writeInt32(bot.hpMax);
        writeInt32(bot.mana);
        writeInt32(bot.manaMax);
        writeInt16(bot.weapon[0]);
        writeInt16(bot.weapon[1]);
        writeInt16(bot.weapon[2]);
        writeInt16(bot.weapon[3]);
        writeInt16(bot.str);
        writeInt16(bot.dex);
        writeInt16(bot.lvl);
        writeInt16(bot.magicLvl);
        writeInt16(bot.weaponMode);
        writeInt16(bot.melee);
        writeInt16(bot.ranged);
        writeInt16(bot.armor);
        writeInt16(bot.helmet);
        writeInt16(bot.shield);
        writeInt16(bot.magic);
        writeString(bot.visual.bodyModel);
        writeInt16(bot.visual.bodyTxt);
        writeString(bot.visual.headModel);
        writeInt16(bot.visual.headTxt);
        writeFloat(bot.direction.x);
        writeFloat(bot.direction.y);
        writeFloat(bot.direction.z);
        writeFloat(bot.direction.distance);
        writeUInt8(bot.direction.mode);
        send(playerId, RELIABLE);
    }

    function unspawn(playerId) {
        reset();

        writeUInt8(BotPackets.Unspawn);
        writeInt16(bot.id);
        send(playerId, RELIABLE);
    }

    function sendStreamer(streamerId) {
        reset();

        writeUInt8(BotPackets.Streamer);
        writeInt16(bot.id);
        writeInt16(streamerId);

        foreach (playerId in bot.playersInStream)
            send(playerId, RELIABLE);
    }

    function sendAttack(playerId, typeAttack) {
        reset();

        writeUInt8(BotPackets.Attack);
        writeInt16(bot.id);
        writeInt16(playerId);
        writeUInt8(typeAttack);

        foreach (playerId in bot.playersInStream)
            send(playerId, RELIABLE);
    }

    function sendAnimation() {
        reset();

        writeUInt8(BotPackets.Animation);
        writeInt16(bot.id);
        writeString(bot.animation);

        foreach (playerId in bot.playersInStream)
            send(playerId, RELIABLE);
    }

    function sendWeaponMode() {
        reset();

        writeUInt8(BotPackets.WeaponMode);
        writeInt16(bot.id);
        writeInt16(bot.weaponMode);

        foreach (playerId in bot.playersInStream)
            send(playerId, RELIABLE);
    }

    function sendHealth() {
        reset();

        writeUInt8(BotPackets.Health);
        writeInt16(bot.id);
        writeInt32(bot.hp);

        foreach (playerId in bot.playersInStream)
            send(playerId, RELIABLE);
    }

    function sendMana() {
        reset();

        writeUInt8(BotPackets.Mana);
        writeInt16(bot.id);
        writeInt32(bot.mana);

        foreach (playerId in bot.playersInStream)
            send(playerId, RELIABLE);
    }

    function sendAngle() {
        reset();

        writeUInt8(BotPackets.Angle);
        writeInt16(bot.id);
        writeInt16(bot.angle);

        foreach (playerId in bot.playersInStream)
            send(playerId, RELIABLE);
    }

    function sendScale() {
        reset();

        writeUInt8(BotPackets.Scale);
        writeInt16(bot.id);
        writeFloat(bot.scale.x);
        writeFloat(bot.scale.y);
        writeFloat(bot.scale.z);

        foreach (playerId in bot.playersInStream)
            send(playerId, RELIABLE);
    }

    function sendDirection() {
        reset();

        writeUInt8(BotPackets.Direction);
        writeInt16(bot.id);
        writeFloat(bot.direction.x);
        writeFloat(bot.direction.y);
        writeFloat(bot.direction.z);
        writeFloat(bot.direction.distance);
        writeUInt8(bot.direction.mode);

        foreach (playerId in bot.playersInStream)
            send(playerId, RELIABLE);
    }

    function sendResetDirection() {
        reset();

        writeUInt8(BotPackets.ResetDirection);
        writeInt16(bot.id);

        foreach (playerId in bot.playersInStream)
            send(playerId, RELIABLE);
    }

    function sendTakken() {
        reset();

        writeUInt8(BotPackets.Taken);
        writeInt16(bot.id);
        writeBool(bot.isTakken);

        foreach (playerId in bot.playersInStream)
            send(playerId, RELIABLE);
    }

    function sendFatness() {
        reset();

        writeUInt8(BotPackets.Fatness);
        writeInt16(bot.id);
        writeInt16(bot.fatness);

        foreach (playerId in bot.playersInStream)
            send(playerId, RELIABLE);
    }

    function sendSynchronization() {
        reset();

        writeUInt8(BotPackets.Synchronization);
        writeInt16(bot.id);
        writeFloat(bot.position.x);
        writeFloat(bot.position.y);
        writeFloat(bot.position.z);

        foreach (playerId in bot.playersInStream)
            send(playerId, RELIABLE);
    }

    function sendDialogs(playerId, dialogLabels) {
        reset();

        writeUInt8(BotPackets.Dialogs);
        writeInt16(bot.id);
        writeInt16(dialogLabels.len());
        foreach(dialog in dialogLabels) {
            writeInt16(dialog.id);
            writeUInt8(dialog.type);
            writeString(dialog.label);
            writeInt16(dialog.text.len());
            foreach(_text in dialog.text)
                writeString(_text);
        }

        send(playerId, RELIABLE);
    }

    function startDialog(playerId) {
        reset();

        writeUInt8(BotPackets.StartDialog);
        writeInt16(bot.id);

        send(playerId, RELIABLE);
    }

    function closeDialog(playerId) {
        reset();

        writeUInt8(BotPackets.CloseDialog);

        send(playerId, RELIABLE);
    }
}

class BotPlayer
{
    dialogsTable = "playerdialog";

    id = -1;

    visibleBots = null;
    doneDialogs = null;

    constructor(id)
    {
        this.id = id;

        this.visibleBots = {};
        this.doneDialogs = {};
    }

    function addDialog(group, npcname, label)
    {
        if(!(group in doneDialogs))
            doneDialogs[group] <- {};

        if(!(npcname in doneDialogs[group]))
            doneDialogs[group][npcname] <- [];

        if(doneDialogs[group][npcname].find(label) == null)
            doneDialogs[group][npcname].append(label);
    }

    function hasDialog(group, npcname, label)
    {
        if(!(group in doneDialogs))
            return false;

        if(!(npcname in doneDialogs[group]))
            return false;

        return doneDialogs[group][npcname].find(label) != null
    }

    function saveDoneDialogs() {
        local rId = getPlayer(id).rId;
        Query().deleteFrom(BotPlayer.dialogsTable).where(["playerId = "+rId]).execute();

        if(doneDialogs.len() > 0)
        {
            local arr = [];

            foreach(group, _group in doneDialogs)
            {
                foreach(npcname, _npcname in _group)
                {
                    foreach(label in _npcname)
                        arr.append(["'"+group+"'", "'"+npcname+"'", "'"+label+"'", rId]);
                }
            }

            Query().insertInto(BotPlayer.dialogsTable, ["npcgroup", "npcname", "label", "playerId"], arr).execute();
        }
    }

    function loadDoneDialogs() {
        local rId = getPlayer(id).rId;

        foreach(data in Query().select().from(BotPlayer.dialogsTable).where(["playerId = "+rId]).all())
            addDialog(data["npcgroup"], data["npcname"], data["label"]);
    }

    function isFractionAllie(bot) {
        if(!bot.hasComponent(BotFactionComponent))
            return false;

        foreach(_component in bot.components)
            if(_component instanceof BotFactionComponent)
                return _component.isPlayerAllie(id);

        return false;
    }

    function hasFaction(factionObj) {
        local factions = getPlayer(id).getPlayerFactions();
        return factions.find(factionObj) != null;
    }

    static function removeBotFromScene(id) {
        foreach(player in BotController.players) {
            if(id in player.visibleBots)
                player.visibleBots.rawdelete(id);
        }
    }
}




class WayPoints
{
    static all = {};
    static cells = {};

    static function add(world,obj) {
        cells[world] <- {};

        foreach(name, objItem in obj) {
            local cellId = getIndex(objItem.x, objItem.z);
            if(cellId in cells[world])
                cells[world][cellId].push(name);
            else
                cells[world][cellId] <- [name];
        }

        all[world] <- obj;
    }

    static function getIndex(x,z) {
        return (x / 1024.0).tointeger() + "," + (z / 1024.0).tointeger()
    }

    static function nearestChunk(index, radius = 1)
    {
        local pos = split(index, ",")
        local chunk = {
            x = pos[0].tointeger(),
            z = pos[1].tointeger(),
        }
        local chunks = [];

        for (local x = chunk.x - radius, endX = chunk.x + radius; x <= endX; ++x)
            for (local z = chunk.z - radius, endZ = chunk.z + radius; z <= endZ; ++z)
                chunks.append(x + "," + z);

        return chunks;
    }

    static function get(world) {
        return all[world];
    }
}

function getWaypoint(world, name) {
    return WayPoints.get(world)[name];
}

function getNearestWaypoint(world, x, y, z) {
    local nearestWaypoint = null, nearestChunks = WayPoints.nearestChunk(WayPoints.getIndex(x,z), 2), worldPath = WayPoints.get(world);
    local distance = 99999, objectClosest = null;
    foreach(nearestChunk in nearestChunks)
    {
        if(!(nearestChunk in WayPoints.cells[world]))
            continue;

        local cellsPaths = WayPoints.cells[world][nearestChunk];
        foreach(path in cellsPaths)
        {
            local obj = worldPath[path];
            local objDistance = getDistance3d(x,y,z,obj.x,obj.y,obj.z);
            if(objDistance < distance)
            {
                objectClosest = obj;
                objectClosest.name <- path;
                distance = objDistance;
            }
        }
    }

    return objectClosest;
}

class Way
{
    start = "";
    end = "";

    waypoints = [];
    count = 0;

    constructor(world, startWp, endWp)
    {
        this.start = startWp;
        this.end = endWp;

        local worldWaypoints = WayPoints.get(world);

        if(!(startWp in worldWaypoints) || !(endWp in worldWaypoints))
            return null;

        if(startWp == endWp)
            return;

        this.waypoints = generateNodeList(startWp, endWp, worldWaypoints);
        this.count = this.waypoints.len();
    }

    function getWaypoints() {
        return this.waypoints;
    }

    function getCountWaypoints() {
        return this.count;
    }

    function generateNodeList(startWp, endWp, worldWaypoints) {
        local endWaypoint = worldWaypoints[endWp], openList = {}, closedList = {};

        addToOpenList(openList, startWp, 0, null);
        while (openList.len() > 0)
        {
            local currNodeName = getMin(openList);
            local currNode = openList[currNodeName];

            closedList[currNodeName] <- currNode.parent;

            if (currNodeName == endWp)
                return getPath(closedList, currNodeName, endWp);

            expandNode(openList, closedList, currNodeName, endWaypoint, worldWaypoints);
            delete openList[currNodeName];
        }
    }

    function addToOpenList(openList, nodeName, distance, startWaypointName)
	{
		openList[nodeName] <- {["dist"] = distance, ["parent"] = startWaypointName};
	}

    function getMin(openList)
	{
		local lastName = null;
		if (openList.len() > 0)
		{
			local lastDist = -1;
			foreach (name, node in openList)
			{
				if (lastDist == -1 || node.dist <= lastDist)
				{
					lastDist = node.dist;
					lastName = name;
				}
			}
		}
		return lastName;
	}

	function getPath(closedList, startWaypointName, endWaypointName)
	{
		local pathOut = [endWaypointName];
		local startWaypointName = closedList[endWaypointName];
		while (startWaypointName != null)
		{
			pathOut.append(startWaypointName);
			startWaypointName = closedList[startWaypointName];
		}
		return pathOut;
	}

	function expandNode(openList, closedList, startWaypointName, endWaypointName, usedWaypointMap)
	{
		local parentNode = usedWaypointMap[startWaypointName];
		foreach (relatedWaypointName in parentNode.related)
		{
			if (relatedWaypointName in closedList)
				continue;

			local relatedNode = usedWaypointMap[relatedWaypointName];
			local distParent = getDistance3d(relatedNode.x, relatedNode.y, relatedNode.z, parentNode.x, parentNode.y, parentNode.z);
			local distEnd = getDistance3d(relatedNode.x, relatedNode.y, relatedNode.z, endWaypointName.x, endWaypointName.y, endWaypointName.z);

			local dist = distParent + distEnd + openList[startWaypointName].dist;
			if (relatedWaypointName in openList && openList[relatedWaypointName].dist < dist)
				continue;

			addToOpenList(openList, relatedWaypointName, dist, startWaypointName);
		}
	}
}

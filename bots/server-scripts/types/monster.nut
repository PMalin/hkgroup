
BEAST_COMBAT <- BotCombatType();

BEAST_COMBAT.useEyeView = true;
BEAST_COMBAT.useSmell = true;

BEAST_COMBAT.weaponDrawModeEnable = true;
BEAST_COMBAT.maxDistanceChase = 3000;
BEAST_COMBAT.maxOtherEnemyAttacks = 3;

BEAST_COMBAT.warnEnable = true;
BEAST_COMBAT.warnTime = 6000;
BEAST_COMBAT.warnMaxDistance = 1500;
BEAST_COMBAT.warnMinDistance = 900;

BEAST_COMBAT.jumpBackActive = true;
BEAST_COMBAT.jumpBackChance = 20;
BEAST_COMBAT.jumpBackTime = 400;

BEAST_COMBAT.blockActive = false;
BEAST_COMBAT.blockChance = 20;
BEAST_COMBAT.blockTime = 600;

BEAST_COMBAT.attackDelay = 1000;
BEAST_COMBAT.attackMinDist = 150;
BEAST_COMBAT.attackMaxDist = 260;

BEAST_COMBAT.jumpSideActive = true;
BEAST_COMBAT.jumpSideChance = 10;
BEAST_COMBAT.jumpSideTime = 500;

class AdminPlayer
{
    static PlayerTable = "admin";
    static LogTable = "adminlog";

    id = -1;
    accountId = -1;
    role = -1;

    seePw = false;
    seePwRange = null;
    seePwTable = null;

    active = false;
    assetActive = false;
    assetBucket = -1

    constructor(id)
    {
        this.id = id;
        this.accountId = -1
        this.role = PlayerRole.User

        this.seePw = false;
        this.seePwRange = null;
        this.seePwTable = [];

        this.active = false;
        this.assetActive = false;
        this.assetBucket = {};
    }

    function canUseGlobal() {
        if(this.role == PlayerRole.User || this.role == PlayerRole.Patreon || this.role == PlayerRole.Narrator)
            return false;

        if(this.active == false)
            return false;

        return true;
    }

    function canGetNames() {
        if(this.role == PlayerRole.User || this.role == PlayerRole.Patreon || this.role == PlayerRole.Narrator)
            return false;

        if(this.active == false)
            return false;

        return true;
    }

    function afterRoleChange() {
        switch(role) {
            case PlayerRole.Patreon:
                setPlayerColor(id, 240, 198, 25);
            break;
            default:
                setPlayerColor(id, 150, 150, 150);
            break;
        }
    }

    function load(accountLoadId) {
        accountId = accountLoadId;

        local object = Query().select().from(AdminPlayer.PlayerTable).where(["accountId = "+accountId]).one();
        if(object == null)
            return;

        role = object.role.tointeger();
        active = false;

        if(DEVELOPMENT)
            active = true;

        if(role == -1)
            role = PlayerRole.User

        afterRoleChange();

        if(canGetNames() == false)
            return;

        foreach(account in getAccounts()) {
            local playerObject = getPlayer(account.id);
            AdminPacket().sendPlayerRegister(id, account.id, playerObject.name, account.username, playerObject.rId)
        }
    }

    function save() {
        Query().deleteFrom(AdminPlayer.PlayerTable).where(["accountId = "+accountId]).execute();

        if(role != PlayerRole.User)
            Query().insertInto(AdminPlayer.PlayerTable, ["accountId", "role"], [accountId, role]).execute();
    }

    function saveLog(log) {
        local dateObject = date();
        dateObject = dateObject.year+"-"+dateObject.month+"-"+dateObject.day+" "+dateObject.hour+":"+dateObject.min;
        log = dateObject+" "+log;
        Query().insertInto(AdminPlayer.LogTable, ["adminId", "log"], [accountId, "'"+log+"'"]).execute();
    }

    function hasPlayerInPwTable(checkId) {
        foreach(id in seePwTable) {
            if(checkId == id)
                return true;
        }

        return false;
    }

    function reset() {
        id = -1;
        accountId = -1;
        role = PlayerRole.User;
        active = false;

        if(DEVELOPMENT)
            active = true;

        seePw = false;
        seePwRange = null;
        seePwTable = [];
    }
}

class AdminController
{
    static players = [];
    static assets = {};
    static bans = {};

    static function givePlayerAsset(playerId, assetId)  {
        local adminPlayer = players[playerId];

        if(!(assetId in assets))
            return;

        if(adminPlayer.assetActive == false) {
            local visual = getPlayerVisual(playerId);
            adminPlayer.assetActive = true;
            local itemscopy = [];
            foreach(i, item in getPlayer(playerId).inventory)
                itemscopy.push(item);

            adminPlayer.assetBucket = {
                str = getPlayerStrength(playerId),
                dex = getPlayerDexterity(playerId),
                maxHp = getPlayerMaxHealth(playerId),
                maxMana = getPlayerMaxMana(playerId),
                oneh = getPlayerSkillWeapon(playerId, 0),
                twoh = getPlayerSkillWeapon(playerId, 1),
                bow = getPlayerSkillWeapon(playerId, 2),
                cbow = getPlayerSkillWeapon(playerId, 3),
                fatness = getPlayerFatness(playerId),
                inteligence = getPlayerInteligence(playerId),
                instance = getPlayerInstance(playerId),
                magicLvl = getPlayerMagicLevel(playerId),
                bodyModel = visual.bodyModel,
                bodyTxt = visual.bodyTxt,
                headModel = visual.headModel,
                headTxt = visual.headTxt,
                scale = getPlayerScale(playerId),
                items = itemscopy
            };
        }

        System.Effect.onPlayerDisconnect(playerId, -1);
        getPlayer(playerId).effectModule.effects.clear();
        clearInventory(playerId);

        setTimer(function(object) {
            AdminController.assets[object.workingAssetId].transfer(object.workingPlayerId);
        }, 500, 1, {workingAssetId = assetId, workingPlayerId = playerId});
    }

    static function revertPlayerAsset(playerId) {
        local adminPlayer = players[playerId];

        if(adminPlayer.assetActive == false)
            return;

        clearInventory(playerId);

        setPlayerInstance(playerId, adminPlayer.assetBucket.instance);
        setPlayerInteligence(playerId, adminPlayer.assetBucket.inteligence);
        setPlayerStrength(playerId, adminPlayer.assetBucket.str);
        setPlayerDexterity(playerId, adminPlayer.assetBucket.dex);
        setPlayerMaxHealth(playerId, adminPlayer.assetBucket.maxHp);
        setPlayerHealth(playerId, adminPlayer.assetBucket.maxHp);
        setPlayerMana(playerId, adminPlayer.assetBucket.maxMana);
        setPlayerMaxMana(playerId, adminPlayer.assetBucket.maxMana);
        setPlayerMagicLevel(playerId, adminPlayer.assetBucket.magicLvl);
        setPlayerScale(playerId, adminPlayer.assetBucket.scale.x, adminPlayer.assetBucket.scale.y, adminPlayer.assetBucket.scale.z);
        setPlayerVisual(playerId, adminPlayer.assetBucket.bodyModel, adminPlayer.assetBucket.bodyTxt, adminPlayer.assetBucket.headModel, adminPlayer.assetBucket.headTxt);
        setPlayerSkillWeapon(playerId, 0, adminPlayer.assetBucket.oneh);
        setPlayerSkillWeapon(playerId, 1, adminPlayer.assetBucket.twoh);
        setPlayerSkillWeapon(playerId, 2, adminPlayer.assetBucket.bow);
        setPlayerSkillWeapon(playerId, 3, adminPlayer.assetBucket.cbow);
        setPlayerFatness(playerId, adminPlayer.assetBucket.fatness);

        foreach(v in adminPlayer.assetBucket.items) {
            giveItem(playerId, v.instance, v.amount);
            if(v.equipped == 1)
                equipItem(playerId, v.instance);
        }

        adminPlayer.assetActive = false;
        adminPlayer.assetBucket = null;
    }

    static function addPlayerAssetFromPlayerImage(playerId, name) {
        local visual = getPlayerVisual(playerId);
        local str = getPlayerStrength(playerId),
        dex = getPlayerDexterity(playerId),
        maxHp = getPlayerMaxHealth(playerId),
        maxMana = getPlayerMaxMana(playerId),
        oneh = getPlayerSkillWeapon(playerId, 0),
        twoh = getPlayerSkillWeapon(playerId, 1),
        bow = getPlayerSkillWeapon(playerId, 2),
        cbow = getPlayerSkillWeapon(playerId, 3),
        fatness = getPlayerFatness(playerId),
        inteligence = getPlayerInteligence(playerId),
        instance = getPlayerInstance(playerId),
        magicLvl = getPlayerMagicLevel(playerId),
        bodyModel = visual.bodyModel,
        bodyTxt = visual.bodyTxt,
        headModel = visual.headModel,
        headTxt = visual.headTxt,
        scale = getPlayerScale(playerId),
        items = clone getPlayer(playerId).inventory;

        local effects = System.Effect.getForPlayer(playerId);
        local effectsString = "";
        foreach(effect in effects)
            effectsString += effect.effectId + " " + effect.refreshTime + ",";

        if(effectsString.len() > 1)
            effectsString = effectsString.slice(0,-1);

        local itemsString = "";
        foreach(item in items)
            itemsString += item.equipped + " " + item.amount + " " + item.instance + ",";

        if(itemsString.len() > 1)
            itemsString = itemsString.slice(0,-1);

        local dateObject = date();
        local createdAt = dateObject.year+"-"+dateObject.month+"-"+dateObject.day+" "+dateObject.hour+":"+dateObject.min;
        local createdBy = getAccount(playerId).username;

        Query().insertInto(AdminAsset.AssetTable, [
            "name", "instance", "str", "dex", "maxHp", "maxMana", "oneh", "twoh", "bow", "cbow",
            "fatness", "inteligence", "magicLvl", "bodyModel", "bodyTxt", "headModel", "headTxt",
            "scaleX", "scaleY", "scaleZ", "items", "effects", "createdAt", "createdBy"
        ], [
            "'"+name+"'", "'"+instance+"'", str, dex, maxHp, maxMana, oneh, twoh, bow, cbow,
            fatness, inteligence, magicLvl, "'"+bodyModel+"'", bodyTxt, "'"+headModel+"'", headTxt,
            scale.x, scale.y, scale.z, "'"+itemsString+"'", "'"+effectsString+"'", "'"+createdAt+"'", "'"+createdBy+"'"
        ]).execute();

        local object = Query().select().from(AdminAsset.AssetTable).orderBy("id DESC").one();

        local newAsset = AdminAsset(object.id.tointeger())

        newAsset.name = object.name;
        newAsset.instance = object.instance;
        newAsset.str = object.str.tointeger();
        newAsset.dex = object.dex.tointeger();
        newAsset.maxHp = object.maxHp.tointeger();
        newAsset.maxMana = object.maxMana.tointeger();
        newAsset.oneh = object.oneh.tointeger();
        newAsset.twoh = object.twoh.tointeger();
        newAsset.bow = object.bow.tointeger();
        newAsset.cbow = object.cbow.tointeger();
        newAsset.fatness = object.fatness.tointeger();
        newAsset.inteligence = object.inteligence.tointeger();
        newAsset.magicLvl = object.magicLvl.tointeger();
        newAsset.bodyModel = object.bodyModel;
        newAsset.bodyTxt = object.bodyTxt.tointeger();
        newAsset.headModel = object.headModel;
        newAsset.headTxt = object.headTxt.tointeger();
        newAsset.headModel = object.headModel;

        local items = [], effects = [];

        if(object.items.len() > 1) {
            object.items = split(object.items, ",");
            foreach(params in object.items) {
                local args = sscanf("dds", params);
                if (args)
                    items.push({instance = args[2], amount = args[1], equipped = args[0]})
            }
        }

        if(object.effects.len() > 1) {
            object.effects = split(object.effects, ",");
            foreach(params in object.effects) {
                local args = sscanf("dd", params);
                if (args)
                    effects.push({refreshTime = args[1], effectId = args[0]})
            }
        }

        newAsset.items = items;
        newAsset.effects = effects;
        newAsset.scale = {x = object.scaleX.tofloat(), y = object.scaleY.tofloat(), z = object.scaleZ.tofloat()}

        newAsset.createdAt = object.createdAt;
        newAsset.createdBy = object.createdBy;

        assets[newAsset.id] <- newAsset;
    }

    static function onInit() {
        for(local i = 0; i < getMaxSlots(); i ++)
            players.push(AdminPlayer(i));

        local assetsInDatabase = Query().select().from(AdminAsset.AssetTable).all();

        foreach(assetsObject in assetsInDatabase)
        {
            local newAsset = AdminAsset(assetsObject.id.tointeger())

            newAsset.name = assetsObject.name;
            newAsset.instance = assetsObject.instance;
            newAsset.str = assetsObject.str.tointeger();
            newAsset.dex = assetsObject.dex.tointeger();
            newAsset.maxHp = assetsObject.maxHp.tointeger();
            newAsset.maxMana = assetsObject.maxMana.tointeger();
            newAsset.oneh = assetsObject.oneh.tointeger();
            newAsset.twoh = assetsObject.twoh.tointeger();
            newAsset.bow = assetsObject.bow.tointeger();
            newAsset.cbow = assetsObject.cbow.tointeger();
            newAsset.fatness = assetsObject.fatness.tointeger();
            newAsset.inteligence = assetsObject.inteligence.tointeger();
            newAsset.magicLvl = assetsObject.magicLvl.tointeger();
            newAsset.bodyModel = assetsObject.bodyModel;
            newAsset.bodyTxt = assetsObject.bodyTxt.tointeger();
            newAsset.headModel = assetsObject.headModel;
            newAsset.headTxt = assetsObject.headTxt.tointeger();
            newAsset.headModel = assetsObject.headModel;

            local items = [], effects = [];

            if(assetsObject.items.len() > 1) {
                assetsObject.items = split(assetsObject.items, ",");
                foreach(params in assetsObject.items) {
                    local args = sscanf("dds", params);
                    if (args)
                        items.push({instance = args[2], amount = args[1], equipped = args[0]})
                }
            }

            if(assetsObject.effects.len() > 1) {
                assetsObject.effects = split(assetsObject.effects, ",");
                foreach(params in assetsObject.effects) {
                    local args = sscanf("dd", params);
                    if (args)
                        effects.push({refreshTime = args[1], effectId = args[0]})
                }
            }

            newAsset.items = items;
            newAsset.effects = effects;
            newAsset.scale = {x = assetsObject.scaleX.tofloat(), y = assetsObject.scaleY.tofloat(), z = assetsObject.scaleZ.tofloat()}

            newAsset.createdAt = assetsObject.createdAt;
            newAsset.createdBy = assetsObject.createdBy;

            assets[newAsset.id] <- newAsset;
        }

        print("We have " + assets.len() + " admin assets.");
    }

    static function onPlayerLoad(playerId) {
        local playerObject = getPlayer(playerId);
        foreach(account in getAccounts())
        {
            if(getAdmin(account.id).role != PlayerRole.User)
                AdminPacket().sendPlayerRegister(account.id, playerId, playerObject.name, getAccount(playerId).username, playerObject.rId)
        }
    }

    static function onAccountLoad(playerId) {
        players[playerId].load(getAccount(playerId).rId);
        AdminPacket().setRole(playerId, players[playerId].role);
    }

    static function onPlayerDisconnect(playerId, reason) {
        players[playerId].save();
        players[playerId].reset();
    }

    static function onPacket(playerId, packet) {
        local typ = packet.readUInt8();

        if(DEVELOPMENT == false) {
            local player = getAdmin(playerId);
            if(player.canUseGlobal() == false)
                return;
        }

        switch(typ)
        {
            case AdminPackets.GetItems:
                local player = getPlayer(packet.readUInt16());
                if(player == null)
                    return;

                if(player.loggIn == false)
                    return;

                AdminPacket().sendPlayerItems(playerId, player.inventory.getItems());
            break;
            case AdminPackets.GetFractionMembers:
                local fractionId = packet.readInt16();
                local membersToSend = {};
                local members = Query().select(["id", "name", "fractionRoleId"]).from(Player.dbTable).where(["fractionId = "+fractionId]).all()
                foreach(member in members)
                    membersToSend[member.id] <- {rank = member.fractionRoleId, name = member.name}

                foreach(_player in getPlayers()) {
                    if(_player.fractionId == fractionId) {
                        if(_player.rId in membersToSend)
                            membersToSend.rawdelete(_player.rId);

                        membersToSend[_player.rId] <- {rank = _player.fractionRoleId, name = _player.name}
                    }
                }

                local packet = ExtendedPacket(Packets.Admin);
                packet.writeUInt8(AdminPackets.GetFractionMembers);
                packet.writeInt16(membersToSend.len());
                foreach(memberId, member in membersToSend) {
                    packet.writeInt32(memberId);
                    packet.writeInt16(member.rank);
                    packet.writeString(member.name);
                }
                packet.send(playerId, RELIABLE);
            break;
            case AdminPackets.AddFractionMember:
                local fractionId = packet.readInt16(), playerId = packet.readInt16(), rankId = packet.readInt16();
                if(rankId == -1)
                    fractionId = -1;

                setPlayerFraction(playerId, fractionId, rankId);
            break;
            case AdminPackets.RemoveFractionMember:
                local fractionId = packet.readInt16(), realPlayerId = packet.readInt32();
                Query().update(Player.dbTable, ["fractionId", "fractionRoleId"], [-1, -1]).where(["id = "+realPlayerId]).execute();
                foreach(player in getPlayers()) {
                    if(player.rId != -1 && player.rId == realPlayerId) {
                        setPlayerFraction(player.id, -1, -1);
                        break;
                    }
                }
            break;
            case AdminPackets.UpdateFractionMember:
                local fractionId = packet.readInt16(), realPlayerId = packet.readInt32(), rankId = packet.readInt16();
                Query().update(Player.dbTable, ["fractionId", "fractionRoleId"], [fractionId, rankId]).where(["id = "+realPlayerId]).execute();
                foreach(player in getPlayers()) {
                    if(player.rId != -1 && player.rId == realPlayerId) {
                        setPlayerFraction(player.id, fractionId, rankId);
                        break;
                    }
                }
            break;
            case AdminPackets.GetAssets:
                foreach(asset in assets) {
                    AdminPacket().sendAsset(playerId, asset);
                }
            break;
            case AdminPackets.AddAsset:
                addPlayerAssetFromPlayerImage(playerId, packet.readString())
                PlayerPacket(playerId).addNotification("Dodano asset.");
            break;
            case AdminPackets.DeleteAsset:
                local id = packet.readInt16();
                if(id in assets)
                    assets.rawdelete(id);

                PlayerPacket(playerId).addNotification("Usuni�to asset.");
                Query().deleteFrom(AdminAsset.AssetTable).where(["id = "+id]).execute();
            break;
            case AdminPackets.GetCrafts:
                CraftController.sendAll(playerId);
            break;
            case AdminPackets.GetCraft:
                CraftController.send(playerId, packet.readInt16());
            break;
            case AdminPackets.Inivisibility:
                local invisible = packet.readBool();
                setPlayerInvisible(playerId, invisible)
                PlayerPacket(playerId).setInvisible(invisible);
            break;
            case AdminPackets.ItemRegister:
                local instance = packet.readString();
                local damage = packet.readInt16();
                local value = packet.readInt16();
                local range = packet.readInt16();
                local condvalue = packet.readInt16();
                local name = packet.readString();
                local visual = packet.readString();
                local description = packet.readString();
                local material = packet.readInt16();
                local mag_circle = packet.readInt16();
                local damageType = packet.readInt16();
                local protection0 = packet.readInt16();
                local protection1 = packet.readInt16();
                local protection2 = packet.readInt16();
                local protection3 = packet.readInt16();
                local protection4 = packet.readInt16();
                local protection5 = packet.readInt16();
                local protection6 = packet.readInt16();

                print("registerItem(\""+instance+"\", {");
                print("//name=\""+name+"\",");
                print("//damage="+damage+",");
                print("//value="+value+",");
                print("//range="+range+",");
                print("//condvalue="+condvalue+",");
                print("//visual=\""+visual+"\",");
                print("//description=\""+description+"\",");
                print("//material="+material+",");
                print("//mag_circle="+mag_circle+",");
                print("//damageType="+damageType+",");
                print("//protection=["+protection0+","+protection1+","+protection2+","+protection3+","+protection4+","+protection5+","+protection6+"],");
                print("})")
            break;
            case AdminPackets.GetBots:
                foreach(bot in getBots()) {
                    AdminPacket().sendBot(playerId, bot);
                }
            break;
            case AdminPackets.GetPlayers:
                for(local i = 0; i <= getMaxSlots(); i ++)
                    if(isPlayerConnected(i))
                        AdminPacket().sendPlayer(playerId, i);
            break;
            case AdminPackets.RemoveBot:
                local bot = getBot(packet.readInt16());
                if(bot) {
                    bot.packetManager.destroyBot();
                    bot.destroyBot();
                    PlayerPacket(playerId).addNotification("Uda�o si� usun�� npc.");
                    getAdmin(playerId).saveLog("Usun�� NPC("+bot.id+") "+bot.name+" "+bot.group);
                }
            break;
            case AdminPackets.CreateBot:
                local group = packet.readString();
                local name = packet.readString();
                local instance = packet.readString();
                local object = null;

                if(instance == "PC_HERO")
                    object = BotHuman(name, group);
                else
                    object = BotMonster(name);

                object.group = group;
                object.instance = instance;
                object.isTemporary = packet.readBool();
                object.isSaved = packet.readBool();
                object.position = {x = packet.readFloat(), y = packet.readFloat(), z = packet.readFloat()}
                object.setPosition(object.position.x,object.position.y,object.position.z);
                object.setAngle(packet.readInt16());
                object.str = packet.readInt16();
                object.dex = packet.readInt16();
                object.hp = packet.readInt32();
                object.hpMax = packet.readInt32();
                object.scale = {x = packet.readFloat(), y = packet.readFloat(), z = packet.readFloat()}
                object.animation = packet.readString();
                object.weapon = [packet.readInt16(),packet.readInt16(),packet.readInt16(),packet.readInt16()]
                object.respawnTime = packet.readInt16();
                object.melee = packet.readInt16();
                object.armor = packet.readInt16();
                object.ranged = packet.readInt16();
                object.helmet = packet.readInt16();
                object.shield = packet.readInt16();
                object.magic = packet.readInt16();
                object.visual = {bodyModel = packet.readString(), bodyTxt = packet.readInt16(), headModel = packet.readString(), headTxt = packet.readInt16()}

                local effectsLen = packet.readInt16();
                for(local i = 0; i < effectsLen; i ++) {
                    object.addEffect(packet.readInt16(), packet.readInt16());
                }

                AdminPacket().sendBot(playerId, object);
                PlayerPacket(playerId).addNotification("Uda�o si� doda� npc.");
                getAdmin(playerId).saveLog("Doda� NPC("+object.id+") "+object.name+" "+object.group);
            break;
            case AdminPackets.UpdateBot:
                local bot = getBot(packet.readInt16());
                if(bot == null)
                    return;

                local group = packet.readString();
                local name = packet.readString();
                local instance = packet.readString();


                bot.group = group;
                bot.instance = instance;
                bot.isTemporary = packet.readBool();
                bot.isSaved = packet.readBool();
                bot.position = {x = packet.readFloat(), y = packet.readFloat(), z = packet.readFloat()}
                bot.setPosition(bot.position.x,bot.position.y,bot.position.z);
                bot.setAngle(packet.readInt16());
                bot.str = packet.readInt16();
                bot.dex = packet.readInt16();
                bot.hp = packet.readInt32();
                bot.hpMax = packet.readInt32();
                bot.scale = {x = packet.readFloat(), y = packet.readFloat(), z = packet.readFloat()}
                bot.animation = packet.readString();
                bot.weapon = [packet.readInt16(),packet.readInt16(),packet.readInt16(),packet.readInt16()]
                bot.respawnTime = packet.readInt16();
                bot.melee = packet.readInt16();
                bot.armor = packet.readInt16();
                bot.ranged = packet.readInt16();
                bot.helmet = packet.readInt16();
                bot.shield = packet.readInt16();
                bot.magic = packet.readInt16();
                bot.visual = {bodyModel = packet.readString(), bodyTxt = packet.readInt16(), headModel = packet.readString(), headTxt = packet.readInt16()}

                foreach(effect in bot.effects) {
                    bot.removeEffect(effect);
                }

                local effectsLen = packet.readInt16();
                for(local i = 0; i < effectsLen; i ++) {
                    bot.addEffect(packet.readInt16(), packet.readInt16());
                }

                foreach (odderId in bot.playersInStream)
                    bot.packetManager.unspawn(odderId);

                foreach (odderId in bot.playersInStream)
                    bot.packetManager.spawn(odderId);

                AdminPacket().sendBot(playerId, bot);
                PlayerPacket(playerId).addNotification("Uda�o si� zaaktualizowa� npc.");

                getAdmin(playerId).saveLog("Zaaktualizowa� NPC("+bot.id+") "+bot.name+" "+bot.group);
            break;
            case AdminPackets.Question:
                local questionId = packet.readInt16();
                local answerId = packet.readInt16();

                foreach(question in Config["Questions"])
                    if(question.id == questionId)
                        if(question.answers[answerId][1] == false)
                            kick(playerId, "Z�a odpowied�. Zapraszamy do naszych poradnik�w na discord.")
            break;
            case AdminPackets.DeleteItem:
                local itemId = packet.readUInt32();
                local _item = getItem(itemId);

                if(_item == null)
                    return;

                item.Controller.deleteCorespondingItemBucket(itemId);
                item.Controller.removeItem(itemId);
                PlayerPacket(playerId).addNotification("Usuni�to item.");
            break;
            case AdminPackets.UpdateItem:
                local itemId = packet.readUInt32();
                local _item = getItem(itemId);

                if(_item == null)
                    return;

                _item.name = packet.readString();
                _item.description = packet.readString();
                _item.guid = packet.readString();
                _item.amount = packet.readInt16();
                _item.resistance = packet.readInt16();
                _item.save();

                item.Controller.updateCorespondingItemBucket(itemId);
                PlayerPacket(playerId).addNotification("Zaaktualizowano item.");
            break;
            case AdminPackets.SaveToLog:
                local _index = packet.readInt16();
                local rfile = io.file("database/adminlog.txt", "a+");

                for(local i = 0; i < _index; i++)
                    rfile.write(packet.readString() + "\n");

                rfile.close();
            break;
        }
    }
}

getAdmin <- @(id) AdminController.players[id];
getAdmins <- @() AdminController.players;

RegisterPacketReceiver(Packets.Admin, AdminController.onPacket.bindenv(AdminController));

addEventHandler("onPlayerLoad", AdminController.onPlayerLoad.bindenv(AdminController))
addEventHandler("onInit", AdminController.onInit.bindenv(AdminController))
addEventHandler("onAccountLoad", AdminController.onAccountLoad.bindenv(AdminController))
addEventHandler("onPlayerDisconnect", AdminController.onPlayerDisconnect.bindenv(AdminController))
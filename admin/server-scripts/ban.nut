
function ban(id, minutes, reason) {
    local mac = getPlayerMacAddr(id), serial = getPlayerSerial(id), pid = id;
    local acc = getAccount(id);
    if(acc.rId == -1)
        return;

    id = acc.rId;
    if(id in AdminController.bans)
        return;

    local timeout = 0;
    if(minutes != 0)
        timeout = time() + ( minutes * 60);

    AdminController.bans[id] <- {
        name = acc.username,
        timeout = timeout,
        reason = reason,
        mac = mac,
        serial = serial
    };

    kick(pid, reason);
    updateBanSystem();
}

function unban(id) {
    AdminController.bans.rawdelete(id);
    updateBanSystem();
}

function updateBanSystem() {
    Query().deleteFrom("ban").execute();

    if(AdminController.bans.len() > 0)
    {
        local arr = [];
        foreach(_, item in AdminController.bans) {
            Query().insertInto("ban", ["accountId", "name", "serial", "mac", "reason", "timeout"], [
                _, "'"+item.name+"'", "'"+item.serial+"'", "'"+item.mac+"'", "'"+item.reason+"'", item.timeout
            ]).execute();
        }
    }
}

addEventHandler ("onInit", function () {
    foreach(data in Query().select().from("ban").all()) {
        AdminController.bans[data["accountId"].tointeger()] <- {
            name = data["name"],
            timeout = data["timeout"].tointeger(),
            reason = data["reason"],
            mac = data["mac"],
            serial = data["serial"]
        }
    }
});

addEventHandler ("onPlayerJoin", function (playerId) {
    local l = AdminController.bans.len();
    local mac = getPlayerMacAddr(playerId), serial = getPlayerSerial(playerId);

    foreach(_, ban in AdminController.bans) {
        if(ban.timeout == 0)
            continue;

        if(ban.timeout < time())
            AdminController.bans.rawdelete(_);
    }

    if(l != AdminController.bans.len())
        updateBanSystem();

    foreach(_ban in AdminController.bans) {
        if(_ban.mac == mac || _ban.serial == serial) {
            kick(playerId, _ban.reason);
            return;
        }
    }
});

addEventHandler ("onAccountLoad", function (playerId) {
    local accName = getAccount(playerId).username;
    foreach(_ban in AdminController.bans) {
        if(_ban.name == accName) {
            kick(playerId, _ban.reason);
            return;
        }
    }
});
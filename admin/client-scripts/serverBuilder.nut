enum ServerBuilderNamespaceCamera {
    Free,
    Follow,
    Close
}

enum ServerBuilderNamespaceContext {
    Fractions,
    Beasts,
    Players,
    Draws,
    Works,
    Crafts,
    Vobs,
    Assets,
    Items,
}

ServerBuilderNamespace <- {};

//* Objects

ServerBuilderNamespace.Object <- null;
ServerBuilderNamespace.ContextId <- -1;
ServerBuilderNamespace.CameraMode <- null;

//* GUI Objects

ServerBuilderNamespace.Window <- GUI.Window(anx(40), any(Resolution.y/2 - 450), anx(350), any(850), "HK_BOX.TGA");
ServerBuilderNamespace.Info <- GUI.Draw(0,8000,"Spacja - zmiana trybu kamery.");
ServerBuilderNamespace.Buttons <- [
    GUI.Button(anx(25), any(50), anx(300), any(60), "HK_BUTTON.TGA", "Frakcje", ServerBuilderNamespace.Window),
    GUI.Button(anx(25), any(130), anx(300), any(60), "HK_BUTTON.TGA", "Bestie", ServerBuilderNamespace.Window),
    GUI.Button(anx(25), any(210), anx(300), any(60), "HK_BUTTON.TGA", "Gracze", ServerBuilderNamespace.Window),
    GUI.Button(anx(25), any(290), anx(300), any(60), "HK_BUTTON.TGA", "Drawy", ServerBuilderNamespace.Window),
    GUI.Button(anx(25), any(370), anx(300), any(60), "HK_BUTTON.TGA", "Prace", ServerBuilderNamespace.Window),
    GUI.Button(anx(25), any(450), anx(300), any(60), "HK_BUTTON.TGA", "Crafty", ServerBuilderNamespace.Window),
    GUI.Button(anx(25), any(530), anx(300), any(60), "HK_BUTTON.TGA", "Voby", ServerBuilderNamespace.Window),
    GUI.Button(anx(25), any(610), anx(300), any(60), "HK_BUTTON.TGA", "Assety", ServerBuilderNamespace.Window),
    GUI.Button(anx(25), any(690), anx(300), any(60), "HK_BUTTON.TGA", "Itemy", ServerBuilderNamespace.Window),
]

ServerBuilderNamespace.List <- MyGUI.SelectList(anx(40), any(Resolution.y/2 - 300), anx(350), any(600))

ServerBuilderNamespace.Extra <- [];

ServerBuilderNamespace.Buttons[0].bind(EventType.Click, function(element) { ServerBuilderNamespace.changeContext(ServerBuilderNamespaceContext.Fractions); });
ServerBuilderNamespace.Buttons[1].bind(EventType.Click, function(element) { ServerBuilderNamespace.changeContext(ServerBuilderNamespaceContext.Beasts); });
ServerBuilderNamespace.Buttons[2].bind(EventType.Click, function(element) { ServerBuilderNamespace.changeContext(ServerBuilderNamespaceContext.Players); });
ServerBuilderNamespace.Buttons[3].bind(EventType.Click, function(element) { ServerBuilderNamespace.changeContext(ServerBuilderNamespaceContext.Draws); });
ServerBuilderNamespace.Buttons[4].bind(EventType.Click, function(element) { ServerBuilderNamespace.changeContext(ServerBuilderNamespaceContext.Works); });
ServerBuilderNamespace.Buttons[5].bind(EventType.Click, function(element) { ServerBuilderNamespace.changeContext(ServerBuilderNamespaceContext.Crafts); });
ServerBuilderNamespace.Buttons[6].bind(EventType.Click, function(element) { ServerBuilderNamespace.changeContext(ServerBuilderNamespaceContext.Vobs); });
ServerBuilderNamespace.Buttons[7].bind(EventType.Click, function(element) { ServerBuilderNamespace.changeContext(ServerBuilderNamespaceContext.Assets); });
ServerBuilderNamespace.Buttons[8].bind(EventType.Click, function(element) { ServerBuilderNamespace.changeContext(ServerBuilderNamespaceContext.Items); });

//* Methods

function ServerBuilderNamespace::show()
{
    if(adminPlayer.role == PlayerRole.User || adminPlayer.role == PlayerRole.Patreon || adminPlayer.role == PlayerRole.Narrator || adminPlayer.role == PlayerRole.GameMaster)
        return;

    if(adminPlayer.active == false)
        return;

    BaseGUI.show();
    ServerBuilderNamespace.changeMode(ServerBuilderNamespaceCamera.Close);

    ServerBuilderNamespace.Window.setVisible(true);
    ServerBuilderNamespace.Info.setVisible(true);
    ActiveGui = PlayerGUI.ServerBuilder;
}

function ServerBuilderNamespace::hide()
{
    BaseGUI.hide();
    ActiveGui = null;
    ServerBuilderNamespace.Window.setVisible(false);
    ServerBuilderNamespace.Info.setVisible(false);

    foreach(extraItem in ServerBuilderNamespace.Extra) {
        extraItem.setVisible(false);
        extraItem.destroy();
    }

    ServerBuilderNamespace.List.setVisible(false);
    ServerBuilderNamespace.Extra.clear();
}


//* Set Builder Context

function ServerBuilderNamespace::changeContext(value)
{
    foreach(extraItem in ServerBuilderNamespace.Extra) {
        extraItem.setVisible(false);
        extraItem.destroy();
    }

    ServerBuilderNamespace.Extra = [];
    ServerBuilderNamespace.ContextId = value;

    switch(value)
    {
        case ServerBuilderNamespaceContext.Fractions:
            ServerBuilderNamespace.hide();
            ServerBuilderFractions.show();
        break;
        case ServerBuilderNamespaceContext.Crafts:
            ServerBuilderNamespace.Craft.List();
        break;
        case ServerBuilderNamespaceContext.Works:
            ServerBuilderNamespace.Work.List();
        break;
        case ServerBuilderNamespaceContext.Draws:
            ServerBuilderNamespace.hide();
            DrawsBuilder.show();
        break;
        case ServerBuilderNamespaceContext.Vobs:
            ServerBuilderNamespace.hide();
            VobsBuilder.show();
        break;
        case ServerBuilderNamespaceContext.Players:
            ServerBuilderNamespace.hide();
            PlayersBuilder.show();
        break;
        case ServerBuilderNamespaceContext.Assets:
            ServerBuilderNamespace.hide();
            AssetsBuilder.show();
        break;
        case ServerBuilderNamespaceContext.Beasts:
            ServerBuilderNamespace.hide();
            BotsBuilder.show();
        break;
        case ServerBuilderNamespaceContext.Items:
            ServerBuilderNamespace.hide();
            ItemsBuilder.show();
        break;
    }
}


ServerBuilderNamespace.Work <- {
    List = function()
    {
        ServerBuilderNamespace.Window.setVisible(false);
        ServerBuilderNamespace.List.clear();

        ServerBuilderNamespace.List.addOption(0, "Drzewa");
        ServerBuilderNamespace.List.addOption(1, "Rudy");
        ServerBuilderNamespace.List.addOption(2, "�owiska ryb");

        ServerBuilderNamespace.List.onClick = function(id) {
            ServerBuilderNamespace.hide();
            if(id == 1) {
                MinesBuilder.show();
            }else if(id == 0) {
                ForestsBuilder.show();
            }else if(id == 2) {
                FishingsBuilder.show();
            }
        }

        ServerBuilderNamespace.List.setVisible(true);

        ServerBuilderNamespace.Extra = [
            GUI.Button(anx(40), any(Resolution.y/2 - 480), anx(350), any(60), "HK_BUTTON.TGA", "Powr�t"),
        ];

        foreach(extraItem in ServerBuilderNamespace.Extra)
            extraItem.setVisible(true);

        ServerBuilderNamespace.Extra[0].bind(EventType.Click, function(element) { ServerBuilderNamespace.changeContext(-1); ServerBuilderNamespace.Window.setVisible(true); });
    }
}

// Craft window

ServerBuilderNamespace.Craft <- {
    CraftId = -1,
    Crafts = [],

    List = function()
    {
        ServerBuilderNamespace.Window.setVisible(false);
        ServerBuilderNamespace.List.clear();

        Crafts.clear();
        CraftController.reset();
        AdminPacket().getAllCrafts();

        ServerBuilderNamespace.List.onClick = function(id) {
            ServerBuilderNamespace.Craft.Show(id);
        }

        ServerBuilderNamespace.List.setVisible(true);

        ServerBuilderNamespace.Extra = [
            GUI.Button(anx(40), any(Resolution.y/2 - 480), anx(350), any(60), "HK_BUTTON.TGA", "Powr�t"),
            GUI.Button(anx(40), any(Resolution.y/2 - 400), anx(350), any(60), "HK_BUTTON.TGA", "Dodaj"),
        ];

        foreach(extraItem in ServerBuilderNamespace.Extra)
            extraItem.setVisible(true);

        ServerBuilderNamespace.Extra[0].bind(EventType.Click, function(element) { ServerBuilderNamespace.changeContext(-1); ServerBuilderNamespace.Window.setVisible(true); });
        ServerBuilderNamespace.Extra[1].bind(EventType.Click, function(element) { ServerBuilderNamespace.Craft.Add(); });

    }

    Add = function()
    {
        foreach(extraItem in ServerBuilderNamespace.Extra) {
            extraItem.setVisible(false);
            extraItem.destroy();
        }

        ServerBuilderNamespace.Extra.clear();

        ServerBuilderNamespace.Window.setVisible(false);
        ServerBuilderNamespace.List.setVisible(false);
        ServerBuilderNamespace.List.clear();

        ServerBuilderNamespace.Extra = [
            GUI.Input(anx(40), any(Resolution.y/2 - 300), anx(300), any(60), "HK_BUTTON.TGA", "FONT_OLD_10_WHITE_HI.TGA", Input.Text, Align.Center, "Nazwa", 2)
            MyGUI.DropDown(anx(40), any(Resolution.y/2 - 200), anx(300), any(60), "Animacja", MyGUI.DropDown.FromTable(Config["CraftAnimations"]), "Wybierz animacj�.."),
            MyGUI.DropDown(anx(40), any(Resolution.y/2 - 100), anx(300), any(60), "Wywo�anie", MyGUI.DropDown.FromTable(Config["CraftCaller"]), "Wybierz wywo�anie.."),
            MyGUI.DropDown(anx(40), any(Resolution.y/2), anx(300), any(60), "Skill", MyGUI.DropDown.FromTable(Config["PlayerSkill"]), "Wybierz skill.."),
            MyGUI.DropDown(anx(40), any(Resolution.y/2 + 100), anx(300), any(60), "Warto�� skilla", MyGUI.DropDown.FromTable(Config["PlayerSkillValue"]), "Wybierz warto��.."),
            GUI.Input(anx(40), any(Resolution.y/2 + 200), anx(300), any(60), "HK_BUTTON.TGA", "FONT_OLD_10_WHITE_HI.TGA", Input.Text, Align.Center, "Stamina", 2)
            GUI.Button(anx(40), any(Resolution.y/2 + 300), anx(300), any(60), "HK_BUTTON.TGA", "Zapisz"),
            GUI.Button(anx(40), any(Resolution.y/2 - 460), anx(300), any(60), "HK_BUTTON.TGA", "Powr�t"),
            GUI.Draw(anx(40), any(Resolution.y/2 + 400), "")
        ];

        ServerBuilderNamespace.Extra[8].setColor(200,0,0);

        ServerBuilderNamespace.Extra[6].bind(EventType.Click, function(element) {
            local name = ServerBuilderNamespace.Extra[0].getText();
            local animation = ServerBuilderNamespace.Extra[1].getValue();
            local caller = ServerBuilderNamespace.Extra[2].getValue();
            local skillRequired = ServerBuilderNamespace.Extra[3].getValue();
            local skillValue = ServerBuilderNamespace.Extra[4].getValue();
            local staminaCost = ServerBuilderNamespace.Extra[5].getText();

            if(name.len() < 3 || staminaCost.len() == 0)
            {
                ServerBuilderNamespace.Extra[8].setText("B��dnie uzupe�nione dane.");
                return;
            }

            staminaCost = staminaCost.tofloat();

            CraftPacket().send(name, animation, caller, staminaCost, skillRequired, skillValue);
            ServerBuilderNamespace.Extra[8].setText("Utworzno craft "+name+".");
        });

        ServerBuilderNamespace.Extra[7].bind(EventType.Click, function(element) {  ServerBuilderNamespace.changeContext(ServerBuilderNamespaceContext.Crafts); });

        foreach(extraItem in ServerBuilderNamespace.Extra)
            extraItem.setVisible(true);
    }

    AddCraft = function(craftId)
    {
        Crafts.append(craftId);
        local obj = getCraft(craftId);
        ServerBuilderNamespace.List.addOption(craftId, "("+craftId+") "+obj.name);
    }

    AddItem = function()
    {
        foreach(extraItem in ServerBuilderNamespace.Extra) {
            extraItem.setVisible(false);
            extraItem.destroy();
        }

        ServerBuilderNamespace.Extra.clear();

        ServerBuilderNamespace.Extra = [
            GUI.Button(anx(40), any(Resolution.y/2 - 400), anx(350), any(60), "HK_BUTTON.TGA", "Powr�t"),
            GUI.Button(anx(40), any(Resolution.y/2 - 100), anx(350), any(60), "HK_BUTTON.TGA", "Dodaj wymagany przedmiot"),
            GUI.Input(anx(40), any(Resolution.y/2 - 300), anx(350), any(60), "HK_BUTTON.TGA", "FONT_OLD_10_WHITE_HI.TGA", Input.Text, Align.Center, "Kod Przedmiotu", 2),
            GUI.Input(anx(40), any(Resolution.y/2 - 200), anx(350), any(60), "HK_BUTTON.TGA", "FONT_OLD_10_WHITE_HI.TGA", Input.Numbers, Align.Center, "0", 2),
            GUI.Draw(anx(40), any(Resolution.y/2), ""),
        ];

        ServerBuilderNamespace.Extra[0].bind(EventType.Click, function(element) { ServerBuilderNamespace.changeContext(ServerBuilderNamespaceContext.Crafts); });
        ServerBuilderNamespace.Extra[1].bind(EventType.Click, function(element) {
            local instance = ServerBuilderNamespace.Extra[2].draw.getText();
            local amount = ServerBuilderNamespace.Extra[3].draw.getText();
            if(instance.len() == 0) {
                ServerBuilderNamespace.Extra[4].setText("�le wype�nione dane.");
                return;
            }

            amount = amount.tointeger();
            if(amount <= 0) {
                ServerBuilderNamespace.Extra[4].setText("�le wype�nione dane.");
                return;
            }

            local craftObj = getCraft(ServerBuilderNamespace.Craft.CraftId);
            if(instance in craftObj.items)
                craftObj.items[instance] = craftObj.items[instance] + amount;
            else
                craftObj.items[instance] <- amount;

            CraftPacket().addItem(craftObj.id, instance, amount);
            ServerBuilderNamespace.Craft.Show(ServerBuilderNamespace.Craft.CraftId);
        });

        foreach(extraItem in ServerBuilderNamespace.Extra)
            extraItem.setVisible(true);

        ServerBuilderNamespace.List.setVisible(false);
    }

    AddReward = function()
    {
        foreach(extraItem in ServerBuilderNamespace.Extra) {
            extraItem.setVisible(false);
            extraItem.destroy();
        }

        ServerBuilderNamespace.Extra.clear();

        ServerBuilderNamespace.Extra = [
            GUI.Button(anx(40), any(Resolution.y/2 - 400), anx(350), any(60), "HK_BUTTON.TGA", "Powr�t"),
            GUI.Button(anx(40), any(Resolution.y/2 - 100), anx(350), any(60), "HK_BUTTON.TGA", "Dodaj otrzymywany przedmiot"),
            GUI.Input(anx(40), any(Resolution.y/2 - 300), anx(350), any(60), "HK_BUTTON.TGA", "FONT_OLD_10_WHITE_HI.TGA", Input.Text, Align.Center, "Kod Przedmiotu", 2),
            GUI.Input(anx(40), any(Resolution.y/2 - 200), anx(350), any(60), "HK_BUTTON.TGA", "FONT_OLD_10_WHITE_HI.TGA", Input.Numbers, Align.Center, "0", 2),
            GUI.Draw(anx(40), any(Resolution.y/2), ""),
        ];

        ServerBuilderNamespace.Extra[0].bind(EventType.Click, function(element) { ServerBuilderNamespace.changeContext(ServerBuilderNamespaceContext.Crafts); });
        ServerBuilderNamespace.Extra[1].bind(EventType.Click, function(element) {
            local instance = ServerBuilderNamespace.Extra[2].draw.getText();
            local amount = ServerBuilderNamespace.Extra[3].draw.getText();
            if(instance.len() == 0) {
                ServerBuilderNamespace.Extra[4].setText("�le wype�nione dane.");
                return;
            }

            amount = amount.tointeger();
            if(amount <= 0) {
                ServerBuilderNamespace.Extra[4].setText("�le wype�nione dane.");
                return;
            }

            local craftObj = getCraft(ServerBuilderNamespace.Craft.CraftId);
            if(instance in craftObj.rewards)
                craftObj.rewards[instance] = craftObj.rewards[instance] + amount;
            else
                craftObj.rewards[instance] <- amount;

            CraftPacket().addReward(craftObj.id, instance, amount);
            ServerBuilderNamespace.Craft.Show(ServerBuilderNamespace.Craft.CraftId);
        });

        foreach(extraItem in ServerBuilderNamespace.Extra)
            extraItem.setVisible(true);

        ServerBuilderNamespace.List.setVisible(false);
    }

    Show = function(id)
    {
        foreach(extraItem in ServerBuilderNamespace.Extra) {
            extraItem.setVisible(false);
            extraItem.destroy();
        }

        ServerBuilderNamespace.Extra.clear();

        CraftId = id;
        ServerBuilderNamespace.Window.setVisible(false);
        ServerBuilderNamespace.List.setVisible(false);
        ServerBuilderNamespace.List.clear();

        local craftObj = getCraft(id);

        ServerBuilderNamespace.Extra = [
            GUI.Input(anx(40), any(Resolution.y/2 - 300), anx(300), any(60), "HK_BUTTON.TGA", "FONT_OLD_10_WHITE_HI.TGA", Input.Text, Align.Center, "Nazwa", 2)
            MyGUI.DropDown(anx(40), any(Resolution.y/2 - 200), anx(300), any(60), "Animacja", MyGUI.DropDown.FromTable(Config["CraftAnimations"]), "Wybierz animacj�..", craftObj.animation),
            MyGUI.DropDown(anx(40), any(Resolution.y/2 - 100), anx(300), any(60), "Wywo�anie", MyGUI.DropDown.FromTable(Config["CraftCaller"]), "Wybierz wywo�anie..", craftObj.caller),
            MyGUI.DropDown(anx(40), any(Resolution.y/2), anx(300), any(60), "Skill", MyGUI.DropDown.FromTable(Config["PlayerSkill"]), "Wybierz skill..", craftObj.skillRequired[0]),
            MyGUI.DropDown(anx(40), any(Resolution.y/2 + 100), anx(300), any(60), "Warto�� skilla", MyGUI.DropDown.FromTable(Config["PlayerSkillValue"]), "Wybierz warto��..", craftObj.skillValue[0]),
            GUI.Input(anx(40), any(Resolution.y/2 + 200), anx(300), any(60), "HK_BUTTON.TGA", "FONT_OLD_10_WHITE_HI.TGA", Input.Text, Align.Center, "Stamina", 2)
            GUI.Button(anx(40), any(Resolution.y/2 + 300), anx(300), any(60), "HK_BUTTON.TGA", "Aktualizuj"),
            MyGUI.SelectList(anx(Resolution.x/2 - 175), any(Resolution.y/2 - 300), anx(350), any(600)),
            MyGUI.SelectList(anx(Resolution.x - 400), any(Resolution.y/2 - 300), anx(350), any(600)),
            GUI.Button(anx(40), any(Resolution.y/2 - 460), anx(300), any(60), "HK_BUTTON.TGA", "Powr�t"),
            GUI.Button(anx(Resolution.x/2 - 175), any(Resolution.y - 200), anx(350), any(60), "HK_BUTTON.TGA", "Usu� craft"),
            GUI.Button(anx(Resolution.x/2 - 175), any(Resolution.y/2 - 400), anx(350), any(60), "HK_BUTTON.TGA", "Dodaj wymagany przedmiot"),
            GUI.Button(anx(Resolution.x - 400), any(Resolution.y/2 - 400), anx(350), any(60), "HK_BUTTON.TGA", "Dodaj nagrode"),
            GUI.Draw(anx(40), any(Resolution.y/2 + 400), ""),
            GUI.Draw(anx(40), any(Resolution.y/2 - 550), ""),
        ];

        ServerBuilderNamespace.Extra[13].setColor(200,0,0);
        ServerBuilderNamespace.Extra[14].setText("Stworzony przez "+craftObj.createdBy+" dnia "+craftObj.createdAt);

        ServerBuilderNamespace.Extra[7].onClick = function(id) {
            local craftId = ServerBuilderNamespace.Craft.CraftId;
            local craftItems = getCraft(craftId).items;

            CraftPacket().removeItem(craftId, id, craftItems[id]);

            if(id in craftItems)
                getCraft(craftId).items.rawdelete(id);

            ServerBuilderNamespace.Extra[13].setText("Usuni�to.");
            ServerBuilderNamespace.Craft.Show(ServerBuilderNamespace.Craft.CraftId);
        }

        ServerBuilderNamespace.Extra[8].onClick = function(id) {
            local craftId = ServerBuilderNamespace.Craft.CraftId;
            local craftItems = getCraft(craftId).rewards;

            CraftPacket().removeReward(craftId, id, craftItems[id]);

            if(id in craftItems)
                getCraft(craftId).rewards.rawdelete(id);

            ServerBuilderNamespace.Extra[13].setText("Usuni�to.");
            ServerBuilderNamespace.Craft.Show(ServerBuilderNamespace.Craft.CraftId);
        }

        foreach(instance, amount in craftObj.items)
            ServerBuilderNamespace.Extra[7].addOption(instance, instance + " " + amount);

        foreach(instance, amount in craftObj.rewards)
            ServerBuilderNamespace.Extra[8].addOption(instance, instance + " " + amount);

        ServerBuilderNamespace.Extra[0].setText(craftObj.name);
        ServerBuilderNamespace.Extra[5].setText(craftObj.staminaCost.tostring());
        ServerBuilderNamespace.Extra[6].bind(EventType.Click, function(element) {
            local name = ServerBuilderNamespace.Extra[0].getText();
            local animation = ServerBuilderNamespace.Extra[1].getValue();
            local caller = ServerBuilderNamespace.Extra[2].getValue();
            local skillRequired = ServerBuilderNamespace.Extra[3].getValue();
            local skillValue = ServerBuilderNamespace.Extra[4].getValue();
            local staminaCost = ServerBuilderNamespace.Extra[5].getText();

            if(name.len() < 3 || staminaCost.len() == 0)
            {
                ServerBuilderNamespace.Extra[13].setText("B��dnie uzupe�nione dane.");
                return;
            }

            staminaCost = staminaCost.tofloat();

            CraftPacket().update(ServerBuilderNamespace.Craft.CraftId, name, animation, caller, staminaCost, skillRequired, skillValue);
            ServerBuilderNamespace.Extra[13].setText("Zaaktualizowano.");
        });

        ServerBuilderNamespace.Extra[9].bind(EventType.Click, function(element) {  ServerBuilderNamespace.changeContext(ServerBuilderNamespaceContext.Crafts); });
        ServerBuilderNamespace.Extra[10].bind(EventType.Click, function(element) {
            CraftPacket().remove(ServerBuilderNamespace.Craft.CraftId);
            ServerBuilderNamespace.changeContext(ServerBuilderNamespaceContext.Crafts);
        });
        ServerBuilderNamespace.Extra[11].bind(EventType.Click, function(element) {ServerBuilderNamespace.Craft.AddItem();});
        ServerBuilderNamespace.Extra[12].bind(EventType.Click, function(element) {ServerBuilderNamespace.Craft.AddReward();});

        foreach(extraItem in ServerBuilderNamespace.Extra)
            extraItem.setVisible(true);
    }
}

addEventHandler("onCreateCraft", function(craftId) {
    if(ActiveGui != PlayerGUI.ServerBuilder)
        return;

    if(ServerBuilderNamespace.ContextId != ServerBuilderNamespaceContext.Crafts)
        return;

    ServerBuilderNamespace.Craft.AddCraft(craftId);
})

// Fraction window

ServerBuilderNamespace.Fraction <- {
    ShowedFraction = -1,
    Territories = [],
    Members = [],
    Permissions = [],

    List = function()
    {
        ServerBuilderNamespace.Window.setVisible(false);
        ServerBuilderNamespace.List.clear();

        foreach(fraction in getFractions())
            ServerBuilderNamespace.List.addOption(fraction.id, fraction.name);

        ServerBuilderNamespace.List.onClick = function(id) {
            ServerBuilderNamespace.Fraction.Show(id);
        }

        ServerBuilderNamespace.List.setVisible(true);

        ServerBuilderNamespace.Extra = [
            GUI.Button(anx(40), any(Resolution.y/2 - 480), anx(350), any(60), "HK_BUTTON.TGA", "Powr�t"),
            GUI.Button(anx(40), any(Resolution.y/2 - 400), anx(350), any(60), "HK_BUTTON.TGA", "Dodaj"),
        ];

        foreach(extraItem in ServerBuilderNamespace.Extra)
            extraItem.setVisible(true);

        ServerBuilderNamespace.Extra[0].bind(EventType.Click, function(element) { ServerBuilderNamespace.changeContext(-1); ServerBuilderNamespace.Window.setVisible(true); });
        ServerBuilderNamespace.Extra[1].bind(EventType.Click, function(element) { ServerBuilderNamespace.Fraction.Add(); });
    }

    Show = function(id)
    {
        ShowedFraction = id;
        ServerBuilderNamespace.Window.setVisible(false);
        ServerBuilderNamespace.List.setVisible(false);
        ServerBuilderNamespace.List.clear();

        foreach(extraItem in ServerBuilderNamespace.Extra)
            extraItem.destroy();

        ServerBuilderNamespace.Extra = [
            MyGUI.SelectList(anx(40), any(Resolution.y/2 - 300), anx(350), any(300)),
            MyGUI.SelectList(anx(40), any(Resolution.y/2 + 100), anx(350), any(300)),
            MyGUI.SelectList(anx(Resolution.x - 400), any(Resolution.y/2 - 300), anx(350), any(500)),
            GUI.Button(anx(40), any(Resolution.y/2 - 460), anx(350), any(60), "HK_BUTTON.TGA", "Powr�t"),
            GUI.Button(anx(40), any(Resolution.y/2 - 380), anx(350), any(60), "HK_BUTTON.TGA", "Dodaj u�ytkownika"),
            GUI.Button(anx(Resolution.x - 400), any(Resolution.y/2 - 380), anx(350), any(60), "HK_BUTTON.TGA", "Dodaj terytorium"),
            GUI.Button(anx(40), any(Resolution.y/2 + 20), anx(350), any(60), "HK_BUTTON.TGA", "Dodaj rang�"),
            GUI.Button(anx(Resolution.x/2 - 175), any(Resolution.y - 200), anx(350), any(60), "HK_BUTTON.TGA", "Usu� frakcj�"),
        ];

        ServerBuilderNamespace.Extra[0].onClick = function(id) {ServerBuilderNamespace.Fraction.EditMember(id);}
        ServerBuilderNamespace.Extra[1].onClick = function(id) {ServerBuilderNamespace.Fraction.EditPermission(id);}
        ServerBuilderNamespace.Extra[2].onClick = function(id) {ServerBuilderNamespace.Fraction.EditTerritory(id);}

        ServerBuilderNamespace.Extra[3].bind(EventType.Click, function(element) { ServerBuilderNamespace.changeContext(ServerBuilderNamespaceContext.Fractions); });
        ServerBuilderNamespace.Extra[4].bind(EventType.Click, function(element) { ServerBuilderNamespace.Fraction.NewMember(); });
        ServerBuilderNamespace.Extra[5].bind(EventType.Click, function(element) { ServerBuilderNamespace.Fraction.NewTerritory();});
        ServerBuilderNamespace.Extra[6].bind(EventType.Click, function(element) { ServerBuilderNamespace.Fraction.NewPermission(); });
        ServerBuilderNamespace.Extra[7].bind(EventType.Click, function(element) {
            AdminPacket().removeFraction(ServerBuilderNamespace.Fraction.ShowedFraction);
            ServerBuilderNamespace.changeContext(ServerBuilderNamespaceContext.Fractions);
        });

        foreach(extraItem in ServerBuilderNamespace.Extra)
            extraItem.setVisible(true);

        FractionPacket().sendMembers(id);
        FractionPacket().sendTerritories(id);
        FractionPacket().sendPermissions(id);
    }

    NewMember = function() {
        foreach(extraItem in ServerBuilderNamespace.Extra)
            extraItem.destroy();

        local content = [];

        for(local i = 0; i < getMaxSlots(); i ++)
            if(getPlayerName(i) != null)
                content.push({id = i, name = getPlayerName(i)});

        local contentSecondary = [];

        foreach(permission in Permissions)
            contentSecondary.push({id = permission.id, name = permission.name })

        ServerBuilderNamespace.Extra = [
            GUI.Button(anx(40), any(Resolution.y/2 - 400), anx(350), any(60), "HK_BUTTON.TGA", "Powr�t"),
            MyGUI.DropDown(anx(40), any(Resolution.y/2 - 300), anx(350), any(60), "Gracz", content, "Wybierz gracza..", null),
            MyGUI.DropDown(anx(40), any(Resolution.y/2 - 200), anx(350), any(60), "Uprawnienia", contentSecondary, "Wybierz uprawnienia...", null, true),
            GUI.Button(anx(40), any(Resolution.y/2 - 100), anx(350), any(60), "HK_BUTTON.TGA", "Dodaj"),
        ];

        ServerBuilderNamespace.Extra[0].bind(EventType.Click, function(element) { ServerBuilderNamespace.changeContext(ServerBuilderNamespaceContext.Fractions); });
        ServerBuilderNamespace.Extra[3].bind(EventType.Click, function(element) {
            local playerId = ServerBuilderNamespace.Extra[1].getValue();
            local permission = String.parseArray(ServerBuilderNamespace.Extra[2].getValue(), ",");

            if(playerId == null)
                return;

            AdminPacket().addMemberToFraction(ServerBuilderNamespace.Fraction.ShowedFraction,playerId, permission);
            ServerBuilderNamespace.changeContext(ServerBuilderNamespaceContext.Fractions);
        });

        foreach(extraItem in ServerBuilderNamespace.Extra)
            extraItem.setVisible(true);

        ServerBuilderNamespace.List.setVisible(false);
    }

    NewPermission = function() {
        foreach(extraItem in ServerBuilderNamespace.Extra)
            extraItem.destroy();

        ServerBuilderNamespace.Extra = [
            GUI.Button(anx(40), any(Resolution.y/2 - 200), anx(350), any(60), "HK_BUTTON.TGA", "Powr�t"),
            GUI.Input(anx(40), any(Resolution.y/2 - 100), anx(350), any(60), "HK_BUTTON.TGA", "FONT_OLD_10_WHITE_HI.TGA", Input.Text, Align.Center, "Nazwa", 2),
            GUI.Button(anx(40), any(Resolution.y/2), anx(350), any(60), "HK_BUTTON.TGA", "Dodaj"),
        ];

        ServerBuilderNamespace.Extra[0].bind(EventType.Click, function(element) { ServerBuilderNamespace.changeContext(ServerBuilderNamespaceContext.Fractions); });
        ServerBuilderNamespace.Extra[2].bind(EventType.Click, function(element) {
            local name = ServerBuilderNamespace.Extra[1].getText();
            if(name.len() == 0)
                return;

            AdminPacket().addPermissionToFraction(ServerBuilderNamespace.Fraction.ShowedFraction,name);
            ServerBuilderNamespace.changeContext(ServerBuilderNamespaceContext.Fractions);
        });

        foreach(extraItem in ServerBuilderNamespace.Extra)
            extraItem.setVisible(true);

        ServerBuilderNamespace.List.setVisible(false);
    }

    NewTerritory = function() {
        foreach(extraItem in ServerBuilderNamespace.Extra)
            extraItem.destroy();

        ServerBuilderNamespace.Extra = [
            GUI.Button(0, 0, anx(Resolution.x), any(Resolution.y), "HK_MAPA.TGA", ""),
            GUI.Button(anx(Resolution.x/2 - 370), any(Resolution.y - 200), anx(350), any(60), "HK_BUTTON.TGA", "Powr�t"),
            GUI.Button(anx(Resolution.x/2 + 20), any(Resolution.y - 200), anx(350), any(60), "HK_BUTTON.TGA", "Dodaj"),
        ];

        ServerBuilderNamespace.Extra[0].attribute = { lines = [], points = [] };

        ServerBuilderNamespace.Extra[0].bind(EventType.Click, function(element) {
            local coordinates = {
                x = -78500,
                y = 47500,
                width = 54000,
                height = -53000
            }

            local pos = getCursorPosition();

            local maxX = coordinates.width - coordinates.x
            local maxY = coordinates.height - coordinates.y

            pos.x = (pos.x * maxX.tofloat()) / 8192
            pos.y = (pos.y * maxY.tofloat()) / 8192

            pos.x += coordinates.x
            pos.y += coordinates.y

            local cursorPos = getCursorPosition();

            element.attribute.points.append({
                position = { x = pos.x, z = pos.y },
                cursorPosition = clone cursorPos,
                text = Texture(cursorPos.x-anx(12), cursorPos.y-any(8), anx(24), any(16), "HK_U.TGA"),
            })

            element.attribute.points[element.attribute.points.len()-1].text.visible = true;
        });
        ServerBuilderNamespace.Extra[1].bind(EventType.Click, function(element) { ServerBuilderNamespace.changeContext(ServerBuilderNamespaceContext.Fractions); });
        ServerBuilderNamespace.Extra[2].bind(EventType.Click, function(element) {
            if(ServerBuilderNamespace.Extra[0].attribute.points.len() < 3)
                return;

            local points = ServerBuilderNamespace.Extra[0].attribute.points;
            AdminPacket().addTerritoryToFraction(ServerBuilderNamespace.Fraction.ShowedFraction, heroId, points);
            ServerBuilderNamespace.changeContext(ServerBuilderNamespaceContext.Fractions);
        });

        foreach(extraItem in ServerBuilderNamespace.Extra)
            extraItem.setVisible(true);

        ServerBuilderNamespace.List.setVisible(false);
    }

    EditMember = function(memberId) {
        foreach(extraItem in ServerBuilderNamespace.Extra)
            extraItem.destroy();

        local contentSecondary = [];

        foreach(permission in Permissions)
            contentSecondary.push({id = permission.id, name = permission.name })

        local contentSelected = [];
        foreach(member in Members)
            if(member.id == memberId)
                foreach(permission in member.permissions)
                    contentSelected.append(permission.id);

        ServerBuilderNamespace.Extra = [
            GUI.Button(anx(40), any(Resolution.y/2 - 400), anx(350), any(60), "HK_BUTTON.TGA", "Powr�t"),
            MyGUI.DropDown(anx(40), any(Resolution.y/2 - 300), anx(350), any(60), "Uprawnienia", contentSecondary, "Wybierz uprawnienia..." contentSelected, true),
            GUI.Button(anx(40), any(Resolution.y/2 - 200), anx(350), any(60), "HK_BUTTON.TGA", "Aktualizuj"),
            GUI.Button(anx(40), any(Resolution.y/2 - 100), anx(350), any(60), "HK_BUTTON.TGA", "Usu�"),
        ];
        ServerBuilderNamespace.Extra[3].attribute = memberId;

        ServerBuilderNamespace.Extra[0].bind(EventType.Click, function(element) { ServerBuilderNamespace.changeContext(ServerBuilderNamespaceContext.Fractions); });
        ServerBuilderNamespace.Extra[2].bind(EventType.Click, function(element) {
            local playerId = ServerBuilderNamespace.Extra[3].attribute;
            local permissionLabel = "";

            foreach(permission in ServerBuilderNamespace.Extra[1].getValue())
                permissionLabel += permission+",";

            if(permissionLabel.len() > 0)
                permissionLabel = permissionLabel.slice(0, -1);

            AdminPacket().updateMemberFraction(ServerBuilderNamespace.Fraction.ShowedFraction, playerId, permissionLabel);
            ServerBuilderNamespace.changeContext(ServerBuilderNamespaceContext.Fractions);
        });
        ServerBuilderNamespace.Extra[3].bind(EventType.Click, function(element) { AdminPacket().removeMemberFromFraction(ServerBuilderNamespace.Fraction.ShowedFraction, element.attribute); ServerBuilderNamespace.changeContext(ServerBuilderNamespaceContext.Fractions); });

        foreach(extraItem in ServerBuilderNamespace.Extra)
            extraItem.setVisible(true);

        ServerBuilderNamespace.List.setVisible(false);
    }

    EditTerritory = function(territoryId) {
        foreach(extraItem in ServerBuilderNamespace.Extra)
            extraItem.destroy();

        ServerBuilderNamespace.Extra = [
            GUI.Button(0, 0, anx(Resolution.x), any(Resolution.y), "HK_MAPA.TGA", ""),
            GUI.Button(anx(Resolution.x/2 - 370), any(Resolution.y - 200), anx(350), any(60), "HK_BUTTON.TGA", "Powr�t"),
            GUI.Button(anx(Resolution.x/2 + 20), any(Resolution.y - 200), anx(350), any(60), "HK_BUTTON.TGA", "Usu�"),
        ];

        ServerBuilderNamespace.Extra[2].attribute = { id = territoryId, texts = [] };

        ServerBuilderNamespace.Extra[1].bind(EventType.Click, function(element) { ServerBuilderNamespace.changeContext(ServerBuilderNamespaceContext.Fractions); });
        ServerBuilderNamespace.Extra[2].bind(EventType.Click, function(element) {
            AdminPacket().removeTerritoryFromFraction(ServerBuilderNamespace.Fraction.ShowedFraction, element.attribute.id);
            ServerBuilderNamespace.changeContext(ServerBuilderNamespaceContext.Fractions);
        });

        foreach(territory in ServerBuilderNamespace.Fraction.Territories)
        {
            if(territory.id == territoryId)
            {
                foreach(coord in territory.coords)
                {
                    local coordinates = {
                        x = -78500,
                        y = 47500,
                        width = 54000,
                        height = -53000
                    }

                    coord.x -= coordinates.x
                    coord.z -= coordinates.y

                    local maxX = coordinates.width - coordinates.x
                    local maxY = coordinates.height - coordinates.y

                    coord.x = (coord.x / maxX.tofloat()) * 8192
                    coord.z = (coord.z / maxY.tofloat()) * 8192

                    ServerBuilderNamespace.Extra[2].attribute.texts.append(Texture(coord.x-anx(12), coord.z-any(8), anx(24), any(16), "HK_U.TGA"))
                }
            }
        }

        foreach(extraItem in ServerBuilderNamespace.Extra)
            extraItem.setVisible(true);

        foreach(text in ServerBuilderNamespace.Extra[2].attribute.texts)
        {
            text.visible = true;
            text.top();
        }

        ServerBuilderNamespace.List.setVisible(false);
    }

    EditPermission = function(permissionId) {
        foreach(extraItem in ServerBuilderNamespace.Extra)
            extraItem.destroy();

        local contentSecondary = [];

        foreach(permissionTypeId, name in Config["FractionPermissionType"])
            contentSecondary.push({id = permissionTypeId, name = name })

        ServerBuilderNamespace.Extra = [
            GUI.Button(anx(40), any(Resolution.y/2 - 400), anx(350), any(60), "HK_BUTTON.TGA", "Powr�t"),
            MyGUI.DropDown(anx(40), any(Resolution.y/2 - 300), anx(350), any(60), "Uprawnienia", contentSecondary, "Typ uprawnienia", Permissions[permissionId].type, false),
            GUI.Input(anx(40), any(Resolution.y/2 - 200), anx(350), any(60), "HK_BUTTON.TGA", "FONT_OLD_10_WHITE_HI.TGA", Input.Text, Align.Center, "Nazwa", 2)
            GUI.Button(anx(40), any(Resolution.y/2 - 100), anx(350), any(60), "HK_BUTTON.TGA", "Aktualizuj"),
            GUI.Button(anx(40), any(Resolution.y/2), anx(350), any(60), "HK_BUTTON.TGA", "Usu�"),
            GUI.Draw(anx(40), any(Resolution.y/2 + 100), "W tym miejscu wpisujemy np. crafty(w formie ID) po przecinku np. 4,6\nLub nie wpisujemy nic w przypadku np. zapraszania."),
            GUI.Input(anx(40), any(Resolution.y/2 + 200), anx(350), any(60), "HK_BUTTON.TGA", "FONT_OLD_10_WHITE_HI.TGA", Input.Text, Align.Center, "", 2)
        ];
        ServerBuilderNamespace.Extra[4].attribute = permissionId;
        ServerBuilderNamespace.Extra[2].setText(Permissions[permissionId].name);
        ServerBuilderNamespace.Extra[6].setText(Permissions[permissionId].labels);

        ServerBuilderNamespace.Extra[0].bind(EventType.Click, function(element) { ServerBuilderNamespace.changeContext(ServerBuilderNamespaceContext.Fractions); });
        ServerBuilderNamespace.Extra[3].bind(EventType.Click, function(element) {
            local permissionId = ServerBuilderNamespace.Extra[4].attribute;
            local permissionTypeId = ServerBuilderNamespace.Extra[1].getValue();

            local name = ServerBuilderNamespace.Extra[2].getText();
            if(name.len() == 0)
                return;

            AdminPacket().updateMemberPermission(ServerBuilderNamespace.Fraction.ShowedFraction, ServerBuilderNamespace.Extra[4].attribute, name);
            ServerBuilderNamespace.changeContext(ServerBuilderNamespaceContext.Fractions);
        });

        ServerBuilderNamespace.Extra[4].bind(EventType.Click, function(element) { AdminPacket().removePermissionFromFraction(ServerBuilderNamespace.Fraction.ShowedFraction, element.attribute); ServerBuilderNamespace.changeContext(ServerBuilderNamespaceContext.Fractions); });

        foreach(extraItem in ServerBuilderNamespace.Extra)
            extraItem.setVisible(true);

        ServerBuilderNamespace.List.setVisible(false);
    }

    AddTerritories = function (territories) {
        Territories = territories;

        ServerBuilderNamespace.Extra[2].clear();

        foreach(territory in territories)
            ServerBuilderNamespace.Extra[2].addOption(territory.id, "Terytorium "+territory.id);
    }

    AddMembers = function (members) {
        Members = members;

        ServerBuilderNamespace.Extra[0].clear();

        foreach(member in Members)
            ServerBuilderNamespace.Extra[0].addOption(member.id, member.name);
    }

    AddPermissions = function (permissions) {
        Permissions = permissions;

        ServerBuilderNamespace.Extra[1].clear();

        foreach(permission in Permissions)
            ServerBuilderNamespace.Extra[1].addOption(permission.id, permission.name);
    }

    Add = function()
    {
        foreach(extraItem in ServerBuilderNamespace.Extra)
            extraItem.destroy();

        ServerBuilderNamespace.Extra = [
            GUI.Button(anx(40), any(Resolution.y/2 - 400), anx(350), any(60), "HK_BUTTON.TGA", "Powr�t"),
            GUI.Input(anx(40), any(Resolution.y/2 - 300), anx(350), any(60), "HK_BUTTON.TGA", "FONT_OLD_10_WHITE_HI.TGA", Input.Text, Align.Center, "Nazwa frakcji", 2),
            GUI.Input(anx(40), any(Resolution.y/2 - 200), anx(100), any(60), "HK_BUTTON.TGA", "FONT_OLD_10_WHITE_HI.TGA", Input.Numbers, Align.Center, "255", 2),
            GUI.Input(anx(160), any(Resolution.y/2 - 200), anx(100), any(60), "HK_BUTTON.TGA", "FONT_OLD_10_WHITE_HI.TGA", Input.Numbers, Align.Center, "255", 2),
            GUI.Input(anx(280), any(Resolution.y/2 - 200), anx(100), any(60), "HK_BUTTON.TGA", "FONT_OLD_10_WHITE_HI.TGA", Input.Numbers, Align.Center, "255", 2),
            GUI.Button(anx(40), any(Resolution.y/2 - 100), anx(350), any(60), "HK_BUTTON.TGA", "Wy�lij"),
        ];

        ServerBuilderNamespace.Extra[0].bind(EventType.Click, function(element) { ServerBuilderNamespace.changeContext(ServerBuilderNamespaceContext.Fractions); });
        ServerBuilderNamespace.Extra[5].bind(EventType.Click, function(element) { ServerBuilderNamespace.Fraction.New(); });

        foreach(extraItem in ServerBuilderNamespace.Extra)
            extraItem.setVisible(true);

        ServerBuilderNamespace.List.setVisible(false);
    }

    New = function()
    {
        local name = ServerBuilderNamespace.Extra[1].getText();
        local r = ServerBuilderNamespace.Extra[2].draw.getText();
        local g = ServerBuilderNamespace.Extra[3].draw.getText();
        local b = ServerBuilderNamespace.Extra[4].draw.getText();

        if(name.len() < 3)
            return;

        if(r.len() == 0 || g.len() == 0 || b.len() == 0 )
            return;

        AdminPacket().addFraction(name,r.tointeger(),g.tointeger(),b.tointeger());
        ServerBuilderNamespace.changeContext(ServerBuilderNamespaceContext.Fractions);
    }
};

addEventHandler("onFractionTerritories", function(fractionId, object) {
    if(ActiveGui != PlayerGUI.ServerBuilder)
        return;

    if(ServerBuilderNamespace.ContextId != ServerBuilderNamespaceContext.Fractions)
        return;

    ServerBuilderNamespace.Fraction.AddTerritories(object);
})

addEventHandler("onFractionMembers", function(fractionId, object) {
    if(ActiveGui != PlayerGUI.ServerBuilder)
        return;

    if(ServerBuilderNamespace.ContextId != ServerBuilderNamespaceContext.Fractions)
        return;

    ServerBuilderNamespace.Fraction.AddMembers(object);
})

addEventHandler("onFractionPermissions", function(fractionId, object) {
    if(ActiveGui != PlayerGUI.ServerBuilder)
        return;

    if(ServerBuilderNamespace.ContextId != ServerBuilderNamespaceContext.Fractions)
        return;

    ServerBuilderNamespace.Fraction.AddPermissions(object);
})

addEventHandler("onFractionRemove", function (fractionId) {
    if(ActiveGui != PlayerGUI.ServerBuilder)
        return;

    if(ServerBuilderNamespace.ContextId != ServerBuilderNamespaceContext.Fractions)
        return;

    ServerBuilderNamespace.changeContext(ServerBuilderNamespaceContext.Fractions);
})

addEventHandler("onFractionCreate", function (fractionId) {
    if(ActiveGui != PlayerGUI.ServerBuilder)
        return;

    if(ServerBuilderNamespace.ContextId != ServerBuilderNamespaceContext.Fractions)
        return;

    ServerBuilderNamespace.changeContext(ServerBuilderNamespaceContext.Fractions);
})

//* Camera manager

local cameraRotation = _Camera.getRotation()

local freeCamMovementSpeed = 300.0 // meters per sec
local freeCamRotationSpeed = 100.0 // meters per sec

local deg2Rad = 0.0174533

local before = getTickCount()
local oldCursorPosition = getCursorPosition()


_Camera.getclass().getRotation <- function()
{
	return cameraRotation
}

local _setRotation = _Camera.setRotation
_Camera.getclass().setRotation <- function(x, y, z)
{
	cameraRotation.x = x
    cameraRotation.y = y
    cameraRotation.z = z

	_setRotation(x, y, z)
}

ServerBuilderNamespace.changeMode <- function(value) {
    ServerBuilderNamespace.CameraMode = value;

    switch(value)
    {
        case ServerBuilderNamespaceCamera.Free:
            _Camera.modeChangeEnabled = false
            _Camera.enableMovement(false)
        break;
        case ServerBuilderNamespaceCamera.Close:
            Camera.setFreeze(true);
        break;
    }
}

ServerBuilderNamespace.onKeyCamera <- function(key) {
    if(ActiveGui != PlayerGUI.ServerBuilder)
        return;

    if(chatInputIsOpen())
        return;

    if(ServerBuilderNamespace.ContextId != ServerBuilderNamespaceContext.Vobs)
        return;

    switch(key)
    {
        case KEY_SPACE:
            switch(ServerBuilderNamespace.CameraMode)
            {
                case ServerBuilderNamespaceCamera.Free:
                    ServerBuilderNamespace.CameraMode = ServerBuilderNamespaceCamera.Close;
                    local pos = getPlayerPosition(heroId);
                    Camera.setPosition(pos.x,pos.y + 200, pos.z);
                break;
                case ServerBuilderNamespaceCamera.Close:
                    ServerBuilderNamespace.CameraMode = ServerBuilderNamespaceCamera.Free;
                break;
            }
        break;
    }
}

ServerBuilderNamespace.onRenderCamera <- function() {
    if(ActiveGui != PlayerGUI.ServerBuilder)
        return;

    switch(ServerBuilderNamespace.CameraMode)
    {
        case ServerBuilderNamespaceCamera.Free:
            local now = getTickCount()
            local cursorPosition = getCursorPosition()

            local deltaTime = (now - before) * 0.001

            local position = _Camera.getPosition()
            local rotation = _Camera.getRotation()

            local movementAcceleration = freeCamMovementSpeed * deltaTime
            local rotationAcceleration = freeCamRotationSpeed * deltaTime

            local vecForward = {
                x = cos(deg2Rad * rotation.x) * sin(deg2Rad * rotation.y),
                y = sin(deg2Rad * rotation.x),
                z = cos(deg2Rad * rotation.x) * cos(deg2Rad * rotation.y)
            }

            local vecRight = {
                x = vecForward.z,
                y = 0,
                z = -vecForward.x
            }

            local isForwardKeyPressed = isKeyPressed(KEY_UP) || isKeyPressed(KEY_W)
            local isBackwardKeyPressed = isKeyPressed(KEY_DOWN) || isKeyPressed(KEY_S)

            local isControlPressed = isKeyPressed(KEY_LCONTROL) || isKeyPressed(KEY_RCONTROL)

            if (isKeyPressed(KEY_LSHIFT) || isKeyPressed(KEY_RSHIFT))
            {
                movementAcceleration *= 3
                rotationAcceleration *= 1.5
            }

            if (isForwardKeyPressed && !isBackwardKeyPressed)
            {
                position.x += vecForward.x * movementAcceleration
                position.y -= vecForward.y * movementAcceleration
                position.z += vecForward.z * movementAcceleration
            }
            else if (isBackwardKeyPressed && !isForwardKeyPressed)
            {
                position.x -= vecForward.x * movementAcceleration
                position.y += vecForward.y * movementAcceleration
                position.z -= vecForward.z * movementAcceleration
            }

            if (isKeyPressed(KEY_LEFT) && !isKeyPressed(KEY_RIGHT))
                if (!isControlPressed)
                    rotation.y -= rotationAcceleration
                else
                    rotation.z -= rotationAcceleration
            else if (isKeyPressed(KEY_RIGHT) && !isKeyPressed(KEY_LEFT))
                if (!isControlPressed)
                    rotation.y += rotationAcceleration
                else
                    rotation.z += rotationAcceleration

            if (isKeyPressed(KEY_SPACE) && !isKeyPressed(KEY_X))
                position.y += movementAcceleration
            else if (isKeyPressed(KEY_X) && !isKeyPressed(KEY_SPACE))
                position.y -= movementAcceleration

            if (isKeyPressed(KEY_NEXT) && !isKeyPressed(KEY_PRIOR))
                rotation.x += rotationAcceleration
            else if (isKeyPressed(KEY_PRIOR) && !isKeyPressed(KEY_NEXT))
                rotation.x -= rotationAcceleration

            if (isKeyPressed(KEY_A) && !isKeyPressed(KEY_D))
            {
                position.x -= vecRight.x * movementAcceleration
                position.z -= vecRight.z * movementAcceleration
            }
            else if (isKeyPressed(KEY_D) && !isKeyPressed(KEY_A))
            {
                position.x += vecRight.x * movementAcceleration
                position.z += vecRight.z * movementAcceleration
            }

            if (oldCursorPosition.x != cursorPosition.x || oldCursorPosition.y != cursorPosition.y)
            {
                local cursorDeltaX = (cursorPosition.x - oldCursorPosition.x) / 8192.0
                local cursorDeltaY = (cursorPosition.y - oldCursorPosition.y) / 8192.0

                rotation.x += cursorDeltaY * freeCamRotationSpeed
                rotation.y += cursorDeltaX * freeCamRotationSpeed

                if (cursorPosition.x == 0)
                    cursorPosition.x = 8192
                else if (cursorPosition.x  >= 8192)
                    cursorPosition.x = 0

                if (cursorPosition.y == 0)
                    cursorPosition.y = 8192
                else if (cursorPosition.y >= 8192)
                    cursorPosition.y = 0

                setCursorPosition(cursorPosition.x, cursorPosition.y)
            }

            _Camera.setPosition(position.x, position.y, position.z)
            _Camera.setRotation(rotation.x, rotation.y, rotation.z)


            before = now
            oldCursorPosition = cursorPosition
        break;
    }
}

addEventHandler("onRender", function () { ServerBuilderNamespace.onRenderCamera(); });
addEventHandler("onKey", function (key) { ServerBuilderNamespace.onKeyCamera(key); });

Bind.addKey(KEY_F9, function() { ServerBuilderNamespace.show(); })
Bind.addKey(KEY_F9, function() { ServerBuilderNamespace.hide(); }, PlayerGUI.ServerBuilder)
Bind.addKey(KEY_ESCAPE, function() { ServerBuilderNamespace.hide(); }, PlayerGUI.ServerBuilder)


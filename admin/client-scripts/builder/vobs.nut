
VobsBuilder <- {
    bucket = null,

    startPosition = null,
    bucketVob = null,
    activeKeys = [],

    freeCameraMode = false,
    freeCameraBucket = [],

    prepareWindow = function() {
        local result = {};

        result.window <- GUI.Window(anx(Resolution.x - 400), any(Resolution.y/2 - 400), anx(350), any(780), "BLACK.TGA");
        result.list <- GUI.Window(anx(50), any(Resolution.y/2 - 400), anx(600), any(800), "HK_BOX.TGA");

        result.scrollbarList <- GUI.ScrollBar(anx(650), any(50), anx(30), any(700), "HK_SCROLL.TGA", "HK_BUTTON.TGA", "HK_U.TGA", "HK_O.TGA", Orientation.Vertical, result.list);
        result.scrollbarList.setMaximum(VobController.vobs.len() / 3);
        result.listContent <- [];

        result.bindForScrollbar <- Bind.onChange(result.scrollbarList, function(value) {
            VobsBuilder.refreshList();
        }, PlayerGUI.ServerBuilderVobs);

        result.addButton <- GUI.Button(anx(Resolution.x/2 - 200), any(Resolution.y - 100), anx(180), any(60), "HK_BUTTON.TGA", "Dodaj nowy");
        result.removeButton <- GUI.Button(anx(Resolution.x/2 + 20), any(Resolution.y - 100), anx(180), any(60), "HK_BUTTON.TGA", "Usu�");

        local animationsContent = [];
        animationsContent.append({ id = 0, name = "Brak" })
        foreach(effectId, effect in System_Effects)
            animationsContent.append({ id = effectId.tointeger(), name = effect })

        local keysComplexityContent = [
            { id = 1, name = "1" },
            { id = 2, name = "2" },
            { id = 3, name = "3" },
            { id = 4, name = "4" },
            { id = 5, name = "5" },
            { id = 6, name = "6" },
        ]

        result.vobTypeSelector <- MyGUI.DropDown(anx(Resolution.x - 400) + anx(15), any(Resolution.y/2 - 400) + any(20), anx(330), any(60), "Typ voba", MyGUI.DropDown.FromTable(Config["VobType"]), "Wybierz typ..", VobType.Static),
        result.vobHealthSelector <- MyGUI.DropDown(anx(Resolution.x - 400) + anx(15), any(Resolution.y/2 - 320) + any(20), anx(330), any(60), "Odporno�� voba", MyGUI.DropDown.FromTable(Config["VobHealth"]), "Wybierz odporno��..", VobHealth.Static),
        result.vobMovementSelector <- MyGUI.DropDown(anx(Resolution.x - 400) + anx(15), any(Resolution.y/2 - 240) + any(20), anx(330), any(60), "Przemieszczanie voba", MyGUI.DropDown.FromTable(Config["VobMovable"]), "Wybierz przemieszczanie..", VobMovable.Static),
        result.inputName <- GUI.Input(anx(15), any(300), anx(330), any(60), "HK_BUTTON.TGA", "FONT_OLD_10_WHITE_HI.TGA", Input.Text, Align.Center, "Nazwa", 2, result.window)
        result.inputWeight <- GUI.Input(anx(15), any(380), anx(330), any(60), "HK_BUTTON.TGA", "FONT_OLD_10_WHITE_HI.TGA", Input.Numbers, Align.Center, "Waga", 2, result.window)
        result.inputAnimationTime <- GUI.Input(anx(205), any(610), anx(100), any(60), "HK_BUTTON.TGA", "FONT_OLD_10_WHITE_HI.TGA", Input.Numbers, Align.Center, "0", 2, result.window)
        result.inputHp <- GUI.Input(anx(15), any(460), anx(330), any(60), "HK_BUTTON.TGA", "FONT_OLD_10_WHITE_HI.TGA", Input.Numbers, Align.Center, "Hp", 2, result.window)
        GUI.Draw(anx(15), any(250), "Kolizja wy��czona?", result.window);
        result.isDynamic <- GUI.CheckBox(anx(280), any(240), anx(35), any(40), "HK_CHECKBOX.TGA", "HK_CHECKBOX_CHECKED.TGA", result.window)
        result.keySelector <- GUI.Input(anx(15), any(530), anx(165), any(60), "HK_BUTTON.TGA", "FONT_OLD_10_WHITE_HI.TGA", Input.Text, Align.Center, "Guid", 2, result.window)
        result.keyConfiguration <- MyGUI.DropDown(anx(Resolution.x - 400) + anx(180), any(Resolution.y/2 - 400) + any(530), anx(165), any(60), "Skomplikowanie klucza", keysComplexityContent, "Wybierz..", 3),
        result.animationsSelector <- MyGUI.DropDown(anx(Resolution.x - 400) + anx(15), any(Resolution.y/2 - 400) + any(610), anx(200), any(60), "Animacja", animationsContent, "Wybierz animacj�..", 0),
        result.saveButton <- GUI.Button(anx(15), any(690), anx(330), any(60), "HK_BUTTON.TGA", "Zapisz", result.window);

        result.addButton.bind(EventType.Click, function(element) {
            VobsBuilder.toggleAddWindow();
        });

        result.vobTypeSelector.onChange = function(vobId) {
            if(type(vobId) == "array" && vobId.len() > 0)
                VobsBuilder.onChangeType(vobId[0]);
        }

        local contentSecondary = [];
        contentSecondary.append({id = 0, name = "Od siebie."});

        foreach(vob in VobController.vobs)
            if(vob.type == VobType.Switch)
                contentSecondary.append({ id = vob.id, name = (vob.name ? vob.id+"-"+vob.name : vob.id+"-"+vob.visual) })

        result.windowDoor <- GUI.Window(anx(Resolution.x/2 - 300), any(Resolution.y/2 - 500), anx(600), any(200), "BLACK.TGA");
        result.animationDoor <- MyGUI.DropDown(anx(Resolution.x/2 - 280), any(Resolution.y/2 - 490), anx(260), any(60), "Animacja ruchu", MyGUI.DropDown.FromTable(Config["VobTriggerAnimation"]), "Wybierz animacj�..", VobTriggerAnimation.None),
        result.triggerDoor <- MyGUI.DropDown(anx(Resolution.x/2 + 20), any(Resolution.y/2 - 490), anx(260), any(60), "Wywo�anie", contentSecondary, "Wybierz wywo�anie..", 0),
        result.triggerX <- GUI.Input(anx(0), any(120), anx(100), any(60), "HK_BUTTON.TGA", "FONT_OLD_10_WHITE_HI.TGA", Input.Numbers, Align.Center, "X", 2, result.windowDoor)
        result.triggerY <- GUI.Input(anx(100), any(120), anx(100), any(60), "HK_BUTTON.TGA", "FONT_OLD_10_WHITE_HI.TGA", Input.Numbers, Align.Center, "Y", 2, result.windowDoor)
        result.triggerZ <- GUI.Input(anx(200), any(120), anx(100), any(60), "HK_BUTTON.TGA", "FONT_OLD_10_WHITE_HI.TGA", Input.Numbers, Align.Center, "Z", 2, result.windowDoor)
        result.drawTrigger <- GUI.Draw(anx(320), any(100), "Rotacja: ", result.windowDoor)
        result.triggerRotationX <- GUI.Input(anx(300), any(120), anx(100), any(60), "HK_BUTTON.TGA", "FONT_OLD_10_WHITE_HI.TGA", Input.Numbers, Align.Center, "X", 2, result.windowDoor)
        result.triggerRotationY <- GUI.Input(anx(400), any(120), anx(100), any(60), "HK_BUTTON.TGA", "FONT_OLD_10_WHITE_HI.TGA", Input.Numbers, Align.Center, "Y", 2, result.windowDoor)
        result.triggerRotationZ <- GUI.Input(anx(500), any(120), anx(100), any(60), "HK_BUTTON.TGA", "FONT_OLD_10_WHITE_HI.TGA", Input.Numbers, Align.Center, "Z", 2, result.windowDoor)
        result.triggerTime <- GUI.Input(anx(200), any(80), anx(100), any(37), "HK_BUTTON.TGA", "FONT_OLD_10_WHITE_HI.TGA", Input.Numbers, Align.Center, "czas w ms.", 2, result.windowDoor)
        result.triggerPos <- GUI.Button(anx(100), any(80), anx(50), any(37), "HK_BUTTON.TGA", "@", result.windowDoor);

        result.triggerPos.bind(EventType.Click, function(element) {
            local pos = VobsBuilder.bucketVob.getPosition();
            local rot = VobsBuilder.bucketVob.getRotation();

            VobsBuilder.bucket.triggerX.setText(pos.x.tostring());
            VobsBuilder.bucket.triggerY.setText(pos.y.tostring());
            VobsBuilder.bucket.triggerZ.setText(pos.z.tostring());

            VobsBuilder.bucket.triggerRotationX.setText(rot.x.tostring());
            VobsBuilder.bucket.triggerRotationY.setText(rot.y.tostring());
            VobsBuilder.bucket.triggerRotationZ.setText(rot.z.tostring());
        });

        result.windowWithSelector <- GUI.Window(0, 0, 8200, 8200, "BLACK.TGA");
        result.sliderWithSelector <- GUI.ScrollBar(anx(Resolution.x - 100), any(50), anx(30), any(Resolution.y - 100), "HK_SCROLL.TGA", "HK_BUTTON.TGA", "HK_U.TGA", "HK_O.TGA", Orientation.Vertical, result.windowWithSelector);

        result.bucketWithSelectors <- [];

        for(local y = 0; y < 8; y ++) {
            for(local x = 0; x < 8; x ++) {
                result.bucketWithSelectors.append({
                    button = GUI.Button(50 + 900 * x, 50 + 900 * y, 850, 850, "WHITE.TGA", "", result.windowWithSelector),
                    render = ItemRender(50 + 900 * x, 50 + 900 * y, 850, 850, "")
                })
            }
        }

        result.saveButton.bind(EventType.Click, function(element) {
            local typeVob = VobsBuilder.bucket.vobTypeSelector.getValue();
            local healthVob = VobsBuilder.bucket.vobHealthSelector.getValue();
            local movementVob = VobsBuilder.bucket.vobMovementSelector.getValue();

            local targetPosition = {x = 0, y = 0, z = 0};
            local targetRotation = {x = 0, y = 0, z = 0};

            local keyValueVob = "";
            local animationValueVob = VobsBuilder.bucket.animationsSelector.getValue();

            local healthValueVob = 0;
            local keyConfigurationVob = VobsBuilder.bucket.keyConfiguration.getValue();
            local trigger = 0;
            local triggerType = 0;
            local triggerAnimation = 0;
            local triggerTime = 0;

            switch(typeVob) {
                case VobType.Door:
                    trigger = VobsBuilder.bucket.triggerDoor.getValue();
                    triggerType = trigger != 0 ? VobTriggerType.Vob : VobTriggerType.Self;
                    triggerAnimation = VobsBuilder.bucket.animationDoor.getValue();
                    targetPosition.x = VobsBuilder.bucket.triggerX.getText().tofloat();
                    targetPosition.y = VobsBuilder.bucket.triggerY.getText().tofloat();
                    targetPosition.z = VobsBuilder.bucket.triggerZ.getText().tofloat();
                    targetRotation.x = VobsBuilder.bucket.triggerRotationX.getText().tofloat();
                    targetRotation.y = VobsBuilder.bucket.triggerRotationY.getText().tofloat();
                    targetRotation.z = VobsBuilder.bucket.triggerRotationZ.getText().tofloat();
                    try {
                        triggerTime = VobsBuilder.bucket.triggerTime.getText().tointeger();
                    }catch(error) {
                        triggerTime = 1000;
                    }
                break;
            }

            try {
                healthValueVob = VobsBuilder.bucket.inputHp.draw.getText().tointeger();
            }catch(error) {
                healthValueVob = 0;
            }

            local weightValueVob = 200.0;

            try {
                weightValueVob = VobsBuilder.bucket.inputWeight.draw.getText().tofloat();
            }catch(error) {
                weightValueVob = 200.0;
            }

            local animationTime = 0;

            try {
                animationTime = VobsBuilder.bucket.inputAnimationTime.draw.getText().tointeger();
            }catch(error) {
                animationTime = 0;
            }

            try {
                keyValueVob = VobsBuilder.bucket.keySelector.draw.getText();
            }catch(error) {
                keyValueVob = "";
            }

            local vobName = VobsBuilder.bucket.inputName.draw.getText();

            if(vobName.len() < 3 || vobName == "Nazwa")
                vobName = "";

            if(typeVob == null || healthVob == null || movementVob == null)
                return;

            local vobDynamic = VobsBuilder.bucket.isDynamic.getChecked();

            local vob = VobsBuilder.bucketVob;
            local position = VobsBuilder.bucketVob.getPosition();
            local rotation = VobsBuilder.bucketVob.getRotation();

            local packet = Packet();
            packet.writeUInt8(PacketReceivers);
            packet.writeUInt8(Packets.Vob);
            packet.writeUInt8(VobPackets.Create);
            packet.writeUInt8(typeVob);
            packet.writeUInt8(healthVob);
            packet.writeUInt8(movementVob);
            packet.writeInt32(healthValueVob);
            packet.writeInt16(trigger);
            packet.writeInt16(triggerType);
            packet.writeInt16(triggerAnimation);
            packet.writeInt16(triggerTime);
            packet.writeString(keyValueVob);
            packet.writeUInt8(keyConfigurationVob);
            packet.writeInt16(animationValueVob);
            packet.writeInt16(animationTime);
            packet.writeFloat(weightValueVob);
            packet.writeBool(vobDynamic);
            packet.writeString(vobName);
            packet.writeString(vob.visual);
            packet.writeFloat(position.x);
            packet.writeFloat(position.y);
            packet.writeFloat(position.z);
            packet.writeFloat(rotation.x);
            packet.writeFloat(rotation.y);
            packet.writeFloat(rotation.z);
            packet.writeFloat(targetPosition.x);
            packet.writeFloat(targetPosition.y);
            packet.writeFloat(targetPosition.z);
            packet.writeFloat(targetRotation.x);
            packet.writeFloat(targetRotation.y);
            packet.writeFloat(targetRotation.z);
            packet.send(RELIABLE);

            VobsBuilder.hideAddWindow();
        });

        result.removeButton.bind(EventType.Click, function(element) {
            VobPacket(VobController.vobs[element.attribute]).remove();
        });

        return result;
    }

    show = function() {
        if(adminPlayer.role == PlayerRole.User)
            return;

        if(bucket == null)
            bucket = prepareWindow();

        local contentSecondary = [];
        contentSecondary.append({id = 0, name = "Od siebie."});

        foreach(vob in VobController.vobs)
            if(vob.type == VobType.Switch)
                contentSecondary.append({ id = vob.id, name = (vob.name ? vob.id+"-"+vob.name : vob.id+"-"+vob.visual) })

        bucket.triggerDoor.setContent(contentSecondary);

        bucket.windowDoor.setVisible(false);
        bucket.window.setVisible(false);
        bucket.list.setVisible(true);
        bucket.addButton.setVisible(true);

        startPosition = getPlayerPosition(heroId);

        refreshList();

        BaseGUI.show();
        ActiveGui = PlayerGUI.ServerBuilderVobs;
    }

    hide = function() {
        if(adminPlayer.role == PlayerRole.User)
            return;

        bucket.window.setVisible(false);
        bucket.windowDoor.setVisible(false);
        bucket.animationDoor.setVisible(false);
        bucket.triggerDoor.setVisible(false);
        bucket.list.setVisible(false);
        bucket.vobTypeSelector.setVisible(false);
        bucket.vobMovementSelector.setVisible(false);
        bucket.vobHealthSelector.setVisible(false);
        bucket.keySelector.setVisible(false);
        bucket.animationsSelector.setVisible(false);
        bucket.keyConfiguration.setVisible(false);
        bucket.addButton.setVisible(false);
        bucket.windowWithSelector.setVisible(false);
        bucket.removeButton.setVisible(false);

        setPlayerPosition(heroId, startPosition.x, startPosition.y, startPosition.z);
        foreach(index, item in bucket.bucketWithSelectors)
            item.render.visible = false;

        foreach(item in bucket.listContent)
            item.remove();

        bucket.listContent.clear();

        bucketVob = null;
        disableFreeCameraMode();
        freeCameraBucket = [];

        BaseGUI.hide();
        ActiveGui = null;
    }

    onChangeType = function(typeId) {
        switch(typeId) {
            case VobType.Door:
                bucket.windowDoor.setVisible(true);
                bucket.animationDoor.setVisible(true);
                bucket.triggerDoor.setVisible(true);

                local pos = bucketVob.getPosition();
                local rot = bucketVob.getRotation();

                bucket.triggerX.setText(pos.x.tostring());
                bucket.triggerY.setText(pos.y.tostring());
                bucket.triggerZ.setText(pos.z.tostring());

                bucket.triggerRotationX.setText(rot.x.tostring());
                bucket.triggerRotationY.setText(rot.y.tostring());
                bucket.triggerRotationZ.setText(rot.z.tostring());
            break;
            default:
                bucket.windowDoor.setVisible(false);
                bucket.animationDoor.setVisible(false);
                bucket.triggerDoor.setVisible(false);
            break;
        }
    }

    refreshList = function() {
        foreach(item in bucket.listContent)
            item.remove();

        bucket.listContent.clear();

        local visibleVobs = 0;
        local _index = -1;
        local vobs = VobController.vobs;
        local value = bucket.scrollbarList.getValue();
        value = value * 3;

        foreach(vob in vobs) {
            _index = _index + 1;

            if(_index < value)
                continue;

            if(visibleVobs >= 12)
                break;

            bucket.listContent.push(VobsBuilderObject(vob, visibleVobs));
            visibleVobs = visibleVobs + 1;
        }

        foreach(vobItem in bucket.listContent)
            vobItem.setVisible(true);
    }

    preview = function(id) {
        disableFreeCameraMode();

        local vob = VobController.vobs[id];
        bucket.removeButton.setVisible(true);
        bucket.removeButton.attribute = id;
        _Camera.enableMovement(true)
        _Camera.modeChangeEnabled = false;
        _Camera.setTargetVob(vob.object);

        if(bucket.window.m_visible)
            hideAddWindow();
    }

    toggleSelectorWindow = function() {
        bucket.windowWithSelector.m_visible ? hideSelectorWindow() : showSelectorWindow();
    }

    showSelectorWindow = function() {
        bucket.windowWithSelector.setVisible(true);
        changeSelectorWindow();
    }

    changeSelectorWindow = function() {
        local value = bucket.sliderWithSelector.getValue() * 16;

        foreach(index, item in bucket.bucketWithSelectors)
        {
            item.render.visible = true;
            item.render.top();

            if(index + value >= vobList.len())
                item.render.visual = "";
            else
                item.render.visual = vobList[index + value];
        }
    }

    checkObjIsSelector = function(objReffer) {
        local value = bucket.sliderWithSelector.getValue() * 16;
        foreach(index, item in bucket.bucketWithSelectors)
        {
            if(item.button == objReffer)
            {
                if(index + value < vobList.len())
                    bucketVob.visual = vobList[index + value]

                _Camera.enableMovement(true)
                _Camera.modeChangeEnabled = false;
                _Camera.setTargetVob(bucketVob);
                hideSelectorWindow();
                return;
            }
        }

        foreach(item in bucket.listContent) {
            if(item.background == objReffer) {
                preview(item.background.attribute);
                return;
            }
        }
    }

    hideSelectorWindow = function() {
        foreach(index, item in bucket.bucketWithSelectors)
            item.render.visible = false;

        bucket.windowWithSelector.setVisible(false);
    }

    toggleAddWindow = function() {
        bucket.window.m_visible ? hideAddWindow() : showAddWindow();
    }

    showAddWindow = function() {
        disableFreeCameraMode();
        bucket.window.setVisible(true);
        bucket.vobTypeSelector.setVisible(true);
        bucket.vobMovementSelector.setVisible(true);
        bucket.vobHealthSelector.setVisible(true);
        bucket.keySelector.setVisible(true);
        bucket.animationsSelector.setVisible(true);
        bucket.keyConfiguration.setVisible(true);
        bucket.removeButton.setVisible(false);

        local pos = _Camera.getPosition()
        bucketVob = Vob(vobList[0]);
        bucketVob.addToWorld();
        bucketVob.setPosition(pos.x,pos.y,pos.z)
        _Camera.enableMovement(true)
        _Camera.modeChangeEnabled = false;
        _Camera.setTargetVob(bucketVob);
    }

    hideAddWindow = function() {
        bucket.window.setVisible(false);
        bucket.vobTypeSelector.setVisible(false);
        bucket.keySelector.setVisible(false);
        bucket.animationsSelector.setVisible(false);
        bucket.keyConfiguration.setVisible(false);
        bucket.vobMovementSelector.setVisible(false);
        bucket.vobHealthSelector.setVisible(false);
        bucket.windowDoor.setVisible(false);
        bucket.animationDoor.setVisible(false);
        bucket.triggerDoor.setVisible(false);

        bucketVob = null
    },

    enableFreeCameraMode = function() {
        enableFreeCam(true);
        setCursorInFreeCamera("HK_CURSOR.TGA");
        freeCameraMode = true;
    },

    disableFreeCameraMode = function() {
        enableFreeCam(false);
        freeCameraMode = false;
        setFreeze(true);
        Camera.setFreeze(true);
        setCursorInFreeCamera("HK_CURSOR.TGA");
        setCursorTxt("HK_CURSOR.TGA");
        freeCameraBucket = [];
        BaseGUI.hide();
        BaseGUI.show();
    }

    onKey = function(key)
    {
        if(bucketVob == null) {
            if(key == KEY_B)
                freeCameraMode ? disableFreeCameraMode() : enableFreeCameraMode();
            return;
        }

        switch(key)
        {
            case KEY_NUMPAD5: case KEY_NUMPAD8: case KEY_NUMPAD4: case KEY_NUMPAD6: case KEY_NUMPAD7: case KEY_NUMPAD9: case KEY_E: case KEY_Q: case KEY_DOWN: case KEY_UP: case KEY_LEFT: case KEY_RIGHT: case KEY_A: case KEY_D: case KEY_W: case KEY_S:
                activeKeys.append(key);
            break;
            case KEY_Z:
                local currentIndex = null;
                foreach(index, vob in vobList) {
                    if(vob.toupper() == bucketVob.visual.toupper())
                        currentIndex = index;
                }

                if(currentIndex == 0)
                    return;

                currentIndex = currentIndex - 1;
                bucketVob.visual = vobList[currentIndex];
            break;
            case KEY_X:
                local currentIndex = null;
                foreach(index, vob in vobList) {
                    if(vob.toupper() == bucketVob.visual.toupper())
                        currentIndex = index;
                }

                if(currentIndex == null) {
                    return;
                }

                currentIndex = currentIndex + 1;
                bucketVob.visual = vobList[currentIndex];
            break;
            case KEY_C:
                toggleSelectorWindow();
            break;
        }
    }

    onMouseClick = function(buttonId)
    {
        if(freeCameraMode == false)
            return;

        local heroPos = getPlayerPosition(heroId);
        if(buttonId == MOUSE_RMB) {
            local cursorPosition = getCursorPositionPx();
            foreach(texture in freeCameraBucket) {
                local position = texture.obj.getPositionPx();
                local size = texture.obj.getSizePx();
                local isIn = (cursorPosition.x >= position.x && cursorPosition.x <= position.x + size.width && cursorPosition.y >= position.y && cursorPosition.y <= position.y + size.height);

                if(isIn) {
                    preview(texture.id);
                    return;
                }
            }
        }
    }

    onRender = function() {
        if(bucketVob == null) {
            if(freeCameraMode) {
                freeCameraBucket.clear();

                local heroPos = _Camera.getPosition();
                local cursorPosition = getCursorPositionPx();

                foreach(vob in VobController.vobs) {
                    local projection = _Camera.project(vob.position.x, vob.position.y, vob.position.z);
                    if(projection == null)
                        continue;

                    local distance = getPositionDifference(heroPos, vob.position);
                    if(distance > 3000)
                        continue;

                    local axis = 300 - abs(distance/10);
                    local range = {x = anx(projection.x) - axis, y = any(projection.y) - axis, width = axis * 2, height = axis * 2};

                    freeCameraBucket.push({obj = Texture(range.x, range.y, range.width, range.height, "WHITE.TGA"), id = vob.id});
                }

                foreach(draw in freeCameraBucket) {
                    draw.obj.setColor(255, 0, 0);
                    draw.obj.alpha = 50;
                    draw.obj.visible = true;
                }
            }
            return;
        }

        local speed = 1;

        if(isKeyPressed(KEY_LSHIFT))
            speed = 10;

        foreach(index, key in activeKeys)
        {
            if(!isKeyPressed(key))
            {
                activeKeys.remove(index);
                continue;
            }

            local position = bucketVob.getPosition();
            local rotation = bucketVob.getRotation();

            switch(key)
            {
                case KEY_DOWN: case KEY_S:
                    position.x = position.x - (sin(rotation.y * 3.14 / 180.0) * speed);
                    position.z = position.z - (cos(rotation.y * 3.14 / 180.0) * speed);
                break;
                case KEY_UP: case KEY_W:
                    position.x = position.x + (sin(rotation.y * 3.14 / 180.0) * speed);
                    position.z = position.z + (cos(rotation.y * 3.14 / 180.0) * speed);
                break;
                case KEY_LEFT: case KEY_A:
                    position.x = position.x + (sin(((rotation.y - 90) - floor(rotation.y / 360) * 360) * 3.14 / 180.0) * speed);
                    position.z = position.z + (cos(((rotation.y - 90) - floor(rotation.y / 360) * 360) * 3.14 / 180.0) * speed);
                break;
                case KEY_RIGHT: case KEY_D:
                    position.x = position.x + (sin(((rotation.y + 90) - floor(rotation.y / 360) * 360) * 3.14 / 180.0) * speed);
                    position.z = position.z + (cos(((rotation.y + 90) - floor(rotation.y / 360) * 360) * 3.14 / 180.0) * speed);
                break;
                case KEY_Q:
                    position.y = position.y + speed;
                break;
                case KEY_E:
                    position.y = position.y - speed;
                break;
                case KEY_NUMPAD4:
                    rotation.y = rotation.y + speed;
                break;
                case KEY_NUMPAD6:
                    rotation.y = rotation.y - speed;
                break;
                case KEY_NUMPAD5:
                    rotation.x = rotation.x + speed;
                break;
                case KEY_NUMPAD8:
                    rotation.x = rotation.x - speed;
                break;
                case KEY_NUMPAD7:
                    rotation.z = rotation.z + speed;
                break;
                case KEY_NUMPAD9:
                    rotation.z = rotation.z - speed;
                break;
            }

            bucketVob.setPosition(position.x, position.y, position.z);
            bucketVob.setRotation(rotation.x, rotation.y, rotation.z);
        }
    }
}

Bind.addKey(KEY_ESCAPE, function() { VobsBuilder.hide(); }, PlayerGUI.ServerBuilderVobs)
addEventHandler("onKey", function(key) {
    if(ActiveGui == PlayerGUI.ServerBuilderVobs)
        VobsBuilder.onKey(key);
})
addEventHandler("onRender", function() {
    if(ActiveGui == PlayerGUI.ServerBuilderVobs)
        VobsBuilder.onRender();
})
addEventHandler("onVobAdd", function(vobId) {
    if(ActiveGui == PlayerGUI.ServerBuilderVobs)
        VobsBuilder.refreshList();
})
addEventHandler("onVobRemove", function(vobId) {
    if(ActiveGui == PlayerGUI.ServerBuilderVobs)
        VobsBuilder.refreshList();
})
addEventHandler("GUI.onChange", function(obj) {
    if(ActiveGui == PlayerGUI.ServerBuilderVobs)
        if(obj == VobsBuilder.bucket.sliderWithSelector)
            VobsBuilder.changeSelectorWindow();
});
addEventHandler("GUI.onClick", function(obj) {
    if(ActiveGui == PlayerGUI.ServerBuilderVobs)
        VobsBuilder.checkObjIsSelector(obj);
});
addEventHandler("onMouseClick", function(button) {
    if(ActiveGui == PlayerGUI.ServerBuilderVobs)
        VobsBuilder.onMouseClick(button);
});


class VobsBuilderObject {
    index = null
    background = null;
    render = null;
    name = null;
    author = null;

    constructor(object, index) {
        local pos = {x = 0, y = 0}
        for(local i = 0; i < index; i ++) {
            pos.x = pos.x + 1;
            if(pos.x >= 3) {
                pos.x = 0
                pos.y = pos.y + 1;
            }
        }

        background = GUI.Button(anx(50) + anx(pos.x * 200), any(Resolution.y/2 - 400) + any(200 * pos.y), anx(200), any(200), "HK_BUTTON.TGA", "");
        background.attribute = object.id
        local backgroundPosition = background.getPosition();
        render = ItemRender(backgroundPosition.x + anx(25), backgroundPosition.y + any(10), anx(150), any(150), "");
        render.visual = object.visual;
        name = Draw(backgroundPosition.x + 20, backgroundPosition.y + any(150), object.id + "-"+(object.name ? object.name+"-" : "")+object.visual);
        if(name.text.len() > 15) {
            name.text = name.text.slice(0, 15) + "...";
        }

        name.font = "FONT_OLD_20_WHITE_HI.TGA";
        name.setScale(0.5, 0.5);
        name.setColor(255, 248, 196);

        author = Draw(backgroundPosition.x + 20, backgroundPosition.y + any(175), object.createdBy);
        if(author.text.len() > 15) {
            author.text = author.text.slice(0, 15) + "...";
        }

        author.font = "FONT_OLD_20_WHITE_HI.TGA";
        author.setScale(0.4, 0.4);
        author.setColor(255, 248, 196);
    }

    function setVisible(value) {
        background.setVisible(value);
        render.visible = value;
        name.visible = value;
        author.visible = value;
    }

    function remove() {
        background.setVisible(false);
        background.destroy();
        name = null;
        render = null;
        author = null;
    }
}
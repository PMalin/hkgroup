
DrawsBuilder <- {
    bucket = null,
    bucketDraw = null,

    startPosition = null,

    prepareWindow = function() {
        local result = {};

        result.window <- GUI.Window(anx(Resolution.x - 400), any(Resolution.y/2 - 300), anx(350), any(680), "BLACK.TGA");
        result.list <- MyGUI.SelectList(anx(40), any(Resolution.y/2 - 300), anx(450), any(600))
        result.addButton <- GUI.Button(anx(Resolution.x/2 - 200), any(Resolution.y - 100), anx(180), any(60), "HK_BUTTON.TGA", "Dodaj nowy");
        result.removeButton <- GUI.Button(anx(Resolution.x/2 + 20), any(Resolution.y - 100), anx(180), any(60), "HK_BUTTON.TGA", "Usu�");

        result.inputText <- GUI.Input(anx(15), any(20), anx(330), any(60), "HK_BUTTON.TGA", "FONT_OLD_10_WHITE_HI.TGA", Input.Text, Align.Left, "1 Linijka", 2, result.window)
        result.inputText2 <- GUI.Input(anx(15), any(90), anx(330), any(60), "HK_BUTTON.TGA", "FONT_OLD_10_WHITE_HI.TGA", Input.Text, Align.Left, "2 Linijka", 2, result.window)
        result.inputText3 <- GUI.Input(anx(15), any(160), anx(330), any(60), "HK_BUTTON.TGA", "FONT_OLD_10_WHITE_HI.TGA", Input.Text, Align.Left, "3 Linijka", 2, result.window)
        result.inputR <- GUI.Input(anx(15), any(250), anx(60), any(60), "HK_BUTTON.TGA", "FONT_OLD_10_WHITE_HI.TGA", Input.Numbers, Align.Center, "255", 2, result.window)
        result.inputG <- GUI.Input(anx(125), any(250), anx(60), any(60), "HK_BUTTON.TGA", "FONT_OLD_10_WHITE_HI.TGA", Input.Numbers, Align.Center, "255", 2, result.window)
        result.inputB <- GUI.Input(anx(235), any(250), anx(60), any(60), "HK_BUTTON.TGA", "FONT_OLD_10_WHITE_HI.TGA", Input.Numbers, Align.Center, "255", 2, result.window)
        result.distance <- GUI.Input(anx(15), any(350), anx(330), any(60), "HK_BUTTON.TGA", "FONT_OLD_10_WHITE_HI.TGA", Input.Numbers, Align.Center, "3000", 2, result.window)
        result.updateButton <- GUI.Button(anx(15), any(450), anx(330), any(60), "HK_BUTTON.TGA", "Aktualizuj", result.window);
        result.saveButton <- GUI.Button(anx(15), any(530), anx(330), any(60), "HK_BUTTON.TGA", "Zapisz", result.window);

        result.updateButton.bind(EventType.Click, function(element) {
            local r = DrawsBuilder.bucket.inputR.draw.getText().tointeger();
            local g = DrawsBuilder.bucket.inputG.draw.getText().tointeger();
            local b = DrawsBuilder.bucket.inputB.draw.getText().tointeger();

            local distance = DrawsBuilder.bucket.distance.draw.getText().tointeger();

            local line1 = DrawsBuilder.bucket.inputText.getText();
            local line2 = DrawsBuilder.bucket.inputText2.getText();
            local line3 = DrawsBuilder.bucket.inputText3.getText();
            local bucketDraw = DrawsBuilder.bucketDraw;

            local pos = bucketDraw.getWorldPosition();

            DrawsBuilder.bucketDraw = Draw3d(pos.x, pos.y, pos.z);
            local bucketDraw = DrawsBuilder.bucketDraw;
            bucketDraw.visible = true;

            if(line1.len() > 2)
                bucketDraw.insertText(line1);

            if(line2.len() > 2 && line2 != "2 linijka..")
                bucketDraw.insertText(line2);

            if(line3.len() > 2 && line3 != "3 linijka..")
                bucketDraw.insertText(line3);

            bucketDraw.setColor(r,g,b);
            bucketDraw.distance = distance;
        });

        result.saveButton.bind(EventType.Click, function(element) {
            local r = DrawsBuilder.bucket.inputR.draw.getText().tointeger();
            local g = DrawsBuilder.bucket.inputG.draw.getText().tointeger();
            local b = DrawsBuilder.bucket.inputB.draw.getText().tointeger();

            local distance = DrawsBuilder.bucket.distance.draw.getText().tointeger();

            local line1 = DrawsBuilder.bucket.inputText.getText();
            local line2 = DrawsBuilder.bucket.inputText2.getText();
            local line3 = DrawsBuilder.bucket.inputText3.getText();
            local bucketDraw = DrawsBuilder.bucketDraw;

            local text = [];

            if(line1.len() > 2)
                text.append(line1);

            if(line2.len() > 2 && line2 != "2 linijka..")
                text.append(line2);

            if(line3.len() > 2 && line3 != "3 linijka..")
                text.append(line3);

            local pos = bucketDraw.getWorldPosition();

            DrawPacket().create(DrawController.getNextId(), distance, r, g, b, pos.x, pos.y, pos.z, getWorld(), text);

            DrawsBuilder.hideAddWindow();
        });

        result.addButton.bind(EventType.Click, function(element) {
            DrawsBuilder.toggleAddWindow();
        });

        result.removeButton.bind(EventType.Click, function(element) {
            DrawPacket().remove(element.attribute);
        });

        return result;
    }

    show = function() {
        if(adminPlayer.role == PlayerRole.User)
            return;

        if(bucket == null)
            bucket = prepareWindow();

        startPosition = getPlayerPosition(heroId);

        foreach(draw in DrawController.draws)
            bucket.list.addOption(draw.id, draw.text[0]);

        bucket.list.onClick = function(id) {
            DrawsBuilder.preview(id);
        }

        bucket.list.setVisible(true);
        bucket.addButton.setVisible(true);

        BaseGUI.show();
        ActiveGui = PlayerGUI.ServerBuilderDraws;
    }

    hide = function() {
        if(adminPlayer.role == PlayerRole.User)
            return;

        if(bucket.window.m_visible)
            hideAddWindow();

        setPlayerPosition(heroId, startPosition.x, startPosition.y, startPosition.z);

        bucket.window.setVisible(false);
        bucket.list.setVisible(false);
        bucket.addButton.setVisible(false);
        bucket.removeButton.setVisible(false);

        BaseGUI.hide();
        ActiveGui = null;
    }

    refreshList = function() {
        bucket.list.clear();

        foreach(draw in DrawController.draws)
            bucket.list.addOption(draw.id, draw.text[0]);
    }

    preview = function(id) {
        local draw = DrawController.draws[id];
        Camera.setBeforePosFromTop(400, draw.position.x, draw.position.y, draw.position.z);
        bucket.removeButton.setVisible(true);
        bucket.removeButton.attribute = id;

        if(bucket.window.m_visible)
            hideAddWindow();
    }

    toggleAddWindow = function() {
        bucket.window.m_visible ? hideAddWindow() : showAddWindow();
    }

    showAddWindow = function() {
        bucket.window.setVisible(true);
        bucket.removeButton.setVisible(false);

        local pos = _Camera.getPosition()
        bucketDraw = Draw3d(pos.x, pos.y, pos.z);
        bucketDraw.visible = true;
        bucketDraw.distance = 3000;
        bucketDraw.insertText("Draw...");

        bucket.inputText.setText("Draw...");
        bucket.inputText2.setText("2 linijka..");
        bucket.inputText3.setText("3 linijka..");

        bucket.inputR.setText("255");
        bucket.inputG.setText("255");
        bucket.inputB.setText("255");
        bucket.distance.setText("3000");

        Camera.setBeforePosFromTop(400, pos.x, pos.y, pos.z);
    }

    hideAddWindow = function() {
        bucket.window.setVisible(false);

        bucketDraw = null
    },

    onKey = function(key)
    {
        if(bucketDraw == null)
            return;
    }

    onRender = function()
    {
        if(bucketDraw == null)
            return;

        local val = 1;

        if(isKeyPressed(KEY_LSHIFT))
            val = 10;

        local drawPos = bucketDraw.getWorldPosition();
        if(isKeyPressed(KEY_LEFT))
            drawPos.x = drawPos.x - val;
        if(isKeyPressed(KEY_RIGHT))
            drawPos.x = drawPos.x + val;
        if(isKeyPressed(KEY_UP))
            drawPos.z = drawPos.z - val;
        if(isKeyPressed(KEY_DOWN))
            drawPos.z = drawPos.z + val;
        if(isKeyPressed(KEY_Q))
            drawPos.y = drawPos.y - val;
        if(isKeyPressed(KEY_E))
            drawPos.y = drawPos.y + val;

        bucketDraw.setWorldPosition(drawPos.x, drawPos.y, drawPos.z);
        Camera.setBeforePosFromTop(400, drawPos.x, drawPos.y, drawPos.z);
        setPlayerPosition(heroId, drawPos.x, drawPos.y - 100, drawPos.z);
    }
}

Bind.addKey(KEY_ESCAPE, function() { DrawsBuilder.hide(); }, PlayerGUI.ServerBuilderDraws)
addEventHandler("onKey", function(key) {
    if(ActiveGui == PlayerGUI.ServerBuilderDraws)
        DrawsBuilder.onKey(key);
})
addEventHandler("onRender", function() {
    if(ActiveGui == PlayerGUI.ServerBuilderDraws)
        DrawsBuilder.onRender();
})

BotsBuilder <- {
    bucket = null,
    bucketBot = null,
    workingObject = null,
    previewId = -1,
    activeTab = 0,

    startPosition = null,
    list = {},

    prepareWindow = function() {
        local result = {};

        result.list <- MyGUI.SelectList(anx(40), any(Resolution.y/2 - 300), anx(450), any(600))

        result.addButton <- GUI.Button(anx(40), any(Resolution.y/2 + 380), anx(180), any(60), "HK_BUTTON.TGA", "Dodaj nowy");
        result.previewWindow <- GUI.Window(anx(Resolution.x - 400), any(100), anx(370), any(680), "BLACK.TGA");
        result.settingsButton <- GUI.Button(anx(10), any(10), anx(110), any(60), "HK_BUTTON.TGA", "Ustawienia", result.previewWindow);
        result.characterButton <- GUI.Button(anx(120), any(10), anx(110), any(60), "HK_BUTTON.TGA", "Statystyki", result.previewWindow);
        result.relationsButton <- GUI.Button(anx(230), any(10), anx(110), any(60), "HK_BUTTON.TGA", "Relacje", result.previewWindow);
        result.updateButton <- GUI.Button(anx(10), any(720), anx(165), any(60), "HK_BUTTON.TGA", "Aktualizuj", result.previewWindow);
        result.saveButton <- GUI.Button(anx(175), any(720), anx(165), any(60), "HK_BUTTON.TGA", "Zapisz", result.previewWindow);
        result.deleteButton <- GUI.Button(anx(10), any(800), anx(165), any(60), "HK_BUTTON.TGA", "Usu�", result.previewWindow);

        result.settingsButton.bind(EventType.Click, function(element) {BotsBuilder.onSwitch(0);});
        result.characterButton.bind(EventType.Click, function(element) {BotsBuilder.onSwitch(1);});
        result.relationsButton.bind(EventType.Click, function(element) {BotsBuilder.onSwitch(2);});

        result.updateButton.bind(EventType.Click, function(element) {
            BotsBuilder.updateSwitch();
        });

        result.saveButton.bind(EventType.Click, function(element) {
            BotsBuilder.save();
        });

        result.deleteButton.bind(EventType.Click, function(element) {
            BotsBuilder.remove();
        });

        result.internals <- [];

        result.addButton.bind(EventType.Click, function(element) {
            BotsBuilder.hideCreate();
            BotsBuilder.bucketBot = createNpc("Bot Nowy");
            local position = Camera.getPosition();
            local element = BotsBuilder.bucketBot;

            spawnNpc(element);
            setPlayerPosition(element, position.x, position.y, position.z);

            BotsBuilder.workingObject = {
                group = "",
                name = "Bot",
                instance = "PC_HERO",
                isTemporary = true,
                isSaved = false,
                position = clone position,
                angle = 1,
                str = 10,
                dex = 10,
                hp = 40,
                hpMax = 40,
                scale = {x = 1.0, y = 1.0, z = 1.0},
                animation = "",
                weapon = [0,0,0,0],
                respawnTime = 10,
                melee = -1,
                armor = -1,
                ranged = -1,
                helmet = -1,
                shield = -1,
                magic = -1,
                bodyModel = "Hum_Body_Naked0",
                bodyTxt = 1,
                headModel = "Hum_Head_FatBald",
                headTxt = 1,
                effects = [],
            }
            BotsBuilder.bucket.previewWindow.setVisible(true);
            BotsBuilder.onSwitch(0);
        });

        return result;
    }

    show = function() {
        if(adminPlayer.role == PlayerRole.User)
            return;

        if(bucket == null)
            bucket = prepareWindow();

        startPosition = getPlayerPosition(heroId);
        bucket.list.setVisible(true);
        bucket.addButton.setVisible(true);
        list.clear();

        BaseGUI.show();
        ActiveGui = PlayerGUI.ServerBuilderBots;

        bucket.list.onClick = function(id) {
            BotsBuilder.preview(id);
        }

        AdminPacket().turnOnInvisibility();
        AdminPacket().getAllBots();
    }

    hide = function() {
        if(adminPlayer.role == PlayerRole.User)
            return;

        setPlayerPosition(heroId, startPosition.x, startPosition.y, startPosition.z);

        bucket.list.setVisible(false);
        bucket.addButton.setVisible(false);
        bucket.previewWindow.setVisible(false);
        AdminPacket().turnOffInvisibility();
        BaseGUI.hide();
        destroyInternals();
        hideCreate();
        ActiveGui = null;
    }

    addBot = function(id, object) {
        list[id] <- object;

        bucket.list.addOption(id, object.name+"("+id+")");
    }

    preview = function(id) {
        local bot = BotsBuilder.list[id];
        Camera.setBeforePosFromTop(400, bot.position.x, bot.position.y, bot.position.z);
        setPlayerPosition(heroId, bot.position.x + 120, bot.position.y + 50, bot.position.z)

        workingObject = list[id];

        BotsBuilder.bucket.previewWindow.setVisible(true);
        BotsBuilder.previewId = id;
        BotsBuilder.onSwitch(0);
    }

    refreshList = function() {
        bucket.list.clear();

        foreach(itemId, item in list)
            bucket.list.addOption(itemId, item.name+"("+itemId+")");
    }

    hideCreate = function() {
        if(bucketBot != null) {
            destroyNpc(bucketBot);
            bucketBot = null;
        }
    }

    function destroyInternals() {
        foreach(internal in bucket.internals) {
            internal.setVisible(false);
            internal.destroy();
        }
        bucket.internals.clear();
    }

    showSettingsPage = function() {
        local yesOrNoTable = [];
        yesOrNoTable.append({id = 0, name = "Nie"});
        yesOrNoTable.append({id = 1, name = "Tak"});

        local startPosition = {x = anx(Resolution.x - 400), y = any(100) + any(60) }

        bucket.internals.append(GUI.Input(startPosition.x + anx(20), startPosition.y + any(20) + any(20), anx(330), any(40), "HK_INPUT.TGA", "FONT_OLD_10_WHITE_HI.TGA", Input.Text, Align.Left, "Nazwa", 2));
        bucket.internals.append(GUI.Input(startPosition.x + anx(20), startPosition.y + any(90) + any(20), anx(330), any(40), "HK_INPUT.TGA", "FONT_OLD_10_WHITE_HI.TGA", Input.Text, Align.Left, "Nazwa", 2));
        bucket.internals.append(GUI.Input(startPosition.x + anx(20), startPosition.y + any(160) + any(20), anx(330), any(40), "HK_INPUT.TGA", "FONT_OLD_10_WHITE_HI.TGA", Input.Text, Align.Left, "Instance", 2));
        bucket.internals.append(MyGUI.DropDown(startPosition.x + anx(20), startPosition.y + any(44240) + any(20), anx(330), any(40), "Zapisa� po restarcie?", yesOrNoTable, "Czy zapisa� po restarcie..", workingObject.isSaved ? 1 : 0));
        bucket.internals.append(MyGUI.DropDown(startPosition.x + anx(20), startPosition.y + any(310) + any(20), anx(330), any(40), "Usun�� po �mierci?", yesOrNoTable, "Czy usun�� po �mierci..", workingObject.isTemporary ? 1 : 0));
        bucket.internals.append(GUI.Input(startPosition.x + anx(20), startPosition.y + any(400) + any(20), anx(110), any(40), "HK_INPUT.TGA", "FONT_OLD_10_WHITE_HI.TGA", Input.Text, Align.Left, "X", 2));
        bucket.internals.append(GUI.Input(startPosition.x + anx(130), startPosition.y + any(400) + any(20), anx(110), any(40), "HK_INPUT.TGA", "FONT_OLD_10_WHITE_HI.TGA", Input.Text, Align.Left, "Y", 2));
        bucket.internals.append(GUI.Input(startPosition.x + anx(240), startPosition.y + any(400) + any(20), anx(110), any(40), "HK_INPUT.TGA", "FONT_OLD_10_WHITE_HI.TGA", Input.Text, Align.Left, "Z", 2));
        bucket.internals.append(GUI.Input(startPosition.x + anx(20), startPosition.y + any(490) + any(20), anx(330), any(40), "HK_INPUT.TGA", "FONT_OLD_10_WHITE_HI.TGA", Input.Text, Align.Left, "Angle", 2));
        bucket.internals.append(GUI.Input(startPosition.x + anx(20), startPosition.y + any(580) + any(20), anx(110), any(40), "HK_INPUT.TGA", "FONT_OLD_10_WHITE_HI.TGA", Input.Text, Align.Left, "X", 2));
        bucket.internals.append(GUI.Input(startPosition.x + anx(130), startPosition.y + any(580) + any(20), anx(110), any(40), "HK_INPUT.TGA", "FONT_OLD_10_WHITE_HI.TGA", Input.Text, Align.Left, "Y", 2));
        bucket.internals.append(GUI.Input(startPosition.x + anx(240), startPosition.y + any(580) + any(20), anx(110), any(40), "HK_INPUT.TGA", "FONT_OLD_10_WHITE_HI.TGA", Input.Text, Align.Left, "Z", 2));

        bucket.internals.append(GUI.Draw(startPosition.x + anx(30),startPosition.y + any(0), "Grupa"));
        bucket.internals.append(GUI.Draw(startPosition.x + anx(30),startPosition.y + any(380), "Pozycja"));
        bucket.internals.append(GUI.Draw(startPosition.x + anx(30),startPosition.y + any(470), "Kierunek"));
        bucket.internals.append(GUI.Draw(startPosition.x + anx(30),startPosition.y + any(560), "Skalowanie"));

        bucket.internals[0].setText(workingObject.group);
        bucket.internals[1].setText(workingObject.name);
        bucket.internals[2].setText(workingObject.instance);
        bucket.internals[5].setText(workingObject.position.x.tostring());
        bucket.internals[6].setText(workingObject.position.y.tostring());
        bucket.internals[7].setText(workingObject.position.z.tostring());
        bucket.internals[8].setText(workingObject.angle.tostring());
        bucket.internals[9].setText(workingObject.scale.x.tostring());
        bucket.internals[10].setText(workingObject.scale.y.tostring());
        bucket.internals[11].setText(workingObject.scale.z.tostring());

        foreach(internal in bucket.internals) {
            internal.setVisible(true);
        }
    }

    updateSettingsPage = function() {
        local element = BotsBuilder.bucketBot != null ? BotsBuilder.bucketBot : getBot(previewId).element;
        local scale = getPlayerScale(element);
        workingObject.group = bucket.internals[0].getText();
        workingObject.name = bucket.internals[1].getText();
        workingObject.scale = {x = bucket.internals[9].getText().tofloat(),y = bucket.internals[10].getText().tofloat(),z = bucket.internals[11].getText().tofloat()}
        workingObject.instance = bucket.internals[2].getText();
        workingObject.isTemporary = bucket.internals[4].getValue();

        setPlayerName(element, workingObject.name);
        setPlayerInstance(element, workingObject.instance);
        setPlayerScale(element, workingObject.scale.x,workingObject.scale.y,workingObject.scale.z);
    }

    showRelationPage = function() {
        local startPosition = {x = anx(Resolution.x - 400), y = any(100) }

        bucket.internals.append(GUI.Input(startPosition.x + anx(20), startPosition.y + any(80) + any(20), anx(165), any(40), "HK_INPUT.TGA", "FONT_OLD_10_WHITE_HI.TGA", Input.Text, Align.Left, "", 2));
        bucket.internals.append(GUI.Input(startPosition.x + anx(185), startPosition.y + any(80) + any(20), anx(165), any(40), "HK_INPUT.TGA", "FONT_OLD_10_WHITE_HI.TGA", Input.Text, Align.Left, "", 2));

        bucket.internals[0].setText(workingObject.animation);
        bucket.internals[1].setText(workingObject.respawnTime);

        bucket.internals.append(GUI.Input(startPosition.x + anx(20), startPosition.y + any(160) + any(20), anx(165), any(40), "HK_INPUT.TGA", "FONT_OLD_10_WHITE_HI.TGA", Input.Text, Align.Left, "", 2));
        bucket.internals.append(GUI.Input(startPosition.x + anx(185), startPosition.y + any(160) + any(20), anx(165), any(40), "HK_INPUT.TGA", "FONT_OLD_10_WHITE_HI.TGA", Input.Text, Align.Left, "", 2));

        bucket.internals[2].setText(workingObject.bodyModel.tostring());
        bucket.internals[3].setText(workingObject.bodyTxt.tostring());

        bucket.internals.append(GUI.Input(startPosition.x + anx(20), startPosition.y + any(240) + any(20), anx(165), any(40), "HK_INPUT.TGA", "FONT_OLD_10_WHITE_HI.TGA", Input.Text, Align.Left, "", 2));
        bucket.internals.append(GUI.Input(startPosition.x + anx(185), startPosition.y + any(240) + any(20), anx(165), any(40), "HK_INPUT.TGA", "FONT_OLD_10_WHITE_HI.TGA", Input.Text, Align.Left, "", 2));

        bucket.internals[4].setText(workingObject.headModel.tostring());
        bucket.internals[5].setText(workingObject.headTxt.tostring());

        bucket.internals.append(GUI.Draw(startPosition.x + anx(30),startPosition.y + any(60), "Animacja"));
        bucket.internals.append(GUI.Draw(startPosition.x + anx(195),startPosition.y + any(60), "Czas odrodzenia"));
        bucket.internals.append(GUI.Draw(startPosition.x + anx(30),startPosition.y + any(140), "P�e�"));
        bucket.internals.append(GUI.Draw(startPosition.x + anx(195),startPosition.y + any(140), "Cia�o"));
        bucket.internals.append(GUI.Draw(startPosition.x + anx(30),startPosition.y + any(220), "G�owa"));
        bucket.internals.append(GUI.Draw(startPosition.x + anx(195),startPosition.y + any(220), "Twarz"));
        bucket.internals.append(GUI.Button(startPosition.x + anx(30),startPosition.y + any(300), anx(165), any(40), "HK_BUTTON.TGA", "Dodaj efekt"));
        bucket.internals.append(GUI.Button(startPosition.x + anx(185),startPosition.y + any(300), anx(165), any(40), "HK_BUTTON.TGA", "Usu� efekt"));

        bucket.internals[bucket.internals.len() - 2].bind(EventType.Click, function(element) {
            local position = BotsBuilder.bucket.internals[BotsBuilder.bucket.internals.len() - 2].getPosition();

            local animationsContent = [];
            animationsContent.append({ id = 0, name = "Brak" })
            foreach(effectId, effect in System_Effects)
                animationsContent.append({ id = effectId.tointeger(), name = effect })

            BotsBuilder.bucket.internals.append(MyGUI.DropDown(position.x + anx(20), position.y + any(60) + any(20), anx(165), any(40), "Animacja", animationsContent, "Wybierz animacj�..", 0));
            BotsBuilder.bucket.internals.append(GUI.Input(position.x + anx(185), position.y + any(60) + any(20), anx(165), any(40), "HK_INPUT.TGA", "FONT_OLD_10_WHITE_HI.TGA", Input.Text, Align.Left, 0, 2));

            foreach(internal in BotsBuilder.bucket.internals)
                internal.setVisible(true);
        });

        bucket.internals[bucket.internals.len() - 1].bind(EventType.Click, function(element) {
            local len = BotsBuilder.bucket.internals.len();
            if(len < 16)
                return;

            BotsBuilder.bucket.internals[len - 2].setVisible(false);
            BotsBuilder.bucket.internals[len - 2].destroy();
            BotsBuilder.bucket.internals[len - 1].setVisible(false);
            BotsBuilder.bucket.internals[len - 1].destroy();
            BotsBuilder.bucket.internals.remove(len - 1);
            BotsBuilder.bucket.internals.remove(len - 1);
        });

        foreach(v, effect in workingObject.effects) {
            local animationsContent = [];
            animationsContent.append({ id = 0, name = "Brak" })
            foreach(effectId, effect in System_Effects)
                animationsContent.append({ id = effectId.tointeger(), name = effect })

            bucket.internals.append(MyGUI.DropDown(startPosition.x + anx(20), startPosition.y + any(320 + 60 * v) + any(20), anx(165), any(40), "Animacja", animationsContent, "Wybierz animacj�..", effect.effectId));
            bucket.internals.append(GUI.Input(startPosition.x + anx(185), startPosition.y + any(320 + 60 * v) + any(20), anx(165), any(40), "HK_INPUT.TGA", "FONT_OLD_10_WHITE_HI.TGA", Input.Text, Align.Left, effect.refreshTime, 2));
        }

        foreach(internal in bucket.internals)
            internal.setVisible(true);
    }

    updateRelationPage = function() {
        local element = BotsBuilder.bucketBot != null ? BotsBuilder.bucketBot : getBot(previewId).element;
        workingObject.animation = bucket.internals[0].getText();
        workingObject.respawnTime = bucket.internals[1].getText();
        workingObject.bodyModel = bucket.internals[2].getText();
        workingObject.bodyTxt = bucket.internals[3].getText().tointeger();
        workingObject.headModel = bucket.internals[4].getText();
        workingObject.headTxt = bucket.internals[5].getText().tointeger();
        workingObject.effects = [];

        local len = BotsBuilder.bucket.internals.len();
        if(len > 16)
        {
            for(local i = len - 15; i < len; i = i + 2) {
                workingObject.effects.append({effectId = bucket.internals[i].getValue(), refreshTime = bucket.internals[i + 1].getText().tointeger()})
            }
        }

        playAni(element, workingObject.animation);
        setPlayerVisual(element, workingObject.bodyModel, workingObject.bodyTxt, workingObject.headModel, workingObject.headTxt);
    }

    showStatisticPage = function() {
        local startPosition = {x = anx(Resolution.x - 400), y = any(100) }

        bucket.internals.append(GUI.Input(startPosition.x + anx(20), startPosition.y + any(80) + any(20), anx(165), any(40), "HK_INPUT.TGA", "FONT_OLD_10_WHITE_HI.TGA", Input.Text, Align.Left, "Si�a", 2));
        bucket.internals.append(GUI.Input(startPosition.x + anx(185), startPosition.y + any(80) + any(20), anx(165), any(40), "HK_INPUT.TGA", "FONT_OLD_10_WHITE_HI.TGA", Input.Text, Align.Left, "Zr�czno��", 2));

        bucket.internals[0].setText(workingObject.str.tostring());
        bucket.internals[1].setText(workingObject.dex.tostring());

        bucket.internals.append(GUI.Input(startPosition.x + anx(20), startPosition.y + any(160) + any(20), anx(165), any(40), "HK_INPUT.TGA", "FONT_OLD_10_WHITE_HI.TGA", Input.Text, Align.Left, "�ycie", 2));
        bucket.internals.append(GUI.Input(startPosition.x + anx(185), startPosition.y + any(160) + any(20), anx(165), any(40), "HK_INPUT.TGA", "FONT_OLD_10_WHITE_HI.TGA", Input.Text, Align.Left, "Max. �ycie", 2));

        bucket.internals[2].setText(workingObject.hp.tostring());
        bucket.internals[3].setText(workingObject.hpMax.tostring());

        bucket.internals.append(GUI.Input(startPosition.x + anx(20), startPosition.y + any(240) + any(20), anx(165), any(40), "HK_INPUT.TGA", "FONT_OLD_10_WHITE_HI.TGA", Input.Text, Align.Left, "1h", 2));
        bucket.internals.append(GUI.Input(startPosition.x + anx(185), startPosition.y + any(240) + any(20), anx(165), any(40), "HK_INPUT.TGA", "FONT_OLD_10_WHITE_HI.TGA", Input.Text, Align.Left, "2h", 2));
        bucket.internals.append(GUI.Input(startPosition.x + anx(20), startPosition.y + any(320) + any(20), anx(165), any(40), "HK_INPUT.TGA", "FONT_OLD_10_WHITE_HI.TGA", Input.Text, Align.Left, "�uk", 2));
        bucket.internals.append(GUI.Input(startPosition.x + anx(185), startPosition.y + any(320) + any(20), anx(165), any(40), "HK_INPUT.TGA", "FONT_OLD_10_WHITE_HI.TGA", Input.Text, Align.Left, "Kusza", 2));

        bucket.internals[4].setText(workingObject.weapon[0].tostring());
        bucket.internals[5].setText(workingObject.weapon[1].tostring());
        bucket.internals[6].setText(workingObject.weapon[2].tostring());
        bucket.internals[7].setText(workingObject.weapon[3].tostring());

        bucket.internals.append(GUI.Input(startPosition.x + anx(20), startPosition.y + any(400) + any(20), anx(165), any(40), "HK_INPUT.TGA", "FONT_OLD_10_WHITE_HI.TGA", Input.Text, Align.Left, "-1", 2));
        bucket.internals.append(GUI.Input(startPosition.x + anx(185), startPosition.y + any(400) + any(20), anx(165), any(40), "HK_INPUT.TGA", "FONT_OLD_10_WHITE_HI.TGA", Input.Text, Align.Left, "-1", 2));
        bucket.internals.append(GUI.Input(startPosition.x + anx(20), startPosition.y + any(480) + any(20), anx(165), any(40), "HK_INPUT.TGA", "FONT_OLD_10_WHITE_HI.TGA", Input.Text, Align.Left, "-1", 2));
        bucket.internals.append(GUI.Input(startPosition.x + anx(185), startPosition.y + any(480) + any(20), anx(165), any(40), "HK_INPUT.TGA", "FONT_OLD_10_WHITE_HI.TGA", Input.Text, Align.Left, "-1", 2));
        bucket.internals.append(GUI.Input(startPosition.x + anx(20), startPosition.y + any(560) + any(20), anx(165), any(40), "HK_INPUT.TGA", "FONT_OLD_10_WHITE_HI.TGA", Input.Text, Align.Left, "-1", 2));
        bucket.internals.append(GUI.Input(startPosition.x + anx(185), startPosition.y + any(560) + any(20), anx(165), any(40), "HK_INPUT.TGA", "FONT_OLD_10_WHITE_HI.TGA", Input.Text, Align.Left, "-1", 2));

        if(workingObject.melee == -1) bucket.internals[8].setText(""); else bucket.internals[8].setText(Items.name(workingObject.melee));
        if(workingObject.ranged == -1) bucket.internals[9].setText(""); else bucket.internals[9].setText(Items.name(workingObject.ranged));
        if(workingObject.magic == -1) bucket.internals[10].setText(""); else bucket.internals[10].setText(Items.name(workingObject.magic));
        if(workingObject.armor == -1) bucket.internals[11].setText(""); else bucket.internals[11].setText(Items.name(workingObject.armor));
        if(workingObject.helmet == -1) bucket.internals[12].setText(""); else bucket.internals[12].setText(Items.name(workingObject.helmet));
        if(workingObject.shield == -1) bucket.internals[13].setText(""); else bucket.internals[13].setText(Items.name(workingObject.shield));

        bucket.internals.append(GUI.Draw(startPosition.x + anx(30),startPosition.y + any(60), "Si�a"));
        bucket.internals.append(GUI.Draw(startPosition.x + anx(195),startPosition.y + any(60), "Zr�czno��"));
        bucket.internals.append(GUI.Draw(startPosition.x + anx(30),startPosition.y + any(140), "�ycie"));
        bucket.internals.append(GUI.Draw(startPosition.x + anx(195),startPosition.y + any(140), "Maks. �ycie"));
        bucket.internals.append(GUI.Draw(startPosition.x + anx(30),startPosition.y + any(220), "Jednor�czna"));
        bucket.internals.append(GUI.Draw(startPosition.x + anx(195),startPosition.y + any(220), "Dwur�czna"));
        bucket.internals.append(GUI.Draw(startPosition.x + anx(30),startPosition.y + any(300), "�uki"));
        bucket.internals.append(GUI.Draw(startPosition.x + anx(195),startPosition.y + any(300), "Kusze"));
        bucket.internals.append(GUI.Draw(startPosition.x + anx(30),startPosition.y + any(380), "Bia�a"));
        bucket.internals.append(GUI.Draw(startPosition.x + anx(195),startPosition.y + any(380), "Dystansowa"));
        bucket.internals.append(GUI.Draw(startPosition.x + anx(30),startPosition.y + any(460), "Magia"));
        bucket.internals.append(GUI.Draw(startPosition.x + anx(195),startPosition.y + any(460), "Zbroja"));
        bucket.internals.append(GUI.Draw(startPosition.x + anx(30),startPosition.y + any(540), "He�m"));
        bucket.internals.append(GUI.Draw(startPosition.x + anx(195),startPosition.y + any(540), "Tarcza"));

        foreach(internal in bucket.internals) {
            internal.setVisible(true);
        }
    }

    updateStatisticPage = function() {
        local element = BotsBuilder.bucketBot != null ? BotsBuilder.bucketBot : getBot(previewId).element;
        workingObject.str = bucket.internals[0].getText().tointeger();
        workingObject.dex = bucket.internals[1].getText().tointeger();
        workingObject.hp = bucket.internals[2].getText().tointeger();
        workingObject.hpMax = bucket.internals[3].getText().tointeger();
        workingObject.weapon[0] = bucket.internals[4].getText().tointeger();
        workingObject.weapon[1] = bucket.internals[5].getText().tointeger();
        workingObject.weapon[2] = bucket.internals[6].getText().tointeger();
        workingObject.weapon[3] = bucket.internals[7].getText().tointeger();
        workingObject.melee = Items.id(bucket.internals[8].getText());
        workingObject.ranged = Items.id(bucket.internals[9].getText());
        workingObject.magic = Items.id(bucket.internals[10].getText());
        workingObject.armor = Items.id(bucket.internals[11].getText());
        workingObject.helmet = Items.id(bucket.internals[12].getText());
        workingObject.shield = Items.id(bucket.internals[13].getText());

        setPlayerStrength(element, workingObject.str);
        setPlayerDexterity(element, workingObject.dex);
        setPlayerHealth(element, workingObject.hp);
        setPlayerMaxHealth(element, workingObject.hpMax);
        setPlayerSkillWeapon(element, 0, workingObject.weapon[0]);
        setPlayerSkillWeapon(element, 1, workingObject.weapon[1]);
        setPlayerSkillWeapon(element, 2, workingObject.weapon[2]);
        setPlayerSkillWeapon(element, 3, workingObject.weapon[3]);
        equipItem(element, workingObject.melee);
        equipItem(element, workingObject.ranged);
        equipItem(element, workingObject.magic);
        equipItem(element, workingObject.armor);
        equipItem(element, workingObject.helmet);
        equipItem(element, workingObject.shield);
    }

    save = function() {
        if(bucketBot != null)
            AdminPacket().createBot(workingObject);
        else
            AdminPacket().updateBot(previewId, workingObject);

        bucket.previewWindow.setVisible(false);
        destroyInternals();
        hideCreate();
    }

    remove = function() {
        if(bucketBot != null)
            return;

        AdminPacket().removeBot(previewId);
    }

    updateSwitch = function() {
        if(1 == activeTab)
            updateStatisticPage();
        else if(2 == activeTab)
            updateRelationPage();
        else
            updateSettingsPage();
    }

    onSwitch = function(tab) {
        destroyInternals();

        activeTab = tab;
        if(1 == activeTab)
            showStatisticPage();
        else if(2 == activeTab)
            showRelationPage();
        else
            showSettingsPage();
    }

    onRender = function() {
        if(bucketBot == null)
            return;

        local val = 1;

        if(isKeyPressed(KEY_LSHIFT))
            val = 10;

        local botPos = clone workingObject.position;
        local botAngle = workingObject.angle;
        if(isKeyPressed(KEY_LEFT))
            botPos.x = botPos.x - val;
        if(isKeyPressed(KEY_RIGHT))
            botPos.x = botPos.x + val;
        if(isKeyPressed(KEY_UP))
            botPos.z = botPos.z - val;
        if(isKeyPressed(KEY_DOWN))
            botPos.z = botPos.z + val;
        if(isKeyPressed(KEY_Q))
            botPos.y = botPos.y - val;
        if(isKeyPressed(KEY_E))
            botPos.y = botPos.y + val;
        if(isKeyPressed(KEY_Z)) {
            botAngle = botAngle - val;
            if(botAngle < 0)
                botAngle = 0;
        }
        if(isKeyPressed(KEY_X)) {
            botAngle = botAngle + val;
            if(botAngle > 360)
                botAngle = 360;
        }
        setPlayerPosition(bucketBot, botPos.x, botPos.y, botPos.z);
        setPlayerAngle(bucketBot, botAngle);
        workingObject.position = clone botPos;
        workingObject.angle = botAngle;
        Camera.setBeforePosFromTop(400, botPos.x, botPos.y, botPos.z);
        setPlayerPosition(heroId, botPos.x + 200, botPos.y, botPos.z);
        if(activeTab != 0)
            return;

        bucket.internals[5].setText(workingObject.position.x.tostring());
        bucket.internals[6].setText(workingObject.position.y.tostring());
        bucket.internals[7].setText(workingObject.position.z.tostring());
        bucket.internals[8].setText(workingObject.angle.tostring());
    }
}

Bind.addKey(KEY_ESCAPE, function() { BotsBuilder.hide(); }, PlayerGUI.ServerBuilderBots)

addEventHandler("onRender", function() {
    if(ActiveGui == PlayerGUI.ServerBuilderBots)
        BotsBuilder.onRender();
})

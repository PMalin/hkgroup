
MinesBuilder <- {
    bucket = null,
    bucketIndex = null,

    startPosition = null,
    bucketVob = null,
    activeKeys = [],

    freeCameraMode = false,
    freeCameraBucket = [],

    prepareWindow = function() {
        local result = {};

        result.window <- GUI.Window(anx(Resolution.x - 400), any(Resolution.y/2 - 400), anx(350), any(0), "HK_BOX.TGA");
        result.list <- GUI.Window(anx(50), any(Resolution.y/2 - 400), anx(600), any(800), "HK_BOX.TGA");

        result.scrollbarList <- GUI.ScrollBar(anx(650), any(50), anx(30), any(700), "HK_SCROLL.TGA", "HK_BUTTON.TGA", "HK_U.TGA", "HK_O.TGA", Orientation.Vertical, result.list);
        result.listContent <- [];

        result.bindForScrollbar <- Bind.onChange(result.scrollbarList, function(value) {
            MinesBuilder.refreshList();
        }, PlayerGUI.ServerBuilderMines);

        result.addButton <- GUI.Button(anx(Resolution.x/2 - 200), any(Resolution.y - 100), anx(180), any(60), "HK_BUTTON.TGA", "Dodaj nowy");
        result.removeButton <- GUI.Button(anx(Resolution.x/2 + 20), any(Resolution.y - 100), anx(180), any(60), "HK_BUTTON.TGA", "Usu�");

        result.saveButton <- GUI.Button(anx(15), any(690), anx(330), any(60), "HK_BUTTON.TGA", "Zapisz", result.window);

        result.addButton.bind(EventType.Click, function(element) {
            MinesBuilder.toggleAddWindow();
        });

        result.windowWithSelector <- GUI.Window(0, 0, 8200, 8200, "BLACK.TGA");
        result.sliderWithSelector <- GUI.ScrollBar(anx(Resolution.x - 100), any(50), anx(30), any(Resolution.y - 100), "HK_SCROLL.TGA", "HK_BUTTON.TGA", "HK_U.TGA", "HK_O.TGA", Orientation.Vertical, result.windowWithSelector);

        result.bucketWithSelectors <- [];

        for(local y = 0; y < 8; y ++) {
            for(local x = 0; x < 8; x ++) {
                result.bucketWithSelectors.append({
                    button = GUI.Button(50 + 900 * x, 50 + 900 * y, 850, 850, "WHITE.TGA", "", result.windowWithSelector),
                    render = ItemRender(50 + 900 * x, 50 + 900 * y, 850, 850, "")
                })
            }
        }

        result.saveButton.bind(EventType.Click, function(element) {
            local vob = MinesBuilder.bucketVob;
            local position = MinesBuilder.bucketVob.getPosition();
            local rotation = MinesBuilder.bucketVob.getRotation();

            local packet = Packet();
            packet.writeUInt8(PacketReceivers);
            packet.writeUInt8(Packets.Mine);
            packet.writeUInt8(MinePackets.Create);
            packet.writeString(MinesBuilder.bucketIndex);
            packet.writeFloat(position.x);
            packet.writeFloat(position.y);
            packet.writeFloat(position.z);
            packet.writeFloat(rotation.x);
            packet.writeFloat(rotation.y);
            packet.writeFloat(rotation.z);
            packet.send(RELIABLE);

            MinesBuilder.hideAddWindow();
        });

        result.removeButton.bind(EventType.Click, function(element) {
            local packet = Packet();
            packet.writeUInt8(PacketReceivers);
            packet.writeUInt8(Packets.Mine);
            packet.writeUInt8(MinePackets.Delete);
            packet.writeInt16(element.attribute);
            packet.send(RELIABLE);
        });

        return result;
    }

    show = function() {
        if(adminPlayer.role == PlayerRole.User)
            return;

        if(bucket == null)
            bucket = prepareWindow();

        bucket.window.setVisible(false);
        bucket.list.setVisible(true);
        bucket.addButton.setVisible(true);

        startPosition = getPlayerPosition(heroId);

        refreshList();

        BaseGUI.show();
        ActiveGui = PlayerGUI.ServerBuilderMines;
    }

    hide = function() {
        if(adminPlayer.role == PlayerRole.User)
            return;

        bucket.window.setVisible(false);
        bucket.list.setVisible(false);
        bucket.addButton.setVisible(false);
        bucket.windowWithSelector.setVisible(false);
        bucket.removeButton.setVisible(false);

        setPlayerPosition(heroId, startPosition.x, startPosition.y, startPosition.z);
        foreach(index, item in bucket.bucketWithSelectors)
            item.render.visible = false;

        foreach(item in bucket.listContent)
            item.remove();

        bucket.listContent.clear();

        bucketVob = null;
        disableFreeCameraMode();
        freeCameraBucket = [];

        BaseGUI.hide();
        ActiveGui = null;
    }

    refreshList = function() {
        foreach(item in bucket.listContent)
            item.remove();

        bucket.listContent.clear();

        local visibleVobs = 0;
        local _index = -1;
        local vobs = MineController.mines;
        local value = bucket.scrollbarList.getValue();
        value = value * 3;

        foreach(vob in vobs) {
            _index = _index + 1;

            if(_index < value)
                continue;

            if(visibleVobs >= 12)
                break;

            bucket.listContent.push(MinesBuilderObject(vob, visibleVobs));
            visibleVobs = visibleVobs + 1;
        }

        foreach(vobItem in bucket.listContent)
            vobItem.setVisible(true);
    }

    preview = function(id) {
        disableFreeCameraMode();

        local mine = MineController.mines[id];
        bucket.removeButton.setVisible(true);
        bucket.removeButton.attribute = id;
        _Camera.enableMovement(true)
        _Camera.modeChangeEnabled = false;
        _Camera.setTargetVob(mine.vob);

        if(bucket.window.m_visible)
            hideAddWindow();
    }

    toggleSelectorWindow = function() {
        bucket.windowWithSelector.m_visible ? hideSelectorWindow() : showSelectorWindow();
    }

    showSelectorWindow = function() {
        bucket.windowWithSelector.setVisible(true);
        changeSelectorWindow();
    }

    changeSelectorWindow = function() {
        local value = bucket.sliderWithSelector.getValue() * 16;

        foreach(index, item in bucket.bucketWithSelectors)
        {
            item.render.visible = true;
            item.render.top();

            if(index + value >= Config["MineSystem"].len())
                item.render.visual = "";
            else {
                local ind = -1;
                foreach(_index, mineObj in Config["MineSystem"]) {
                    ind = ind + 1;
                    if(index + value == ind) {
                        item.render.visual = mineObj.vob;
                        item.button.attribute = _index;
                    }
                }
            }
        }
    }

    checkObjIsSelector = function(objReffer) {
        local value = bucket.sliderWithSelector.getValue() * 16;
        foreach(index, item in bucket.bucketWithSelectors)
        {
            if(item.button == objReffer)
            {
                if(index + value < Config["MineSystem"].len()) {
                    local ind = -1;
                    foreach(_index, mineObj in Config["MineSystem"]) {
                        ind = ind + 1;
                        if(index + value == ind) {
                            bucketVob.visual = mineObj.vob;
                        }
                    }
                }
                MinesBuilder.bucketIndex = objReffer.attribute;

                _Camera.enableMovement(true)
                _Camera.modeChangeEnabled = false;
                _Camera.setTargetVob(bucketVob);
                hideSelectorWindow();
                return;
            }
        }

        foreach(item in bucket.listContent) {
            if(item.background == objReffer) {
                preview(item.background.attribute);
                return;
            }
        }
    }

    hideSelectorWindow = function() {
        foreach(index, item in bucket.bucketWithSelectors)
            item.render.visible = false;

        bucket.windowWithSelector.setVisible(false);
    }

    toggleAddWindow = function() {
        bucket.window.m_visible ? hideAddWindow() : showAddWindow();
    }

    showAddWindow = function() {
        disableFreeCameraMode();
        bucket.window.setVisible(true);
        bucket.removeButton.setVisible(false);
        bucketIndex = null;
        foreach(ind, _ in Config["MineSystem"]) {
            bucketIndex = ind;
            break;
        }

        local pos = _Camera.getPosition()
        bucketVob = Vob(Config["MineSystem"][bucketIndex].vob);
        bucketVob.addToWorld();
        bucketVob.setPosition(pos.x,pos.y,pos.z)
        _Camera.enableMovement(true)
        _Camera.modeChangeEnabled = false;
        _Camera.setTargetVob(bucketVob);
    }

    hideAddWindow = function() {
        bucket.window.setVisible(false);
        bucketVob = null
    },

    enableFreeCameraMode = function() {
        enableFreeCam(true);
        setCursorInFreeCamera("HK_CURSOR.TGA");
        freeCameraMode = true;
    },

    disableFreeCameraMode = function() {
        enableFreeCam(false);
        freeCameraMode = false;
        setFreeze(true);
        Camera.setFreeze(true);
        setCursorInFreeCamera("HK_CURSOR.TGA");
        setCursorTxt("HK_CURSOR.TGA");
        freeCameraBucket = [];
        BaseGUI.hide();
        BaseGUI.show();
    }

    onKey = function(key)
    {
        if(bucketVob == null) {
            if(key == KEY_B)
                freeCameraMode ? disableFreeCameraMode() : enableFreeCameraMode();
            return;
        }

        switch(key)
        {
            case KEY_NUMPAD5: case KEY_NUMPAD8: case KEY_NUMPAD4: case KEY_NUMPAD6: case KEY_NUMPAD7: case KEY_NUMPAD9: case KEY_E: case KEY_Q: case KEY_DOWN: case KEY_UP: case KEY_LEFT: case KEY_RIGHT: case KEY_A: case KEY_D: case KEY_W: case KEY_S:
                activeKeys.append(key);
            break;
            case KEY_C:
                toggleSelectorWindow();
            break;
        }
    }

    onMouseClick = function(buttonId)
    {
        if(freeCameraMode == false)
            return;

        local heroPos = getPlayerPosition(heroId);
        if(buttonId == MOUSE_RMB) {
            local cursorPosition = getCursorPositionPx();
            foreach(texture in freeCameraBucket) {
                local position = texture.obj.getPositionPx();
                local size = texture.obj.getSizePx();
                local isIn = (cursorPosition.x >= position.x && cursorPosition.x <= position.x + size.width && cursorPosition.y >= position.y && cursorPosition.y <= position.y + size.height);

                if(isIn) {
                    preview(texture.id);
                    return;
                }
            }
        }
    }

    onRender = function() {
        if(bucketVob == null) {
            if(freeCameraMode) {
                freeCameraBucket.clear();

                local heroPos = _Camera.getPosition();
                local cursorPosition = getCursorPositionPx();

                foreach(mine in MineController.mines) {
                    local vob = mine.vob;
                    local projection = _Camera.project(vob.position.x, vob.position.y, vob.position.z);
                    if(projection == null)
                        continue;

                    local distance = getPositionDifference(heroPos, vob.position);
                    if(distance > 3000)
                        continue;

                    local axis = 300 - abs(distance/10);
                    local range = {x = anx(projection.x) - axis, y = any(projection.y) - axis, width = axis * 2, height = axis * 2};

                    freeCameraBucket.push({obj = Texture(range.x, range.y, range.width, range.height, "WHITE.TGA"), id = vob.id});
                }

                foreach(draw in freeCameraBucket) {
                    draw.obj.setColor(255, 0, 0);
                    draw.obj.alpha = 50;
                    draw.obj.visible = true;
                }
            }
            return;
        }

        local speed = 1;

        if(isKeyPressed(KEY_LSHIFT))
            speed = 10;

        foreach(index, key in activeKeys)
        {
            if(!isKeyPressed(key))
            {
                activeKeys.remove(index);
                continue;
            }

            local position = bucketVob.getPosition();
            local rotation = bucketVob.getRotation();

            switch(key)
            {
                case KEY_DOWN: case KEY_S:
                    position.x = position.x - (sin(rotation.y * 3.14 / 180.0) * speed);
                    position.z = position.z - (cos(rotation.y * 3.14 / 180.0) * speed);
                break;
                case KEY_UP: case KEY_W:
                    position.x = position.x + (sin(rotation.y * 3.14 / 180.0) * speed);
                    position.z = position.z + (cos(rotation.y * 3.14 / 180.0) * speed);
                break;
                case KEY_LEFT: case KEY_A:
                    position.x = position.x + (sin(((rotation.y - 90) - floor(rotation.y / 360) * 360) * 3.14 / 180.0) * speed);
                    position.z = position.z + (cos(((rotation.y - 90) - floor(rotation.y / 360) * 360) * 3.14 / 180.0) * speed);
                break;
                case KEY_RIGHT: case KEY_D:
                    position.x = position.x + (sin(((rotation.y + 90) - floor(rotation.y / 360) * 360) * 3.14 / 180.0) * speed);
                    position.z = position.z + (cos(((rotation.y + 90) - floor(rotation.y / 360) * 360) * 3.14 / 180.0) * speed);
                break;
                case KEY_Q:
                    position.y = position.y + speed;
                break;
                case KEY_E:
                    position.y = position.y - speed;
                break;
                case KEY_NUMPAD4:
                    rotation.y = rotation.y + speed;
                break;
                case KEY_NUMPAD6:
                    rotation.y = rotation.y - speed;
                break;
                case KEY_NUMPAD5:
                    rotation.x = rotation.x + speed;
                break;
                case KEY_NUMPAD8:
                    rotation.x = rotation.x - speed;
                break;
                case KEY_NUMPAD7:
                    rotation.z = rotation.z + speed;
                break;
                case KEY_NUMPAD9:
                    rotation.z = rotation.z - speed;
                break;
            }

            bucketVob.setPosition(position.x, position.y, position.z);
            bucketVob.setRotation(rotation.x, rotation.y, rotation.z);
        }
    }
}

Bind.addKey(KEY_ESCAPE, function() { MinesBuilder.hide(); }, PlayerGUI.ServerBuilderMines)
addEventHandler("onKey", function(key) {
    if(ActiveGui == PlayerGUI.ServerBuilderMines)
        MinesBuilder.onKey(key);
})
addEventHandler("onRender", function() {
    if(ActiveGui == PlayerGUI.ServerBuilderMines)
        MinesBuilder.onRender();
})
addEventHandler("onMineAdd", function(vobId) {
    if(ActiveGui == PlayerGUI.ServerBuilderMines)
        MinesBuilder.refreshList();
})
addEventHandler("onMineRemove", function(vobId) {
    if(ActiveGui == PlayerGUI.ServerBuilderMines)
        MinesBuilder.refreshList();
})
addEventHandler("GUI.onChange", function(obj) {
    if(ActiveGui == PlayerGUI.ServerBuilderMines)
        if(obj == MinesBuilder.bucket.sliderWithSelector)
            MinesBuilder.changeSelectorWindow();
});
addEventHandler("GUI.onClick", function(obj) {
    if(ActiveGui == PlayerGUI.ServerBuilderMines)
        MinesBuilder.checkObjIsSelector(obj);
});
addEventHandler("onMouseClick", function(button) {
    if(ActiveGui == PlayerGUI.ServerBuilderMines)
        MinesBuilder.onMouseClick(button);
});


class MinesBuilderObject {
    index = null
    background = null;
    render = null;
    name = null;
    author = null;

    constructor(object, index) {
        local pos = {x = 0, y = 0}
        for(local i = 0; i < index; i ++) {
            pos.x = pos.x + 1;
            if(pos.x >= 3) {
                pos.x = 0
                pos.y = pos.y + 1;
            }
        }

        background = GUI.Button(anx(50) + anx(pos.x * 200), any(Resolution.y/2 - 400) + any(200 * pos.y), anx(200), any(200), "HK_BUTTON.TGA", "");
        background.attribute = object.id
        local backgroundPosition = background.getPosition();
        render = ItemRender(backgroundPosition.x + anx(25), backgroundPosition.y + any(10), anx(150), any(150), "");
        render.visual = object.vob.visual;
        name = Draw(backgroundPosition.x + 20, backgroundPosition.y + any(150), object.id + "-"+object.vob.visual);
        if(name.text.len() > 15) {
            name.text = name.text.slice(0, 15) + "...";
        }

        name.font = "FONT_OLD_20_WHITE_HI.TGA";
        name.setScale(0.5, 0.5);
        name.setColor(255, 248, 196);

        author = Draw(backgroundPosition.x + 20, backgroundPosition.y + any(175), object.scheme);
        if(author.text.len() > 15) {
            author.text = author.text.slice(0, 15) + "...";
        }

        author.font = "FONT_OLD_20_WHITE_HI.TGA";
        author.setScale(0.4, 0.4);
        author.setColor(255, 248, 196);
    }

    function setVisible(value) {
        background.setVisible(value);
        render.visible = value;
        name.visible = value;
        author.visible = value;
    }

    function remove() {
        background.setVisible(false);
        background.destroy();
        name = null;
        render = null;
        author = null;
    }
}
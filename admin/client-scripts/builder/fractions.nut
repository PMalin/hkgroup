
ServerBuilderFractions <- {
    bucket = null,
    openedFractionId = -1,
    members = {},

    prepareWindow = function() {
        local result = {};

        result.list <- MyGUI.SelectList(anx(40), any(Resolution.y/2 - 300), anx(350), any(600));
        result.list.onClick = function(id) {
            ServerBuilderFractions.open(id);
        }

        result.userList <- MyGUI.SelectList(anx(Resolution.x - 400), any(Resolution.y/2 - 300), anx(350), any(600));
        result.userList.onClick = function(id) {
            ServerBuilderFractions.fastActions(id);
        }

        result.errorButton <- GUI.Button(anx(Resolution.x/2 - 200), any(Resolution.y - 200), anx(400), any(60), "HK_BUTTON.TGA", "");
        result.addButton <- GUI.Button(anx(Resolution.x - 400), any(Resolution.y/2 + 320), anx(350), any(60), "HK_BUTTON.TGA", "Dodaj");
        result.addButton.bind(EventType.Click, function(element) {
            ServerBuilderFractions.createMember();
        });

        result.external <- [];

        return result;
    }

    show = function() {
        if(adminPlayer.role == PlayerRole.User)
            return;

        if(bucket == null)
            bucket = prepareWindow();

        bucket.list.clear();

        foreach(fraction in FractionController.fractions)
            bucket.list.addOption(fraction.id, fraction.name);

        bucket.userList.clear();

        bucket.list.setVisible(true);
        bucket.userList.setVisible(false);
        bucket.addButton.setVisible(false);
        bucket.errorButton.setVisible(false);

        openedFractionId = -1
        members = {}

        BaseGUI.show();
        ActiveGui = PlayerGUI.ServerBuilderFractions;
    }

    hide = function() {
        if(adminPlayer.role == PlayerRole.User)
            return;

        bucket.userList.setVisible(false);
        bucket.list.setVisible(false);
        bucket.addButton.setVisible(false);
        bucket.errorButton.setVisible(false);

        foreach(external in bucket.external) {
            external.setVisible(false);
            external.destroy();
        }

        bucket.external.clear();

        BaseGUI.hide();
        ActiveGui = null;
    }

    open = function(fractionId) {
        openedFractionId = fractionId;
        bucket.userList.clear();
        bucket.userList.setVisible(true);
        bucket.addButton.setVisible(true);
        members = {}

        foreach(external in bucket.external) {
            external.setVisible(false);
            external.destroy();
        }

        bucket.external.clear();
        AdminPacket().getFractionMembers(fractionId);
    }

    fastActions = function(playerId) {
        foreach(external in bucket.external) {
            external.setVisible(false);
            external.destroy();
        }

        bucket.external.clear();
        bucket.external.push(GUI.Texture(anx(Resolution.x/2 - 200), any(Resolution.y/2 - 100), anx(400), any(300), "HK_BOX.TGA"));

        local ranksSelector = [];
        foreach(rank in getFraction(openedFractionId).ranks)
            ranksSelector.push({id = rank.id, name = rank.name});

        bucket.external.push(GUI.Button(anx(Resolution.x/2 - 190), any(Resolution.y/2 - 80), anx(380), any(60), "HK_BUTTON.TGA", members[playerId].name));
        bucket.external.push(MyGUI.DropDown(anx(Resolution.x/2 - 190), any(Resolution.y/2), anx(380), any(60), "Rola", ranksSelector, "Wybierz rang�..", members[playerId].rank));
        bucket.external.push(GUI.Button(anx(Resolution.x/2 - 190), any(Resolution.y/2 + 120), anx(180), any(60), "HK_BOX.TGA", "Usu�"));
        local deleteButton = bucket.external[bucket.external.len() - 1];
        deleteButton.attribute = playerId;
        deleteButton.bind(EventType.Click, function(element) {
            ServerBuilderFractions.removeMember(element.attribute);
        });

        bucket.external.push(GUI.Button(anx(Resolution.x/2 + 10), any(Resolution.y/2 + 120), anx(180), any(60), "HK_BOX.TGA", "Zapisz"));
        local saveButton = bucket.external[bucket.external.len() - 1];
        saveButton.attribute = playerId;
        saveButton.bind(EventType.Click, function(element) {
            ServerBuilderFractions.saveMember(element.attribute);
        });

        foreach(external in bucket.external)
            external.setVisible(true);
    }

    addMember = function(playerId, roleId, name) {
        members[playerId] <- {rank = roleId, name = name};
        bucket.userList.addOption(playerId, name + " - " + getFraction(openedFractionId).ranks[roleId].name);
    }

    createMember = function() {
        foreach(external in bucket.external) {
            external.setVisible(false);
            external.destroy();
        }

        bucket.external.clear();
        bucket.external.push(GUI.Texture(anx(Resolution.x/2 - 200), any(Resolution.y/2 - 100), anx(400), any(300), "HK_BOX.TGA"));

        local usersSelector = [];
        for(local i = 0; i < getMaxSlots(); i ++)
            if(isPlayerCreated(i))
                usersSelector.push({id = i, name = getPlayerAdminStatistics(i).characterName});

        local ranksSelector = [];
        foreach(rank in getFraction(openedFractionId).ranks)
            ranksSelector.push({id = rank.id, name = rank.name});

        bucket.external.push(MyGUI.DropDown(anx(Resolution.x/2 - 190), any(Resolution.y/2 - 80), anx(380), any(60), "U�ytkownik", usersSelector, "Wybierz u�ytkownika.."));
        bucket.external.push(MyGUI.DropDown(anx(Resolution.x/2 - 190), any(Resolution.y/2), anx(380), any(60), "Rola", ranksSelector, "Wybierz rang�.."));

        bucket.external.push(GUI.Button(anx(Resolution.x/2 + 10), any(Resolution.y/2 + 120), anx(380), any(60), "HK_BOX.TGA", "Dodaj"));
        local saveButton = bucket.external[bucket.external.len() - 1];
        saveButton.bind(EventType.Click, function(element) {
            local _ext = ServerBuilderFractions.bucket.external;
            local playerId = _ext[1].getValue();
            local rankId = _ext[2].getValue();
            if(playerId == null)
                return;

            foreach(external in ServerBuilderFractions.bucket.external) {
                external.setVisible(false);
                external.destroy();
            }

            ServerBuilderFractions.bucket.external.clear();

            AdminPacket().addFractionMember(ServerBuilderFractions.openedFractionId, playerId, rankId);
            ServerBuilderFractions.bucket.errorButton.setText("Dodano gracza do frakcji.");
        });

        foreach(external in bucket.external)
            external.setVisible(true);
    }

    removeMember = function(playerId) {
        members.rawdelete(playerId);
        bucket.userList.removeOption(playerId);
        bucket.errorButton.setVisible(true);
        bucket.errorButton.setText("Usuni�to gracza z frakcji.");

        foreach(external in bucket.external) {
            external.setVisible(false);
            external.destroy();
        }

        bucket.external.clear();
        AdminPacket().removeFractionMember(ServerBuilderFractions.openedFractionId, playerId);
    }

    saveMember = function(playerId) {
        local name = members[playerId].name;
        members.rawdelete(playerId);
        bucket.userList.removeOption(playerId);
        bucket.errorButton.setVisible(true);
        bucket.errorButton.setText("Zaaktualizowano gracza z frakcji.");
        local rankId = bucket.external[2].getValue();

        foreach(external in bucket.external) {
            external.setVisible(false);
            external.destroy();
        }

        members[playerId] <- {rank = rankId, name = name};
        bucket.userList.addOption(playerId, name + " - " + getFraction(ServerBuilderFractions.openedFractionId).ranks[rankId].name);

        bucket.external.clear();
        AdminPacket().updateFractionMember(ServerBuilderFractions.openedFractionId, playerId, rankId);
    }
}

Bind.addKey(KEY_ESCAPE, ServerBuilderFractions.hide.bindenv(ServerBuilderFractions), PlayerGUI.ServerBuilderFractions)

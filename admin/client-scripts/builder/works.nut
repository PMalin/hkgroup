
WorksBuilder <- {
    bucket = null,

    startPosition = null,
    bucketVob = null,
    typeObject = -1,
    activeKeys = [],

    prepareWindow = function() {
        local result = {};

        result.previewWindow <- GUI.Window(anx(Resolution.x - 400), any(Resolution.y/2 - 400), anx(350), any(780), "BLACK.TGA");
        result.list <- MyGUI.SelectList(anx(40), any(Resolution.y/2 - 300), anx(450), any(600))

        return result;
    }

    show = function() {
        if(adminPlayer.role == PlayerRole.User)
            return;

        if(bucket == null)
            bucket = prepareWindow();

        list.clear();
        bucket.previewWindow.setVisible(false);
        bucket.list.setVisible(true);

        startPosition = getPlayerPosition(heroId);

        bucket.list.onClick = function(id) {
            WorksBuilder.preview(id);
        }

        AdminPacket().turnOnInvisibility();

        BaseGUI.show();
        ActiveGui = PlayerGUI.ServerBuilderWork;
    }

    hide = function() {
        if(adminPlayer.role == PlayerRole.User)
            return;

        bucket.previewWindow.setVisible(false);
        bucket.list.setVisible(false);

        setPlayerPosition(heroId, startPosition.x, startPosition.y, startPosition.z);
        AdminPacket().turnOffInvisibility();

        BaseGUI.hide();
        ActiveGui = null;
    }
}
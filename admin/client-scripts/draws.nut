
local localDraws = [];
local enableLocalDraws = false;

function enableLocalDrawsSystem(value) {
    localDraws.clear();
    enableLocalDraws = value;
}

function getEnableLocalDrawsSystem() {
    return enableLocalDraws;
}

addEventHandler ("onRender", function () {
    if(enableLocalDraws == false)
        return;

    local objects = [];

    local heroPos = getPlayerPosition(heroId);
    foreach(vob in VobController.vobs) {
        local posDiff = getPositionDifference(heroPos, vob.position);
        if(posDiff < 2000) {
            local obj = {};
            obj.position <- vob.position;
            obj.name <- vob.id+ "| - " + vob.name + " " + vob.visual;
            obj.posDifference <- posDiff;
            objects.push(obj);
        }
    }

    localDraws.clear();

    foreach(obj in objects) {
        local draw = Draw(0, 0, obj.name);

        local project = _Camera.project(obj.position.x, obj.position.y + 109.2, obj.position.z);
        if(project == null)
            continue;

        draw.font = "FONT_OLD_20_WHITE_HI.TGA";
        local scaleI = 0.2 + 0.3 * (((2000.0 - obj.posDifference)/2000.0) * 1.0);

        draw.setScale(scaleI, scaleI);
        draw.setPosition(anx(project.x), any(project.y));

        draw.visible = true;
        localDraws.push(draw);
    }
});

class AdminPacket
{
    packet = null;

    constructor()
    {
        packet = ExtendedPacket(Packets.Admin);
    }

    function sendPlayerItems(playerId) {
        packet.writeUInt8(AdminPackets.GetItems);
        packet.writeUInt16(playerId);
        packet.send();
    }

    function getFractionMembers(fractionId) {
        packet.writeUInt8(AdminPackets.GetFractionMembers)
        packet.writeInt16(fractionId);
        packet.send();
    }

    function addFractionMember(fractionId, playerId, rankId) {
        packet.writeUInt8(AdminPackets.AddFractionMember)
        packet.writeInt16(fractionId);
        packet.writeInt16(playerId);
        packet.writeInt16(rankId);
        packet.send();
    }

    function removeFractionMember(fractionId, realPlayerId) {
        packet.writeUInt8(AdminPackets.RemoveFractionMember)
        packet.writeInt16(fractionId);
        packet.writeInt32(realPlayerId);
        packet.send();
    }

    function updateFractionMember(fractionId, realPlayerId, rankId) {
        packet.writeUInt8(AdminPackets.UpdateFractionMember)
        packet.writeInt16(fractionId);
        packet.writeInt32(realPlayerId);
        packet.writeInt16(rankId);
        packet.send();
    }

    function getAllAssets() {
        packet.writeUInt8(AdminPackets.GetAssets)
        packet.send();
    }

    function addAsset(assetName) {
        packet.writeUInt8(AdminPackets.AddAsset)
        packet.writeString(assetName)
        packet.send();
    }

    function deleteAsset(id) {
        packet.writeUInt8(AdminPackets.DeleteAsset)
        packet.writeInt16(id)
        packet.send();
    }

    function getAllCrafts() {
        packet.writeUInt8(AdminPackets.GetCrafts)
        packet.send();
    }

    function getCraft(craftId) {
        packet.writeUInt8(AdminPackets.GetCraft)
        packet.writeInt16(craftId);
        packet.send();
    }

    function getAllPlayers() {
        packet.writeUInt8(AdminPackets.GetPlayers)
        packet.send();
    }

    function getAllBots() {
        packet.writeUInt8(AdminPackets.GetBots)
        packet.send();
    }

    function removeBot(id) {
        packet.writeUInt8(AdminPackets.RemoveBot)
        packet.writeInt16(id);
        packet.send();
    }

    function updateBot(id, object) {
        packet.writeUInt8(AdminPackets.UpdateBot)
        packet.writeInt16(id);
        packet.writeString(object.group);
        packet.writeString(object.name);
        packet.writeString(object.instance);
        packet.writeBool(object.isTemporary),
        packet.writeBool(object.isSaved);
        packet.writeFloat(object.position.x);
        packet.writeFloat(object.position.y);
        packet.writeFloat(object.position.z);
        packet.writeInt16(object.angle);
        packet.writeInt16(object.str);
        packet.writeInt16(object.dex);
        packet.writeInt32(object.hp);
        packet.writeInt32(object.hpMax);
        packet.writeFloat(object.scale.x);
        packet.writeFloat(object.scale.y);
        packet.writeFloat(object.scale.z);
        packet.writeString(object.animation);
        packet.writeInt16(object.weapon[0]);
        packet.writeInt16(object.weapon[1]);
        packet.writeInt16(object.weapon[2]);
        packet.writeInt16(object.weapon[3]);
        packet.writeInt16(object.respawnTime);
        packet.writeInt16(object.melee);
        packet.writeInt16(object.armor);
        packet.writeInt16(object.ranged);
        packet.writeInt16(object.helmet);
        packet.writeInt16(object.shield);
        packet.writeInt16(object.magic);
        packet.writeString(object.bodyModel);
        packet.writeInt16(object.bodyTxt);
        packet.writeString(object.headModel);
        packet.writeInt16(object.headTxt);
        packet.writeInt16(object.effects.len());
        foreach(effect in object.effects) {
            packet.writeInt16(effect.effectId);
            packet.writeInt16(effect.refreshTime);
        }
        packet.send();
    }

    function createBot(object) {
        packet.writeUInt8(AdminPackets.CreateBot)
        packet.writeString(object.group);
        packet.writeString(object.name);
        packet.writeString(object.instance);
        packet.writeBool(object.isTemporary),
        packet.writeBool(object.isSaved);
        packet.writeFloat(object.position.x);
        packet.writeFloat(object.position.y);
        packet.writeFloat(object.position.z);
        packet.writeInt16(object.angle);
        packet.writeInt16(object.str);
        packet.writeInt16(object.dex);
        packet.writeInt32(object.hp);
        packet.writeInt32(object.hpMax);
        packet.writeFloat(object.scale.x);
        packet.writeFloat(object.scale.y);
        packet.writeFloat(object.scale.z);
        packet.writeString(object.animation);
        packet.writeInt16(object.weapon[0]);
        packet.writeInt16(object.weapon[1]);
        packet.writeInt16(object.weapon[2]);
        packet.writeInt16(object.weapon[3]);
        packet.writeInt16(object.respawnTime);
        packet.writeInt16(object.melee);
        packet.writeInt16(object.armor);
        packet.writeInt16(object.ranged);
        packet.writeInt16(object.helmet);
        packet.writeInt16(object.shield);
        packet.writeInt16(object.magic);
        packet.writeString(object.bodyModel);
        packet.writeInt16(object.bodyTxt);
        packet.writeString(object.headModel);
        packet.writeInt16(object.headTxt);
        packet.writeInt16(object.effects.len());
        foreach(effect in object.effects) {
            packet.writeInt16(effect.effectId);
            packet.writeInt16(effect.refreshTime);
        }
        packet.send();
    }

    function turnOnInvisibility() {
        packet.writeUInt8(AdminPackets.Inivisibility);
        packet.writeBool(true);
        packet.send();
    }

    function turnOffInvisibility() {
        packet.writeUInt8(AdminPackets.Inivisibility);
        packet.writeBool(false);
        packet.send();
    }

    function sendAnswer(questionIq, answerId) {
        packet.writeUInt8(AdminPackets.Question)
        packet.writeInt16(questionIq)
        packet.writeInt16(answerId)
        packet.send();
    }

    function updateItem(itemId, name, description, guid, amount, resistance) {
        packet.writeUInt8(AdminPackets.UpdateItem)
        packet.writeUInt32(itemId);
        packet.writeString(name);
        packet.writeString(description);
        packet.writeString(guid);
        packet.writeInt16(amount);
        packet.writeInt16(resistance);
        packet.send();
    }

    function deleteItem(itemId) {
        packet.writeUInt8(AdminPackets.DeleteItem)
        packet.writeUInt32(itemId);
        packet.send();
    }

    function saveToLog(str) {
        packet.writeUInt8(AdminPackets.SaveToLog)
        packet.writeInt16(1);
        packet.writeString(str);
        packet.send();
    }

    function saveToLogTable(tab) {
        packet.writeUInt8(AdminPackets.SaveToLog)
        packet.writeInt16(tab.len());
        foreach(str in tab)
            packet.writeString(str);

        packet.send();
    }
}

adminPlayer <- {};

adminPlayer.role <- PlayerRole.User;
adminPlayer.active <- false;

if(DEVELOPMENT)
    adminPlayer.active = true;

adminPlayer.setRole <- function(role) {
    if(adminPlayer.role == PlayerRole.Admin || adminPlayer.role == PlayerRole.Mod || adminPlayer.role == PlayerRole.Support || adminPlayer.role == PlayerRole.GameMaster) {
        Chat.removeContainer(ChatType.REPORT);
        Chat.removeContainer(ChatType.ADMIN);
    }
    adminPlayer.role = role;
    if(adminPlayer.role == PlayerRole.Admin || adminPlayer.role == PlayerRole.Mod || adminPlayer.role == PlayerRole.Support || adminPlayer.role == PlayerRole.GameMaster) {
        Chat.addContainer(ChatType.REPORT);
        Chat.addContainer(ChatType.ADMIN);
    }
}

adminPlayer.canJump <- function() {
    if(adminPlayer.active && (adminPlayer.role == PlayerRole.Admin || adminPlayer.role == PlayerRole.Mod || adminPlayer.role == PlayerRole.Support))
        return true;

    return false;
}

adminPlayer.canCommand <- function() {
    if(adminPlayer.active && (adminPlayer.role == PlayerRole.Admin || adminPlayer.role == PlayerRole.Mod || adminPlayer.role == PlayerRole.Support || adminPlayer.role == PlayerRole.GameMaster))
        return true;

    return false;
}
local bucket = [];

class VobLog
{
    playerId = -1;
    vobId = -1;

    static VobLogTable = "voblog";

    constructor(pid, vid)
    {
        playerId = pid;
        vobId = vid;
    }

    function log(logType,info = "")
    {
        local pos = getPlayerPosition(playerId);

        local dateObject = date();

        bucket.push(["'"+logType+"'",getAccount(playerId).rId,vobId,pos.x,pos.y,pos.z,"now()","'"+info+"'"]);
    }

    function onMinute()
    {
        local d = date();

        if(bucket.len() > 0){
            Query().insertInto(
                VobLogTable, 
                ["type", "playerId", "vobId","x", "y", "z","date","info"], 
                bucket
            ) .execute()

            bucket = [];
        }

        if(d.min = 30)
        {
            DB.queryGet("DELETE FROM "+VobLogTable+" WHERE date < now() - interval 5 DAY");
        }
    }
}

addEventHandler("onMinute", VobLog.onMinute.bindenv(VobLog))
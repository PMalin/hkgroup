
class ac.HealthModule extends ac.Module {
    function onTenSeconds() {
        local hp = getPlayerHealth(heroId);
        if(hp < 100)
            return;

        if(getPlayerInstance(heroId) != "PC_HERO")
            return;

        ac.Packet().health(hp);
    }
}

ac.Object <- {
    cache = null
    modules = null

    function onInit() {
        cache = {};
        modules = {};

        modules[AntyCheatModule.ItemsChecksum] <- ac.ItemsChecksumModule(AntyCheatModule.ItemsChecksum);
        modules[AntyCheatModule.StatisticsChecksum] <- ac.StatisticsChecksumModule(AntyCheatModule.StatisticsChecksum);
        modules[AntyCheatModule.Health] <- ac.HealthModule(AntyCheatModule.Health);
        modules[AntyCheatModule.Stamina] <- ac.StaminaModule(AntyCheatModule.Stamina);
        modules[AntyCheatModule.Teleportation] <- ac.TeleportationModule(AntyCheatModule.Teleportation);
    }

    function onSecond() {
        foreach(module in modules)
            module.onSecond();
    }

    function onTenSeconds() {
        foreach(module in modules)
            module.onTenSeconds();
    }
}

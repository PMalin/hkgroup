
class ac.Controller {
    static function onInit() {
        ac.Object.onInit();
    }

    static function onPacket(packet) {
        local typ = packet.readUInt8();

        switch(typ) {

        }
    }

    static function onTenSeconds() {
        ac.Object.onTenSeconds();
    }

    static function onSecond() {
        ac.Object.onSecond();
    }
}

addEventHandler("onInit", ac.Controller.onInit.bindenv(ac.Controller));
addEventHandler("onTenSeconds", ac.Controller.onTenSeconds.bindenv(ac.Controller));
addEventHandler("onSecond", ac.Controller.onSecond.bindenv(ac.Controller));
RegisterPacketReceiver(Packets.AntyCheat, ac.Controller.onPacket.bindenv(ac.Controller));
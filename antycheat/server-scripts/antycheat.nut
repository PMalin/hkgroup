
class ac.Object {
    playerId = -1
    cache = null;

    constructor(player) {
        playerId = player;
        cache = {};
    }

    function checkHealth(healthFromClient) {
        local hp = getPlayerHealth(playerId);
        if(hp != healthFromClient) {
            addPlayerLog(playerId, "[server:"+hp+", client: "+healthFromClient+"]Podejrzenie cheatu hp");
            //kick(playerId, "Ostrzeżenie: Brak synchronizacji z klientem.");
        }
    }

    function checkStamina(staminaFromClient) {
        local stamina = getPlayerStamina(playerId);
        if(stamina != staminaFromClient) {
            //addPlayerLog(playerId, "[server:"+stamina+", client: "+staminaFromClient+"]Podejrzenie cheatu staminy");
            //kick(playerId, "Ostrzeżenie: Brak synchronizacji z klientem.");
        }
    }

    function checkItems(amountOfItems) {
        local originalAmount = 0;

        local player = getPlayer(playerId);

        foreach(item in player.inventory.getItems())
            originalAmount += item.amount;

        cache[AntyCheatModule.ItemsChecksum] <- originalAmount;

        if(originalAmount != amountOfItems) {
            addPlayerLog(playerId, "[server:"+originalAmount+", client: "+amountOfItems+"]Podejrzenie cheatu itemów");
            kick(playerId, "Ostrzeżenie: Niepoprawna suma przedmiotów.");
        }
    }

    function flushItemsCache() {
        if(AntyCheatModule.ItemsChecksum in cache)
            cache.rawdelete(AntyCheatModule.ItemsChecksum)
    }
}
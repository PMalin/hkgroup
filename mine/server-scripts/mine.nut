
class Mine
{
    static MineTable = "mine";

    id = -1
    position = null
    rotation = -1

    scheme = -1
    growth = -1
    slots = 0

    constructor(_id)
    {
        id = _id
        scheme = 0

        position = {
            x = 0,
            y = 0,
            z = 0
        }

        rotation = {
            x = 0,
            y = 0,
            z = 0
        }
        
        growth = 0
        slots = 0
    }

    function onSecond() {
        if(slots >= getScheme().slots)
            return false;

        if(growth >= getScheme().refresh) {
            slots = slots + 1;
            growth = 0;
            update();
            return true;
        }

        growth = growth + 1;
        return false;
    }

    function update() {
        DB.queryGet("UPDATE "+Mine.MineTable+" SET slots = "+slots+" WHERE id = "+id);
    }

    function getScheme() {
        return Config["MineSystem"][scheme];
    }
}
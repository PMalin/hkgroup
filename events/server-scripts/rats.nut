
local config = Config["Rats"], slots = [], bots = {}, meetsOnGround = [];

for(local i = 0; i < config.slots; i++) {
    slots.push(config.cooldown + rand() % 200);
    bots[i] <- null;
}

RatsController <- {
    pushBotIntoSlot = function(index) {
        local randomSlotId = irand(config.positions.len() - 1);
        local pos = config.positions[randomSlotId];

        local bot = BotBase("Szczur mi�sny");

        bot.instance = "SCHEUNENRATTE_01";
        bot.group = "RATS";
        bot.setScale(0.25, 0.25, 0.25);

        bot.addComponent(BotAnimComponent());
        bot.addComponent(BotStreamComponent());
        bot.addComponent(BotSpawnComponent());
        bot.addComponent(BotWanderingComponent());
        bot.addComponent(BotFactionComponent());
        bot.addComponent(BotRoutineComponent());
        bot.addComponent(BotCombatComponent());

        bot.setPosition(pos[0],pos[1],pos[2]);
        bot.angle = rand() % 300
        bot.hpMax = 10;
        bot.hp = 10;
        bot.str = 5;
        bot.dex = 5;

        bot.isTemporary = true;

        bots[index] = bot;
    }

    check = function(botId) {
        local bot = bots[botId];
        foreach(index, meet in meetsOnGround) {
            if(bot.positionDifference(meet) < 200) {
                ItemsGround.destroy(meet.id);
                meetsOnGround.remove(index);
                bot.playAnimation("STOP");
                bot.playAnimation("T_EAT");
                check(botId);
                return;
            }
        }
    }

    onSecond = function() {
        if(slots.len() == 0)
            return;

        if(!config.active)
            return;

        foreach(_, slot in slots) {
            if(bots[_] != null) {
                if(bots[_].hp == 0) {
                    bots[_] = null;
                    slots[_] = config.cooldown = rand() % 300;
                }else
                    check(_);

                continue;
            }

            if(slot <= 0)
                pushBotIntoSlot(_);
            else
                slots[_] = slots[_] - 1;
        }
    }

    onPlayerLeftOnGround = function(item) {
        if(config.items.find(item.instance) == null)
            return;

        local pos = item.getPosition();
        meetsOnGround.push({x = pos.x, y = pos.y, z = pos.z, id = item.id});
    }

    onPlayerTakeFromGround = function(item) {
        foreach(index, meet in meetsOnGround) {
            if(meet.id == item.id) {
                meetsOnGround.remove(index);
                onPlayerTakeFromGround(item);
                return;
            }
        }
    }
}

addEventHandler ("onSecond", RatsController.onSecond.bindenv(RatsController));
addEventHandler ("onPlayerLeftOnGround", RatsController.onPlayerLeftOnGround.bindenv(RatsController));
addEventHandler ("onPlayerTakeFromGround", RatsController.onPlayerTakeFromGround.bindenv(RatsController));
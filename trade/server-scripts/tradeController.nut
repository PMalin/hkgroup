
class TradeController
{
    static trades = {};

    static function startTrade(playerId, tradeId) {
        trades[playerId] <- Trade(playerId, playerId, tradeId);
        trades[playerId].offerOne = item.Controller.createBucket(ItemRelationType.Trade, playerId);
        trades[playerId].offerTwo = item.Controller.createBucket(ItemRelationType.Trade, tradeId);

        trades[playerId].offerOne.open(playerId);
        trades[playerId].offerOne.open(tradeId);

        trades[playerId].offerTwo.open(playerId);
        trades[playerId].offerTwo.open(tradeId);

        trades[playerId].packetManager.start();
    }

    static function onBeforeDisconnectGame(playerId) {
        foreach(id, trade in trades) {
            if(trade.playerTwoId == playerId || trade.playerOneId == playerId) {
                trade.endTrade();
                trades.rawdelete(id);
            }
        }
    }

    static function success(tradeId) {
        if(!(tradeId in trades))
            return;

        local tradeObject = trades[tradeId];
        local continueTrade = true;

        if(continueTrade == true) {
            local itemsText = "";

            foreach(itemId in tradeObject.offerTwo.ids) {
                local _item = getItem(itemId);
                itemsText += _item.name+" ("+_item.amount+") ";
                getPlayer(tradeObject.playerOneId).inventory.addItem(itemId);
            }

            itemsText += " --Za-- ";

            foreach(itemId in tradeObject.offerOne.ids) {
                local _item = getItem(itemId);
                itemsText += _item.name+" ("+_item.amount+") ";
                getPlayer(tradeObject.playerTwoId).inventory.addItem(itemId);
            }

            addPlayerNotification(tradeObject.playerTwoId, "Handel zako�czony sukcesem.")
            addPlayerNotification(tradeObject.playerOneId, "Handel zako�czony sukcesem.")

            addPlayerLog(tradeObject.playerTwoId, "Zako�czy� handel z "+getPlayer(tradeObject.playerOneId).name + " " +itemsText);
            addPlayerLog(tradeObject.playerOneId, "Zako�czy� handel z "+getPlayer(tradeObject.playerTwoId).name + " " +itemsText);

            item.Controller.removeBucket(tradeObject.offerOne.id);
            item.Controller.removeBucket(tradeObject.offerTwo.id);

            tradeObject.packetManager.end();
        }else{
            tradeObject.endTrade();

            addPlayerNotification(tradeObject.playerTwoId, "Handel zako�czony pora�k�.")
            addPlayerNotification(tradeObject.playerOneId, "Handel zako�czony pora�k�.")
        }

        trades.rawdelete(tradeId);
    }

    static function onPacket(playerId, packet)
    {
        local typ = packet.readUInt8();

        switch(typ)
        {
            case TradePackets.Accept:
                local id = packet.readInt16();
                if(id in trades)
                    trades[id].acceptTrade();
            break;
            case TradePackets.End:
                local id = packet.readInt16();
                if(id in trades) {
                    trades[id].endTrade();
                    trades.rawdelete(id);
                }
            break;
        }
    }
}

RegisterPacketReceiver(Packets.Trade, TradeController.onPacket.bindenv(TradeController));


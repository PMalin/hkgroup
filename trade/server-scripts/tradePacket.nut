
class TradePacket
{
    packet = null;
    obj = null;

    constructor(_obj)
    {
        obj = _obj;
        packet = ExtendedPacket(Packets.Trade);
    }

    function reset() {
        packet.reset();
        packet.writeUInt8(PacketReceivers);
        packet.writeUInt8(Packets.Trade);
    }

    function start() {
        packet.writeUInt8(TradePackets.Create)
        packet.writeInt16(obj.id);
        packet.writeInt16(obj.playerOneId);
        packet.writeInt16(obj.playerTwoId);
        packet.writeUInt16(obj.offerOne.id);
        packet.writeUInt16(obj.offerTwo.id);
        packet.send(obj.playerTwoId);
        packet.send(obj.playerOneId);

        reset();
    }

    function acceptTrade() {
        packet.writeUInt8(TradePackets.Accept)
        packet.writeInt16(obj.id);
        packet.send(obj.playerTwoId);
        packet.send(obj.playerOneId);

        reset();
    }

    function end() {
        packet.writeUInt8(TradePackets.End)
        packet.writeInt16(obj.id);
        packet.send(obj.playerTwoId);
        packet.send(obj.playerOneId);

        reset();
    }

    function addToOffer(playerId, instance, amount) {
        packet.writeUInt8(TradePackets.AddItem)
        packet.writeInt16(obj.id);
        packet.writeString(instance);
        packet.writeInt16(amount);
        packet.send(playerId);

        reset();
    }

    function removeFromOffer(playerId, instance, amount) {
        packet.writeUInt8(TradePackets.RemoveItem)
        packet.writeInt16(obj.id);
        packet.writeString(instance);
        packet.writeInt16(amount);
        packet.send(playerId);

        reset();
    }
}
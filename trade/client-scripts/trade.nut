
class Trade
{
    id = -1;

    playerTwoId = -1;
    playerOneId = -1;

    offerOne = null
    offerTwo = null

    accepted = false;
    packetManager = null;
    timeout = null;

    constructor(identification, playerIdOne, playerIdTwo)
    {
        id = identification;

        playerTwoId = playerIdTwo;
        playerOneId = playerIdOne;

        offerOne = null;
        offerTwo = null;

        accepted = false;

        packetManager = TradePacket(this);
        timeout = getTickCount() + 10000;
    }

    function setUpInterface() {
        TradeInterface.show(this);
    }

    function acceptTrade() {
        accepted = true;
        TradeInterface.getAccepted();
    }
}

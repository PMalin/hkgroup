
Config <- {};

// General

Config["MinuteSeconds"] <- 5;

// Chat

Config["ChatAnimation"] <- true;
Config["ChatHistorySize"] <- 100;
Config["ChatDefaultLines"] <- 10;
Config["ChatScreen"] <- 70;
Config["ChatLineMultiplier"] <- 1.0;

Config["ChatLabel"] <- {}
Config["ChatLabel"][ChatType.OOC] <- "OOC"
Config["ChatLabel"][ChatType.ALL] <- "ALL"
Config["ChatLabel"][ChatType.IC] <- "IC"
Config["ChatLabel"][ChatType.REPORT] <- "REP"
Config["ChatLabel"][ChatType.ADMIN] <- "ADM"

Config["ChatMethod"] <- {}
Config["ChatMethod"][ChatMethods.Normal] <- { distance = 800, r = 255, g = 255, b = 255 }
Config["ChatMethod"][ChatMethods.Global] <- { distance = -1, r = 200, g = 150, b = 150 }
Config["ChatMethod"][ChatMethods.OutOfCharacter] <- { distance = 800, r = 0, g = 200, b = 255 }
Config["ChatMethod"][ChatMethods.Whisper] <- { distance = 210, r = 150, g = 150, b = 160 }
Config["ChatMethod"][ChatMethods.Shout] <- { distance = 3000, r = 250, g = 150, b = 150 }
Config["ChatMethod"][ChatMethods.Enviroment] <- { distance = 800, r = 125, g = 200, b = 125 }
Config["ChatMethod"][ChatMethods.Action] <- { distance = 800, r = 100, g = 150, b = 200 }

Config["AdminConfirms"] <- {
    // wpisz tu usera i haslo - masz pewnosc ze to ta osoba wbija
}

Config["DynamicBindings"] <- {
    "SPRINT": {
        defined = 219,
        writable = true,
        label = "Sprint",
    },
    "EQUIPMENT": {
        defined = 23,
        writable = true,
        label = "Ekwipunek",
    },
    "EQUIPMENT_ADDITIONAL": {
        defined = 15,
        writable = false,
        label = "Ewkipunek gwarant."
    },
    "DESCRIPTION": {
        defined = 50,
        writable = true,
        label = "Opis targetu",
    },
    "STATISTICS": {
        defined = 48,
        writable = true,
        label = "Statystyki",
    },
    "NOTES": {
        defined = 49,
        writable = true,
        label = "Notatki",
    },
    "POSSIBLE_ONE": {
        defined = 34,
        writable = true,
        label = "Klawisz funkcyjny 1",
    },
    "POSSIBLE_TWO": {
        defined = 36,
        writable = true,
        label = "Klawisz funkcyjny 2",
    },
    "POSSIBLE_THREE": {
        defined = 37,
        writable = true,
        label = "Klawisz funkcyjny 3",
    },
    "POSSIBLE_FOUR": {
        defined = 38,
        writable = true,
        label = "Klawisz funkcyjny 4",
    },
    "CALENDAR": {
        defined = 24,
        writable = true,
        label = "Kalendarz",
    },
    "HELP": {
        defined = 35,
        writable = true,
        label = "Pomoc",
    },
}

// Forest

Config["ForestSystem"] <- {}

Config["ForestSystem"]["Dab"] <- {
    time = 450,
    vob = [
        [0, "OW_LOB_BUSH_V2.3DS"],
        [225, "OW_LOB_BUSH_V7.3DS"],
        [225, "NW_NATURE_BIGTREE_356P.3DS"],
    ],
    outcome = [
        {chance = -1, skill = 1, instance = "ITCR_HK_DROP_62", amount = 1},
    ]
}

Config["FishingSystem"] <- {}

Config["FishingSystem"]["Morskie t0"] <- {
    refresh = 180,
    slots = 20,
    vob = "FISCHEN_1.ASC",
    outcome = [
        {chance = 1, skill = 3, instance = "ITFO_HK_FOOD_63", amount = 1}, //sledz

        {chance = 1, skill = 2, instance = "ITFO_HK_FOOD_63", amount = 1}, //sledz

        {chance = 1, skill = 1, instance = "ITFO_HK_FOOD_63", amount = 1}, //sledz

        {chance = 1, skill = 0, instance = "ITFO_HK_FOOD_63", amount = 1}, //sledz
    ]
}

Config["FishingSystem"]["Morskie t1"] <- {
    refresh = 180,
    slots = 20,
    vob = "FISCHEN_1.ASC",
    outcome = [
        {chance = 25, skill = 3, instance = "ITFO_HK_FOOD_33", amount = 1}, //tunczyk
        {chance = 25, skill = 3, instance = "ITFO_HK_FOOD_74", amount = 1}, //pstrag
        {chance = 25, skill = 3, instance = "ITFO_HK_FOOD_71", amount = 1}, //szprot
        {chance = 25, skill = 3, instance = "ITFO_HK_FOOD_63", amount = 1}, //sledz

        {chance = 25, skill = 2, instance = "ITFO_HK_FOOD_33", amount = 1}, //tunczyk
        {chance = 25, skill = 2, instance = "ITFO_HK_FOOD_74", amount = 1}, //pstrag
        {chance = 25, skill = 2, instance = "ITFO_HK_FOOD_71", amount = 1}, //szprot
        {chance = 25, skill = 2, instance = "ITFO_HK_FOOD_63", amount = 1}, //sledz

        {chance = 25, skill = 1, instance = "ITFO_HK_FOOD_33", amount = 1}, //tunczyk
        {chance = 25, skill = 1, instance = "ITFO_HK_FOOD_74", amount = 1}, //pstrag
        {chance = 25, skill = 1, instance = "ITFO_HK_FOOD_71", amount = 1}, //szprot
        {chance = 25, skill = 1, instance = "ITFO_HK_FOOD_63", amount = 1}, //sledz

        {chance = 1, skill = 0, instance = "ITFO_HK_FOOD_63", amount = 1}, //sledz
    ]
}

Config["FishingSystem"]["Morskie t2"] <- {
    refresh = 180,
    slots = 20,
    vob = "FISCHEN_1.ASC",
    outcome = [
        {chance = 15, skill = 3, instance = "ITFO_HK_FOOD_70", amount = 1}, //morszczuk
        {chance = 15, skill = 3, instance = "ITFO_HK_FOOD_69", amount = 1}, //pirania
        {chance = 15, skill = 3, instance = "ITFO_HK_FOOD_31", amount = 1}, //makrela
        {chance = 15, skill = 3, instance = "ITFO_HK_FOOD_32", amount = 1}, //seriola olbrzymia

        {chance = 15, skill = 2, instance = "ITFO_HK_FOOD_70", amount = 1}, //morszczuk
        {chance = 15, skill = 2, instance = "ITFO_HK_FOOD_69", amount = 1}, //pirania
        {chance = 15, skill = 2, instance = "ITFO_HK_FOOD_31", amount = 1}, //makrela
        {chance = 15, skill = 2, instance = "ITFO_HK_FOOD_32", amount = 1}, //seriola olbrzymia

        {chance = 25, skill = 1, instance = "ITFO_HK_FOOD_33", amount = 1}, //tunczyk
        {chance = 25, skill = 1, instance = "ITFO_HK_FOOD_74", amount = 1}, //pstrag
        {chance = 25, skill = 1, instance = "ITFO_HK_FOOD_71", amount = 1}, //szprot
        {chance = 25, skill = 1, instance = "ITFO_HK_FOOD_63", amount = 1}, //sledz

        {chance = 1, skill = 0, instance = "ITFO_HK_FOOD_63", amount = 1}, //sledz
    ]
}

Config["FishingSystem"]["Morskie t3"] <- {
    refresh = 180,
    slots = 20,
    vob = "FISCHEN_1.ASC",
    outcome = [
        {chance = 1, skill = 3, instance = "ITFO_HK_FOOD_68", amount = 1}, //zlota rybka
        {chance = 33, skill = 3, instance = "ITFO_HK_FOOD_28", amount = 1}, //miecznik
        {chance = 33, skill = 3, instance = "ITFO_HK_FOOD_72", amount = 1}, //rekin
        {chance = 33, skill = 3, instance = "ITFO_HK_FOOD_73", amount = 1}, //barrakuda

        {chance = 15, skill = 2, instance = "ITFO_HK_FOOD_70", amount = 1}, //morszczuk
        {chance = 15, skill = 2, instance = "ITFO_HK_FOOD_69", amount = 1}, //pirania
        {chance = 15, skill = 2, instance = "ITFO_HK_FOOD_31", amount = 1}, //makrela
        {chance = 15, skill = 2, instance = "ITFO_HK_FOOD_32", amount = 1}, //seriola olbrzymia

        {chance = 25, skill = 1, instance = "ITFO_HK_FOOD_33", amount = 1}, //tunczyk
        {chance = 25, skill = 1, instance = "ITFO_HK_FOOD_74", amount = 1}, //pstrag
        {chance = 25, skill = 1, instance = "ITFO_HK_FOOD_71", amount = 1}, //szprot
        {chance = 25, skill = 1, instance = "ITFO_HK_FOOD_63", amount = 1}, //sledz

        {chance = 1, skill = 0, instance = "ITFO_HK_FOOD_63", amount = 1}, //sledz
    ]
}

Config["FishingSystem"]["Rzeczne t0"] <- {
    refresh = 180,
    slots = 20,
    vob = "FISCHEN_1.ASC",
    outcome = [
        {chance = 1, skill = 3, instance = "ITFO_HK_FOOD_30", amount = 1}, //ploc

        {chance = 1, skill = 2, instance = "ITFO_HK_FOOD_30", amount = 1}, //ploc

        {chance = 1, skill = 1, instance = "ITFO_HK_FOOD_30", amount = 1}, //ploc

        {chance = 1, skill = 0, instance = "ITFO_HK_FOOD_30", amount = 1}, //ploc
    ]
}

Config["FishingSystem"]["Rzeczne t1"] <- {
    refresh = 180,
    slots = 20,
    vob = "FISCHEN_1.ASC",
    outcome = [
        {chance = 25, skill = 3, instance = "ITFO_HK_FOOD_67", amount = 1}, //karp
        {chance = 25, skill = 3, instance = "ITFO_HK_FOOD_74", amount = 1}, //pstrag
        {chance = 25, skill = 3, instance = "ITFO_HK_FOOD_29", amount = 1}, //karas
        {chance = 25, skill = 3, instance = "ITFO_HK_FOOD_30", amount = 1}, //ploc

        {chance = 25, skill = 2, instance = "ITFO_HK_FOOD_67", amount = 1}, //karp
        {chance = 25, skill = 2, instance = "ITFO_HK_FOOD_74", amount = 1}, //pstrag
        {chance = 25, skill = 2, instance = "ITFO_HK_FOOD_29", amount = 1}, //karas
        {chance = 25, skill = 2, instance = "ITFO_HK_FOOD_30", amount = 1}, //ploc

        {chance = 25, skill = 1, instance = "ITFO_HK_FOOD_67", amount = 1}, //karp
        {chance = 25, skill = 1, instance = "ITFO_HK_FOOD_74", amount = 1}, //pstrag
        {chance = 25, skill = 1, instance = "ITFO_HK_FOOD_29", amount = 1}, //karas
        {chance = 25, skill = 1, instance = "ITFO_HK_FOOD_30", amount = 1}, //ploc

        {chance = 1, skill = 0, instance = "ITFO_HK_FOOD_30", amount = 1}, //ploc
    ]
}

Config["FishingSystem"]["Rzeczne t2"] <- {
    refresh = 180,
    slots = 20,
    vob = "FISCHEN_1.ASC",
    outcome = [
        {chance = 1, skill = 3, instance = "ITFO_HK_FOOD_68", amount = 1}, //zlota rybka
        {chance = 50, skill = 3, instance = "ITFO_HK_FOOD_27", amount = 1}, //sum
        {chance = 50, skill = 3, instance = "ITFO_HK_FOOD_26", amount = 1}, //szczupak

        {chance = 50, skill = 2, instance = "ITFO_HK_FOOD_27", amount = 1}, //sum
        {chance = 50, skill = 2, instance = "ITFO_HK_FOOD_26", amount = 1}, //szczupak

        {chance = 25, skill = 1, instance = "ITFO_HK_FOOD_67", amount = 1}, //karp
        {chance = 25, skill = 1, instance = "ITFO_HK_FOOD_74", amount = 1}, //pstrag
        {chance = 25, skill = 1, instance = "ITFO_HK_FOOD_29", amount = 1}, //karas
        {chance = 25, skill = 1, instance = "ITFO_HK_FOOD_30", amount = 1}, //ploc

        {chance = 1, skill = 0, instance = "ITFO_HK_FOOD_30", amount = 1}, //ploc
    ]
}

Config["MineSystem"] <- {}

Config["MineSystem"]["Zloze magicznej rudy"] <- {
    refresh = 180,
    slots = 20,
    vob = "ORE_GROUND.ASC",
    outcome = [
        {chance = 70, skill = 5, instance = "ITCR_HK_DROP_61", amount = 9},
        {chance = 20, skill = 5, instance = "ITCR_HK_DROP_61", amount = 7},
        {chance = 5, skill = 5, instance = "ITCR_HK_DROP_61", amount = 5},
        {chance = 5, skill = 5, instance = "ITCR_HK_DROP_61", amount = 4},

        {chance = 65, skill = 4, instance = "ITCR_HK_DROP_61", amount = 7},
        {chance = 30, skill = 4, instance = "ITCR_HK_DROP_61", amount = 5},
        {chance = 5, skill = 4, instance = "ITCR_HK_DROP_61", amount = 3},

        {chance = 70, skill = 3, instance = "ITCR_HK_DROP_61", amount = 5},
        {chance = 25, skill = 3, instance = "ITCR_HK_DROP_61", amount = 3},
        {chance = 5, skill = 3, instance = "ITCR_HK_DROP_61", amount = 2},

        {chance = 65, skill = 2, instance = "ITCR_HK_DROP_61", amount = 3},
        {chance = 30, skill = 2, instance = "ITCR_HK_DROP_61", amount = 2},
        {chance = 5, skill = 2, instance = "ITCR_HK_DROP_61", amount = 1},

        {chance = 60, skill = 1, instance = "ITCR_HK_DROP_61", amount = 2},
        {chance = 30, skill = 1, instance = "ITCR_HK_DROP_61", amount = 1},
        {chance = 10, skill = 1, instance = "ITCR_HK_DROP_61", amount = 0},

        {chance = 90, skill = 0, instance = "ITCR_HK_DROP_61", amount = 1},
        {chance = 10, skill = 0, instance = "ITCR_HK_DROP_61", amount = 0},
    ]
}
Config["MineSystem"]["Zloze nasyconej magicznej rudy"] <- {
    refresh = 180,
    slots = 20,
    vob = "ORE_GROUND_MAGERZ.ASC",
    outcome = [
        {chance = 80, skill = 5, instance = "ITCR_HK_DROP_61", amount = 20},
        {chance = 40, skill = 5, instance = "ITCR_HK_DROP_61", amount = 15},
        {chance = 20, skill = 5, instance = "ITCR_HK_DROP_61", amount = 10},
        {chance = 10, skill = 5, instance = "ITCR_HK_DROP_61", amount = 0},

        {chance = 40, skill = 4, instance = "ITCR_HK_DROP_61", amount = 15},
        {chance = 20, skill = 4, instance = "ITCR_HK_DROP_61", amount = 10},
        {chance = 10, skill = 4, instance = "ITCR_HK_DROP_61", amount = 0},

        {chance = 20, skill = 3, instance = "ITCR_HK_DROP_61", amount = 10},
        {chance = 10, skill = 3, instance = "ITCR_HK_DROP_61", amount = 0},

        {chance = 10, skill = 2, instance = "ITCR_HK_DROP_61", amount = 5},
        {chance = 40, skill = 2, instance = "ITCR_HK_DROP_61", amount = 0},

        {chance = 10, skill = 1, instance = "ITCR_HK_DROP_61", amount = 4},
        {chance = 80, skill = 1, instance = "ITCR_HK_DROP_61", amount = 0},

        {chance = 10, skill = 0, instance = "ITCR_HK_DROP_61", amount = 3},
        {chance = 160, skill = 0, instance = "ITCR_HK_DROP_61", amount = 0},
    ]
}
/*
Config["MineSystem"]["Zloze czerwonej rudy"] <- {
    refresh = 180,
    slots = 20,
    vob = "ORE_GROUND_REDERZ.ASC",
    outcome = [
        {chance = 80, skill = 5, instance = "ITCR_HK_DROP_61", amount = 3},
        {chance = 40, skill = 5, instance = "ITCR_HK_DROP_61", amount = 2},
        {chance = 20, skill = 5, instance = "ITCR_HK_DROP_61", amount = 1},
        {chance = 10, skill = 5, instance = "ITCR_HK_DROP_61", amount = 0},

        {chance = 40, skill = 4, instance = "ITCR_HK_DROP_61", amount = 2},
        {chance = 20, skill = 4, instance = "ITCR_HK_DROP_61", amount = 1},
        {chance = 10, skill = 4, instance = "ITCR_HK_DROP_61", amount = 0},

        {chance = 20, skill = 3, instance = "ITCR_HK_DROP_61", amount = 1},
        {chance = 10, skill = 3, instance = "ITCR_HK_DROP_61", amount = 0},

        {chance = 10, skill = 2, instance = "ITCR_HK_DROP_61", amount = 1},
        {chance = 40, skill = 2, instance = "ITCR_HK_DROP_61", amount = 0},

        {chance = 10, skill = 1, instance = "ITCR_HK_DROP_61", amount = 1},
        {chance = 80, skill = 1, instance = "ITCR_HK_DROP_61", amount = 0},

        {chance = 10, skill = 1, instance = "ITCR_HK_DROP_61", amount = 1},
        {chance = 160, skill = 1, instance = "ITCR_HK_DROP_61", amount = 0},
    ]
}
*/
Config["MineSystem"]["Zloze czarnej rudy"] <- {
    refresh = 180,
    slots = 20,
    vob = "ORE_GROUND_BLACKERZ.ASC",
    outcome = [
        {chance = 80, skill = 5, instance = "ITCR_HK_DROP_74", amount = 3},
        {chance = 40, skill = 5, instance = "ITCR_HK_DROP_74", amount = 2},
        {chance = 20, skill = 5, instance = "ITCR_HK_DROP_74", amount = 1},
        {chance = 10, skill = 5, instance = "ITCR_HK_DROP_74", amount = 0},

        {chance = 40, skill = 4, instance = "ITCR_HK_DROP_74", amount = 2},
        {chance = 20, skill = 4, instance = "ITCR_HK_DROP_74", amount = 1},
        {chance = 10, skill = 4, instance = "ITCR_HK_DROP_74", amount = 0},

        {chance = 20, skill = 3, instance = "ITCR_HK_DROP_74", amount = 1},
        {chance = 10, skill = 3, instance = "ITCR_HK_DROP_74", amount = 0},

        {chance = 10, skill = 2, instance = "ITCR_HK_DROP_74", amount = 1},
        {chance = 40, skill = 2, instance = "ITCR_HK_DROP_74", amount = 0},

        {chance = 10, skill = 1, instance = "ITCR_HK_DROP_74", amount = 1},
        {chance = 80, skill = 1, instance = "ITCR_HK_DROP_74", amount = 0},

        {chance = 10, skill = 1, instance = "ITCR_HK_DROP_74", amount = 1},
        {chance = 160, skill = 1, instance = "ITCR_HK_DROP_74", amount = 0},
    ]
}
Config["MineSystem"]["Zloze wegla"] <- {
    refresh = 180,
    slots = 20,
    vob = "ORE_GROUND_KOHLE.ASC",
    outcome = [
        {chance = 50, skill = 5, instance = "ITCR_HK_DROP_60", amount = 8},
        {chance = 30, skill = 5, instance = "ITCR_HK_DROP_60", amount = 7},
        {chance = 10, skill = 5, instance = "ITCR_HK_DROP_60", amount = 5},
        {chance = 10, skill = 5, instance = "ITCR_HK_DROP_60", amount = 4},

        {chance = 50, skill = 4, instance = "ITCR_HK_DROP_60", amount = 7},
        {chance = 30, skill = 4, instance = "ITCR_HK_DROP_60", amount = 5},
        {chance = 10, skill = 4, instance = "ITCR_HK_DROP_60", amount = 3},
        {chance = 10, skill = 4, instance = "ITCR_HK_DROP_60", amount = 2},

        {chance = 60, skill = 3, instance = "ITCR_HK_DROP_60", amount = 5},
        {chance = 20, skill = 3, instance = "ITCR_HK_DROP_60", amount = 3},
        {chance = 10, skill = 3, instance = "ITCR_HK_DROP_60", amount = 2},
        {chance = 10, skill = 3, instance = "ITCR_HK_DROP_60", amount = 1},

        {chance = 60, skill = 2, instance = "ITCR_HK_DROP_60", amount = 3},
        {chance = 30, skill = 2, instance = "ITCR_HK_DROP_60", amount = 2},
        {chance = 10, skill = 2, instance = "ITCR_HK_DROP_60", amount = 1},

        {chance = 60, skill = 1, instance = "ITCR_HK_DROP_60", amount = 2},
        {chance = 30, skill = 1, instance = "ITCR_HK_DROP_60", amount = 1},
        {chance = 10, skill = 1, instance = "ITCR_HK_DROP_60", amount = 0},

        {chance = 90, skill = 0, instance = "ITCR_HK_DROP_60", amount = 1},
        {chance = 10, skill = 0, instance = "ITCR_HK_DROP_60", amount = 0},
    ]
}
Config["MineSystem"]["Zloze akwamarynu"] <- {
    refresh = 180,
    slots = 20,
    vob = "ORE_GROUND_AQUAMARIN.ASC",
    outcome = [
        {chance = 60, skill = 5, instance = "ITCR_HK_DROP_55", amount = 5},
        {chance = 20, skill = 5, instance = "ITCR_HK_DROP_55", amount = 3},
        {chance = 10, skill = 5, instance = "ITCR_HK_DROP_55", amount = 2},
        {chance = 10, skill = 5, instance = "ITCR_HK_DROP_55", amount = 1},

        {chance = 60, skill = 4, instance = "ITCR_HK_DROP_55", amount = 3},
        {chance = 30, skill = 4, instance = "ITCR_HK_DROP_55", amount = 2},
        {chance = 10, skill = 4, instance = "ITCR_HK_DROP_55", amount = 1},

        {chance = 60, skill = 3, instance = "ITCR_HK_DROP_55", amount = 2},
        {chance = 30, skill = 3, instance = "ITCR_HK_DROP_55", amount = 1},
        {chance = 10, skill = 3, instance = "ITCR_HK_DROP_55", amount = 0},

        {chance = 90, skill = 2, instance = "ITCR_HK_DROP_55", amount = 1},
        {chance = 10, skill = 2, instance = "ITCR_HK_DROP_55", amount = 0},

        {chance = 50, skill = 1, instance = "ITCR_HK_DROP_55", amount = 1},
        {chance = 50, skill = 1, instance = "ITCR_HK_DROP_55", amount = 0},

        {chance = 10, skill = 0, instance = "ITCR_HK_DROP_55", amount = 1},
        {chance = 90, skill = 0, instance = "ITCR_HK_DROP_55", amount = 0},
    ]
}
Config["MineSystem"]["Zloze amethystu"] <- {
    refresh = 180,
    slots = 20,
    vob = "ORE_GROUND_AMETHYST.ASC",
    outcome = [
        {chance = 60, skill = 5, instance = "ITCR_HK_DROP_57", amount = 5},
        {chance = 20, skill = 5, instance = "ITCR_HK_DROP_57", amount = 3},
        {chance = 10, skill = 5, instance = "ITCR_HK_DROP_57", amount = 2},
        {chance = 10, skill = 5, instance = "ITCR_HK_DROP_57", amount = 1},

        {chance = 60, skill = 4, instance = "ITCR_HK_DROP_57", amount = 3},
        {chance = 30, skill = 4, instance = "ITCR_HK_DROP_57", amount = 2},
        {chance = 10, skill = 4, instance = "ITCR_HK_DROP_57", amount = 1},

        {chance = 60, skill = 3, instance = "ITCR_HK_DROP_57", amount = 2},
        {chance = 30, skill = 3, instance = "ITCR_HK_DROP_57", amount = 1},
        {chance = 10, skill = 3, instance = "ITCR_HK_DROP_57", amount = 0},

        {chance = 90, skill = 2, instance = "ITCR_HK_DROP_57", amount = 1},
        {chance = 10, skill = 2, instance = "ITCR_HK_DROP_57", amount = 0},

        {chance = 50, skill = 1, instance = "ITCR_HK_DROP_57", amount = 1},
        {chance = 50, skill = 1, instance = "ITCR_HK_DROP_57", amount = 0},

        {chance = 10, skill = 0, instance = "ITCR_HK_DROP_57", amount = 1},
        {chance = 90, skill = 0, instance = "ITCR_HK_DROP_57", amount = 0},
    ]
}
Config["MineSystem"]["Zloze zelaza"] <- {
    refresh = 180,
    slots = 20,
    vob = "ORE_GROUNDIRON.ASC",
    outcome = [
        {chance = 70, skill = 5, instance = "ITCR_HK_DROP_13", amount = 5},
        {chance = 20, skill = 5, instance = "ITCR_HK_DROP_13", amount = 4},
        {chance = 10, skill = 5, instance = "ITCR_HK_DROP_13", amount = 3},

        {chance = 70, skill = 4, instance = "ITCR_HK_DROP_13", amount = 4},
        {chance = 20, skill = 4, instance = "ITCR_HK_DROP_13", amount = 3},
        {chance = 10, skill = 4, instance = "ITCR_HK_DROP_13", amount = 2},

        {chance = 70, skill = 3, instance = "ITCR_HK_DROP_13", amount = 3},
        {chance = 30, skill = 3, instance = "ITCR_HK_DROP_13", amount = 2},

        {chance = 70, skill = 2, instance = "ITCR_HK_DROP_13", amount = 2},
        {chance = 30, skill = 2, instance = "ITCR_HK_DROP_13", amount = 1},

        {chance = 50, skill = 1, instance = "ITCR_HK_DROP_13", amount = 1},
        {chance = 50, skill = 1, instance = "ITCR_HK_DROP_13", amount = 0},

        {chance = 10, skill = 0, instance = "ITCR_HK_DROP_13", amount = 1},
        {chance = 90, skill = 0, instance = "ITCR_HK_DROP_13", amount = 0},
    ]
}
Config["MineSystem"]["Zloze rubinu"] <- {
    refresh = 180,
    slots = 20,
    vob = "ORE_GROUND_RUBIN.ASC",
    outcome = [
        {chance = 60, skill = 5, instance = "ITCR_HK_DROP_53", amount = 5},
        {chance = 20, skill = 5, instance = "ITCR_HK_DROP_53", amount = 3},
        {chance = 10, skill = 5, instance = "ITCR_HK_DROP_53", amount = 2},
        {chance = 10, skill = 5, instance = "ITCR_HK_DROP_53", amount = 1},

        {chance = 60, skill = 4, instance = "ITCR_HK_DROP_53", amount = 3},
        {chance = 30, skill = 4, instance = "ITCR_HK_DROP_53", amount = 2},
        {chance = 10, skill = 4, instance = "ITCR_HK_DROP_53", amount = 1},

        {chance = 60, skill = 3, instance = "ITCR_HK_DROP_53", amount = 2},
        {chance = 30, skill = 3, instance = "ITCR_HK_DROP_53", amount = 1},
        {chance = 10, skill = 3, instance = "ITCR_HK_DROP_53", amount = 0},

        {chance = 90, skill = 2, instance = "ITCR_HK_DROP_53", amount = 1},
        {chance = 10, skill = 2, instance = "ITCR_HK_DROP_53", amount = 0},

        {chance = 50, skill = 1, instance = "ITCR_HK_DROP_53", amount = 1},
        {chance = 50, skill = 1, instance = "ITCR_HK_DROP_53", amount = 0},

        {chance = 10, skill = 0, instance = "ITCR_HK_DROP_53", amount = 1},
        {chance = 90, skill = 0, instance = "ITCR_HK_DROP_53", amount = 0},
    ]
}
Config["MineSystem"]["Zloze topasu"] <- {
    refresh = 180,
    slots = 20,
    vob = "ORE_GROUND_TOPAS.ASC",
    outcome = [
        {chance = 60, skill = 5, instance = "ITCR_HK_DROP_56", amount = 5},
        {chance = 20, skill = 5, instance = "ITCR_HK_DROP_56", amount = 3},
        {chance = 10, skill = 5, instance = "ITCR_HK_DROP_56", amount = 2},
        {chance = 10, skill = 5, instance = "ITCR_HK_DROP_56", amount = 1},

        {chance = 60, skill = 4, instance = "ITCR_HK_DROP_56", amount = 3},
        {chance = 30, skill = 4, instance = "ITCR_HK_DROP_56", amount = 2},
        {chance = 10, skill = 4, instance = "ITCR_HK_DROP_56", amount = 1},

        {chance = 60, skill = 3, instance = "ITCR_HK_DROP_56", amount = 2},
        {chance = 30, skill = 3, instance = "ITCR_HK_DROP_56", amount = 1},
        {chance = 10, skill = 3, instance = "ITCR_HK_DROP_56", amount = 0},

        {chance = 90, skill = 2, instance = "ITCR_HK_DROP_56", amount = 1},
        {chance = 10, skill = 2, instance = "ITCR_HK_DROP_56", amount = 0},

        {chance = 50, skill = 1, instance = "ITCR_HK_DROP_56", amount = 1},
        {chance = 50, skill = 1, instance = "ITCR_HK_DROP_56", amount = 0},

        {chance = 10, skill = 0, instance = "ITCR_HK_DROP_56", amount = 1},
        {chance = 90, skill = 0, instance = "ITCR_HK_DROP_56", amount = 0},
    ]
}
Config["MineSystem"]["Zloze szmaragdu"] <- {
    refresh = 180,
    slots = 20,
    vob = "ORE_GROUND_SMARAGD.ASC",
    outcome = [
        {chance = 60, skill = 5, instance = "ITCR_HK_DROP_54", amount = 5},
        {chance = 20, skill = 5, instance = "ITCR_HK_DROP_54", amount = 3},
        {chance = 10, skill = 5, instance = "ITCR_HK_DROP_54", amount = 2},
        {chance = 10, skill = 5, instance = "ITCR_HK_DROP_54", amount = 1},

        {chance = 60, skill = 4, instance = "ITCR_HK_DROP_54", amount = 3},
        {chance = 30, skill = 4, instance = "ITCR_HK_DROP_54", amount = 2},
        {chance = 10, skill = 4, instance = "ITCR_HK_DROP_54", amount = 1},

        {chance = 60, skill = 3, instance = "ITCR_HK_DROP_54", amount = 2},
        {chance = 30, skill = 3, instance = "ITCR_HK_DROP_54", amount = 1},
        {chance = 10, skill = 3, instance = "ITCR_HK_DROP_54", amount = 0},

        {chance = 90, skill = 2, instance = "ITCR_HK_DROP_54", amount = 1},
        {chance = 10, skill = 2, instance = "ITCR_HK_DROP_54", amount = 0},

        {chance = 50, skill = 1, instance = "ITCR_HK_DROP_54", amount = 1},
        {chance = 50, skill = 1, instance = "ITCR_HK_DROP_54", amount = 0},

        {chance = 10, skill = 0, instance = "ITCR_HK_DROP_54", amount = 1},
        {chance = 90, skill = 0, instance = "ITCR_HK_DROP_54", amount = 0},
    ]
}

Config["MineSystem"]["Zloze zlota"] <- {
    refresh = 180,
    slots = 20,
    vob = "ORE_GROUND_GOLD.ASC",
    outcome = [
        {chance = 25, skill = 5, instance = "ItMi_GoldNugget_Addon", amount = 5},
        {chance = 25, skill = 5, instance = "ItMi_GoldNugget_Addon", amount = 3},
        {chance = 25, skill = 5, instance = "ItMi_GoldNugget_Addon", amount = 2},
        {chance = 25, skill = 5, instance = "ItMi_GoldNugget_Addon", amount = 1},

        {chance = 20, skill = 4, instance = "ItMi_GoldNugget_Addon", amount = 3},
        {chance = 30, skill = 4, instance = "ItMi_GoldNugget_Addon", amount = 2},
        {chance = 50, skill = 4, instance = "ItMi_GoldNugget_Addon", amount = 1},

        {chance = 20, skill = 3, instance = "ItMi_GoldNugget_Addon", amount = 2},
        {chance = 20, skill = 3, instance = "ItMi_GoldNugget_Addon", amount = 1},
        {chance = 60, skill = 3, instance = "ItMi_GoldNugget_Addon", amount = 0},

        {chance = 30, skill = 2, instance = "ItMi_GoldNugget_Addon", amount = 1},
        {chance = 70, skill = 2, instance = "ItMi_GoldNugget_Addon", amount = 0},

        {chance = 20, skill = 1, instance = "ItMi_GoldNugget_Addon", amount = 1},
        {chance = 80, skill = 1, instance = "ItMi_GoldNugget_Addon", amount = 0},

        {chance = 10, skill = 0, instance = "ItMi_GoldNugget_Addon", amount = 1},
        {chance = 90, skill = 0, instance = "ItMi_GoldNugget_Addon", amount = 0},
    ]
}
Config["MineSystem"]["Zloze srebra"] <- {
    refresh = 180,
    slots = 20,
    vob = "ORE_GROUND_SILVER.ASC",
    outcome = [
        {chance = 25, skill = 5, instance = "ITCR_HK_DROP_52", amount = 5},
        {chance = 25, skill = 5, instance = "ITCR_HK_DROP_52", amount = 3},
        {chance = 25, skill = 5, instance = "ITCR_HK_DROP_52", amount = 2},
        {chance = 25, skill = 5, instance = "ITCR_HK_DROP_52", amount = 1},

        {chance = 20, skill = 4, instance = "ITCR_HK_DROP_52", amount = 3},
        {chance = 30, skill = 4, instance = "ITCR_HK_DROP_52", amount = 2},
        {chance = 50, skill = 4, instance = "ITCR_HK_DROP_52", amount = 1},

        {chance = 20, skill = 3, instance = "ITCR_HK_DROP_52", amount = 2},
        {chance = 20, skill = 3, instance = "ITCR_HK_DROP_52", amount = 1},
        {chance = 60, skill = 3, instance = "ITCR_HK_DROP_52", amount = 0},

        {chance = 30, skill = 2, instance = "ITCR_HK_DROP_52", amount = 1},
        {chance = 70, skill = 2, instance = "ITCR_HK_DROP_52", amount = 0},

        {chance = 20, skill = 1, instance = "ITCR_HK_DROP_52", amount = 1},
        {chance = 80, skill = 1, instance = "ITCR_HK_DROP_52", amount = 0},

        {chance = 10, skill = 0, instance = "ITCR_HK_DROP_52", amount = 1},
        {chance = 90, skill = 0, instance = "ITCR_HK_DROP_52", amount = 0},
    ]
}
Config["MineSystem"]["Zloze siarki"] <- {
    refresh = 180,
    slots = 20,
    vob = "ORE_GROUNDSIAR.ASC",
    outcome = [
        {chance = 60, skill = 5, instance = "ITCR_HK_DROP_58", amount = 5},
        {chance = 20, skill = 5, instance = "ITCR_HK_DROP_58", amount = 3},
        {chance = 10, skill = 5, instance = "ITCR_HK_DROP_58", amount = 2},
        {chance = 10, skill = 5, instance = "ITCR_HK_DROP_58", amount = 1},

        {chance = 60, skill = 4, instance = "ITCR_HK_DROP_58", amount = 3},
        {chance = 30, skill = 4, instance = "ITCR_HK_DROP_58", amount = 2},
        {chance = 10, skill = 4, instance = "ITCR_HK_DROP_58", amount = 1},

        {chance = 60, skill = 3, instance = "ITCR_HK_DROP_58", amount = 2},
        {chance = 30, skill = 3, instance = "ITCR_HK_DROP_58", amount = 1},
        {chance = 10, skill = 3, instance = "ITCR_HK_DROP_58", amount = 0},

        {chance = 90, skill = 2, instance = "ITCR_HK_DROP_58", amount = 1},
        {chance = 10, skill = 2, instance = "ITCR_HK_DROP_58", amount = 0},

        {chance = 50, skill = 1, instance = "ITCR_HK_DROP_58", amount = 1},
        {chance = 50, skill = 1, instance = "ITCR_HK_DROP_58", amount = 0},

        {chance = 10, skill = 0, instance = "ITCR_HK_DROP_58", amount = 1},
        {chance = 90, skill = 0, instance = "ITCR_HK_DROP_58", amount = 0},
    ]
}
Config["MineSystem"]["Zloze soli"] <- {
    refresh = 180,
    slots = 20,
    vob = "ORE_GROUNDSOL.ASC",
    outcome = [
        {chance = 25, skill = 5, instance = "ITCR_HK_DROP_59", amount = 5},
        {chance = 25, skill = 5, instance = "ITCR_HK_DROP_59", amount = 3},
        {chance = 25, skill = 5, instance = "ITCR_HK_DROP_59", amount = 2},
        {chance = 25, skill = 5, instance = "ITCR_HK_DROP_59", amount = 1},

        {chance = 20, skill = 4, instance = "ITCR_HK_DROP_59", amount = 3},
        {chance = 30, skill = 4, instance = "ITCR_HK_DROP_59", amount = 2},
        {chance = 50, skill = 4, instance = "ITCR_HK_DROP_59", amount = 1},

        {chance = 20, skill = 3, instance = "ITCR_HK_DROP_59", amount = 2},
        {chance = 20, skill = 3, instance = "ITCR_HK_DROP_59", amount = 1},
        {chance = 60, skill = 3, instance = "ITCR_HK_DROP_59", amount = 0},

        {chance = 30, skill = 2, instance = "ITCR_HK_DROP_59", amount = 1},
        {chance = 70, skill = 2, instance = "ITCR_HK_DROP_59", amount = 0},

        {chance = 20, skill = 1, instance = "ITCR_HK_DROP_59", amount = 1},
        {chance = 80, skill = 1, instance = "ITCR_HK_DROP_59", amount = 0},

        {chance = 10, skill = 0, instance = "ITCR_HK_DROP_59", amount = 1},
        {chance = 90, skill = 0, instance = "ITCR_HK_DROP_59", amount = 0},
    ]
}
Config["MineSystem"]["Zloze kamienia"] <- {
    refresh = 180,
    slots = 20,
    vob = "ORE_GROUND_STONE.ASC",
    outcome = [
        {chance = 50, skill = 5, instance = "ITCR_HK_DROP_85", amount = 8},
        {chance = 30, skill = 5, instance = "ITCR_HK_DROP_85", amount = 7},
        {chance = 10, skill = 5, instance = "ITCR_HK_DROP_85", amount = 5},
        {chance = 10, skill = 5, instance = "ITCR_HK_DROP_85", amount = 4},

        {chance = 50, skill = 4, instance = "ITCR_HK_DROP_85", amount = 7},
        {chance = 30, skill = 4, instance = "ITCR_HK_DROP_85", amount = 5},
        {chance = 10, skill = 4, instance = "ITCR_HK_DROP_85", amount = 3},
        {chance = 10, skill = 4, instance = "ITCR_HK_DROP_85", amount = 2},

        {chance = 60, skill = 3, instance = "ITCR_HK_DROP_85", amount = 5},
        {chance = 20, skill = 3, instance = "ITCR_HK_DROP_85", amount = 3},
        {chance = 10, skill = 3, instance = "ITCR_HK_DROP_85", amount = 2},
        {chance = 10, skill = 3, instance = "ITCR_HK_DROP_85", amount = 1},

        {chance = 60, skill = 2, instance = "ITCR_HK_DROP_85", amount = 3},
        {chance = 30, skill = 2, instance = "ITCR_HK_DROP_85", amount = 2},
        {chance = 10, skill = 2, instance = "ITCR_HK_DROP_85", amount = 1},

        {chance = 60, skill = 1, instance = "ITCR_HK_DROP_85", amount = 2},
        {chance = 30, skill = 1, instance = "ITCR_HK_DROP_85", amount = 1},
        {chance = 10, skill = 1, instance = "ITCR_HK_DROP_85", amount = 0},

        {chance = 90, skill = 0, instance = "ITCR_HK_DROP_85", amount = 1},
        {chance = 10, skill = 0, instance = "ITCR_HK_DROP_85", amount = 0},
    ]
}


// Player settings configuration

Config["PlayerMaxSkin"] <- 65;
Config["PlayerMaxFace"] <- 456;

Config["InventoryRaws"] <- 6;
Config["InventoryColumns"] <- 4;

// GUI Pointer

Config["NoCharactersInfo"] <- "Witaj na serwerze Historia Kolonii\nTo serwer w kt�rym poczujesz\nco to prawdziwa rozgrywka roleplay.\nZapraszamy do utworzenia postaci.";
Config["HelpInfo"] <- "[#FF0000]Pomoc\n[#FFFFFF]Je�li jeste� nowy\n i chcia�by� dowiedzie� si� \njak dobrze zacz�� \nrozgrywk� na serwerze.\nOdwied� nasz� stron�\nhistoriakolonii.pl\noraz discorda, gdzie\nznajdziesz poradniki, kt�re pomog�\nCi wej�� w ten �wiat.";
Config["FAQ"] <- [
    "Jest� bogiem?",
    "> U�wiadom to sobie",
]

// FractionPermissionType

Config["FractionPermissionType"] <- {};

Config["FractionPermissionType"][FractionPermissionType.Invite] <- "Zapraszanie";
Config["FractionPermissionType"][FractionPermissionType.Leadership] <- "Zarz�dzanie";
Config["FractionPermissionType"][FractionPermissionType.Crafting] <- "Crafting";
Config["FractionPermissionType"][FractionPermissionType.Working] <- "Praca";
Config["FractionPermissionType"][FractionPermissionType.Trade] <- "Handel";
Config["FractionPermissionType"][FractionPermissionType.Faction] <- "Frakcja NPC";

// Interactions

Config["Interactions"] <- {};

Config["Interactions"][InteractionVob.Barbq] <- "BARBQ_SCAV.MDS"
Config["Interactions"][InteractionVob.Anvil] <- "BSANVIL_OC.MDS"
Config["Interactions"][InteractionVob.Mill] <- "MOERS_MESH.ASC"
Config["Interactions"][InteractionVob.Cooker] <- "STOVE_NW_CITY_01.ASC"
Config["Interactions"][InteractionVob.Armourer] <- "ARMORSTAND_PLATTNER.ASC"
Config["Interactions"][InteractionVob.NewCampfire] <- "CAMPFIRE_NORMAL.ASC"
Config["Interactions"][InteractionVob.Alchemy] <- "LATI_G1.MDS"
Config["Interactions"][InteractionVob.RuneTable] <- "RMAKER_1.MDS"
Config["Interactions"][InteractionVob.Pot] <- "CAULDRON_OC2.ASC"
Config["Interactions"][InteractionVob.Bake] <- "BAKE_MESH.MDS"
Config["Interactions"][InteractionVob.Tanning] <- "SKINLAB_PLACE.MDS"
Config["Interactions"][InteractionVob.Sewing] <- "WEBSTUHL_USE.MDS"
Config["Interactions"][InteractionVob.ArrowTable] <- "TISCHLER_MESH.MDS"
Config["Interactions"][InteractionVob.BowTable] <- "BOWSDW_WBENCH.MDS"
Config["Interactions"][InteractionVob.Writing] <- "BUCHSCHREIBEN_01.ASC"
Config["Interactions"][InteractionVob.Cutting] <- "WOODCHOPPIN_NORMAL.MDS"
Config["Interactions"][InteractionVob.RuneMaker] <- "RMELTER_1.ASC"
Config["Interactions"][InteractionVob.BlastFurnace] <- "BSMELTER_1.MDS"
Config["Interactions"][InteractionVob.Furnace] <- "BSFIRE_OC.MDS"
Config["Interactions"][InteractionVob.Herbal] <- "LAB_PSI3.ASC"
Config["Interactions"][InteractionVob.Sworder] <- "BSANVIL_OC2.MDS"
Config["Interactions"][InteractionVob.Juicer] <- "HERB_PSI.MDS"
Config["Interactions"][InteractionVob.Alchemy2] <- "LAB_PSI.ASC"
Config["Interactions"][InteractionVob.FSlaughter] <- "FISCHGRILLEN_FINAL.MDS"
Config["Interactions"][InteractionVob.Carpentry] <- "SAEGEN_3.ASC"
Config["Interactions"][InteractionVob.Sharper] <- "BSSHARP_OC.MDS"
Config["Interactions"][InteractionVob.Cooker2] <- "PAN_OC.MDS"
Config["Interactions"][InteractionVob.Slaught] <- "FISCHSCHNEIDEN_RZEZNIK.ASC"
Config["Interactions"][InteractionVob.MedLab] <- "MOERS_LEKARZ.ASC"
Config["Interactions"][InteractionVob.WaterBucket] <- "BSCOOL_OC.MDS"
Config["Interactions"][InteractionVob.Fischsneiden] <- "FISCHSCHNEIDEN_1.ASC"
Config["Interactions"][InteractionVob.Baumsaege] <- "BAUMSAEGE_1.ASC"
Config["Interactions"][InteractionVob.Stomper] <- "BSFIRE_PROJEKTO_AUSSEN_VAR1.MDS"

Config["InteractionsCraft"] <- [InteractionVob.Cooker, InteractionVob.Armourer, InteractionVob.Barbq, InteractionVob.Anvil, InteractionVob.NewCampfire, InteractionVob.Alchemy, InteractionVob.RuneTable, InteractionVob.Mill, InteractionVob.Pot, InteractionVob.Bake, InteractionVob.Tanning, InteractionVob.Sewing, InteractionVob.ArrowTable, InteractionVob.BowTable, InteractionVob.Writing, InteractionVob.Cutting, InteractionVob.RuneMaker, InteractionVob.BlastFurnace, InteractionVob.Herbal, InteractionVob.Sworder, InteractionVob.Juicer, InteractionVob.Alchemy2, InteractionVob.FSlaughter, InteractionVob.Carpentry, InteractionVob.Sharper, InteractionVob.Cooker2, InteractionVob.Slaught, InteractionVob.Furnace, InteractionVob.MedLab, InteractionVob.WaterBucket, InteractionVob.Fischsneiden, InteractionVob.Baumsaege, InteractionVob.Stomper]

// Craft

Config["CraftAnimations"] <- {};
Config["CraftAnimations"]["NONE"] <- "Nie przypisuj";
Config["CraftAnimations"]["S_SIT"] <- "Siadanie";
Config["CraftAnimations"]["S_LGUARD"] <- "R�ce za�o�one";

Config["CraftCaller"] <- {};
Config["CraftCaller"][CraftCaller.Barbq] <- "Ognisko";
Config["CraftCaller"][CraftCaller.BarbqFraction] <- "Ognisko + frakcja";
Config["CraftCaller"][CraftCaller.Anvil] <- "Kowad�o";
Config["CraftCaller"][CraftCaller.AnvilFraction] <- "Kowad�o + frakcja";
Config["CraftCaller"][CraftCaller.Mill] <- "Mo�dzierz";
Config["CraftCaller"][CraftCaller.MillFraction] <- "Mo�dzierz + frakcja";
Config["CraftCaller"][CraftCaller.Alchemy] <- "St� alchemiczny";
Config["CraftCaller"][CraftCaller.AlchemyFraction] <- "St� alchemiczny + frakcja";
Config["CraftCaller"][CraftCaller.RuneTable] <- "St� runiczny";
Config["CraftCaller"][CraftCaller.RuneTableFraction] <- "St� runiczny + frakcja";
Config["CraftCaller"][CraftCaller.Cooker] <- "Kuchenka";
Config["CraftCaller"][CraftCaller.CookerFraction] <- "Kuchenka + frakcja";
Config["CraftCaller"][CraftCaller.Cooker2] <- "Patelnia";
Config["CraftCaller"][CraftCaller.Cooker2Fraction] <- "Patelnia + frakcja";
Config["CraftCaller"][CraftCaller.Armourer] <- "St� p�atnerza";
Config["CraftCaller"][CraftCaller.ArmourerFraction] <- "St� p�atnerza + frakcja";
Config["CraftCaller"][CraftCaller.Pot] <- "Koci�";
Config["CraftCaller"][CraftCaller.PotFraction] <- "Koci� + frakcja";
Config["CraftCaller"][CraftCaller.Bake] <- "Piec";
Config["CraftCaller"][CraftCaller.BakeFraction] <- "Piec + frakcja";
Config["CraftCaller"][CraftCaller.Tanning] <- "Stojak do garbowania";
Config["CraftCaller"][CraftCaller.TanningFraction] <- "Stojak do garbowania + frakcja";
Config["CraftCaller"][CraftCaller.Sewing] <- "St� do szycia";
Config["CraftCaller"][CraftCaller.SewingFraction] <- "St� do szycia + frakcja";
Config["CraftCaller"][CraftCaller.ArrowTable] <- "St� do strza�";
Config["CraftCaller"][CraftCaller.ArrowTableFraction] <- "St� do strza� + frakcja";
Config["CraftCaller"][CraftCaller.BowTable] <- "St� �uczarski";
Config["CraftCaller"][CraftCaller.BowTableFraction] <- "St� �uczarski + frakcja";
Config["CraftCaller"][CraftCaller.Writing] <- "Pulpit do pisania";
Config["CraftCaller"][CraftCaller.WritingFraction] <- "Pulpit do pisania + frakcja";
Config["CraftCaller"][CraftCaller.Cutting] <- "Pieniek drwala";
Config["CraftCaller"][CraftCaller.CuttingFraction] <- "Pieniek drwala + frakcja";
Config["CraftCaller"][CraftCaller.RuneMaker] <- "Piec runiczny";
Config["CraftCaller"][CraftCaller.RuneMakerFraction] <- "Piec runiczny + frakcja";
Config["CraftCaller"][CraftCaller.BlastFurnace] <- "Piec hutniczy";
Config["CraftCaller"][CraftCaller.BlastFurnaceFraction] <- "Piec hutniczy + frakcja";
Config["CraftCaller"][CraftCaller.Furnace] <- "Piec do stali";
Config["CraftCaller"][CraftCaller.FurnaceFraction] <- "Piec do stali + frakcja";
Config["CraftCaller"][CraftCaller.Herbal] <- "St� zielarza";
Config["CraftCaller"][CraftCaller.HerbalFraction] <- "St� zielarza + frakcja";
Config["CraftCaller"][CraftCaller.Sworder] <- "St� miecznika";
Config["CraftCaller"][CraftCaller.SworderFraction] <- "St� miecznika + frakcja";
Config["CraftCaller"][CraftCaller.Sugarer] <- "St� cukiernika";
Config["CraftCaller"][CraftCaller.SugarerFraction] <- "St� cukiernika + frakcja";
Config["CraftCaller"][CraftCaller.Juicer] <- "St� do soku";
Config["CraftCaller"][CraftCaller.JuicerFraction] <- "St� do soku + frakcja";
Config["CraftCaller"][CraftCaller.Alchemy2] <- "St� gorzelniczny";
Config["CraftCaller"][CraftCaller.Alchemy2Fraction] <- "St� gorzelniczny + frakcja";
Config["CraftCaller"][CraftCaller.FSlaughter] <- "St� do siekania ryby";
Config["CraftCaller"][CraftCaller.FSlaughterFraction] <- "St� do siekania ryby + frakcja";
Config["CraftCaller"][CraftCaller.Carpentry] <- "St� do pi�owania desek";
Config["CraftCaller"][CraftCaller.CarpentryFraction] <- "St� do pi�owania desek + frakcja";
Config["CraftCaller"][CraftCaller.Sharper] <- "Ko�o szlifierskie";
Config["CraftCaller"][CraftCaller.SharperFraction] <- "Ko�o szlifierskie + frakcja";
Config["CraftCaller"][CraftCaller.Slaught] <- "St� rze�nika";
Config["CraftCaller"][CraftCaller.SlaughtFraction] <- "St� rze�nika + frakcja";
Config["CraftCaller"][CraftCaller.MedLab] <- "St� medyka";
Config["CraftCaller"][CraftCaller.MedLabFraction] <- "St� medyka + frakcja";
Config["CraftCaller"][CraftCaller.WaterBucket] <- "Wiadro z wod�";
Config["CraftCaller"][CraftCaller.WaterBucketFraction] <- "Wiadro z wod� + frakcja";
Config["CraftCaller"][CraftCaller.Fischsneiden] <- "St� do filetowania";
Config["CraftCaller"][CraftCaller.FischsneidenFraction] <- "St� do filetowania + frakcja";
Config["CraftCaller"][CraftCaller.NewCampfire] <- "Ognisko z patykiem";
Config["CraftCaller"][CraftCaller.NewCampfireFraction] <- "Ognisko z patykiem + frakcja";
Config["CraftCaller"][CraftCaller.Baumsaege] <- "K�oda";
Config["CraftCaller"][CraftCaller.BaumsaegeFraction] <- "K�oda + frakcja";
Config["CraftCaller"][CraftCaller.Stomper] <- "Przerabiarka";
Config["CraftCaller"][CraftCaller.StomperFraction] <- "Przerabiarka + frakcja";

Config["CraftCallerVobRelation"] <- {};

Config["CraftCallerVobRelation"][CraftCaller.Barbq] <- [InteractionVob.Barbq, false];
Config["CraftCallerVobRelation"][CraftCaller.BarbqFraction] <- [InteractionVob.Barbq, true];
Config["CraftCallerVobRelation"][CraftCaller.Anvil] <- [InteractionVob.Anvil, false];
Config["CraftCallerVobRelation"][CraftCaller.AnvilFraction] <- [InteractionVob.Anvil, true];
Config["CraftCallerVobRelation"][CraftCaller.Mill] <- [InteractionVob.Mill, false];
Config["CraftCallerVobRelation"][CraftCaller.MillFraction] <- [InteractionVob.Mill, true];
Config["CraftCallerVobRelation"][CraftCaller.Alchemy] <- [InteractionVob.Alchemy, false];
Config["CraftCallerVobRelation"][CraftCaller.AlchemyFraction] <- [InteractionVob.Alchemy, true];
Config["CraftCallerVobRelation"][CraftCaller.RuneTable] <- [InteractionVob.RuneTable, false];
Config["CraftCallerVobRelation"][CraftCaller.RuneTableFraction] <- [InteractionVob.RuneTable, true];
Config["CraftCallerVobRelation"][CraftCaller.Cooker] <- [InteractionVob.Cooker, false];
Config["CraftCallerVobRelation"][CraftCaller.CookerFraction] <- [InteractionVob.Cooker, true];
Config["CraftCallerVobRelation"][CraftCaller.Cooker2] <- [InteractionVob.Cooker2, false];
Config["CraftCallerVobRelation"][CraftCaller.Cooker2Fraction] <- [InteractionVob.Cooker2, true];
Config["CraftCallerVobRelation"][CraftCaller.Armourer] <- [InteractionVob.Armourer, false];
Config["CraftCallerVobRelation"][CraftCaller.ArmourerFraction] <- [InteractionVob.Armourer, true];
Config["CraftCallerVobRelation"][CraftCaller.Pot] <- [InteractionVob.Pot, false];
Config["CraftCallerVobRelation"][CraftCaller.PotFraction] <- [InteractionVob.Pot, true];
Config["CraftCallerVobRelation"][CraftCaller.Bake] <- [InteractionVob.Bake, false];
Config["CraftCallerVobRelation"][CraftCaller.BakeFraction] <- [InteractionVob.Bake, true];
Config["CraftCallerVobRelation"][CraftCaller.Tanning] <- [InteractionVob.Tanning, false];
Config["CraftCallerVobRelation"][CraftCaller.TanningFraction] <- [InteractionVob.Tanning, true];
Config["CraftCallerVobRelation"][CraftCaller.Sewing] <- [InteractionVob.Sewing, false];
Config["CraftCallerVobRelation"][CraftCaller.SewingFraction] <- [InteractionVob.Sewing, true];
Config["CraftCallerVobRelation"][CraftCaller.ArrowTable] <- [InteractionVob.ArrowTable, false];
Config["CraftCallerVobRelation"][CraftCaller.ArrowTableFraction] <- [InteractionVob.ArrowTable, true];
Config["CraftCallerVobRelation"][CraftCaller.BowTable] <- [InteractionVob.BowTable, false];
Config["CraftCallerVobRelation"][CraftCaller.BowTableFraction] <- [InteractionVob.BowTable, true];
Config["CraftCallerVobRelation"][CraftCaller.Writing] <- [InteractionVob.Writing, false];
Config["CraftCallerVobRelation"][CraftCaller.WritingFraction] <- [InteractionVob.Writing, true];
Config["CraftCallerVobRelation"][CraftCaller.Cutting] <- [InteractionVob.Cutting, false];
Config["CraftCallerVobRelation"][CraftCaller.CuttingFraction] <- [InteractionVob.Cutting, true];
Config["CraftCallerVobRelation"][CraftCaller.RuneMaker] <- [InteractionVob.RuneMaker, false];
Config["CraftCallerVobRelation"][CraftCaller.RuneMakerFraction] <- [InteractionVob.RuneMaker, true];
Config["CraftCallerVobRelation"][CraftCaller.BlastFurnace] <- [InteractionVob.BlastFurnace, false];
Config["CraftCallerVobRelation"][CraftCaller.BlastFurnaceFraction] <- [InteractionVob.BlastFurnace, true];
Config["CraftCallerVobRelation"][CraftCaller.Furnace] <- [InteractionVob.Furnace, false];
Config["CraftCallerVobRelation"][CraftCaller.FurnaceFraction] <- [InteractionVob.Furnace, true];
Config["CraftCallerVobRelation"][CraftCaller.Herbal] <- [InteractionVob.Herbal, false];
Config["CraftCallerVobRelation"][CraftCaller.HerbalFraction] <- [InteractionVob.Herbal, true];
Config["CraftCallerVobRelation"][CraftCaller.Sworder] <- [InteractionVob.Sworder, false];
Config["CraftCallerVobRelation"][CraftCaller.SworderFraction] <- [InteractionVob.Sworder, true];
Config["CraftCallerVobRelation"][CraftCaller.Sugarer] <- [InteractionVob.Sugarer, false];
Config["CraftCallerVobRelation"][CraftCaller.SugarerFraction] <- [InteractionVob.Sugarer, true];
Config["CraftCallerVobRelation"][CraftCaller.Juicer] <- [InteractionVob.Juicer, false];
Config["CraftCallerVobRelation"][CraftCaller.JuicerFraction] <- [InteractionVob.Juicer, true];
Config["CraftCallerVobRelation"][CraftCaller.Alchemy2] <- [InteractionVob.Alchemy2, false];
Config["CraftCallerVobRelation"][CraftCaller.Alchemy2Fraction] <- [InteractionVob.Alchemy2, true];
Config["CraftCallerVobRelation"][CraftCaller.FSlaughter] <- [InteractionVob.FSlaughter, false];
Config["CraftCallerVobRelation"][CraftCaller.FSlaughterFraction] <- [InteractionVob.FSlaughter, true];
Config["CraftCallerVobRelation"][CraftCaller.Carpentry] <- [InteractionVob.Carpentry, false];
Config["CraftCallerVobRelation"][CraftCaller.CarpentryFraction] <- [InteractionVob.Carpentry, true];
Config["CraftCallerVobRelation"][CraftCaller.Sharper] <- [InteractionVob.Sharper, false];
Config["CraftCallerVobRelation"][CraftCaller.SharperFraction] <- [InteractionVob.Sharper, true];
Config["CraftCallerVobRelation"][CraftCaller.Slaught] <- [InteractionVob.Slaught, false];
Config["CraftCallerVobRelation"][CraftCaller.SlaughtFraction] <- [InteractionVob.Slaught, true];
Config["CraftCallerVobRelation"][CraftCaller.MedLab] <- [InteractionVob.MedLab, false];
Config["CraftCallerVobRelation"][CraftCaller.MedLabFraction] <- [InteractionVob.MedLab, true];
Config["CraftCallerVobRelation"][CraftCaller.WaterBucket] <- [InteractionVob.WaterBucket, false];
Config["CraftCallerVobRelation"][CraftCaller.WaterBucketFraction] <- [InteractionVob.WaterBucket, true];
Config["CraftCallerVobRelation"][CraftCaller.Fischsneiden] <- [InteractionVob.Fischsneiden, false];
Config["CraftCallerVobRelation"][CraftCaller.FischsneidenFraction] <- [InteractionVob.Fischsneiden, true];
Config["CraftCallerVobRelation"][CraftCaller.NewCampfire] <- [InteractionVob.NewCampfire, false];
Config["CraftCallerVobRelation"][CraftCaller.NewCampfireFraction] <- [InteractionVob.NewCampfire, true];
Config["CraftCallerVobRelation"][CraftCaller.Baumsaege] <- [InteractionVob.Baumsaege, false];
Config["CraftCallerVobRelation"][CraftCaller.BaumsaegeFraction] <- [InteractionVob.Baumsaege, true];
Config["CraftCallerVobRelation"][CraftCaller.Stomper] <- [InteractionVob.Stomper, false];
Config["CraftCallerVobRelation"][CraftCaller.StomperFraction] <- [InteractionVob.Stomper, true];

// Player const to values

Config["MaxDailyLearnPoints"] <- 2;
Config["DailyGivenPoints"] <- 3;
Config["HourlyGivenNuggets"] <- 3;

Config["PlayerFatness"] <- {}
Config["PlayerFatness"][PlayerFatness.Thin] <- 0.44
Config["PlayerFatness"][PlayerFatness.Skinny] <- 0.18
Config["PlayerFatness"][PlayerFatness.Normal] <- 1.00
Config["PlayerFatness"][PlayerFatness.Muscular] <- 1.28
Config["PlayerFatness"][PlayerFatness.Thick] <- 1.50
Config["PlayerFatness"][PlayerFatness.Fat] <- 1.72

Config["PlayerWalk"] <- {}
Config["PlayerWalk"][PlayerWalk.Normal] <- "";
Config["PlayerWalk"][PlayerWalk.Tired] <- "HUMANS_TIRED.MDS";
Config["PlayerWalk"][PlayerWalk.Woman] <- "HUMANS_BABE.MDS";
Config["PlayerWalk"][PlayerWalk.Army] <- "HUMANS_MILITIA.MDS";
Config["PlayerWalk"][PlayerWalk.Lazy] <- "HUMANS_RELAXED.MDS";
Config["PlayerWalk"][PlayerWalk.Arrogant] <- "HUMANS_ARROGANCE.MDS";
Config["PlayerWalk"][PlayerWalk.Mage] <- "HUMANS_MAGE.MDS";
Config["PlayerWalk"][PlayerWalk.Pockets] <- "HUMANS_KIESZENIE.MDS";
Config["PlayerWalk"][PlayerWalk.OldTired] <- "HUMANS_G1TIRED.MDS";
Config["PlayerWalk"][PlayerWalk.OldArrogant] <- "HUMANS_G1ARROGANCE.MDS";
Config["PlayerWalk"][PlayerWalk.OldMage] <- "HUMANS_G1MAGE.MDS";
Config["PlayerWalk"][PlayerWalk.OldLazy] <- "HUMANS_G1RELAXED.MDS";

Config["PlayerStatus"] <- {}
Config["PlayerStatus"][PlayerStatus.Active] <- "Aktywna";
Config["PlayerStatus"][PlayerStatus.Killed] <- "Zabita";
Config["PlayerStatus"][PlayerStatus.Deleted] <- "Usuni�ta";

Config["PlayerSkill"] <- {}
Config["PlayerSkill"][PlayerSkill.Blacksmith] <- "Kowal";
Config["PlayerSkill"][PlayerSkill.Armourer] <- "P�atnerz";
Config["PlayerSkill"][PlayerSkill.Alchemy] <- "Alchemik";
Config["PlayerSkill"][PlayerSkill.Cook] <- "Kucharz";
Config["PlayerSkill"][PlayerSkill.Fisherman] <- "Rybak";
Config["PlayerSkill"][PlayerSkill.Miner] <- "G�rnik";
Config["PlayerSkill"][PlayerSkill.Lumberjack] <- "Drwal";
Config["PlayerSkill"][PlayerSkill.Herbalist] <- "Zielarz";
Config["PlayerSkill"][PlayerSkill.Hunter] <- "My�liwy";
Config["PlayerSkill"][PlayerSkill.Butcher] <- "Rze�nik";
Config["PlayerSkill"][PlayerSkill.Carpenter] <- "Stolarz";
Config["PlayerSkill"][PlayerSkill.Fletcher] <- "�uczarz";
Config["PlayerSkill"][PlayerSkill.Medic] <- "Medyk";
Config["PlayerSkill"][PlayerSkill.Enchanter] <- "---------";
Config["PlayerSkill"][PlayerSkill.Tailor] <- "Krawiec";
Config["PlayerSkill"][PlayerSkill.Thief] <- "Z�odziej";

Config["PlayerAttributes"] <- {}
Config["PlayerAttributes"][PlayerAttributes.Str] <- "Si�a";
Config["PlayerAttributes"][PlayerAttributes.Dex] <- "Zr�czno��";
Config["PlayerAttributes"][PlayerAttributes.Int] <- "Inteligencja";
Config["PlayerAttributes"][PlayerAttributes.Hp] <- "Witalno��";
Config["PlayerAttributes"][PlayerAttributes.Mana] <- "Mana";
Config["PlayerAttributes"][PlayerAttributes.OneH] <- "Bro� 1h";
Config["PlayerAttributes"][PlayerAttributes.TwoH] <- "Bro� 2h";
Config["PlayerAttributes"][PlayerAttributes.Bow] <- "�uk";
Config["PlayerAttributes"][PlayerAttributes.Cbow] <- "Kusza";
Config["PlayerAttributes"][PlayerAttributes.MagicLvl] <- "Kr�g magii";
Config["PlayerAttributes"][PlayerAttributes.Stamina] <- "Wytrzyma�o��";
Config["PlayerAttributes"][PlayerAttributes.LearnPoints] <- "Punkty nauki";
Config["PlayerAttributes"][PlayerAttributes.Hunger] <- "Najedzenie";

Config["PlayerSkillValue"] <- ["0","1","2","3","4","5","6","7","8","9"];

Config["PlayerSkillUpgrade"] <- {};
Config["PlayerSkillUpgrade"][PlayerSkill.Blacksmith] <- [
    { min = 0, costLp = 6 },
    { min = 1, costLp = 60 },
    { min = 2, costLp = 100 },
    { min = 3, costLp = 150 },
    { min = 4, max = 5, costLp = 200 },
];
Config["PlayerSkillUpgrade"][PlayerSkill.Armourer] <- [
    { min = 0, costLp = 6 },
    { min = 1, costLp = 60 },
    { min = 2, costLp = 100 },
    { min = 3, costLp = 150 },
    { min = 4, max = 5, costLp = 200 },
];
Config["PlayerSkillUpgrade"][PlayerSkill.Alchemy] <- [
    { min = 0, costLp = 6 },
    { min = 1, costLp = 60 },
    { min = 2, costLp = 100 },
    { min = 3, costLp = 150 },
    { min = 4, max = 5, costLp = 200 },
];
Config["PlayerSkillUpgrade"][PlayerSkill.Cook] <- [
    { min = 0, costLp = 6 },
    { min = 1, costLp = 60 },
    { min = 2, costLp = 100 },
    { min = 3, costLp = 150 },
    { min = 4, max = 5, costLp = 200 },
];
Config["PlayerSkillUpgrade"][PlayerSkill.Miner] <- [
    { min = 0, costLp = 6 },
    { min = 1, costLp = 60 },
    { min = 2, costLp = 100 },
    { min = 3, costLp = 150 },
    { min = 4, max = 5, costLp = 200 },
];
Config["PlayerSkillUpgrade"][PlayerSkill.Lumberjack] <- [
    { min = 0, costLp = 6 },
    { min = 1, costLp = 60 },
    { min = 2, costLp = 100 },
    { min = 3, costLp = 150 },
    { min = 4, max = 5, costLp = 200 },
];
Config["PlayerSkillUpgrade"][PlayerSkill.Herbalist] <- [
    { min = 0, costLp = 6 },
    { min = 1, costLp = 60 },
    { min = 2, max = 3, costLp = 100 },
];
Config["PlayerSkillUpgrade"][PlayerSkill.Hunter] <- [
    { min = 0, costLp = 6 },
    { min = 1, costLp = 30 },
    { min = 2, costLp = 50 },
    { min = 3, costLp = 75 },
    { min = 4, max = 5, costLp = 100 },
];

Config["PlayerSkillUpgrade"][PlayerSkill.Enchanter] <- [
    //{ min = 0, costLp = 6 },
    { min = 1, costLp = 60 },
    { min = 2, max = 3, costLp = 100 },
];

Config["PlayerSkillUpgrade"][PlayerSkill.Thief] <- [
    //{ min = 0, costLp = 60 },
    { min = 1, max = 2, costLp = 100 },
];

Config["PlayerSkillUpgrade"][PlayerSkill.Fisherman] <- [
    { min = 0, costLp = 6 },
    { min = 1, costLp = 60 },
    { min = 2, max = 3, costLp = 100 },
];

Config["PlayerSkillUpgrade"][PlayerSkill.Tailor] <- [
    { min = 0, costLp = 6 },
    { min = 1, costLp = 60 },
    { min = 2, costLp = 100 },
    { min = 3, costLp = 150 },
    { min = 4, max = 5, costLp = 200 },
];

Config["PlayerSkillUpgrade"][PlayerSkill.Butcher] <- [
    { min = 0, costLp = 6 },
    { min = 1, costLp = 60 },
    { min = 2, max = 3, costLp = 100 },
];

Config["PlayerSkillUpgrade"][PlayerSkill.Carpenter] <- [
    { min = 0, costLp = 6 },
    { min = 1, costLp = 60 },
    { min = 2, costLp = 100 },
    { min = 3, costLp = 150 },
    { min = 4, max = 5, costLp = 200 },
];

Config["PlayerSkillUpgrade"][PlayerSkill.Fletcher] <- [
    { min = 0, costLp = 6 },
    { min = 1, costLp = 60 },
    { min = 2, costLp = 100 },
    { min = 3, costLp = 150 },
    { min = 4, max = 5, costLp = 200 },
];

Config["PlayerSkillUpgrade"][PlayerSkill.Medic] <- [
    { min = 0, costLp = 6 },
    { min = 1, costLp = 60 },
    { min = 2, costLp = 100 },
    { min = 3, costLp = 150 },
    { min = 4, max = 5, costLp = 200 },
];


Config["PlayerAttributeUpgrade"] <- {};
Config["PlayerAttributeUpgrade"][PlayerAttributes.Str] <- [
    { min = 10, costLp = 3, value = 1 },
    { min = 31, costLp = 5, value = 1},
    { min = 51, max = 100, costLp = 6, value = 1},
];
Config["PlayerAttributeUpgrade"][PlayerAttributes.Dex] <- [
    { min = 10, costLp = 3, value = 1 },
    { min = 31, costLp = 5, value = 1},
    { min = 51, max = 100, costLp = 6, value = 1},
];
Config["PlayerAttributeUpgrade"][PlayerAttributes.Int] <- [
    { min = 10, costLp = 5, value = 2 },
    { min = 31, costLp = 3, value = 1},
    { min = 51, max = 100, costLp = 4, value = 1},
];
Config["PlayerAttributeUpgrade"][PlayerAttributes.MagicLvl] <- [
    { min = 1, costLp = 60 },
    { min = 2, max = 3, costLp = 120 },
];
Config["PlayerAttributeUpgrade"][PlayerAttributes.Hp] <- [
    { min = 100, max = 400, costLp = 6, value = 10 },
];
Config["PlayerAttributeUpgrade"][PlayerAttributes.Mana] <- [
    { min = 0, costLp = 5, value = 10 },
    { min = 100, costLp = 8, value = 10 },
    { min = 200, costLp = 9, value = 10 },
    { min = 400, costLp = 10, value = 10 },
    { min = 600, costLp = 11, value = 10 },
    { min = 800, max = 1000, costLp = 12, value = 10 },
];
Config["PlayerAttributeUpgrade"][PlayerAttributes.OneH] <- [
    { min = 0, costLp = 5, value = 2 },
    { min = 31, max = 100, costLp = 4, value = 1 },
];
Config["PlayerAttributeUpgrade"][PlayerAttributes.TwoH] <- [
    { min = 0, costLp = 5, value = 2 },
    { min = 31, max = 100, costLp = 4, value = 1 },
];
Config["PlayerAttributeUpgrade"][PlayerAttributes.Bow] <- [
    { min = 0, costLp = 5, value = 2 },
    { min = 31, max = 100, costLp = 4, value = 1 },
];
Config["PlayerAttributeUpgrade"][PlayerAttributes.Cbow] <- [
    { min = 0, costLp = 5, value = 2 },
    { min = 31, max = 100, costLp = 4, value = 1 },
];

Config["PlayerScenario"] <- {};
Config["PlayerScenario"][PlayerScenario.Castaway] <- {
    name = "Rozbitek",
    spawn = {x = -38693.3, y = -1857.19, z = 35685.4, angle = 66.4793},
    description = "Tw�j statek zniszczy� potw�r\n morski, a ty obudzi�e�\n si� na pla�y."
}
Config["PlayerScenario"][PlayerScenario.Prisoner] <- {
    name = "Skazany",
    spawn = {x = 5534.22, y = 5349.3, z = 36573, angle = 189},
    description = "Zosta�e� zes�any na \nprace w G�rniczej Dolinie."
}
Config["PlayerScenario"][PlayerScenario.Traveler] <- {
    name = "Podr�nik",
    spawn = {x = -10537.1, y = -770.156, z = 12978, angle = 201},
    description = "Wyruszy�e� w dot�d \nnieznane - do G�rniczej Doliny."
}
Config["PlayerScenario"][PlayerScenario.Amnesion] <- {
    name = "Amnezja",
    spawn = {x = 10768, y = 120.781, z = -8274.84, angle = 308},
    description = "Nie wiesz co si� z tob� dzia�o,\nkim jeste�, ani gdzie jeste�."
}

// Settings

Config["Settings"] <- {
    "Resolution": {
        name = "Rozdzielczo��",
        description = "Rozdzielczo�� ekranu.",
        type = SettingsFormType.Select,
        data = [],
        defined = 0,
    },
    "ChatAnimation": {
        name = "Animacja chatu",
        type = SettingsFormType.Checkbox,
        defined = true
    },
    "DialogCustomFont": {
        name = "Customowy font dialog�w z npc",
        type = SettingsFormType.Checkbox,
        defined = true
    },
    "OtherCustomFont": {
        name = "Customowy font w GUI",
        type = SettingsFormType.Checkbox,
        defined = true
    },
    "ChatPercentOnScreen": {
        name = "Szeroko�� chatu",
        description = "Szeroko�� chatu w odniesieniu do ca�o�ci ekranu.",
        type = SettingsFormType.Range,
        min = 50,
        max = 100,
        defined = 70
    },
    "ChatLines": {
        name = "Linijki chatu",
        type = SettingsFormType.Range,
        min = 5,
        max = 30,
        defined = 10,
    },
    "InterfaceType": {
        name = "Tryb interfejsu",
        type = SettingsFormType.Select,
        data = [
            {
                id = "Minimal",
                name = "Minimalny"
            },
            {
                id = "Optimal",
                name = "Optymalny"
            },
            {
                id = "Maximum",
                name = "Ci�g�a widoczno��"
            },
        ]
        defined = "Optimal"
    },
    "InterfaceScale": {
        name = "Skala interfejsu",
        type = SettingsFormType.Range,
        min = 1,
        max = 10,
        defined = 5
    },
    "DialogAutoContinue": {
        name = "Automatyczne przewijanie dialogu",
        description = "Czas po jakim automatycznie dialog przesuwa si� dalej.",
        type = SettingsFormType.Range,
        min = 5,
        max = 30,
        defined = 15,
    }
    "NamesRound": {
        name = "Wy�wietlanie nick�w w pobli�u",
        description = "Wy�wietla nicki graczy w pobli�u naszego bohatera.",
        type = SettingsFormType.Checkbox,
        defined = true
    }
    "TraditionalBars": {
        name = "Wy�wietlanie pask�w hp/many",
        description = "Wy�wietla tradycyjny pasek hp i many.",
        type = SettingsFormType.Checkbox,
        defined = false
    },
    "TraditionalFocusBars": {
        name = "Wy�wietlanie paska hp przeciwnika",
        description = "Wy�wietla tradycyjny pasek hp dla targetu.",
        type = SettingsFormType.Checkbox,
        defined = true
    }
};

//Wypalona gleba - polna gleba
//Gleba niskiej jako�ci - lesna gleba
//Zwyk�a gleba - gorska gleba
//Bogata w minera�y gleba - bagienna gleba
//Idealna gleba - gleba przy wodzie

Config["GroundLevel"] <- ["Wypalona gleba", "Gleba niskiej jako�ci", "Zwyk�a gleba", "Bogata w minera�y gleba", "Idealna gleba"]

Config["GroundHerbSystem"] <- {};
Config["GroundHerbSystem"]["Wszedzie"] <- [
    {
        cooldown = 7200, // sekundy
        instance = "ITPL_HK_HERB_01" // Serafis
    },
    {
        cooldown = 7200, // sekundy
        instance = "ITPL_HK_HERB_10" // Ziele lecznicze
    },
	{
        cooldown = 7200, // sekundy
        instance = "ITPL_SPEED_HERB_01" // Z�bate ziele
    },
	{
        cooldown = 7200, // sekundy
        instance = "ITPL_HK_HERB_11" // Korze� leczniczy
    },
	{
        cooldown = 7200, // sekundy
        instance = "ITPL_MANA_HERB_01" // Ognista pokrzywa
    },
	{
        cooldown = 7200, // sekundy
        instance = "ITPL_WEED" // Chwasty
    }
]
Config["GroundHerbSystem"]["Las"] <- [
    {
        cooldown = 7200, // sekundy
        instance = "ITPL_FORESTBERRY" // Le�na jagoda
    },
    {
        cooldown = 7200, // sekundy
        instance = "ITPL_HK_HERB_14" // Twardzie�
    },
	{
        cooldown = 7200, // sekundy
        instance = "ITPL_HK_HERB_09" // Ro�lina lecznicza
    },
	{
        cooldown = 7200, // sekundy
        instance = "ITPL_HK_HERB_05" // Psianka
    },
	{
        cooldown = 7200, // sekundy
        instance = "ITPL_HK_HERB_18" // Piekielnik
    },
	{
        cooldown = 7200, // sekundy
        instance = "ITPL_HK_HERB_19" // Gorzki chleb
    },
	{
        cooldown = 7200, // sekundy
        instance = "ITPL_MUSHROOM_01" // ciemny grzyb
    },
	{
        cooldown = 7200, // sekundy
        instance = "ITPL_MUSHROOM_02" // mi�so kopacza
    },
	{
        cooldown = 7200, // sekundy
        instance = "ITPL_HK_HERB_61" // Amanita
    },
	{
        cooldown = 7200, // sekundy
        instance = "ITPL_HK_HERB_62" // Craterellus
    },
	{
        cooldown = 7200, // sekundy
        instance = "ITPL_HK_HERB_63" // T�usty kret
    },
	{
        cooldown = 7200, // sekundy
        instance = "ITPL_HK_HERB_64" // Du�y muchomor
    },
	{
        cooldown = 7200, // sekundy
        instance = "ITPL_HK_HERB_65" // Pieprznik
    },
	{
        cooldown = 7200, // sekundy
        instance = "ITPL_WEED" // Chwasty
    }
]
Config["GroundHerbSystem"]["Jaskinia"] <- [
    	{
        cooldown = 7200, // sekundy
        instance = "ITPL_HK_HERB_18" // Piekielnik
    },
	{
        cooldown = 7200, // sekundy
        instance = "ITPL_HK_HERB_19" // Gorzki chleb
    },
	{
        cooldown = 7200, // sekundy
        instance = "ITPL_MUSHROOM_01" // ciemny grzyb
    },
	{
        cooldown = 7200, // sekundy
        instance = "ITPL_MUSHROOM_02" // mi�so kopacza
    },
	{
        cooldown = 7200, // sekundy
        instance = "ITPL_HK_HERB_61" // Amanita
    },
	{
        cooldown = 7200, // sekundy
        instance = "ITPL_HK_HERB_62" // Craterellus
    },
	{
        cooldown = 7200, // sekundy
        instance = "ITPL_HK_HERB_63" // T�usty kret
    },
	{
        cooldown = 7200, // sekundy
        instance = "ITPL_HK_HERB_64" // Du�y muchomor
    },
	{
        cooldown = 7200, // sekundy
        instance = "ITPL_HK_HERB_65" // Pieprznik
    },
	{
        cooldown = 7200, // sekundy
        instance = "ITPL_TEMP_HERB" // Rdest polny
    }
]
Config["GroundHerbSystem"]["Bagna"] <- [
    {
        cooldown = 7200, // sekundy
        instance = "ITPL_SWAMPHERB" // Bagienne ziele
    },
	{
        cooldown = 7200, // sekundy
        instance = "ITPL_WEED" // Chwasty
    }
]
Config["GroundHerbSystem"]["Orkowe"] <- [
    {
        cooldown = 8000, // sekundy
        instance = "ITPL_HK_HERB_07" // Orkowe ziele
    },
	{
        cooldown = 7200, // sekundy
        instance = "ITPL_WEED" // Chwasty
    }
]
Config["GroundHerbSystem"]["Trolowe"] <- [
    {
        cooldown = 8000, // sekundy
        instance = "ITPL_SAGITTA_HERB_MIS" // S�oneczny aloes
    },
	{
        cooldown = 7200, // sekundy
        instance = "ITPL_WEED" // Chwasty
    }
]
Config["GroundHerbSystem"]["Polne"] <- [
    {
        cooldown = 7200, // sekundy
        instance = "ITPL_BLUEPLANT " // Niebieski bez
    },
	{
        cooldown = 7200, // sekundy
        instance = "ITPL_PLANEBERRY " // Polna jagoda
    },
	{
        cooldown = 7200, // sekundy
        instance = "ITPL_WEED" // Chwasty
    }
]
Config["GroundHerbSystem"]["RzadkieWszedzie"] <- [
    {
        cooldown = 8000, // sekundy
        instance = "ITPL_MANA_HERB_02" // Ogniste ziele
    },
	{
        cooldown = 8000, // sekundy
        instance = "ITPL_MANA_HERB_03" // Ognisty korze�
    },
	{
        cooldown = 7200, // sekundy
        instance = "ITPL_WEED" // Chwasty
    }
]
Config["GroundHerbSystem"]["RzadkieJaskinia"] <- [
    {
        cooldown = 8000, // sekundy
        instance = "ITPL_HK_HERB_66" // Trolli czort
    },
	{
        cooldown = 8000, // sekundy
        instance = "ITPL_HK_HERB_67" // Harpii szpon
    },
	{
        cooldown = 7200, // sekundy
        instance = "ITPL_HK_HERB_68" // Magiczny grzyb
    },
]
Config["GroundHerbSystem"]["RzadkieJawne"] <- [
    {
        cooldown = 8000, // sekundy
        instance = "ITPL_HK_HERB_02" // Velais
    },
	{
        cooldown = 8000, // sekundy
        instance = "ITPL_HK_HERB_06" // Kocianka
    },
	{
        cooldown = 8000, // sekundy
        instance = "ITPL_DEX_HERB_01" // Goblinie jagody
    },
	{
        cooldown = 8000, // sekundy
        instance = "ITPL_STRENGTH_HERB_01" // Smoczy korze�
    },
	{
        cooldown = 7200, // sekundy
        instance = "ITPL_WEED" // Chwasty
    }
]
Config["GroundHerbSystem"]["Gory"] <- [
    {
        cooldown = 8000, // sekundy
        instance = "ITPL_HK_HERB_03" // G�rski mech
    },
	{
        cooldown = 7200, // sekundy
        instance = "ITPL_WEED" // Chwasty
    }
]
Config["GroundHerbSystem"]["Cmentarz"] <- [
    {
        cooldown = 9500, // sekundy
        instance = "ITPL_HK_HERB_04" // Mech nagrobny
    },
	{
        cooldown = 7200, // sekundy
        instance = "ITPL_WEED" // Chwasty
    }
]
Config["GroundHerbSystem"]["Rzepa"] <- [
    {
        cooldown = 7200, // sekundy
        instance = "ITPL_BEET" // Rzepa
    },
	{
        cooldown = 7200, // sekundy
        instance = "ITPL_WEED" // Chwasty
    }
]
Config["GroundHerbSystem"]["BardzoRzadkie1"] <- [
    {
        cooldown = 9500, // sekundy
        instance = "ITPL_HK_HERB_08" // Li�� d�bu
    },
	{
        cooldown = 7200, // sekundy
        instance = "ITPL_WEED" // Chwasty
    }
]
Config["GroundHerbSystem"]["BardzoRzadkie2"] <- [
    {
        cooldown = 9500, // sekundy
        instance = "ITPL_HK_HERB_12" // Krucze ziele
    },
	{
        cooldown = 7200, // sekundy
        instance = "ITPL_WEED" // Chwasty
    }
]
Config["GroundHerbSystem"]["BardzoRzadkie3"] <- [
    {
        cooldown = 9500, // sekundy
        instance = "ITPL_HK_HERB_13" // Czarne ziele
    },
	{
        cooldown = 7200, // sekundy
        instance = "ITPL_WEED" // Chwasty
    }
]
Config["GroundHerbSystem"]["BardzoRzadkie4"] <- [
    {
        cooldown = 9500, // sekundy
        instance = "ITPL_HK_HERB_15" // Dragrot
    },
	{
        cooldown = 7200, // sekundy
        instance = "ITPL_WEED" // Chwasty
    }
]
Config["GroundHerbSystem"]["BardzoRzadkie5"] <- [
    {
        cooldown = 9500, // sekundy
        instance = "ITPL_PERM_HERB" // Szczaw kr�lewski
    },
	{
        cooldown = 7200, // sekundy
        instance = "ITPL_WEED" // Chwasty
    }
]
Config["GroundHerbSystem"]["Jablka"] <- [
    {
        cooldown = 7200, // sekundy
        instance = "ITFO_HK_FOOD_11" // jablko
    },
]
Config["GroundHerbSystem"]["Woda morska"] <- [
    {
        cooldown = 7200, // sekundy
        instance = "ITCR_HK_INGR_26" // woda morska
    },
]




Config["HerbSystem"] <- {};
Config["HerbSystem"]["Bagienne ziele 1"] <- [
    {
        name = "Nasiono bagiennego ziela",
        time = 90, // sekundy
        difficulty = 2, // 0 - 10
        instance = "ITPL_HK_HERB_20",
        amount = 1,
        staminaCost = 2,
        ground = 3,
    },
    {
        name = "Ro�lina bagiennego ziela",
        time = 115, // sekundy
        difficulty = 2, // 0 - 10
        instance = "ITPL_WEED",
        amount = 1,
        staminaCost = 2,
        ground = 3,
    },
    {
        name = "Bagienne ziele",
        time = 135, // sekundy
        difficulty = 2, // 0 - 10
        instance = "ITPL_WEED",
        amount = 1,
        staminaCost = 2,
        ground = 3,
    },
	{
        name = "Bagienne ziele",
        time = 165, // sekundy
        difficulty = 2, // 0 - 10
        instance = "ITPL_SWAMPHERB",
        amount = 1,
        staminaCost = 2,
        ground = 3,
    }
]

Config["HerbSystem"]["Bagienne ziele 2"] <- [
    {
        name = "Nasiono bagiennego ziela",
        time = 120, // sekundy
        difficulty = 2, // 0 - 10
        instance = "ITPL_HK_HERB_20",
        amount = 1,
        staminaCost = 2,
        ground = 3,
    },
    {
        name = "Ro�lina bagiennego ziela",
        time = 135, // sekundy
        difficulty = 2, // 0 - 10
        instance = "ITPL_WEED",
        amount = 1,
        staminaCost = 2,
        ground = 3,
    },
    {
        name = "Ro�lina bagiennego ziela",
        time = 150, // sekundy
        difficulty = 2, // 0 - 10
        instance = "ITPL_WEED",
        amount = 1,
        staminaCost = 2,
        ground = 3,
    },
	{
        name = "Bagienne ziele",
        time = 175, // sekundy
        difficulty = 2, // 0 - 10
        instance = "ITPL_SWAMPHERB",
        amount = 2,
        staminaCost = 2,
        ground = 3,
    }
]

Config["HerbSystem"]["Bagienne ziele 3"] <- [
    {
        name = "Nasiono bagiennego ziela",
        time = 120, // sekundy
        difficulty = 3, // 0 - 10
        instance = "ITPL_HK_HERB_20",
        amount = 1,
        staminaCost = 2,
        ground = 3,
    },
    {
        name = "Ro�lina bagiennego ziela",
        time = 150, // sekundy
        difficulty = 3, // 0 - 10
        instance = "ITPL_WEED",
        amount = 1,
        staminaCost = 2,
        ground = 3,
    },
    {
        name = "Bagienne ziele",
        time = 175, // sekundy
        difficulty = 3, // 0 - 10
        instance = "ITPL_SWAMPHERB",
        amount = 1,
        staminaCost = 2,
        ground = 3,
    },
	{
        name = "Bagienne ziele",
        time = 20, // sekundy
        difficulty = 3, // 0 - 10
        instance = "ITPL_SWAMPHERB",
        amount = 3,
        staminaCost = 2,
        ground = 3,
    }
]

Config["HerbSystem"]["Ro�lina lecznicza"] <- [
    {
        name = "Nasiono ro�liny leczniczej",
        time = 90, // sekundy
        difficulty = 1, // 0 - 10
        instance = "ITPL_HK_HERB_30",
        amount = 1,
        staminaCost = 2,
        ground = 0,
    },
    {
        name = "Niedojrza�a ro�lina lecznicza",
        time = 115, // sekundy
        difficulty = 1, // 0 - 10
        instance = "ITPL_WEED",
        amount = 1,
        staminaCost = 2,
        ground = 0,
    },
    {
        name = "Ro�lina lecznicza",
        time = 135, // sekundy
        difficulty = 1, // 0 - 10
        instance = "ITPL_WEED",
        amount = 1,
        staminaCost = 2,
        ground = 0,
    },
	{
        name = "Ro�lina lecznicza",
        time = 165, // sekundy
        difficulty = 1, // 0 - 10
        instance = "ITPL_HK_HERB_09",
        amount = 1,
        staminaCost = 2,
        ground = 0,
    }
]

Config["HerbSystem"]["Ro�lina lecznicza 2"] <- [
    {
        name = "Nasiono ro�liny leczniczej",
        time = 120, // sekundy
        difficulty = 2, // 0 - 10
        instance = "ITPL_HK_HERB_30",
        amount = 1,
        staminaCost = 20,
        ground = 0,
    },
    {
        name = "Niedojrza�a ro�lina lecznicza",
        time = 135, // sekundy
        difficulty = 2, // 0 - 10
        instance = "ITPL_WEED",
        amount = 1,
        staminaCost = 2,
        ground = 0,
    },
    {
        name = "Niedojrza�a ro�lina lecznicza",
        time = 150, // sekundy
        difficulty = 2, // 0 - 10
        instance = "ITPL_WEED",
        amount = 1,
        staminaCost = 2,
        ground = 0,
    },
	{
        name = "Ro�lina lecznicza",
        time = 175, // sekundy
        difficulty = 2, // 0 - 10
        instance = "ITPL_HK_HERB_09",
        amount = 2,
        staminaCost = 2,
        ground = 0,
    }
]

Config["HerbSystem"]["Ro�lina lecznicza 3"] <- [
    {
        name = "Nasiono ro�liny leczniczej",
        time = 120, // sekundy
        difficulty = 3, // 0 - 10
        instance = "ITPL_HK_HERB_30",
        amount = 1,
        staminaCost = 2,
        ground = 0,
    },
    {
        name = "Niedojrza�a ro�lina lecznicza",
        time = 150, // sekundy
        difficulty = 3, // 0 - 10
        instance = "ITPL_WEED",
        amount = 1,
        staminaCost = 2,
        ground = 0,
    },
    {
        name = "Ro�lina lecznicza",
        time = 175, // sekundy
        difficulty = 3, // 0 - 10
        instance = "ITPL_HK_HERB_09",
        amount = 1,
        staminaCost = 2,
        ground = 0,
    },
	{
        name = "Ro�lina lecznicza",
        time = 220, // sekundy
        difficulty = 3, // 0 - 10
        instance = "ITPL_HK_HERB_09",
        amount = 3,
        staminaCost = 2,
        ground = 0,
    }
]

Config["HerbSystem"]["Ognista pokrzywa"] <- [
    {
        name = "Nasiono ognistej pokrzywy",
        time = 90, // sekundy
        difficulty = 1, // 0 - 10
        instance = "ITPL_HK_HERB_25",
        amount = 1,
        staminaCost = 2,
        ground = 1,
    },
    {
        name = "Niedojrza�a ognista pokrzywa",
        time = 115, // sekundy
        difficulty = 1, // 0 - 10
        instance = "ITPL_WEED",
        amount = 1,
        staminaCost = 2,
        ground = 1,
    },
    {
        name = "Niedojrza�a ognista pokrzywa",
        time = 135, // sekundy
        difficulty = 1, // 0 - 10
        instance = "ITPL_WEED",
        amount = 1,
        staminaCost = 2,
        ground = 1,
    },
	{
        name = "Ognista pokrzywa",
        time = 165, // sekundy
        difficulty = 1, // 0 - 10
        instance = "ITPL_MANA_HERB_01",
        amount = 1,
        staminaCost = 2,
        ground = 1,
    }
]

Config["HerbSystem"]["Ognista pokrzywa 2"] <- [
    {
        name = "Nasiono ognistej pokrzywy",
        time = 120, // sekundy
        difficulty = 2, // 0 - 10
        instance = "ITPL_HK_HERB_25",
        amount = 1,
        staminaCost = 20,
        ground = 1,
    },
    {
        name = "Niedojrza�a ognista pokrzywa",
        time = 135, // sekundy
        difficulty = 2, // 0 - 10
        instance = "ITPL_WEED",
        amount = 1,
        staminaCost = 2,
        ground = 1,
    },
    {
        name = "Niedojrza�a ognista pokrzywa",
        time = 150, // sekundy
        difficulty = 2, // 0 - 10
        instance = "ITPL_WEED",
        amount = 1,
        staminaCost = 2,
        ground = 1,
    },
	{
        name = "Ognista pokrzywa",
        time = 175, // sekundy
        difficulty = 2, // 0 - 10
        instance = "ITPL_MANA_HERB_01",
        amount = 2,
        staminaCost = 2,
        ground = 1,
    }
]

Config["HerbSystem"]["Ognista pokrzywa 3"] <- [
    {
        name = "Nasiono ognistej pokrzywy",
        time = 120, // sekundy
        difficulty = 3, // 0 - 10
        instance = "ITPL_HK_HERB_25",
        amount = 1,
        staminaCost = 2,
        ground = 1,
    },
    {
        name = "Niedojrza�a ognista pokrzywa",
        time = 150, // sekundy
        difficulty = 3, // 0 - 10
        instance = "ITPL_WEED",
        amount = 1,
        staminaCost = 2,
        ground = 1,
    },
    {
        name = "Ognista pokrzywa",
        time = 175, // sekundy
        difficulty = 3, // 0 - 10
        instance = "ITPL_MANA_HERB_01",
        amount = 1,
        staminaCost = 2,
        ground = 1,
    },
	{
        name = "Ognista pokrzywa",
        time = 220, // sekundy
        difficulty = 3, // 0 - 10
        instance = "ITPL_MANA_HERB_01",
        amount = 3,
        staminaCost = 2,
        ground = 1,
    }
]

Config["HerbSystem"]["Krucze ziele"] <- [
    {
        name = "Nasiono kruczego ziela",
        time = 90, // sekundy
        difficulty = 1, // 0 - 10
        instance = "ITPL_HK_HERB_47",
        amount = 1,
        staminaCost = 2,
        ground = 2,
    },
    {
        name = "Niedojrza�e krucze ziele",
        time = 115, // sekundy
        difficulty = 3, // 0 - 10
        instance = "ITPL_WEED",
        amount = 1,
        staminaCost = 2,
        ground = 2,
    },
    {
        name = "Niedojrza�e krucze ziele",
        time = 135, // sekundy
        difficulty = 3, // 0 - 10
        instance = "ITPL_WEED",
        amount = 1,
        staminaCost = 2,
        ground = 2,
    },
	{
        name = "Krucze ziele",
        time = 165, // sekundy
        difficulty = 3, // 0 - 10
        instance = "ITPL_HK_HERB_12",
        amount = 1,
        staminaCost = 2,
        ground = 2,
    }
]

Config["HerbSystem"]["Krucze ziele 2"] <- [
    {
        name = "Nasiono kruczego ziela",
        time = 120, // sekundy
        difficulty = 3, // 0 - 10
        instance = "ITPL_HK_HERB_47",
        amount = 1,
        staminaCost = 20,
        ground = 2,
    },
    {
        name = "Niedojrza�e krucze ziele",
        time = 135, // sekundy
        difficulty = 3, // 0 - 10
        instance = "ITPL_WEED",
        amount = 1,
        staminaCost = 2,
        ground = 2,
    },
    {
        name = "Niedojrza�e krucze ziele",
        time = 150, // sekundy
        difficulty = 3, // 0 - 10
        instance = "ITPL_WEED",
        amount = 1,
        staminaCost = 2,
        ground = 2,
    },
	{
        name = "Krucze ziele",
        time = 175, // sekundy
        difficulty = 3, // 0 - 10
        instance = "ITPL_HK_HERB_12",
        amount = 2,
        staminaCost = 2,
        ground = 2,
    }
]

Config["HerbSystem"]["Krucze ziele 3"] <- [
    {
        name = "Nasiono kruczego ziela",
        time = 120, // sekundy
        difficulty = 3, // 0 - 10
        instance = "ITPL_HK_HERB_47",
        amount = 1,
        staminaCost = 2,
        ground = 2,
    },
    {
        name = "Niedojrza�e krucze ziele",
        time = 150, // sekundy
        difficulty = 3, // 0 - 10
        instance = "ITPL_WEED",
        amount = 1,
        staminaCost = 2,
        ground = 2,
    },
    {
        name = "Krucze ziele",
        time = 175, // sekundy
        difficulty = 3, // 0 - 10
        instance = "ITPL_HK_HERB_12",
        amount = 1,
        staminaCost = 2,
        ground = 2,
    },
	{
        name = "Krucze ziele",
        time = 220, // sekundy
        difficulty = 3, // 0 - 10
        instance = "ITPL_HK_HERB_12",
        amount = 3,
        staminaCost = 2,
        ground = 2,
    }
]

Config["HerbSystem"]["Rdest polny"] <- [
    {
        name = "Nasiono rdestu polnego",
        time = 90, // sekundy
        difficulty = 2, // 0 - 10
        instance = "ITPL_HK_HERB_29",
        amount = 1,
        staminaCost = 2,
        ground = 0,
    },
    {
        name = "Niedojrza�y rdest polny",
        time = 115, // sekundy
        difficulty = 2, // 0 - 10
        instance = "ITPL_WEED",
        amount = 1,
        staminaCost = 2,
        ground = 0,
    },
    {
        name = "Niedojrza�y rdest polny",
        time = 135, // sekundy
        difficulty = 2, // 0 - 10
        instance = "ITPL_WEED",
        amount = 1,
        staminaCost = 2,
        ground = 0,
    },
	{
        name = "Rdest polny",
        time = 165, // sekundy
        difficulty = 2, // 0 - 10
        instance = "ITPL_TEMP_HERB",
        amount = 1,
        staminaCost = 2,
        ground = 0,
    }
]

Config["HerbSystem"]["Rdest polny 2"] <- [
    {
        name = "Nasiono rdestu polnego",
        time = 120, // sekundy
        difficulty = 2, // 0 - 10
        instance = "ITPL_HK_HERB_29",
        amount = 1,
        staminaCost = 20,
        ground = 0,
    },
    {
        name = "Niedojrza�y rdest polny",
        time = 135, // sekundy
        difficulty = 2, // 0 - 10
        instance = "ITPL_WEED",
        amount = 1,
        staminaCost = 2,
        ground = 0,
    },
    {
        name = "Niedojrza�y rdest polny",
        time = 150, // sekundy
        difficulty = 2, // 0 - 10
        instance = "ITPL_WEED",
        amount = 1,
        staminaCost = 2,
        ground = 0,
    },
	{
        name = "Rdest polny",
        time = 175, // sekundy
        difficulty = 2, // 0 - 10
        instance = "ITPL_TEMP_HERB",
        amount = 2,
        staminaCost = 2,
        ground = 0,
    }
]

Config["HerbSystem"]["Rdest polny 3"] <- [
    {
        name = "Nasiono rdestu polnego",
        time = 120, // sekundy
        difficulty = 3, // 0 - 10
        instance = "ITPL_HK_HERB_29",
        amount = 1,
        staminaCost = 2,
        ground = 0,
    },
    {
        name = "Niedojrza�y rdest polny",
        time = 150, // sekundy
        difficulty = 3, // 0 - 10
        instance = "ITPL_WEED",
        amount = 1,
        staminaCost = 2,
        ground = 0,
    },
    {
        name = "Rdest polny",
        time = 175, // sekundy
        difficulty = 3, // 0 - 10
        instance = "ITPL_TEMP_HERB",
        amount = 1,
        staminaCost = 2,
        ground = 0,
    },
	{
        name = "Rdest polny",
        time = 220, // sekundy
        difficulty = 3, // 0 - 10
        instance = "ITPL_TEMP_HERB",
        amount = 3,
        staminaCost = 2,
        ground = 0,
    }
]

Config["HerbSystem"]["Ciemny grzyb"] <- [
    {
        name = "Nasiono ciemnego grzyba",
        time = 90, // sekundy
        difficulty = 1, // 0 - 10
        instance = "ITPL_HK_HERB_58",
        amount = 1,
        staminaCost = 2,
        ground = 1,
    },
    {
        name = "Niedojrza�y ciemny grzyb",
        time = 115, // sekundy
        difficulty = 1, // 0 - 10
        instance = "ITPL_HK_HERB_58",
        amount = 1,
        staminaCost = 2,
        ground = 1,
    },
    {
        name = "Niedojrza�y ciemny grzyb",
        time = 135, // sekundy
        difficulty = 1, // 0 - 10
        instance = "ITPL_HK_HERB_58",
        amount = 1,
        staminaCost = 2,
        ground = 1,
    },
	{
        name = "Ciemny grzyb",
        time = 165, // sekundy
        difficulty = 1, // 0 - 10
        instance = "ITPL_MUSHROOM_01",
        amount = 1,
        staminaCost = 2,
        ground = 1,
    }
]

Config["HerbSystem"]["Ciemny grzyb 2"] <- [
    {
        name = "Nasiono ciemnego grzyba",
        time = 120, // sekundy
        difficulty = 2, // 0 - 10
        instance = "ITPL_HK_HERB_58",
        amount = 1,
        staminaCost = 20,
        ground = 1,
    },
    {
        name = "Niedojrza�y ciemny grzyb",
        time = 135, // sekundy
        difficulty = 2, // 0 - 10
        instance = "ITPL_HK_HERB_58",
        amount = 1,
        staminaCost = 2,
        ground = 1,
    },
    {
        name = "Niedojrza�y ciemny grzyb",
        time = 150, // sekundy
        difficulty = 2, // 0 - 10
        instance = "ITPL_HK_HERB_58",
        amount = 1,
        staminaCost = 2,
        ground = 1,
    },
	{
        name = "Ciemny grzyb",
        time = 175, // sekundy
        difficulty = 2, // 0 - 10
        instance = "ITPL_MUSHROOM_01",
        amount = 2,
        staminaCost = 2,
        ground = 1,
    }
]

Config["HerbSystem"]["Ciemny grzyb 3"] <- [
    {
        name = "Nasiono ciemnego grzyba",
        time = 120, // sekundy
        difficulty = 3, // 0 - 10
        instance = "ITPL_HK_HERB_58",
        amount = 1,
        staminaCost = 2,
        ground = 1,
    },
    {
        name = "Niedojrza�y ciemny grzyb",
        time = 150, // sekundy
        difficulty = 3, // 0 - 10
        instance = "ITPL_HK_HERB_58",
        amount = 1,
        staminaCost = 2,
        ground = 1,
    },
    {
        name = "Ciemny grzyb",
        time = 175, // sekundy
        difficulty = 3, // 0 - 10
        instance = "ITPL_MUSHROOM_01",
        amount = 1,
        staminaCost = 2,
        ground = 1,
    },
	{
        name = "Ciemny grzyb",
        time = 220, // sekundy
        difficulty = 3, // 0 - 10
        instance = "ITPL_MUSHROOM_01",
        amount = 3,
        staminaCost = 2,
        ground = 1,
    }
]

Config["HerbSystem"]["Mi�so kopacza"] <- [
    {
        name = "Nasiono mi�sa kopacza",
        time = 90, // sekundy
        difficulty = 1, // 0 - 10
        instance = "ITPL_HK_HERB_59",
        amount = 1,
        staminaCost = 2,
        ground = 0,
    },
    {
        name = "Niedojrza�e mi�so kopacza",
        time = 115, // sekundy
        difficulty = 1, // 0 - 10
        instance = "ITPL_HK_HERB_59",
        amount = 1,
        staminaCost = 2,
        ground = 0,
    },
    {
        name = "Niedojrza�e mi�so kopacza",
        time = 135, // sekundy
        difficulty = 1, // 0 - 10
        instance = "ITPL_HK_HERB_59",
        amount = 1,
        staminaCost = 2,
        ground = 0,
    },
	{
        name = "Mi�so kopacza",
        time = 165, // sekundy
        difficulty = 1, // 0 - 10
        instance = "ITPL_MUSHROOM_02",
        amount = 1,
        staminaCost = 2,
        ground = 0,
    }
]

Config["HerbSystem"]["Mi�so kopacza 2"] <- [
    {
        name = "Nasiono mi�sa kopacza",
        time = 120, // sekundy
        difficulty = 2, // 0 - 10
        instance = "ITPL_HK_HERB_59",
        amount = 1,
        staminaCost = 20,
        ground = 0,
    },
    {
        name = "Niedojrza�e mi�so kopacza",
        time = 135, // sekundy
        difficulty = 2, // 0 - 10
        instance = "ITPL_HK_HERB_59",
        amount = 1,
        staminaCost = 2,
        ground = 0,
    },
    {
        name = "Niedojrza�e mi�so kopacza",
        time = 150, // sekundy
        difficulty = 2, // 0 - 10
        instance = "ITPL_HK_HERB_59",
        amount = 1,
        staminaCost = 2,
        ground = 0,
    },
	{
        name = "Mi�so kopacza",
        time = 175, // sekundy
        difficulty = 2, // 0 - 10
        instance = "ITPL_MUSHROOM_02",
        amount = 2,
        staminaCost = 2,
        ground = 0,
    }
]

Config["HerbSystem"]["Mi�so kopacza 3"] <- [
    {
        name = "Nasiono mi�sa kopacza",
        time = 120, // sekundy
        difficulty = 3, // 0 - 10
        instance = "ITPL_HK_HERB_59",
        amount = 1,
        staminaCost = 2,
        ground = 0,
    },
    {
        name = "Niedojrza�e mi�so kopacza",
        time = 150, // sekundy
        difficulty = 3, // 0 - 10
        instance = "ITPL_HK_HERB_59",
        amount = 1,
        staminaCost = 2,
        ground = 0,
    },
    {
        name = "Mi�so kopacza",
        time = 175, // sekundy
        difficulty = 3, // 0 - 10
        instance = "ITPL_MUSHROOM_02",
        amount = 1,
        staminaCost = 2,
        ground = 0,
    },
	{
        name = "Mi�so kopacza",
        time = 220, // sekundy
        difficulty = 3, // 0 - 10
        instance = "ITPL_MUSHROOM_02",
        amount = 3,
        staminaCost = 2,
        ground = 0,
    }
]

Config["HerbSystem"]["Piekielnik"] <- [
    {
        name = "Nasiono piekielnika",
        time = 90, // sekundy
        difficulty = 1, // 0 - 10
        instance = "ITPL_HK_HERB_45",
        amount = 1,
        staminaCost = 2,
        ground = 0,
    },
    {
        name = "Niedojrza�y piekielnik",
        time = 115, // sekundy
        difficulty = 1, // 0 - 10
        instance = "ITPL_HK_HERB_45",
        amount = 1,
        staminaCost = 2,
        ground = 0,
    },
    {
        name = "Niedojrza�y piekielnik",
        time = 135, // sekundy
        difficulty = 1, // 0 - 10
        instance = "ITPL_HK_HERB_45",
        amount = 1,
        staminaCost = 2,
        ground = 0,
    },
	{
        name = "Piekielnik",
        time = 165, // sekundy
        difficulty = 1, // 0 - 10
        instance = "ITPL_HK_HERB_18",
        amount = 1,
        staminaCost = 2,
        ground = 0,
    }
]

Config["HerbSystem"]["Piekielnik 2"] <- [
    {
        name = "Nasiono piekielnika",
        time = 120, // sekundy
        difficulty = 2, // 0 - 10
        instance = "ITPL_HK_HERB_45",
        amount = 1,
        staminaCost = 20,
        ground = 0,
    },
    {
        name = "Niedojrza�y piekielnik",
        time = 150, // sekundy
        difficulty = 2, // 0 - 10
        instance = "ITPL_HK_HERB_45",
        amount = 1,
        staminaCost = 2,
        ground = 0,
    },
    {
        name = "Niedojrza�y piekielnik",
        time = 175, // sekundy
        difficulty = 2, // 0 - 10
        instance = "ITPL_HK_HERB_45",
        amount = 1,
        staminaCost = 2,
        ground = 0,
    },
	{
        name = "Piekielnik",
        time = 175, // sekundy
        difficulty = 2, // 0 - 10
        instance = "ITPL_HK_HERB_18",
        amount = 2,
        staminaCost = 2,
        ground = 0,
    }
]

Config["HerbSystem"]["Piekielnik 3"] <- [
    {
        name = "Nasiono piekielnika",
        time = 120, // sekundy
        difficulty = 3, // 0 - 10
        instance = "ITPL_HK_HERB_45",
        amount = 1,
        staminaCost = 2,
        ground = 0,
    },
    {
        name = "Niedojrza�y piekielnik",
        time = 150, // sekundy
        difficulty = 3, // 0 - 10
        instance = "ITPL_HK_HERB_45",
        amount = 1,
        staminaCost = 2,
        ground = 0,
    },
    {
        name = "Piekielnik",
        time = 175, // sekundy
        difficulty = 3, // 0 - 10
        instance = "ITPL_HK_HERB_18",
        amount = 1,
        staminaCost = 2,
        ground = 0,
    },
	{
        name = "Piekielnik",
        time = 220, // sekundy
        difficulty = 3, // 0 - 10
        instance = "ITPL_HK_HERB_18",
        amount = 3,
        staminaCost = 2,
        ground = 0,
    }
]

Config["HerbSystem"]["Gorzki chleb"] <- [
    {
        name = "Nasiono gorzkiego chleba",
        time = 90, // sekundy
        difficulty = 1, // 0 - 10
        instance = "ITPL_HK_HERB_46",
        amount = 1,
        staminaCost = 2,
        ground = 0,
    },
    {
        name = "Niedojrza�y gorzki chleb",
        time = 115, // sekundy
        difficulty = 1, // 0 - 10
        instance = "ITPL_HK_HERB_46",
        amount = 1,
        staminaCost = 2,
        ground = 0,
    },
    {
        name = "Niedojrza�y gorzki chleb",
        time = 135, // sekundy
        difficulty = 1, // 0 - 10
        instance = "ITPL_HK_HERB_46",
        amount = 1,
        staminaCost = 2,
        ground = 0,
    },
	{
        name = "Gorzki chleb",
        time = 165, // sekundy
        difficulty = 1, // 0 - 10
        instance = "ITPL_HK_HERB_19",
        amount = 1,
        staminaCost = 2,
        ground = 0,
    }
]

Config["HerbSystem"]["Gorzki chleb 2"] <- [
    {
        name = "Nasiono gorzkiego chleba",
        time = 120, // sekundy
        difficulty = 2, // 0 - 10
        instance = "ITPL_HK_HERB_46",
        amount = 1,
        staminaCost = 20,
        ground = 0,
    },
    {
        name = "Niedojrza�y gorzki chleb",
        time = 135, // sekundy
        difficulty = 2, // 0 - 10
        instance = "ITPL_HK_HERB_46",
        amount = 1,
        staminaCost = 2,
        ground = 0,
    },
    {
        name = "Niedojrza�y gorzki chleb",
        time = 150, // sekundy
        difficulty = 2, // 0 - 10
        instance = "ITPL_HK_HERB_46",
        amount = 1,
        staminaCost = 2,
        ground = 0,
    },
	{
        name = "Gorzki chleb",
        time = 175, // sekundy
        difficulty = 2, // 0 - 10
        instance = "ITPL_HK_HERB_19",
        amount = 2,
        staminaCost = 2,
        ground = 0,
    }
]

Config["HerbSystem"]["Gorzki chleb 3"] <- [
    {
        name = "Nasiono gorzkiego chleba",
        time = 120, // sekundy
        difficulty = 3, // 0 - 10
        instance = "ITPL_HK_HERB_46",
        amount = 1,
        staminaCost = 2,
        ground = 0,
    },
    {
        name = "Niedojrza�y gorzki chleb",
        time = 150, // sekundy
        difficulty = 3, // 0 - 10
        instance = "ITPL_HK_HERB_46",
        amount = 1,
        staminaCost = 2,
        ground = 0,
    },
    {
        name = "Gorzki chleb",
        time = 175, // sekundy
        difficulty = 3, // 0 - 10
        instance = "ITPL_HK_HERB_19",
        amount = 1,
        staminaCost = 2,
        ground = 0,
    },
	{
        name = "Gorzki chleb",
        time = 220, // sekundy
        difficulty = 3, // 0 - 10
        instance = "ITPL_HK_HERB_19",
        amount = 3,
        staminaCost = 2,
        ground = 0,
    }
]

Config["HerbSystem"]["Niebieski bez"] <- [
    {
        name = "Nasiono niebieskiego bzu",
        time = 90, // sekundy
        difficulty = 1, // 0 - 10
        instance = "ITPL_HK_HERB_24",
        amount = 1,
        staminaCost = 2,
        ground = 1,
    },
    {
        name = "Niedojrza�y niebieski bez",
        time = 115, // sekundy
        difficulty = 1, // 0 - 10
        instance = "ITPL_WEED",
        amount = 1,
        staminaCost = 2,
        ground = 1,
    },
    {
        name = "Niedojrza�y niebieski bez",
        time = 135, // sekundy
        difficulty = 1, // 0 - 10
        instance = "ITPL_WEED",
        amount = 1,
        staminaCost = 2,
        ground = 1,
    },
	{
        name = "Niebieski bez",
        time = 165, // sekundy
        difficulty = 1, // 0 - 10
        instance = "ITPL_BLUEPLANT",
        amount = 1,
        staminaCost = 2,
        ground = 1,
    }
]

Config["HerbSystem"]["Niebieski bez 2"] <- [
    {
        name = "Nasiono niebieskiego bzu",
        time = 120, // sekundy
        difficulty = 2, // 0 - 10
        instance = "ITPL_HK_HERB_24",
        amount = 1,
        staminaCost = 20,
        ground = 1,
    },
    {
        name = "Niedojrza�y niebieski bez",
        time = 135, // sekundy
        difficulty = 2, // 0 - 10
        instance = "ITPL_WEED",
        amount = 1,
        staminaCost = 2,
        ground = 1,
    },
    {
        name = "Niedojrza�y niebieski bez",
        time = 150, // sekundy
        difficulty = 2, // 0 - 10
        instance = "ITPL_WEED",
        amount = 1,
        staminaCost = 2,
        ground = 1,
    },
	{
        name = "Niebieski bez",
        time = 175, // sekundy
        difficulty = 2, // 0 - 10
        instance = "ITPL_BLUEPLANT",
        amount = 2,
        staminaCost = 2,
        ground = 1,
    }
]

Config["HerbSystem"]["Niebieski bez 3"] <- [
    {
        name = "Nasiono niebieskiego bzu",
        time = 120, // sekundy
        difficulty = 3, // 0 - 10
        instance = "ITPL_HK_HERB_24",
        amount = 1,
        staminaCost = 2,
        ground = 1,
    },
    {
        name = "Niedojrza�y niebieski bez",
        time = 150, // sekundy
        difficulty = 3, // 0 - 10
        instance = "ITPL_WEED",
        amount = 1,
        staminaCost = 2,
        ground = 1,
    },
    {
        name = "Niebieski bez",
        time = 175, // sekundy
        difficulty = 3, // 0 - 10
        instance = "ITPL_BLUEPLANT",
        amount = 1,
        staminaCost = 2,
        ground = 1,
    },
	{
        name = "Niebieski bez",
        time = 220, // sekundy
        difficulty = 3, // 0 - 10
        instance = "ITPL_BLUEPLANT",
        amount = 3,
        staminaCost = 2,
        ground = 1,
    }
]

Config["HerbSystem"]["Le�ne jagody"] <- [
    {
        name = "Nasiono le�nej jagody",
        time = 90, // sekundy
        difficulty = 1, // 0 - 10
        instance = "ITPL_HK_HERB_23",
        amount = 1,
        staminaCost = 2,
        ground = 1,
    },
    {
        name = "Niedojrza�a le�na jagoda",
        time = 115, // sekundy
        difficulty = 1, // 0 - 10
        instance = "ITPL_WEED",
        amount = 1,
        staminaCost = 2,
        ground = 1,
    },
    {
        name = "Niedojrza�a le�na jagoda",
        time = 135, // sekundy
        difficulty = 1, // 0 - 10
        instance = "ITPL_FORESTBERRY",
        amount = 1,
        staminaCost = 2,
        ground = 1,
    },
	{
        name = "Le�ne jagody",
        time = 165, // sekundy
        difficulty = 1, // 0 - 10
        instance = "ITPL_FORESTBERRY",
        amount = 1,
        staminaCost = 2,
        ground = 1,
    }
]

Config["HerbSystem"]["Le�ne jagody 2"] <- [
    {
        name = "Nasiono le�nej jagody",
        time = 120, // sekundy
        difficulty = 2, // 0 - 10
        instance = "ITPL_HK_HERB_23",
        amount = 1,
        staminaCost = 20,
        ground = 1,
    },
    {
        name = "Niedojrza�a le�na jagoda",
        time = 135, // sekundy
        difficulty = 2, // 0 - 10
        instance = "ITPL_WEED",
        amount = 1,
        staminaCost = 2,
        ground = 1,
    },
    {
        name = "Niedojrza�a le�na jagoda",
        time = 150, // sekundy
        difficulty = 2, // 0 - 10
        instance = "ITPL_WEED",
        amount = 1,
        staminaCost = 2,
        ground = 1,
    },
	{
        name = "Le�ne jagody",
        time = 175, // sekundy
        difficulty = 2, // 0 - 10
        instance = "ITPL_FORESTBERRY",
        amount = 2,
        staminaCost = 2,
        ground = 1,
    }
]

Config["HerbSystem"]["Le�ne jagody 3"] <- [
    {
        name = "Nasiono le�nej jagody",
        time = 120, // sekundy
        difficulty = 3, // 0 - 10
        instance = "ITPL_HK_HERB_23",
        amount = 1,
        staminaCost = 2,
        ground = 1,
    },
    {
        name = "Niedojrza�a le�na jagoda",
        time = 150, // sekundy
        difficulty = 3, // 0 - 10
        instance = "ITPL_WEED",
        amount = 1,
        staminaCost = 2,
        ground = 1,
    },
    {
        name = "Le�ne jagody",
        time = 175, // sekundy
        difficulty = 3, // 0 - 10
        instance = "ITPL_FORESTBERRY",
        amount = 1,
        staminaCost = 2,
        ground = 1,
    },
	{
        name = "Le�ne jagody",
        time = 220, // sekundy
        difficulty = 3, // 0 - 10
        instance = "ITPL_FORESTBERRY",
        amount = 3,
        staminaCost = 2,
        ground = 1,
    }
]

Config["HerbSystem"]["Polne jagody"] <- [
    {
        name = "Nasiono polnej jagody",
        time = 90, // sekundy
        difficulty = 1, // 0 - 10
        instance = "ITPL_HK_HERB_28",
        amount = 1,
        staminaCost = 2,
        ground = 0,
    },
    {
        name = "Niedojrza�a polna jagoda",
        time = 115, // sekundy
        difficulty = 1, // 0 - 10
        instance = "ITPL_WEED",
        amount = 1,
        staminaCost = 2,
        ground = 0,
    },
    {
        name = "Niedojrza�a polna jagoda",
        time = 135, // sekundy
        difficulty = 1, // 0 - 10
        instance = "ITPL_WEED",
        amount = 1,
        staminaCost = 2,
        ground = 0,
    },
	{
        name = "Polne jagody",
        time = 165, // sekundy
        difficulty = 1, // 0 - 10
        instance = "ITPL_PLANEBERRY",
        amount = 1,
        staminaCost = 2,
        ground = 0,
    }
]

Config["HerbSystem"]["Polne jagody 2"] <- [
    {
        name = "Nasiono polnej jagody",
        time = 120, // sekundy
        difficulty = 2, // 0 - 10
        instance = "ITPL_HK_HERB_28",
        amount = 1,
        staminaCost = 20,
        ground = 0,
    },
    {
        name = "Niedojrza�a polna jagoda",
        time = 135, // sekundy
        difficulty = 2, // 0 - 10
        instance = "ITPL_WEED",
        amount = 1,
        staminaCost = 2,
        ground = 0,
    },
    {
        name = "Niedojrza�a polna jagoda",
        time = 150, // sekundy
        difficulty = 2, // 0 - 10
        instance = "ITPL_WEED",
        amount = 1,
        staminaCost = 2,
        ground = 0,
    },
	{
        name = "Polne jagody",
        time = 175, // sekundy
        difficulty = 2, // 0 - 10
        instance = "ITPL_PLANEBERRY",
        amount = 2,
        staminaCost = 2,
        ground = 0,
    }
]

Config["HerbSystem"]["Polne jagody 3"] <- [
    {
        name = "Nasiono polnej jagody",
        time = 120, // sekundy
        difficulty = 3, // 0 - 10
        instance = "ITPL_HK_HERB_28",
        amount = 1,
        staminaCost = 2,
        ground = 0,
    },
    {
        name = "Niedojrza�a polna jagoda",
        time = 150, // sekundy
        difficulty = 3, // 0 - 10
        instance = "ITPL_WEED",
        amount = 1,
        staminaCost = 2,
        ground = 0,
    },
    {
        name = "Polne jagody",
        time = 175, // sekundy
        difficulty = 3, // 0 - 10
        instance = "ITPL_PLANEBERRY",
        amount = 1,
        staminaCost = 2,
        ground = 0,
    },
	{
        name = "Polne jagody",
        time = 220, // sekundy
        difficulty = 3, // 0 - 10
        instance = "ITPL_PLANEBERRY",
        amount = 3,
        staminaCost = 2,
        ground = 0,
    }
]

Config["HerbSystem"]["Ziele lecznicze"] <- [
    {
        name = "Nasiono ziela leczniczego",
        time = 90, // sekundy
        difficulty = 2, // 0 - 10
        instance = "ITPL_HK_HERB_35",
        amount = 1,
        staminaCost = 20,
        ground = 0,
    },
    {
        name = "Niedojrza�e ziele lecznicze",
        time = 115, // sekundy
        difficulty = 2, // 0 - 10
        instance = "ITPL_WEED",
        amount = 1,
        staminaCost = 2,
        ground = 0,
    },
    {
        name = "Niedojrza�e ziele lecznicze",
        time = 135, // sekundy
        difficulty = 2, // 0 - 10
        instance = "ITPL_WEED",
        amount = 1,
        staminaCost = 2,
        ground = 0,
    },
	{
        name = "Ziele lecznicze",
        time = 165, // sekundy
        difficulty = 2, // 0 - 10
        instance = "ITPL_HK_HERB_10",
        amount = 1,
        staminaCost = 2,
        ground = 0,
    }
]

Config["HerbSystem"]["Ziele lecznicze 2"] <- [
    {
        name = "Nasiono ziela leczniczego",
        time = 120, // sekundy
        difficulty = 3, // 0 - 10
        instance = "ITPL_HK_HERB_35",
        amount = 1,
        staminaCost = 2,
        ground = 0,
    },
    {
        name = "Niedojrza�e ziele lecznicze",
        time = 135, // sekundy
        difficulty = 3, // 0 - 10
        instance = "ITPL_WEED",
        amount = 1,
        staminaCost = 2,
        ground = 0,
    },
    {
        name = "Niedojrza�e ziele lecznicze",
        time = 150, // sekundy
        difficulty = 3, // 0 - 10
        instance = "ITPL_WEED",
        amount = 1,
        staminaCost = 2,
        ground = 0,
    },
	{
        name = "Ziele lecznicze",
        time = 175, // sekundy
        difficulty = 3, // 0 - 10
        instance = "ITPL_HK_HERB_10",
        amount = 2,
        staminaCost = 2,
        ground = 0,
    }
]

Config["HerbSystem"]["Ziele lecznicze 3"] <- [
    {
        name = "Nasiono ziela leczniczego",
        time = 120, // sekundy
        difficulty = 3, // 0 - 10
        instance = "ITPL_HK_HERB_35",
        amount = 1,
        staminaCost = 20,
        ground = 0,
    },
    {
        name = "Niedojrza�e ziele lecznicze",
        time = 150, // sekundy
        difficulty = 3, // 0 - 10
        instance = "ITPL_WEED",
        amount = 1,
        staminaCost = 2,
        ground = 0,
    },
    {
        name = "Ziele lecznicze",
        time = 175, // sekundy
        difficulty = 3, // 0 - 10
        instance = "ITPL_HK_HERB_10",
        amount = 1,
        staminaCost = 2,
        ground = 0,
    },
	{
        name = "Ziele lecznicze",
        time = 220, // sekundy
        difficulty = 3, // 0 - 10
        instance = "ITPL_HK_HERB_10",
        amount = 3,
        staminaCost = 2,
        ground = 0,
    }
]

Config["HerbSystem"]["Ogniste ziele"] <- [
    {
        name = "Nasiono ognistego ziela",
        time = 90, // sekundy
        difficulty = 2, // 0 - 10
        instance = "ITPL_HK_HERB_26",
        amount = 1,
        staminaCost = 20,
        ground = 1,
    },
    {
        name = "Niedojrza�e ogniste ziele",
        time = 115, // sekundy
        difficulty = 2, // 0 - 10
        instance = "ITPL_WEED",
        amount = 1,
        staminaCost = 2,
        ground = 1,
    },
    {
        name = "Niedojrza�e ogniste ziele",
        time = 135, // sekundy
        difficulty = 2, // 0 - 10
        instance = "ITPL_WEED",
        amount = 1,
        staminaCost = 2,
        ground = 1,
    },
	{
        name = "Ogniste ziele",
        time = 165, // sekundy
        difficulty = 2, // 0 - 10
        instance = "ITPL_MANA_HERB_02",
        amount = 1,
        staminaCost = 2,
        ground = 1,
    }
]

Config["HerbSystem"]["Ogniste ziele 2"] <- [
    {
        name = "Nasiono ognistego ziela",
        time = 120, // sekundy
        difficulty = 3, // 0 - 10
        instance = "ITPL_HK_HERB_26",
        amount = 1,
        staminaCost = 2,
        ground = 1,
    },
    {
        name = "Niedojrza�e ogniste ziele",
        time = 135, // sekundy
        difficulty = 3, // 0 - 10
        instance = "ITPL_WEED",
        amount = 1,
        staminaCost = 2,
        ground = 1,
    },
    {
        name = "Niedojrza�e ogniste ziele",
        time = 150, // sekundy
        difficulty = 3, // 0 - 10
        instance = "ITPL_WEED",
        amount = 2,
        staminaCost = 2,
        ground = 1,
    },
	{
        name = "Ogniste ziele",
        time = 175, // sekundy
        difficulty = 3, // 0 - 10
        instance = "ITPL_MANA_HERB_02",
        amount = 2,
        staminaCost = 2,
        ground = 1,
    }
]

Config["HerbSystem"]["Ogniste ziele 3"] <- [
    {
        name = "Nasiono ognistego ziela",
        time = 120, // sekundy
        difficulty = 3, // 0 - 10
        instance = "ITPL_HK_HERB_26",
        amount = 1,
        staminaCost = 20,
        ground = 1,
    },
    {
        name = "Niedojrza�e ogniste ziele",
        time = 150, // sekundy
        difficulty = 3, // 0 - 10
        instance = "ITPL_WEED",
        amount = 1,
        staminaCost = 2,
        ground = 1,
    },
    {
        name = "Niedojrza�e ogniste ziele",
        time = 175, // sekundy
        difficulty = 3, // 0 - 10
        instance = "ITPL_MANA_HERB_02",
        amount = 1,
        staminaCost = 2,
        ground = 1,
    },
	{
        name = "Ogniste ziele",
        time = 220, // sekundy
        difficulty = 3, // 0 - 10
        instance = "ITPL_MANA_HERB_02",
        amount = 3,
        staminaCost = 2,
        ground = 1,
    }
]

Config["HerbSystem"]["Czarne ziele"] <- [
    {
        name = "Nasiono czarnego ziela",
        time = 90, // sekundy
        difficulty = 3, // 0 - 10
        instance = "ITPL_HK_HERB_48",
        amount = 1,
        staminaCost = 20,
        ground = 2,
    },
    {
        name = "Niedojrza�e czarne ziele",
        time = 115, // sekundy
        difficulty = 3, // 0 - 10
        instance = "ITPL_WEED",
        amount = 1,
        staminaCost = 2,
        ground = 2,
    },
    {
        name = "Niedojrza�e czarne ziele",
        time = 135, // sekundy
        difficulty = 3, // 0 - 10
        instance = "ITPL_WEED",
        amount = 1,
        staminaCost = 2,
        ground = 2,
    },
	{
        name = "Czarne ziele",
        time = 165, // sekundy
        difficulty = 3, // 0 - 10
        instance = "ITPL_HK_HERB_13",
        amount = 1,
        staminaCost = 2,
        ground = 2,
    }
]

Config["HerbSystem"]["Czarne ziele 2"] <- [
    {
        name = "Nasiono czarnego ziela",
        time = 120, // sekundy
        difficulty = 3, // 0 - 10
        instance = "ITPL_HK_HERB_48",
        amount = 1,
        staminaCost = 2,
        ground = 2,
    },
    {
        name = "Niedojrza�e czarne ziele",
        time = 135, // sekundy
        difficulty = 3, // 0 - 10
        instance = "ITPL_WEED",
        amount = 1,
        staminaCost = 2,
        ground = 2,
    },
    {
        name = "Niedojrza�e czarne ziele",
        time = 150, // sekundy
        difficulty = 3, // 0 - 10
        instance = "ITPL_WEED",
        amount = 1,
        staminaCost = 2,
        ground = 2,
    },
	{
        name = "Czarne ziele",
        time = 175, // sekundy
        difficulty = 3, // 0 - 10
        instance = "ITPL_HK_HERB_13",
        amount = 2,
        staminaCost = 2,
        ground = 2,
    }
]

Config["HerbSystem"]["Czarne ziele 3"] <- [
    {
        name = "Nasiono czarnego ziela",
        time = 120, // sekundy
        difficulty = 3, // 0 - 10
        instance = "ITPL_HK_HERB_48",
        amount = 1,
        staminaCost = 20,
        ground = 2,
    },
    {
        name = "Niedojrza�e czarne ziele",
        time = 150, // sekundy
        difficulty = 3, // 0 - 10
        instance = "ITPL_WEED",
        amount = 1,
        staminaCost = 2,
        ground = 2,
    },
    {
        name = "Czarne ziele",
        time = 175, // sekundy
        difficulty = 3, // 0 - 10
        instance = "ITPL_HK_HERB_13",
        amount = 1,
        staminaCost = 2,
        ground = 2,
    },
	{
        name = "Czarne ziele",
        time = 220, // sekundy
        difficulty = 3, // 0 - 10
        instance = "ITPL_HK_HERB_13",
        amount = 3,
        staminaCost = 2,
        ground = 2,
    }
]

Config["HerbSystem"]["Twardzie�"] <- [
    {
        name = "Nasiono twardzienia",
        time = 90, // sekundy
        difficulty = 1, // 0 - 10
        instance = "ITPL_HK_HERB_56",
        amount = 1,
        staminaCost = 20,
        ground = 3,
    },
    {
        name = "Niedojrza�y twardzie�",
        time = 115, // sekundy
        difficulty = 1, // 0 - 10
        instance = "ITPL_WEED",
        amount = 1,
        staminaCost = 2,
        ground = 3,
    },
    {
        name = "Niedojrza�y twardzie�",
        time = 135, // sekundy
        difficulty = 1, // 0 - 10
        instance = "ITPL_WEED",
        amount = 1,
        staminaCost = 2,
        ground = 3,
    },
	{
        name = "Twardzie�",
        time = 165, // sekundy
        difficulty = 1, // 0 - 10
        instance = "ITPL_HK_HERB_14",
        amount = 1,
        staminaCost = 2,
        ground = 3,
    }
]

Config["HerbSystem"]["Twardzie� 2"] <- [
    {
        name = "Nasiono twardzienia",
        time = 120, // sekundy
        difficulty = 2, // 0 - 10
        instance = "ITPL_HK_HERB_56",
        amount = 1,
        staminaCost = 2,
        ground = 3,
    },
    {
        name = "Niedojrza�y twardzie�",
        time = 135, // sekundy
        difficulty = 2, // 0 - 10
        instance = "ITPL_WEED",
        amount = 1,
        staminaCost = 2,
        ground = 3,
    },
    {
        name = "Niedojrza�y twardzie�",
        time = 150, // sekundy
        difficulty = 2, // 0 - 10
        instance = "ITPL_WEED",
        amount = 1,
        staminaCost = 2,
        ground = 3,
    },
	{
        name = "Twardzie�",
        time = 175, // sekundy
        difficulty = 2, // 0 - 10
        instance = "ITPL_HK_HERB_14",
        amount = 2,
        staminaCost = 2,
        ground = 3,
    }
]

Config["HerbSystem"]["Twardzie� 3"] <- [
    {
        name = "Nasiono twardzienia",
        time = 120, // sekundy
        difficulty = 3, // 0 - 10
        instance = "ITPL_HK_HERB_56",
        amount = 1,
        staminaCost = 20,
        ground = 3,
    },
    {
        name = "Niedojrza�y twardzie�",
        time = 150, // sekundy
        difficulty = 3, // 0 - 10
        instance = "ITPL_WEED",
        amount = 1,
        staminaCost = 2,
        ground = 3,
    },
    {
        name = "Twardzie�",
        time = 175, // sekundy
        difficulty = 3, // 0 - 10
        instance = "ITPL_HK_HERB_14",
        amount = 1,
        staminaCost = 2,
        ground = 3,
    },
	{
        name = "Twardzie�",
        time = 220, // sekundy
        difficulty = 3, // 0 - 10
        instance = "ITPL_HK_HERB_14",
        amount = 3,
        staminaCost = 2,
        ground = 3,
    }
]

Config["HerbSystem"]["Serafis"] <- [
    {
        name = "Nasiono serafisu",
        time = 90, // sekundy
        difficulty = 1, // 0 - 10
        instance = "ITPL_HK_HERB_37",
        amount = 1,
        staminaCost = 20,
        ground = 1,
    },
    {
        name = "Niedojrza�y serafis",
        time = 115, // sekundy
        difficulty = 1, // 0 - 10
        instance = "ITPL_WEED",
        amount = 1,
        staminaCost = 2,
        ground = 1,
    },
    {
        name = "Niedojrza�y serafis",
        time = 135, // sekundy
        difficulty = 1, // 0 - 10
        instance = "ITPL_HK_HERB_01",
        amount = 1,
        staminaCost = 2,
        ground = 1,
    },
	{
        name = "Serafis",
        time = 165, // sekundy
        difficulty = 1, // 0 - 10
        instance = "ITPL_HK_HERB_01",
        amount = 1,
        staminaCost = 2,
        ground = 1,
    }
]

Config["HerbSystem"]["Serafis 2"] <- [
    {
        name = "Nasiono serafisu",
        time = 120, // sekundy
        difficulty = 2, // 0 - 10
        instance = "ITPL_HK_HERB_37",
        amount = 1,
        staminaCost = 2,
        ground = 1,
    },
    {
        name = "Niedojrza�y serafis",
        time = 135, // sekundy
        difficulty = 2, // 0 - 10
        instance = "ITPL_WEED",
        amount = 1,
        staminaCost = 2,
        ground = 1,
    },
    {
        name = "Niedojrza�y serafis",
        time = 150, // sekundy
        difficulty = 2, // 0 - 10
        instance = "ITPL_WEED",
        amount = 1,
        staminaCost = 2,
        ground = 1,
    },
	{
        name = "Serafis",
        time = 175, // sekundy
        difficulty = 2, // 0 - 10
        instance = "ITPL_HK_HERB_01",
        amount = 2,
        staminaCost = 2,
        ground = 1,
    }
]

Config["HerbSystem"]["Serafis 3"] <- [
    {
        name = "Nasiono serafisu",
        time = 120, // sekundy
        difficulty = 3, // 0 - 10
        instance = "ITPL_HK_HERB_37",
        amount = 1,
        staminaCost = 20,
        ground = 1,
    },
    {
        name = "Niedojrza�y serafis",
        time = 150, // sekundy
        difficulty = 3, // 0 - 10
        instance = "ITPL_WEED",
        amount = 1,
        staminaCost = 2,
        ground = 1,
    },
    {
        name = "Serafis",
        time = 175, // sekundy
        difficulty = 3, // 0 - 10
        instance = "ITPL_HK_HERB_01",
        amount = 1,
        staminaCost = 2,
        ground = 1,
    },
	{
        name = "Serafis",
        time = 175, // sekundy
        difficulty = 3, // 0 - 10
        instance = "ITPL_HK_HERB_01",
        amount = 3,
        staminaCost = 2,
        ground = 1,
    }
]

Config["HerbSystem"]["Rzepa"] <- [
    {
        name = "Nasiono rzepy",
        time = 90, // sekundy
        difficulty = 2, // 0 - 10
        instance = "ITPL_HK_HERB_31",
        amount = 1,
        staminaCost = 20,
        ground = 4,
    },
    {
        name = "Niedojrza�a rzepa",
        time = 115, // sekundy
        difficulty = 2, // 0 - 10
        instance = "ITPL_HK_HERB_58",
        amount = 1,
        staminaCost = 2,
        ground = 4,
    },
    {
        name = "Niedojrza�a rzepa",
        time = 135, // sekundy
        difficulty = 2, // 0 - 10
        instance = "ITPL_BEET",
        amount = 1,
        staminaCost = 2,
        ground = 4,
    },
	{
        name = "Rzepa",
        time = 165, // sekundy
        difficulty = 2, // 0 - 10
        instance = "ITPL_BEET",
        amount = 1,
        staminaCost = 2,
        ground = 4,
    }
]

Config["HerbSystem"]["Rzepa 2"] <- [
    {
        name = "Nasiono rzepy",
        time = 120, // sekundy
        difficulty = 3, // 0 - 10
        instance = "ITPL_HK_HERB_31",
        amount = 1,
        staminaCost = 2,
        ground = 4,
    },
    {
        name = "Niedojrza�a rzepa",
        time = 135, // sekundy
        difficulty = 3, // 0 - 10
        instance = "ITPL_HK_HERB_58",
        amount = 1,
        staminaCost = 2,
        ground = 4,
    },
    {
        name = "Niedojrza�a rzepa",
        time = 150, // sekundy
        difficulty = 3, // 0 - 10
        instance = "ITPL_HK_HERB_58"
        amount = 1,
        staminaCost = 2,
        ground = 4,
    },
	{
        name = "Rzepa",
        time = 175, // sekundy
        difficulty = 3, // 0 - 10
        instance = "ITPL_BEET",
        amount = 2,
        staminaCost = 2,
        ground = 4,
    }
]

Config["HerbSystem"]["Rzepa 3"] <- [
    {
        name = "Nasiono rzepy",
        time = 120, // sekundy
        difficulty = 3, // 0 - 10
        instance = "ITPL_HK_HERB_31",
        amount = 1,
        staminaCost = 20,
        ground = 4,
    },
    {
        name = "Niedojrza�a rzepa",
        time = 150, // sekundy
        difficulty = 3, // 0 - 10
        instance = "ITPL_HK_HERB_58",
        amount = 1,
        staminaCost = 2,
        ground = 4,
    },
    {
        name = "Rzepa",
        time = 175, // sekundy
        difficulty = 3, // 0 - 10
        instance = "ITPL_BEET",
        amount = 1,
        staminaCost = 2,
        ground = 4,
    },
	{
        name = "Rzepa",
        time = 220, // sekundy
        difficulty = 3, // 0 - 10
        instance = "ITPL_BEET",
        amount = 3,
        staminaCost = 2,
        ground = 4,
    }
]

Config["HerbSystem"]["Ryz 1"] <- [
    {
        name = "Nasiono ry�u",
        time = 90, // sekundy
        difficulty = 1, // 0 - 10
        instance = "ITPL_HK_HERB_72",
        amount = 1,
        staminaCost = 20,
        ground = 4,
    },
    {
        name = "Niedojrza�y ry�",
        time = 115, // sekundy
        difficulty = 1, // 0 - 10
        instance = "ITPL_WEED",
        amount = 1,
        staminaCost = 2,
        ground = 4,
    },
    {
        name = "Niedojrza�y ry�",
        time = 135, // sekundy
        difficulty = 1, // 0 - 10
        instance = "ITPL_WEED",
        amount = 1,
        staminaCost = 2,
        ground = 4,
    },
	{
        name = "Ry�",
        time = 165, // sekundy
        difficulty = 1, // 0 - 10
        instance = "ITPL_HK_HERB_73",
        amount = 1,
        staminaCost = 2,
        ground = 4,
    }
]

Config["HerbSystem"]["Ryz 2"] <- [
    {
        name = "Nasiono ry�u",
        time = 120, // sekundy
        difficulty = 2, // 0 - 10
        instance = "ITPL_HK_HERB_72",
        amount = 1,
        staminaCost = 20,
        ground = 4,
    },
    {
        name = "Niedojrza�y ry�",
        time = 135, // sekundy
        difficulty = 2, // 0 - 10
        instance = "ITPL_WEED",
        amount = 1,
        staminaCost = 2,
        ground = 4,
    },
    {
        name = "Niedojrza�y ry�",
        time = 150, // sekundy
        difficulty = 2, // 0 - 10
        instance = "ITPL_WEED",
        amount = 1,
        staminaCost = 2,
        ground = 4,
    },
	{
        name = "Ry�",
        time = 175, // sekundy
        difficulty = 2, // 0 - 10
        instance = "ITPL_HK_HERB_73",
        amount = 2,
        staminaCost = 2,
        ground = 4,
    }
]

Config["HerbSystem"]["Ryz 3"] <- [
    {
        name = "Nasiono ry�u",
        time = 120, // sekundy
        difficulty = 3, // 0 - 10
        instance = "ITPL_HK_HERB_72",
        amount = 1,
        staminaCost = 20,
        ground = 4,
    },
    {
        name = "Niedojrza�y ry�",
        time = 150, // sekundy
        difficulty = 3, // 0 - 10
        instance = "ITPL_WEED",
        amount = 1,
        staminaCost = 2,
        ground = 4,
    },
    {
        name = "Ry�",
        time = 175, // sekundy
        difficulty = 3, // 0 - 10
        instance = "ITPL_HK_HERB_73",
        amount = 1,
        staminaCost = 2,
        ground = 4,
    },
	{
        name = "Ry�",
        time = 220, // sekundy
        difficulty = 3, // 0 - 10
        instance = "ITPL_HK_HERB_73",
        amount = 3,
        staminaCost = 2,
        ground = 4,
    }
]

Config["HerbSystem"]["Korze� leczniczy"] <- [
    {
        name = "Nasiono korzenia leczniczego",
        time = 90, // sekundy
        difficulty = 1, // 0 - 10
        instance = "ITPL_HK_HERB_22",
        amount = 1,
        staminaCost = 2,
        ground = 1,
    },
    {
        name = "Niedojrza�y korze� leczniczy",
        time = 115, // sekundy
        difficulty = 1, // 0 - 10
        instance = "ITPL_WEED",
        amount = 1,
        staminaCost = 2,
        ground = 1,
    },
    {
        name = "Niedojrza�y korze� leczniczy",
        time = 135, // sekundy
        difficulty = 1, // 0 - 10
        instance = "ITPL_WEED",
        amount = 1,
        staminaCost = 2,
        ground = 1,
    },
	{
        name = "Korze� leczniczy",
        time = 165, // sekundy
        difficulty = 1, // 0 - 10
        instance = "ITPL_HK_HERB_11",
        amount = 1,
        staminaCost = 2,
        ground = 1,
    }
]

Config["HerbSystem"]["Korze� leczniczy 2"] <- [
    {
        name = "Nasiono korzenia leczniczego",
        time = 120, // sekundy
        difficulty = 2, // 0 - 10
        instance = "ITPL_HK_HERB_22",
        amount = 1,
        staminaCost = 20,
        ground = 1,
    },
    {
        name = "Niedojrza�y korze� leczniczy",
        time = 135, // sekundy
        difficulty = 2, // 0 - 10
        instance = "ITPL_WEED",
        amount = 1,
        staminaCost = 2,
        ground = 1,
    },
    {
        name = "Niedojrza�y korze� leczniczy",
        time = 150, // sekundy
        difficulty = 2, // 0 - 10
        instance = "ITPL_WEED",
        amount = 1,
        staminaCost = 2,
        ground = 1,
    },
	{
        name = "Korze� leczniczy",
        time = 175, // sekundy
        difficulty = 2, // 0 - 10
        instance = "ITPL_HK_HERB_11",
        amount = 2,
        staminaCost = 2,
        ground = 1,
    }
]

Config["HerbSystem"]["Korze� leczniczy 3"] <- [
    {
        name = "Nasiono korzenia leczniczego",
        time = 120, // sekundy
        difficulty = 3, // 0 - 10
        instance = "ITPL_HK_HERB_22",
        amount = 1,
        staminaCost = 2,
        ground = 1,
    },
    {
        name = "Niedojrza�y korze� leczniczy",
        time = 150, // sekundy
        difficulty = 3, // 0 - 10
        instance = "ITPL_WEED",
        amount = 1,
        staminaCost = 2,
        ground = 1,
    },
    {
        name = "Korze� leczniczy",
        time = 175, // sekundy
        difficulty = 3, // 0 - 10
        instance = "ITPL_HK_HERB_11",
        amount = 1,
        staminaCost = 2,
        ground = 1,
    },
	{
        name = "Korze� leczniczy",
        time = 220, // sekundy
        difficulty = 3, // 0 - 10
        instance = "ITPL_HK_HERB_11",
        amount = 3,
        staminaCost = 2,
        ground = 1,
    }
]

Config["HerbSystem"]["Ognisty korze�"] <- [
    {
        name = "Nasiono ognistego korzenia",
        time = 90, // sekundy
        difficulty = 3, // 0 - 10
        instance = "ITPL_HK_HERB_27",
        amount = 1,
        staminaCost = 2,
        ground = 1,
    },
    {
        name = "Niedojrza�y ognisty korze�",
        time = 115, // sekundy
        difficulty = 3, // 0 - 10
        instance = "ITPL_WEED",
        amount = 1,
        staminaCost = 2,
        ground = 1,
    },
    {
        name = "Niedojrza�y ognisty korze�",
        time = 135, // sekundy
        difficulty = 3, // 0 - 10
        instance = "ITPL_WEED",
        amount = 1,
        staminaCost = 2,
        ground = 1,
    },
	{
        name = "Ognisty korze�",
        time = 165, // sekundy
        difficulty = 3, // 0 - 10
        instance = "ITPL_MANA_HERB_03",
        amount = 1,
        staminaCost = 2,
        ground = 1,
    }
]

Config["HerbSystem"]["Ognisty korze� 2"] <- [
    {
        name = "Nasiono ognistego korzenia",
        time = 120, // sekundy
        difficulty = 3, // 0 - 10
        instance = "ITPL_HK_HERB_27",
        amount = 1,
        staminaCost = 20,
        ground = 1,
    },
    {
        name = "Niedojrza�y ognisty korze�",
        time = 135, // sekundy
        difficulty = 3, // 0 - 10
        instance = "ITPL_WEED",
        amount = 1,
        staminaCost = 2,
        ground = 1,
    },
    {
        name = "Niedojrza�y ognisty korze�",
        time = 150, // sekundy
        difficulty = 3, // 0 - 10
        instance = "ITPL_WEED",
        amount = 1,
        staminaCost = 2,
        ground = 1,
    },
	{
        name = "Ognisty korze�",
        time = 175, // sekundy
        difficulty = 3, // 0 - 10
        instance = "ITPL_MANA_HERB_03",
        amount = 2,
        staminaCost = 2,
        ground = 1,
    }
]

Config["HerbSystem"]["Ognisty korze� 3"] <- [
    {
        name = "Nasiono ognistego korzenia",
        time = 120, // sekundy
        difficulty = 3, // 0 - 10
        instance = "ITPL_HK_HERB_27",
        amount = 1,
        staminaCost = 2,
        ground = 1,
    },
    {
        name = "Niedojrza�y ognisty korze�",
        time = 150, // sekundy
        difficulty = 3, // 0 - 10
        instance = "ITPL_WEED",
        amount = 1,
        staminaCost = 2,
        ground = 1,
    },
    {
        name = "Ognisty korze�",
        time = 175, // sekundy
        difficulty = 3, // 0 - 10
        instance = "ITPL_MANA_HERB_03",
        amount = 1,
        staminaCost = 2,
        ground = 1,
    },
	{
        name = "Ognisty korze�",
        time = 220, // sekundy
        difficulty = 3, // 0 - 10
        instance = "ITPL_MANA_HERB_03",
        amount = 3,
        staminaCost = 2,
        ground = 1,
    }
]

Config["HerbSystem"]["Szczaw kr�lewski"] <- [
    {
        name = "Nasiono szczawu kr�lewskiego",
        time = 90, // sekundy
        difficulty = 3, // 0 - 10
        instance = "ITPL_HK_HERB_33",
        amount = 1,
        staminaCost = 2,
        ground = 0,
    },
    {
        name = "Niedojrza�y szczaw kr�lewski",
        time = 115, // sekundy
        difficulty = 3, // 0 - 10
        instance = "ITPL_WEED",
        amount = 1,
        staminaCost = 2,
        ground = 0,
    },
    {
        name = "Niedojrza�y szczaw kr�lewski",
        time = 135, // sekundy
        difficulty = 3, // 0 - 10
        instance = "ITPL_WEED",
        amount = 1,
        staminaCost = 2,
        ground = 0,
    },
	{
        name = "Szczaw kr�lewski",
        time = 165, // sekundy
        difficulty = 3, // 0 - 10
        instance = "ITPL_PERM_HERB",
        amount = 1,
        staminaCost = 2,
        ground = 0,
    }
]

Config["HerbSystem"]["Szczaw kr�lewski 2"] <- [
    {
        name = "Nasiono szczawu kr�lewskiego",
        time = 120, // sekundy
        difficulty = 3, // 0 - 10
        instance = "ITPL_HK_HERB_33",
        amount = 1,
        staminaCost = 20,
        ground = 0,
    },
    {
        name = "Niedojrza�y szczaw kr�lewski",
        time = 135, // sekundy
        difficulty = 3, // 0 - 10
        instance = "ITPL_WEED",
        amount = 1,
        staminaCost = 2,
        ground = 0,
    },
    {
        name = "Niedojrza�y szczaw kr�lewski",
        time = 150, // sekundy
        difficulty = 3, // 0 - 10
        instance = "ITPL_WEED",
        amount = 1,
        staminaCost = 2,
        ground = 0,
    },
	{
        name = "Szczaw kr�lewski",
        time = 175, // sekundy
        difficulty = 3, // 0 - 10
        instance = "ITPL_PERM_HERB",
        amount = 2,
        staminaCost = 2,
        ground = 0,
    }
]

Config["HerbSystem"]["Szczaw kr�lewski 3"] <- [
    {
        name = "Nasiono szczawu kr�lewskiego",
        time = 120, // sekundy
        difficulty = 3, // 0 - 10
        instance = "ITPL_HK_HERB_33",
        amount = 1,
        staminaCost = 2,
        ground = 0,
    },
    {
        name = "Niedojrza�y szczaw kr�lewski",
        time = 150, // sekundy
        difficulty = 3, // 0 - 10
        instance = "ITPL_WEED",
        amount = 1,
        staminaCost = 2,
        ground = 0,
    },
    {
        name = "Szczaw kr�lewski",
        time = 175, // sekundy
        difficulty = 3, // 0 - 10
        instance = "ITPL_PERM_HERB",
        amount = 1,
        staminaCost = 2,
        ground = 0,
    },
	{
        name = "Szczaw kr�lewski",
        time = 220, // sekundy
        difficulty = 3, // 0 - 10
        instance = "ITPL_PERM_HERB",
        amount = 3,
        staminaCost = 2,
        ground = 0,
    }
]

Config["HerbSystem"]["Smoczy korze�"] <- [
    {
        name = "Nasiono smoczego korzenia",
        time = 90, // sekundy
        difficulty = 2, // 0 - 10
        instance = "ITPL_HK_HERB_32",
        amount = 1,
        staminaCost = 2,
        ground = 2,
    },
    {
        name = "Niedojrza�y smoczy korze�",
        time = 115, // sekundy
        difficulty = 2, // 0 - 10
        instance = "ITPL_WEED",
        amount = 1,
        staminaCost = 2,
        ground = 2,
    },
    {
        name = "Niedojrza�y smoczy korze�",
        time = 135, // sekundy
        difficulty = 2, // 0 - 10
        instance = "ITPL_WEED",
        amount = 1,
        staminaCost = 2,
        ground = 2,
    },
	{
        name = "Smoczy korze�",
        time = 165, // sekundy
        difficulty = 2, // 0 - 10
        instance = "ITPL_STRENGTH_HERB_01",
        amount = 1,
        staminaCost = 2,
        ground = 2,
    }
]

Config["HerbSystem"]["Smoczy korze� 2"] <- [
    {
        name = "Nasiono smoczego korzenia",
        time = 120, // sekundy
        difficulty = 3, // 0 - 10
        instance = "ITPL_HK_HERB_32",
        amount = 1,
        staminaCost = 20,
        ground = 2,
    },
    {
        name = "Niedojrza�y smoczy korze�",
        time = 135, // sekundy
        difficulty = 3, // 0 - 10
        instance = "ITPL_WEED",
        amount = 1,
        staminaCost = 2,
        ground = 2,
    },
    {
        name = "Niedojrza�y smoczy korze�",
        time = 150, // sekundy
        difficulty = 3, // 0 - 10
        instance = "ITPL_WEED",
        amount = 2,
        staminaCost = 2,
        ground = 2,
    },
	{
        name = "Smoczy korze�",
        time = 175, // sekundy
        difficulty = 3, // 0 - 10
        instance = "ITPL_STRENGTH_HERB_01",
        amount = 2,
        staminaCost = 2,
        ground = 2,
    }
]

Config["HerbSystem"]["Smoczy korze� 3"] <- [
    {
        name = "Nasiono smoczego korzenia",
        time = 120, // sekundy
        difficulty = 3, // 0 - 10
        instance = "ITPL_HK_HERB_32",
        amount = 1,
        staminaCost = 2,
        ground = 2,
    },
    {
        name = "Niedojrza�y smoczy korze�",
        time = 150, // sekundy
        difficulty = 3, // 0 - 10
        instance = "ITPL_WEED",
        amount = 1,
        staminaCost = 2,
        ground = 2,
    },
    {
        name = "Smoczy korze�",
        time = 175, // sekundy
        difficulty = 3, // 0 - 10
        instance = "ITPL_STRENGTH_HERB_01",
        amount = 1,
        staminaCost = 2,
        ground = 2,
    },
	{
        name = "Smoczy korze�",
        time = 220, // sekundy
        difficulty = 3, // 0 - 10
        instance = "ITPL_STRENGTH_HERB_01",
        amount = 3,
        staminaCost = 2,
        ground = 2,
    }
]

Config["HerbSystem"]["Orkowe ziele"] <- [
    {
        name = "Nasiono orkowego ziela",
        time = 90, // sekundy
        difficulty = 3, // 0 - 10
        instance = "ITPL_HK_HERB_43",
        amount = 1,
        staminaCost = 2,
        ground = 2,
    },
    {
        name = "Niedojrza�e orkowe ziele",
        time = 115, // sekundy
        difficulty = 3, // 0 - 10
        instance = "ITPL_WEED",
        amount = 1,
        staminaCost = 2,
        ground = 2,
    },
    {
        name = "Niedojrza�e orkowe ziele",
        time = 135, // sekundy
        difficulty = 3, // 0 - 10
        instance = "ITPL_WEED",
        amount = 1,
        staminaCost = 2,
        ground = 2,
    },
	{
        name = "Orkowe ziele",
        time = 165, // sekundy
        difficulty = 3, // 0 - 10
        instance = "ITPL_HK_HERB_07",
        amount = 1,
        staminaCost = 2,
        ground = 2,
    }
]

Config["HerbSystem"]["Orkowe ziele 2"] <- [
    {
        name = "Nasiono orkowego ziela",
        time = 120, // sekundy
        difficulty = 3, // 0 - 10
        instance = "ITPL_HK_HERB_43",
        amount = 1,
        staminaCost = 20,
        ground = 2,
    },
    {
        name = "Niedojrza�e orkowe ziele",
        time = 135, // sekundy
        difficulty = 3, // 0 - 10
        instance = "ITPL_WEED",
        amount = 1,
        staminaCost = 2,
        ground = 2,
    },
    {
        name = "Niedojrza�e orkowe ziele",
        time = 150, // sekundy
        difficulty = 3, // 0 - 10
        instance = "ITPL_WEED",
        amount = 1,
        staminaCost = 2,
        ground = 2,
    },
	{
        name = "Orkowe ziele",
        time = 175, // sekundy
        difficulty = 3, // 0 - 10
        instance = "ITPL_HK_HERB_07",
        amount = 2,
        staminaCost = 2,
        ground = 2,
    }
]

Config["HerbSystem"]["Orkowe ziele 3"] <- [
    {
        name = "Nasiono orkowego ziela",
        time = 120, // sekundy
        difficulty = 3, // 0 - 10
        instance = "ITPL_HK_HERB_43",
        amount = 1,
        staminaCost = 2,
        ground = 2,
    },
    {
        name = "Niedojrza�e orkowe ziele",
        time = 150, // sekundy
        difficulty = 3, // 0 - 10
        instance = "ITPL_WEED",
        amount = 1,
        staminaCost = 2,
        ground = 2,
    },
    {
        name = "Orkowe ziele",
        time = 175, // sekundy
        difficulty = 3, // 0 - 10
        instance = "ITPL_HK_HERB_07",
        amount = 1,
        staminaCost = 2,
        ground = 2,
    },
	{
        name = "Orkowe ziele",
        time = 220, // sekundy
        difficulty = 3, // 0 - 10
        instance = "ITPL_HK_HERB_07",
        amount = 3,
        staminaCost = 2,
        ground = 2,
    }
]

Config["HerbSystem"]["Psianka"] <- [
    {
        name = "Nasiono psianki",
        time = 90, // sekundy
        difficulty = 1, // 0 - 10
        instance = "ITPL_HK_HERB_41",
        amount = 1,
        staminaCost = 2,
        ground = 0,
    },
    {
        name = "Niedojrza�a psianka",
        time = 115, // sekundy
        difficulty = 1, // 0 - 10
        instance = "ITPL_WEED",
        amount = 1,
        staminaCost = 2,
        ground = 0,
    },
    {
        name = "Niedojrza�a psianka",
        time = 135, // sekundy
        difficulty = 1, // 0 - 10
        instance = "ITPL_WEED",
        amount = 1,
        staminaCost = 2,
        ground = 0,
    },
	{
        name = "Psianka",
        time = 165, // sekundy
        difficulty = 1, // 0 - 10
        instance = "ITPL_HK_HERB_05",
        amount = 1,
        staminaCost = 2,
        ground = 0,
    }
]

Config["HerbSystem"]["Psianka 2"] <- [
    {
        name = "Nasiono psianki",
        time = 120, // sekundy
        difficulty = 2, // 0 - 10
        instance = "ITPL_HK_HERB_41",
        amount = 1,
        staminaCost = 20,
        ground = 0,
    },
    {
        name = "Niedojrza�a psianka",
        time = 135, // sekundy
        difficulty = 2, // 0 - 10
        instance = "ITPL_WEED",
        amount = 1,
        staminaCost = 2,
        ground = 0,
    },
    {
        name = "Niedojrza�a psianka",
        time = 150, // sekundy
        difficulty = 2, // 0 - 10
        instance = "ITPL_WEED",
        amount = 1,
        staminaCost = 2,
        ground = 0,
    },
	{
        name = "Psianka",
        time = 175, // sekundy
        difficulty = 2, // 0 - 10
        instance = "ITPL_HK_HERB_05",
        amount = 2,
        staminaCost = 2,
        ground = 0,
    }
]

Config["HerbSystem"]["Psianka 3"] <- [
    {
        name = "Nasiono psianki",
        time = 120, // sekundy
        difficulty = 3, // 0 - 10
        instance = "ITPL_HK_HERB_41",
        amount = 1,
        staminaCost = 2,
        ground = 0,
    },
    {
        name = "Niedojrza�a psianka",
        time = 150, // sekundy
        difficulty = 3, // 0 - 10
        instance = "ITPL_WEED",
        amount = 1,
        staminaCost = 2,
        ground = 0,
    },
    {
        name = "Psianka",
        time = 175, // sekundy
        difficulty = 3, // 0 - 10
        instance = "ITPL_HK_HERB_05",
        amount = 1,
        staminaCost = 2,
        ground = 0,
    },
	{
        name = "Psianka",
        time = 220, // sekundy
        difficulty = 3, // 0 - 10
        instance = "ITPL_HK_HERB_05",
        amount = 3,
        staminaCost = 2,
        ground = 0,
    }
]

Config["HerbSystem"]["Kocianka"] <- [
    {
        name = "Nasiono kocianki",
        time = 90, // sekundy
        difficulty = 2, // 0 - 10
        instance = "ITPL_HK_HERB_42",
        amount = 1,
        staminaCost = 2,
        ground = 0,
    },
    {
        name = "Niedojrza�a kocianka",
        time = 115, // sekundy
        difficulty = 2, // 0 - 10
        instance = "ITPL_WEED",
        amount = 1,
        staminaCost = 2,
        ground = 0,
    },
    {
        name = "Niedojrza�a kocianka",
        time = 135, // sekundy
        difficulty = 2, // 0 - 10
        instance = "ITPL_WEED",
        amount = 1,
        staminaCost = 2,
        ground = 0,
    },
	{
        name = "Kocianka",
        time = 165, // sekundy
        difficulty = 2, // 0 - 10
        instance = "ITPL_HK_HERB_06",
        amount = 1,
        staminaCost = 2,
        ground = 0,
    }
]

Config["HerbSystem"]["Kocianka 2"] <- [
    {
        name = "Nasiono kocianki",
        time = 120, // sekundy
        difficulty = 2, // 0 - 10
        instance = "ITPL_HK_HERB_42",
        amount = 1,
        staminaCost = 20,
        ground = 0,
    },
    {
        name = "Niedojrza�a kocianka",
        time = 135, // sekundy
        difficulty = 2, // 0 - 10
        instance = "ITPL_WEED",
        amount = 1,
        staminaCost = 2,
        ground = 0,
    },
    {
        name = "Niedojrza�a kocianka",
        time = 150, // sekundy
        difficulty = 2, // 0 - 10
        instance = "ITPL_WEED",
        amount = 1,
        staminaCost = 2,
        ground = 0,
    },
	{
        name = "Kocianka",
        time = 175, // sekundy
        difficulty = 2, // 0 - 10
        instance = "ITPL_HK_HERB_06",
        amount = 2,
        staminaCost = 2,
        ground = 0,
    }
]

Config["HerbSystem"]["Kocianka 3"] <- [
    {
        name = "Nasiono kocianki",
        time = 120, // sekundy
        difficulty = 3, // 0 - 10
        instance = "ITPL_HK_HERB_42",
        amount = 1,
        staminaCost = 2,
        ground = 0,
    },
    {
        name = "Niedojrza�a kocianka",
        time = 150, // sekundy
        difficulty = 3, // 0 - 10
        instance = "ITPL_WEED",
        amount = 1,
        staminaCost = 2,
        ground = 0,
    },
    {
        name = "Kocianka",
        time = 175, // sekundy
        difficulty = 3, // 0 - 10
        instance = "ITPL_HK_HERB_06",
        amount = 1,
        staminaCost = 2,
        ground = 0,
    },
	{
        name = "Kocianka",
        time = 220, // sekundy
        difficulty = 3, // 0 - 10
        instance = "ITPL_HK_HERB_06",
        amount = 3,
        staminaCost = 2,
        ground = 0,
    }
]

Config["HerbSystem"]["Dragrot"] <- [
    {
        name = "Nasiono dragrotu",
        time = 90, // sekundy
        difficulty = 3, // 0 - 10
        instance = "ITPL_HK_HERB_57",
        amount = 1,
        staminaCost = 2,
        ground = 3,
    },
    {
        name = "Niedojrza�y dragrot",
        time = 115, // sekundy
        difficulty = 3, // 0 - 10
        instance = "ITPL_WEED",
        amount = 1,
        staminaCost = 2,
        ground = 3,
    },
    {
        name = "Niedojrza�y dragrot",
        time = 135, // sekundy
        difficulty = 3, // 0 - 10
        instance = "ITPL_WEED",
        amount = 1,
        staminaCost = 2,
        ground = 3,
    },
	{
        name = "Dragrot",
        time = 165, // sekundy
        difficulty = 3, // 0 - 10
        instance = "ITPL_HK_HERB_15",
        amount = 1,
        staminaCost = 2,
        ground = 3,
    }
]

Config["HerbSystem"]["Dragrot 2"] <- [
    {
        name = "Nasiono dragrotu",
        time = 120, // sekundy
        difficulty = 3, // 0 - 10
        instance = "ITPL_HK_HERB_57",
        amount = 1,
        staminaCost = 20,
        ground = 3,
    },
    {
        name = "Niedojrza�y dragrot",
        time = 135, // sekundy
        difficulty = 3, // 0 - 10
        instance = "ITPL_WEED",
        amount = 1,
        staminaCost = 2,
        ground = 3,
    },
    {
        name = "Niedojrza�y dragrot",
        time = 150, // sekundy
        difficulty = 3, // 0 - 10
        instance = "ITPL_WEED",
        amount = 1,
        staminaCost = 2,
        ground = 3,
    },
	{
        name = "Dragrot",
        time = 175, // sekundy
        difficulty = 3, // 0 - 10
        instance = "ITPL_HK_HERB_15",
        amount = 2,
        staminaCost = 2,
        ground = 3,
    }
]

Config["HerbSystem"]["Dragrot 3"] <- [
    {
        name = "Nasiono dragrotu",
        time = 120, // sekundy
        difficulty = 3, // 0 - 10
        instance = "ITPL_HK_HERB_57",
        amount = 1,
        staminaCost = 2,
        ground = 3,
    },
    {
        name = "Niedojrza�y dragrot",
        time = 150, // sekundy
        difficulty = 3, // 0 - 10
        instance = "ITPL_WEED",
        amount = 1,
        staminaCost = 2,
        ground = 3,
    },
    {
        name = "Dragrot",
        time = 175, // sekundy
        difficulty = 3, // 0 - 10
        instance = "ITPL_HK_HERB_15",
        amount = 1,
        staminaCost = 2,
        ground = 3,
    },
	{
        name = "Dragrot",
        time = 220, // sekundy
        difficulty = 3, // 0 - 10
        instance = "ITPL_HK_HERB_15",
        amount = 3,
        staminaCost = 2,
        ground = 3,
    }
]

Config["HerbSystem"]["Z�bate ziele"] <- [
    {
        name = "Nasiono z�batego ziela",
        time = 90, // sekundy
        difficulty = 1, // 0 - 10
        instance = "ITPL_HK_HERB_36",
        amount = 1,
        staminaCost = 2,
        ground = 0,
    },
    {
        name = "Niedojrza�e z�bate ziele",
        time = 115, // sekundy
        difficulty = 1, // 0 - 10
        instance = "ITPL_WEED",
        amount = 1,
        staminaCost = 2,
        ground = 0,
    },
    {
        name = "Niedojrza�e z�bate ziele",
        time = 135, // sekundy
        difficulty = 1, // 0 - 10
        instance = "ITPL_WEED",
        amount = 1,
        staminaCost = 2,
        ground = 0,
    },
	{
        name = "Z�bate ziele",
        time = 165, // sekundy
        difficulty = 1, // 0 - 10
        instance = "ITPL_SPEED_HERB_01",
        amount = 1,
        staminaCost = 2,
        ground = 0,
    }
]

Config["HerbSystem"]["Z�bate ziele 2"] <- [
    {
        name = "Nasiono z�batego ziela",
        time = 120, // sekundy
        difficulty = 2, // 0 - 10
        instance = "ITPL_HK_HERB_36",
        amount = 1,
        staminaCost = 20,
        ground = 0,
    },
    {
        name = "Niedojrza�e z�bate ziele",
        time = 135, // sekundy
        difficulty = 2, // 0 - 10
        instance = "ITPL_WEED",
        amount = 1,
        staminaCost = 2,
        ground = 0,
    },
    {
        name =  "Niedojrza�e z�bate ziele",
        time = 150, // sekundy
        difficulty = 2, // 0 - 10
        instance = "ITPL_WEED",
        amount = 1,
        staminaCost = 2,
        ground = 0,
    },
	{
        name = "Z�bate ziele",
        time = 175, // sekundy
        difficulty = 2, // 0 - 10
        instance = "ITPL_SPEED_HERB_01",
        amount = 2,
        staminaCost = 2,
        ground = 0,
    }
]

Config["HerbSystem"]["Z�bate ziele 3"] <- [
    {
        name = "Nasiono z�batego ziela",
        time = 120, // sekundy
        difficulty = 3, // 0 - 10
        instance = "ITPL_HK_HERB_36",
        amount = 1,
        staminaCost = 2,
        ground = 0,
    },
    {
        name = "Niedojrza�e z�bate ziele",
        time = 150, // sekundy
        difficulty = 3, // 0 - 10
        instance = "ITPL_WEED",
        amount = 1,
        staminaCost = 2,
        ground = 0,
    },
    {
        name = "Z�bate ziele",
        time = 175, // sekundy
        difficulty = 3, // 0 - 10
        instance = "ITPL_SPEED_HERB_01",
        amount = 1,
        staminaCost = 2,
        ground = 0,
    },
	{
        name = "Z�bate ziele",
        time = 220, // sekundy
        difficulty = 3, // 0 - 10
        instance = "ITPL_SPEED_HERB_01",
        amount = 3,
        staminaCost = 2,
        ground = 0,
    }
]

Config["HerbSystem"]["Velais"] <- [
    {
        name = "Nasiono velaisu",
        time = 90, // sekundy
        difficulty = 2, // 0 - 10
        instance = "ITPL_HK_HERB_38",
        amount = 1,
        staminaCost = 2,
        ground = 1,
    },
    {
        name = "Niedojrza�y velais",
        time = 115, // sekundy
        difficulty = 2, // 0 - 10
        instance = "ITPL_WEED",
        amount = 1,
        staminaCost = 2,
        ground = 1,
    },
    {
        name = "Niedojrza�y velais",
        time = 135, // sekundy
        difficulty = 2, // 0 - 10
        instance = "ITPL_WEED",
        amount = 1,
        staminaCost = 2,
        ground = 1,
    },
	{
        name = "Velais",
        time = 165, // sekundy
        difficulty = 2, // 0 - 10
        instance = "ITPL_HK_HERB_02",
        amount = 1,
        staminaCost = 2,
        ground = 1,
    }
]

Config["HerbSystem"]["Velais 2"] <- [
    {
        name = "Nasiono velaisu",
        time = 120, // sekundy
        difficulty = 2, // 0 - 10
        instance = "ITPL_HK_HERB_38",
        amount = 1,
        staminaCost = 20,
        ground = 1,
    },
    {
        name = "Niedojrza�y velais",
        time = 135, // sekundy
        difficulty = 2, // 0 - 10
        instance = "ITPL_WEED",
        amount = 1,
        staminaCost = 2,
        ground = 1,
    },
    {
        name = "Niedojrza�y velais",
        time = 150, // sekundy
        difficulty = 2, // 0 - 10
        instance = "ITPL_WEED",
        amount = 1,
        staminaCost = 2,
        ground = 1,
    },
	{
        name = "Velais",
        time = 175, // sekundy
        difficulty = 2, // 0 - 10
        instance = "ITPL_HK_HERB_02",
        amount = 2,
        staminaCost = 2,
        ground = 1,
    }
]

Config["HerbSystem"]["Velais 3"] <- [
    {
        name = "Nasiono velaisu",
        time = 120, // sekundy
        difficulty = 3, // 0 - 10
        instance = "ITPL_HK_HERB_38",
        amount = 1,
        staminaCost = 2,
        ground = 1,
    },
    {
        name = "Niedojrza�y velais",
        time = 150, // sekundy
        difficulty = 3, // 0 - 10
        instance = "ITPL_WEED",
        amount = 1,
        staminaCost = 2,
        ground = 1,
    },
    {
        name = "Velais",
        time = 175, // sekundy
        difficulty = 3, // 0 - 10
        instance = "ITPL_HK_HERB_02",
        amount = 1,
        staminaCost = 2,
        ground = 1,
    },
	{
        name = "Velais",
        time = 220, // sekundy
        difficulty = 3, // 0 - 10
        instance = "ITPL_HK_HERB_02",
        amount = 3,
        staminaCost = 2,
        ground = 1,
    }
]

Config["HerbSystem"]["S�oneczny aloes"] <- [
    {
        name = "Nasiono s�onecznego aloesu",
        time = 90, // sekundy
        difficulty = 3, // 0 - 10
        instance = "ITPL_HK_HERB_34",
        amount = 1,
        staminaCost = 2,
        ground = 1,
    },
    {
        name = "Niedojrza�y s�oneczny aloes",
        time = 115, // sekundy
        difficulty = 3, // 0 - 10
        instance = "ITPL_WEED",
        amount = 1,
        staminaCost = 2,
        ground = 1,
    },
    {
        name = "Niedojrza�y s�oneczny aloes",
        time = 135, // sekundy
        difficulty = 3, // 0 - 10
        instance = "ITPL_WEED",
        amount = 1,
        staminaCost = 2,
        ground = 1,
    },
	{
        name = "S�oneczny aloes",
        time = 165, // sekundy
        difficulty = 3, // 0 - 10
        instance = "ITPL_SAGITTA_HERB_MIS",
        amount = 1,
        staminaCost = 2,
        ground = 1,
    }
]

Config["HerbSystem"]["S�oneczny aloes 2"] <- [
    {
        name = "Nasiono s�onecznego aloesu",
        time = 120, // sekundy
        difficulty = 3, // 0 - 10
        instance = "ITPL_HK_HERB_34",
        amount = 1,
        staminaCost = 20,
        ground = 1,
    },
    {
        name = "Niedojrza�y s�oneczny aloes",
        time = 135, // sekundy
        difficulty = 3, // 0 - 10
        instance = "ITPL_WEED",
        amount = 1,
        staminaCost = 2,
        ground = 1,
    },
    {
        name = "Niedojrza�y s�oneczny aloes",
        time = 150, // sekundy
        difficulty = 3, // 0 - 10
        instance = "ITPL_WEED",
        amount = 1,
        staminaCost = 2,
        ground = 1,
    },
	{
        name = "S�oneczny aloes",
        time = 175, // sekundy
        difficulty = 3, // 0 - 10
        instance = "ITPL_SAGITTA_HERB_MIS",
        amount = 2,
        staminaCost = 2,
        ground = 1,
    }
]

Config["HerbSystem"]["S�oneczny aloes 3"] <- [
    {
        name = "Nasiono s�onecznego aloesu",
        time = 120, // sekundy
        difficulty = 3, // 0 - 10
        instance = "ITPL_HK_HERB_34",
        amount = 1,
        staminaCost = 2,
        ground = 1,
    },
    {
        name = "Niedojrza�y s�oneczny aloes",
        time = 150, // sekundy
        difficulty = 3, // 0 - 10
        instance = "ITPL_WEED",
        amount = 1,
        staminaCost = 2,
        ground = 1,
    },
    {
        name = "S�oneczny aloes",
        time = 175, // sekundy
        difficulty = 3, // 0 - 10
        instance = "ITPL_SAGITTA_HERB_MIS",
        amount = 1,
        staminaCost = 2,
        ground = 1,
    },
	{
        name = "S�oneczny aloes",
        time = 220, // sekundy
        difficulty = 3, // 0 - 10
        instance = "ITPL_SAGITTA_HERB_MIS",
        amount = 3,
        staminaCost = 2,
        ground = 1,
    }
]

Config["HerbSystem"]["Goblinie jagody"] <- [
    {
        name = "Nasiono goblinich jag�d",
        time = 90, // sekundy
        difficulty = 2, // 0 - 10
        instance = "ITPL_HK_HERB_21",
        amount = 1,
        staminaCost = 2,
        ground = 2,
    },
    {
        name = "Niedojrza�e gobilnie jagody",
        time = 115, // sekundy
        difficulty = 2, // 0 - 10
        instance = "ITPL_WEED",
        amount = 1,
        staminaCost = 2,
        ground = 2,
    },
    {
        name = "Niedojrza�e gobilnie jagody"
        time = 135, // sekundy
        difficulty = 2, // 0 - 10
        instance = "ITPL_WEED",
        amount = 1,
        staminaCost = 2,
        ground = 2,
    },
	{
        name = "Goblinie jagody",
        time = 165, // sekundy
        difficulty = 2, // 0 - 10
        instance = "ITPL_DEX_HERB_01",
        amount = 1,
        staminaCost = 2,
        ground = 2,
    }
]

Config["HerbSystem"]["Goblinie jagody 2"] <- [
    {
        name = "Nasiono goblinich jag�d",
        time = 120, // sekundy
        difficulty = 2, // 0 - 10
        instance = "ITPL_HK_HERB_21",
        amount = 1,
        staminaCost = 20,
        ground = 2,
    },
    {
        name = "Niedojrza�e gobilnie jagody",
        time = 135, // sekundy
        difficulty = 2, // 0 - 10
        instance = "ITPL_WEED",
        amount = 1,
        staminaCost = 2,
        ground = 2,
    },
    {
        name = "Niedojrza�e gobilnie jagody"
        time = 150, // sekundy
        difficulty = 2, // 0 - 10
        instance = "ITPL_WEED",
        amount = 1,
        staminaCost = 2,
        ground = 2,
    },
	{
        name = "Goblinie jagody",
        time = 175, // sekundy
        difficulty = 2, // 0 - 10
        instance = "ITPL_DEX_HERB_01",
        amount = 2,
        staminaCost = 2,
        ground = 2,
    }
]

Config["HerbSystem"]["Goblinie jagody 3"] <- [
    {
        name = "Nasiono goblinich jag�d",
        time = 120, // sekundy
        difficulty = 3, // 0 - 10
        instance = "ITPL_HK_HERB_21",
        amount = 1,
        staminaCost = 2,
        ground = 2,
    },
    {
        name = "Niedojrza�e gobilnie jagody",
        time = 150, // sekundy
        difficulty = 3, // 0 - 10
        instance = "ITPL_WEED",
        amount = 1,
        staminaCost = 2,
        ground = 2,
    },
    {
        name = "Goblinie jagody",
        time = 175, // sekundy
        difficulty = 3, // 0 - 10
        instance = "ITPL_DEX_HERB_01",
        amount = 1,
        staminaCost = 2,
        ground = 2,
    },
	{
        name = "Goblinie jagody",
        time = 220, // sekundy
        difficulty = 3, // 0 - 10
        instance = "ITPL_DEX_HERB_01",
        amount = 3,
        staminaCost = 2,
        ground = 2,
    }
]

Config["HerbSystem"]["Winogrono"] <- [
    {
        name = "Pestka winogrona",
        time = 90, // sekundy
        difficulty = 1, // 0 - 10
        instance = "ITPL_HK_HERB_74",
        amount = 1,
        staminaCost = 2,
        ground = 4,
    },
    {
        name = "Ro�lina winogrona",
        time = 115, // sekundy
        difficulty = 1, // 0 - 10
        instance = "ITPL_WEED",
        amount = 1,
        staminaCost = 2,
        ground = 4,
    },
    {
        name = "Ro�lina winogrona",
        time = 135, // sekundy
        difficulty = 1, // 0 - 10
        instance = "ITPL_WEED",
        amount = 1
        staminaCost = 2,
        ground = 4,
    },
	{
        name = "Winogrono",
        time = 165, // sekundy
        difficulty = 1, // 0 - 10
        instance = "ITFO_HK_FOOD_05",
        amount = 1
        staminaCost = 2,
        ground = 4,
    }
]

Config["HerbSystem"]["Winogrono 2"] <- [
    {
        name = "Pestka winogrona",
        time = 120, // sekundy
        difficulty = 2, // 0 - 10
        instance = "ITPL_HK_HERB_74",
        amount = 1,
        staminaCost = 20,
        ground = 4,
    },
    {
        name = "Ro�lina winogrona",
        time = 135, // sekundy
        difficulty = 2, // 0 - 10
        instance = "ITPL_WEED",
        amount = 1,
        staminaCost = 2,
        ground = 4,
    },
    {
        name = "Ro�lina winogrona",
        time = 150, // sekundy
        difficulty = 2, // 0 - 10
        instance = "ITPL_WEED",
        amount = 2,
        staminaCost = 2,
        ground = 4,
    },
	{
        name = "Winogrono",
        time = 175, // sekundy
        difficulty = 2, // 0 - 10
        instance = "ITFO_HK_FOOD_05",
        amount = 2,
        staminaCost = 2,
        ground = 4,
    }
]

Config["HerbSystem"]["Winogrono 3"] <- [
    {
        name = "Pestka winogrona",
        time = 120, // sekundy
        difficulty = 3, // 0 - 10
        instance = "ITPL_HK_HERB_74",
        amount = 1,
        staminaCost = 2,
        ground = 4,
    },
    {
        name = "Ro�lina winogrona",
        time = 150, // sekundy
        difficulty = 3, // 0 - 10
        instance = "ITPL_WEED",
        amount = 1,
        staminaCost = 2,
        ground = 4,
    },
    {
        name = "Winogrono",
        time = 175, // sekundy
        difficulty = 3, // 0 - 10
        instance = "ITFO_HK_FOOD_05",
        amount =  1,
        staminaCost = 2,
        ground = 4,
    },
	{
        name = "Winogrono",
        time = 220, // sekundy
        difficulty = 3, // 0 - 10
        instance = "ITFO_HK_FOOD_05",
        amount = 3,
        staminaCost = 2,
        ground = 4,
    }
]




Config["Questions"] <- [
    {
        question = "Czy jeste� debilem?",
        answers = [
            ["Tak", true],
            ["Nie", false],
            ["Niewiem, ale si� domy�lam", true],
        ]
    },
]

// Vobs

Config["VobType"] <- {};
Config["VobType"][VobType.Static] <- "Statyczny";
Config["VobType"][VobType.StaticName] <- "Statyczny + nazwa";
Config["VobType"][VobType.Interaction] <- "Interakcja";
Config["VobType"][VobType.Door] <- "Ruchomy";
Config["VobType"][VobType.Switch] <- "Prze��cznik";
Config["VobType"][VobType.Container] <- "Magazyn";

Config["VobHealth"] <- {};
Config["VobHealth"][VobHealth.Static] <- "Statyczny";
Config["VobHealth"][VobHealth.Vulnerable] <- "Do zniszczenia";

Config["VobMovable"] <- {};
Config["VobMovable"][VobMovable.Static] <- "Statyczny";
Config["VobMovable"][VobMovable.Active] <- "Do przemieszczenia";

Config["VobTriggerType"] <- {};
Config["VobTriggerType"][VobTriggerType.Self] <- "Brak";
Config["VobTriggerType"][VobTriggerType.Vob] <- "Inny Vob";

Config["VobTriggerAnimation"] <- {};
Config["VobTriggerAnimation"][VobTriggerAnimation.Door] <- "Drzwi";
Config["VobTriggerAnimation"][VobTriggerAnimation.None] <- "�adna";

Config["DamageLabel"] <- {};

Config["DamageLabel"][DAMAGE_BLUNT] <- "Obuch";
Config["DamageLabel"][DAMAGE_POINT] <- "Strza�a";
Config["DamageLabel"][DAMAGE_EDGE] <- "Ostrze";
Config["DamageLabel"][DAMAGE_MAGIC] <- "Magia";
Config["DamageLabel"][DAMAGE_FIRE] <- "Ogie�";
Config["DamageLabel"][DAMAGE_FLY] <- "Powietrze";
Config["DamageLabel"][DAMAGE_FALL] <- "Upadek";
Config["DamageLabel"][DAMAGE_BARRIER] <- "Bariera";
Config["DamageLabel"][DAMAGE_UNKNOWN] <- "Nieznane";

Config["PlayerHeal"] <- {
    "ITCR_HK_MED_20":30000,
    "ITCR_HK_MED_21":30000,
    "ITCR_HK_MED_22":30000,
    "ITCR_HK_MED_23":30000,
    "ITCR_HK_MED_24":30000,
    "ITCR_HK_MED_12":30000,
    "ITSC_LIGHTHEAL":30000,
    "ITSC_MEDIUMHEAL":30000,
    "ITSC_FULLHEAL":30000,
};

Config["AnimationCommands"] <- {
    "taniec1" : "T_DANCE_09",
    "taniec2" : "T_DANCE_08",
    "taniec3" : "T_DANCE_07",
    "taniec4" : "T_DANCE_06",
    "taniec5" : "T_DANCE_05",
    "taniec6" : "T_DANCE_04",
    "taniec7" : "T_DANCE_03",
    "ktaniec1" : "T_DANCEK1",
    "ktaniec 2" : "T_DANCEK2",
    "striptiz" : "T_STAND_2_STRIPPEN",
    "tbrzucha" : "T_STAND_2_BAUCHTANZ",
    "tludowy" : "T_FALK_DANCE",
    "wariat" : "S_FIRE_VICTIM",
    "mrugnij" : ["R_EYESBLINK"],
    "zly" : ["S_ANGRY"],
    "oczy" : ["S_EYESCLOSED"],
    "przestraszony" : ["S_FRIGHTENED"],
    "normalny" : ["S_NEUTRAL"],
    "przyjazny" : ["S_FRIENDLY"],
    "czczenie" : "T_IDOL_S0_2_S1",
    "uklon" : "T_GREETNOV",
    "przywitaniemag" : "T_JUBELN_04",
    "artysta1" : "T_AMBIENT_VERBEUGEN_01",
    "artysta2" : "T_AMBIENT_VERBEUGEN_02",
    "arena" : "T_WATCHFIGHT_YEAH",
    "kibicowanie" : "S_KIBICOWANIE",
    "kdomyslne" : "T_STAND_2_CLAPHANDS",
    "kpowolne" : "T_JUBELN_05",
    "keskpresyjne" : "T_JUBELN_01",
    "kwyrachowane" : "T_JUBELN_02",
    "kdystynkt" : "T_JUBELN_03",
    "machaj" : "T_MACHANIE",
    "machaj2" : "T_MACHANIE2",
    "machaj3" : "T_ARENAMACHANIE",
    "pomachaj" : "T_AMBIENT_SCHIMPFEN",
    "zbierz" : "T_PLUNDER",
    "sikanie" : "T_STAND_2_PEE",
    "sikaniepijak" : "T_DRUNKENPEE",
    "zyganie" : "S_KOTZKUEBEL_S1",
    "pochodnia1" : "T_POSWIECENIE",
    "pochodnia2" : "T_POSWIECENIE2",
    "pochodnia3" : "T_POCHODNIA",
    "nakolana" : "T_BETEN_STAND_2_S0",
    "blaganie" : "T_BLAGANIE",
    "scinaj" : "S_LUMBERJACK_S1",
    "przybijaj" : "S_FLOORHAM_S1",
    "naprawiaj" : "S_REPAIR_S1",
    "kopziemie" : "S_DIGGER_S1",
    "koprude" : "S_ORE_S1",
    "zbieraj" : "S_EINPFLANZEN_S1",
    "dobij" : "T_1HSFINISH",
    "wygrana" : "T_WYGRANA",
    "alchemia" : "S_LAB_S1",
    "pal" : "T_JOINT_RANDOM_1",
    "pij" : "T_POTION_RANDOM_1",
    "jedz" : "T_MEAT_RANDOM_1",
    "myj" : "T_STAND_2_WASH",
    "czytajn" : "T_MAP_STAND_2_S0",
    "czytajp" : "S_BOOK_S1",
    "ostrz" : "T_CARVE_STAND_2_S0",
    "pijzrzeki" : "T_KNIEENTRINKEN_STAND_2_S0",
    "salto" : "T_SALTO",
    "lutnia" : "S_LUTE_S1",
    "rog" : "T_HORN_S0_2_S1" ,
    "trening" : "T_1HSFREE",
    "rozgrzewka" : "T_ROZGRZEWKA",
    "box" : "T_BOXOWANIE",
    "rozciaganie" : "T_GYMNASTIK_01",
    "miesniak" : "T_GYMNASTIK_04",
    "przysiady" : "T_GYMNASTIK_05",
    "pompki" : "S_POMPKI_S1",
    "dopompek" : "T_POMPKI",
    "szpagat" : "T_GYMNASTIK_08",
    "przeciaganie" : "T_GYMNASTIK_09",
    "siad" : "T_STAND_2_SIT",
    "siado" : "T_STAND_2_PACO",
    "siadw" : "T_STAND_2_MELVIN",
    "siadm" : "T_STAND_2_BAAL",
    "lawka" : "S_BENCH_S1",
    "lez" : "S_DEADB",
    "lezbrzuch" : "S_DEAD",
    "spij" : "T_STAND_2_SLEEPGROUND",
    "mumia" : "T_MUMIE_STAND",
    "straznik1" : "T_STAND_2_LGUARD",
    "straznik2" : "T_STAND_2_HGUARD",
    "salut" : "T_SALUTOWANIE",
    "amor" : "T_STRZELANIELUK",
    "przykuc" : "T_STAND_2_PRZYKUC",
    "oparcie" : "T_STAND_2_LEAN",
    "oparcie2" : "S_OPIERANIESIE",
    "kucniecie" : "T_PAN_STAND_2_S0",
    "modlitwa" : "T_STAND_2_PRAY",
    "magia1": "T_PRACTICEMAGIC",
    "magia2": "T_PRACTICEMAGIC2",
    "magia3": "T_PRACTICEMAGIC3",
    "magia4": "T_PRACTICEMAGIC4",
    "magia5": "T_PRACTICEMAGIC6",
    "magia6": "T_PRACTICEMAGIC7",
    "energia": "T_ENERGIA",
    "magicznakula": "T_KULAFULA",
    "czar": "T_CZAR",
    "przeglad" : "T_1HSINSPECT",
    "szalenstwo" : "S_CON_VICTIM",
    "nudy" : "T_BORINGKICK",
    "rozgladanie1" : "T_ZASIEBIE",
    "rozgladanie2" : "T_ZASIEBIE2",
    "rozgladanie3" : "T_ROZGLADANIE",
    "wypatrywanie" : "S_UMSEHEN_S1",
    "wytarcie" : "T_POTION_RANDOM_2",
    "zebranie" : "T_STAND_2_BETTELN",
    "skrzyzowane" : "T_STAND_2_ARMCROSSED_A",
    "latarnia" : "T_STANDTALK",
    "kopacz" : "T_STANDRIECHEN",
    "gadanie" : "T_GADANIE",
    "tak" : "T_YES",
    "nie" : "T_NO",
    "niewiem" : "T_DONTKNOW",
    "facepalm" : "T_IDIOT"
    "dzieki" : "T_DZIEKUJE",
    "nonie" : "T_NONIE",
    "zapomnij" : "T_FORGETIT",
    "grozba1" : "T_GETLOST",
    "grozba2" : "T_GETLOST2",
    "ho" : "T_COMEOVERHERE",
    "smiech1" : "T_LAUGHING",
    "smiech2" : "T_LACHEN",
    "krzyk" : "T_OKRZYK",
    "zastraszony" : "T_FRIGHTENED",
    "zawiedziony" : "T_ZAWIEDZIONY",
    "strach" : "T_STRACH",
    "obolaly" : "T_WJAJA",
    "mdlenie" : "T_VICTIM"
};

Config["UnStealableItems"] <- [
    "ITKE_addon_bloodwyn_01",
    "ITKE_Orlan_TeleportStation",
    "ITKE_Addon_Esteban_01",
    "ITKE_Addon_Thorus",
    "ITKE_Hotel",
    "ITRI_addon_bandittrader",
    "ITRI_addon_morgansring_mission",
    "ITMI_HK_QUEST_11",
    "ITMI_HK_QUEST_12",
    "ITMI_HK_QUEST_13",
    "ITKE_HK_CHEST_01",
    "ITKE_HK_CHEST_02",
    "ITKE_HK_CHEST_03",
    "ITKE_HK_CHEST_04",
    "ITKE_HK_CHEST_05",
    "ITKE_HK_CHEST_06",
    "ITKE_HK_CHEST_07",
    "ITKE_HK_CHEST_08",
    "ITKE_HK_CHEST_09",
    "ITKE_HK_CHEST_10",
    "ITKE_HK_CHEST_11",
    "ITKE_HK_CHEST_12",
    "ITKE_HK_CHEST_13",
    "ITKE_HK_CHEST_14",
    "ITKE_HK_CHEST_15",
    "ITKE_HK_CHEST_16",
    "ITKE_HK_CHEST_17",
    "ITKE_HK_CHEST_18",
    "ITKE_HK_CHEST_19",
    "ITKE_HK_CHEST_20",
    "ITKE_HK_CHEST_21",
    "ITKE_HK_CHEST_22",
    "ITKE_HK_CHEST_23",
    "ITKE_HK_CHEST_24",
    "ITKE_HK_CHEST_25",
    "ITKE_HK_CHEST_26",
    "ITKE_HK_CHEST_27",
    "ITKE_HK_CHEST_28",
    "ITKE_HK_CHEST_29",
    "ITKE_HK_CHEST_30",
    "ITKE_HK_CHEST_31",
    "ITKE_HK_CHEST_32",
    "ITKE_HK_CHEST_33",
    "ITKE_HK_CHEST_34",
    "ITKE_HK_CHEST_35",
    "ITKE_HK_CHEST_36",
    "ITKE_HK_CHEST_37",
    "ITKE_HK_CHEST_38",
    "ITKE_HK_CHEST_39",
    "ITKE_HK_CHEST_40",
    "ITKE_HK_CHEST_41",
    "ITKE_HK_CHEST_42",
    "ITKE_HK_CHEST_43",
    "ITKE_HK_CHEST_44",
    "ITKE_HK_CHEST_45",
    "ITKE_HK_CHEST_46",
    "ITKE_HK_CHEST_47",
    "ITKE_HK_CHEST_48",
    "ITKE_HK_CHEST_49",
    "ITKE_HK_CHEST_50",
    "ITKE_HK_CHEST_51",
    "ITKE_HK_CHEST_52",
    "ITKE_HK_CHEST_53",
    "ITKE_HK_CHEST_54",
    "ITKE_HK_CHEST_55",
    "ITKE_HK_CHEST_56",
    "ITKE_HK_CHEST_57",
    "ITKE_HK_CHEST_58",
    "ITKE_HK_CHEST_59",
    "ITKE_HK_CHEST_60",
    "ITKE_HK_CHEST_61",
    "ITKE_HK_CHEST_62",
    "ITKE_HK_CHEST_63",
    "ITKE_HK_CHEST_64",
    "ITKE_HK_CHEST_65",
    "ITKE_HK_CHEST_66",
    "ITKE_HK_CHEST_67",
    "ITKE_HK_CHEST_68",
    "ITKE_HK_CHEST_69",
    "ITKE_HK_CHEST_70",
    "ITKE_HK_CHEST_71",
    "ITKE_HK_CHEST_72",
    "ITKE_HK_CHEST_73",
    "ITKE_HK_CHEST_74",
    "ITKE_HK_CHEST_75",
    "ITKE_HK_CHEST_76",
    "ITKE_HK_CHEST_77",
    "ITKE_HK_CHEST_78",
    "ITKE_HK_CHEST_79",
    "ITKE_HK_CHEST_80",
    "ITKE_HK_CHEST_81",
    "ITKE_HK_CHEST_82",
    "ITKE_HK_CHEST_83",
    "ITKE_HK_CHEST_84",
    "ITKE_HK_CHEST_85",
    "ITKE_HK_CHEST_86",
    "ITKE_HK_CHEST_87",
    "ITKE_HK_CHEST_88",
    "ITKE_HK_CHEST_89",
    "ITKE_HK_CHEST_90",
    "ITKE_HK_CHEST_91",
    "ITKE_HK_CHEST_92",
    "ITKE_HK_CHEST_93",
    "ITKE_HK_CHEST_94",
    "ITKE_HK_CHEST_95",
    "ITKE_HK_CHEST_96",
    "ITKE_HK_CHEST_97",
    "ITKE_HK_CHEST_98",
    "ITKE_HK_CHEST_99",
    "ITKE_HK_CHEST_100",
    "ITKE_HK_CHEST_101",
    "ITKE_HK_CHEST_102",
    "ITKE_HK_CHEST_103",
    "ITKE_HK_CHEST_104",
    "ITKE_HK_CHEST_105",
    "ITKE_HK_CHEST_106",
    "ITKE_HK_CHEST_107",
    "ITKE_HK_CHEST_108",
    "ITKE_HK_CHEST_109",
    "ITKE_HK_CHEST_110",
    "ITKE_HK_CHEST_111",
    "ITKE_HK_CHEST_112",
    "ITKE_HK_CHEST_113",
    "ITKE_HK_CHEST_114",
    "ITKE_HK_CHEST_115",
    "ITKE_HK_CHEST_116",
    "ITKE_HK_CHEST_117",
    "ITKE_HK_CHEST_118",
    "ITKE_HK_CHEST_119",
    "ITKE_HK_CHEST_120",
    "ITKE_HK_CHEST_121",
    "ITKE_HK_CHEST_122",
    "ITKE_HK_CHEST_123",
    "ITKE_HK_CHEST_124",
    "ITKE_HK_CHEST_125",
    "ITKE_HK_CHEST_126",
    "ITKE_HK_CHEST_127",
    "ITKE_HK_CHEST_128",
    "ITKE_HK_CHEST_129",
    "ITKE_HK_CHEST_130",
    "ITKE_HK_CHEST_131",
    "ITKE_HK_CHEST_132",
    "ITKE_HK_CHEST_133",
    "ITKE_HK_CHEST_134",
    "ITKE_HK_CHEST_135",
    "ITKE_HK_CHEST_136",
    "ITKE_HK_CHEST_137",
    "ITKE_HK_CHEST_138",
    "ITKE_HK_CHEST_139",
    "ITKE_HK_CHEST_140",
    "ITKE_HK_CHEST_141",
    "ITKE_HK_CHEST_142",
    "ITKE_HK_CHEST_143",
    "ITKE_HK_CHEST_144",
    "ITKE_HK_CHEST_145",
    "ITKE_HK_CHEST_146",
    "ITKE_HK_CHEST_147",
    "ITKE_HK_CHEST_148",
    "ITKE_HK_CHEST_149",
    "ITKE_HK_CHEST_150",
    "ITKE_HK_CHEST_151",
    "ITKE_HK_CHEST_152",
    "ITKE_HK_CHEST_153",
    "ITKE_HK_CHEST_154",
    "ITKE_HK_CHEST_155",
    "ITKE_HK_CHEST_156",
    "ITKE_HK_CHEST_157",
    "ITKE_HK_CHEST_158",
    "ITKE_HK_CHEST_159",
    "ITKE_HK_CHEST_160",
    "ITKE_HK_CHEST_161",
    "ITKE_HK_CHEST_162",
    "ITKE_HK_CHEST_163",
    "ITKE_HK_CHEST_164",
    "ITKE_HK_CHEST_165",
    "ITKE_HK_CHEST_166",
    "ITKE_HK_CHEST_167",
    "ITKE_HK_CHEST_168",
    "ITKE_HK_CHEST_169",
    "ITKE_HK_CHEST_170",
    "ITKE_HK_CHEST_171",
    "ITKE_HK_CHEST_172",
    "ITKE_HK_CHEST_173",
    "ITKE_HK_CHEST_174",
    "ITKE_HK_CHEST_175",
    "ITKE_HK_CHEST_176",
    "ITKE_HK_CHEST_177",
    "ITKE_HK_CHEST_178",
    "ITKE_HK_CHEST_179",
    "ITKE_HK_CHEST_180",
    "ITKE_HK_CHEST_181",
    "ITKE_HK_CHEST_182",
    "ITKE_HK_CHEST_183",
    "ITKE_HK_CHEST_184",
    "ITKE_HK_CHEST_185",
    "ITKE_HK_CHEST_186",
    "ITKE_HK_CHEST_187",
    "ITKE_HK_CHEST_188",
    "ITKE_HK_CHEST_189",
    "ITKE_HK_CHEST_190",
    "ITKE_HK_CHEST_191",
    "ITKE_HK_CHEST_192",
    "ITKE_HK_CHEST_193",
    "ITKE_HK_CHEST_194",
    "ITKE_HK_CHEST_195",
    "ITKE_HK_CHEST_196",
    "ITKE_HK_CHEST_197",
    "ITKE_HK_CHEST_198",
    "ITKE_HK_CHEST_199",
    "ITKE_HK_CHEST_200",
    "ITKE_HK_CHEST_201",
    "ITKE_HK_CHEST_202",
    "ITKE_HK_CHEST_203",
    "ITKE_HK_CHEST_204",
    "ITKE_HK_CHEST_205",
    "ITKE_HK_CHEST_206",
    "ITKE_HK_CHEST_207",
    "ITKE_HK_CHEST_208",
    "ITKE_HK_CHEST_209",
    "ITKE_HK_CHEST_210",
    "ITKE_HK_CHEST_211",
    "ITKE_HK_CHEST_212",
    "ITKE_HK_CHEST_213",
    "ITKE_HK_CHEST_214",
    "ITKE_HK_CHEST_215",
    "ITKE_HK_CHEST_216",
    "ITKE_HK_CHEST_217",
    "ITKE_HK_CHEST_218",
    "ITKE_HK_CHEST_219",
    "ITKE_HK_CHEST_220",
    "ITKE_HK_CHEST_221",
    "ITKE_HK_CHEST_222",
    "ITKE_HK_CHEST_223",
    "ITKE_HK_CHEST_224",
    "ITKE_HK_CHEST_225",
    "ITKE_HK_CHEST_226",
    "ITKE_HK_CHEST_227",
    "ITKE_HK_CHEST_228",
    "ITKE_HK_CHEST_229",
    "ITKE_HK_CHEST_230",
    "ITKE_HK_CHEST_231",
    "ITKE_HK_CHEST_232",
    "ITKE_HK_CHEST_233",
    "ITKE_HK_CHEST_234",
    "ITKE_HK_CHEST_235",
    "ITKE_HK_CHEST_236",
    "ITKE_HK_CHEST_237",
    "ITKE_HK_CHEST_238",
    "ITKE_HK_CHEST_239",
    "ITKE_HK_CHEST_240",
    "ITKE_HK_CHEST_241",
    "ITKE_HK_CHEST_242",
    "ITKE_HK_CHEST_243",
    "ITKE_HK_CHEST_244",
    "ITKE_HK_CHEST_245",
    "ITKE_HK_CHEST_246",
    "ITKE_HK_CHEST_247",
    "ITKE_HK_CHEST_248",
    "ITKE_HK_CHEST_249",
    "ITKE_HK_CHEST_250",
    "ITKE_HK_CHEST_251",
    "ITKE_HK_CHEST_252",
    "ITKE_HK_CHEST_253",
    "ITKE_HK_CHEST_254",
    "ITKE_HK_CHEST_255",
    "ITKE_HK_CHEST_256",
    "ITKE_HK_CHEST_257",
    "ITKE_HK_CHEST_258",
    "ITKE_HK_CHEST_259",
    "ITKE_HK_CHEST_260",
    "ITKE_HK_CHEST_261",
    "ITKE_HK_CHEST_262",
    "ITKE_HK_CHEST_263",
    "ITKE_HK_CHEST_264",
    "ITKE_HK_CHEST_265",
    "ITKE_HK_CHEST_266",
    "ITKE_HK_CHEST_267",
    "ITKE_HK_CHEST_268",
    "ITKE_HK_CHEST_269",
    "ITKE_HK_CHEST_270",
    "ITKE_HK_CHEST_271",
    "ITKE_HK_CHEST_272",
    "ITKE_HK_CHEST_273",
    "ITKE_HK_CHEST_274",
    "ITKE_HK_CHEST_275",
    "ITKE_HK_CHEST_276",
    "ITKE_HK_CHEST_277",
    "ITKE_HK_CHEST_278",
    "ITKE_HK_CHEST_279",
    "ITKE_HK_CHEST_280",
    "ITKE_HK_CHEST_281",
    "ITKE_HK_CHEST_282",
    "ITKE_HK_CHEST_283",
    "ITKE_HK_CHEST_284",
    "ITKE_HK_CHEST_285",
    "ITKE_HK_CHEST_286",
    "ITKE_HK_CHEST_287",
    "ITKE_HK_CHEST_288",
    "ITKE_HK_CHEST_289",
    "ITKE_HK_CHEST_290",
    "ITKE_HK_CHEST_291",
    "ITKE_HK_CHEST_292",
    "ITKE_HK_CHEST_293",
    "ITKE_HK_CHEST_294",
    "ITKE_HK_CHEST_295",
    "ITKE_HK_CHEST_296",
    "ITKE_HK_CHEST_297",
    "ITKE_HK_CHEST_298",
    "ITKE_HK_CHEST_299",
    "ITKE_HK_CHEST_300",
    "ITKE_HK_CHEST_301",
    "ITKE_HK_CHEST_302",
    "ITKE_HK_CHEST_303",
    "ITKE_HK_CHEST_304",
    "ITKE_HK_CHEST_305",
    "ITKE_HK_CHEST_306",
    "ITKE_HK_CHEST_307",
    "ITKE_HK_CHEST_308",
    "ITKE_HK_CHEST_309",
    "ITKE_HK_CHEST_310",
    "ITKE_HK_CHEST_311",
    "ITKE_HK_CHEST_312",
    "ITKE_HK_CHEST_313",
    "ITKE_HK_CHEST_314",
    "ITKE_HK_CHEST_315",
    "ITKE_HK_CHEST_316",
    "ITKE_HK_CHEST_317",
    "ITKE_HK_CHEST_318",
    "ITKE_HK_CHEST_319",
    "ITKE_HK_CHEST_320",
    "ITKE_HK_CHEST_321",
    "ITKE_HK_CHEST_322",
    "ITKE_HK_CHEST_323",
    "ITKE_HK_CHEST_324",
    "ITKE_HK_CHEST_325",
    "ITKE_HK_CHEST_326",
    "ITKE_HK_CHEST_327",
    "ITKE_HK_CHEST_328",
    "ITKE_HK_CHEST_329",
    "ITKE_HK_CHEST_330",
    "ITKE_HK_CHEST_331",
    "ITKE_HK_CHEST_332",
    "ITKE_HK_CHEST_333",
    "ITKE_HK_CHEST_334",
    "ITKE_HK_CHEST_335",
    "ITKE_HK_CHEST_336",
    "ITKE_HK_CHEST_337",
    "ITKE_HK_CHEST_338",
    "ITKE_HK_CHEST_339",
    "ITKE_HK_CHEST_340",
    "ITKE_HK_CHEST_341",
    "ITKE_HK_CHEST_342",
    "ITKE_HK_CHEST_343",
    "ITKE_HK_CHEST_344",
    "ITKE_HK_CHEST_345",
    "ITKE_HK_CHEST_346",
    "ITKE_HK_CHEST_347",
    "ITKE_HK_CHEST_348",
    "ITKE_HK_CHEST_349",
    "ITKE_HK_CHEST_350",
    "ITKE_HK_CHEST_351",
    "ITKE_HK_CHEST_352",
    "ITKE_HK_CHEST_353",
    "ITKE_HK_CHEST_354",
    "ITKE_HK_CHEST_355",
    "ITKE_HK_CHEST_356",
    "ITKE_HK_CHEST_357",
    "ITKE_HK_CHEST_358",
    "ITKE_HK_CHEST_359",
    "ITKE_HK_CHEST_360",
    "ITKE_HK_CHEST_361",
    "ITKE_HK_CHEST_362",
    "ITKE_HK_CHEST_363",
    "ITKE_HK_CHEST_364",
    "ITKE_HK_CHEST_365",
    "ITKE_HK_CHEST_366",
    "ITKE_HK_CHEST_367",
    "ITKE_HK_CHEST_368",
    "ITKE_HK_CHEST_369",
    "ITKE_HK_CHEST_370",
    "ITKE_HK_CHEST_371",
    "ITKE_HK_CHEST_372",
    "ITKE_HK_CHEST_373",
    "ITKE_HK_CHEST_374",
    "ITKE_HK_CHEST_375",
    "ITKE_HK_CHEST_376",
    "ITKE_HK_CHEST_377",
    "ITKE_HK_CHEST_378",
    "ITKE_HK_CHEST_379",
    "ITKE_HK_CHEST_380",
    "ITKE_HK_CHEST_381",
    "ITKE_HK_CHEST_382",
    "ITKE_HK_CHEST_383",
    "ITKE_HK_CHEST_384",
    "ITKE_HK_CHEST_385",
    "ITKE_HK_CHEST_386",
    "ITKE_HK_CHEST_387",
    "ITKE_HK_CHEST_388",
    "ITKE_HK_CHEST_389",
    "ITKE_HK_CHEST_390",
    "ITKE_HK_CHEST_391",
    "ITKE_HK_CHEST_392",
    "ITKE_HK_CHEST_393",
    "ITKE_HK_CHEST_394",
    "ITKE_HK_CHEST_395",
    "ITKE_HK_CHEST_396",
    "ITKE_HK_CHEST_397",
    "ITKE_HK_CHEST_398",
    "ITKE_HK_CHEST_399",
    "ITKE_HK_CHEST_400",
    "ITKE_HK_DOOR_01",
    "ITKE_HK_DOOR_02",
    "ITKE_HK_DOOR_03",
    "ITKE_HK_DOOR_04",
    "ITKE_HK_DOOR_05",
    "ITKE_HK_DOOR_06",
    "ITKE_HK_DOOR_07",
    "ITKE_HK_DOOR_08",
    "ITKE_HK_DOOR_09",
    "ITKE_HK_DOOR_10",
    "ITKE_HK_DOOR_11",
    "ITKE_HK_DOOR_12",
    "ITKE_HK_DOOR_13",
    "ITKE_HK_DOOR_14",
    "ITKE_HK_DOOR_15",
    "ITKE_HK_DOOR_16",
    "ITKE_HK_DOOR_17",
    "ITKE_HK_DOOR_18",
    "ITKE_HK_DOOR_19",
    "ITKE_HK_DOOR_20",
    "ITKE_HK_DOOR_21",
    "ITKE_HK_DOOR_22",
    "ITKE_HK_DOOR_23",
    "ITKE_HK_DOOR_24",
    "ITKE_HK_DOOR_25",
    "ITKE_HK_DOOR_26",
    "ITKE_HK_DOOR_27",
    "ITKE_HK_DOOR_28",
    "ITKE_HK_DOOR_29",
    "ITKE_HK_DOOR_30",
    "ITKE_HK_DOOR_31",
    "ITKE_HK_DOOR_32",
    "ITKE_HK_DOOR_33",
    "ITKE_HK_DOOR_34",
    "ITKE_HK_DOOR_35",
    "ITKE_HK_DOOR_36",
    "ITKE_HK_DOOR_37",
    "ITKE_HK_DOOR_38",
    "ITKE_HK_DOOR_39",
    "ITKE_HK_DOOR_40",
    "ITKE_HK_DOOR_41",
    "ITKE_HK_DOOR_42",
    "ITKE_HK_DOOR_43",
    "ITKE_HK_DOOR_44",
    "ITKE_HK_DOOR_45",
    "ITKE_HK_DOOR_46",
    "ITKE_HK_DOOR_47",
    "ITKE_HK_DOOR_48",
    "ITKE_HK_DOOR_49",
    "ITKE_HK_DOOR_50",
    "ITKE_HK_DOOR_51",
    "ITKE_HK_DOOR_52",
    "ITKE_HK_DOOR_53",
    "ITKE_HK_DOOR_54",
    "ITKE_HK_DOOR_55",
    "ITKE_HK_DOOR_56",
    "ITKE_HK_DOOR_57",
    "ITKE_HK_DOOR_58",
    "ITKE_HK_DOOR_59",
    "ITKE_HK_DOOR_60",
    "ITKE_HK_DOOR_61",
    "ITKE_HK_DOOR_62",
    "ITKE_HK_DOOR_63",
    "ITKE_HK_DOOR_64",
    "ITKE_HK_DOOR_65",
    "ITKE_HK_DOOR_66",
    "ITKE_HK_DOOR_67",
    "ITKE_HK_DOOR_68",
    "ITKE_HK_DOOR_69",
    "ITKE_HK_DOOR_70",
    "ITKE_HK_DOOR_71",
    "ITKE_HK_DOOR_72",
    "ITKE_HK_DOOR_73",
    "ITKE_HK_DOOR_74",
    "ITKE_HK_DOOR_75",
    "ITKE_HK_DOOR_76",
    "ITKE_HK_DOOR_77",
    "ITKE_HK_DOOR_78",
    "ITKE_HK_DOOR_79",
    "ITKE_HK_DOOR_80",
    "ITKE_HK_DOOR_81",
    "ITKE_HK_DOOR_82",
    "ITKE_HK_DOOR_83",
    "ITKE_HK_DOOR_84",
    "ITKE_HK_DOOR_85",
    "ITKE_HK_DOOR_86",
    "ITKE_HK_DOOR_87",
    "ITKE_HK_DOOR_88",
    "ITKE_HK_DOOR_89",
    "ITKE_HK_DOOR_90",
    "ITKE_HK_DOOR_91",
    "ITKE_HK_DOOR_92",
    "ITKE_HK_DOOR_93",
    "ITKE_HK_DOOR_94",
    "ITKE_HK_DOOR_95",
    "ITKE_HK_DOOR_96",
    "ITKE_HK_DOOR_97",
    "ITKE_HK_DOOR_98",
    "ITKE_HK_DOOR_99",
    "ITKE_HK_DOOR_100",
    "ITKE_HK_DOOR_101",
    "ITKE_HK_DOOR_102",
    "ITKE_HK_DOOR_103",
    "ITKE_HK_DOOR_104",
    "ITKE_HK_DOOR_105",
    "ITKE_HK_DOOR_106",
    "ITKE_HK_DOOR_107",
    "ITKE_HK_DOOR_108",
    "ITKE_HK_DOOR_109",
    "ITKE_HK_DOOR_110",
    "ITKE_HK_DOOR_111",
    "ITKE_HK_DOOR_112",
    "ITKE_HK_DOOR_113",
    "ITKE_HK_DOOR_114",
    "ITKE_HK_DOOR_115",
    "ITKE_HK_DOOR_116",
    "ITKE_HK_DOOR_117",
    "ITKE_HK_DOOR_118",
    "ITKE_HK_DOOR_119",
    "ITKE_HK_DOOR_120",
    "ITKE_HK_DOOR_121",
    "ITKE_HK_DOOR_122",
    "ITKE_HK_DOOR_123",
    "ITKE_HK_DOOR_124",
    "ITKE_HK_DOOR_125",
    "ITKE_HK_DOOR_126",
    "ITKE_HK_DOOR_127",
    "ITKE_HK_DOOR_128",
    "ITKE_HK_DOOR_129",
    "ITKE_HK_DOOR_130",
    "ITKE_HK_DOOR_131",
    "ITKE_HK_DOOR_132",
    "ITKE_HK_DOOR_133",
    "ITKE_HK_DOOR_134",
    "ITKE_HK_DOOR_135",
    "ITKE_HK_DOOR_136",
    "ITKE_HK_DOOR_137",
    "ITKE_HK_DOOR_138",
    "ITKE_HK_DOOR_139",
    "ITKE_HK_DOOR_140",
    "ITKE_HK_DOOR_141",
    "ITKE_HK_DOOR_142",
    "ITKE_HK_DOOR_143",
    "ITKE_HK_DOOR_144",
    "ITKE_HK_DOOR_145",
    "ITKE_HK_DOOR_146",
    "ITKE_HK_DOOR_147",
    "ITKE_HK_DOOR_148",
    "ITKE_HK_DOOR_149",
    "ITKE_HK_DOOR_150",
    "ITKE_HK_DOOR_151",
    "ITKE_HK_DOOR_152",
    "ITKE_HK_DOOR_153",
    "ITKE_HK_DOOR_154",
    "ITKE_HK_DOOR_155",
    "ITKE_HK_DOOR_156",
    "ITKE_HK_DOOR_157",
    "ITKE_HK_DOOR_158",
    "ITKE_HK_DOOR_159",
    "ITKE_HK_DOOR_160",
    "ITKE_HK_DOOR_161",
    "ITKE_HK_DOOR_162",
    "ITKE_HK_DOOR_163",
    "ITKE_HK_DOOR_164",
    "ITKE_HK_DOOR_165",
    "ITKE_HK_DOOR_166",
    "ITKE_HK_DOOR_167",
    "ITKE_HK_DOOR_168",
    "ITKE_HK_DOOR_169",
    "ITKE_HK_DOOR_170",
    "ITKE_HK_DOOR_171",
    "ITKE_HK_DOOR_172",
    "ITKE_HK_DOOR_173",
    "ITKE_HK_DOOR_174",
    "ITKE_HK_DOOR_175",
    "ITKE_HK_DOOR_176",
    "ITKE_HK_DOOR_177",
    "ITKE_HK_DOOR_178",
    "ITKE_HK_DOOR_179",
    "ITKE_HK_DOOR_180",
    "ITKE_HK_DOOR_181",
    "ITKE_HK_DOOR_182",
    "ITKE_HK_DOOR_183",
    "ITKE_HK_DOOR_184",
    "ITKE_HK_DOOR_185",
    "ITKE_HK_DOOR_186",
    "ITKE_HK_DOOR_187",
    "ITKE_HK_DOOR_188",
    "ITKE_HK_DOOR_189",
    "ITKE_HK_DOOR_190",
    "ITKE_HK_DOOR_191",
    "ITKE_HK_DOOR_192",
    "ITKE_HK_DOOR_193",
    "ITKE_HK_DOOR_194",
    "ITKE_HK_DOOR_195",
    "ITKE_HK_DOOR_196",
    "ITKE_HK_DOOR_197",
    "ITKE_HK_DOOR_198",
    "ITKE_HK_DOOR_199",
    "ITKE_HK_DOOR_200",
    "ITKE_HK_DOOR_201",
    "ITKE_HK_DOOR_202",
    "ITKE_HK_DOOR_203",
    "ITKE_HK_DOOR_204",
    "ITKE_HK_DOOR_205",
    "ITKE_HK_DOOR_206",
    "ITKE_HK_DOOR_207",
    "ITKE_HK_DOOR_208",
    "ITKE_HK_DOOR_209",
    "ITKE_HK_DOOR_210",
    "ITKE_HK_DOOR_211",
    "ITKE_HK_DOOR_212",
    "ITKE_HK_DOOR_213",
    "ITKE_HK_DOOR_214",
    "ITKE_HK_DOOR_215",
    "ITKE_HK_DOOR_216",
    "ITKE_HK_DOOR_217",
    "ITKE_HK_DOOR_218",
    "ITKE_HK_DOOR_219",
    "ITKE_HK_DOOR_220",
    "ITKE_HK_DOOR_221",
    "ITKE_HK_DOOR_222",
    "ITKE_HK_DOOR_223",
    "ITKE_HK_DOOR_224",
    "ITKE_HK_DOOR_225",
    "ITKE_HK_DOOR_226",
    "ITKE_HK_DOOR_227",
    "ITKE_HK_DOOR_228",
    "ITKE_HK_DOOR_229",
    "ITKE_HK_DOOR_230",
    "ITKE_HK_DOOR_231",
    "ITKE_HK_DOOR_232",
    "ITKE_HK_DOOR_233",
    "ITKE_HK_DOOR_234",
    "ITKE_HK_DOOR_235",
    "ITKE_HK_DOOR_236",
    "ITKE_HK_DOOR_237",
    "ITKE_HK_DOOR_238",
    "ITKE_HK_DOOR_239",
    "ITKE_HK_DOOR_240",
    "ITKE_HK_DOOR_241",
    "ITKE_HK_DOOR_242",
    "ITKE_HK_DOOR_243",
    "ITKE_HK_DOOR_244",
    "ITKE_HK_DOOR_245",
    "ITKE_HK_DOOR_246",
    "ITKE_HK_DOOR_247",
    "ITKE_HK_DOOR_248",
    "ITKE_HK_DOOR_249",
    "ITKE_HK_DOOR_250",
    "ITKE_HK_DOOR_251",
    "ITKE_HK_DOOR_252",
    "ITKE_HK_DOOR_253",
    "ITKE_HK_DOOR_254",
    "ITKE_HK_DOOR_255",
    "ITKE_HK_DOOR_256",
    "ITKE_HK_DOOR_257",
    "ITKE_HK_DOOR_258",
    "ITKE_HK_DOOR_259",
    "ITKE_HK_DOOR_260",
    "ITKE_HK_DOOR_261",
    "ITKE_HK_DOOR_262",
    "ITKE_HK_DOOR_263",
    "ITKE_HK_DOOR_264",
    "ITKE_HK_DOOR_265",
    "ITKE_HK_DOOR_266",
    "ITKE_HK_DOOR_267",
    "ITKE_HK_DOOR_268",
    "ITKE_HK_DOOR_269",
    "ITKE_HK_DOOR_270",
    "ITKE_HK_DOOR_271",
    "ITKE_HK_DOOR_272",
    "ITKE_HK_DOOR_273",
    "ITKE_HK_DOOR_274",
    "ITKE_HK_DOOR_275",
    "ITKE_HK_DOOR_276",
    "ITKE_HK_DOOR_277",
    "ITKE_HK_DOOR_278",
    "ITKE_HK_DOOR_279",
    "ITKE_HK_DOOR_280",
    "ITKE_HK_DOOR_281",
    "ITKE_HK_DOOR_282",
    "ITKE_HK_DOOR_283",
    "ITKE_HK_DOOR_284",
    "ITKE_HK_DOOR_285",
    "ITKE_HK_DOOR_286",
    "ITKE_HK_DOOR_287",
    "ITKE_HK_DOOR_288",
    "ITKE_HK_DOOR_289",
    "ITKE_HK_DOOR_290",
    "ITKE_HK_DOOR_291",
    "ITKE_HK_DOOR_292",
    "ITKE_HK_DOOR_293",
    "ITKE_HK_DOOR_294",
    "ITKE_HK_DOOR_295",
    "ITKE_HK_DOOR_296",
    "ITKE_HK_DOOR_297",
    "ITKE_HK_DOOR_298",
    "ITKE_HK_DOOR_299",
    "ITKE_HK_DOOR_300",
    "ITKE_HK_DOOR_301",
    "ITKE_HK_DOOR_302",
    "ITKE_HK_DOOR_303",
    "ITKE_HK_DOOR_304",
    "ITKE_HK_DOOR_305",
    "ITKE_HK_DOOR_306",
    "ITKE_HK_DOOR_307",
    "ITKE_HK_DOOR_308",
    "ITKE_HK_DOOR_309",
    "ITKE_HK_DOOR_310",
    "ITKE_HK_DOOR_311",
    "ITKE_HK_DOOR_312",
    "ITKE_HK_DOOR_313",
    "ITKE_HK_DOOR_314",
    "ITKE_HK_DOOR_315",
    "ITKE_HK_DOOR_316",
    "ITKE_HK_DOOR_317",
    "ITKE_HK_DOOR_318",
    "ITKE_HK_DOOR_319",
    "ITKE_HK_DOOR_320",
    "ITKE_HK_DOOR_321",
    "ITKE_HK_DOOR_322",
    "ITKE_HK_DOOR_323",
    "ITKE_HK_DOOR_324",
    "ITKE_HK_DOOR_325",
    "ITKE_HK_DOOR_326",
    "ITKE_HK_DOOR_327",
    "ITKE_HK_DOOR_328",
    "ITKE_HK_DOOR_329",
    "ITKE_HK_DOOR_330",
    "ITKE_HK_DOOR_331",
    "ITKE_HK_DOOR_332",
    "ITKE_HK_DOOR_333",
    "ITKE_HK_DOOR_334",
    "ITKE_HK_DOOR_335",
    "ITKE_HK_DOOR_336",
    "ITKE_HK_DOOR_337",
    "ITKE_HK_DOOR_338",
    "ITKE_HK_DOOR_339",
    "ITKE_HK_DOOR_340",
    "ITKE_HK_DOOR_341",
    "ITKE_HK_DOOR_342",
    "ITKE_HK_DOOR_343",
    "ITKE_HK_DOOR_344",
    "ITKE_HK_DOOR_345",
    "ITKE_HK_DOOR_346",
    "ITKE_HK_DOOR_347",
    "ITKE_HK_DOOR_348",
    "ITKE_HK_DOOR_349",
    "ITKE_HK_DOOR_350",
    "ITKE_HK_DOOR_351",
    "ITKE_HK_DOOR_352",
    "ITKE_HK_DOOR_353",
    "ITKE_HK_DOOR_354",
    "ITKE_HK_DOOR_355",
    "ITKE_HK_DOOR_356",
    "ITKE_HK_DOOR_357",
    "ITKE_HK_DOOR_358",
    "ITKE_HK_DOOR_359",
    "ITKE_HK_DOOR_360",
    "ITKE_HK_DOOR_361",
    "ITKE_HK_DOOR_362",
    "ITKE_HK_DOOR_363",
    "ITKE_HK_DOOR_364",
    "ITKE_HK_DOOR_365",
    "ITKE_HK_DOOR_366",
    "ITKE_HK_DOOR_367",
    "ITKE_HK_DOOR_368",
    "ITKE_HK_DOOR_369",
    "ITKE_HK_DOOR_370",
    "ITKE_HK_DOOR_371",
    "ITKE_HK_DOOR_372",
    "ITKE_HK_DOOR_373",
    "ITKE_HK_DOOR_374",
    "ITKE_HK_DOOR_375",
    "ITKE_HK_DOOR_376",
    "ITKE_HK_DOOR_377",
    "ITKE_HK_DOOR_378",
    "ITKE_HK_DOOR_379",
    "ITKE_HK_DOOR_380",
    "ITKE_HK_DOOR_381",
    "ITKE_HK_DOOR_382",
    "ITKE_HK_DOOR_383",
    "ITKE_HK_DOOR_384",
    "ITKE_HK_DOOR_385",
    "ITKE_HK_DOOR_386",
    "ITKE_HK_DOOR_387",
    "ITKE_HK_DOOR_388",
    "ITKE_HK_DOOR_389",
    "ITKE_HK_DOOR_390",
    "ITKE_HK_DOOR_391",
    "ITKE_HK_DOOR_392",
    "ITKE_HK_DOOR_393",
    "ITKE_HK_DOOR_394",
    "ITKE_HK_DOOR_395",
    "ITKE_HK_DOOR_396",
    "ITKE_HK_DOOR_397",
    "ITKE_HK_DOOR_398",
    "ITKE_HK_DOOR_399",
    "ITKE_HK_DOOR_400",
    "ITRU_FIREBOLT",
    "ITRU_ICEBOLT",
    "ITRU_LIGHT",
    "ITRU_INSTANTFIREBALL",
    "ITRU_ICELANCE",
    "ITRU_LIGHTHEAL",
    "ITRU_CHARGEFIREBALL",
    "ITRU_HEALOTHER",
]

// Notification Board

Config["NotificationBoard"] <- [
    {
        x = 3378.750000,
        y = -1368.046875,
        z = 1707.890625,
        angle = 194,
        corners = [
            {x = 3442.87, y = -1204.3, z = 1617.13},
            {x = 3444.37, y = -1388.24, z = 1625.49},
            {x = 3259.39, y = -1197.3, z = 1676.31},
            {x = 3270.55, y = -1413.25, z = 1677.01},
        ],
        size = [5, 7],
        camera = {x = 3583.43, y = -1258.5, z = 1900.86, rotx = 194},
    },
    {
        x = -5470.97,
        y = -957.175,
        z = 3856.19,
        angle = 51,
        corners = [
            {x = -5568.04, y = -817.088, z = 4093.8},
            {x = -5572.23, y = -990.751, z = 4080.19},
            {x = -5377.36, y = -769.086, z = 3668.35},
            {x = -5382.65, y = -947.308, z = 3664.08},
        ],
        size = [10, 5],
        camera = {x = -5922.86, y = -861.36, z = 3786.44, rotx = 51},
    },
]

Config["Questions"] <- [
    {
        id = 1,
        question = "Kt�rego z czat�w powiniene� u�y�, by napisa� co� jako twoja posta�?",
        answers = [
            ["Czatu ALL", false],
            ["Czatu OOC", false],
            ["Czatu IC", true],
            ["Czatu RP", false],
        ]
    },
    {
        id = 2,
        question = "Kt�rego z czat�w powiniene� u�y�, by spyta� si� kolegi o prac� domow� do szko�y?",
        answers = [
            ["Czatu ALL", false],
            ["Czatu OOC", true],
            ["Czatu IC", false],
            ["Czatu RP", false],
        ]
    },
    {
        id = 3,
        question = "Kt�rego z czat�w powiniene� u�y�, by wyrazi� niesmak do matki innego gracza?",
        answers = [
            ["Czatu IC, nie zapominaj�c r�wnie� o ojcu", false],
            ["�adnego. Je�li ju� chc� co� wyja�ni�, to we w�asnym zakresie lub poprzez ticketa \nze skarg� do administracji", true],
            ["Czatu OOC, lecz powinienem wyrazi� niesmak do osoby gracza, a nie jego matki", false],
            ["Nie u�yje do tego czatu w grze, lecz oficjalnego Discorda HK", false],
        ]
    },
    {
        id = 4,
        question = "Kolega w prywatnej wiadomo�ci wys�a� Ci informacje na temat planowanej zasadzce na twoj� posta�. \nCo powiniene� z t� informacj� zrobi�?",
        answers = [
            ["Wykorzystam t� wiadomo��, by uciec. Tylko przed tym odegram, �e dosta�em \ngo��bia pocztowego", false],
            ["Jest to MetaGaming, wi�c nie powinienem u�ywa� tej informacji, a koleg� \nuprzedzi� na przysz�o��", true],
            ["Jest to PowerGaming, wi�c nie powinienem u�ywa� tej informacji, a koleg� \nuprzedzi� na przysz�o��", false],
            ["Jest to CombatLog, wi�c nie powinienem u�ywa� tej informacji, a koleg� \nuprzedzi� na przysz�o��", false],
        ]
    },
    {
        id = 5,
        question = "Co to jest MG, czyli MetaGaming?",
        answers = [
            ["Wykorzystywanie informacji, kt�re zosta�y zdobyte drog� OOC, w �wiecie gry (IC). \nAbsolutnie zakazane", true],
            ["Zmuszenie kogo� do akcji RP, narzucanie komu� danej gry, akcj� RP. Np. /me rzuca w \nniego no�em, tak by zgin�� itp. Absolutnie zakazane", false],
            ["Wychodzenie z roli, b��dne odegranie akcji RP", false],
            ["Wyj�cie z gry podczas walki, oraz akcji RP w celu unikni�cia konsekwencji", false],
        ]
    },
    {
        id = 6,
        question = "Nowy gracz zacz�� Ci� uderza� broni� obuchow� bez odegrania. Jest to \nprzyk�ad jakiego� poj�cia RP?",
        answers = [
            ["Spawn Kill", false],
            ["Combat Gaming (CG)", false],
            ["Deathmatch (DM)", false],
            ["Random Deathmatch (RDM)", true],
        ]
    },
    {
        id = 7,
        question = "Czy skoczenie do stogu siana z klifu sprawia, �e twoja posta� prze�yje?",
        answers = [
            ["Tak, je�li napisz� KP assasyna", false],
            ["Tak, je�li mam lekki pancerz, inaczej nie", false],
            ["Zale�y od wysoko�ci, ale z regu�y nie. Moja posta� to zwyk�y cz�owiek, a nie bohater gry akcji", true],
            ["Zale�y czy dobrze wykonam fiko�a na /me, to nie takie trudne", false],
        ]
    },
    {
        id = 8,
        question = "Chcesz kogo� zaatakowa�, jakie warunki musisz spe�ni�?",
        answers = [
            ["Atakujemy si�, gdy obie strony wyci�gn� bro�", false],
            ["Napisz� narracj� na me i/lub do. Zaatakuj�, gdy druga strona te� wyci�gnie bro�.\nNie dotyczy akcji dynamicznych, np. zasadzek", true],
            ["W ka�dej sytuacji atakuje od razu, bo to akcja dynamiczna", false],
            ["Napisz� narracj� na me i/lub do. Zaatakuj�, gdy druga strona te� wyci�gnie bro�", false],
        ]
    },
    {
        id = 9,
        question = "Co to jest PG, czyli PowerGaming?",
        answers = [
            ["Wykorzystywanie informacji, kt�re zosta�y zdobyte drog� OOC, w �wiecie gry (IC). \nAbsolutnie zakazane", false],
            ["Zmuszenie kogo� do akcji RP, narzucanie komu� danej gry, akcj� RP. Np. /me rzuca w niego no�em,\n tak by zgin�� itp. Absolutnie zakazane", true],
            ["Wychodzenie z roli, b��dne odegranie akcji RP", false],
            ["Pr�ba zemsty na zab�jcy swojej poprzedniej postaci. Nienaturalna, niezgodna z akcj� pr�ba zemsty.", false],
        ]
    },
    {
        id = 10,
        question = "Co to jest BW, czyli Brutally Wounded?",
        answers = [
            ["omdlenie, czy odniesienie powa�nych ran uniemo�liwiaj�cych poruszanie si� (pasek HP schodzi do 0)", true],
            ["Atakowanie ka�dego wok�, bez odegrania, bez przygotowania obu stron. \nJest to umy�lne atakowanie innych os�b bez powodu IC", false],
            ["Losowe skakanie, szczeg�lnie skakanie po dachach itp., by szybciej gdzie� dotrze�. \nAbsolutnie zakazane", false],
            ["Pr�ba zemsty na zab�jcy swojej poprzedniej postaci. Nienaturalna, niezgodna z \nakcj� pr�ba zemsty.", false],
        ]
    },
    {
        id = 11,
        question = "Co to jest CK, czyli Character Kill?",
        answers = [
            ["Niewymuszone u�miercenie postaci", true],
            ["Wymuszone u�miercenie postaci przez inn� osob�, ni� w�a�ciciel postaci, \nza pozwoleniem administracji.", false],
            ["Atakowanie gracza", false],
            ["Nijaczenie postaci", false],
        ]
    },
    {
        id = 12,
        question = "Co to jest FCK, czyli Forced Character Kill?",
        answers = [
            ["Niewymuszone u�miercenie postaci", false],
            ["Wymuszone u�miercenie postaci przez inn� osob�, ni� w�a�ciciel postaci, \nza pozwoleniem administracji  ", true],
            ["Atakowanie gracza", false],
            ["Nijaczenie postaci", false],
        ]
    },
    {
        id = 13,
        question = "Co to jest DM, czyli DeathMatch?",
        answers = [
            ["Losowe atakowanie ka�dego wok�, bez odegrania, bez przygotowania obu stron. Jest \nto umy�lne atakowanie innych os�b z zamiarem wbicia BW, bez powodu IC Absolutnie zakazane", false],
            ["Wymuszone u�miercenie postaci przez inn� osob�, ni� w�a�ciciel postaci, za \npozwoleniem administracji", false],
            ["Walka skryptowa, mechaniczna. Czyli zwyczajna walka znana nam z singleplayer", true],
            ["Pr�ba zemsty na zab�jcy swojej poprzedniej postaci. Nienaturalna, niezgodna z \nakcj� pr�ba zemsty", false],
        ]
    },
    {
        id = 14,
        question = "Co to jest RDM, czyli Random DeathMatch?",
        answers = [
            ["Losowe atakowanie ka�dego wok�, bez odegrania, bez przygotowania obu stron. Jest \nto umy�lne atakowanie innych os�b z zamiarem wbicia BW, bez powodu IC Absolutnie zakazane", true],
            ["Wymuszone u�miercenie postaci przez inn� osob�, ni� w�a�ciciel postaci, za \npozwoleniem administracji", false],
            ["Walka skryptowa, mechaniczna. Czyli zwyczajna walka znana nam z singleplayer", false],
            ["Pr�ba zemsty na zab�jcy swojej poprzedniej postaci. Nienaturalna, niezgodna z \nakcj� pr�ba zemsty", false],
        ]
    },
    {
        id = 15,
        question = "Co to jest CN, czyli Celebrity Name?",
        answers = [
            ["Nazwanie swojej postaci Exodus lub Szakal. Absolutnie zakazane", false],
            ["Nazwanie swojej postaci niezgodnie z uniwersum Gothic, szczeg�lnie imieniem \nosoby znanej. Np. Justin Bieber. Absolutnie zakazane", true],
            ["Zaklepanie imienia postaci na serwerze", false],
            ["Tzw. leakowanie danych osobowych innego gracza. Absolutnie zakazane, mo�e \nsi� sko�czy� s�dem", false],
        ]
    },
    {
        id = 16,
        question = "Co to jest BH, czyli Bunny Hop?",
        answers = [
            ["U�ywanie nazwy jakiego� zwierz�cia jako imi� postaci. Np. Kr�lik, zaj�c", false],
            ["U�ywanie kamery trzecioosobowej do akcji RP. Wychylanie zza rogu itp., \nby by� niewidocznym. Absolutnie zakazane", false],
            ["Nienaturalne u�ywanie skakania, szczeg�lnie skakanie po dachach itp., \nby szybciej dotrze� w dane miejsce. Absolutnie zakazane", true],
            ["Omdlenie, czy odniesienie powa�nych ran uniemo�liwiaj�cych poruszanie \nsi� (pasek HP schodzi do 0)", false],
        ]
    },
    {
        id = 17,
        question = "Czy w ci�kim pancerzu mo�esz wykonywa� czynno�ci takie jak \nwspinanie si� po g�rach, czy p�ywanie?",
        answers = [
            ["Tak", false],
            ["Nie, mog� tylko p�ywa�", false],
            ["Tak, lecz wi��e si� to z konsekwencjami IC", true],
            ["Nie, mog� tylko chodzi� po g�rach", false],
        ]
    },
    {
        id = 18,
        question = "Co to jest CL, czyli Combat Log?",
        answers = [
            ["Wyj�cie z gry podczas walki, oraz akcji RP w celu unikni�cia konsekwencji", true],
            ["Walka skryptowa, mechaniczna. Czyli zwyczajna walka znana nam z singleplayer", false],
            ["Jest to umy�lne atakowanie innych os�b z zamiarem wbicia BW, \nbez powodu IC. Absolutnie zakazane", false],
            ["Logi ka�dego uderzenia innego gracza, dost�pne tylko dla administracji", false],
        ]
    },
    {
        id = 19,
        question = "Co to jest RK, czyli Revenge Kill?",
        answers = [
            ["Wyj�cie z gry podczas walki, oraz akcji RP w celu unikni�cia konsekwencji", false],
            ["Wychodzenie z roli, b��dne odegranie akcji RP", false],
            ["�mier� postaci", false],
            ["Pr�ba zemsty na zab�jcy swojej poprzedniej postaci. Nienaturalna, niezgodna\n z akcj� pr�ba zemsty. Absolutnie zakazane", true],
        ]
    },
    {
        id = 20,
        question = "Co to jest Cam Spying?",
        answers = [
            ["U�ywanie kamery trzecioosobowej do akcji RP, takich jak wychylanie zza rogu\n kamery bez pokazania postaci, zapominanie, �e nasza posta� nie ma oczu dooko�a g�owy. Absolutnie zakazane", true],
            ["Wychodzenie z roli, b��dne odegranie akcji RP", false],
            ["Nadmierne poruszanie si� z �ukiem, kusz�, magi�, by namierzy� kogo� np. w krzakach", false],
            ["Odgrywanie kamer w Gothicu. Absolutnie zakazane", false],
        ]
    },
    {
        id = 21,
        question = "Co to jest Fail RP?",
        answers = [
            ["Wyj�cie z gry podczas walki, oraz akcji RP w celu unikni�cia konsekwencji", false],
            ["losowe skakanie, szczeg�lnie skakanie po dachach itp., by szybciej gdzie�\n dotrze�. \nAbsolutnie zakazane", false],
            ["Wykorzystywanie informacji, kt�re zosta�y zdobyte drog� OOC, w �wiecie gry (IC). \nAbsolutnie zakazane", false],
            ["Wychodzenie z roli, b��dne odegranie akcji RP", true],
        ]
    },
    {
        id = 22,
        question = "Co to jest Scouting?",
        answers = [
            ["U�ywanie kamery trzecioosobowej do akcji RP, takich jak wychylanie zza rogu \nkamery bez pokazania postaci, zapominanie, �e nasza posta� nie ma oczu dooko�a g�owy. Absolutnie zakazane", false],
            ["Wykorzystywanie informacji, kt�re zosta�y zdobyte drog� OOC, w �wiecie gry (IC). \nAbsolutnie zakazane", false],
            ["losowe skakanie, szczeg�lnie skakanie po dachach itp., by szybciej gdzie� dotrze�. \nAbsolutnie zakazane", false],
            ["Nadmierne poruszanie si� z �ukiem, kusz�, magi�, by namierzy� kogo� np. w krzakach", true],
        ]
    },
    {
        id = 0,
        question = "Co to jest Ghost Town (Miasto Duch�w)?",
        answers = [
            ["B��dne zak�adanie przez ludzi, �e miejsca fabularne zaludnione, takie jak miasta \npomimo braku graczy s� puste", true],
            ["Wykorzystywanie informacji, kt�re zosta�y zdobyte drog� OOC, w �wiecie gry (IC). \nAbsolutnie zakazane", false],
            ["U�ywanie kamery trzecioosobowej do akcji RP, takich jak wychylanie zza rogu kamery \nbez pokazania postaci, zapominanie, �e nasza posta� nie ma oczu dooko�a g�owy. Absolutnie zakazane", false],
            ["Nadmierne poruszanie si� z �ukiem, kusz�, magi�, by namierzy� kogo� np. w krzakach", false],
        ]
    }
];

Config["Calendar"] <- {};
Config["Calendar"].yearDays <- 305;
Config["Calendar"]["Seasons"] <- ["�niwa Innosa","Ciemno�� Beliara","R�wnowaga Adannosa"]
Config["Calendar"]["�niwa Innosa"] <- {
    months = [30, 31, 30, 31, 30],
    buffToHerbGrowth = 10,
    buffToMineGrowth = 0,
    buffToForestGrowth = 5,
    buffToFishGrowth = 0,
};
Config["Calendar"]["Ciemno�� Beliara"] <- {
    months = [31, 30, 31],
    buffToHerbGrowth = 0,
    buffToMineGrowth = 10,
    buffToForestGrowth = 0,
    buffToFishGrowth = 0,
};
Config["Calendar"]["R�wnowaga Adannosa"] <- {
    months = [30, 31],
    buffToHerbGrowth = 10,
    buffToMineGrowth = 0,
    buffToForestGrowth = 10,
    buffToFishGrowth = 10,
};

Config["PlayerRecovery"] <- [
{ name = "CHAIR_1_NC.ASC", value = 0.015 },
{ name = "BENCH_1_NC.ASC", value = 0.015 },
{ name = "CHAIR_1_PC.ASC", value = 0.015 },
{ name = "CHAIR_3_PC.ASC", value = 0.020 },
{ name = "CHAIR_1_OC.ASC", value = 0.020 },
{ name = "CHAIR_2_OC.ASC", value = 0.020 },
{ name = "CHAIR_3_OC.ASC", value = 0.020 },
{ name = "CHAIR_NW_NORMAL_01.ASC", value = 0.020 },
{ name = "CHAIR_NW_EDEL_01.ASC", value = 0.020 },
{ name = "BENCH_1_NC.ASC", value = 0.020 },
{ name = "BENCH_1_OC.ASC", value = 0.020 },
{ name = "BENCH_2_OC.ASC", value = 0.020 },
{ name = "BENCH_NW_OW_01.ASC", value = 0.020 },
{ name = "BENCH_NW_CITY_01.ASC", value = 0.020 },
{ name = "BENCH_NW_CITY_02.ASC", value = 0.020 },
{ name = "BENCH_NW_CITY_01.ASC", value = 0.020 },
{ name = "BENCH_NW_CITY_01.ASC", value = 0.020 },
{ name = "BENCH_3_OC.ASC", value = 0.020 },
{ name = "BENCH_THRONE.ASC", value = 0.025 },
{ name = "THRONE_NW_CITY_01.ASC", value = 0.030 },
{ name = "BEDHIGH_PSI.ASC", value = 0.085 },
{ name = "THRONE_BIG.ASC", value = 0.030 },
{ name = "BEDHIGH_PC.ASC", value = 0.085 },
{ name = "BEDHIGH_1_OC.ASC", value = 0.1 },
{ name = "BEDHIGH_3_OC.ASC", value = 0.13 },
{ name = "BEDHIGH_4_OC.ASC", value = 0.13 },
{ name = "SMOKE_WATERPIPE.MDS", value = 0.13},
{ name = "BEDHIGH_2_OC.ASC", value = 0.2 },
{ name = "BATHTUB_WOODEN_BIGGER.ASC", value = 0.2 },
{ name = "BEDHIGH_NW_NORMAL_01.ASC", value = 0.25 },
{ name = "BEDHIGH_NW_EDEL_01.ASC", value = 0.3 },
{ name = "BEDHIGH_NW_MASTER_01.ASC", value = 0.3 }
{ name = "BEDHIGH_LOVE_OC.ASC", value = 0.3 }
];

Config["Barrier"] <- [
    [ 57939.2, 1280.28, ],
    [ 55954.4, 5421.51, ],
    [ 52856.8, 10047, ],
    [ 49451.9, 14908.2, ],
    [ 44199.8, 20513.3, ],
    [ 37684.2, 26271.2, ],
    [ 30434, 31462.4, ],
    [ 25573.6, 32692.7, ],
    [ 21248.3, 35176.1, ],
    [ 19450.7, 35205, ],
    [ 16263.1, 32799.6, ],
    [ 10755.6, 34744.4, ],
    [ 9736.9, 37990.5, ],
    [ 8218.6, 38393.1, ],
    [ 4065, 39018.4, ],
    [ 839.9, 39079.3, ],
    [ -9312.9, 38694.2, ],
    [ -19258.3, 40991.4, ],

    [ -28570.3,47572, ],
    //[ -29684.1, 40535.7, ],
    //[ -42060.9, 41084, ],

    //[ -39313.7, 36558.8, ],
    [ -49319.6, 31970.2, ],
    [ -54137.3, 26761.7, ],
    [ -62089.3, 21598.1, ],
    [ -66193.7, 12999.2, ],
    [ -66132.3, 6204, ],
    [ -63855.2, -5700.8, ],
    [ -59385.1, -10081.5, ],
    [ -56013.8, -22393.4, ],
    [ -47250.3, -28502, ],
    [ -37136.5, -38319.2, ],
    [ -24664.7, -46687.9, ],
    [ -7860.6, -48966.6, ],
    [ 4876.6, -49691, ],
    [ 23147.8, -47875.1, ],
    [ 48722.3, -39488.8, ],
    [ 55902.4, -31909.8, ],
    [ 61238.6, -23412.8, ],
    [ 60230.1, -6641.9, ],
];

Config["TwistedVobs"] <- [
    "CHESTSMALL_NW_POOR_LOCKED.MDS",
    "CHESTBIG_NW_NORMAL_LOCKED.MDS",
    "CHESTBIG_NW_RICH_LOCKED.MDS",
    "CHESTSMALL_OCCHESTSMALL.MDS",
    "CHESTSMALL_OCCHESTSMALLLOCKED.MDS",
    "CHESTBIG_OCCHESTLARGELOCKED.MDS",
    "CHESTSMALL_NW_POOR_LOCKED.MDS",
    "CHESTSMALL_NW_POOR_OPEN.MDS",
    "CHESTBIG_OCCHESTMEDIUM.MDS",
    "CHESTSMALL_OCCRATESMALLLOCKED.MDS",
    "CHESTBIG_OCCRATELARGELOCKED.MDS",
    "CHESTBIG_NW_RICH_OPEN.MDS",
    "CHESTBIG_NW_RICH_LOCKED.ASC",
    "CHESTBIG_NW_RICH_LOCKED.ASC",
];

Config["CraftVobRelation"] <- {
    "BOOK_BLUE.ASC": {canBeInteractedWith = true, lockable = false, combination = "RLRL", destroyable = false, health = 10, canbechest = false, weight = 40.50}
    "BOOK_NW_CITY_CUPBOARD_01.ASC": {canBeInteractedWith = true, lockable = false, combination = "RLRL", destroyable = false, health = 10, canbechest = false, weight = 40.50}
    "THRONE_NW_CITY_01.ASC": {canBeInteractedWith = true, lockable = false, combination = "RLRL", destroyable = false, health = 10, canbechest = false, weight = 40.50}
    "THRONE_BIG.ASC": {canBeInteractedWith = true, lockable = false, combination = "RLRL", destroyable = false, health = 10, canbechest = false, weight = 40.50}
    "BEDHIGH_PSI.ASC": {canBeInteractedWith = true, lockable = false, combination = "RLRL", destroyable = false, health = 10, canbechest = false, weight = 40.50}
    "BEDHIGH_PC.ASC": {canBeInteractedWith = true, lockable = false, combination = "RLRL", destroyable = false, health = 10, canbechest = false, weight = 40.50}
    "BEDHIGH_1_OC.ASC": {canBeInteractedWith = true, lockable = false, combination = "RLRL", destroyable = false, health = 10, canbechest = false, weight = 40.50}
    "BEDHIGH_3_OC.ASC": {canBeInteractedWith = true, lockable = false, combination = "RLRL", destroyable = false, health = 10, canbechest = false, weight = 40.50}
    "BEDHIGH_4_OC.ASC": {canBeInteractedWith = true, lockable = false, combination = "RLRL", destroyable = false, health = 10, canbechest = false, weight = 40.50}
    "BEDHIGH_2_OC.ASC": {canBeInteractedWith = true, lockable = false, combination = "RLRL", destroyable = false, health = 10, canbechest = false, weight = 40.50}
    "BEDHIGH_NW_NORMAL_01.ASC": {canBeInteractedWith = true, lockable = false, combination = "RLRL", destroyable = false, health = 10, canbechest = false, weight = 40.50}
    "BEDHIGH_NW_EDEL_01.ASC": {canBeInteractedWith = true, lockable = false, combination = "RLRL", destroyable = false, health = 10, canbechest = false, weight = 40.50}
    "BEDHIGH_NW_MASTER_01.ASC": {canBeInteractedWith = true, lockable = false, combination = "RLRL", destroyable = false, health = 10, canbechest = false, weight = 40.50}
    "CHESTSMALL_NW_POOR_LOCKED.MDS": {canBeInteractedWith = true, lockable = true, combination = "RLRLRL", destroyable = false, health = 10, canbechest = true, weight = 300}
    "CHESTBIG_NW_NORMAL_LOCKED.MDS": {canBeInteractedWith = true, lockable = true, combination = "RLRLRLRL", destroyable = false, health = 10, canbechest = true, weight = 500}
    "CHESTBIG_NW_RICH_LOCKED.MDS": {canBeInteractedWith = true, lockable = true, combination = "RLRLRLRLRL", destroyable = false, health = 10, canbechest = true, weight = 500}
    "CHESTSMALL_OCCHESTSMALLLOCKED.MDS": {canBeInteractedWith = true, lockable = true, combination = "RLRLRLR", destroyable = false, health = 10, canbechest = true, weight = 300}
    "CHESTBIG_OCCHESTLARGELOCKED.MDS": {canBeInteractedWith = true, lockable = true, combination = "RLRLRLR", destroyable = false, health = 10, canbechest = true, weight = 500}
    "CHESTSMALL_OCCRATESMALLLOCKED.MDS": {canBeInteractedWith = true, lockable = true, combination = "RLRLR", destroyable = false, health = 10, canbechest = true, weight = 300}
    "CHESTBIG_OCCRATELARGELOCKED.MDS": {canBeInteractedWith = true, lockable = true, combination = "RLRLR", destroyable = false, health = 10, canbechest = true, weight = 500}
    "LADDER_2.ASC": {canBeInteractedWith = true, lockable = false, combination = "RLRL", destroyable = false, health = 10, canbechest = false, weight = 40.50}
    "LADDER_3.ASC": {canBeInteractedWith = true, lockable = false, combination = "RLRL", destroyable = false, health = 10, canbechest = false, weight = 40.50}
    "LADDER_4.ASC": {canBeInteractedWith = true, lockable = false, combination = "RLRL", destroyable = false, health = 10, canbechest = false, weight = 40.50}
    "SMOKE_WATERPIPE.MDS": {canBeInteractedWith = true, lockable = false, combination = "RLRL", destroyable = false, health = 10, canbechest = false, weight = 40.50}
    "BENCH_THRONE.ASC": {canBeInteractedWith = true, lockable = false, combination = "RLRL", destroyable = false, health = 10, canbechest = false, weight = 40.50}
    "BENCH_3_OC.ASC": {canBeInteractedWith = true, lockable = false, combination = "RLRL", destroyable = false, health = 10, canbechest = false, weight = 40.50}
    "BENCH_1_NC.ASC": {canBeInteractedWith = true, lockable = false, combination = "RLRL", destroyable = false, health = 10, canbechest = false, weight = 40.50}
    "BENCH_1_OC.ASC": {canBeInteractedWith = true, lockable = false, combination = "RLRL", destroyable = false, health = 10, canbechest = false, weight = 40.50}
    "BENCH_2_OC.ASC": {canBeInteractedWith = true, lockable = false, combination = "RLRL", destroyable = false, health = 10, canbechest = false, weight = 40.50}
    "BENCH_NW_OW_01.ASC": {canBeInteractedWith = true, lockable = false, combination = "RLRL", destroyable = false, health = 10, canbechest = false, weight = 40.50}
    "BENCH_NW_CITY_01.ASC": {canBeInteractedWith = true, lockable = false, combination = "RLRL", destroyable = false, health = 10, canbechest = false, weight = 40.50}
    "BENCH_NW_CITY_02.ASC": {canBeInteractedWith = true, lockable = false, combination = "RLRL", destroyable = false, health = 10, canbechest = false, weight = 40.50}
    "INNOS_NW_MISC_01.ASC": {canBeInteractedWith = true, lockable = false, combination = "RLRL", destroyable = false, health = 10, canbechest = false, weight = 40.50}
    "CHAIR_2_NC.ASC": {canBeInteractedWith = true, lockable = false, combination = "RLRL", destroyable = false, health = 10, canbechest = false, weight = 40.50}
    "CHAIR_1_NC.ASC": {canBeInteractedWith = true, lockable = false, combination = "RLRL", destroyable = false, health = 10, canbechest = false, weight = 40.50}
    "CHAIR_1_PC.ASC": {canBeInteractedWith = true, lockable = false, combination = "RLRL", destroyable = false, health = 10, canbechest = false, weight = 40.50}
    "CHAIR_3_PC.ASC": {canBeInteractedWith = true, lockable = false, combination = "RLRL", destroyable = false, health = 10, canbechest = false, weight = 40.50}
    "CHAIR_1_OC.ASC": {canBeInteractedWith = true, lockable = false, combination = "RLRL", destroyable = false, health = 10, canbechest = false, weight = 40.50}
    "CHAIR_2_OC.ASC": {canBeInteractedWith = true, lockable = false, combination = "RLRL", destroyable = false, health = 10, canbechest = false, weight = 40.50}
    "CHAIR_3_OC.ASC": {canBeInteractedWith = true, lockable = false, combination = "RLRL", destroyable = false, health = 10, canbechest = false, weight = 40.50}
    "CHAIR_NW_NORMAL_01.ASC": {canBeInteractedWith = true, lockable = false, combination = "RLRL", destroyable = false, health = 10, canbechest = false, weight = 40.50}
    "CHAIR_NW_EDEL_01.ASC": {canBeInteractedWith = true, lockable = false, combination = "RLRL", destroyable = false, health = 10, canbechest = false, weight = 40.50}
    "NW_MISC_FIELDGRAVE_01.3DS": {canBeInteractedWith = false, lockable = false, combination = "RLRL", destroyable = false, health = 10, canbechest = false, weight = 40.50}
    "NW_MISC_GRAVESTONE_01.3DS": {canBeInteractedWith = false, lockable = false, combination = "RLRL", destroyable = false, health = 10, canbechest = false, weight = 40.50}
    "NW_MISC_GRAVESTONE_02.3DS": {canBeInteractedWith = false, lockable = false, combination = "RLRL", destroyable = false, health = 10, canbechest = false, weight = 40.50}
    "NW_CITY_CUPBOARD_POOR_01.3DS": {canBeInteractedWith = false, lockable = false, combination = "RLRL", destroyable = false, health = 10, canbechest = true, weight = 40.50}
    "NW_CITY_CUPBOARD_POOR_02.3DS": {canBeInteractedWith = false, lockable = false, combination = "RLRL", destroyable = false, health = 10, canbechest = true, weight = 40.50}
    "NW_CITY_TABLE_PEASANT_01.3DS": {canBeInteractedWith = false, lockable = false, combination = "RLRL", destroyable = false, health = 10, canbechest = false, weight = 40.50}
    "NW_CITY_TABLE_NORMAL_01.3DS": {canBeInteractedWith = false, lockable = false, combination = "RLRL", destroyable = false, health = 10, canbechest = false, weight = 40.50}
    "NW_CITY_TABLE_RICH_01.3DS": {canBeInteractedWith = false, lockable = false, combination = "RLRL", destroyable = false, health = 10, canbechest = false, weight = 40.50}
    "NW_CITY_TABLE_RICH_02.3DS": {canBeInteractedWith = false, lockable = false, combination = "RLRL", destroyable = false, health = 10, canbechest = false, weight = 40.50}
    "OC_TABLE_V2.3DS": {canBeInteractedWith = false, lockable = false, combination = "RLRL", destroyable = false, health = 10, canbechest = false, weight = 40.50}
    "OM_ORETABLE.3DS": {canBeInteractedWith = false, lockable = false, combination = "RLRL", destroyable = false, health = 10, canbechest = false, weight = 40.50}
    "PC_LOB_TABLE1.3DS": {canBeInteractedWith = false, lockable = false, combination = "RLRL", destroyable = false, health = 10, canbechest = false, weight = 40.50}
    "PC_LOB_TABLE2.3DS": {canBeInteractedWith = false, lockable = false, combination = "RLRL", destroyable = false, health = 10, canbechest = false, weight = 40.50}
    "BSANVIL_OC.MDS": {canBeInteractedWith = true, lockable = false, combination = "RLRL", destroyable = false, health = 10, canbechest = false, weight = 40.50}
    "BSCOOL_OC.MDS": {canBeInteractedWith = true, lockable = false, combination = "RLRL", destroyable = false, health = 10, canbechest = false, weight = 40.50}
    "BSSHARP_OC.MDS": {canBeInteractedWith = true, lockable = false, combination = "RLRL", destroyable = false, health = 10, canbechest = false, weight = 40.50}
    "BARBQ_SCAV.MDS": {canBeInteractedWith = true, lockable = false, combination = "RLRL", destroyable = false, health = 10, canbechest = false, weight = 40.50}
    "CAULDRON_OC.ASC": {canBeInteractedWith = true, lockable = false, combination = "RLRL", destroyable = false, health = 10, canbechest = false, weight = 40.50}
    "STOVE_NW_CITY_01.ASC": {canBeInteractedWith = true, lockable = false, combination = "RLRL", destroyable = false, health = 10, canbechest = false, weight = 40.50}
    "PAN_OC.MDS": {canBeInteractedWith = true, lockable = false, combination = "RLRL", destroyable = false, health = 10, canbechest = false, weight = 40.50}
    "LAB_PSI.ASC": {canBeInteractedWith = true, lockable = false, combination = "RLRL", destroyable = false, health = 10, canbechest = false, weight = 40.50}
    "LATI_G1.MDS": {canBeInteractedWith = true, lockable = false, combination = "RLRL", destroyable = false, health = 10, canbechest = false, weight = 40.50}
    "ARMORSTAND_PLATTNER.ASC": {canBeInteractedWith = true, lockable = false, combination = "RLRL", destroyable = false, health = 10, canbechest = false, weight = 40.50}
    "BSMELTER_1.MDS": {canBeInteractedWith = true, lockable = false, combination = "RLRL", destroyable = false, health = 10, canbechest = false, weight = 40.50}
    "BSFIRE_OC.MDS": {canBeInteractedWith = true, lockable = false, combination = "RLRL", destroyable = false, health = 10, canbechest = false, weight = 40.50}
    "BSMELTER_1.MDS": {canBeInteractedWith = true, lockable = false, combination = "RLRL", destroyable = false, health = 10, canbechest = false, weight = 40.50}
    "WOODCHOPPIN_NORMAL.MDS": {canBeInteractedWith = true, lockable = false, combination = "RLRL", destroyable = false, health = 10, canbechest = false, weight = 40.50}
    "MOERS_MESH.ASC": {canBeInteractedWith = true, lockable = false, combination = "RLRL", destroyable = false, health = 10, canbechest = false, weight = 40.50}
    "BOWSDW_WBENCH.MDS": {canBeInteractedWith = true, lockable = false, combination = "RLRL", destroyable = false, health = 10, canbechest = false, weight = 40.50}
    "TISCHLER_MESH.MDS": {canBeInteractedWith = true, lockable = false, combination = "RLRL", destroyable = false, health = 10, canbechest = false, weight = 40.50}
    "WEBSTUHL_USE.MDS": {canBeInteractedWith = true, lockable = false, combination = "RLRL", destroyable = false, health = 10, canbechest = false, weight = 40.50}
    "SKINLAB_PLACE.MDS": {canBeInteractedWith = true, lockable = false, combination = "RLRL", destroyable = false, health = 10, canbechest = false, weight = 40.50}
    "MOERS_LEKARZ.ASC": {canBeInteractedWith = true, lockable = false, combination = "RLRL", destroyable = false, health = 10, canbechest = false, weight = 40.50}
    "SAEGEN_3.ASC": {canBeInteractedWith = true, lockable = false, combination = "RLRL", destroyable = false, health = 10, canbechest = false, weight = 40.50}
    "FISCHGRILLEN_FINAL.MDS": {canBeInteractedWith = true, lockable = false, combination = "RLRL", destroyable = false, health = 10, canbechest = false, weight = 40.50}
    "HERB_PSI.MDS": {canBeInteractedWith = true, lockable = false, combination = "RLRL", destroyable = false, health = 10, canbechest = false, weight = 40.50}
    "BSANVIL_OC2.MDS": {canBeInteractedWith = true, lockable = false, combination = "RLRL", destroyable = false, health = 10, canbechest = false, weight = 40.50}
    "LAB_PSI3.ASC": {canBeInteractedWith = true, lockable = false, combination = "RLRL", destroyable = false, health = 10, canbechest = false, weight = 40.50}
    "BUCHSCHREIBEN_01.ASC": {canBeInteractedWith = true, lockable = false, combination = "RLRL", destroyable = false, health = 10, canbechest = false, weight = 40.50}
    "HERB_NW_MISC_01.ASC": {canBeInteractedWith = true, lockable = false, combination = "RLRL", destroyable = false, health = 10, canbechest = false, weight = 40.50}
    "NW_CITY_MAP_WAR_CLOSED_01.3DS": {canBeInteractedWith = false, lockable = false, combination = "RLRL", destroyable = false, health = 10, canbechest = false, weight = 40.50}
    "OC_BARREL_V1.3DS": {canBeInteractedWith = false, lockable = false, combination = "RLRL", destroyable = false, health = 10, canbechest = true, weight = 30}
    "NW_HARBOUR_CRATE_01.3DS": {canBeInteractedWith = false, lockable = false, combination = "RLRL", destroyable = false, health = 10, canbechest = true, weight = 30}
    "NW_CITY_DECO_SHADOWHEAD_01.3DS": {canBeInteractedWith = false, lockable = false, combination = "RLRL", destroyable = false, health = 10, canbechest = false, weight = 30}
    "NW_CITY_DECO_TROLLHEAD_01.3DS": {canBeInteractedWith = false, lockable = false, combination = "RLRL", destroyable = false, health = 10, canbechest = false, weight = 30}
    "NW_CITY_DECO_SWORDFISH_01.3DS": {canBeInteractedWith = false, lockable = false, combination = "RLRL", destroyable = false, health = 10, canbechest = false, weight = 30}
    "NW_CITY_DECO_WOLFHEAD_01.3DS": {canBeInteractedWith = false, lockable = false, combination = "RLRL", destroyable = false, health = 10, canbechest = false, weight = 30}
    "OC_DECORATE_V2.3DS": {canBeInteractedWith = false, lockable = false, combination = "RLRL", destroyable = false, health = 10, canbechest = false, weight = 30}
    "BEDHIGH_LOVE_OC.ASC": {canBeInteractedWith = true, lockable = false, combination = "RLRL", destroyable = false, health = 10, canbechest = false, weight = 40.50}
    "FISCHSCHNEIDEN_RZEZNIK.ASC": {canBeInteractedWith = true, lockable = false, combination = "RLRL", destroyable = false, health = 10, canbechest = false, weight = 40.50}
    "BAKE_MESH.MDS": {canBeInteractedWith = true, lockable = false, combination = "RLRL", destroyable = false, health = 10, canbechest = false, weight = 40.50}
    "RMELTER_1.ASC": {canBeInteractedWith = true, lockable = false, combination = "RLRL", destroyable = false, health = 10, canbechest = false, weight = 40.50}
    "RMAKER_1.MDS": {canBeInteractedWith = true, lockable = false, combination = "RLRL", destroyable = false, health = 10, canbechest = false, weight = 40.50}
    "CAULDRON_OC2.ASC": {canBeInteractedWith = true, lockable = false, combination = "RLRL", destroyable = false, health = 10, canbechest = false, weight = 40.50}
    "BAUMSAEGE_1.ASC": {canBeInteractedWith = true, lockable = false, combination = "RLRL", destroyable = false, health = 10, canbechest = false, weight = 40.50}
    "STOVE_PO_OUTDOOR_01.ASC": {canBeInteractedWith = true, lockable = false, combination = "RLRL", destroyable = false, health = 10, canbechest = false, weight = 40.50}
    "STOMPER_OM.ASC": {canBeInteractedWith = true, lockable = false, combination = "RLRL", destroyable = false, health = 10, canbechest = false, weight = 40.50}
    "BSFIRE_PROJEKTO_AUSSEN_VAR1.MDS": {canBeInteractedWith = true, lockable = false, combination = "RLRL", destroyable = false, health = 10, canbechest = false, weight = 40.50}
}

Config["BedVobs"] <- [
    "BEDHIGH_PSI.ASC",
    "BEDHIGH_PC.ASC",
    "BEDHIGH_1_OC.ASC",
    "BEDHIGH_3_OC.ASC",
    "BEDHIGH_4_OC.ASC",
    "BEDHIGH_2_OC.ASC",
    "BEDHIGH_NW_NORMAL_01.ASC",
    "BEDHIGH_NW_EDEL_01.ASC",
    "BEDHIGH_NW_MASTER_01.ASC",
    "BEDHIGH_LOVE_OC.ASC"
]

Config["Rats"] <- {
    active = true, // is active module
    cooldown = 1800, // seconds after new rat will apear
    slots = 4, // max rats on server
    positions = [ // positions
        //[-3181.72,-671.719,3590.55],
        //[142.266,-1091.72,3074.3],
        //[-1303.52,-900.938,-6650.55],
        [1026.17,-771.844,4843.44],
       // [1576.64,-671.797,4705.78],
       // [1524.06,-671.797,5561.56],
        [-38768.7,44.1406,-5216.48],
        //[-7890,-441.797,-25.2344],
        [1718.36,-371.797,-6669.53],
        [-2780.7,-531.797,4560.7],
    ],
    items = [
        "ITCR_HK_DROP_37",
        "ITCR_HK_DROP_64",
        "ITCR_HK_DROP_38",
        "ITCR_HK_DROP_71",
        "ITCR_HK_DROP_36",
        "ITCR_HK_DROP_69",
        "ITCR_HK_DROP_81",
        "ITCR_HK_DROP_31",
        "ITCR_HK_DROP_66",
        "ITCR_HK_DROP_51",
        "ITCR_HK_DROP_45",
        "ITCR_HK_DROP_42",
        "ITCR_HK_DROP_33",
        "ITCR_HK_DROP_30",
        "ITCR_HK_DROP_49",
        "ITCR_HK_DROP_65",
        "ITCR_HK_DROP_63",
        "ITCR_HK_DROP_68",
        "ITCR_HK_DROP_39",
        "ITCR_HK_DROP_70",
        "ITCR_HK_DROP_43",
        "ITCR_HK_DROP_47",
        "ITCR_HK_DROP_40",
        "ITCR_HK_DROP_83",
        "ITCR_HK_DROP_84",
        "ITCR_HK_DROP_44",
        "ITCR_HK_DROP_08",
        "ITCR_HK_DROP_22",
        "ITCR_HK_DROP_77",
        "ITCR_HK_DROP_78",
        "ITCR_HK_DROP_06",
        "ITCR_HK_DROP_24",
        "ITCR_HK_DROP_41",
        "ITCR_HK_DROP_07"
    ],
}


/*
enum BuffType
{
    None,
    Custom,
    Attribute,
        attributeId = PlayerAttribute.Str
    Skill,
        skillId = PlayerSkill.Alchemy
    HealthReduce,
    HealthRegeneration,
    ManaReduce,
    ManaRegeneration,
    EnduranceRegeneration,
    StaminaRegeneration,
    WeightLimit,
    DamageBonus,
    DamageReduce,
    ProtectionBonus,
    ProtectionReduce,
    Mds,
}

*/
Config["Buffs"] <- {


"Adanos1": { //Na czas 30 min zwi�ksza Man� +100
        name = "Adanos1",
        type = BuffType.Attribute,
        attributeId = PlayerAttributes.Mana,
        value = 100,
        interval = 0,
        icon = "SHOULDER_EQ_ICON_R.TGA",
        time = 1800,
    },

"Adanos2": { //Na czas 30 min zwi�ksza stamin� +25
        name = "Adanos2",
        type = BuffType.Attribute,
        attributeId = PlayerAttributes.Stamina,
        value = 25,
        interval = 0,
        icon = "SHOULDER_EQ_ICON_R.TGA",
        time = 1800,
    },

"Innos1": { //Na czas 30 min zwi�ksza hp +50
        name = "Innos1",
        type = BuffType.Attribute,
        attributeId = PlayerAttributes.Hp,
        value = 50,
        interval = 0,
        icon = "SHOULDER_EQ_ICON_R.TGA",
        time = 1800,
    },

"Innos2": { //Na czas 30 min zwi�ksza si�� +5
        name = "Innos2",
        type = BuffType.Attribute,
        attributeId = PlayerAttributes.Str,
        value = 5,
        interval = 0,
        icon = "SHOULDER_EQ_ICON_R.TGA",
        time = 1800,
    },

"Innos3": { //Na czas 30 min zwi�ksza zr�czno�� +5
        name = "Innos3",
        type = BuffType.Attribute,
        attributeId = PlayerAttributes.Dex,
        value = 5,
        interval = 0,
        icon = "SHOULDER_EQ_ICON_R.TGA",
        time = 1800,
    },

"Wol": { //TIER 3 - zwi�ksza udzwig o 30 na 10 minut
        name = "Wol",
        type = BuffType.WeightLimit,
        value = 30,
        interval = 0,
        icon = "BAG2_EQ_ICON_R.TGA",
        time = 600,
    },

"Gornik": { //TIER 3/TIER 4 - zwi�ksza lvl g�rnika o jeden
        name = "Gornik",
        type = BuffType.Skill,
        skillId = PlayerSkill.Miner,
        value = 1,
        interval = 0,
        icon = "PICK_EQ_ICON_R.TGA",
        time = 15,
    },

"Biegacz": { //TIER 2 - regeneruje 4 biegania
        name = "Biegacz",
        type = BuffType.EnduranceRegeneration,
        value = 20,
        interval = 1,
        icon = "PLAYER_EQ_ICON_R.TGA",
        time = 30,
    },

"Stama2": { //TIER 2 - regeneruje 2 stamy
        name = "Stama2",
        type = BuffType.StaminaRegeneration,
        value = 0.4,
        interval = 10,
        icon = "FOOD_EQ_ICON_R.TGA",
        time = 50,
    },

"Stama3": { //TIER 2 - regeneruje 3 stamy
        name = "Stama3",
        type = BuffType.StaminaRegeneration,
        value = 0.6,
        interval = 10,
        icon = "FOOD_EQ_ICON_R.TGA",
        time = 50,
    },

"Stama4": { //TIER 2 - regeneruje 4 stamy
        name = "Stama4",
        type = BuffType.StaminaRegeneration,
        value = 0.8,
        interval = 10,
        icon = "FOOD_EQ_ICON_R.TGA",
        time = 50,
    },

"Stama5": { //TIER 2 - regeneruje 5 stamy
        name = "Stama5",
        type = BuffType.StaminaRegeneration,
        value = 1,
        interval = 10,
        icon = "FOOD_EQ_ICON_R.TGA",
        time = 50,
    },

"Stama6": { //TIER 2 - regeneruje 6 stamy
        name = "Stama6",
        type = BuffType.StaminaRegeneration,
        value = 1.2,
        interval = 10,
        icon = "FOOD_EQ_ICON_R.TGA",
        time = 50,
    },

"Stama7": { //TIER 2 - regeneruje 7 stamy
        name = "Stama7",
        type = BuffType.StaminaRegeneration,
        value = 1.4,
        interval = 10,
        icon = "FOOD_EQ_ICON_R.TGA",
        time = 50,
    },

"Stama8": { //TIER 2/TIER 3 - regeneruje 8 stamy
        name = "Stama8",
        type = BuffType.StaminaRegeneration,
        value = 1.6,
        interval = 10,
        icon = "FOOD_EQ_ICON_R.TGA",
        time = 50,
    },

"Stama10": { //- regeneruje 10 stamy
        name = "Stama10",
        type = BuffType.StaminaRegeneration,
        value = 2,
        interval = 10,
        icon = "FOOD_EQ_ICON_R.TGA",
        time = 50,
    },


"Stama12": { //TIER 3 - regeneruje 12 stamy
        name = "Stama12",
        type = BuffType.StaminaRegeneration,
        value = 2.4,
        interval = 10,
        icon = "FOOD_EQ_ICON_R.TGA",
        time = 50,
    },

"Stama15": { //TIER 3/TIER 4 - regeneruje 15 stamy
        name = "Stama15",
        type = BuffType.StaminaRegeneration,
        value = 3,
        interval = 10,
        icon = "FOOD_EQ_ICON_R.TGA",
        time = 50,
    },

"Stama20": { //TIER 4 - regeneruje 20 stamy
        name = "Stama20",
        type = BuffType.StaminaRegeneration,
        value = 4,
        interval = 10,
        icon = "FOOD_EQ_ICON_R.TGA",
        time = 50,
    },

"Stama30": { //TIER 5 - regeneruje 30 stamy
        name = "Stama30",
        type = BuffType.StaminaRegeneration,
        value = 6,
        interval = 10,
        icon = "FOOD_EQ_ICON_R.TGA",
        time = 50,
    },

"Rybahp": { //Z DROPU - redukuje 5 hp
        name = "Rybahp",
        type = BuffType.HealthReduce,
        value = 1,
        interval = 20,
        icon = "WIND_EQ_ICON_R.TGA",
        time = 100,
    },

"Rybamana": { //Z DROPU - redukuje 5 many
        name = "Rybamana",
        type = BuffType.ManaReduce,
        value = 1,
        interval = 20,
        icon = "LIGHTNING_ICONS.TGA",
        time = 100,
    },

"Srybahp": { //Z DROPU - regeneruje 15 hp
        name = "Srybahp",
        type = BuffType.HealthRegeneration,
        value = 3,
        interval = 10,
        icon = "PLUS_EQ_ICON_R.TGA",
        time = 50,
    },

"Herbatka": { //Z DROPU - regeneruje 50 hp
        name = "Herbatka",
        type = BuffType.HealthRegeneration,
        value = 5,
        interval = 10,
        icon = "PLUS_EQ_ICON_R.TGA",
        time = 100,
    },

"Herbatka2": { //Z DROPU - regeneruje 100 hp
        name = "Herbatka2",
        type = BuffType.HealthRegeneration,
        value = 10,
        interval = 10,
        icon = "PLUS_EQ_ICON_R.TGA",
        time = 100,
    },

"Herbatka3": { //Z DROPU - regeneruje 150 hp
        name = "Herbatka3",
        type = BuffType.HealthRegeneration,
        value = 15,
        interval = 20,
        icon = "PLUS_EQ_ICON_R.TGA",
        time = 200,
    },

"Srybamana": { //Z DROPU - regeneruje 15 many
        name = "Srybamana",
        type = BuffType.ManaRegeneration,
        value = 3,
        interval = 10,
        icon = "HAND2_EQ_ICON_R.TGA",
        time = 50,
    },

"Nieprzetworzonehp": { //Z DROPU - redukuje 10 hp
        name = "Nieprzetworzonehp",
        type = BuffType.HealthReduce,
        value = 1,
        interval = 10,
        icon = "WIND_EQ_ICON_R.TGA",
        time = 100,
    },

"Nieprzetworzonemana": { //Z DROPU - redukuje 10 many
        name = "Nieprzetworzonemana",
        type = BuffType.ManaReduce,
        value = 1,
        interval = 10,
        icon = "LIGHTNING_ICONS.TGA",
        time = 100,
    },

"Surowehp": { //Z DROPU - redukuje 20 hp
        name = "Surowehp",
        type = BuffType.HealthReduce,
        value = 2,
        interval = 10,
        icon = "WIND_EQ_ICON_R.TGA",
        time = 100,
    },

"Surowemana": { //Z DROPU - redukuje 20 many
        name = "Surowemana",
        type = BuffType.ManaReduce,
        value = 2,
        interval = 10,
        icon = "LIGHTNING_ICONS.TGA",
        time = 100,
    },

"Zgnilehp": { //Z DROPU - redukuje 40 hp
        name = "Zgnilehp",
        type = BuffType.HealthReduce,
        value = 4,
        interval = 10,
        icon = "WIND_EQ_ICON_R.TGA",
        time = 100,
    },

"Zgnilemana": { //Z DROPU - redukuje 40 many
        name = "Zgnilemana",
        type = BuffType.ManaReduce,
        value = 4,
        interval = 10,
        icon = "LIGHTNING_ICONS.TGA",
        time = 100,
    },

"Roslinahp": { //TIER 0 - regeneruje 5 hp
        name = "Roslinahp",
        type = BuffType.HealthRegeneration,
        value = 1,
        interval = 20,
        icon = "PLUS_EQ_ICON_R.TGA",
        time = 100,
    },

"Jablkohp": { //TIER 0 - regeneruje 7 hp
        name = "Jablkohp",
        type = BuffType.HealthRegeneration,
        value = 1,
        interval = 10,
        icon = "PLUS_EQ_ICON_R.TGA",
        time = 70,
    },

"Pieczonehp": { //TIER 0 - regeneruje 18 hp
        name = "Pieczonehp",
        type = BuffType.HealthRegeneration,
        value = 3,
        interval = 10,
        icon = "PLUS_EQ_ICON_R.TGA",
        time = 60,
    },

"Potrawahp": { //TIER 1 - regeneruje 25 hp
        name = "Potrawahp",
        type = BuffType.HealthRegeneration,
        value = 5,
        interval = 10,
        icon = "PLUS_EQ_ICON_R.TGA",
        time = 50,
    },

"Daniehp": { //TIER 2 - regeneruje 42 hp
        name = "Daniehp",
        type = BuffType.HealthRegeneration,
        value = 7,
        interval = 10,
        icon = "PLUS_EQ_ICON_R.TGA",
        time = 60,
    },

"Posilekmaxhp": { //TIER 3 - dodaje 15 max hp
        name = "Posilekmaxhp",
        type = BuffType.Attribute,
        attributeId = PlayerAttributes.Hp,
        value = 15,
        interval = 0,
        icon = "HP_EQ_ICON_R.TGA",
        time = 120,
    },

"Strawamaxhp": { //TIER 4 - dodaje 30 max hp
        name = "Strawamaxhp",
        type = BuffType.Attribute,
        attributeId = PlayerAttributes.Hp,
        value = 30,
        interval = 0,
        icon = "HP_EQ_ICON_R.TGA",
        time = 120,
    },

"Krewhp": { //TIER 4 - dodaje 30 max hp
        name = "krewhp",
        type = BuffType.Attribute,
        attributeId = PlayerAttributes.Hp,
        value = 30,
        interval = 0,
        icon = "HP_EQ_ICON_R.TGA",
        time = 900,
    },

"Chemolmana": { //TIER 0 - regeneruje 10 many
        name = "Chemolmana",
        type = BuffType.ManaRegeneration,
        value = 1,
        interval = 10,
        icon = "HAND2_EQ_ICON_R.TGA",
        time = 100,
    },

"Skretmana": { //TIER 1 - regeneruje 30 many
        name = "Skretmana",
        type = BuffType.ManaRegeneration,
        value = 3,
        interval = 10,
        icon = "HAND2_EQ_ICON_R.TGA",
        time = 100,
    },

"Jointmana": { //TIER 2 - regeneruje 50 many
        name = "Jointmana",
        type = BuffType.ManaRegeneration,
        value = 5,
        interval = 10,
        icon = "HAND2_EQ_ICON_R.TGA",
        time = 100,
    },

"Ganjamaxmana": { //TIER 3 - dodaje 30 max many
        name = "Ganjamaxmana",
        type = BuffType.Attribute,
        attributeId = PlayerAttributes.Mana,
        value = 30,
        interval = 0,
        icon = "EYE_EQ_ICON_R.TGA",
        time = 120,
    },

"Ziolomaxmana": { //TIER 4 - dodaje 50 max many
        name = "Ziolomaxmana",
        type = BuffType.Attribute,
        attributeId = PlayerAttributes.Mana,
        value = 50,
        interval = 0,
        icon = "EYE_EQ_ICON_R.TGA",
        time = 120,
    },

"Krewmana": { //TIER 4 - dodaje 50 max many
        name = "Krewmana",
        type = BuffType.Attribute,
        attributeId = PlayerAttributes.Mana,
        value = 50,
        interval = 0,
        icon = "EYE_EQ_ICON_R.TGA",
        time = 900,
    },

"Sprint": { //TIER 3 - umo�liwia sprint na 1 minut�
        name = "Sprint",
        type = BuffType.Mds,
        mds = "HUMANS_SPRINT.MDS",
        value = 0,
        interval = 0,
        icon = "BOOTS_EQ_ICON_R.TGA",
        time = 60,
    },

"Sprint2": { //TIER 4 - umo�liwia sprint na 2 minuty
        name = "Sprint2",
        type = BuffType.Mds,
        mds = "HUMANS_SPRINT.MDS",
        value = 0,
        interval = 0,
        icon = "BOOTS_EQ_ICON_R.TGA",
        time = 120,
    },

"Akrobatyka": { //TIER 5 - umo�liwia akrobatyk� na 3 minuty
        name = "Akrobatyka",
        type = BuffType.Mds,
        mds = "HUMANS_ACROBATIC.MDS",
        value = 0,
        interval = 0,
        icon = "BOOTS_EQ_ICON_R.TGA",
        time = 160,
    },

"Akrobatyka2": { //TIER 5 - umo�liwia akrobatyk� na 2 minuty
        name = "Akrobatyka2",
        type = BuffType.Mds,
        mds = "HUMANS_ACROBATIC.MDS",
        value = 0,
        interval = 0,
        icon = "BOOTS_EQ_ICON_R.TGA",
        time = 120,
    },

"Miod": { //ALKOHOL - Miod
        name = "Miod",
        type = BuffType.Mds,
        mds = "HUMANS_DRUNKEN.MDS",
        value = 0,
        interval = 0,
        stack = true,
        icon = "ALCHEMY_EQ_ICON_R.TGA",
        time = 15,
    },

"Piwo": { //ALKOHOL - piwo
        name = "Piwo",
        type = BuffType.Mds,
        mds = "HUMANS_DRUNKEN.MDS",
        value = 0,
        interval = 0,
        stack = true,
        icon = "ALCHEMY_EQ_ICON_R.TGA",
        time = 120,
    },

"Wino": { //ALKOHOL - wino marki wino
        name = "Wino",
        type = BuffType.Mds,
        mds = "HUMANS_DRUNKEN.MDS",
        value = 0,
        interval = 0,
        stack = true,
        icon = "ALCHEMY_EQ_ICON_R.TGA",
        time = 240,
    },

"Gorzalka": { //ALKOHOL - wodka
        name = "Gorzalka",
        type = BuffType.Mds,
        mds = "HUMANS_DRUNKEN.MDS",
        value = 0,
        interval = 0,
        stack = true,
        icon = "ALCHEMY_EQ_ICON_R.TGA",
        time = 360,
    },

"Samogon": { //ALKOHOL - bimber/spirytus
        name = "Samogon",
        type = BuffType.Mds,
        mds = "HUMANS_DRUNKEN.MDS",
        value = 0,
        interval = 0,
        stack = true,
        icon = "ALCHEMY_EQ_ICON_R.TGA",
        time = 480,
    },

"Lewitacja": { //TIER 3 - Lewitacja
        name = "Lewitacja",
        type = BuffType.Mds,
        mds = "HUMANS_LEWITACJA.MDS",
        value = 0,
        interval = 0,
        icon = "NATURE_EQ_ICON_R.TGA",
        time = 30,
    },

"Lewitacja2": { //TIER 5 - Lewitacja2
        name = "Lewitacja2",
        type = BuffType.Mds,
        mds = "HUMANS_LEWITACJA.MDS",
        value = 0,
        interval = 0,
        icon = "NATURE_EQ_ICON_R.TGA",
        time = 60,
    },

"Zatrucie": { //TIER 3 - redukuje 1000 hp
        name = "Zatrucie",
        type = BuffType.HealthReduce,
        value = 20,
        interval = 120,
        icon = "POTION2_EQ_ICON_R.TGA",
        time = 3600,
    },

};

Config["Buildings"] <- {
    "Biedne drzwi": {
        skill = 3,
        model = "DOOR_NW_POOR_01.MDS",
        work = 3,
        vobInfo = {
            type = VobType.Door,
            hpMode = VobHealth.Vulnerable,
            hp = 40,
            movable = VobMovable.Static,
        },
        materials = [
            ["ITCR_HK_DROP_15", 30],
            ["ITUS_HK_TOOL_35", 3],
            ["ITCR_HK_INGR_18", 1],
        ],
        construction = [
            [-83.50,130.70,-1.30,0,0,0],
            [50.78,130.70,-1.0,0,0,0],
            [-20.58,286.70,-4.50,-88,92,0],
        ]
    },
    "Drzwi": {
        skill = 3,
        model = "DOOR_NW_RICH_01.MDS",
        work = 80,
        vobInfo = {
            type = VobType.Door,
            hpMode = VobHealth.Vulnerable,
            hp = 120,
            movable = VobMovable.Static,
        },
        materials = [
            ["ITCR_HK_DROP_15", 45],
            ["ITUS_HK_TOOL_35", 6],
            ["ITCR_HK_INGR_18", 2],
        ],
        construction = [
            [-83.50,130.70,-1.30,0,0,0],
            [50.78,130.70,-1.0,0,0,0],
            [-20.58,286.70,-4.50,-88,92,0],
        ]
    },
    "Mocne Drzwi": {
        skill = 4,
        model = "DOOR_WOODEN.MDS",
        work = 90,
        vobInfo = {
            type = VobType.Door,
            hpMode = VobHealth.Vulnerable,
            hp = 160,
            movable = VobMovable.Static,
        },
        materials = [
            ["ITCR_HK_DROP_15", 60],
            ["ITUS_HK_TOOL_35", 12],
            ["ITCR_HK_INGR_18", 2],
        ],
        construction = [
            [-83.50,130.70,-1.30,0,0,0],
            [50.78,130.70,-1.0,0,0,0],
            [-20.58,286.70,-4.50,-88,92,0],
        ]
    },
    "Wrota": {
        skill = 5,
        model = "DOOR_NW_DRAGONISLE_02.MDS",
        work = 120,
        vobInfo = {
            type = VobType.Door,
            hpMode = VobHealth.Static,
            hp = 2000,
            movable = VobMovable.Static,
        },
        materials = [
            ["ITCR_HK_DROP_15", 90],
            ["ITUS_HK_TOOL_35", 24],
            ["ITCR_HK_INGR_18", 7],
        ],
        construction = [
            [-83.50,130.70,-1.30,0,0,0],
            [50.78,130.70,-1.0,0,0,0],
            [-20.58,286.70,-4.50,-88,92,0],
        ]
    },
    "Sciana": {
        skill = 3,
        model = "EVT_NC_MAINGATE01.3DS",
        work = 55,
        vobInfo = {
            type = VobType.Static,
            hpMode = VobHealth.Vulnerable,
            hp = 150,
            movable = VobMovable.Static,
        },
        materials = [
            ["ITCR_HK_DROP_15", 20],
            ["ITUS_HK_TOOL_35", 5],
        ],
        construction = [
            [-200.50,-30.70,-30.30,0,0,0],
            [0.78,-30.70,-21.0,0,0,0],
            [200.58,-30.70,24.50,0,0,0],
        ]
    },
}

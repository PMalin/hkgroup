local textures = {}

addEventHandler("onInit", function() {
    foreach(boardId, boardPosition in Config["NotificationBoard"]) {
        textures[boardId] <- {};

        for(local x = 0; x < boardPosition.size[0]; x ++)
            for(local y = 0; y < boardPosition.size[1]; y ++)
                textures[boardId][x+"_"+y] <- null;
    }
})


addEventHandler("onBoardNotificationAdd", function(boardId, notificationBoardId) {
    local board = getBoard(boardId);
    local boardNotification = board.notifications[notificationBoardId];

    textures[boardId][boardNotification.position.x+"_"+boardNotification.position.y] = ItemRender(0,0,0,0,"FAKESCROLL_ADDON");
});

addEventHandler("onBoardNotificationRemove", function(boardId, notificationBoardId) {
    local board = getBoard(boardId);
    local boardNotification = board.notifications[notificationBoardId];

    textures[boardId][boardNotification.position.x+"_"+boardNotification.position.y] = null;
});

addEventHandler("onRender", function() {
    foreach(boardId, boardShadows in textures)
    {
        foreach(index, shadow in boardShadows)
            if(shadow != null)
                shadow.visible = false;

        if(ActiveGui != null)
            continue;

        local heroPosition = getPlayerPosition(heroId);
        local board = getBoard(boardId);

        if(getPositionDifference(heroPosition, board.position) > 1000)
            continue;

        local heroAngle = getPlayerAngle(heroId);
        if(heroAngle < (board.position.angle - 40) || heroAngle > (board.position.angle + 40) )
            continue;

        local topLeftProjection = _Camera.project(board.corners[0].x, board.corners[0].y, board.corners[0].z);
        local bottomLeftProjection = _Camera.project(board.corners[1].x, board.corners[1].y, board.corners[1].z);
        local topRightProjection = _Camera.project(board.corners[2].x, board.corners[2].y, board.corners[2].z);
        local bottomRightProjection = _Camera.project(board.corners[2].x, board.corners[2].y, board.corners[2].z);

        if(topLeftProjection == null || topRightProjection == null || bottomLeftProjection == null || bottomRightProjection == null)
            continue;

        local widthOfElement = abs((topLeftProjection.x - topRightProjection.x)/board.size[0])
        local heightOfElement = abs((topLeftProjection.y - bottomLeftProjection.y)/board.size[1])

        local baseY = topLeftProjection.y + (abs(topLeftProjection.y - topRightProjection.y)/2);
        if(topLeftProjection.y > topRightProjection.y)
            baseY = topRightProjection.y + (abs(topLeftProjection.y - topRightProjection.y)/2);

        local baseX = topLeftProjection.x + (abs(topLeftProjection.x - bottomLeftProjection.x)/2);
        if(topLeftProjection.x > bottomLeftProjection.x)
            baseX = bottomLeftProjection.x + (abs(topLeftProjection.x - bottomLeftProjection.x)/2);

        foreach(index, shadow in boardShadows)
        {
            if(shadow == null)
                continue;

            index = split(index, "_");

            local x = baseX + widthOfElement * index[0].tointeger();
            local y = baseY + heightOfElement * index[1].tointeger();

            shadow.setPositionPx(x,y);
            shadow.setSize(anx(widthOfElement), any(heightOfElement));

            if(x <= 0 || y <= 0 || widthOfElement == 0 || heightOfElement == 0 || x > 8129 || y > 8129)
                shadow.visible = false;
            else
                shadow.visible = true;
        }
    }
})


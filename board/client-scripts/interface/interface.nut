
BoardInterface <- {
    boardId = -1
    noteId = -1,
    bucket = null,

    prepareWindow = function()
    {
        local result = {};

        result.window <- GUI.Window(0, 0, 0, 0, "HK_BOX.TGA", null, false);
        result.windowSecond <- GUI.Window(anx(Resolution.x/2 - 580), any(Resolution.y/2 - 340), anx(560), any(700), "LETTERS.TGA", null, false);
        result.drawText <- Draw(anx(Resolution.x/2 - 540), any(Resolution.y/2 - 220),"");
        result.drawText.setColor(42,52,55);
        result.notes <- [];

        result.addButton <- GUI.Button(0, 0, anx(400), any(60), "HK_BUTTON.TGA", "Dodaj now� notatk�", result.window);
        result.addButton.bind(EventType.Click, function(element) {
            local boardId = BoardInterface.boardId;
            BoardInterface.hide();
            BoardInterfaceCreate.show(boardId);
        });

        result.removeButton <- GUI.Button(0, any(720), anx(200), any(60), "HK_BUTTON.TGA", "Usu�", result.windowSecond);
        result.removeButton.bind(EventType.Click, function(element) {
            local boardId = BoardInterface.boardId;
            BoardPacket().remove(BoardInterface.boardId, BoardInterface.noteId)
            BoardInterface.hide();
        });

        return result;
    }

    show = function(_boardId)
    {
        if(bucket == null)
            bucket = prepareWindow();

        boardId = _boardId;
        BaseGUI.show();
        ActiveGui = PlayerGUI.Board;

        playAni(heroId,"T_MAP_STAND_2_S0");

        local board = getBoard(boardId);
        Camera.setPosition(board.camera.x, board.camera.y, board.camera.z);
        Camera.setRotation(0, board.camera.rotx, 0);

        bucket.window.setVisible(true);

        refresh();
    }

    hide = function()
    {
        bucket.window.setVisible(false);
        bucket.windowSecond.setVisible(false);

        bucket.drawText.text = "";
        bucket.drawText.visible = false;

        boardId = -1;
        noteId = -1;

        foreach(note in bucket.notes)
        {
            note.button.setVisible(false);
            note.button.destroy();
        }

        bucket.notes.clear();
        BaseGUI.hide();
        ActiveGui = null;

        playAni(heroId,"S_RUN");
    }

    showDescription = function(_noteId) {
        noteId = _noteId;
        local note = getBoard(boardId).notifications[noteId];
        bucket.windowSecond.setVisible(true);
        bucket.drawText.text = note.getLineText();
        bucket.drawText.visible = true;
    }

    refresh = function() {
        foreach(note in bucket.notes)
        {
            note.button.setVisible(false);
            note.button.destroy();
        }

        bucket.notes.clear();

        local board = getBoard(boardId);

        local baseSize = bucket.window.getSize();
        local basePosition = bucket.window.getPosition();

        switch(boardId)
        {
            case 0:
                bucket.window.setSize(2048, 4092); baseSize = { width = 2048, height = 4092};
                bucket.window.setPosition(4592, 2024); basePosition = {x = 4592, y = 2024};
            break;
            case 1:
                bucket.window.setSize(3076, 3076); baseSize = { width = 3076, height = 3076};
                bucket.window.setPosition(4192, 2524); basePosition = {x = 4192, y = 2524};
            break;
        }

        baseSize.width = baseSize.width - 100;
        baseSize.height = baseSize.height - 100;
        basePosition.x = basePosition.x + 50;
        basePosition.y = basePosition.y + 50;

        bucket.addButton.setPosition(basePosition.x, basePosition.y - any(100));

        local chunkSize = [abs(baseSize.width/board.size[0]), abs(baseSize.height/board.size[1])]

        foreach(notification in board.notifications)
        {
            local button = GUI.Button(
                basePosition.x + chunkSize[0] * notification.position.x,
                basePosition.y + chunkSize[1] * notification.position.y,
                chunkSize[0],
                chunkSize[1],
                "LETTERS.TGA"
            );

            local draw = Draw(basePosition.x + chunkSize[0] * notification.position.x + 25, basePosition.y + chunkSize[1] * notification.position.y + 25, notification.getLineText());
            draw.setScale(0.175, 0.175);
            draw.setColor(25, 31, 21);

            button.attribute = notification.id;
            button.setVisible(true);
            draw.visible = true;
            bucket.notes.append({ button = button, draw = draw });
        }
    }

    onBoardNotificationAdd = function(_boardId, boardNotificationId)
    {
        if(ActiveGui != PlayerGUI.Board)
            return;

        if(boardId != _boardId)
            return;

        refresh();
    }

    onBoardNotificationRemove = function(_boardId, boardNotificationId)
    {
        if(ActiveGui != PlayerGUI.Board)
            return;

        if(boardId != _boardId)
            return;

        refresh();
    }

    onClick = function(element)
    {
        if(ActiveGui != PlayerGUI.Board)
            return;

        foreach(note in bucket.notes)
        {
            if(note.button instanceof GUI.Button)
            {
                if(note.button == element)
                {
                    showDescription(note.button.attribute);
                    return;
                }
            }
        }
    }

    onForceCloseGUI = function()
    {
        if(ActiveGui == PlayerGUI.Board)
            hide();
    }
}

addEventHandler("onForceCloseGUI", BoardInterface.onForceCloseGUI.bindenv(BoardInterface));
addEventHandler("onBoardNotificationAdd", BoardInterface.onBoardNotificationAdd.bindenv(BoardInterface));
addEventHandler("onBoardNotificationRemove", BoardInterface.onBoardNotificationRemove.bindenv(BoardInterface));
addEventHandler("GUI.onClick", BoardInterface.onClick.bindenv(BoardInterface));

Bind.addKey(KEY_ESCAPE, BoardInterface.hide.bindenv(BoardInterface), PlayerGUI.Board)

//     boardId = -1
//     noteId = -1
//     bucket = null

//     prepareWindow = function()
//     {
//         local result = {};

//         result.window <- GUI.Window(anx(Resolution.x/2 - 520), any(Resolution.y/2 - 300), anx(500), any(620), "HK_BOX.TGA", null, false);
//         result.scrollBar <- GUI.ScrollBar(anx(460), any(50), anx(30), any(520), "HK_SCROLL.TGA", "HK_INDICATOR.TGA", "HK_U.TGA", "HK_O.TGA", Orientation.Vertical, result.window);

//         result.bindForScrollbar <- Bind.onChange(result.scrollBar, function(value) {
//             BoardInterface.refresh(value);
//         }, PlayerGUI.Board);

//         result.boardWindow <- GUI.Window(anx(Resolution.x/2 + 20), any(Resolution.y/2 - 300), anx(500), any(620), "HK_BOX.TGA", null, false);
//         result.removeButton <- GUI.Button(anx(Resolution.x/2 + 170), any(Resolution.y/2 + 250), anx(200), any(60), "HK_BUTTON.TGA", "Zerwij");
//         result.addButton <- GUI.Button(anx(150), any(550), anx(200), any(60), "HK_BUTTON.TGA", "Dodaj", result.window);

//         result.addButton.bind(EventType.Click, function(element) {
//             local boardId = BoardInterface.boardId;
//             BoardInterface.hide();
//             BoardInterfaceCreate.show(boardId);
//         });

//         result.removeButton.bind(EventType.Click, function(element) {
//             if(BoardInterface.boardId == -1 || BoardInterface.noteId == -1)
//                 return;

//             playAni(heroId, "T_IDROP_2_STAND");
//             BoardPacket().remove(BoardInterface.boardId,BoardInterface.noteId);

//             BoardInterface.bucket.description.clear();
//             BoardInterface.bucket.removeButton.setVisible(false);
//         });

//         result.notes <- [];
//         result.description <- [];

//         return result;
//     }

//     show = function(_boardId)
//     {
//         if(bucket == null)
//             bucket = prepareWindow();

//         boardId = _boardId;
//         BaseGUI.show();
//         ActiveGui = PlayerGUI.Board;

//         BlackScreen(1000);
//         playAni(heroId,"T_MAP_STAND_2_S0");

//         bucket.window.setVisible(true);
//         bucket.boardWindow.setVisible(true);
//     }

//     hide = function()
//     {
//         bucket.window.setVisible(false);
//         bucket.boardWindow.setVisible(false);

//         boardId = -1;
//         noteId = -1;
//         BaseGUI.hide();
//         ActiveGui = null;

//         foreach(note in bucket.notes) {
//             note.setVisible(false);
//             note.destroy();
//         }

//         bucket.description.clear();
//         bucket.removeButton.setVisible(false);

//         bucket.notes.clear();
//         playAni(heroId,"S_RUN");
//     }

//     showDescription = function(id)
//     {
//         noteId = id;
//         BoardPacket().showNotification(boardId,id);

//         bucket.description.clear();
//         bucket.removeButton.setVisible(true);
//         bucket.removeButton.attribute = id;
//     }

//     addDescriptionLine = function(text)
//     {
//         if(ActiveGui != PlayerGUI.Board)
//             return;

//         local line = Draw(anx(Resolution.x/2 + 40), any(Resolution.y/2 - 280 + 30 * bucket.description.len()), text);
//         line.font = "FONT_OLD_20_WHITE_HI.TGA";
//         line.setScale(0.4, 0.4);
//         line.visible = true;

//         bucket.description.append(line);
//     }

//     refresh = function(value = 0)
//     {
//         foreach(note in bucket.notes) {
//             note.setVisible(false);
//             note.destroy();
//         }

//         bucket.notes.clear();

//         value = value * 2;

//         local boardTitles = BoardController.boards[boardId].titles;
//         local appeared = 0;
//         foreach(index, boardTitle in boardTitles)
//         {
//             if(index >= value && index <= value + 8)
//             {
//                 local xParse = 20 + (appeared % 2 * 220);
//                 local yParse = 20 + abs(appeared/2) * 120;
//                 local button = GUI.Button(anx(Resolution.x/2 - 520 + xParse), any(Resolution.y/2 - 280 + yParse), anx(200), any(100), "HK_BOX", boardTitle.name);
//                 button.attribute = boardTitle.id;
//                 bucket.notes.push(button);
//                 local draw = GUI.Draw(anx(Resolution.x/2 - 520 + xParse), any(Resolution.y/2 - 245 + yParse), boardTitle.author);
//                 draw.setFont("FONT_OLD_20_WHITE_HI.TGA");
//                 draw.setPosition(anx(Resolution.x/2 - 520 + xParse + 10), any(Resolution.y/2 - 215 + yParse));
//                 draw.setScale(0.4, 0.4);
//                 bucket.notes.push(draw);

//                 appeared = appeared + 1;
//             }
//         }

//         foreach(note in bucket.notes)
//             note.setVisible(true);
//     }

//     onClick = function(element)
//     {
//         if(ActiveGui != PlayerGUI.Board)
//             return;

//         foreach(note in bucket.notes)
//         {
//             if(note instanceof GUI.Button)
//             {
//                 if(note == element)
//                 {
//                     showDescription(note.attribute);
//                     return;
//                 }
//             }
//         }
//     }

//     onBoardNotificationAdd = function(_boardId, boardNotificationId)
//     {
//         if(ActiveGui != PlayerGUI.Board)
//             return;

//         if(boardId != _boardId)
//             return;

//         refresh();
//     }

//     onBoardNotificationRemove = function(_boardId, boardNotificationId)
//     {
//         if(ActiveGui != PlayerGUI.Board)
//             return;

//         if(boardId != _boardId)
//             return;

//         hide();
//     }

//     onForceCloseGUI = function()
//     {
//         if(ActiveGui == PlayerGUI.Board)
//             hide();
//     }
// };

// addEventHandler("onForceCloseGUI", BoardInterface.onForceCloseGUI.bindenv(BoardInterface));
// addEventHandler("onBoardNotificationAdd", BoardInterface.onBoardNotificationAdd.bindenv(BoardInterface));
// addEventHandler("onBoardNotificationRemove", BoardInterface.onBoardNotificationRemove.bindenv(BoardInterface));
// addEventHandler("GUI.onClick", BoardInterface.onClick.bindenv(BoardInterface));

// Bind.addKey(KEY_ESCAPE, BoardInterface.hide.bindenv(BoardInterface), PlayerGUI.Board)
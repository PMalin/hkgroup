
class BoardController
{
    static boards = {};

    static function onInit() {
        local boardInDatabase = Query().select().from(Board.BoardTable).all();

        foreach(boardId, boardPosition in Config["NotificationBoard"])
            boards[boardId] <- Board(boardId, boardPosition, boardPosition.size);

        foreach(boardNotificationObject in boardInDatabase)
        {
            local newNotificationBoard = BoardNotification(boardNotificationObject.boardId.tointeger())

            newNotificationBoard.id = boardNotificationObject.id.tointeger();
            newNotificationBoard.world = boardNotificationObject.world;
            newNotificationBoard.user = boardNotificationObject.userName;
            newNotificationBoard.text = split(boardNotificationObject.text,"&");
            newNotificationBoard.position = {x = boardNotificationObject.x.tointeger(), y = boardNotificationObject.y.tointeger()}

            boards[boardNotificationObject.boardId].addNotification(newNotificationBoard);
        }

        print("We have " + boards.len() + " registered boards.");
    }

    static function onPlayerLoad(playerId) {
        foreach(boardId, board in boards)
            BoardPacket().sendBoard(playerId, board);
    }

    static function onPacket(playerId, packet)
    {
        local typ = packet.readUInt8();

        switch(typ)
        {
            case BoardPackets.Delete:
                local boardId = packet.readInt16();
                if(!(boardId in boards))
                    return;

                local notificationId = packet.readInt16();

                local object = Config["ChatMethod"][ChatMethods.Action];
                distanceChat(playerId, ChatType.IC, object.distance, object.r, object.g, object.b, "[PLAYER:"+playerId+"] Zerwa� z listy og�osze� notatk�");
                boards[boardId].removeNotification(notificationId);
                BoardPacket().remove(boardId, notificationId);
                Query().deleteFrom(Board.BoardTable).where(["id = "+notificationId]).execute();
            break;
            case BoardPackets.Create:
                local boardId = packet.readInt16();
                if(!(boardId in boards))
                    return;

                local emptySlot = null;
                local emptySlot = boards[boardId].getEmptySlot();
                if(emptySlot == null) {
                    addPlayerNotification(playerId, "Nie ma wolnych miejsc.");
                    return;
                }

                local listStrings = [];
                // Tutaj olewamy X,Y bo klient mo�e nas wprowadzi� w b��d
                //TODO: Usun�� paczk� od klienta, kt�ra odpowiada za wysy�anie X,Y
                local dummy = packet.readInt16();
                local dummy2 = packet.readInt16();
                local positionX = emptySlot.x;
                local positionY = emptySlot.y;

                local listLen = packet.readInt16();
                for(local i = 0; i < listLen; i ++) {
                    local str = mysql_escape_string(packet.readString());
                    if(str == null)
                        str = "";

                    listStrings.append(str);
                }

                local newNotificationBoard = BoardNotification(boardId)

                newNotificationBoard.world = getPlayerWorld(playerId);
                newNotificationBoard.user = getPlayer(playerId).name;
                newNotificationBoard.position = emptySlot;
                newNotificationBoard.text = listStrings;

                local queryResult = Query().insertInto(Board.BoardTable, ["text", "boardId", "userName", "world", "x", "y"
                ], [
                    "'"+String.parseArray(listStrings, "&")+"'", boardId, "'"+getAccount(playerId).username+"'", "'"+newNotificationBoard.world+"'", positionX, positionY
                ]).execute();

                if(queryResult == false)
                {
                    // Jak ju� wejdziemy tutaj to raczej co� z baz� jest nie tak
                    addPlayerNotification(playerId, "Bl�d podczas dodawania og�oszenia");
                    return;
                }

                local object = Config["ChatMethod"][ChatMethods.Action];
                distanceChat(playerId, ChatType.IC, object.distance, object.r, object.g, object.b, "[PLAYER:"+playerId+"] Doda� notatk� do listy og�osze�");

                local idBoard = DB.queryGet("SELECT id FROM "+Board.BoardTable+" ORDER BY id DESC LIMIT 1")["id"];

                newNotificationBoard.id = idBoard;

                boards[boardId].addNotification(newNotificationBoard);
                BoardPacket().sendNotification(newNotificationBoard);
            break;
        }
    }
}

addEventHandler("onInit", BoardController.onInit.bindenv(BoardController));
addEventHandler("onPlayerLoad", BoardController.onPlayerLoad.bindenv(BoardController))
RegisterPacketReceiver(Packets.Board, BoardController.onPacket.bindenv(BoardController));

getBoard <- @(id) BoardController.boards[id];
getBoards <- @() BoardController.boards;



// enum BoardPackets
// {
//     Create,
//     Delete,
//     List,
//     Show,
// }

class BoardPacket
{
    packet = null;

    constructor()
    {
        packet = ExtendedPacket(Packets.Board);
    }

    function sendBoard(playerId, board) {
        packet.writeUInt8(BoardPackets.List)
        packet.writeInt16(board.id);
        packet.writeInt16(board.notifications.len());
        foreach(notification in board.notifications) {
            packet.writeInt16(notification.id);
            packet.writeInt16(notification.position.x);
            packet.writeInt16(notification.position.y);
            packet.writeInt16(notification.text.len());
            foreach(notificationText in notification.text)
                packet.writeString(notificationText);
        }

        packet.send(playerId, RELIABLE);
    }

    function sendNotification(notification) {
        packet.writeUInt8(BoardPackets.Create)
        packet.writeInt16(notification.id);
        packet.writeInt16(notification.boardId);
        packet.writeInt16(notification.position.x);
        packet.writeInt16(notification.position.y);
        packet.writeInt16(notification.text.len());
        foreach(notificationText in notification.text)
            packet.writeString(notificationText);

        packet.sendToAll(RELIABLE);
    }

    function remove(boardId, boardNotificationId) {
        packet.writeUInt8(BoardPackets.Delete)
        packet.writeInt16(boardId);
        packet.writeInt16(boardNotificationId);
        packet.sendToAll(RELIABLE);
    }
}
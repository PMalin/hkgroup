
class String
{
    function find(text, find) {
        local expression = regexp(@""+find);
        local results = expression.capture(text);

        if(results)
            return results;

        return false;
    }

    /**
    Get char at given index
    @params
        index - int in witch we will start
        text - string in witch we will be working
    @return
        text - string with is first char on text
    */
    function charAt(index, text)
    {
        if(text.len() <= index)
            return "";

        return text.slice(index, index + 1);
    }

    /**
    Get first char from text
    @params
        text - string in witch we will be working
    @return
        text - string with is first char on text
    */
    function firstChar(text)
    {
        return charAt(0, text);
    }

    /**
    Get last char from text
    @params
        text - string in witch we will be working
    @return
        text - string with is first char on text
    */
    function lastChar(text)
    {
        return charAt((text.len() - 1), text);
    }

    /**
    Using regex to find and replace first searched element from text
    @params
        text - string in witch we will be working
        find - string to replace
        replacement - string with will be use as replacement
    @return
        text - string after replacement
    */
    function replace(text, find, replacement)
    {
        local result = String.find(text, find);

        if(result)
        {
            local subString = text.slice(0, result[0].begin);
            local endString = text.slice(result[0].end);

            return subString + replacement + endString;
        }

        return text;
    }

    /**
    Using regex to find and replace all searched elements from text
    @params
        text - string in witch we will be working
        find - string to replace
        replacement - string with will be use as replacement
    @return
        text - string after replacement
    */
    function replaceAll(text, find, replacement)
    {
        local newText = text;
        do {
            text = newText;
            newText = replace(newText, find, replacement);
        } while (newText != text)

        return newText;
    }

    /**
    Return text amount of times multiplied
    @params
        text - string in witch we will be working
        amount - multiplier
    @return
        text - string after multiply
    */
    function repeat(text, amount)
    {
        local returnText = text;
        for(local i = 0; i < amount; i++)
            returnText += text;

        return returnText;
    }

    /**
    Return text from array
    @params
        array - array with strings
    @return
        text - string after place
    */
    function parseArray(arrayToParse, between = "\n")
    {
        local text = "";
        foreach(item in arrayToParse)
        {
            if(typeof item == "string") {
                if(text == "")
                    text = item;
                else
                    text = text + between + item;
            }
        }
        return text;
    }

    /**
    Replace char
    @params
        text - string before replace
    @return
        text - string after replace
    */
    function replaceChar(text, x, newChar)
    {
        local newText = text.slice(0, x-1);
        local nextText = text.slice(x, text.len());
        return newText + newChar + nextText;
    }

    /**
    @params
        length - int amount of chars
    */
    function makeid(length)
    {
        local text = "";
        local nonSpecialChars = ["a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","u","p","r","t","u","z","y","x","4","1","5","2","6","7","8","9"];
        for(local i = 0; i < length; i ++)
            text += nonSpecialChars[irand(nonSpecialChars.len() - 1)];

        return text;
    }

    /**
    Return text without polish signs
    @params
        text - string before replace
    @return
        text - string after replace
    */
    function dePolonizer(text)
    {
        local specialChars = ["�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�"];
        local nonSpecialChars = ["E","O","A","S","L","Z","Z","C","N","e","o","a","s","l","z","z","c","n"];
        for(local i = 0; i < specialChars.len(); i++)
        {
            local char = specialChars[i], index = null;
            do {
                index = text.find(char);
                if (index != null) {
                    text = String.replaceChar(text,index+1,nonSpecialChars[i])
                }
            } while (index != null);
        }
        return text;
    }
}

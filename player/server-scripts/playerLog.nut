local bucket = [];

class PlayerLog
{
    static dbTable = "playerlog";
    static dbChatTable = "chatlog";

    id = -1
    cache = null

    constructor(id)
    {
        this.id = id;
        this.cache = [];

        read();
    }

    function read() {
        local logs = DB.queryGetAll("SELECT * FROM "+PlayerLog.dbTable+" WHERE playerId = "+id+" ORDER BY id DESC LIMIT 120;");
        foreach(log in logs)
            cache.push({content = log.log, dateObject = log.date});
    }

    function add(logContent) {
        local dateObject = date();
        dateObject = dateObject.year+"-"+dateObject.month+"-"+dateObject.day+" "+dateObject.hour+":"+dateObject.min;

        Query().insertInto(PlayerLog.dbTable, ["log", "playerId", "date"], ["'"+logContent+"'", id, "'"+dateObject+"'"]).execute();

        cache.insert(0, {content = logContent, dateObject = dateObject});

        if(cache.len() > 120)
            cache.remove(120);
    }

    function prepareChatLog(text,chatType){
        local textEscaped = mysql_escape_string(text);
        bucket.push(["'"+textEscaped+"'","'"+chatType+"'",id,"now()"]);
    }

    static function handleChatLog() {
        local d = date();

        if(bucket.len() > 0){
            Query().insertInto(
                PlayerLog.dbChatTable, 
                ["log", "type", "playerId","date"], 
                bucket
            ) .execute()

            bucket = [];
        }

        if(d.min = 30)
        {
            DB.queryGet("DELETE FROM "+PlayerLog.dbChatTable+" WHERE date < now() - interval 5 DAY");
        }
    }
}

addEventHandler("onMinute", PlayerLog.handleChatLog.bindenv(PlayerLog))
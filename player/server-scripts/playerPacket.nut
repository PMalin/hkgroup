
class PlayerPacket
{
    id = -1;
    packet = null;

    constructor(playerId)
    {
        id = playerId;
        packet = ExtendedPacket(Packets.Player);
    }

    // Character packets

    function sendInventoryId(inventoryId) {
        packet.writeUInt8(PlayerPackets.Inventory)
        packet.writeUInt16(inventoryId);
        packet.send(id);
    }

    function sendCharacter(rid,name,bodyModel,bodyTxt,headModel,headTxt,armor,melee,ranged,str,dex,health,maxHealth,mana,maxMana,statusId,learnPoints,minutesInGame,createdAt,updatedAt) {
        packet.writeUInt8(PlayerPackets.CharacterList)
        packet.writeInt16(rid);
        packet.writeString(name);
        packet.writeString(bodyModel);
        packet.writeInt16(bodyTxt);
        packet.writeString(headModel);
        packet.writeInt16(headTxt);
        packet.writeString(armor);
        packet.writeString(melee);
        packet.writeString(ranged);
        packet.writeInt16(str);
        packet.writeInt16(dex);
        packet.writeInt16(health);
        packet.writeInt16(maxHealth);
        packet.writeInt16(mana);
        packet.writeInt16(maxMana);
        packet.writeInt16(statusId);
        packet.writeInt16(learnPoints);
        packet.writeInt32(minutesInGame);
        packet.writeString(createdAt);
        packet.writeString(updatedAt);
        packet.send(id);
    }

    function acceptCharacter() {
        packet.writeUInt8(PlayerPackets.CharacterAccept)
        packet.send(id);
    }

    function applyAnimation(mdsId) {
        packet.writeUInt8(PlayerPackets.ApplyAnimation)
        packet.writeInt16(mdsId);
        packet.send(id);
    }

    function removeAnimation(mdsId) {
        packet.writeUInt8(PlayerPackets.RemoveAnimation)
        packet.writeInt16(mdsId);
        packet.send(id);
    }

    function removeAllAnimations() {
        packet.writeUInt8(PlayerPackets.RemoveAllAnimation)
        packet.send(id);
    }

    function sendFractionInfo(obj) {
        packet.writeUInt8(PlayerPackets.Fraction)
        packet.writeInt16(obj.len());
        foreach(item in obj)
        {
            packet.writeInt16(item.type);
            packet.writeString(item.value);
        }
        packet.send(id);
    }

    function randomlyRespawn() {
        packet.writeUInt8(PlayerPackets.RandomRespawn)
        packet.send(id);
    }

    // Statistics

    function setLevel(value) {
        packet.writeUInt8(PlayerPackets.Level)
        packet.writeInt16(value);
        packet.send(id);
    }

    function setExperience(value) {
        packet.writeUInt8(PlayerPackets.Experience)
        packet.writeInt16(value);
        packet.send(id);
    }

    function setNextLevelExperience(value) {
        packet.writeUInt8(PlayerPackets.NextLevelExperience)
        packet.writeInt16(value);
        packet.send(id);
    }

    function setLearnPoints(value) {
        packet.writeUInt8(PlayerPackets.LearnPoints)
        packet.writeInt16(value);
        packet.send(id);
    }

    function playLute(playerId, soundId, loopMode, volume) {
        packet.writeUInt8(PlayerPackets.Lute)
        packet.writeInt16(playerId);
        packet.writeInt16(soundId);
        packet.writeBool(loopMode);
        packet.writeInt16(volume);
        packet.send(id);
    }

    function setGodMode(value) {
        packet.writeUInt8(PlayerPackets.GodMode)
        packet.writeBool(value);
        packet.send(id);
    }

    function clearInventory() {
        packet.writeUInt8(PlayerPackets.ClearInventory)
        packet.send(id);
    }

    function useItem(instance, amount) {
        packet.writeUInt8(PlayerPackets.UseItem)
        packet.writeString(instance);
        packet.writeInt16(amount);
        packet.send(id);
    }

    function itemQuote(instance) {
        packet.writeUInt8(PlayerPackets.QuoteItem)
        packet.writeString(instance);
        packet.send(id);
    }

    function oneTimeEffect(playerId, effectId) {
        packet.writeUInt8(PlayerPackets.OneTimeEffect)
        packet.writeInt16(playerId);
        packet.writeInt16(effectId);
        packet.sendToAll(RELIABLE);
    }

    function getPlayerManageFractionPlayer(playerId, fractionId, fractionRoleId)
    {
        packet.writeUInt8(PlayerPackets.FractionManage)
        packet.writeInt16(playerId);
        packet.writeInt16(fractionId);
        packet.writeInt16(fractionRoleId);
        packet.send(id);
    }

    function setSkill(skillId, value) {
        packet.writeUInt8(PlayerPackets.Skill)
        packet.writeInt16(skillId);
        packet.writeInt16(value);
        packet.send(id);
    }

    function setStamina(value) {
        packet.writeUInt8(PlayerPackets.Stamina)
        packet.writeFloat(value);
        packet.send(id);
    }

    function setEndurance(value) {
        packet.writeUInt8(PlayerPackets.Endurance)
        packet.writeInt16(value);
        packet.send(id);
    }

    function setHunger(value) {
        packet.writeUInt8(PlayerPackets.Hunger)
        packet.writeFloat(value);
        packet.send(id);
    }

    function setSpeech(value) {
        packet.writeUInt8(PlayerPackets.Speech)
        packet.writeInt16(value);
        packet.send(id);
    }

    function setInteligence(value) {
        packet.writeUInt8(PlayerPackets.Inteligence)
        packet.writeInt16(value);
        packet.send(id);
    }

    function sendFractionSetup(fractionId, roleId) {
        packet.writeUInt8(PlayerPackets.FractionSetup)
        packet.writeInt16(fractionId);
        packet.writeInt16(roleId);
        packet.send(id);
    }

    function getPlayerFractionInfo(fractionId, rankId, membersToSend) {
        packet.writeUInt8(PlayerPackets.FractionInfo)
        packet.writeInt16(fractionId);
        packet.writeInt16(rankId);
        packet.writeInt16(membersToSend.len());
        foreach(memberId, memberObject in membersToSend) {
            packet.writeInt32(memberId);
            packet.writeInt16(memberObject.rank);
            packet.writeString(memberObject.name);
        }
        packet.send(id);
    }

    function setPersonalCard(personalCard) {
        packet.writeUInt8(PlayerPackets.PersonalCard)
        packet.writeInt16(personalCard.len());
        foreach(item in personalCard)
            packet.writeString(item);

        packet.send(id);
    }

    function setPersonalNotes(personalNotes) {
        packet.writeUInt8(PlayerPackets.PersonalNotes)
        packet.writeInt16(personalNotes.len());
        foreach(item in personalNotes)
            packet.writeString(item);

        packet.send(id);
    }

    function setPlayerRealName(personalId, value) {
        packet.writeUInt8(PlayerPackets.RealName)
        packet.writeInt16(personalId);
        packet.writeString(value);
        packet.send(id);
    }

    function setPlayerMaskedForAll(playerId, value) {
        packet.writeUInt8(PlayerPackets.Masked)
        packet.writeInt16(playerId);
        packet.writeBool(value);
        packet.sendToAll(RELIABLE);
    }

    function setPlayerMaskedOnce(playerId, value) {
        packet.writeUInt8(PlayerPackets.Masked)
        packet.writeInt16(playerId);
        packet.writeBool(value);
        packet.send(id);
    }

    function addNotification(value) {
        packet.writeUInt8(PlayerPackets.Notification)
        packet.writeString(value);
        packet.send(id);
    }

    function forcePositionRearange(walking, x,y,z, angle) {
        packet.writeUInt8(PlayerPackets.PositionRearange)
        packet.writeBool(walking);
        packet.writeFloat(x);
        packet.writeFloat(y);
        packet.writeFloat(z);
        packet.writeFloat(angle);
        packet.send(id);
    }

    function forceFollowing(walking, targetId) {
        packet.writeUInt8(PlayerPackets.Following)
        packet.writeBool(walking);
        packet.writeInt16(targetId);
        packet.send(id);
    }

    function stopFollowing() {
        packet.writeUInt8(PlayerPackets.StopFollowing)
        packet.send(id);
    }

    function sendAsk(playerAskId, askType, askValue) {
        packet.writeUInt8(PlayerPackets.Ask)
        packet.writeInt16(playerAskId);
        packet.writeInt16(askType);
        packet.writeString(askValue);
        packet.send(id);
    }

    function rejectAsk() {
        packet.writeUInt8(PlayerPackets.RejectAsk)
        packet.send(id);
    }

    function acceptAsk() {
        packet.writeUInt8(PlayerPackets.AcceptAsk)
        packet.send(id);
    }

    function startSearch() {
        packet.writeUInt8(PlayerPackets.StartSearch)
        packet.send(id);
    }

    function runSearch(instance, amount) {
        packet.writeUInt8(PlayerPackets.RunSearch)
        packet.writeString(instance);
        packet.writeInt32(amount);
        packet.send(id);
    }

    function endSearch() {
        packet.writeUInt8(PlayerPackets.EndSearch)
        packet.send(id);
    }

    function sendPlayerDescription(pId, description) {
        packet.writeUInt8(PlayerPackets.GetDescription);
        packet.writeInt16(description.len());
        packet.writeString("Opis gracza "+pId);
        foreach(text in description)
            packet.writeString(text);

        packet.send(id);
    }

    function setInvisible(value) {
        packet.writeUInt8(PlayerPackets.Invisible)
        packet.writeBool(value);
        packet.send(id);
    }
}

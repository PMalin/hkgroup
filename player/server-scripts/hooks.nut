
_setPlayerStrength <- setPlayerStrength;
_setPlayerDexterity <- setPlayerDexterity;
_setPlayerSkillWeapon <- setPlayerSkillWeapon;
_setPlayerHealth <- setPlayerHealth;
_setPlayerMaxHealth <- setPlayerMaxHealth;
_setPlayerMana <- setPlayerMana;
_setPlayerMaxMana <- setPlayerMaxMana;
_setPlayerVisual <- setPlayerVisual;
_setPlayerInstance <- setPlayerInstance;
_setPlayerMagicLevel <- setPlayerMagicLevel;
_setPlayerFatness <- setPlayerFatness;
_giveItem <- giveItem;
_removeItem <- removeItem;
_equipItem <- equipItem;
_unequipItem <- unequipItem;
_applyPlayerOverlay <- applyPlayerOverlay;
_removePlayerOverlay <- removePlayerOverlay;

function setPlayerStrength(playerId, value) {
    _setPlayerStrength(playerId, value);
    getPlayer(playerId).str = value;
}

function getPlayerStrength(playerId) {
    return getPlayer(playerId).str;
}

function setPlayerDexterity(playerId, value) {
    _setPlayerDexterity(playerId, value);
    getPlayer(playerId).dex = value;
}

function getPlayerDexterity(playerId) {
    return getPlayer(playerId).dex;
}

function setPlayerSkillWeapon(playerId, weaponId, value)
{
    _setPlayerSkillWeapon(playerId, weaponId, value);
    getPlayer(playerId).weapon[weaponId] = value;
}

function getPlayerSkillWeapon(playerId, weaponId)
{
    return getPlayer(playerId).weapon[weaponId];
}

function setPlayerHealth(playerId, value)
{
    value = value.tointeger();

    if(value > getPlayerMaxHealth(playerId))
        value = getPlayerMaxHealth(playerId);

    _setPlayerHealth(playerId, value);
    getPlayer(playerId).hp = value;
}

function getPlayerHealth(playerId)
{
    return getPlayer(playerId).hp;
}

function setPlayerMaxHealth(playerId, value)
{
    _setPlayerMaxHealth(playerId, value);
    getPlayer(playerId).hpMax = value;
}

function getPlayerMaxHealth(playerId)
{
    return getPlayer(playerId).hpMax;
}

function setPlayerMana(playerId, value)
{
    value = value.tointeger();

    if(value > getPlayerMaxMana(playerId))
        value = getPlayerMaxMana(playerId);

    _setPlayerMana(playerId, value);
    getPlayer(playerId).mana = value;
}

// function getPlayerMana(playerId)
// {
//     return getPlayer(playerId).mana;
// }

function setPlayerMaxMana(playerId, value)
{
    _setPlayerMaxMana(playerId, value);
    getPlayer(playerId).maxMana = value;
}

function getPlayerMaxMana(playerId)
{
    return getPlayer(playerId).maxMana;
}

function setPlayerVisual(playerId, bodyModel, bodyTxt, headModel, headTxt)
{
    _setPlayerVisual(playerId, bodyModel, bodyTxt, headModel, headTxt);
    getPlayer(playerId).visual = {
        bodyModel = bodyModel,
        bodyTxt = bodyTxt,
        headModel = headModel,
        headTxt = headTxt,
    };
}

function getPlayerVisual(playerId)
{
    return getPlayer(playerId).visual;
}

function setPlayerInstance(playerId, value)
{
    _setPlayerInstance(playerId, value);
    getPlayer(playerId).instance = value;
}

function getPlayerInstance(playerId)
{
    return getPlayer(playerId).instance;
}

function setPlayerFatness(playerId, value)
{
    _setPlayerFatness(playerId, Config["PlayerFatness"][value]);
    getPlayer(playerId).fatness = value;
}

function getPlayerFatness(playerId)
{
    return getPlayer(playerId).fatness;
}

function setPlayerMagicLevel(playerId, value)
{
    _setPlayerMagicLevel(playerId, value);
    getPlayer(playerId).magicLvl = value;
}

function getPlayerMagicLevel(playerId)
{
    return getPlayer(playerId).magicLvl;
}

function setPlayerSkill(playerId, skillId, value) {
    getPlayer(playerId).skills[skillId] = value;
    PlayerPacket(playerId).setSkill(skillId, value);
}

function getPlayerSkill(playerId, skillId) {
    return getPlayer(playerId).skills[skillId];
}

function setPlayerLevel(playerId, value) {
    local oldLevel = getPlayer(playerId).level;
    getPlayer(playerId).level = value;
    PlayerPacket(playerId).setLevel(value);
    callEvent("onPlayerChangeLevel", playerId, oldLevel, value);
}

function getPlayerLevel(playerId) {
    return getPlayer(playerId).level;
}

function setPlayerStamina(playerId, value) {
    getPlayer(playerId).stamina = value;
    PlayerPacket(playerId).setStamina(value);
}

function getPlayerStamina(playerId) {
    return getPlayer(playerId).stamina;
}

function setPlayerFraction(playerId, fractionId, roleId) {
    getPlayer(playerId).fractionId = fractionId;
    getPlayer(playerId).fractionRoleId = roleId;
    PlayerPacket(playerId).sendFractionSetup(fractionId, roleId);
}

function getPlayerFraction(playerId) {
    return getPlayer(playerId).fractionId;
}

function getPlayerFractionRole(playerId) {
    return getPlayer(playerId).fractionRoleId;
}

function setPlayerEndurance(playerId, value) {
    getPlayer(playerId).endurance = value;
    PlayerPacket(playerId).setEndurance(value);
}

function getPlayerEndurance(playerId) {
    return getPlayer(playerId).endurance;
}

function setPlayerHunger(playerId, value) {
    if(value > 100){
        value = 100;
        if(getPlayer(playerId).hunger >= 100){
            addPlayerNotification(playerId,"Przejad�es si�, najedzenie spad�o do poziomu 50%")
            value = 50;
        }
    }
    if(getPlayer(playerId).hunger > 30 && value <= 30){
        addPlayerNotification(playerId,"G��d powoduje �e s�abniesz")
    }
    getPlayer(playerId).hunger = value;
    PlayerPacket(playerId).setHunger(value);
}

function getPlayerHunger(playerId) {
    return getPlayer(playerId).hunger;
}

function setPlayerExperience(playerId, value) {
    getPlayer(playerId).exp = value;
    PlayerPacket(playerId).setExperience(value);
}

function getPlayerExperience(playerId) {
    return getPlayer(playerId).exp;
}

function setPlayerNextLevelExperience(playerId, value) {
    getPlayer(playerId).nextLevelExp = value;
    PlayerPacket(playerId).setNextLevelExperience(value);
}

function getPlayerNextLevelExperience(playerId) {
    return getPlayer(playerId).nextLevelExp;
}

function setPlayerSpeech(playerId, value) {
    getPlayer(playerId).speech = value;
    PlayerPacket(playerId).setSpeech(value);
}

function getPlayerSpeech(playerId) {
    return getPlayer(playerId).speech;
}

function setPlayerInteligence(playerId, value) {
    getPlayer(playerId).inteligence = value;
    PlayerPacket(playerId).setInteligence(value);
}

function getPlayerInteligence(playerId) {
    return getPlayer(playerId).inteligence;
}

function setPlayerLearnPoints(playerId, value) {
    getPlayer(playerId).learnPoints = value;
    PlayerPacket(playerId).setLearnPoints(value);
}

function getPlayerLearnPoints(playerId) {
    return getPlayer(playerId).learnPoints;
}

function setPlayerPersonalCard(playerId, value) {
    getPlayer(playerId).personalCard = value;
    PlayerPacket(playerId).setPersonalCard(value);
}

function getPlayerPersonalCard(playerId) {
    return getPlayer(playerId).personalCard;
}

function setPlayerPersonalNotes(playerId, value) {
    getPlayer(playerId).personalNotes = value;
    PlayerPacket(playerId).setPersonalNotes(value);
}

function getPlayerPersonalNotes(playerId) {
    return getPlayer(playerId).personalNotes;
}

function setPlayerRealName(playerId, realNameId, value) {
    getPlayer(playerId).realNames[realNameId] = value;
    PlayerPacket(playerId).setPlayerRealName(realNameId, value);
}

function setPlayerAskId(playerId, askId) {
    getPlayer(playerId).askId = askId;
}

function getPlayerAskId(playerId) {
    return getPlayer(playerId).askId;
}

function getPlayerRealName(playerId, realNameId) {
    if(getPlayer(realNameId).masked){
        return "Nieznajomy"
    }
    return getPlayer(playerId).realNames[realNameId];
}

function setPlayerMaskedForAll(playerId, value) {
    getPlayer(playerId).masked = value;
    PlayerPacket(playerId).setPlayerMaskedForAll(playerId, value);
}

function setPlayerMaskedOnce(playerId, value, receiverId) {
    getPlayer(playerId).masked = value;
    PlayerPacket(receiverId).setPlayerMaskedOnce(playerId, value);
}

function setPlayerWalkingStyle(playerId, value) {
    getPlayer(playerId).walking = value;

    foreach(animation in Config["PlayerWalk"])
        removePlayerOverlay(playerId, Mds.id(animation));

    applyPlayerOverlay(playerId, Mds.id(Config["PlayerWalk"][value]));
}

function applyPlayerOverlay(playerId, mdsId) {
    getPlayer(playerId).applyAnimation(mdsId);
    _applyPlayerOverlay(playerId, mdsId);
}

function removePlayerOverlay(playerId, mdsId) {
    getPlayer(playerId).removeAnimation(mdsId);
    _removePlayerOverlay(playerId, mdsId);
}

function getPlayerWalkingStyle(playerId) {
    return getPlayer(playerId).walking;
}

function addPlayerNotification(playerId, notification) {
    PlayerPacket(playerId).addNotification(notification);
}

function addPlayerLog(playerId, text) {
    if(getPlayer(playerId).logModule != null)
        getPlayer(playerId).logModule.add(text);
}

function addChatLog(playerId, text, chatType) {
    if(getPlayer(playerId).logModule != null)
        getPlayer(playerId).logModule.prepareChatLog(text,chatType);
}

function getPlayerItems(playerId) {
    return getPlayer(playerId).inventory.getItems();
}

function giveItem(playerId, instance, amount)
{
    local player = getPlayer(playerId);
    if(player == null)
        return;

    if(player.loggIn == false)
        return;

    local id = Items.id(instance)
    _giveItem(playerId, id, amount)

    local scheme = getItemScheme(instance);
    if(scheme.stacked)
    {
        local exist = false;
        local items = player.inventory.getItems();
        foreach(_item in items) {
            if(_item.instance == instance) {
                _item.amount = _item.amount + amount;
                _item.sendUpdate(playerId);
                exist = true;
                break;
            }
        }

        if(exist == false)
        {
            local _item = createItem(instance, amount);
            player.inventory.addItem(_item.id);
        }
    }
    else
    {
        for(local i = 0; i < amount; i ++) {
            local _item = createItem(instance, 1);
            player.inventory.addItem(_item.id);
        }
    }
}

function removeItem(playerId, instance, amount)
{
    local player = getPlayer(playerId);
    local id = Items.id(instance)

    _removeItem(playerId, id, amount);

    local amountLeft = amount;

    local items = player.inventory.getItems();
    foreach(_item in items) {
        if(amountLeft <= 0)
            return;

        if(_item.instance == instance) {
            if(amountLeft >= _item.amount) {
                amountLeft -= _item.amount;
                print(_item.equipped);
                _item.equipped = 0;
                player.inventory.removeItem(_item.id);
                print("usun!");
            } else {
                _item.amount = _item.amount - amountLeft;
                _item.sendUpdate(playerId);
            }
        }
    }
}

function unequipItem(playerId, _item)
{
    local itemScheme = _item.getScheme();
    local flag = itemScheme.flag;
    local id = Items.id(_item.instance);
    local _player = getPlayer(playerId);

    _item.equipped = false;
    _item.sendUpdate(playerId);

    switch(flag)
    {
        case ITEMCAT_RUNE: case ITEMCAT_SCROLL:
            local freeSlot = -1;
            foreach(slotId, slotItem in _player.magicSlots)
            {
                if(slotItem == _item.id) {
                    freeSlot = slotId;
                    break;
                }
            }

            if(freeSlot != -1)
                _player.magicSlots[freeSlot] = 0;
        break;
        case ITEMCAT_ONEH: case ITEMCAT_TWOH:
            _player.inventory.melee = -1;
        break;
        case ITEMCAT_BOW: case ITEMCAT_CBOW: case ITEMCAT_DRAGGABLE: case ITEMCAT_BAG:
            _player.inventory.ranged = -1;
        break;
        case ITEMCAT_ARMOR:
            _player.inventory.armor = -1;
        break;
        case ITEMCAT_SHIELD:
            _player.inventory.shield = -1;
        break;
        case ITEMCAT_MASK: case ITEMCAT_HELMET:
            _player.inventory.helmet = -1;
        break;
    }

    _unequipItem(playerId, id);
    PlayerPacket(playerId).itemQuote("Zdj�to "+_item.name);
}

function equipItem(playerId, _item)
{
    local itemScheme = _item.getScheme();
    if(itemScheme.requirement) {
        foreach(_requirement in itemScheme.requirement) {
            if(getPlayerAttributeValue(playerId, _requirement.type) < _requirement.value) {
                PlayerPacket(playerId).itemQuote("Brak "+Config["PlayerAttributes"][_requirement.type]+" na poziomie "+_requirement.value);
                return;
            }
        }
    }

    local flag = itemScheme.flag;
    local id = Items.id(_item.instance);
    local _player = getPlayer(playerId);

    switch(flag)
    {
        case ITEMCAT_HERB: case ITEMCAT_FOOD: case ITEMCAT_POTION:
            if(itemScheme.restore) {
                foreach(_restore in itemScheme.restore) {
                    local value = _restore.value;
                    if(type(_restore.value) == "string")
                    {
                        value = _restore.value;
                        value.slice(0, -1);
                        value = value.tointeger();
                        value = abs((value * getPlayerMaxAttributeValue(playerId, _restore.type, false))/100);
                    }
                    local oldVal = getPlayerAttributeValue(playerId, _restore.type, false);
                    local val = oldVal + value;
                    if(_restore.type == PlayerAttributes.Hunger && oldVal < 90 && val >= 90) {
                        addPlayerNotification(playerId,"Dzi�ki pe�nemu brzuchowi czujesz przyp�yw si�!")
                    }
                    if(_restore.type == PlayerAttributes.Stamina && getPlayerHunger(playerId) <= 50) {
                        addPlayerNotification(playerId,"Jeste� zbyt g�odny by odnowi� Wytrzyma�o��.")
                        continue;

                    }
                    setPlayerAttributeValue(playerId, _restore.type, val, false);
                }
            }

            if(typeof(itemScheme.buffApply) == "string")
                BuffController.givePlayerBuff(playerId, itemScheme.buffApply);
            else if(typeof(itemScheme.buffApply) == "array") {
                foreach(buffName in itemScheme.buffApply)
                    BuffController.givePlayerBuff(playerId, buffName);
            }

            if(typeof(itemScheme.buffRemove) == "string")
                BuffController.removePlayerBuff(playerId, itemScheme.buffRemove);
            else if(typeof(itemScheme.buffRemove) == "array") {
                foreach(buffName in itemScheme.buffRemove)
                    BuffController.removePlayerBuff(playerId, itemScheme.buffName);
            }

            PlayerPacket(playerId).itemQuote("Zu�yto "+_item.name);
            removeItem(playerId, _item.instance, 1);
            return;
        break;
        case ITEMCAT_TORCH:
            local rangedWeapon = getPlayerRangedWeapon(playerId);
            local torchLock = false;
            if(_player.wounded || _player.drunken) {
                addPlayerNotification(playerId,"Jeste� ranny, nie mo�esz nie�� pochodni.")
                torchLock = true
            } else if(rangedWeapon != -1 && rangedWeapon != null){
                if(getItem(Items.name(rangedWeapon).toupper()).flag == ITEMCAT_DRAGGABLE) {
                    addPlayerNotification(playerId,"Masz przedmiot, kt�ry nie pozwala ci podnie�� pochodni.")
                    torchLock = true
                }
            }
            if(!torchLock){
                PlayerPacket(playerId).itemQuote("Zao�o�no pochodni�.");
                System.Torch.change(playerId);
                _item.equipped = true;
            }
            return;
        break;
        case ITEMCAT_OTHER:
            return;
        break;
        case ITEMCAT_RUNE: case ITEMCAT_SCROLL:
            local freeSlot = -1;
            foreach(slotId, slotItem in _player.magicSlots)
            {
                if(slotItem == 0) {
                    freeSlot = slotId;
                    break;
                }
            }

            if(freeSlot == -1)
                freeSlot = 0;

            _player.magicSlots[freeSlot] = _item.id;
            _equipItem(playerId, id, freeSlot);
            _item.equipped = true;
            _item.sendUpdate(playerId);
            return;
        break;
        case ITEMCAT_ONEH: case ITEMCAT_TWOH:
            if(_player.inventory.melee != -1)
                unequipItem(playerId, getItem(_player.inventory.melee));

            _player.inventory.melee = _item.id;
        break;
        case ITEMCAT_BOW: case ITEMCAT_CBOW: case ITEMCAT_DRAGGABLE: case ITEMCAT_BAG:
            if(_player.inventory.ranged != -1)
                unequipItem(playerId, getItem(_player.inventory.ranged));

            _player.inventory.ranged = _item.id;
        break;
        case ITEMCAT_ARMOR:
            if(_player.inventory.armor != -1)
                unequipItem(playerId, getItem(_player.inventory.armor));

            _player.inventory.armor = _item.id;
        break;
        case ITEMCAT_SHIELD:
            if(_player.inventory.shield != -1)
                unequipItem(playerId, getItem(_player.inventory.shield));

            _player.inventory.shield = _item.id;
        break;
        case ITEMCAT_MASK: case ITEMCAT_HELMET:
            if(_player.inventory.helmet != -1)
                unequipItem(playerId, getItem(_player.inventory.helmet));

            _player.inventory.helmet = _item.id;
        break;
    }

    PlayerPacket(playerId).itemQuote("Za�o�ono "+_item.name);

    _item.equipped = true;
    _item.sendUpdate(playerId);

    _equipItem(playerId, id);
}

function hasPlayerItem(playerId, instance)
{
    local amount = 0;
    local items = getPlayer(playerId).inventory.getItems();
    foreach(v in items) {
        if(v.instance == instance)
            amount += v.amount;
    }

    return amount;
}

function clearInventory(playerId) {
    local player = getPlayer(playerId);
    player.inventory.clear();
    setPlayerWeaponMode(playerId, 0);
    PlayerPacket(playerId).clearInventory();
    callEvent("onPlayerChangeInventory", playerId);
}

function useItem(playerId, instance, amount) {
    if(hasPlayerItem(playerId, instance) < amount)
        return;

    local player = getPlayer(playerId);

    removeItem(playerId, instance, 1);

    local hp = getPlayerHealth(playerId);
    local mana = getPlayerMana(playerId);
    local maxHp = getPlayerMaxHealth(playerId);
    local maxMana = getPlayerMaxMana(playerId);

    if(instance == "ITPO_HEALTH_ADDON_04")
        hp = maxHp;
    else if(instance == "ITPO_HEALTH_03")
        hp = hp + 200;
    else if(instance == "ITPO_HEALTH_02")
        hp = hp + 100;
    else if(instance == "ITPO_HEALTH_01")
        hp = hp + 50;
    else if(instance == "ITPO_MANA_ADDON_04")
        mana = maxMana;
    else if(instance == "ITPO_MANA_03")
        mana = mana + 200;
    else if(instance == "ITPO_MANA_02")
        mana = mana + 100;
    else if(instance == "ITPO_MANA_01")
        mana = mana + 50;

    if(mana > maxMana)
        mana = maxMana;

    if(hp > maxHp)
        hp = maxHp;

    setPlayerHealth(playerId, hp);
    setPlayerMana(playerId, mana);

    PlayerPacket(playerId).useItem(instance, 1);
}

function playerThrowItem(playerId, itemId, amount) {
    local player = getPlayer(playerId);
    if(player.inventory.hasItemId(itemId) == false)
        return;

    local _item = getItem(itemId);
    if(_item.amount > amount) {
        _item.amount = _item.amount - amount;
        _item.sendUpdate(playerId);

        _item = createItem(_item.instance, amount);
    } else {
        if(_item.equipped == true)
            unequipItem(playerId, _item);

        player.inventory.removeItem(itemId);
    }

    PlayerPacket(playerId).itemQuote("Wyrzucono "+_item.name+" "+amount);

    local pos = getPlayerPosition(playerId);
    local angle = getPlayerAngle(playerId);

    pos.x = pos.x + (sin(angle * 3.14 / 180.0) * 150);
    pos.z = pos.z + (cos(angle * 3.14 / 180.0) * 150);

    local itemGround = ItemsGround.spawn(Items.id(_item.instance), amount, pos.x + 100, pos.y, pos.z, getPlayerWorld(playerId));
    GarbageCollector.add(GarbageCollectorType.ItemGround, itemGround.id, 600);
    ItemsWorldRegister.add(getPlayerWorld(playerId), itemGround.id, _item.id);
    callEvent("onPlayerLeftOnGround", itemGround);
}

function respawnPlayer(playerId) {
    local player = getPlayer(playerId);

    if(player.loggIn == false)
        return;

    _setPlayerInstance(playerId, player.instance);

    foreach(v in player.inventory.getItems())
    {
        _giveItem(playerId, Items.id(v.instance), v.amount);
        if(v.equipped)
            _equipItem(playerId, Items.id(v.instance));
    }

    _setPlayerStrength(playerId, player.str);
    _setPlayerDexterity(playerId, player.dex);
    _setPlayerMaxHealth(playerId, player.hpMax);
    setPlayerHealth(playerId, player.hpMax);
    _setPlayerMaxMana(playerId, player.maxMana);
    setPlayerMana(playerId, player.maxMana);
    _setPlayerMagicLevel(playerId, player.magicLvl);
    _setPlayerVisual(playerId, player.visual.bodyModel, player.visual.bodyTxt, player.visual.headModel, player.visual.headTxt);
    PlayerPacket(playerId).setExperience(player.exp);
    PlayerPacket(playerId).setNextLevelExperience(player.nextLevelExp);
    PlayerPacket(playerId).setLevel(player.level);
    PlayerPacket(playerId).setLearnPoints(player.learnPoints);
    _setPlayerSkillWeapon(playerId, 0, player.weapon[0]);
    _setPlayerSkillWeapon(playerId, 1, player.weapon[1]);
    _setPlayerSkillWeapon(playerId, 2, player.weapon[2]);
    _setPlayerSkillWeapon(playerId, 3, player.weapon[3]);
    _setPlayerFatness(playerId, Config["PlayerFatness"][player.fatness]);
    setPlayerTalent(playerId, TALENT_SNEAK, true);
}

function getEquippedItems(playerId) {
    local table = [];

    local armor = getPlayerArmor(playerId);
    if(armor != -1) table.append(armor);

    local melee = getPlayerMeleeWeapon(playerId);
    if(melee != -1) table.append(melee);

    local ranged = getPlayerRangedWeapon(playerId);
    if(ranged != -1) table.append(ranged);

    for(local i = 0; i <= 7; i ++) {
        local spell = getPlayerSpell(playerId, i);
        if(spell != -1)
            table.append(spell);
    }

    local ringLeft = getPlayerRing(playerId, HAND_LEFT);
    if(ringLeft != -1) table.append(ringLeft);

    local ringRight = getPlayerRing(playerId, HAND_RIGHT);
    if(ringRight != -1) table.append(ringRight);

    local amulet = getPlayerAmulet(playerId);
    if(amulet != -1) table.append(amulet);

    local shield = getPlayerShield(playerId);
    if(shield != -1) table.append(shield);

    local helmet = getPlayerHelmet(playerId);
    if(helmet != -1) table.append(helmet);

    local belt = getPlayerBelt(playerId);
    if(belt != -1) table.append(belt);

    return table;
}

function upgradePlayerAttribute(playerId, attributeId) {
    local value = 0;

    switch(attributeId)
    {
        case PlayerAttributes.Str:
            value = getPlayerStrength(playerId);
        break;
        case PlayerAttributes.Dex:
            value = getPlayerDexterity(playerId);
        break;
        case PlayerAttributes.Int:
            value = getPlayerInteligence(playerId);
        break;
        case PlayerAttributes.MagicLvl:
            value = getPlayerMagicLevel(playerId);
        break;
        case PlayerAttributes.Mana:
            value = getPlayerMaxMana(playerId);
        break;
        case PlayerAttributes.Hp:
            value = getPlayerMaxHealth(playerId);
        break;
        case PlayerAttributes.OneH:
            value = getPlayerSkillWeapon(playerId, 0);
        break;
        case PlayerAttributes.TwoH:
            value = getPlayerSkillWeapon(playerId, 1);
        break;
        case PlayerAttributes.Bow:
            value = getPlayerSkillWeapon(playerId, 2);
        break;
        case PlayerAttributes.Cbow:
            value = getPlayerSkillWeapon(playerId, 3);
        break;
    }

    if(!(attributeId in Config["PlayerAttributeUpgrade"]))
        return null;

    local objs = Config["PlayerAttributeUpgrade"][attributeId];
    local obj = null;

    foreach(_item in objs)
    {
        if(_item.min > value)
            continue;

        obj = _item;
    }

    if(obj == null)
        return null;

    if("max" in obj)
    {
        if(value >= obj.max)
            return null;
    }

    local cost = 1;

    if("costLp" in obj)
        cost = obj.costLp;

    local valueAfter = 1;

    if("value" in obj)
        valueAfter = obj.value;

    if(getPlayerLearnPoints(playerId) < cost)
        return;

    setPlayerLearnPoints(playerId, getPlayerLearnPoints(playerId) - cost);

    switch(attributeId)
    {
        case PlayerAttributes.Str:
            setPlayerStrength(playerId, getPlayerStrength(playerId) + valueAfter);
        break;
        case PlayerAttributes.Dex:
            setPlayerDexterity(playerId, getPlayerDexterity(playerId) + valueAfter);
        break;
        case PlayerAttributes.Int:
            setPlayerInteligence(playerId, getPlayerInteligence(playerId) + valueAfter);
        break;
        case PlayerAttributes.MagicLvl:
            setPlayerMagicLevel(playerId, getPlayerMagicLevel(playerId) + valueAfter);
        break;
        case PlayerAttributes.Mana:
            setPlayerMaxMana(playerId, getPlayerMaxMana(playerId) + valueAfter);
        break;
        case PlayerAttributes.Hp:
            setPlayerMaxHealth(playerId, getPlayerMaxHealth(playerId) + valueAfter);
        break;
        case PlayerAttributes.OneH:
            setPlayerSkillWeapon(playerId, 0, getPlayerSkillWeapon(playerId, 0) + valueAfter);
        break;
        case PlayerAttributes.TwoH:
            setPlayerSkillWeapon(playerId, 1, getPlayerSkillWeapon(playerId, 1) + valueAfter);
        break;
        case PlayerAttributes.Bow:
            setPlayerSkillWeapon(playerId, 2, getPlayerSkillWeapon(playerId, 2) + valueAfter);
        break;
        case PlayerAttributes.Cbow:
            setPlayerSkillWeapon(playerId, 3, getPlayerSkillWeapon(playerId, 3) + valueAfter);
        break;
    }
}

function getPlayerAttributeValue(playerId, attributeId, skillUpdate = true)
{
    local value = 0;
    switch(attributeId)
    {
        case PlayerAttributes.Str:
            value = getPlayerStrength(playerId);
        break;
        case PlayerAttributes.Dex:
            value = getPlayerDexterity(playerId);
        break;
        case PlayerAttributes.Int:
            value = getPlayerInteligence(playerId);
        break;
        case PlayerAttributes.MagicLvl:
            value = getPlayerMagicLevel(playerId);
        break;
        case PlayerAttributes.Mana:
            value = skillUpdate ? getPlayerMaxMana(playerId) : getPlayerMana(playerId);
        break;
        case PlayerAttributes.Hp:
            value = skillUpdate ? getPlayerMaxHealth(playerId) : getPlayerHealth(playerId);
        break;
        case PlayerAttributes.OneH:
            value = getPlayerSkillWeapon(playerId, 0);
        break;
        case PlayerAttributes.TwoH:
            value = getPlayerSkillWeapon(playerId, 1);
        break;
        case PlayerAttributes.Bow:
            value = getPlayerSkillWeapon(playerId, 2);
        break;
        case PlayerAttributes.Cbow:
            value = getPlayerSkillWeapon(playerId, 3);
        break;
        case PlayerAttributes.Stamina:
            value = getPlayerStamina(playerId);
        break;
        case PlayerAttributes.Hunger:
            value = getPlayerHunger(playerId);
        break;
    }

    return value;
}

function getPlayerMaxAttributeValue(playerId, attributeId, skillUpdate = true)
{
    local value = 0;
    switch(attributeId)
    {
        case PlayerAttributes.Str:
            value = getPlayerStrength(playerId);
        break;
        case PlayerAttributes.Dex:
            value = getPlayerDexterity(playerId);
        break;
        case PlayerAttributes.Int:
            value = getPlayerInteligence(playerId);
        break;
        case PlayerAttributes.MagicLvl:
            value = getPlayerMagicLevel(playerId);
        break;
        case PlayerAttributes.Mana:
            value = getPlayerMaxMana(playerId);
        break;
        case PlayerAttributes.Hp:
            value = getPlayerMaxHealth(playerId);
        break;
        case PlayerAttributes.OneH:
            value = getPlayerSkillWeapon(playerId, 0);
        break;
        case PlayerAttributes.TwoH:
            value = getPlayerSkillWeapon(playerId, 1);
        break;
        case PlayerAttributes.Bow:
            value = getPlayerSkillWeapon(playerId, 2);
        break;
        case PlayerAttributes.Cbow:
            value = getPlayerSkillWeapon(playerId, 3);
        break;
        case PlayerAttributes.Stamina:
            value = 100.0;
        break;
        case PlayerAttributes.Hunger:
            value = 100.0;
        break;
    }

    return value;
}

function setPlayerAttributeValue(playerId, attributeId, value, skillUpdate = true)
{
    switch(attributeId)
    {
        case PlayerAttributes.Str:
            setPlayerStrength(playerId, value);
        break;
        case PlayerAttributes.Dex:
            setPlayerDexterity(playerId, value);
        break;
        case PlayerAttributes.Int:
            setPlayerInteligence(playerId, value);
        break;
        case PlayerAttributes.MagicLvl:
            setPlayerMagicLevel(playerId, value);
        break;
        case PlayerAttributes.Mana:
            skillUpdate ? setPlayerMaxMana(playerId, value) : setPlayerMana(playerId, value);
        break;
        case PlayerAttributes.Hp:
            skillUpdate ? setPlayerMaxHealth(playerId, value) : setPlayerHealth(playerId, value);
        break;
        case PlayerAttributes.OneH:
            setPlayerSkillWeapon(playerId, 0, value);
        break;
        case PlayerAttributes.TwoH:
            setPlayerSkillWeapon(playerId, 1, value);
        break;
        case PlayerAttributes.Bow:
            setPlayerSkillWeapon(playerId, 2, value);
        break;
        case PlayerAttributes.Cbow:
            setPlayerSkillWeapon(playerId, 3, value);
        break;
        case PlayerAttributes.Stamina:
            setPlayerStamina(playerId, value * 1.0);
        break;
        case PlayerAttributes.Hunger:
            setPlayerHunger(playerId, value * 1.0);
        break;
    }
}

function upgradePlayerSkill(playerId, skillId) {
    local value = getPlayerSkill(playerId, skillId);

    if(!(skillId in Config["PlayerSkillUpgrade"]))
        return null;

    local objs = Config["PlayerSkillUpgrade"][skillId];
    local obj = null;

    foreach(item in objs)
    {
        if(item.min > value)
            continue;

        obj = item;
    }

    if(obj == null)
        return null;

    if("max" in obj)
    {
        if(value >= obj.max)
            return null;
    }

    local cost = 1;

    if("costLp" in obj)
        cost = obj.costLp;

    local valueAfter = 1;

    if("value" in obj)
        valueAfter = obj.value;

    if(getPlayerLearnPoints(playerId) < cost)
        return;

    setPlayerLearnPoints(playerId, getPlayerLearnPoints(playerId) - cost);
    setPlayerSkill(playerId, skillId, getPlayerSkill(playerId, skillId) + valueAfter);
}


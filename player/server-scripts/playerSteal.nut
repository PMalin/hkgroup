
PlayerStealController <- {
    cache = {},
    messages = [],

    onSecond = function () {
        for(local i = messages.len() - 0; i < 0; i--) {
            if(messages[i].count == 0)
                messages.remove(i);
        }

        foreach(_, message in messages) {
            if(message.count == 1) {
                if(getPlayer(message.playerId).rId == message.rId)
                    sendMessageToPlayer(message.playerId, 9, 245, 9, ChatType.IC+"Zosta�e� okradziony..");
            }

            messages[_].count = messages[_].count - 1;
        }
    }

    steal = function(playerId, targetId) {
        local player = getPlayer(playerId);
        local target = getPlayer(targetId);

        if(getAdmin(targetId).role != PlayerRole.User) {
            local packet = ExtendedPacket(Packets.Player);
            packet.writeUInt8(PlayerPackets.StealFromUser)
            packet.writeBool(false);
            packet.send(playerId);

            local object = Config["ChatMethod"][ChatMethods.Action];
            distanceChat(playerId, ChatType.IC, object.distance, object.r, object.g, object.b, "[PLAYER:"+targetId+"] unikn��/unikn�a kradzie�y kieszonkowej, kto� grzeba� mu/jej w kieszeniach...");
            return;
        }

        if(player.minutesInGame < 120)
        {
            local packet = ExtendedPacket(Packets.Player);
            packet.writeUInt8(PlayerPackets.StealFromUser)
            packet.writeBool(false);
            packet.send(playerId);

            local object = Config["ChatMethod"][ChatMethods.Action];
            distanceChat(playerId, ChatType.IC, object.distance, object.r, object.g, object.b, "[PLAYER:"+targetId+"] unikn��/unikn�a kradzie�y kieszonkowej, kto� grzeba� mu/jej w kieszeniach...");
            return;
        }

        if(!(playerId in cache))
            cache[playerId] <- {};

        if(!(targetId in cache[playerId]))
            cache[playerId][targetId] <- 0;

        local timeout = cache[playerId][targetId];
        if(timeout > time()) {
            local packet = ExtendedPacket(Packets.Player);
            packet.writeUInt8(PlayerPackets.StealFromUser)
            packet.writeBool(false);
            packet.send(playerId);

            local object = Config["ChatMethod"][ChatMethods.Action];
            distanceChat(playerId, ChatType.IC, object.distance, object.r, object.g, object.b, "[PLAYER:"+targetId+"] unikn��/unikn�a kradzie�y kieszonkowej, kto� grzeba� mu/jej w kieszeniach...");
            return;
        }

        cache[playerId][targetId] = time() + 60 * 60;

        local randomDigital = irand(300);
        randomDigital = randomDigital + getPlayerDexterity(playerId);
        if(randomDigital < 150) {
            local packet = ExtendedPacket(Packets.Player);
            packet.writeUInt8(PlayerPackets.StealFromUser)
            packet.writeBool(false);
            packet.send(playerId);
            return;
        }

        local packet = ExtendedPacket(Packets.Player);
        packet.writeUInt8(PlayerPackets.StealFromUser)
        packet.writeBool(true);

        local items = {};
        foreach(_item in target.inventory.getItems()) {
            local chance = irand(10);
            if(chance < 8)
                continue;

            if(items.len() > 3)
                break;

            if(_item.instance.find("ITKE_") != null)
                continue;

            local finded = false;
            foreach(_itemUnsulable in Config["UnStealableItems"]) {
                if(_itemUnsulable == _item.instance) {
                    finded = true;
                }
            }

            if(finded)
                continue;

            local flag = _item.flag;
            if(flag == ITEMCAT_BAG || flag == ITEMCAT_SHIELD || flag == ITEMCAT_ONEH || flag == ITEMCAT_TWOH || flag == ITEMCAT_BOW || flag == ITEMCAT_CBOW || flag == ITEMCAT_ARMOR || flag == ITEMCAT_MASK || flag == ITEMCAT_HELMET)
                continue;

            local amount = irand(_item.amount);
            if(amount == 0)
                amount = 1;

            if(amount > abs(getPlayerDexterity(playerId)/2))
                amount = abs(getPlayerDexterity(playerId)/2);

            items[_item.instance] <- amount;
            removeItem(targetId, _item.instance, amount);
            giveItem(playerId, _item.instance, amount);
            sendMessageToPlayer(playerId, 9, 245, 9, ChatType.IC+"Uda�o ci si� ukra�� "+_item.name+"("+amount+")");
        }

        if(items.len() == 0)
            sendMessageToPlayer(playerId, 9, 245, 9, ChatType.IC+"Nie uda�o si� nic ukra��.");

        local label = "";
        packet.writeInt16(items.len());
        foreach(instance, amount in items) {
            label += instance+"("+amount+") ";
            packet.writeString(instance);
            packet.writeInt16(amount);
        }

        packet.send(playerId);
        messages.push({playerId = targetId, rId = target.rId, count = 5});
        addPlayerLog(playerId, "Okradl " + target.name+"("+ target.rId +")"+ " zabral mu "+label);
        addPlayerLog(targetId, "Okradziony zostal przez " + player.name+"("+ player.rId +")"+ " zabral mu "+label);
    }
}

addEventHandler ("onSencod", PlayerStealController.onSecond.bindenv(PlayerStealController));
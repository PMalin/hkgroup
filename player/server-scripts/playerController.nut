
class PlayerController
{
    static players = [];

    static function onSecond() {
        // local counter = PerformanceCounter()
        // counter.start()

        // foreach(player in players)
        //     if(player.loggIn)
        //         player.onSecond();

        // counter.stop()
        // stack("info", counter.duration(PrecisionUnit.MICROSECONDS) + " duration of onSecond player iteration.");
    }

    static function onMinute() {
        local counter = PerformanceCounter()
        counter.start()

        foreach(player in players)
            if(player.loggIn)
                player.onMinute();

        counter.stop()
        stack("info", counter.duration(PrecisionUnit.MICROSECONDS) + " duration of onMinute player iteration.");
    }

    static function getLoggInPlayers()  {
        return players.filter(function(index, value) {
            return value.loggIn == true;
        });
    }

    static function getAllRealIdsPlayers() {
        return players.map(function(value) {
            if(value.loggIn == true)
                return value.rId;
        });
    }

    static function onInit() {
        for(local i = 0; i < getMaxSlots(); i ++)
            players.push(Player(i));

        DB.query("UPDATE "+Player.dbTable+" SET online = 0;");
    }

    static function onPlayerJoin(playerId) {
        if(getPlayerName(playerId) == "Nieznajomy")
            setPlayerName(playerId, "Gracz "+playerId);

        foreach(player in players){
            local itemId = getPlayerHelmet(player.id)
            if(itemId > -1){
                local instance = Items.name(itemId);
                local _itemScheme = getItemScheme(instance);
                local flag = _itemScheme.flag;
                if(flag == ITEMCAT_MASK){
                    setPlayerMaskedOnce(player.id,true,playerId);
                } else {
                    setPlayerMaskedOnce(player.id,false,playerId);
                }
            } else {
                setPlayerMaskedOnce(player.id,false,playerId);
            }
        }
    }

    static function onExit(playerId, reason) {
        BuffController.onPlayerDisconnect(playerId);
        TradeController.onBeforeDisconnectGame(playerId);

        //OnDisconnect
        players[playerId].save();

        callEvent("onPlayerUnLoad", playerId);
        players[playerId].clear();
    }

    static function onPlayerHit(playerId, killerId, dmg, type) {
        local isDmgBlocked = isDmBlockedForPlayer(killerId);
        if(isDmgBlocked || getPlayer(playerId).isGodModeEnabled()) {
            eventValue(0);
            return false;
        }

        local pObject = getPlayer(playerId);
        local kObject = getPlayer(killerId);

        if(kObject.loggIn == false || pObject.loggIn == false) {
            eventValue(0);
            return false;
        }

        if(kObject.inventory == null || pObject.inventory == null)
        {
            eventValue(0);
            return false;
        }

        if(kObject.lastHitDelay > getTickCount()) {
            eventValue(0);
            return false;
        }

        kObject.lastHitDelay = getTickCount() + 220;
        eventValue(-DMGCalculation.calculateDamage(kObject, pObject));
    }

    static function onPlayerDead(playerId, killerId) {
        BuffController.resetBuffsForPlayer(playerId);
        players[playerId].onDead(killerId);
        sendMessageToAll(255, 80, 0, ChatType.ADMIN + format("%s zabi� %s", "Gracz "+killerId, "gracza "+playerId));
    }

    static function onPlayerUseCheat(playerId, cheatTypeId) {
        kick(playerId, "Nie u�ywaj cheat�w :)");
    }

    static function onPlayerRespawn(playerId) {
        spawnPlayer(playerId);
        respawnPlayer(playerId);

        players[playerId].onRespawn();
        setPlayerHealth(playerId, 5);
        setPlayerMana(playerId, 5);
    }

    static function onAccountLoad(playerId) {
        local accountObject = getAccount(playerId);
        local playerCharacters = Player.getAccountPlayers(accountObject.rId);

        foreach(data in playerCharacters) {
            PlayerPacket(playerId).sendCharacter(data["id"],data["name"],data["bodyModel"],data["bodyTxt"],data["headModel"],data["headTxt"],data["armor"],data["melee"],
            data["ranged"],data["str"],data["dex"],data["health"],data["maxHealth"],data["mana"],data["maxMana"],data["statusId"],data["learnPoints"],
            data["minutesInGame"],data["createdAt"],data["updatedAt"])
        }
    }

    static function onPlayerChangeWeaponMode (playerid, oldWeaponMode, newWeaponMode) {
        local shieldEquiped = getPlayerShield(playerid) != -1;
        setTimer(function () {
            if(newWeaponMode == WEAPONMODE_1HS){
                local skill = getPlayerSkillWeapon(playerid,WEAPON_1H)
                if(shieldEquiped){
                    if(skill >= 60){
                        applyPlayerOverlay(playerid, Mds.id("C_SHIELD.MDS"));
                    } else if(skill >= 30){
                        applyPlayerOverlay(playerid, Mds.id("B_SHIELD.MDS"));
                    } else {
                        applyPlayerOverlay(playerid, Mds.id("A_SHIELD.MDS"));
                    }
                } else {
                    if(skill >= 60){
                        applyPlayerOverlay(playerid, Mds.id("HUMANS_1HST2.MDS"));
                    } else if(skill >= 30){
                        applyPlayerOverlay(playerid, Mds.id("HUMANS_1HST1.MDS"));
                    } else {
                        applyPlayerOverlay(playerid, Mds.id("HUMANS.MDS"));
                    }
                }
            }
            if(newWeaponMode == WEAPONMODE_2HS){
                local skill = getPlayerSkillWeapon(playerid,WEAPON_2H);
                local itemId = getPlayerMeleeWeapon(playerid);
                local instance = Items.name(itemId);
                if(instance.find("STAFF") != null || instance.find("POLE") != null){
                    if(skill >= 60){
                        applyPlayerOverlay(playerid, Mds.id("HUMANS_STAFFT2.MDS"));
                    } else if(skill >= 30){
                        applyPlayerOverlay(playerid, Mds.id("HUMANS_STAFFT1.MDS"));
                    } else {
                        applyPlayerOverlay(playerid, Mds.id("HUMANS_STAFFT0.MDS"));
                    }
                } else {
                    if(skill >= 60){
                        applyPlayerOverlay(playerid, Mds.id("HUMANS_2HST2.MDS"));
                    } else if(skill >= 30){
                        applyPlayerOverlay(playerid, Mds.id("HUMANS_2HST1.MDS"));
                    } else {
                        applyPlayerOverlay(playerid, Mds.id("HUMANS.MDS"));
                    }
                }
            }
        },400,1)
    }

    static function onPlayerEquipHelmet(playerid,itemId) {
        if(itemId > -1){
            local instance = Items.name(itemId);
            local _item = getItemScheme(instance.toupper());

            local flag = _item.flag;
            if(flag == ITEMCAT_MASK){
                setPlayerMaskedForAll(playerid,true);
            } else {
                setPlayerMaskedForAll(playerid,false);
            }
        } else {
            setPlayerMaskedForAll(playerid,false);
        }
    }

    static function register(playerId, name)
    {
        local playerObject = players[playerId];

        if(playerObject.rId != -1)
            return;

        name = mysql_escape_string(name);

        local alredyExist = Player.findByUsername(name);
        if(alredyExist != null) {
            addPlayerNotification(playerId, "Taki u�ytkownik ju� istnieje.");
            return;
        }

        playerObject.name = name;
        PlayerController.load(playerId, playerObject.register());

        callEvent("onPlayerRegister", playerId);
    }

    static function load(playerId, characterId)
    {
        local playerObject = players[playerId];

        if(playerObject.loggIn)
            return;

        playerObject.load(characterId);
        PlayerPacket(playerId).acceptCharacter();

        callEvent("onPlayerLoad", playerId);
    }

    static function onPacket(playerId, packet) {
        local typ = packet.readUInt8();
        switch(typ)
        {
            // Statistics
            case PlayerPackets.Scale:
                local playerId = packet.readInt16();
                setPlayerScale(playerId, packet.readFloat(), packet.readFloat(), packet.readFloat())
            break;
            case PlayerPackets.WalkingStyle:
                setPlayerWalkingStyle(packet.readInt16(), packet.readInt16())
            break;
            case PlayerPackets.Visual:
                setPlayerVisual(playerId, packet.readString(), packet.readInt16(), packet.readString(), packet.readInt16())
            break;
            case PlayerPackets.Fatness:
                setPlayerFatness(packet.readInt16(), packet.readInt16());
            break;
            case PlayerPackets.Upgrade:
                upgradePlayerAttribute(playerId, packet.readInt16());
            break;
            case PlayerPackets.SkillUpgrade:
                upgradePlayerSkill(playerId, packet.readInt16());
            break;
            case PlayerPackets.Fraction:
                getPlayer(playerId).sendFractionInfo(packet.readInt16());
            break;
            case PlayerPackets.FractionInfo:
                local pObj = getPlayer(playerId);
                local membersToSend = {};
                if(pObj.fractionId != -1) {
                    local fractionObject = getFraction(pObj.fractionId);
                    local rank = fractionObject.ranks[pObj.fractionRoleId];
                    if(rank.canManage) {
                        local members = Query().select(["id", "name", "fractionRoleId"]).from(Player.dbTable).where(["fractionId = "+pObj.fractionId]).all()
                        foreach(member in members)
                            membersToSend[member.id] <- {rank = member.fractionRoleId, name = member.name}

                        foreach(_player in getPlayers()) {
                            if(_player.fractionId == pObj.fractionId) {
                                if(_player.rId in membersToSend)
                                    membersToSend.rawdelete(_player.rId);

                                membersToSend[_player.rId] <- {rank = _player.fractionRoleId, name = _player.name}
                            }
                        }
                    }
                }

                PlayerPacket(playerId).getPlayerFractionInfo(pObj.fractionId, pObj.fractionRoleId, membersToSend);
            break;
            case PlayerPackets.FractionManage:
                local playerFraction = getPlayer(packet.readInt16());

                PlayerPacket(playerId).getPlayerManageFractionPlayer(playerFraction.id, playerFraction.fractionId, playerFraction.fractionRoleId)
            break;
            case PlayerPackets.FractionRemove:
                local realPlayerId = packet.readInt32();
                Query().update(Player.dbTable, ["fractionId", "fractionRoleId"], [-1, -1]).where(["id = "+realPlayerId]).execute();
                foreach(player in getPlayers()) {
                    if(player.rId != -1 && player.rId == realPlayerId) {
                        setPlayerFraction(player.id, -1, -1);
                        break;
                    }
                }
            break;
            case PlayerPackets.Lute:
                local soundId = packet.readInt16();
                local loopMode = packet.readBool();
                local volume = packet.readInt16();
                local pos = getPlayerPosition(playerId);
                for(local i = 0; i < getMaxSlots(); i ++) {
                    if(isPlayerConnected(i))
                        if(getPlayer(i).distanceToPosition(pos) < 3000)
                            PlayerPacket(i).playLute(playerId, soundId, loopMode, volume);
                }
            break;
            // Methods
            case PlayerPackets.UseItem:
                local itemId = packet.readUInt32();
                local itemObject = getItem(itemId);
                if(itemObject == null)
                    return;

                if(itemObject.equipped)
                    unequipItem(playerId, itemObject);
                else
                    equipItem(playerId, itemObject);
            break;
            case PlayerPackets.Speech:
                setPlayerSpeech(playerId, packet.readInt16());
            break;
            case PlayerPackets.ThrowItem:
                local itemId = packet.readUInt32();
                local amount = packet.readInt16();

                playerThrowItem(playerId, itemId, amount);
            break;
            case PlayerPackets.PersonalNotes:
                local tabPersonalNotes = [];
                local length = packet.readInt16();

                for(local i = 0; i < length; i ++) {
                    local str = mysql_escape_string(packet.readString());
                    if(str == null)
                        str = "";

                    tabPersonalNotes.append(str)
                }

                setPlayerPersonalNotes(playerId, tabPersonalNotes);
            break;
            case PlayerPackets.PersonalCard:
                local tabPersonalCard = [];
                local length = packet.readInt16();

                for(local i = 0; i < length; i ++) {
                    local str = mysql_escape_string(packet.readString());
                    if(str == null)
                        str = "";

                    tabPersonalCard.append(str)
                }

                setPlayerPersonalCard(playerId, tabPersonalCard);
            break;
            case PlayerPackets.RealName:
                local targetId = packet.readInt16();
                local name = packet.readString();
                if(targetId == -1)
                    return;

                name = mysql_escape_string(name);
                if(name == null)
                    name = "";

                local playerObject = getPlayer(playerId);
                local playerTargetObject = getPlayer(targetId);
                Query().deleteFrom(Player.relationsTable).where(["playerId = "+playerObject.rId+"", "relationPlayerId = "+playerTargetObject.rId+""]).execute();
                Query().insertInto(Player.relationsTable, ["playerId", "relationPlayerId", "name"], [
                    playerObject.rId, playerTargetObject.rId, "'"+name+"'"
                ]).execute();

                setPlayerRealName(playerId, targetId, name);
            break;
            case PlayerPackets.Ask:
                local askId = packet.readInt16();
                local askType = packet.readInt16();
                local askValue = packet.readString();

                if(getPlayerAskId(askId) != -1 || getPlayerAskId(playerId) != -1) {
                    PlayerPacket(playerId).rejectAsk();
                    return;
                }

                setPlayerAskId(playerId, askId);
                setPlayerAskId(askId, playerId);

                PlayerPacket(askId).sendAsk(playerId, askType, askValue);
            break;
            case PlayerPackets.StealFromUser:
                PlayerStealController.steal(playerId, packet.readInt16())
            break;
            case PlayerPackets.Shoot:
                local instance = packet.readString();
                _giveItem(playerId, Items.id(instance), 1);
                removeItem(playerId, instance, 1);
            break;
            case PlayerPackets.GetDescription:
                local focusId = packet.readInt16();
                local description = getPlayer(focusId).personalCard;

                PlayerPacket(playerId).sendPlayerDescription(focusId, description);
            break;
            case PlayerPackets.AcceptAsk:
                local askType = packet.readInt16();
                if(getPlayerAskId(playerId) == -1)
                    return;

                local askId = getPlayerAskId(playerId);

                PlayerPacket(askId).acceptAsk();
                PlayerPacket(playerId).acceptAsk();

                switch(askType) {
                    case PlayerAskType.Search:
                        PlayerPacket(askId).startSearch();

                        foreach(item in getPlayerItems(playerId))
                            PlayerPacket(askId).runSearch(item.instance, item.amount);

                        PlayerPacket(askId).endSearch();

                        local object = Config["ChatMethod"][ChatMethods.Action];
                        distanceChat(playerId, ChatType.IC, object.distance, object.r, object.g, object.b, "[PLAYER:"+askId+"] Przeszukuje [PLAYER:"+playerId+"]");
                    break;
                    case PlayerAskType.Trade:
                        TradeController.startTrade(playerId,askId);
                    break;
                    case PlayerAskType.Trap:
                        System.Bound.add(playerId);

                        local object = Config["ChatMethod"][ChatMethods.Action];
                        distanceChat(playerId, ChatType.IC, object.distance, object.r, object.g, object.b, "[PLAYER:"+askId+"] Uwi�zi� [PLAYER:"+playerId+"]");
                    break;
                    case PlayerAskType.UnTrap:
                        System.Bound.remove(playerId);

                        local object = Config["ChatMethod"][ChatMethods.Action];
                        distanceChat(playerId, ChatType.IC, object.distance, object.r, object.g, object.b, "[PLAYER:"+askId+"] Wypu�ci� [PLAYER:"+playerId+"]");
                    break;
                    case PlayerAskType.Invite:
                        setPlayerFraction(playerId, getPlayerFraction(askId), getFraction(getPlayerFraction(askId)).getFirstRank());
                        local object = Config["ChatMethod"][ChatMethods.Action];
                        distanceChat(playerId, ChatType.IC, object.distance, object.r, object.g, object.b, "[PLAYER:"+playerId+"] Do��czy� do frakcji [PLAYER:"+askId+"]");
                    break;
                    case PlayerAskType.Heal:
                        local value = -1, instance = -1;
                        foreach(item in getPlayerItems(askId)) {
                            if(item.instance in Config["PlayerHeal"]) {
                                value = Config["PlayerHeal"][item.instance];
                                instance = item.instance;
                                break;
                            }
                        }

                        if(value == -1) {
                            local object = Config["ChatMethod"][ChatMethods.Action];
                            distanceChat(playerId, ChatType.IC, object.distance, object.r, object.g, object.b, "[PLAYER:"+askId+"] Nie posiada ma�ci.");
                            return;
                        }

                        removeItem(askId, instance, 1);

                        local hp = getPlayerHealth(playerId);
                        hp = hp + value;

                        if(hp > getPlayerMaxHealth(playerId))
                            hp = getPlayerMaxHealth(playerId);

                        setPlayerHealth(playerId, hp);

                        local object = Config["ChatMethod"][ChatMethods.Action];
                        distanceChat(playerId, ChatType.IC, object.distance, object.r, object.g, object.b, "[PLAYER:"+askId+"] Uleczy� [PLAYER:"+playerId+"]");
                    break;
                }

                setPlayerAskId(askId, -1);
                setPlayerAskId(playerId, -1);
            break;
            case PlayerPackets.RejectAsk:
                if(getPlayerAskId(playerId) == -1)
                    return;

                local askId = getPlayerAskId(playerId);

                PlayerPacket(askId).rejectAsk();
                PlayerPacket(playerId).rejectAsk();

                setPlayerAskId(askId, -1);
                setPlayerAskId(playerId, -1);
            break;
            case PlayerPackets.UseSprint:
                getPlayer(playerId).useSprint();
            break;
            // Character packets
            case PlayerPackets.CharacterAdd:
                PlayerController.register(playerId, packet.readString());
            break;
            case PlayerPackets.CharacterAccept:
                PlayerController.load(playerId, packet.readInt16());
            break;
            case PlayerPackets.Recovery:
                local value = packet.readFloat();
                local stamina = getPlayerStamina(playerId);
                local hunger = getPlayerHunger(playerId);
                if(hunger > 50){
                    stamina = stamina + value;
                    if(stamina > 100)
                        stamina = 100.00;

                    setPlayerStamina(playerId, stamina);
                }
            break;
            case PlayerPackets.CharacterAccept:
                PlayerController.load(playerId, packet.readInt16());
            break;
            case PlayerPackets.SpellSummon:
                local value = packet.readInt16();
                local targetId = packet.readInt16();
                callEvent("onPlayerSpellSummon", playerId, value, targetId);
            break;
        }
    }
}

getPlayer <- @(id) PlayerController.players[id];
getPlayers <- @() PlayerController.players;

addEventHandler("onInit", PlayerController.onInit.bindenv(PlayerController));
addEventHandler("onPlayerJoin", PlayerController.onPlayerJoin.bindenv(PlayerController));
addEventHandler("onPlayerDisconnect", PlayerController.onExit.bindenv(PlayerController));
addEventHandler("onPlayerRespawn", PlayerController.onPlayerRespawn.bindenv(PlayerController));
addEventHandler("onPlayerHit", PlayerController.onPlayerHit.bindenv(PlayerController));
addEventHandler("onPlayerDead", PlayerController.onPlayerDead.bindenv(PlayerController));
addEventHandler("onAccountLoad", PlayerController.onAccountLoad.bindenv(PlayerController));
addEventHandler("onPlayerChangeWeaponMode", PlayerController.onPlayerChangeWeaponMode.bindenv(PlayerController));
addEventHandler("onPlayerEquipHelmet", PlayerController.onPlayerEquipHelmet.bindenv(PlayerController));
addEventHandler("onPlayerUseCheat", PlayerController.onPlayerUseCheat.bindenv(PlayerController));
RegisterPacketReceiver(Packets.Player, PlayerController.onPacket.bindenv(PlayerController));

setTimer(function() {
    PlayerController.onSecond();
}, 1000, 0);

setTimer(function() {
    PlayerController.onMinute();
}, 60 * 1000, 0);

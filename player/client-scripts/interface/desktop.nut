
PlayerDesktop <- {
    healthRound = Texture(anx(30), any(Resolution.y - 150), anx(128), any(128), "HK_ROUNDED.TGA"),
    healthBack = Texture(anx(35), any(Resolution.y - 145), anx(118), any(118), "HK_HEALTHBACK_ROUNDED.TGA"),
    health = Texture(anx(35), any(Resolution.y - 145), anx(118), any(118), "HK_HEALTH_ROUNDED.TGA"),

    manaRound = Texture(anx(168), any(Resolution.y - 115), anx(100), any(100), "HK_ROUNDED.TGA"),
    manaBack = Texture(anx(172), any(Resolution.y - 111), anx(92), any(92), "HK_MANABACK_ROUNDED.TGA"),
    mana = Texture(anx(172), any(Resolution.y - 111), anx(92), any(92), "HK_MANA_ROUNDED.TGA"),

    staminaRound = Texture(anx(280), any(Resolution.y - 90), anx(80), any(80), "HK_ROUNDED.TGA"),
    staminaBack = Texture(anx(283), any(Resolution.y - 87), anx(73), any(73), "HK_STAMINABACK_ROUNDED.TGA"),
    stamina = Texture(anx(283), any(Resolution.y - 87), anx(73), any(73), "HK_STAMINA_ROUNDED.TGA"),

    hungerRound = Texture(anx(370), any(Resolution.y - 90), anx(80), any(80), "HK_ROUNDED.TGA"),
    hungerBack = Texture(anx(373), any(Resolution.y - 87), anx(73), any(73), "HK_HUNGERBACK_ROUNDED.TGA"),
    hunger = Texture(anx(373), any(Resolution.y - 87), anx(73), any(73), "HK_HUNGER_ROUNDED.TGA"),

    timeDraw = Draw(anx(Resolution.x - 200), any(Resolution.y - 40), ""),
    timeDrawOOC = Draw(anx(Resolution.x - 200), any(Resolution.y - 20), ""),
    nextStaminaCheck = null,

    invisibleDraw = Draw(anx(Resolution.x - 250), any(Resolution.y - 120), "Jeste� niewidzialny"),

    open = function()
    {
        healthRound.visible = true;
        health.visible = true;
        healthBack.visible = true;

        manaRound.visible = true;
        mana.visible = true;
        manaBack.visible = true;

        staminaRound.visible = true;
        stamina.visible = true;
        staminaBack.visible = true;

        hungerRound.visible = true;
        hunger.visible = true;
        hungerBack.visible = true;

        BuffInterface.show();

        if(getSetting("OtherCustomFont")) {
            timeDraw.font = "FONT_OLD_20_WHITE_HI.TGA";
            timeDraw.setScale(0.45,0.45);
        }

        timeDraw.setColor(160, 150, 140);
        timeDraw.visible = true;
        local time = getTime();
        timeDraw.text = "("+heroId+") Czas IC "+format("%02d",time.hour)+":"+format("%02d",time.min);
        timeDraw.setPosition(8129 - timeDraw.width - 50, 7800);

        if(getSetting("OtherCustomFont")) {
            timeDrawOOC.font = "FONT_OLD_20_WHITE_HI.TGA";
            timeDrawOOC.setScale(0.45,0.45);
        }

        timeDrawOOC.setColor(160, 150, 140);
        timeDrawOOC.visible = true;
        local oocTime = date();
        timeDrawOOC.text = "Czas OOC "+format("%02d",oocTime.hour)+":"+format("%02d",oocTime.min);
        timeDrawOOC.setPosition(8129 - timeDrawOOC.width - 50, 8000);

        invisibleDraw.font = "FONT_OLD_20_WHITE_HI.TGA";
        invisibleDraw.setScale(0.4,0.4);
        invisibleDraw.setColor(255, 0, 0);
    }

    hide = function()
    {
        healthRound.visible = false;
        healthBack.visible = false;
        health.visible = false;

        manaRound.visible = false;
        mana.visible = false;
        manaBack.visible = false;

        staminaRound.visible = false;
        stamina.visible = false;
        staminaBack.visible = false;

        hungerRound.visible = false;
        hungerBack.visible = false;
        hunger.visible = false;

        timeDraw.visible = false;
        timeDrawOOC.visible = false;

        BuffInterface.hide();
    }

    onSettingsChange = function()
    {
        local opened = false;

        if(healthRound.visible)
            opened = true;

        timeDraw = Draw(anx(Resolution.x - 200), any(Resolution.y - 40), "");
        timeDrawOOC = Draw(anx(Resolution.x - 200), any(Resolution.y - 20), "");

        if(getSetting("OtherCustomFont")) {
            timeDraw.font = "FONT_OLD_20_WHITE_HI.TGA";
            timeDraw.setScale(0.45,0.45);
        }

        timeDraw.setColor(160, 150, 140);
        timeDraw.visible = true;
        local time = getTime();
        timeDraw.text = "("+heroId+") Czas IC "+format("%02d",time.hour)+":"+format("%02d",time.min);
        timeDraw.setPosition(8129 - timeDraw.width - 50, 7800);

        if(getSetting("OtherCustomFont")) {
            timeDrawOOC.font = "FONT_OLD_20_WHITE_HI.TGA";
            timeDrawOOC.setScale(0.45,0.45);
        }

        timeDrawOOC.setColor(160, 150, 140);
        timeDrawOOC.visible = true;
        local oocTime = date();
        timeDrawOOC.text = "Czas OOC "+format("%02d",oocTime.hour)+":"+format("%02d",oocTime.min);
        timeDrawOOC.setPosition(8129 - timeDrawOOC.width - 50, 8000);

        local traditionalBars = getSetting("TraditionalBars");

        if(traditionalBars) {
            healthRound = Texture(anx(30), any(Resolution.y - 190), anx(256), any(32), "BAR_BACK.TGA")
            healthBack = Texture(0,0,0,0, "HK_HEALTHBACK_ROUNDED.TGA")
            health = Texture(anx(40), any(Resolution.y - 185), anx(236), any(22), "BAR_HEALTH.TGA")

            manaRound = Texture(anx(30), any(Resolution.y - 150), anx(256), any(32), "BAR_BACK.TGA")
            manaBack = Texture(0,0,0,0, "HK_MANABACK_ROUNDED.TGA")
            mana = Texture(anx(40), any(Resolution.y - 145), anx(236), any(22), "BAR_MANA.TGA")

            staminaRound = Texture(anx(30), any(Resolution.y - 110), anx(256), any(32), "BAR_BACK.TGA")
            staminaBack = Texture(0,0,0,0, "HK_STAMINABACK_ROUNDED.TGA")
            stamina = Texture(anx(40), any(Resolution.y - 105), anx(236), any(22), "BAR_MISC.TGA")

            hungerRound = Texture(anx(30), any(Resolution.y - 70), anx(256), any(32), "BAR_BACK.TGA")
            hungerBack = Texture(0,0,0,0, "HK_STAMINABACK_ROUNDED.TGA")
            hunger = Texture(anx(40), any(Resolution.y - 65), anx(236), any(22), "BAR_MISC.TGA")
            hunger.setColor(15, 118, 15)
        }else{
            healthRound = Texture(anx(30), any(Resolution.y - 150), anx(128), any(128), "HK_ROUNDED.TGA")
            healthBack = Texture(anx(35), any(Resolution.y - 145), anx(118), any(118), "HK_HEALTHBACK_ROUNDED.TGA")
            health = Texture(anx(35), any(Resolution.y - 145), anx(118), any(118), "HK_HEALTH_ROUNDED.TGA")

            manaRound = Texture(anx(168), any(Resolution.y - 115), anx(100), any(100), "HK_ROUNDED.TGA")
            manaBack = Texture(anx(172), any(Resolution.y - 111), anx(92), any(92), "HK_MANABACK_ROUNDED.TGA")
            mana = Texture(anx(172), any(Resolution.y - 111), anx(92), any(92), "HK_MANA_ROUNDED.TGA")

            staminaRound = Texture(anx(280), any(Resolution.y - 90), anx(80), any(80), "HK_ROUNDED.TGA")
            staminaBack = Texture(anx(283), any(Resolution.y - 87), anx(73), any(73), "HK_STAMINABACK_ROUNDED.TGA")
            stamina = Texture(anx(283), any(Resolution.y - 87), anx(73), any(73), "HK_STAMINA_ROUNDED.TGA")

            hungerRound = Texture(anx(370), any(Resolution.y - 90), anx(80), any(80), "HK_ROUNDED.TGA")
            hungerBack = Texture(anx(373), any(Resolution.y - 87), anx(73), any(73), "HK_HUNGERBACK_ROUNDED.TGA")
            hunger = Texture(anx(373), any(Resolution.y - 87), anx(73), any(73), "HK_HUNGER_ROUNDED.TGA")
        }

        if(opened)
            open();
    }

    render = function()
    {
        try {
            local traditionalBars = getSetting("TraditionalBars");
            if(traditionalBars) {
                local hpPercent = abs((getPlayerHealth(heroId) * 100)/getPlayerMaxHealth(heroId));
                PlayerDesktop.health.setRect(0,0,anx((236 * hpPercent)/100),any(22));
                PlayerDesktop.health.setSize(anx((236 * hpPercent)/100),any(22));

                hpPercent = abs((getPlayerMana(heroId) * 100)/getPlayerMaxMana(heroId));
                PlayerDesktop.mana.setRect(0,0,anx((236 * hpPercent)/100),any(22));
                PlayerDesktop.mana.setSize(anx((236 * hpPercent)/100),any(22));

                hpPercent = abs((getPlayerStamina(heroId) * 100)/100.0);
                PlayerDesktop.stamina.setRect(0,0,anx((236 * hpPercent)/100),any(22));
                PlayerDesktop.stamina.setSize(anx((236 * hpPercent)/100),any(22));

                hpPercent = abs((getPlayerHunger(heroId) * 100)/100.0);
                PlayerDesktop.hunger.setRect(0,0,anx((236 * hpPercent)/100),any(22));
                PlayerDesktop.hunger.setSize(anx((236 * hpPercent)/100),any(22));
            }else {
                local hpPercent = (100 - abs((getPlayerHealth(heroId) * 100)/getPlayerMaxHealth(heroId)));

                PlayerDesktop.healthBack.setSize(anx(118),any((118 * hpPercent)/100));
                PlayerDesktop.healthBack.setUV(0.0,0.0,1.0,(1.0 * hpPercent)/100);

                hpPercent = (100 - abs((getPlayerMana(heroId) * 100)/getPlayerMaxMana(heroId)));
                PlayerDesktop.manaBack.setSize(anx(92),any((92 * hpPercent)/100));
                PlayerDesktop.manaBack.setUV(0.0,0.0,1.0,(1.0 * hpPercent)/100);

                hpPercent = (100 - abs((getPlayerStamina(heroId) * 100)/100.0));
                PlayerDesktop.staminaBack.setSize(anx(73),any((73 * hpPercent)/100));
                PlayerDesktop.staminaBack.setUV(0.0,0.0,1.0,(1.0 * hpPercent)/100);

                hpPercent = (100 - abs((getPlayerHunger(heroId) * 100)/100.0));
                PlayerDesktop.hungerBack.setSize(anx(73),any((73 * hpPercent)/100));
                PlayerDesktop.hungerBack.setUV(0.0,0.0,1.0,(1.0 * hpPercent)/100);
            }
        }catch(error) {}

        local time = getTime();
        timeDraw.text = "("+heroId+") Czas IC "+format("%02d",time.hour)+":"+format("%02d",time.min);

        local oocTime = date();
        timeDrawOOC.text = "Czas OOC "+format("%02d",oocTime.hour)+":"+format("%02d",oocTime.min);

        invisibleDraw.visible = getPlayerInvisible(heroId);

        if(nextStaminaCheck != null) {
            if(nextStaminaCheck == true) {
                if(isKeyPressed(getAccountKeyBind("SPRINT")))
                    return;

                PlayerPacket().useSprint();
                nextStaminaCheck = null;
            }else if(nextStaminaCheck < getTickCount()) {
                if(isKeyPressed(getAccountKeyBind("SPRINT")) == false)
                    return;

                PlayerPacket().useSprint();
                nextStaminaCheck = true;
            }
        }
    }

    onKey = function(key) {
        if(healthRound.visible == false)
            return;

        if(key == getAccountKeyBind("SPRINT"))
            if(nextStaminaCheck == null)
                nextStaminaCheck = getTickCount() + 500;
    }
}

addEventHandler("onOpenGUI", function() {
    PlayerDesktop.hide();
})

addEventHandler("onCloseGUI", function() {
    PlayerDesktop.open();
})

addEventHandler("onRender", function() {
    PlayerDesktop.render();
})

addEventHandler("onKey", PlayerDesktop.onKey.bindenv(PlayerDesktop));
addEventHandler("onSettingsChange", PlayerDesktop.onSettingsChange.bindenv(PlayerDesktop));

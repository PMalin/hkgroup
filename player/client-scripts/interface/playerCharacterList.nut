
local characters = [], activeCharacterId = -1;

local textureCharacterInfo = GUI.Texture(anx(Resolution.x/2 - 350), any(Resolution.y/2 - 240), anx(700), any(160), "DLG_CONVERSATION.TGA");
local drawLogoCharacterInfo = GUI.Texture(anx(Resolution.x/2 - 315), any(100), anx(310), any(155), "HK_LOGO.TGA");
local drawCharacterInfo = GUI.Draw(anx(42), any(Resolution.y/2 - 200), Config["NoCharactersInfo"]);
drawCharacterInfo.center(anx(Resolution.x/2 - 330), any(Resolution.y/2 - 220), anx(660), any(140));

local characterListWindow = GUI.Window(anx(0), any(0), anx(410), any(Resolution.y), "DLG_CONVERSATION.TGA", null, false);

local createCharacterGameButton = GUI.Button(anx(55), any(Resolution.y - 150), anx(300), any(60), "HK_BUTTON.TGA", "Utw�rz posta�");
local exitGameButton = GUI.Button(anx(55), any(Resolution.y - 80), anx(300), any(60), "HK_BUTTON.TGA", "Wyjd� z gry");

local scrollBarCharacterList = GUI.ScrollBar(anx(360), any(50), anx(30), any(460), "HK_SCROLL.TGA", "HK_INDICATOR.TGA", "HK_U.TGA", "HK_O.TGA", Orientation.Vertical, characterListWindow);
local listOfCharacterList = [];

local windowOperationDraw = Draw(anx(30), any(Resolution.y/2 - 140), "");
local playCharacterButton = GUI.Button(anx(55), any(Resolution.y - 220), anx(300), any(60), "HK_BUTTON.TGA", "Graj", characterListWindow);

//HK_VERTICAL_BACKGROUND.TGA

exitGameButton.bind(EventType.Click, function(element) {
    exitGame();
});

playCharacterButton.bind(EventType.Click, function(element) {
    if(Loading.active)
        false;

    Loading.show();
    PlayerPacket().playCharacter(activeCharacterId);
});

createCharacterGameButton.bind(EventType.Click, function(element) {
    hideCharacterList();
    CharacterAdd.start();
});

function showCharacterList()
{
    if(ActiveGui == PlayerGUI.PasswordReroll)
        return;

    BaseGUI.show();
    ActiveGui = PlayerGUI.CharacterList;

    setPlayerPosition(heroId, 7569.92, 5625.78, 37102.4);
    setPlayerAngle(heroId, 59);

    Camera.setPosition(7850.62, 5695.86, 37282.7);
    Camera.setRotation(10, 234, 0);

    drawLogoCharacterInfo.setVisible(true);

    updateCharacterList();
    exitGameButton.setVisible(true);
    createCharacterGameButton.setVisible(true);
    windowOperationDraw.visible = false;
}

function hideCharacterList()
{
    BaseGUI.hide();
    ActiveGui = null;

    foreach(butt in listOfCharacterList) {
        butt.setVisible(false);
        butt.destroy();
    }

    listOfCharacterList.clear();

    textureCharacterInfo.setVisible(false);
    drawLogoCharacterInfo.setVisible(false);
    drawCharacterInfo.setVisible(false);
    characterListWindow.setVisible(false);
    windowOperationDraw.visible = false;

    exitGameButton.setVisible(false);
    createCharacterGameButton.setVisible(false);
}

function selectCharacterFromListToPreview(name)
{
    if(ActiveGui != PlayerGUI.CharacterList)
        return;

    local object = null;

    foreach(data in characters)
        if(data.name == name)
            object = data;

    activeCharacterId = object.rid;

    setPlayerVisual(heroId, object.bodyModel, object.bodyTxt, object.headModel, object.headTxt);
    equipArmor(heroId, Items.id(object.armor));
    equipMeleeWeapon(heroId, Items.id(object.melee));
    equipRangedWeapon(heroId, Items.id(object.ranged));

    local text = "";
    text += object.name+"\n";
    text += "Id: "+object.rid +"\n";
    text += "Si�a: "+object.str +"\n";
    text += "Zr�czno��: "+object.dex +"\n";
    text += "Wytrzyma�o��: "+object.health+"/"+object.maxHealth +"\n";
    text += "Mana: "+object.mana+"/"+object.maxMana +"\n";
    text += "Status postaci: "+Config["PlayerStatus"][object.statusId] +"\n";
    text += "Punkt�w nauki: "+object.learnPoints +"\n";
    text += "Minuty w grze: "+object.minutesInGame +"\n";
    text += "Stworzono: "+object.createdAt +"\n";
    text += "Aktualizowano: "+object.updatedAt +"\n";

    windowOperationDraw.text = text;
    windowOperationDraw.font = "FONT_OLD_20_WHITE_HI.TGA";
    windowOperationDraw.setColor(210, 190, 176);
    windowOperationDraw.setScale(0.53, 0.47);
    windowOperationDraw.visible = true;
    windowOperationDraw.setPosition(anx(45), any(Resolution.y - (230 + windowOperationDraw.heightPx)));
}

function updateCharacterList()
{
    if(ActiveGui != PlayerGUI.CharacterList)
        return;

    if(characters.len() == 0) {
        createCharacterGameButton.setPosition(anx(Resolution.x/2 - 150), any(Resolution.y/2 - 40));
        drawLogoCharacterInfo.setPosition(anx(Resolution.x/2 - 155), any(100));
        textureCharacterInfo.setVisible(true);
        drawCharacterInfo.setVisible(true);
    }else {
        textureCharacterInfo.setVisible(false);
        drawCharacterInfo.setVisible(false);
        createCharacterGameButton.setPosition(anx(55), any(Resolution.y - 150));
    }

    if(characters.len() == 0)
        return;

    drawLogoCharacterInfo.setPosition(anx(Resolution.x/2 - 155), any(100));
    characterListWindow.setVisible(true);
    exitGameButton.setVisible(true);
    createCharacterGameButton.setVisible(true);

    updateCharacterListButtons(0);

    if(characters.len() > 5)
        scrollBarCharacterList.setVisible(true);
    else
        scrollBarCharacterList.setVisible(false);
}

function updateCharacterListButtons(value)
{
    foreach(butt in listOfCharacterList) {
        butt.setVisible(false);
        butt.destroy();
    }

    listOfCharacterList.clear();

    foreach(index, data in characters)
    {
        if(index >= value && index < (value + 5)) {
            listOfCharacterList.append(GUI.Button(anx(55), any(35) + any(65 * (index - value)), anx(300), any(50), "HK_BUTTON.TGA", data.name));
            listOfCharacterList[listOfCharacterList.len() - 1].bind(EventType.Click, function(element) {
                selectCharacterFromListToPreview(element.getText());
            });
            listOfCharacterList[listOfCharacterList.len() - 1].setVisible(true);
        }
    }

}

function addToCharacterList(data) {
    characters.append(data);

    if(activeCharacterId == -1)
        selectCharacterFromListToPreview(data.name);

    updateCharacterList();
}

function clearCharacters() {
    characters.clear();
}

addEventHandler("onPlayerLoad", function() { if( ActiveGui == PlayerGUI.CharacterList) hideCharacterList(); })

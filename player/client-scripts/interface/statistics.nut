enum PlayerStatisticsBookmark
{
    Main,
    Stats,
    Skills,
    Fractions,
    Note,
}

PlayerStatistics <- {
    fractionObj = {fractionId = -1, rankId = -1}
    showSkills = false,
    activeBookmark = -1,

    gui = [
        Texture(anx(Resolution.x/2 - 600), any(Resolution.y/2 - 300), anx(600), any(600), "HK_BOOK_LEFT.TGA"),
        Texture(anx(Resolution.x/2 - 450), any(Resolution.y/2 - 300), anx(500), any(550), "HK_STATISTICS_BACKMAIN.TGA"),
        Texture(anx(Resolution.x/2), any(Resolution.y/2 - 300), anx(600), any(600), "HK_BOOK_RIGHT.TGA"),
    ],
    updateUi = {
        background = Texture(anx(Resolution.x/2 - 250), any(Resolution.y/2 - 250), anx(500), any(300), "HK_BOX.TGA"),
        title = Draw(anx(Resolution.x/2 - 230), any(Resolution.y/2 - 220), ""),
        description = Draw(anx(Resolution.x/2 - 230), any(Resolution.y/2 - 170), ""),
        doButton = GUI.Button(anx(Resolution.x/2 - 230), any(Resolution.y/2 - 40), anx(210), any(60), "HK_BUTTON.TGA", "Ulepsz"),
        exitButton = GUI.Button(anx(Resolution.x/2 + 20), any(Resolution.y/2 - 40), anx(210), any(60), "HK_BUTTON.TGA", "Wyjd�"),
    },
    bookmarks = [
        GUI.Button(anx(Resolution.x/2 + 150), any(Resolution.y/2 + 262), anx(44), any(88), "HK_STATISTICS_MAIN.TGA", ""),
        GUI.Button(anx(Resolution.x/2 + 205), any(Resolution.y/2 + 265), anx(44), any(88), "HK_STATISTICS_STATS.TGA", ""),
        GUI.Button(anx(Resolution.x/2 + 260), any(Resolution.y/2 + 268), anx(44), any(88), "HK_STATISTICS_SKILLS.TGA", ""),
        GUI.Button(anx(Resolution.x/2 + 315), any(Resolution.y/2 + 271), anx(44), any(88), "HK_STATISTICS_FRACTIONS.TGA", ""),
        GUI.Button(anx(Resolution.x/2 + 370), any(Resolution.y/2 + 274), anx(44), any(88), "HK_STATISTICS_NOTES.TGA", ""),
    ],
    external = [],

    init = function()
    {
        updateUi.doButton.bind(EventType.Click, function(element) {
            PlayerStatistics.hideUpdate();
            if(PlayerStatistics.showSkills == false)
                PlayerPacket().upgradeAttribute(element.attribute);
            else
                PlayerPacket().upgradeSkill(element.attribute);

            setTimer(function() {
                PlayerStatistics.changeBookmark(PlayerStatistics.activeBookmark);
            }, 500, 1);
        });
        updateUi.exitButton.bind(EventType.Click, function(element) {
            PlayerStatistics.hideUpdate();
        });
    }

    show = function()
    {
        PlayerPacket().getPlayerFractionInfo();

        foreach(guiItem in gui)
            guiItem.visible = true;

        foreach(index, bookmark in bookmarks) {
            bookmark.setVisible(true);
            bookmark.attribute = index;
            bookmark.bind(EventType.Click, function(element) {
                PlayerStatistics.changeBookmark(element.attribute);
            });
        }

        changeBookmark(PlayerStatisticsBookmark.Main);

        BaseGUI.show();
        ActiveGui = PlayerGUI.Statistics;
    }

    hide = function()
    {
        foreach(guiItem in gui)
            guiItem.visible = false;

        foreach(bookmark in bookmarks)
            bookmark.setVisible(false);

        BaseGUI.hide();
        ActiveGui = null;
        PlayerStatistics.hideUpdate();

        foreach(extern in external) {
            extern.setVisible(false);
            extern.destroy();
        }

        external.clear();
    }

    onForceCloseGUI = function()
    {
        if(ActiveGui == PlayerGUI.Statistics)
            hide();
    }

    changeBookmark = function(value)
    {
        activeBookmark = value;

        foreach(extern in external) {
            extern.setVisible(false);
            extern.destroy();
        }

        PlayerStatistics.hideUpdate();

        external.clear();

        switch(value)
        {
            case PlayerStatisticsBookmark.Main:
                mainBookmark();
            break;
            case PlayerStatisticsBookmark.Stats:
                statsBookmark();
            break;
            case PlayerStatisticsBookmark.Skills:
                skillsBookmark();
            break;
            case PlayerStatisticsBookmark.Fractions:
                fractionsBookmark();
            break;
            case PlayerStatisticsBookmark.Note:
                noteBookmark();
            break;
        }
    }

    mainBookmark = function()
    {
        gui[1].file = "HK_STATISTICS_BACKMAIN.TGA";
        external = [
            GUI.Draw(anx(0), any(0), Hero.realNames[heroId]),
            MyGUI.MultiLineInput(anx(Resolution.x/2 + 20), any(Resolution.y/2 - 200), anx(360), 16),
            GUI.Button(anx(Resolution.x/2 + 110), any(Resolution.y/2 + 190), anx(200), any(60), "HK_BUTTON.TGA", "Zaaktualizuj")
            GUI.Draw(anx(0), any(0), "Karta postaci"),
        ];

        external[0].setFont("FONT_OLD_20_WHITE_HI.TGA");
        external[3].setFont("FONT_OLD_20_WHITE_HI.TGA");
        external[0].setColor(110, 90, 76);
        external[1].setColor(235, 225, 197);
        external[3].setColor(110, 90, 76);
        external[3].setScale(0.7, 0.7);
        external[0].setPosition(anx(Resolution.x/2 + 200) - external[0].getSize().width/2, any(Resolution.y/2 - 280))
        external[3].setPosition(anx(Resolution.x/2 + 200) - (external[3].getSize().width * 0.7)/2, any(Resolution.y/2 - 240))
        external[1].setValue(Hero.personalCard);
        external[1].active = true;

        external[2].bind(EventType.Click, function(element) {
            PlayerPacket().setPersonalCard(PlayerStatistics.external[1].getValue());
            addPlayerNotification(heroId, "Zaaktualizowano kart� postaci.")
        });

        foreach(guiItem in external)
            guiItem.setVisible(true);
    }

    showUpdater = function(attributeId)
    {
        showSkills = false;
        updateUi.title.font = "FONT_OLD_20_WHITE_HI.TGA";
        updateUi.title.setScale(0.7, 0.7);
        updateUi.title.text = "Aktualizacja " + Config["PlayerAttributes"][attributeId];

        local upgradeObj = getPlayerAttributeUpgradeObject(attributeId);
        if(upgradeObj == null)
            updateUi.description.text = "Ulepszenie nie jest mo�liwe.";
        else
            updateUi.description.text = "Twoje punkty nauki "+getLearnPoints()+" (-" +upgradeObj.cost+ ")\nStatystyka "+upgradeObj.afterValue+" (+" +upgradeObj.value+ ")";

        updateUi.description.font = "FONT_OLD_20_WHITE_HI.TGA";
        updateUi.description.setScale(0.5, 0.5);

        updateUi.background.visible = true;
        updateUi.title.visible = true;
        updateUi.description.visible = true;
        updateUi.exitButton.setVisible(true);
        updateUi.doButton.setVisible(true);
        updateUi.doButton.attribute = attributeId;
    }

    showUpdaterSkill = function(skillId)
    {
        showSkills = true;
        updateUi.title.font = "FONT_OLD_20_WHITE_HI.TGA";
        updateUi.title.setScale(0.7, 0.7);
        updateUi.title.text = "Umiej�tno�� " + Config["PlayerSkill"][skillId];

        local upgradeObj = getPlayerSkillUpgradeObject(skillId);
        if(upgradeObj == null)
            updateUi.description.text = "Ulepszenie nie jest mo�liwe.";
        else
            updateUi.description.text = "Twoje punkty nauki "+getLearnPoints()+" (-" +upgradeObj.cost+ ")\nUmiej�tno�� "+upgradeObj.afterValue+" (+" +upgradeObj.value+ ")";

        updateUi.description.font = "FONT_OLD_20_WHITE_HI.TGA";
        updateUi.description.setScale(0.5, 0.5);

        updateUi.background.visible = true;
        updateUi.title.visible = true;
        updateUi.description.visible = true;
        updateUi.exitButton.setVisible(true);
        updateUi.doButton.setVisible(true);
        updateUi.doButton.attribute = skillId;
    }

    hideUpdate = function()
    {
        updateUi.background.visible = false;
        updateUi.title.visible = false;
        updateUi.description.visible = false;
        updateUi.exitButton.setVisible(false);
        updateUi.doButton.setVisible(false);
    }

    statsBookmark = function()
    {
        gui[1].file = "HK_STATISTICS_BACKSTATS.TGA";
        external = [
            GUI.Draw(anx(0), any(0), Hero.realNames[heroId]),
            GUI.Draw(anx(0), any(0), "Statystyki"),
            GUI.Draw(anx(Resolution.x/2 + 30), any(Resolution.y/2 - 180), "Si�a: "+getPlayerStrength(heroId)),
            GUI.Draw(anx(Resolution.x/2 + 30), any(Resolution.y/2 - 130), "Zr�czno��: "+getPlayerDexterity(heroId)),
            GUI.Draw(anx(Resolution.x/2 + 30), any(Resolution.y/2 + 80), "Witalno��: "+getPlayerHealth(heroId)+"/"+getPlayerMaxHealth(heroId)),
            GUI.Draw(anx(Resolution.x/2 + 230), any(Resolution.y/2 - 180), "Kr�g: "+getPlayerMagicLevel(heroId)),
            GUI.Draw(anx(Resolution.x/2 + 230), any(Resolution.y/2 - 130), "Inteligencja: "+getPlayerInteligence(heroId)),
            GUI.Draw(anx(Resolution.x/2 + 30), any(Resolution.y/2 + 130), "Mana: "+getPlayerMana(heroId)+"/"+getPlayerMaxMana(heroId)),
            GUI.Draw(anx(Resolution.x/2 + 30), any(Resolution.y/2 - 80), "1H: "+getPlayerSkillWeapon(heroId, 0) + "/100"),
            GUI.Draw(anx(Resolution.x/2 + 30), any(Resolution.y/2 - 30), "2H: "+getPlayerSkillWeapon(heroId, 1) + "/100"),
            GUI.Draw(anx(Resolution.x/2 + 230), any(Resolution.y/2 - 80), "�uk: "+getPlayerSkillWeapon(heroId, 2) + "/100"),
            GUI.Draw(anx(Resolution.x/2 + 230), any(Resolution.y/2 - 30), "Kusza: "+getPlayerSkillWeapon(heroId, 3) + "/100"),
            GUI.Draw(anx(Resolution.x/2 + 445), any(Resolution.y/2 - 15), "LP:"+getLearnPoints()),
        ]

        external[2].attribute = PlayerAttributes.Str;
        external[3].attribute = PlayerAttributes.Dex;
        external[4].attribute = PlayerAttributes.Hp;
        external[5].attribute = PlayerAttributes.MagicLvl;
        external[6].attribute = PlayerAttributes.Int;
        external[7].attribute = PlayerAttributes.Mana;
        external[8].attribute = PlayerAttributes.OneH;
        external[9].attribute = PlayerAttributes.TwoH;
        external[10].attribute = PlayerAttributes.Bow;
        external[11].attribute = PlayerAttributes.Cbow;

        foreach(index, guiItem in external) {
            guiItem.setFont("FONT_OLD_20_WHITE_HI.TGA");
            guiItem.setColor(110, 90, 76);
            guiItem.setScale(0.5, 0.5);

            if(index > 1)
            {
                external[index].bind(EventType.Click, function(element) {
                    PlayerStatistics.showUpdater(element.attribute)
                });
            }
        }

        external[0].setScale(0.7, 0.7);
        external[12].setScale(0.7, 0.7);
        external[12].setColor(210, 200, 190);
        external[0].setPosition(anx(Resolution.x/2 + 200) - (external[0].getSize().width * 0.7)/2, any(Resolution.y/2 - 280))
        external[1].setPosition(anx(Resolution.x/2 + 200) - (external[1].getSize().width * 0.5)/2, any(Resolution.y/2 - 240))

        foreach(guiItem in external)
            guiItem.setVisible(true);
    }

    skillsBookmark = function()
    {
        gui[1].file = "HK_STATISTICS_BACKSKILLS.TGA";
        external = [
            GUI.Draw(anx(0), any(0), Hero.realNames[heroId]),
            GUI.Draw(anx(0), any(0), "Umiej�tno�ci"),
            GUI.Draw(anx(Resolution.x/2 + 445), any(Resolution.y/2 - 15), "LP:"+getLearnPoints()),
        ]

        foreach(skillId, skillName in Config["PlayerSkill"])
        {
            external.append(GUI.Draw(anx(Resolution.x/2 + 30 + 200 * (skillId % 2)), any(Resolution.y/2 - 180 + 50 * abs((skillId)/2)), skillName + ": "+Hero.skills[skillId]));
            external[external.len() - 1].setScale(0.5, 0.5);
            external[external.len() - 1].setFont("FONT_OLD_20_WHITE_HI.TGA");
            external[external.len() - 1].attribute = skillId;
            external[external.len() - 1].bind(EventType.Click, function(element) {
                PlayerStatistics.showUpdaterSkill(element.attribute)
            });
        }
        external[0].setFont("FONT_OLD_20_WHITE_HI.TGA");
        external[2].setFont("FONT_OLD_20_WHITE_HI.TGA");
        external[1].setFont("FONT_OLD_20_WHITE_HI.TGA");

        external[0].setScale(0.7, 0.7);
        external[2].setScale(0.7, 0.7);
        external[1].setScale(0.5, 0.5);

        external[0].setPosition(anx(Resolution.x/2 + 200) - (external[0].getSize().width * 0.7)/2, any(Resolution.y/2 - 280))
        external[1].setPosition(anx(Resolution.x/2 + 200) - (external[1].getSize().width * 0.5)/2, any(Resolution.y/2 - 240))


        foreach(guiItem in external) {
            guiItem.setVisible(true);
            guiItem.setColor(110, 90, 76);
        }

        external[2].setColor(210, 200, 190);
    }

    fractionsBookmark = function()
    {
        gui[1].file = "HK_STATISTICS_BACKFRACTIONS.TGA";
        external = [
            GUI.Draw(anx(0), any(0), Hero.realNames[heroId]),
            GUI.Draw(anx(0), any(0), "Frakcja"),
        ]

        external[0].setFont("FONT_OLD_20_WHITE_HI.TGA");
        external[1].setFont("FONT_OLD_20_WHITE_HI.TGA");

        external[0].setScale(0.7, 0.7);
        external[1].setScale(0.5, 0.5);

        local fractionLabel = "Brak", roleLabel = "Brak";
        if(fractionObj.fractionId in FractionController.fractions)
            fractionLabel = getFraction(fractionObj.fractionId).name;

        if(fractionLabel != "Brak")
            roleLabel = getFraction(fractionObj.fractionId).ranks[fractionObj.rankId].name;

        external.push(GUI.Draw(anx(Resolution.x/2 + 30), any(Resolution.y/2 - 190), "Twoja frakcja: "+fractionLabel));
        external.push(GUI.Draw(anx(Resolution.x/2 + 30), any(Resolution.y/2 - 160), "Twoja rola w frakcji: "+roleLabel));

        if(fractionObj.fractionId in FractionController.fractions) {
            local fractionObject = getFraction(fractionObj.fractionId);
            local rank = fractionObject.ranks[fractionObj.rankId];

            if(rank.canTrade)
                external.push(GUI.Draw(anx(Resolution.x/2 + 30), any(Resolution.y/2 - 100), "Mo�esz handlowa� na \nterytorium frakcji."));

            if(rank.canManage) {
                external.push(GUI.Draw(anx(Resolution.x/2 + 30), any(Resolution.y/2 - 60), "Mo�esz zarz�dza� \nfrakcj�."));
                external.push(GUI.Draw(anx(Resolution.x/2 - 380), any(Resolution.y/2 - 280), "Kliknij na u�ytkownika, \naby usun��."));
                external.push(MyGUI.SelectList(anx(Resolution.x/2 - 380), any(Resolution.y/2 - 230), anx(350), any(500), false));

                external[external.len() - 1].onClick = function(id) {
                    PlayerPacket().removePlayerFraction(id);
                    PlayerStatistics.fractionObj.members.rawdelete(id);
                    foreach(ext in PlayerStatistics.external) {
                        if(ext instanceof MyGUI.SelectList) {
                            ext.removeOption(id);
                            break;
                        }
                    }
                }

                foreach(memberId, member in fractionObj.members) {
                    external[external.len() - 1].addOption(memberId, member.name + " - " + fractionObject.ranks[member.rank].name);
                }
            }

            if(rank.canInvite)
                external.push(GUI.Draw(anx(Resolution.x/2 + 30), any(Resolution.y/2 - 20), "Mo�esz dodawa� osoby \ndo frakcji."));
        }

        external[2].setFont("FONT_OLD_20_WHITE_HI.TGA");
        external[3].setFont("FONT_OLD_20_WHITE_HI.TGA");
        external[2].setScale(0.5, 0.5);
        external[3].setScale(0.5, 0.5);

        external[0].setPosition(anx(Resolution.x/2 + 200) - (external[0].getSize().width * 0.7)/2, any(Resolution.y/2 - 280))
        external[1].setPosition(anx(Resolution.x/2 + 200) - (external[1].getSize().width * 0.5)/2, any(Resolution.y/2 - 240))

        foreach(guiItem in external) {
            guiItem.setVisible(true);
            if(guiItem instanceof GUI.Draw)
                guiItem.setColor(110, 90, 76);
        }
    }

    noteBookmark = function()
    {
        gui[1].file = "HK_STATISTICS_BACKNOTES.TGA";
        external = [
            GUI.Draw(anx(0), any(0), Hero.realNames[heroId]),
            MyGUI.MultiLineInput(anx(Resolution.x/2 + 20), any(Resolution.y/2 - 200), anx(360), 16),
            GUI.Button(anx(Resolution.x/2 + 110), any(Resolution.y/2 + 190), anx(200), any(60), "HK_BUTTON.TGA", "Zaaktualizuj")
            GUI.Draw(anx(0), any(0), "Notatki"),
        ];

        external[0].setFont("FONT_OLD_20_WHITE_HI.TGA");
        external[3].setFont("FONT_OLD_20_WHITE_HI.TGA");
        external[0].setColor(110, 90, 76);
        external[1].setColor(235, 225, 197);
        external[3].setColor(110, 90, 76);
        external[3].setScale(0.7, 0.7);
        external[0].setPosition(anx(Resolution.x/2 + 200) - external[0].getSize().width/2, any(Resolution.y/2 - 280))
        external[3].setPosition(anx(Resolution.x/2 + 200) - (external[3].getSize().width * 0.7)/2, any(Resolution.y/2 - 240))
        external[1].setValue(Hero.personalNotes);
        external[1].active = true;

        external[2].bind(EventType.Click, function(element) {
            PlayerPacket().setPersonalNotes(PlayerStatistics.external[1].getValue());
            addPlayerNotification(heroId, "Zaaktualizowano notatki.")
        });

        foreach(guiItem in external)
            guiItem.setVisible(true);
    }
}

addEventHandler("onInit", function() { PlayerStatistics.init(); });
addEventHandler("onForceCloseGUI", PlayerStatistics.onForceCloseGUI.bindenv(PlayerStatistics));
Bind.addKey("STATISTICS", function () {PlayerStatistics.show()})
Bind.addKey(KEY_ESCAPE, function () {PlayerStatistics.hide()}, PlayerGUI.Statistics)


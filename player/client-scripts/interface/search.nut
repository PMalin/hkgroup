
InventorySearch <- {
    bucket = null
    items = []

    prepareWindow = function() {
        local result = {};

        result.window <- Equipment("Ekwipunek");

        result.window.left = Resolution.x/2 - 256;
        result.window.top = Resolution.y/2 - 256;

        return result;
    }

    start = function() {
        if(ActiveGui != null)
            return;

        items.clear();

        if(bucket == null)
            bucket = prepareWindow();

        bucket.window.setVisible(true);
        BaseGUI.show();
        ActiveGui = PlayerGUI.Search;
    }

    end = function() {
        bucket.window.setBucket(items);
        bucket.window.showItemsWithIndex();
    }

    hide = function() {
        bucket.window.setVisible(false);

        BaseGUI.hide();
        ActiveGui = null;
    }

    run = function(instance, amount) {
        items.push({
            instance = instance,
            amount = amount
        });
    }

    onForceCloseGUI = function() {
        if(ActiveGui == PlayerGUI.Search)
            hide();
    }
}

Bind.addKey(KEY_ESCAPE, InventorySearch.hide.bindenv(InventorySearch), PlayerGUI.Search)
addEventHandler("onForceCloseGUI", InventorySearch.onForceCloseGUI.bindenv(InventorySearch));

Calendar <- {
    previewId = -1,
    bucket = null,

    prepareWindow = function()
    {
        local result = {};

        result.window <- GUI.Window(anx(Resolution.x/2 - 400), any(Resolution.y/2 - 350), anx(800), any(700), "HK_BOX.TGA", null, false);
        result.draw <- GUI.Button(anx(100), any(20), anx(600), any(50), "DLG_CONVERSATION.TGA", "", result.window);
        result.texture <- Texture(anx(Resolution.x/2 - 400), any(Resolution.y/2 - 350), anx(800), any(700), "ADANOS_CALENDAR.TGA");
        result.buttons <- [];

        return result;
    }

    show = function() {
        if(bucket == null)
            bucket = prepareWindow();

        bucket.texture.visible = true;
        bucket.window.setVisible(true);
        refresh();
        BaseGUI.show();
        ActiveGui = PlayerGUI.Calendar;
        playAni(heroId, "T_MAP_STAND_2_S0");
    }

    hide = function() {
        bucket.window.setVisible(false);
        bucket.texture.visible = false;
        foreach(butt in bucket.buttons) {
            butt.setVisible(false);
            butt.destroy();
        }
        bucket.buttons.clear();
        BaseGUI.hide();
        ActiveGui = null;

        playAni(heroId,"S_RUN");
    }

    refresh = function() {
        local cache = System.TimeGame.cache;
        foreach(butt in bucket.buttons) {
            butt.setVisible(false);
            butt.destroy();
        }

        switch(cache.season) {
            case "�niwa Innosa":
                bucket.texture.file = "INNOS_CALENDAR.TGA";
            break;
            case "Ciemno�� Beliara":
                bucket.texture.file = "BELIAR_CALENDAR.TGA";
            break;
            case "R�wnowaga Adannosa":
                bucket.texture.file = "ADANOS_CALENDAR.TGA";
            break;
        }

        local days = ["Niedziela", "Poniedzia�ek", "Wtorek", "�roda", "Czwartek", "Pi�tek", "Sobota", "Niedziela"];
        bucket.buttons.clear();
        bucket.draw.setText(days[cache.dayOfWeek] + " " + cache.dayOfMonth + ".0"+cache.month+" "+cache.season);

        local startXRay = cache.firstDayOfMonth % 7;
        local startPos = { x = anx(Resolution.x/2 - 340), y = any(Resolution.y/2 - 250)}
        for(local i = 0; i < cache.daysInMonth; i ++) {
            local label = i < 9 ? "0"+(i+1) : ""+(i+1);
            bucket.buttons.push(GUI.Button(startPos.x + anx(startXRay * 100), startPos.y, anx(80), any(90), "HK_BUTTON.TGA", label));
            startXRay = startXRay + 1;
            if(startXRay >= 7) {
                startXRay = 0;
                startPos.y = startPos.y + any(110);
            }

            if((i+1) == cache.dayOfMonth)
                bucket.buttons[bucket.buttons.len() -1].setColor(250, 120, 0);
        }

        foreach(butt in bucket.buttons)
            butt.setVisible(true);
    }
}

Bind.addKey("CALENDAR", Calendar.show.bindenv(Calendar))
Bind.addKey("CALENDAR", Calendar.hide.bindenv(Calendar), PlayerGUI.Calendar)
Bind.addKey(KEY_ESCAPE, Calendar.hide.bindenv(Calendar), PlayerGUI.Calendar)
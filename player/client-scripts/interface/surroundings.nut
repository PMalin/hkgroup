
Surroundings <- {};

/**
Bucket with array of every existing 3DDraws
array[Draw3d]
*/
Surroundings.bucket <- [];

/**
Bucket with array of every health target texture
array[Texture]
*/
Surroundings.hpTextures <- [];

/**
Returning element labels for surrounding players/npcs

@param elementId - int
@return array[Surroundings.label]
*/
Surroundings.getLabels <- function(elementId) {
    if(elementId < getMaxSlots())
        return playerLabels(elementId);
    else if(elementId in System.Mount.getNpcs())
        return [];
    else
        return botLabels(getBotElements()[elementId])
}

/**
Returning bot labels depending on instance of bot

@param botId - int
@return array[Surroundings.label]
*/
Surroundings.botLabels <- function (botId) {
    local labels = [];
    local botObj = getBots()[botId];

    labels.append(label(botObj.name));

    if(adminPlayer.role > PlayerRole.Narrator)
        labels.append(label("Bot "+botId));

    return labels;
}

/**
Returning player labels depending on real name

@param playerId - int
@return array[Surroundings.label]
*/
Surroundings.playerLabels <- function (playerId) {
    local labels = [];
    if(Hero.realNames[playerId] == "" || getPlayer(playerId).masked) {
        local color = getPlayerColor(playerId);
        if(color.r == 240 && color.g == 198 && color.b == 25)
            labels.append(label(getPlayerRealName(playerId), 240, 198, 25));
        else
            labels.append(label(getPlayerRealName(playerId), 150, 150, 150));
    } else {
        local color = getPlayerColor(playerId);
        if(color.r == 240 && color.g == 198 && color.b == 25)
            labels.append(label(getPlayerRealName(playerId) + " "+playerId, 240, 198, 25));
        else
            labels.append(label(getPlayerRealName(playerId) + " "+playerId, 235, 212, 178));
    }

    if(adminPlayer.role > PlayerRole.Narrator) {
        local adminAcc = getPlayerAdminStatistics(playerId);
        labels.append(label(adminAcc.characterName + " ("+adminAcc.account+")", 200, 50, 50));
    }

    return labels;
}

/**
Render every player nickanem near hero
Render also hp of currently targeted player
*/
Surroundings.onRender <- function () {
    bucket.clear();
    hpTextures.clear();

    if(ActiveGui != null)
        return;

    local heroPos = getPlayerPosition(heroId);
    local playerFocus = getPlayerFocus();

    if(playerFocus != -1)
    {
        local position = getPlayerPosition(playerFocus);
        local posDifference = getPositionDifference(heroPos, position);
        if(posDifference < 1400)
        {
            local labels = getLabels(playerFocus)
            local project = _Camera.project(position.x, position.y + 109.2, position.z);
            if(project == null)
                return;

            local draws = [];

            local label = "";
            local maxWidth = 0;
            local heightOfDraw = 0;

            local scaleI = 0.2 + 0.3 * (((1400.0 - posDifference)/1400.0) * 1.0);

            foreach(_label in labels)  {
                local draw = Draw(0, 0, _label.text);

                if(getSetting("OtherCustomFont")) {
                    draw.font = "FONT_OLD_20_WHITE_HI.TGA";
                    draw.setScale(scaleI, scaleI);
                }

                draw.setColor(_label.color.r,_label.color.g,_label.color.b)
                draws.append(draw);

                heightOfDraw = draw.height;

                if(draw.width > maxWidth)
                    maxWidth = draw.width;
            }

            foreach(_index, draw in draws) {
                local finalX = (anx(project.x) - maxWidth/2) + (maxWidth/2 - draw.width/2);
                local finalY = any(project.y) + ((heightOfDraw + 20) * _index);
                draw.setPosition(finalX, finalY);
                draw.visible = true;
                bucket.push(draw);
            }
        }else {
            local position = getPlayerPosition(playerFocus);
            if(position != null) {
                local draw = Draw3d(position.x, position.y + 109.2, position.z);
                local labels = getLabels(playerFocus)
                foreach(index, label in labels)
                    draw.insertText(label.text);

                draw.distance = 3500;
                draw.visible = true;
                draw.setColor(250, 50, 50);
                bucket.push(draw);
            }
        }

        if(getPlayerWeaponMode(heroId) != 0) {
            local hp = getPlayerHealth(playerFocus);
            local maxHp = getPlayerMaxHealth(playerFocus);
            local percent = 0;

            try {
                percent = abs(hp * 100/maxHp);
            }catch(error) {}

            local traditionalBars = getSetting("TraditionalFocusBars");

            if(traditionalBars) {
                Surroundings.hpTextures.push(Texture(anx(Resolution.x/2 - 80), any(23), anx(160), any(22), "BAR_BACK.TGA"));
                Surroundings.hpTextures.push(Texture(0,0,0,0, "HK_HEALTHBACK_ROUNDED.TGA"));
                Surroundings.hpTextures.push(Texture(anx(Resolution.x/2 - 75), any(25), anx(150), any(18), "BAR_HEALTH.TGA"));
                Surroundings.hpTextures[Surroundings.hpTextures.len()-1].setRect(0,0,anx((150 * percent)/100),any(18));
                Surroundings.hpTextures[Surroundings.hpTextures.len()-1].setSize(anx((150 * percent)/100),any(18));
            }else {
                Surroundings.hpTextures.push(Texture(anx(Resolution.x - 100), any(20), anx(88), any(88), "HK_ROUNDED.TGA"));
                Surroundings.hpTextures.push(Texture(anx(Resolution.x - 96), any(24), anx(80), any(80), "HK_HEALTHBACK_ROUNDED.TGA"));
                Surroundings.hpTextures.push(Texture(anx(Resolution.x - 96), any(24), anx(80), any(80), "HK_HEALTH_ROUNDED.TGA"));

                Surroundings.hpTextures[Surroundings.hpTextures.len()-1].setSize(anx(78),any((78 * percent)/100));
                Surroundings.hpTextures[Surroundings.hpTextures.len()-1].setUV(0.0,0.0,1.0,(1.0 * percent)/100);
            }

            foreach(text in Surroundings.hpTextures)
                text.visible = true;
        }
    }

    if(getSetting("NamesRound") == false)
        return;

    foreach(bot in getBots())
    {
        if(isPlayerStreamed(bot.element) == false)
            continue;

        if(bot.element == playerFocus)
            continue;

        local position = getPlayerPosition(bot.element);
        local posDifference = getPositionDifference(heroPos, position);
        if(posDifference < 1400)
        {
            local labels = botLabels(bot.id);
            local project = _Camera.project(position.x, position.y + 109.2, position.z);
            if(project == null)
                continue;

            local draws = [];

            local label = "";
            local maxWidth = 0;
            local heightOfDraw = 0;

            local scaleI = 0.2 + 0.3 * (((1400.0 - posDifference)/1400.0) * 1.0);

            foreach(_label in labels)  {
                local draw = Draw(0, 0, _label.text);

                if(getSetting("OtherCustomFont")) {
                    draw.font = "FONT_OLD_20_WHITE_HI.TGA";
                    draw.setScale(scaleI, scaleI);
                }

                if(bot.instance == "PC_HERO")
                    draw.setColor(220, 200, 180);
                else
                    draw.setColor(250, 150, 100);

                draws.append(draw);

                heightOfDraw = draw.height;

                if(draw.width > maxWidth)
                    maxWidth = draw.width;
            }

            foreach(_index, draw in draws) {
                local finalX = (anx(project.x) - maxWidth/2) + (maxWidth/2 - draw.width/2);
                local finalY = any(project.y) + ((heightOfDraw + 20) * _index);
                draw.setPosition(finalX, finalY);
                draw.visible = true;
                bucket.push(draw);
            }
        }
    }

    // foreach(playerId, player in getPlayers())
    // {
    //     if(isPlayerStreamed(playerId) == false)
    //         continue;

    //     if(playerId == playerFocus || heroId == playerId)
    //         continue;

    //     local position = getPlayerPosition(playerId);
    //     local posDifference = getPositionDifference(heroPos, position);
    //     if(posDifference < 1400)
    //     {
    //         local labels = playerLabels(playerId);
    //         local project = _Camera.project(position.x, position.y + 109.2, position.z);
    //         if(project == null)
    //             continue;

    //         local draws = [];

    //         local label = "";
    //         local maxWidth = 0;
    //         local heightOfDraw = 0;

    //         local scaleI = 0.2 + 0.3 * (((1400.0 - posDifference)/1400.0) * 1.0);

    //         foreach(_label in labels)  {
    //             local draw = Draw(0, 0, _label.text);

    //             draw.font = "FONT_OLD_20_WHITE_HI.TGA";
    //             draw.setScale(scaleI, scaleI);
    //             draw.setColor(_label.color.r,_label.color.g,_label.color.b)
    //             draws.append(draw);

    //             heightOfDraw = draw.height;

    //             if(draw.width > maxWidth)
    //                 maxWidth = draw.width;
    //         }

    //         foreach(_index, draw in draws) {
    //             local finalX = (anx(project.x) - maxWidth/2) + (maxWidth/2 - draw.width/2);
    //             local finalY = any(project.y) + ((heightOfDraw + 20) * _index);
    //             draw.setPosition(finalX, finalY);
    //             draw.visible = true;
    //             bucket.push(draw);
    //         }
    //     }
    // }
}

addEventHandler("onRender", Surroundings.onRender.bindenv(Surroundings))
addEventHandler("onRenderFocus", function(type, id, x, y, name) {cancelEvent();});

/**
Class containing color of label and text

@param text string
@param r int
@param g int
@param b int
*/
Surroundings.label <- class
{
    text = null;
    color = null;

    constructor(text, r = 255, g = 255, b = 255)
    {
        this.text = text;
        this.color = { r = r, g = g, b = b }
    }
}
local sounds = [
    "LUTNIA_INSERT1.WAV",
    "LUTNIA_INSERT2.WAV",
    "LUTNIA_INSERT3.WAV",
    "LUTNIA_INSERT4.WAV",
    "LUTNIA_INSERT5.WAV",
    "LUTNIA_INSERT6.WAV",
    "LUTNIA_INSERT7.WAV",
    "LUTNIA_INSERT8.WAV",
    "LUTNIA_INSERT9.WAV",
    "LUTNIA_INSERT10.WAV",
    "LUTNIA_INSERT11.WAV",
    "LUTNIA_INSERT12.WAV",
    "LUTNIA_INSERT13.WAV",
    "LUTNIA_INSERT14.WAV",
    "LUTNIA_INSERT15.WAV",
    "LUTNIA_INSERT16.WAV",
    "LUTNIA_INSERT17.WAV",
    "LUTNIA_INSERT18.WAV",
    "LUTNIA_INSERT19.WAV",
    "LUTNIA_INSERT20.WAV",
    "LUTNIA_INSERT21.WAV",
];

local pck = getTickCount();

PlayerLute <- {
    bucket = null
    setLoop = false,
    setVolume = 5,

    prepareWindow = function() {
        local result = {};

        result.window <- GUI.Window(anx(Resolution.x/2 - 300), any(Resolution.y/2 + 100), anx(600), any(100), "HK_BOX.TGA", null, false)
        result.buttonExit <- GUI.Button(anx(400), any(120), anx(200), any(60), "HK_BUTTON.TGA", "Wyjd�", result.window);

        result.draw <- GUI.Draw(anx(50), any(20), "1,2,3,4,5,6,7,8,9 - klawisze aby odegra� d�wi�k.\nZ,X - aby zmieni� tryby.", result.window);
        result.drawTwo <- GUI.Draw(anx(50), any(60), "G�o�no��: 5", result.window);

        result.buttonExit.bind(EventType.Click, function(element) {
            PlayerLute.hide();
        });

        return result;
    }

    playSound = function(playerId, soundId, looping, volume) {
        local obj = Sound3d(sounds[soundId]);
        obj.volume = (volume * 1.0).tofloat() * 0.1;
        obj.setTargetPlayer(playerId);
        obj.looping = false;
        obj.radius = 1500;
        obj.play();
    }

    show = function() {
        if(bucket == null)
            bucket = prepareWindow();

        bucket.window.setVisible(true);
        BaseGUI.show();
        ActiveGui = PlayerGUI.Lute;

        playAni(heroId, "S_LUTE_S1");
    }

    hide = function() {
        bucket.window.setVisible(false);

        BaseGUI.hide();
        ActiveGui = null;

        playAni(heroId,"S_RUN");
    }

    onKey = function(key) {
        if(ActiveGui != PlayerGUI.Lute)
            return;

        if(pck > getTickCount())
            return;

        pck = getTickCount() + 333;
        switch(key) {
            case KEY_1:
                PlayerPacket().playLute(0, setLoop, setVolume);
            break;
            case KEY_2:
                PlayerPacket().playLute(1, setLoop, setVolume);
            break;
            case KEY_3:
                PlayerPacket().playLute(2, setLoop, setVolume);
            break;
            case KEY_4:
                PlayerPacket().playLute(3, setLoop, setVolume);
            break;
            case KEY_5:
                PlayerPacket().playLute(4, setLoop, setVolume);
            break;
            case KEY_6:
                PlayerPacket().playLute(5, setLoop, setVolume);
            break;
            case KEY_7:
                PlayerPacket().playLute(6, setLoop, setVolume);
            break;
            case KEY_8:
                PlayerPacket().playLute(7, setLoop, setVolume);
            break;
            case KEY_9:
                PlayerPacket().playLute(8, setLoop, setVolume);
            break;
            case KEY_0:
                PlayerPacket().playLute(9, setLoop, setVolume);
            break;
            case KEY_Q:
                PlayerPacket().playLute(10, setLoop, setVolume);
            break;
            case KEY_W:
                PlayerPacket().playLute(11, setLoop, setVolume);
            break;
            case KEY_E:
                PlayerPacket().playLute(12, setLoop, setVolume);
            break;
            case KEY_R:
                PlayerPacket().playLute(13, setLoop, setVolume);
            break;
            case KEY_T:
                PlayerPacket().playLute(14, setLoop, setVolume);
            break;
            case KEY_Y:
                PlayerPacket().playLute(15, setLoop, setVolume);
            break;
            case KEY_U:
                PlayerPacket().playLute(16, setLoop, setVolume);
            break;
            case KEY_I:
                PlayerPacket().playLute(17, setLoop, setVolume);
            break;
            case KEY_O:
                PlayerPacket().playLute(18, setLoop, setVolume);
            break;
            case KEY_P:
                PlayerPacket().playLute(19, setLoop, setVolume);
            break;
            case KEY_Z:
                setVolume = setVolume - 1;
                if(setVolume < 1)
                    setVolume = 1;

                bucket.drawTwo.setText("G�o�no��: "+setVolume);
            break;
            case KEY_X:
                setVolume = setVolume + 1;
                if(setVolume > 10)
                    setVolume = 10;

                bucket.drawTwo.setText("G�o�no��: "+setVolume);
            break;
        }
    }
}

addEventHandler("onKey", PlayerLute.onKey.bindenv(PlayerLute));

Bind.addKey(KEY_ESCAPE, PlayerLute.hide.bindenv(PlayerLute), PlayerGUI.Lute)
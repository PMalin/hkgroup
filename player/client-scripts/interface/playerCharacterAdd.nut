local lock = false;

enum CharacterAddOption
{
    Body,
    Skin,
    Head,
    Face,
    Size,
    Fatness,
    Walking
}

CharacterAdd <-
{
    bucket = null,

    prepareWindow = function()
    {
        local result = {};

        result.windowStorySelector <- GUI.Window(anx(Resolution.x/2 - 210), any(Resolution.y/2 - 300), anx(420), any(530), "HK_BOX.TGA", null, false);
        result.leftStorySelector <- GUI.Button(anx(-20), any(250), anx(40), any(40), "HK_L.TGA", "", result.windowStorySelector);
        result.rightStorySelector <- GUI.Button(anx(400), any(250), anx(40), any(40), "HK_R.TGA", "", result.windowStorySelector);
        result.acceptStorySelector <- GUI.Button(anx(110), any(480), anx(200), any(60), "HK_BUTTON.TGA", "Wybierz", result.windowStorySelector);
        result.windowStorySelectorDraws <- [];

        result.leftStorySelector.bind(EventType.Click, function(element) {CharacterAdd.showStorySelector(element.attribute);});
        result.rightStorySelector.bind(EventType.Click, function(element) {CharacterAdd.showStorySelector(element.attribute);});
        result.acceptStorySelector.bind(EventType.Click, function(element) {CharacterAdd.selectStory(result.windowStorySelectorDraws[2]); CharacterAdd.hide();});

        result.window <- GUI.Window(anx(40), any(Resolution.y/2 - 300), anx(420), any(560), "HK_BOX.TGA", null, false);

        result.nameInput <- GUI.Input(anx(60), any(40), anx(300), any(55), "HK_INPUT.TGA", "FONT_OLD_10_WHITE_HI.TGA", Input.Text, Align.Center, "Nazwa uytkownika", 2, result.window);

        result.bodyButton <- GUI.Button(anx(10), any(120), anx(200), any(60), "HK_BUTTON.TGA", "P�e�", result.window);
        result.skinButton <- GUI.Button(anx(210), any(120), anx(200), any(60), "HK_BUTTON.TGA", "Cia�o", result.window);
        result.headButton <- GUI.Button(anx(10), any(200), anx(200), any(60), "HK_BUTTON.TGA", "G�owa", result.window);
        result.faceButton <- GUI.Button(anx(210), any(200), anx(200), any(60), "HK_BUTTON.TGA", "Twarz", result.window);
        result.sizeButton <- GUI.Button(anx(10), any(280), anx(0), any(0), "HK_BUTTON.TGA", "", result.window);
        result.fatnessButton <- GUI.Button(anx(210), any(280), anx(200), any(60), "HK_BUTTON.TGA", "Grubo��", result.window);
        result.walkingButton <- GUI.Button(anx(10), any(280), anx(200), any(60), "HK_BUTTON.TGA", "Chodzenie", result.window);

        result.scrollBar <- GUI.ScrollBar(anx(Resolution.x/2), any(50), anx(30), any(30), "HK_SCROLL_HORIZONTAL.TGA", "HK_INDICATOR.TGA", "HK_L.TGA", "HK_R.TGA", Orientation.Horizontal, result.window);
        result.scrollBar.setMaximum(360);
        result.scrollBar.setSize(anx(500), any(30));
        result.scrollBar.setPosition(anx(Resolution.x/2 - 250), any(Resolution.y - 200));

        result.bindForScrollbar <- Bind.onChange(result.scrollBar, function(value) {
            setPlayerAngle(heroId, value);
        }, PlayerGUI.CharacterAdd);

        result.bodyButton.bind(EventType.Click, function(element) {
            CharacterAdd.chooseOperation(CharacterAddOption.Body);
        });

        result.skinButton.bind(EventType.Click, function(element) {
            CharacterAdd.chooseOperation(CharacterAddOption.Skin);
        });

        result.headButton.bind(EventType.Click, function(element) {
            CharacterAdd.chooseOperation(CharacterAddOption.Head);
        });

        result.faceButton.bind(EventType.Click, function(element) {
            CharacterAdd.chooseOperation(CharacterAddOption.Face);
        });

        result.sizeButton.bind(EventType.Click, function(element) {
            CharacterAdd.chooseOperation(CharacterAddOption.Size);
        });

        result.fatnessButton.bind(EventType.Click, function(element) {
            CharacterAdd.chooseOperation(CharacterAddOption.Fatness);
        });

        result.walkingButton.bind(EventType.Click, function(element) {
            CharacterAdd.chooseOperation(CharacterAddOption.Walking);
        });

        result.backButton <- GUI.Button(anx(10), any(440), anx(200), any(80), "HK_BUTTON.TGA", "Wr��", result.window);
        result.createButton <- GUI.Button(anx(210), any(440), anx(200), any(80), "HK_BUTTON.TGA", "Stw�rz", result.window);

        result.windowOperation <- GUI.Window(anx(Resolution.x - 420), any(Resolution.y/2 - 300), anx(400), any(560), "HK_BOX.TGA", null, false);
        result.operations <- [];

        result.backButton.bind(EventType.Click, function(element) {
            CharacterAdd.hide();
            showCharacterList();
        });

        result.createButton.bind(EventType.Click, function(element) {
            if(CharacterAdd.bucket.nameInput.getText().len() > 2)
                PlayerPacket().addCharacter(CharacterAdd.bucket.nameInput.getText());
        });

        return result;
    },

    chooseOperation = function (typeId)
    {
        foreach(element in bucket.operations)
        {
            if(element instanceof GUI.Input || element instanceof GUI.Button || element instanceof GUI.Texture || element instanceof GUI.CheckBox)
                element.setVisible(false);

            element.destroy();
        }

        lock = false;
        stopAni(heroId);

        bucket.operations.clear();
        bucket.windowOperation.destroyAll();
        bucket.windowOperation = GUI.Window(anx(Resolution.x - 420), any(Resolution.y/2 - 300), anx(400), any(560), "HK_BOX.TGA", null, false);

        switch(typeId)
        {
            case CharacterAddOption.Body:
                local maleButton = GUI.Button(anx(50), any(200), anx(300), any(80), "HK_BUTTON.TGA", "M�czyzna", bucket.windowOperation);
                local femaleButton = GUI.Button(anx(50), any(300), anx(300), any(80), "HK_BUTTON.TGA", "Kobieta", bucket.windowOperation);

                maleButton.bind(EventType.Click, function(element) {
                    CharacterAdd.changeVisual(CharacterAddOption.Body, "Hum_Body_Naked0");
                });
                femaleButton.bind(EventType.Click, function(element) {
                    CharacterAdd.changeVisual(CharacterAddOption.Body, "Hum_Body_Babe0");
                });

                bucket.operations.append(maleButton);
                bucket.operations.append(femaleButton);
            break;
            case CharacterAddOption.Skin:
                for(local left = 0; left < 3; left ++)
                    for(local top = 0; top < 3; top ++)
                        bucket.operations.append(GUI.Button(anx(120) * left + anx(20), any(120) * top + any(60), anx(120), any(120), "HUM_BODY_NAKED_V"+((left * 2) + top + left)+"_C0.TGA", ((left * 2) + top + left).tostring(), bucket.windowOperation));

                foreach(butt in bucket.operations) {
                    butt.bind(EventType.Click, function(element) {
                        CharacterAdd.changeVisual(CharacterAddOption.Skin, element.getText().tointeger());
                    });
                }

                bucket.operations.append(GUI.ScrollBar(anx(Resolution.x/2), any(50), anx(30), any(30), "HK_SCROLL_HORIZONTAL.TGA", "HK_INDICATOR.TGA", "HK_L.TGA", "HK_R.TGA", Orientation.Horizontal, bucket.windowOperation));
                bucket.operations[bucket.operations.len()-1].setMaximum(Config["PlayerMaxSkin"]);
                bucket.operations[bucket.operations.len()-1].setSize(anx(340), any(30));
                bucket.operations[bucket.operations.len()-1].setPosition(anx(Resolution.x - 400), any(Resolution.y/2 + 200));

                Bind.onChange(bucket.operations[bucket.operations.len()-1], function(value) {
                    for(local left = 0; left < 3; left ++) {
                        for(local top = 0; top < 3; top ++) {
                            CharacterAdd.bucket.operations[((left * 2) + top + left)].setText((value + (left * 2) + top + left).tostring());
                            CharacterAdd.bucket.operations[((left * 2) + top + left)].setFile("HUM_BODY_NAKED_V"+(value + (left * 2) + top + left)+"_C0.TGA");
                        }
                    }

                }, PlayerGUI.CharacterAdd);
            break;
            case CharacterAddOption.Head:
                local fatBaldHeadButton = GUI.Button(anx(20), any(30), anx(170), any(50), "HK_BUTTON.TGA", "Tusty �ysy", bucket.windowOperation);
                local fighterHeadButton = GUI.Button(anx(210), any(30), anx(170), any(50), "HK_BUTTON.TGA", "Wojowniczy", bucket.windowOperation);
                local ponyHeadButton = GUI.Button(anx(20), any(80), anx(170), any(50), "HK_BUTTON.TGA", "Kucyk", bucket.windowOperation);
                local baldHeadButton = GUI.Button(anx(210), any(80), anx(170), any(50), "HK_BUTTON.TGA", "�ysy", bucket.windowOperation);
                local thiefHeadButton = GUI.Button(anx(20), any(130), anx(170), any(50), "HK_BUTTON.TGA", "Zodziej", bucket.windowOperation);
                local psionicHeadButton = GUI.Button(anx(210), any(130), anx(170), any(50), "HK_BUTTON.TGA", "Z�o�liwy", bucket.windowOperation);
                local tiredHeadButton = GUI.Button(anx(20), any(180), anx(170), any(50), "HK_BUTTON.TGA", "Zm�czony", bucket.windowOperation);
                local tallHeadButton = GUI.Button(anx(210), any(180), anx(170), any(50), "HK_BUTTON.TGA", "Wysoki", bucket.windowOperation);
                local fatHeadButton = GUI.Button(anx(20), any(230), anx(170), any(50), "HK_BUTTON.TGA", "Gruby", bucket.windowOperation);
                local angryHeadButton = GUI.Button(anx(210), any(230), anx(170), any(50), "HK_BUTTON.TGA", "Z�y", bucket.windowOperation);
                local bigBeardHeadButton = GUI.Button(anx(20), any(280), anx(170), any(50), "HK_BUTTON.TGA", "Broda dua", bucket.windowOperation);
                local smallBeardHeadButton = GUI.Button(anx(210), any(280), anx(170), any(50), "HK_BUTTON.TGA", "Broda ma�a", bucket.windowOperation);
                local middleBeardHeadButton = GUI.Button(anx(20), any(330), anx(170), any(50), "HK_BUTTON.TGA", "Broda �rednia", bucket.windowOperation);
                local musstacheHeadButton = GUI.Button(anx(210), any(330), anx(170), any(50), "HK_BUTTON.TGA", "Broda + w�s", bucket.windowOperation);
                local alternativeHeadButton = GUI.Button(anx(20), any(380), anx(170), any(50), "HK_BUTTON.TGA", "Broda alter.", bucket.windowOperation);
                local pontyailHeadButton = GUI.Button(anx(210), any(380), anx(170), any(50), "HK_BUTTON.TGA", "Broda + kucyk", bucket.windowOperation);
                local sideburnsHeadButton = GUI.Button(anx(20), any(430), anx(170), any(50), "HK_BUTTON.TGA", "Boki wygolone", bucket.windowOperation);
                local longhairHeadButton = GUI.Button(anx(210), any(430), anx(170), any(50), "HK_BUTTON.TGA", "Dugie wosy", bucket.windowOperation);
                local babe12HeadButton = GUI.Button(anx(20), any(480), anx(170), any(50), "HK_BUTTON.TGA", "Kobieta warkocz", bucket.windowOperation);
                local beard4HeadButton = GUI.Button(anx(210), any(480), anx(170), any(50), "HK_BUTTON.TGA", "Broda altern.", bucket.windowOperation);
                local nord2HeadButton = GUI.Button(anx(20), any(530), anx(170), any(50), "HK_BUTTON.TGA", "Nord altern.", bucket.windowOperation);
                local mageHeadButton = GUI.Button(anx(210), any(530), anx(170), any(50), "HK_BUTTON.TGA", "Mag", bucket.windowOperation);
                local nerdHeadButton = GUI.Button(anx(20), any(580), anx(170), any(50), "HK_BUTTON.TGA", "Nerd", bucket.windowOperation);
                local segmentsHeadButton = GUI.Button(anx(210), any(580), anx(170), any(50), "HK_BUTTON.TGA", "Segmenty", bucket.windowOperation);
                local dredHeadButton = GUI.Button(anx(20), any(630), anx(170), any(50), "HK_BUTTON.TGA", "Dredy", bucket.windowOperation);
                local page2HeadButton = GUI.Button(anx(210), any(630), anx(170), any(50), "HK_BUTTON.TGA", "Pa�", bucket.windowOperation);
                local pageHeadButton = GUI.Button(anx(20), any(680), anx(170), any(50), "HK_BUTTON.TGA", "Pa� altern.", bucket.windowOperation);
                local nordHeadButton = GUI.Button(anx(210), any(680), anx(170), any(50), "HK_BUTTON.TGA", "Nord", bucket.windowOperation);
                local monkHeadButton = GUI.Button(anx(20), any(730), anx(170), any(50), "HK_BUTTON.TGA", "Mnich", bucket.windowOperation);
                local longbabeHeadButton = GUI.Button(anx(210), any(730), anx(170), any(50), "HK_BUTTON.TGA", "Kobieta d�ugie", bucket.windowOperation);

                fatBaldHeadButton.bind(EventType.Click, function(element) {CharacterAdd.changeVisual(CharacterAddOption.Head, "Hum_Head_FatBald");});
                fighterHeadButton.bind(EventType.Click, function(element) {CharacterAdd.changeVisual(CharacterAddOption.Head, "Hum_Head_Fighter");});
                ponyHeadButton.bind(EventType.Click, function(element) {CharacterAdd.changeVisual(CharacterAddOption.Head, "Hum_Head_Pony");});
                baldHeadButton.bind(EventType.Click, function(element) {CharacterAdd.changeVisual(CharacterAddOption.Head, "Hum_Head_Bald");});
                thiefHeadButton.bind(EventType.Click, function(element) {CharacterAdd.changeVisual(CharacterAddOption.Head, "Hum_Head_Thief");});
                psionicHeadButton.bind(EventType.Click, function(element) {CharacterAdd.changeVisual(CharacterAddOption.Head, "Hum_Head_Psionic");});
                tiredHeadButton.bind(EventType.Click, function(element) {CharacterAdd.changeVisual(CharacterAddOption.Head, "ASC_HUM_HEAD_TIRED");});
                tallHeadButton.bind(EventType.Click, function(element) {CharacterAdd.changeVisual(CharacterAddOption.Head, "ASC_HUM_HEAD_TALL");});
                fatHeadButton.bind(EventType.Click, function(element) {CharacterAdd.changeVisual(CharacterAddOption.Head, "ASC_HUM_HEAD_FAT");});
                angryHeadButton.bind(EventType.Click, function(element) {CharacterAdd.changeVisual(CharacterAddOption.Head, "ASC_HUM_HEAD_ANGRY");});
                bigBeardHeadButton.bind(EventType.Click, function(element) {CharacterAdd.changeVisual(CharacterAddOption.Head, "HUM_HEAD_BrodaDuza");});
                smallBeardHeadButton.bind(EventType.Click, function(element) {CharacterAdd.changeVisual(CharacterAddOption.Head, "HUM_HEAD_BrodaMala");});
                middleBeardHeadButton.bind(EventType.Click, function(element) {CharacterAdd.changeVisual(CharacterAddOption.Head, "HUM_HEAD_BrodaSrednia");});
                musstacheHeadButton.bind(EventType.Click, function(element) {CharacterAdd.changeVisual(CharacterAddOption.Head, "HUM_HEAD_BrodaWas");});
                alternativeHeadButton.bind(EventType.Click, function(element) {CharacterAdd.changeVisual(CharacterAddOption.Head, "HUM_HEAD_BEARD2");});
                pontyailHeadButton.bind(EventType.Click, function(element) {CharacterAdd.changeVisual(CharacterAddOption.Head, "HUM_HEAD_PONYBEARD");});
                sideburnsHeadButton.bind(EventType.Click, function(element) {CharacterAdd.changeVisual(CharacterAddOption.Head, "HUM_HEAD_SIDEBURNS");});
                longhairHeadButton.bind(EventType.Click, function(element) {CharacterAdd.changeVisual(CharacterAddOption.Head, "HUM_HEAD_LONGHAIR");});
                babe12HeadButton.bind(EventType.Click, function(element) {CharacterAdd.changeVisual(CharacterAddOption.Head, "HUM_HEAD_BABE12");});
                beard4HeadButton.bind(EventType.Click, function(element) {CharacterAdd.changeVisual(CharacterAddOption.Head, "HUM_HEAD_BEARD4");});
                nord2HeadButton.bind(EventType.Click, function(element) {CharacterAdd.changeVisual(CharacterAddOption.Head, "ASC_HUM_HEAD_NORD2");});
                mageHeadButton.bind(EventType.Click, function(element) {CharacterAdd.changeVisual(CharacterAddOption.Head, "ASC_HUM_HEAD_MAGE");});
                nerdHeadButton.bind(EventType.Click, function(element) {CharacterAdd.changeVisual(CharacterAddOption.Head, "ASC_HUM_HEAD_NERD");});
                segmentsHeadButton.bind(EventType.Click, function(element) {CharacterAdd.changeVisual(CharacterAddOption.Head, "ASC_HUM_HEAD_SEGMENTS");});
                dredHeadButton.bind(EventType.Click, function(element) {CharacterAdd.changeVisual(CharacterAddOption.Head, "ASC_HUM_HEAD_DRED");});
                page2HeadButton.bind(EventType.Click, function(element) {CharacterAdd.changeVisual(CharacterAddOption.Head, "ASC_HUM_HEAD_PAGE2");});
                pageHeadButton.bind(EventType.Click, function(element) {CharacterAdd.changeVisual(CharacterAddOption.Head, "ASC_HUM_HEAD_PAGE");});
                nordHeadButton.bind(EventType.Click, function(element) {CharacterAdd.changeVisual(CharacterAddOption.Head, "ASC_HUM_HEAD_NORD");});
                monkHeadButton.bind(EventType.Click, function(element) {CharacterAdd.changeVisual(CharacterAddOption.Head, "ASC_HUM_HEAD_MONK");});
                longbabeHeadButton.bind(EventType.Click, function(element) {CharacterAdd.changeVisual(CharacterAddOption.Head, "HUM_HEAD_LONGBABE");});

                bucket.operations.append(fatBaldHeadButton);
                bucket.operations.append(fighterHeadButton);
                bucket.operations.append(ponyHeadButton);
                bucket.operations.append(baldHeadButton);
                bucket.operations.append(thiefHeadButton);
                bucket.operations.append(psionicHeadButton);
                bucket.operations.append(tiredHeadButton);
                bucket.operations.append(tallHeadButton);
                bucket.operations.append(fatHeadButton);
                bucket.operations.append(angryHeadButton);
                bucket.operations.append(bigBeardHeadButton);
                bucket.operations.append(smallBeardHeadButton);
                bucket.operations.append(middleBeardHeadButton);
                bucket.operations.append(musstacheHeadButton);
                bucket.operations.append(alternativeHeadButton);
                bucket.operations.append(pontyailHeadButton);
                bucket.operations.append(sideburnsHeadButton);
                bucket.operations.append(longhairHeadButton);
                bucket.operations.append(babe12HeadButton);
                bucket.operations.append(beard4HeadButton);
                bucket.operations.append(nord2HeadButton);
                bucket.operations.append(mageHeadButton);
                bucket.operations.append(nerdHeadButton);
                bucket.operations.append(segmentsHeadButton);
                bucket.operations.append(dredHeadButton);
                bucket.operations.append(page2HeadButton);
                bucket.operations.append(pageHeadButton);
                bucket.operations.append(nordHeadButton);
                bucket.operations.append(monkHeadButton);
                bucket.operations.append(longbabeHeadButton);
            break;
            case CharacterAddOption.Face:
                for(local left = 0; left < 3; left ++)
                    for(local top = 0; top < 3; top ++)
                        bucket.operations.append(GUI.Button(anx(120) * left + anx(20), any(120) * top + any(60), anx(120), any(120), "Hum_Head_V"+((left * 2) + top + left)+"_C0.TGA", ((left * 2) + top + left).tostring(), bucket.windowOperation));

                foreach(butt in bucket.operations) {
                    butt.bind(EventType.Click, function(element) {
                        CharacterAdd.changeVisual(CharacterAddOption.Face, element.getText().tointeger());
                    });
                }

                bucket.operations.append(GUI.ScrollBar(anx(Resolution.x/2), any(50), anx(30), any(30), "HK_SCROLL_HORIZONTAL.TGA", "HK_INDICATOR.TGA", "HK_L.TGA", "HK_R.TGA", Orientation.Horizontal, bucket.windowOperation));
                bucket.operations[bucket.operations.len()-1].setMaximum(Config["PlayerMaxFace"]);
                bucket.operations[bucket.operations.len()-1].setSize(anx(340), any(30));
                bucket.operations[bucket.operations.len()-1].setPosition(anx(Resolution.x - 400), any(Resolution.y/2 + 200));

                Bind.onChange(bucket.operations[bucket.operations.len()-1], function(value) {
                    for(local left = 0; left < 3; left ++) {
                        for(local top = 0; top < 3; top ++) {
                            CharacterAdd.bucket.operations[((left * 2) + top + left)].setText((value + (left * 2) + top + left).tostring());
                            CharacterAdd.bucket.operations[((left * 2) + top + left)].setFile("Hum_Head_V"+(value + (left * 2) + top + left)+"_C0.TGA");
                        }
                    }

                }, PlayerGUI.CharacterAdd);
            break;

            case CharacterAddOption.Size:
                local midgetSizeButton = GUI.Button(anx(20), any(150), anx(170), any(80), "HK_BUTTON.TGA", "Krasnal", bucket.windowOperation);
                local dwarfSizeButton = GUI.Button(anx(210), any(150), anx(170), any(80), "HK_BUTTON.TGA", "Ma�y", bucket.windowOperation);
                local smallSizeButton = GUI.Button(anx(20), any(250), anx(170), any(80), "HK_BUTTON.TGA", "Niski", bucket.windowOperation);
                local normalSizeButton = GUI.Button(anx(210), any(250), anx(170), any(80), "HK_BUTTON.TGA", "Normalny", bucket.windowOperation);
                local tallSizeButton = GUI.Button(anx(20), any(350), anx(170), any(80), "HK_BUTTON.TGA", "Wysoki", bucket.windowOperation);
                local giantSizeButton = GUI.Button(anx(210), any(350), anx(170), any(80), "HK_BUTTON.TGA", "Gigant", bucket.windowOperation);

                midgetSizeButton.bind(EventType.Click, function(element) {CharacterAdd.changeVisual(CharacterAddOption.Size, PlayerSpeech.Midget);});
                dwarfSizeButton.bind(EventType.Click, function(element) {CharacterAdd.changeVisual(CharacterAddOption.Size, PlayerSpeech.Dwarf);});
                smallSizeButton.bind(EventType.Click, function(element) {CharacterAdd.changeVisual(CharacterAddOption.Size, PlayerSpeech.Small);});
                normalSizeButton.bind(EventType.Click, function(element) {CharacterAdd.changeVisual(CharacterAddOption.Size, PlayerSpeech.Normal);});
                tallSizeButton.bind(EventType.Click, function(element) {CharacterAdd.changeVisual(CharacterAddOption.Size, PlayerSpeech.Tall);});
                giantSizeButton.bind(EventType.Click, function(element) {CharacterAdd.changeVisual(CharacterAddOption.Size, PlayerSpeech.Giant);});

                bucket.operations.append(midgetSizeButton);bucket.operations.append(dwarfSizeButton);bucket.operations.append(smallSizeButton);
                bucket.operations.append(normalSizeButton);bucket.operations.append(tallSizeButton);bucket.operations.append(giantSizeButton);
            break;

            case CharacterAddOption.Fatness:
                local thinFatnessButton = GUI.Button(anx(20), any(150), anx(170), any(80), "HK_BUTTON.TGA", "Chudzielec", bucket.windowOperation);
                local skinnyFatnessButton = GUI.Button(anx(210), any(150), anx(170), any(80), "HK_BUTTON.TGA", "Chudy", bucket.windowOperation);
                local normalFatnessButton = GUI.Button(anx(20), any(250), anx(170), any(80), "HK_BUTTON.TGA", "Normalny", bucket.windowOperation);
                local muscularFatnessButton = GUI.Button(anx(210), any(250), anx(170), any(80), "HK_BUTTON.TGA", "Uminiony", bucket.windowOperation);
                local thickFatnessButton = GUI.Button(anx(20), any(350), anx(170), any(80), "HK_BUTTON.TGA", "Pulchny", bucket.windowOperation);
                local fatFatnessButton = GUI.Button(anx(210), any(350), anx(170), any(80), "HK_BUTTON.TGA", "Tusty", bucket.windowOperation);

                thinFatnessButton.bind(EventType.Click, function(element) {CharacterAdd.changeVisual(CharacterAddOption.Fatness, PlayerFatness.Thin);});
                skinnyFatnessButton.bind(EventType.Click, function(element) {CharacterAdd.changeVisual(CharacterAddOption.Fatness, PlayerFatness.Skinny);});
                normalFatnessButton.bind(EventType.Click, function(element) {CharacterAdd.changeVisual(CharacterAddOption.Fatness, PlayerFatness.Normal);});
                muscularFatnessButton.bind(EventType.Click, function(element) {CharacterAdd.changeVisual(CharacterAddOption.Fatness, PlayerFatness.Muscular);});
                thickFatnessButton.bind(EventType.Click, function(element) {CharacterAdd.changeVisual(CharacterAddOption.Fatness, PlayerFatness.Thick);});
                fatFatnessButton.bind(EventType.Click, function(element) {CharacterAdd.changeVisual(CharacterAddOption.Fatness, PlayerFatness.Fat);});

                bucket.operations.append(thinFatnessButton);bucket.operations.append(skinnyFatnessButton);bucket.operations.append(normalFatnessButton);
                bucket.operations.append(muscularFatnessButton);bucket.operations.append(thickFatnessButton);bucket.operations.append(fatFatnessButton);
            break;

            case CharacterAddOption.Walking:
                lock = getPlayerPosition(heroId);
                playAni(heroId, "S_WALKL");

                local normalWalkingButton = GUI.Button(anx(20), any(80), anx(170), any(50), "HK_BUTTON.TGA", "Normalny", bucket.windowOperation);
                local tiredWalkingButton = GUI.Button(anx(210), any(80), anx(170), any(50), "HK_BUTTON.TGA", "Zm�czony", bucket.windowOperation);
                local womanWalkingButton = GUI.Button(anx(20), any(130), anx(170), any(50), "HK_BUTTON.TGA", "Kobieta", bucket.windowOperation);
                local armyWalkingButton = GUI.Button(anx(210), any(130), anx(170), any(50), "HK_BUTTON.TGA", "Wojsko", bucket.windowOperation);
                local lazyWalkingButton = GUI.Button(anx(20), any(180), anx(170), any(50), "HK_BUTTON.TGA", "Wyluzowany", bucket.windowOperation);
                local arrogantWalkingButton = GUI.Button(anx(210), any(180), anx(170), any(50), "HK_BUTTON.TGA", "Arogancki", bucket.windowOperation);
                local mageWalkingButton = GUI.Button(anx(20), any(230), anx(170), any(50), "HK_BUTTON.TGA", "Mag", bucket.windowOperation);
                local pocketsWalkingButton = GUI.Button(anx(210), any(230), anx(170), any(50), "HK_BUTTON.TGA", "Z�odziej", bucket.windowOperation);
                local tired2WalkingButton = GUI.Button(anx(20), any(280), anx(170), any(50), "HK_BUTTON.TGA", "Zm�czony v2", bucket.windowOperation);
                local arrogance2WalkingButton = GUI.Button(anx(210), any(280), anx(170), any(50), "HK_BUTTON.TGA", "Arogancki v2", bucket.windowOperation);
                local mage2WalkingButton = GUI.Button(anx(20), any(330), anx(170), any(50), "HK_BUTTON.TGA", "Mag v2", bucket.windowOperation);
                local relaxed2WalkingButton = GUI.Button(anx(210), any(330), anx(170), any(50), "HK_BUTTON.TGA", "Wyluzowany v2", bucket.windowOperation);

                normalWalkingButton.bind(EventType.Click, function(element) {CharacterAdd.changeVisual(CharacterAddOption.Walking, PlayerWalk.Normal);});
                tiredWalkingButton.bind(EventType.Click, function(element) {CharacterAdd.changeVisual(CharacterAddOption.Walking, PlayerWalk.Tired);});
                womanWalkingButton.bind(EventType.Click, function(element) {CharacterAdd.changeVisual(CharacterAddOption.Walking, PlayerWalk.Woman);});
                armyWalkingButton.bind(EventType.Click, function(element) {CharacterAdd.changeVisual(CharacterAddOption.Walking, PlayerWalk.Army);});
                lazyWalkingButton.bind(EventType.Click, function(element) {CharacterAdd.changeVisual(CharacterAddOption.Walking, PlayerWalk.Lazy);});
                arrogantWalkingButton.bind(EventType.Click, function(element) {CharacterAdd.changeVisual(CharacterAddOption.Walking, PlayerWalk.Arrogant);});
                mageWalkingButton.bind(EventType.Click, function(element) {CharacterAdd.changeVisual(CharacterAddOption.Walking, PlayerWalk.Mage);});
                pocketsWalkingButton.bind(EventType.Click, function(element) {CharacterAdd.changeVisual(CharacterAddOption.Walking, PlayerWalk.Pockets);});
                tired2WalkingButton.bind(EventType.Click, function(element) {CharacterAdd.changeVisual(CharacterAddOption.Walking, PlayerWalk.OldTired);});
                arrogance2WalkingButton.bind(EventType.Click, function(element) {CharacterAdd.changeVisual(CharacterAddOption.Walking, PlayerWalk.OldArrogant);});
                mage2WalkingButton.bind(EventType.Click, function(element) {CharacterAdd.changeVisual(CharacterAddOption.Walking, PlayerWalk.OldMage);});
                relaxed2WalkingButton.bind(EventType.Click, function(element) {CharacterAdd.changeVisual(CharacterAddOption.Walking, PlayerWalk.OldLazy);});

                bucket.operations.append(normalWalkingButton);bucket.operations.append(tiredWalkingButton);bucket.operations.append(womanWalkingButton);
                bucket.operations.append(armyWalkingButton);bucket.operations.append(lazyWalkingButton);bucket.operations.append(arrogantWalkingButton);
                bucket.operations.append(mageWalkingButton);bucket.operations.append(pocketsWalkingButton);bucket.operations.append(tired2WalkingButton);
                bucket.operations.append(arrogance2WalkingButton);bucket.operations.append(mage2WalkingButton);bucket.operations.append(relaxed2WalkingButton);
            break;
        }

        bucket.windowOperation.setVisible(true);
    }

    changeVisual = function(type, value)
    {
        local getVisual = getPlayerVisual(heroId);
        switch(type)
        {
            case CharacterAddOption.Body:
                setPlayerVisual(heroId, value, getVisual.bodyTxt, getVisual.headModel, getVisual.headTxt);
            break;
            case CharacterAddOption.Skin:
                setPlayerVisual(heroId, getVisual.bodyModel, value, getVisual.headModel, getVisual.headTxt);
            break;
            case CharacterAddOption.Head:
                setPlayerVisual(heroId, getVisual.bodyModel, getVisual.bodyTxt, value, getVisual.headTxt);
            break;
            case CharacterAddOption.Face:
                setPlayerVisual(heroId, getVisual.bodyModel, getVisual.bodyTxt, getVisual.headModel, value);
            break;
            case CharacterAddOption.Size:
                setPlayerSpeech(heroId, value);
            break;
            case CharacterAddOption.Fatness:
                setPlayerFatness(heroId, value);
            break;
            case CharacterAddOption.Walking:
                setPlayerWalkingStyle(heroId, value);
            break;
        }
        getVisual = getPlayerVisual(heroId);
        PlayerPacket().sendVisual(getVisual.bodyModel, getVisual.bodyTxt, getVisual.headModel, getVisual.headTxt);
    }

    destroyWindow = function ()
    {
        foreach(element in bucket)
        {
            if(element instanceof GUI.Input || element instanceof GUI.Button || element instanceof GUI.Texture || element instanceof GUI.CheckBox || element instanceof GUI.ScrollBar)
                element.setVisible(false);
        }

        Bind.remove(bucket.bindForScrollbar);

        bucket.window.destroyAll();
        bucket.windowOperation.destroyAll();
        bucket.windowStorySelector.destroyAll();
        bucket.windowStorySelectorDraws.clear();
    },

    start = function ()
    {
        BaseGUI.show();
        ActiveGui = PlayerGUI.CharacterAdd;

        local getVisual = getPlayerVisual(heroId);
        setPlayerVisual(heroId, getVisual.bodyModel, getVisual.bodyTxt, getVisual.headModel, getVisual.headTxt);

        Camera.setPosition(7850.62, 5695.86, 37282.7);
        Camera.setRotation(10, 234, 0);

        bucket = prepareWindow();
        bucket.window.setVisible(true);
        bucket.scrollBar.setVisible(true);
        bucket.scrollBar.setValue(getPlayerAngle(heroId).tointeger());
    },

    showStorySelector = function(currentId = 0)
    {
        bucket.windowStorySelector.setVisible(true);
        bucket.window.setVisible(false);
        bucket.scrollBar.setVisible(false);
        bucket.windowOperation.setVisible(false);

        bucket.leftStorySelector.attribute = currentId > 0 ? (currentId - 1) : 3;
        bucket.rightStorySelector.attribute = currentId < 3 ? (currentId + 1) : 0;

        local obj = Config["PlayerScenario"][currentId];
        local x = anx(Resolution.x/2 - 210), y = any(Resolution.y/2 - 300);

        bucket.windowStorySelectorDraws = [
            Draw(x + anx(30), y + any(30), obj.name),
            Draw(x + anx(25), y + any(90), obj.description),
            currentId
        ];

        bucket.windowStorySelectorDraws[0].font = "FONT_OLD_20_WHITE_HI.TGA";
        bucket.windowStorySelectorDraws[1].font = "FONT_OLD_20_WHITE_HI.TGA";
        bucket.windowStorySelectorDraws[0].setColor(200, 0, 0);
        bucket.windowStorySelectorDraws[1].setColor(235, 215, 187);
        bucket.windowStorySelectorDraws[1].setScale(0.5, 0.5);
        bucket.windowStorySelectorDraws[0].visible = true;
        bucket.windowStorySelectorDraws[1].visible = true;
    }

    selectStory = function(currentId)
    {
        local obj = Config["PlayerScenario"][currentId];
        setPlayerPosition(heroId, obj.spawn.x, obj.spawn.y, obj.spawn.z);
        setPlayerAngle(heroId, obj.spawn.angle);
    }

    hide = function ()
    {
        BaseGUI.hide();
        ActiveGui = null;

        lock = false;
        stopAni(heroId);

        destroyWindow();
        bucket = null;
    }
}

addEventHandler("onRender", function() {
    if(lock != false)
    {
        if(getPlayerAni(heroId) != "S_WALKL")
            playAni(heroId, "S_WALKL");

        setPlayerPosition(heroId, lock.x, lock.y, lock.z);
    }
})


addEventHandler("onPlayerLoad", function() { if( ActiveGui == PlayerGUI.CharacterAdd) CharacterAdd.showStorySelector(); })

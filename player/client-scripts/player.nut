
class Player
{
    id = -1;
    fractionId = -1;

    godMode = false;

    masked = false;

    constructor(pid)
    {
        id = pid;
        fractionId = -1;

        godMode = false;

        masked = false;
    }

    function setGodMode(value) 
    {
        godMode = value;
    }

    function isGodModeEnabled() { return godMode; }
    
    function clear()
    {
        fractionId = -1;
    }
}
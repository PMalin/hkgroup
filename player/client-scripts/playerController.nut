class PlayerController
{
    static players = [];

    static function setTeam(playerId, teamId) {
        players[playerId].teamId = teamId;
        callEvent("onPlayerChangeTeam", playerId, teamId);
    }

    static function onInit() {
        for(local i = 0; i < getMaxSlots(); i ++)
            players.push(Player(i));
    }

    static function onDamage(damage, damageType)
    {
        if(isGodModeEnabled()) cancelEvent();
    }

    static function onPacket(packet)
    {
        local typ = packet.readUInt8();
        switch(typ)
        {
            // Character
            case PlayerPackets.CharacterList:
                addToCharacterList({
                    rid = packet.readInt16(),
                    name = packet.readString(),
                    bodyModel = packet.readString(),
                    bodyTxt = packet.readInt16(),
                    headModel = packet.readString(),
                    headTxt = packet.readInt16(),
                    armor = packet.readString(),
                    melee = packet.readString(),
                    ranged = packet.readString(),
                    str = packet.readInt16(),
                    dex = packet.readInt16(),
                    health = packet.readInt16(),
                    maxHealth = packet.readInt16(),
                    mana = packet.readInt16(),
                    maxMana = packet.readInt16(),
                    statusId = packet.readInt16(),
                    learnPoints = packet.readInt16(),
                    minutesInGame = packet.readInt32(),
                    createdAt = packet.readString(),
                    updatedAt = packet.readString(),
                })
            break;
            case PlayerPackets.CharacterAccept:
                Loading.hide();

                switch(ActiveGui)
                {
                    case PlayerGUI.CharacterAdd:
                        callEvent("onPlayerLoad");
                        callEvent("onPlayerRegister");
                    break;
                    case PlayerGUI.CharacterList:
                        callEvent("onPlayerLoad");
                    break;
                }

                setKeyLayout(1);
            break;

            // Statistics
            case PlayerPackets.Level:
                local oldLevel = getLevel();
                setLevel(packet.readInt16());
                callEvent("onPlayerChangeLevel", heroId, oldLevel, getLevel());
            break;
            case PlayerPackets.Experience:
                setExp(packet.readInt16());
            break;
            case PlayerPackets.Inteligence:
                setPlayerInteligence(heroId, packet.readInt16());
            break;
            case PlayerPackets.NextLevelExperience:
                setNextLevelExp(packet.readInt16());
            break;
            case PlayerPackets.LearnPoints:
                setLearnPoints(packet.readInt16());
            break;
            case PlayerPackets.Stamina:
                setPlayerStamina(heroId, packet.readFloat());
            break;
            case PlayerPackets.Hunger:
                setPlayerHunger(heroId, packet.readFloat());
            break;
            case PlayerPackets.Endurance:
                setPlayerEndurance(heroId, packet.readInt16());
            break;
            case PlayerPackets.ClearInventory:
                clearInventory();
            break;
            case PlayerPackets.PersonalNotes:
                Hero.personalNotes.clear();
                local length = packet.readInt16();

                for(local i = 0; i < length; i ++)
                    Hero.personalNotes.append(packet.readString())
            break;
            case PlayerPackets.PersonalCard:
                Hero.personalCard.clear();
                local length = packet.readInt16();

                for(local i = 0; i < length; i ++)
                    Hero.personalCard.append(packet.readString())
            break;
            case PlayerPackets.RealName:
                local playerId = packet.readInt16();
                local name = packet.readString();

                Hero.realNames[playerId] = name;

                if(playerId == heroId) {
                    Discord.activity.state = format("W grze: %s - %s", name, "Historia Kolonii")
                    Discord.activity.update();
                }
            break;
            case PlayerPackets.Skill:
                local skillId = packet.readInt16();
                local value = packet.readInt16();
                Hero.skills[skillId] = value;
            break;
            case PlayerPackets.UseItem:
                if(ActiveGui == PlayerGUI.Inventory)
                    Inventory.hide();
            break;
            case PlayerPackets.Fraction:
                local infoLenght = packet.readInt16();
                local obj = [];
                for(local i = 0; i < infoLenght; i++)
                    obj.append({type = packet.readInt16(), value = packet.readString()});

                if(ActiveGui == PlayerGUI.Fraction)
                    FractionMenu.load(obj);
            break;
            case PlayerPackets.FractionManage:
                FractionsInterface.setUp({
                    playerId = packet.readInt16(),
                    fractionId = packet.readInt16(),
                    fractionRoleId = packet.readInt16(),
                })
            break;
            case PlayerPackets.Notification:
                addPlayerNotification(heroId, packet.readString())
            break;
            case PlayerPackets.PositionRearange:
                local isWalking = packet.readBool();
                Hero.goTo(isWalking, packet.readFloat(), packet.readFloat(),packet.readFloat(), packet.readFloat());
            break;
            case PlayerPackets.Ask:
                local askId = packet.readInt16();
                local askType = packet.readInt16();
                local askValue = packet.readString();
                PlayerAsk.run(askId, askType, askValue);
            break;
            case PlayerPackets.AcceptAsk:
                PlayerAsk.close();
            break;
            case PlayerPackets.RejectAsk:
                PlayerAsk.close();
                addPlayerNotification(heroId, "Odrzucono propozycję.");
            break;
            case PlayerPackets.RandomRespawn:
                setRandomRespawn();
            break;
            case PlayerPackets.ApplyAnimation:
                applyOverlay(packet.readInt16())
            break;
            case PlayerPackets.RemoveAnimation:
                removeOverlay(packet.readInt16())
            break;
            case PlayerPackets.RemoveAllAnimation:
                removeAllOverlay();
            break;
            case PlayerPackets.Speech:
                Hero.speech = packet.readInt16();
            break;
            case PlayerPackets.FractionInfo:
                local obj = {
                    fractionId = packet.readInt16(),
                    rankId = packet.readInt16(),
                    members = {},
                }
                local lenghtMembers = packet.readInt16();
                for(local i = 0; i < lenghtMembers; i ++)
                    obj.members[packet.readInt32()] <- {rank = packet.readInt16(), name = packet.readString()}

                PlayerStatistics.fractionObj = obj;
            break;
            case PlayerPackets.FractionSetup:
                Hero.fractionId = packet.readInt16();
                Hero.fractionRoleId = packet.readInt16();
            break;
            case PlayerPackets.OneTimeEffect:
                local playerId = packet.readInt16();
                local effectId = packet.readInt16();
                addEffect(playerId, System_Effects[effectId.tostring()])
            case PlayerPackets.GodMode:
                getPlayer(heroId).setGodMode(packet.readBool());
            break;
            case PlayerPackets.Lute:
                PlayerLute.playSound(packet.readInt16(), packet.readInt16(), packet.readBool(), packet.readInt16());
            break;
            case PlayerPackets.StealFromUser:
                local result = packet.readBool();

                if(result == false)
                    PlayerStealInteraction.hide();
                else {
                    local lenght = packet.readInt16();
                    local items = {};
                    for(local i = 0; i < lenght; i ++)
                        items[packet.readString()] <- packet.readInt16();

                    PlayerStealInteraction.load(items);
                }
            break;
            case PlayerPackets.QuoteItem:
                Inventory.renderQuote(packet.readString());
            break;
            case PlayerPackets.StartSearch:
                InventorySearch.start();
            break;
            case PlayerPackets.RunSearch:
                InventorySearch.run(packet.readString(), packet.readInt32());
            break;
            case PlayerPackets.EndSearch:
                InventorySearch.end();
            break;
            case PlayerPackets.GetDescription:
                local len = packet.readInt16();
                local text = [];
                for(local i = 0; i < len; i ++)
                    text.push(packet.readString());

                OtherPlayerDescription.show(text);
            break;
            case PlayerPackets.Invisible:
                setPlayerInvisible(heroId, packet.readBool());
            break;
            case PlayerPackets.Masked:
                setPlayerMasked(packet.readInt16(), packet.readBool());
            break;
            case PlayerPackets.Inventory:
                Hero.inventory = PlayerInventory(packet.readUInt16());
                Hero.inventory.ui = item.Bucket.UI(Hero.inventory);
                item.Controller.buckets[Hero.inventory.id] <- Hero.inventory.weakref();
            break;
        }
    }
}

getPlayer <- @(id) PlayerController.players[id];
getPlayers <- @() PlayerController.players;

addEventHandler("onInit", PlayerController.onInit.bindenv(PlayerController));
addEventHandler("onDamage", PlayerController.onDamage.bindenv(PlayerController));
RegisterPacketReceiver(Packets.Player, PlayerController.onPacket.bindenv(PlayerController));

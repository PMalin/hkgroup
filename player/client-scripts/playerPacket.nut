class PlayerPacket
{
    packet = null;

    constructor()
    {
        packet = ExtendedPacket(Packets.Player);
    }

    function playLute(soundId, loopMode, volume) {
        packet.writeUInt8(PlayerPackets.Lute)
        packet.writeInt16(soundId);
        packet.writeBool(loopMode);
        packet.writeInt16(volume);
        packet.send();
    }

    function summonSpell(spellId, targetId) {
        packet.writeUInt8(PlayerPackets.SpellSummon)
        packet.writeInt16(spellId);
        packet.writeInt16(targetId);
        packet.send();
    }

    function stealFromUser(targetId) {
        packet.writeUInt8(PlayerPackets.StealFromUser)
        packet.writeInt16(targetId);
        packet.send();
    }

    function itemUse(itemId) {
        packet.writeUInt8(PlayerPackets.UseItem)
        packet.writeUInt32(itemId);
        packet.send();
    }

    function throwItem(itemId, amount) {
        packet.writeUInt8(PlayerPackets.ThrowItem)
        packet.writeUInt32(itemId);
        packet.writeInt16(amount);
        packet.send();
    }

    function playerFractionInfo(fractionId)
    {
        packet.writeUInt8(PlayerPackets.Fraction)
        packet.writeInt16(fractionId);
        packet.send();
    }

    function getPlayerFractionInfo()
    {
        packet.writeUInt8(PlayerPackets.FractionInfo)
        packet.send();
    }

    function getPlayerManageFractionPlayer(playerId)
    {
        packet.writeUInt8(PlayerPackets.FractionManage)
        packet.writeInt16(playerId);
        packet.send();
    }

    function recovery(value)
    {
        packet.writeUInt8(PlayerPackets.Recovery);
        packet.writeFloat(value);
        packet.send();
    }

    function sendVisual(bodyModel, bodyTxt, headModel, headTxt) {
        packet.writeUInt8(PlayerPackets.Visual)
        packet.writeString(bodyModel);
        packet.writeInt16(bodyTxt);
        packet.writeString(headModel);
        packet.writeInt16(headTxt);
        packet.send();
    }

    function sendScale(playerId, x, y, z) {
        packet.writeUInt8(PlayerPackets.Scale)
        packet.writeInt16(playerId);
        packet.writeFloat(x);
        packet.writeFloat(y);
        packet.writeFloat(z);
        packet.send();
    }

    function sendWalkingStyle(playerId, walkId) {
        packet.writeUInt8(PlayerPackets.WalkingStyle)
        packet.writeInt16(playerId);
        packet.writeInt16(walkId);
        packet.send();
    }

    function sendFatness(playerId, fatnessId) {
        packet.writeUInt8(PlayerPackets.Fatness)
        packet.writeInt16(playerId);
        packet.writeInt16(fatnessId);
        packet.send();
    }

    function sendSpeech(playerId, fatnessId) {
        packet.writeUInt8(PlayerPackets.Speech)
        packet.writeInt16(playerId);
        packet.writeInt16(fatnessId);
        packet.send();
    }

    function setPersonalCard(personalCard) {
        packet.writeUInt8(PlayerPackets.PersonalCard)
        packet.writeInt16(personalCard.len());
        foreach(item in personalCard)
            packet.writeString(item);

        packet.send();
    }

    function setRealName(targetId, name) {
        packet.writeUInt8(PlayerPackets.RealName)
        packet.writeInt16(targetId);
        packet.writeString(name);
        packet.send();
    }

    function upgradeAttribute(attributeId) {
        packet.writeUInt8(PlayerPackets.Upgrade)
        packet.writeInt16(attributeId);
        packet.send();
    }

    function upgradeSkill(attributeId) {
        packet.writeUInt8(PlayerPackets.SkillUpgrade)
        packet.writeInt16(attributeId);
        packet.send();
    }

    function useSprint() {
        packet.writeUInt8(PlayerPackets.UseSprint)
        packet.send();
    }

    function setPersonalNotes(personalNotes) {
        packet.writeUInt8(PlayerPackets.PersonalNotes)
        packet.writeInt16(personalNotes.len());
        foreach(item in personalNotes)
            packet.writeString(item);

        packet.send();
    }

    function removePlayerFraction(realPlayerId) {
        packet.writeUInt8(PlayerPackets.FractionRemove)
        packet.writeInt32(realPlayerId);
        packet.send();
    }

    // Character Packets

    function playCharacter(characterId)
    {
        packet.writeUInt8(PlayerPackets.CharacterAccept)
        packet.writeInt16(characterId);
        packet.send();
    }

    function addCharacter(name)
    {
        packet.writeUInt8(PlayerPackets.CharacterAdd)
        packet.writeString(name);
        packet.send();
    }

    function shootItem(instance)
    {
        packet.writeUInt8(PlayerPackets.Shoot)
        packet.writeString(instance);
        packet.send();
    }

    function sendAsk(playerAskId, askType, askValue) {
        packet.writeUInt8(PlayerPackets.Ask)
        packet.writeInt16(playerAskId);
        packet.writeInt16(askType);
        packet.writeString(askValue);
        packet.send();
    }

    function rejectAsk() {
        packet.writeUInt8(PlayerPackets.RejectAsk)
        packet.send();
    }

    function acceptAsk(type) {
        packet.writeUInt8(PlayerPackets.AcceptAsk)
        packet.writeInt16(type)
        packet.send();
    }

    function sendPlayerDescription(focusId) {
        packet.writeUInt8(PlayerPackets.GetDescription);
        packet.writeInt16(focusId);
        packet.send();
    }
}
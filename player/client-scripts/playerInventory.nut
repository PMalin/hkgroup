
class PlayerInventory extends item.Bucket
{
    melee = -1
    ranged = -1
    armor = -1
    shield = -1
    helmet = -1

    constructor(_id) {
        melee = -1
        ranged = -1
        armor = -1
        shield = -1
        helmet = -1

        base.constructor(_id);
    }

    function clear() {
        melee = -1
        ranged = -1
        armor = -1
        shield = -1
        helmet = -1

        ids.clear();
        item.Packet(this).resend();
    }

    function addItem(itemId) {
        ids.push(itemId);
    }

    function removeItem(itemId) {
        if(melee == itemId) melee = -1;
        else if(ranged == itemId) ranged = -1;
        else if(armor == itemId) armor = -1;
        else if(shield == itemId) shield = -1;
        else if(helmet == itemId) helmet = -1;

        local index = ids.find(itemId);

        if(index != null)
            ids.remove(index);
    }

    function onRefresh() {
        Inventory.refresh();

        if(ui != null)
            ui.refresh();

        weight = calculateWeight();
    }
}
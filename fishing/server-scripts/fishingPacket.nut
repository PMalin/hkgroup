
class FishingPacket
{
    packet = null;
    object = null;

    constructor(fishingObject)
    {
        packet = ExtendedPacket(Packets.Fishing);
        object = fishingObject;
    }

    function create(targetId = -1) {
        packet.writeUInt8(FishingPackets.Create)
        packet.writeInt32(object.id);
        packet.writeString(object.scheme);
        packet.writeFloat(object.position.x);
        packet.writeFloat(object.position.y);
        packet.writeFloat(object.position.z);
        packet.writeFloat(object.rotation.x);
        packet.writeFloat(object.rotation.y);
        packet.writeFloat(object.rotation.z);
        packet.writeInt16(object.slots);

        if(targetId == -1)
            packet.sendToAll(RELIABLE);
        else
            packet.send(targetId, RELIABLE);
    }

    function deleteFishing() {
        packet.writeUInt8(FishingPackets.Delete)
        packet.writeInt32(object.id);
        packet.sendToAll(RELIABLE);
    }
}
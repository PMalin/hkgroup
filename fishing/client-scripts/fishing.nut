
class Fishing
{
    id = -1
    position = null
    rotation = -1

    scheme = -1
    slots = -1

    draw = null
    vob = null

    constructor(_id)
    {
        id = _id
        scheme = 0

        position = {
            x = 0,
            y = 0,
            z = 0
        }

        rotation = 0
        slots = 0

        draw = null
        vob = null
    }

    function addToWorld() {
        draw = WorldInterface.Draw3d(position.x,position.y + 67,position.z);
        draw.addLine("Z�o�a: "+slots+"/"+getScheme().slots);
        draw.setFont("FONT_OLD_20_WHITE_HI.TGA");
        draw.distance = 600;
        draw.setScale(0.4, 0.4);
        draw.visible = true;
        updateDrawText();

        vob = MobInter(getScheme().vob);
        vob.setPosition(position.x, position.y,position.z);
        vob.setRotation(rotation.x,rotation.y,rotation.z);
        vob.cdDynamic = true;
        vob.cdStatic = true;
        vob.addToWorld();
        vob.floor();

        update(slots);
    }

    function updateDrawText() {
        draw.setLineText(0, "Z�o�a: "+slots+"/"+getScheme().slots);
    }

    function update(_slots) {
        slots = _slots;
        updateDrawText();
    }

    function remove()  {
        draw.remove();
    }

    function getScheme() {
        return Config["FishingSystem"][scheme];
    }
}
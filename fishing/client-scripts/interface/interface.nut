
FishingInterface <- {
    fishingId = -1,
    bucket = null,
    eggTimer = null,

    prepareWindow = function()
    {
        local result = {};

        result.window <- GUI.Window(anx(Resolution.x/2 - 120), any(Resolution.y/2 - 252), anx(240), any(220), "HK_BOX.TGA", null, false);
        result.lumberButton <- GUI.Button(anx(20), any(40), anx(200), any(60), "HK_BUTTON.TGA","��w ryby", result.window);
        result.closeButton <- GUI.Button(anx(20), any(120), anx(200), any(60), "HK_BUTTON.TGA","Wyjd�", result.window);

        result.lumberButton.bind(EventType.Click, function(element) {
            if(getPlayerStamina(heroId) < 5.0) {
                addPlayerNotification(heroId, "Jeste� zbyt zm�czony.")
                return;
            }

            FishingInterface.eggTimer = EggTimer(5000);
            FishingInterface.bucket.window.setVisible(false);
            FishingInterface.eggTimer.attribute = FishingInterface.fishingId;
            FishingInterface.eggTimer.onEnd = function(eleobjectment) {
                FishingPacket(getFishing(eleobjectment.attribute)).takeFishing();
                FishingInterface.hide();
            }
        });

        result.closeButton.bind(EventType.Click, function(element) {
            FishingInterface.hide();
        });

        return result;
    }

    show = function(_fishingId)
    {
        if(bucket == null)
            bucket = prepareWindow();

        fishingId = _fishingId;
        BaseGUI.show();
        ActiveGui = PlayerGUI.Fishing;
        bucket.window.setVisible(true);
    }

    hide = function()
    {
        bucket.window.setVisible(false);

        fishingId = -1;
        BaseGUI.hide();
        ActiveGui = null;

        playAni(heroId,"S_RUN");
        resetEggTimer();
        eggTimer = null;
    }

    onForceCloseGUI = function()
    {
        if(ActiveGui == PlayerGUI.Fishing)
            hide();
    }
}

addEventHandler("onForceCloseGUI", FishingInterface.onForceCloseGUI.bindenv(FishingInterface));
Bind.addKey(KEY_ESCAPE, FishingInterface.hide.bindenv(FishingInterface), PlayerGUI.Fishing)
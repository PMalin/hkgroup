
class FishingController
{
    static fishings = {};

    static function interactWithFishing(_fishingId) {
        local pos = getPlayerPosition(heroId);
        local fishingId = getNearFishing(pos);
        if(fishingId == null || fishingId != _fishingId)
            return;

        if(hasItemThatIsNotDestroyed("ITMW_HK_2H_OTH_01") == false) {
            addPlayerNotification(heroId, "Nie masz w�dki.");
            return;
        }

        local fishing = fishings[fishingId];
        if(fishing.slots <= 0) {
            addPlayerNotification(heroId, "Brak zasobu.");
            return;
        }

        local eggTimer = EggTimer(5000);
        BaseGUI.show();
        eggTimer.onEnd = function(eleobjectment) {
            BaseGUI.hide();
        }

        unequipMeleeWeapon(heroId);
        FishingPacket(fishing).takeFishing();
    }

    static function getNearFishing(pos)
    {
        foreach(fishingId, fishing in fishings)
            if(getPositionDifference(pos, fishing.position) < 500)
                return fishingId;

        return null;
    }

    static function onPacket(packet)
    {
        local typ = packet.readUInt8();

        switch(typ)
        {
            case FishingPackets.Create:
                local id = packet.readInt32();
                local scheme = packet.readString();
                local positionX = packet.readFloat();
                local positionY = packet.readFloat();
                local positionZ = packet.readFloat();
                local rotationX = packet.readFloat();
                local rotationY = packet.readFloat();
                local rotationZ = packet.readFloat();
                local slots = packet.readInt16();

                local obj = Fishing(id);
                obj.scheme = scheme;
                obj.position = {x = positionX, y = positionY, z = positionZ};
                obj.rotation = {x = rotationX, y = rotationY, z = rotationZ};
                obj.slots = slots;
                obj.addToWorld();

                fishings[id] <- obj;
                callEvent("onFishingAdd", id);
            break;
            case FishingPackets.Delete:
                local id = packet.readInt32();
                if(!(id in fishings))
                    return;

                getFishing(id).remove();
                fishings.rawdelete(id);
                callEvent("onFishingRemove", id);
            break;
            case FishingPackets.Update:
                local length = packet.readInt16();
                for(local i = 0; i < length; i ++)
                    fishings[packet.readInt32()].update(packet.readInt16())
            break;
        }
    }
}

getFishing <- @(id) FishingController.fishings[id];
getFishings <- @() FishingController.fishings;

RegisterPacketReceiver(Packets.Fishing, FishingController.onPacket.bindenv(FishingController));
